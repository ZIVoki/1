﻿using System;
using System.Windows.Forms;
using Tasker.Server.Services;
using System.ServiceModel;

namespace Host
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = string.Format("Tasker server (ver. {0})", Application.ProductVersion);

            DialogResult dialogResult = DialogResult.OK;
            while (dialogResult == DialogResult.OK)
            {
                ServiceHost serviceHost = new ServiceHost(typeof(WorkerService));
                ServiceHost managerServiceHost = new ServiceHost(typeof(ManagerService));
                try
                {
                    serviceHost.Open();
                    Console.WriteLine("Worker service started...");

                    foreach (var endpoint in serviceHost.Description.Endpoints)
                    {
                        Console.WriteLine(endpoint.Address);
                    }


                    managerServiceHost.Open();
                    Console.WriteLine("Manager service started...");
                    foreach (var endpoint in managerServiceHost.Description.Endpoints)
                    {
                        Console.WriteLine(endpoint.Address);  
                    }
                    Console.WriteLine("Press <Enter> to restart");
                    Console.ReadLine();
                }
                catch (Exception e)
                {
//                    string message = string.Format("Service Faulted: {0} {1}", e, e.StackTrace);
//                    ServerFaultWindow serverFaultWindow = new ServerFaultWindow(message);
//                    dialogResult = serverFaultWindow.ShowDialog();
                    Console.WriteLine(e.StackTrace);
                    Console.ReadLine();
                }
                finally
                {
                    if (serviceHost.State == CommunicationState.Opened)
                    {
                        Console.WriteLine("Worker service closed...");
                        serviceHost.Close();
                    }
                    if (managerServiceHost.State == CommunicationState.Opened)
                    {
                        Console.WriteLine("Manager service closed...");
                        managerServiceHost.Close();
                    }
                }
            }
        }
    }
}
