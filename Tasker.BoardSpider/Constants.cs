using System;

namespace Tasker.BoardSpider
{
    public class Constants
    {
        public enum Statuses
        {
            Nothing = 0,
            Enabled = 1,
            Disabled = 2,
            Deleted = 3,
            Planing = 4,
            Closed = 5,
            InProcess = 6,
            SendedForReview = 7,
            UserEnter = 8,
            UserLeave = 9,
            UserLunch = 10
        }
        
        public static string LastVersionFolderName
        {
            get { return "prev"; }
        }

        public static String AvailableDiffFormats
        {
            get { return ".docx .xlsx"; }
        }
    }
}