﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Data.Entity;
using Microsoft.Office.Interop.Word;
using NLog;
using Tasker.BoardSpider.Properties;

namespace Tasker.BoardSpider
{
    class Program
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        static void Main(string[] args)
        {
            Logger.Info("Spider run.");
            try
            {
            UpdateAllBoardFilesAsync();
            FindAndCreateNewFiles();
        }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Logger.Error(e.ToString());
            }
            Logger.Info("Spider stop..");
        }

        private static void FindAndCreateNewFiles()
        {
            using (TaskerEntities context = new TaskerEntities())
            {
                List<folders> folderses = context.folders.Where(f => f.childFolderId == null).ToList();
                foreach (var nextDirPath in folderses)
                {
                    Logger.Info(String.Format("Mapping directory {0}", nextDirPath));
                    MapDirectoryToDB(nextDirPath, context);
                }
            }
        }

        // map only new files, do no map dirs
        private static void MapDirectoryToDB(folders folder, TaskerEntities context)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(folder.path);

            FileInfo[] dirFiles = directoryInfo.GetFiles();
            foreach (var nextFile in dirFiles)
            {
                // skip files with wrong extension
                if (!Settings.Default.ExtensionsFilter.Contains(nextFile.Extension)) continue;
                // skip exist files
                if (context.files.Any(f => f.filePath == nextFile.FullName)) continue;

                Logger.Info(String.Format("Adding new file {0} to db", nextFile.FullName));

                fileVersions newFileVersion = context.fileVersions.Create<fileVersions>();
                files newFile = new files
                {
                    filePath = nextFile.FullName,
                    title = nextFile.Name,
                    fileSource = nextFile.FullName,
                    folders = folder,
                    statusId = (int)Constants.Statuses.Enabled
                };
                string sourceFilePath = Path.Combine(folder.path, Settings.Default.SourceFolderName, newFile.title);
                if (File.Exists(sourceFilePath))
                    newFile.fileSource = sourceFilePath;
                CreatePrevDirectoryIfNotExist(sourceFilePath);

                newFileVersion.files = newFile;
                newFileVersion.changesDate = nextFile.LastWriteTime;
                newFileVersion.version = 1;
                newFileVersion.versionsDiff = "";
                newFileVersion.statusId = (int)Constants.Statuses.Enabled;

                context.fileVersions.Add(newFileVersion);
                context.SaveChanges();

                SetUread(newFileVersion, context);
            }
        }

        private static void MapSourceDirectoryToDB(DirectoryInfo directoryInfo, folders newFolder, TaskerEntities context)
        {
            FileInfo[] dirFiles = directoryInfo.GetFiles();
            foreach (var nextFile in dirFiles)
            {
                string thisFileName = Path.GetFileNameWithoutExtension(nextFile.Name);
                files file =
                    context.files.FirstOrDefault(fn => fn.title.Contains(thisFileName));
                if (file == null) continue;
                file.fileSource = nextFile.FullName;
                context.SaveChanges();
            }
        }

        private static void UpdateAllBoardFilesAsync()
        {
            using (TaskerEntities context = new TaskerEntities())
            {
                List<files> eFiles =
                    context.files.Include(f => f.fileVersions).Include(fv => fv.folders).Include(
                        fvv => fvv.folders.folders1).Where(t => t.statusId == (int) Constants.Statuses.Enabled).ToList();

                foreach (var file in eFiles)
                {
                    DoUpdateBoardFile(file, context);
                }
            }
        }

        private static void DoUpdateBoardFile(files fileToUpdate, TaskerEntities context)
        {
            string oldSourceFilePath = GetOldSourceFilePath(fileToUpdate);
            FileInfo oldSourceFile = new FileInfo(oldSourceFilePath);
            FileInfo currentSourceFile = new FileInfo(fileToUpdate.fileSource);

            if (currentSourceFile.LastWriteTimeUtc > oldSourceFile.LastWriteTimeUtc)
            {
                // исходный файл документа был изменен
                string diffString = "";
                if (File.Exists(oldSourceFilePath) &&
                    Constants.AvailableDiffFormats.Contains(oldSourceFile.Extension))
                {
                    diffString = CretateDiff(oldSourceFilePath, fileToUpdate.fileSource);
                }

                fileVersions lastFileVersion = fileToUpdate.fileVersions.OrderByDescending(v => v.version).FirstOrDefault();
                fileVersions newFileVesion = CreateNewFileVesion(lastFileVersion, diffString);

                try
                {
                    CreatePrevDirectoryIfNotExist(fileToUpdate.filePath);

                    // сохраняем новую версию файла
                    File.Copy(currentSourceFile.FullName, oldSourceFile.FullName, true);
                    // сохраняем pdf если это word файл
                    SaveDocToPdf(fileToUpdate.fileSource, fileToUpdate.filePath);

                    context.fileVersions.Add(newFileVesion);
                    context.SaveChanges();

                    // создаем записи о прочтении документа
                    SetUread(newFileVesion, context);
                }
                catch (Exception e)
                {
                    String message = String.Format("Ошибка во время обновленяи файла {0}", e.Message);
                    Console.WriteLine(message);
                    Logger.Error(message);
                }
            }
        }

        private static void CreatePrevDirectoryIfNotExist(string currentSourceFile)
        {
            string fileDirName = Path.GetDirectoryName(currentSourceFile);
            string lastFileVersionDir = Path.Combine(fileDirName, Constants.LastVersionFolderName);
            bool exists = Directory.Exists(lastFileVersionDir);
            if (!exists)
            {
                DirectoryInfo directoryInfo = Directory.CreateDirectory(lastFileVersionDir);
                directoryInfo.Attributes = FileAttributes.Directory | FileAttributes.Hidden;
            }
        }

        private static void SetUread(fileVersions newFileVesion, TaskerEntities context)
        {
            Logger.Info(String.Format("Set unread file {0}", newFileVesion.files.title));

            List<users> queryable = context.users.Where(u => u.statusId == (int) Constants.Statuses.Enabled).ToList();
            foreach (var user in queryable)
            {
                usersFileVersion usersFileVersion = new usersFileVersion
                {
                    userId = user.userId,
                    fileVersionId = newFileVesion.fileVersionId,
                    statusId = (int)Constants.Statuses.Enabled
                };

                newFileVesion.usersFileVersion.Add(usersFileVersion);
            }
            context.fileVersions.Add(newFileVesion);

            context.SaveChanges();
        }


        private static string GetOldSourceFilePath(files file)
        {
            string directoryName = Path.GetDirectoryName(file.filePath);
            string sourceFileName = Path.GetFileName(file.fileSource);
            string sourceFilePath = Path.Combine(directoryName, Constants.LastVersionFolderName,
                                                 sourceFileName);
            return sourceFilePath;
        }

        private static string CretateDiff(string oldSourceFilePath, string sourceFilePath)
        {
            string result = "";
/*
            using (MemoryStream stream = new MemoryStream())
            {
                stream.Position = 0;
                StreamReader streamReader = new StreamReader(stream);

                XmlWriter diffgramWriter = new XmlTextWriter(stream, Encoding.Unicode);
                XmlDiff xmldiff = new XmlDiff(XmlDiffOptions.IgnoreChildOrder |
                                              XmlDiffOptions.IgnoreNamespaces |
                                              XmlDiffOptions.IgnorePrefixes);
                bool bIdentical = xmldiff.Compare(oldSourceFilePath, sourceFilePath, false, diffgramWriter);
                diffgramWriter.Close();

                if (bIdentical)
                    result = streamReader.ReadToEnd();
            }
*/

            return result;
        }

        private static fileVersions CreateNewFileVesion(fileVersions lastFileVersion, string diffString)
        {
            Logger.Info(String.Format("Setting new file version {0}", lastFileVersion.files.title));

            fileVersions newFileVesion = new fileVersions
            {
                fileId = lastFileVersion.fileId,
                version = lastFileVersion.version + 1,
                statusId = (int)Constants.Statuses.Enabled
            };
            FileInfo fileInfo = new FileInfo(lastFileVersion.files.filePath);
            // запоминаем дату последней записи
            newFileVesion.changesDate = fileInfo.LastWriteTime;
            newFileVesion.versionsDiff = diffString;

            return newFileVesion;
        }

        private static void SaveDocToPdf(string wordSourcePath, string destPath)
        {
            const string availableConvertFormats = ".doc .docx";
            FileInfo wordFile = new FileInfo(wordSourcePath);

            if (!availableConvertFormats.Contains(wordFile.Extension)) return;

            Logger.Info(String.Format("Save doc to pdf {0}", wordSourcePath));

            // Create a new Microsoft Word application object
            Application word = new Application();

            // C# doesn't have optional arguments so we'll need a dummy value
            object oMissing = System.Reflection.Missing.Value;

            //            word.Visible = false;
            word.ScreenUpdating = false;

            // Cast as Object for word Open method
            Object filename = (Object)wordFile.FullName;

            // Use the dummy value as a placeholder for optional arguments
            Document doc = word.Documents.Open(ref filename, ref oMissing,
                ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                ref oMissing, ref oMissing, ref oMissing, ref oMissing);
            doc.Activate();

            object outputFileName = destPath;
            /*
                        if (wordFile.Extension == ".doc")
                             outputFileName = wordFile.FullName.Replace(".doc", ".pdf");
                        else
                            outputFileName = wordFile.FullName.Replace(".docx", ".pdf");
            */

            object fileFormat = WdSaveFormat.wdFormatPDF;

            // Save document into PDF Format
            doc.SaveAs(ref outputFileName,
                ref fileFormat, ref oMissing, ref oMissing,
                ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                ref oMissing, ref oMissing, ref oMissing, ref oMissing);

            // Close the Word document, but leave the Word application open.
            // doc has to be cast to type _Document so that it will find the
            // correct Close method.                
            object saveChanges = WdSaveOptions.wdDoNotSaveChanges;
            ((_Document)doc).Close(ref saveChanges, ref oMissing, ref oMissing);
            doc = null;


            // word has to be cast to type _Application so that it will find
            // the correct Quit method.
            ((_Application)word).Quit(ref oMissing, ref oMissing, ref oMissing);
            word = null;
        }
    }
}
