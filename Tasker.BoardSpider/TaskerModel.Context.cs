﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tasker.BoardSpider
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class TaskerEntities : DbContext
    {
        public TaskerEntities()
            : base("name=TaskerEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public DbSet<files> files { get; set; }
        public DbSet<fileVersions> fileVersions { get; set; }
        public DbSet<folders> folders { get; set; }
        public DbSet<users> users { get; set; }
        public DbSet<usersFileVersion> usersFileVersion { get; set; }
    }
}
