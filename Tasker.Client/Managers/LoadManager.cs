﻿using System;
using Tasker.Tools.Managers;
using Tasker.Client.Properties;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Client.Managers
{
    public static class LoadManager
    {
        public static void LoadData()
        {
            LoadCurrentTask();
            LoadTaskTypes();
            LoadUnreadMessages();
            LoadTimeTable();
        }

        public static void LoadSelfStatistic(DateTime month)
        {
            // loading only current month statistic
            DateTime startDate = new DateTime(month.Year, month.Month, 1);
            DateTime endDate = startDate.AddMonths(1).AddMilliseconds(-1);
            TimeSpan taskTotalHoursByDates = LoginManager.ServiceChannel.GetTaskTotalHoursByDates(startDate, endDate);
            Repository.MonthTotalHours = taskTotalHoursByDates;
        }

        private static void LoadTaskTypes()
        {
            var myTaskTypes = LoginManager.ServiceChannel.GetMyUserTaskTypes(Repository.LoginUser.Id);
            foreach (var dto in myTaskTypes)
            {
                if (UserTaskTypeManager.GetUserTaskType(dto, Repository.LoginUser, Repository.TaskTypes) != null)
                {
                    UserTaskTypeManager.UserTaskTypeUpdate(dto, Repository.Users, Repository.TaskTypes);
                }
            }
        }

        private static void LoadTimeTable()
        {
            TimeTableDTO[] timeTableDtos = LoginManager.ServiceChannel.GetMyTimeTables(DateTime.Now.Date, DateTime.Now.Date.AddDays(366));
            foreach (var timeTableDto in timeTableDtos)
            {
                if (TimeTableManager.GetTimeTable(timeTableDto, Repository.TimeTable, Repository.Users, Repository.Shifts) != null)
                {
                    TimeTableManager.UpdateTimeTableView(timeTableDto, Repository.TimeTable, Repository.Users, Repository.Shifts);
                }
            }
        }

        private static void LoadUnreadMessages()
        {
            //load my Unread messages
            MessageDTO[] messages = LoginManager.ServiceChannel.GetUnreadMessages();
            foreach (var dto in messages)
            {
                if (Repository.CurrentTask != null && Repository.CurrentTask.Id == dto.Task.Id)
                {
                     MessageManager.GetMessage(dto, Repository.CurrentTask, Repository.CurrentTask.Messages.Messages,
                                              Repository.Users, Repository.LoginUser.Id);
                }
            }
        }

        private static bool LoadCurrentTask()
        {
            TaskDTO task = LoginManager.ServiceChannel.GetMyCurrentTask();
            if (task != null)
            {
                if(task.Status == ConstantsStatuses.Planing)
                {
                    WorkerCallback.invokeCurrentTaskState(Constants.TaskFormType.NewTask, task);
                }
                else
                {
                    WorkerCallback.invokeCurrentTaskState(Constants.TaskFormType.BackgroundTaskSet, task);
                }
                
                // TODO if currentTaskAcceptTime is null get it from history on server
                Repository.AcceptCurrentTask = Settings.Default.CurrentTaskAcceptTime;
                return true;
            }

            return false;
        }

        public static void LoadTaskFiles(TaskView task)
        {
            FileDTO[] files = LoginManager.ServiceChannel.GetFilesData(task.Id);
            foreach (var fileDTO in files)
            {
                FileManager.GetFileData(fileDTO, Repository.CurrentTask);
            }
        }
    }

}
