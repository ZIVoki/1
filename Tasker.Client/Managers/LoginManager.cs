﻿using System;
using System.ComponentModel;
using System.ServiceModel;
using System.Threading;
using System.Windows;
using Tasker.Client.Managers.Logger;
using Tasker.Client.Properties;
using Tasker.Client.ServiceReference;
using Tasker.Tools.Resources;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Client.Managers
{
    public delegate void LostServerConnectionDelegate(object sender);
    class LoginManager
    {
        public static event LostServerConnectionDelegate LostServerConnectionEventHandler;

        public static void InvokeLostServerConnectionEventHandler()
        {
            LostServerConnectionDelegate handler = LostServerConnectionEventHandler;
            if (handler != null) handler(null);
        }

        private static BackgroundWorker pingServicesBackgroundWorker;

        private static IWorkerInterfaceChannel m_serviceChannel;
        public static IWorkerInterfaceChannel ServiceChannel
        {
            get
            {
                if (m_serviceChannel == null)
                {
                    return null;
                }
                if (m_serviceChannel.State == CommunicationState.Faulted)
                {
                    Relogin();
                }
                try
                {
                    m_serviceChannel.Ping(DateTime.Now);
                }
                catch (Exception ex)
                {
                    Relogin();
                }
                return m_serviceChannel;
            }
            set { m_serviceChannel = value; }
        }

        private static void Relogin()
        {
            try
            {
                Login(Repository.LoginUser.Login, Repository.LoginUser.Password,
                      Settings.Default.NetTcpAddress);
                
            }
            catch (Exception e)
            {
                Log.Write(LogLevel.Fatal, "Ошибка связи с сервером", e);
                Repository.LoginUser = null;
                MessageBox.Show("Не удалось связаться с сервером. Приложение будет закрыто",
                                "Ошибка связи с сервером", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(0);
            }
            if (m_serviceChannel.State == CommunicationState.Faulted)
            {
                Log.Write(LogLevel.Fatal, "Ошибка связи с сервером state = faulted.");
                Repository.LoginUser = null;
                MessageBox.Show("Не удалось связаться с сервером. Приложение будет закрыто",
                                "Ошибка связи с сервером", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(0);
            }
        }

/*
        private static IWorkerInterface m_serviceChannel;
        public static IWorkerInterface ServiceChannel
        {
            get
            {
                try
                {
                    var pingResult = m_serviceChannel.Ping(DateTime.Now);
                }
                catch (Exception ex)
                {
                    // перелогин
                    try
                    {
                        Login(Repository.LoginUser.Login, Repository.LoginUser.Password,
                              Settings.Default.NetTcpAddress);
                    }
                    catch (Exception e)
                    {
                        Log.Write(LogLevel.Fatal, "Ошибка связи с сервером", e);
                        Repository.LoginUser = null;
                        MessageBox.Show("Не удалось связаться с сервером. Приложение будет закрыто",
                                        "Ошибка связи с сервером", MessageBoxButton.OK, MessageBoxImage.Error);
                        Environment.Exit(0);
                    }
                }
                return m_serviceChannel;
            }
            set { m_serviceChannel = value; }
        }
*/

        public static bool Connect(string serverURI)
        {
            if (string.IsNullOrEmpty(serverURI))
            {
                DuplexChannelFactory<IWorkerInterfaceChannel> duplexChannelFactory =
                    new DuplexChannelFactory<IWorkerInterfaceChannel>(new InstanceContext(new WorkerCallback()));
                m_serviceChannel = duplexChannelFactory.CreateChannel();
            }
            else
            {
                string URI = string.Format("net.tcp://{0}/worker", serverURI);

                DuplexChannelFactory<IWorkerInterfaceChannel> duplexChannelFactory =
                    new DuplexChannelFactory<IWorkerInterfaceChannel>(new InstanceContext(new WorkerCallback()),
                                                                       "workerEndPoint",
                                                                       new EndpointAddress(URI));
                m_serviceChannel = duplexChannelFactory.CreateChannel();
            }
            //m_serviceChannel.Faulted += m_serviceChannel_Faulted;
            return true;
        }

        static void m_serviceChannel_Faulted(object sender, EventArgs e)
        {
            InvokeLostServerConnectionEventHandler();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="login"></param>
        /// <param name="password"></param>
        /// <param name="serverURI"></param>
        /// <returns></returns>
        public static string Login(string login, string password, string serverURI = null)
        {
            Connect(serverURI);

            LoginResultDTO loginSuccess;

            try
            {
                var result = m_serviceChannel.Ping(DateTime.Now);
            }
            catch (Exception ex)
            {
                Log.Write(LogLevel.Error, LoginManagerResources.NoServerResponse, ex);
                return LoginManagerResources.NoServerResponse ;
            }

            try
            {
                loginSuccess = ServiceChannel.Login(login, password);
            }
            catch (Exception ex)
            {
                Log.Write(LogLevel.Error, LoginManagerResources.AuthorizationError, ex);
                return LoginManagerResources.AuthorizationError;
            }

            if (!loginSuccess.Success)
            {
                Log.Write(LogLevel.Info, LoginManagerResources.AuthorizationError);
                return LoginManagerResources.AuthorizationError;
            }

            if(loginSuccess.Role != ConstantsRoles.Worker)
            {
                ServiceChannel.Logoff();
                Log.Write(LogLevel.Info, LoginManagerResources.WrongClientError);
                return LoginManagerResources.WrongClientError;
            }

            Repository.LoginUser = new WorkerView
                                         {
                                             Id = loginSuccess.User.Id,
                                             LastName = loginSuccess.User.LastName,
                                             FirstName = loginSuccess.User.FirstName,
                                             Status = loginSuccess.User.Status,
                                             WeeklyHours = loginSuccess.User.WeeklyHours,
                                             Role = ConstantsRoles.Worker,
                                             CurrentState = ConstantsNetworkStates.Online,
                                             LastActiveDate = loginSuccess.User.LastActiveDate,
                                             Login = login,
                                             Password = password
                                         };
            StartServicePing();

            Log.Write(LogLevel.Info, LoginManagerResources.AuthorizationSuccess);
            return LoginManagerResources.AuthorizationSuccess;
        }

        public static void Logoff()
        {
            Repository.CurrentTask = null;
            Repository.LoginUser = null;
            Repository.AcceptCurrentTask = null;
            Repository.TimeTable.Clear();
            
            pingServicesBackgroundWorker.CancelAsync();
            if (m_serviceChannel == null || m_serviceChannel.State == CommunicationState.Faulted) return;
            try
            {
                m_serviceChannel.Logoff();
                m_serviceChannel.Close();
            }
            catch (Exception e)
            {
                Log.Write(LogLevel.Error, e.Message);
            }
        }

        private static void StartServicePing()
        {
            if (pingServicesBackgroundWorker == null)
            {
                pingServicesBackgroundWorker = new BackgroundWorker();
                pingServicesBackgroundWorker.DoWork += backgroundWorker_DoWork;
                pingServicesBackgroundWorker.WorkerSupportsCancellation = true;
            }

            if (!pingServicesBackgroundWorker.IsBusy)
                pingServicesBackgroundWorker.RunWorkerAsync();
        }

        static void backgroundWorker_DoWork(object sender, DoWorkEventArgs eventArgs)
        {
            while (!Repository.LoginUser.IsEmpty)
            {
                try
                {
                    var pingResult = m_serviceChannel.Ping(DateTime.Now);
                }
                catch (Exception ex)
                {
                    // перелогин
                    try
                    {
                        string login = Login(Repository.LoginUser.Login, Repository.LoginUser.Password,
                                             Settings.Default.NetTcpAddress);
                        if (login == LoginManagerResources.AuthorizationSuccess)
                            LoadManager.LoadData();
                    }
                    catch (Exception e)
                    {
                        Log.Write(LogLevel.Fatal, "Ошибка связи с сервером", e);
                    }
                }
                Thread.Sleep(new TimeSpan(0, 0, 0, 30));
            }
        }
    }
}
