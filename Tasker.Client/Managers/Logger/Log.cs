using System;
using System.Collections.Generic;
using log4net;
using log4net.Config;

namespace Tasker.Client.Managers.Logger
{
    /// <summary>
    /// Write out messages using the logging provider.
    /// </summary>
    public static class Log
    {
        #region Members
        private static readonly log4net.ILog _logger = LogManager.GetLogger(typeof(Log));
        private static readonly Dictionary<LogLevel, Action<object>> _actions;
        #endregion

        /// <summary>
        /// Static instance of the log manager.
        /// </summary>
        static Log()
        {
            XmlConfigurator.Configure();
            _actions = new Dictionary<LogLevel, Action<object>>
                           {
                               {LogLevel.Debug, WriteDebug},
                               {LogLevel.Error, WriteError},
                               {LogLevel.Fatal, WriteFatal},
                               {LogLevel.Info, WriteInfo},
                               {LogLevel.Warning, WriteWarning}
                           };
        }
        /*
                /// <summary>
                /// Get the <see cref="NotifyAppender"/> log.
                /// </summary>
                /// <returns>The instance of the <see cref="NotifyAppender"/> log, if configured. 
                /// Null otherwise.</returns>
                public static NotifyAppender Appender
                {
                    get
                    {
                        foreach (ILog log in LogManager.GetCurrentLoggers())
                        {
                            foreach (IAppender appender in log.Logger.Repository.GetAppenders())
                            {
                                if (appender is NotifyAppender)
                                {
                                    return appender as NotifyAppender;
                                }
                            }
                        }
                        return null;
                    }
                }*/

        /// <summary>
        /// Write the message to the appropriate log based on the relevant log level.
        /// </summary>
        /// <param name="level">The log level to be used.</param>
        /// <param name="message">The message to be written.</param>
        /// <param name="ex"></param>
        /// <exception cref="ArgumentNullException">Thrown if the message is empty.</exception>
        public static void Write(LogLevel level, string message, Exception ex = null)
        {
            if (!string.IsNullOrEmpty(message))
            {
                if (level > LogLevel.Warning || level < LogLevel.Debug)
                    throw new ArgumentOutOfRangeException("level");

                // Now call the appropriate log level message.

                if (ex != null)
                {
                    var textAndException = new object[]
                                 {
                                     message, ex
                                 };
                    _actions[level](textAndException);
                }
                else
                {
                    _actions[level](message);
                }

            }
        }

        #region Action methods
        private static void WriteDebug(object message)
        {
            if (_logger.IsDebugEnabled)
                _logger.Debug(message);
        }

        private static void WriteError(object message)
        {
            if (_logger.IsErrorEnabled)
                if (message is object[])
                {
                    _logger.Error(((object[])message)[0], (Exception)((object[])message)[1]);
                }

        }

        private static void WriteFatal(object message)
        {
            if (_logger.IsFatalEnabled)
                _logger.Fatal(message);
        }

        private static void WriteInfo(object message)
        {
            if (_logger.IsInfoEnabled)
                _logger.Info(message);
        }

        private static void WriteWarning(object message)
        {
            if (_logger.IsWarnEnabled)
                _logger.Warn(message);
        }
        #endregion
    }
}
