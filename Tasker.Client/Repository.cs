﻿using System;
using Tasker.Tools.Extensions;
using Tasker.Tools.ViewModel;

namespace Tasker.Client
{
    internal class Repository
    {
        private static WorkerView _loginUser;

        public static WorkerView LoginUser
        {
            get
            {
                return _loginUser;
            }
            set
            {
                if (value == null)
                {
                    _loginUser = new WorkerView {FirstName = "", LastName = "", IsEmpty = true};
                }
                else
                {
                    _loginUser = value;
                    _loginUser.IsEmpty = false;
                }
            }
        }

        public static DateTime? AcceptCurrentTask { get; set; }
        public static TaskView CurrentTask { get; set; }
        public static UserView CurrentManager { get; set;}
        public static TimeSpan MonthTotalHours { get; set; }

        private static readonly UniqBindableCollection<CustomerView> _customers = new UniqBindableCollection<CustomerView>();
        private static readonly UniqBindableCollection<UserView> _users = new UniqBindableCollection<UserView>();
        private static readonly UniqBindableCollection<TaskTypeView> _taskTypes = new UniqBindableCollection<TaskTypeView>();
        private static readonly UniqBindableCollection<TimeTableView> _timeTable = new UniqBindableCollection<TimeTableView>();
        private static readonly UniqBindableCollection<ShiftView> _shifts = new UniqBindableCollection<ShiftView>();

        public static UniqBindableCollection<CustomerView> Customers
        {
            get { return _customers; }
        }

        public static UniqBindableCollection<UserView> Users
        {
            get { return _users; }
        }

        public static UniqBindableCollection<TaskTypeView> TaskTypes
        {
            get { return _taskTypes; }
        }

        public static UniqBindableCollection<TimeTableView> TimeTable
        {
            get { return _timeTable; }
        }

        public static UniqBindableCollection<ShiftView> Shifts
        {
            get { return _shifts; }
        }

        private static UniqBindableCollection<ChannelView> _channels = new UniqBindableCollection<ChannelView>();
        public static UniqBindableCollection<ChannelView> Channels
        {
            get { return _channels; }
        }
        
    }
}
