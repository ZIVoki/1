﻿using System.Windows;   
using Tasker.Client.Managers;
using Tasker.Client.Properties;

namespace Tasker.Client
{
    /// <summary>
    /// Interaction logic for ClientLoginWindow.xaml
    /// </summary>
    public partial class ClientLoginWindow : Window
    {
        private string lastLoginMessage;

        public ClientLoginWindow()
        {
            InitializeComponent();
            versionLabel.Content = string.Format("ver. {0}", MainWindow.CurrentVersion);
            loginControl.Focus();
        }

        private void loginControl_LoginEventHandler(object sender, Tools.Controls.LoginEventArgs eventArgs)
        {
            lastLoginMessage = "";
            SetMessageEventHandler setMessageEventHandler = SetMessage;
            this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, setMessageEventHandler);

            lastLoginMessage = LoginManager.Login(eventArgs.Login, eventArgs.Password, Settings.Default.NetTcpAddress);

            this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, setMessageEventHandler);

            if (Repository.LoginUser != null)
            {
                var loginSuccessDelegate = new LoginSuccessEventHandler(LoginSuccess);
                this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, loginSuccessDelegate);
                Settings.Default.LastLogin = eventArgs.Login;
                Settings.Default.Save();
                Constants.Password = eventArgs.Password;
            }
        }

        private delegate void LoginSuccessEventHandler();

        private delegate void SetMessageEventHandler();

        private void SetMessage()
        {
            systemMessageLabel.Content = lastLoginMessage;
            systemMessageLabel.Visibility = Visibility.Visible;
        }

        private void LoginSuccess()
        {
            this.DialogResult = true;
            this.Close();
        }

        private void loginControl_NewLoginEventHandler(object sender, Tools.Controls.NewOptionsEventArgs eventArgs)
        {
            if (!string.IsNullOrEmpty(eventArgs.AddressUri))
                Settings.Default.NetTcpAddress = eventArgs.AddressUri;
            if (eventArgs.LogOffMinutes > 0)
                Settings.Default.LogOffTime = eventArgs.LogOffMinutes;
            Settings.Default.AutoLogOff = eventArgs.AutoLofOffOn;

            Settings.Default.Save();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(Constants.Password))
                loginControl.SetPass(Constants.Password);
        }
    }
}
