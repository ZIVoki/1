﻿namespace Tasker.Client
{
    public class Constants
    {
        public enum DinnerFormState
        {
            Request,
            Started,
            Reject,
            Late,
            LimitExceed,
            None
        }

        public enum TaskFormType
        {
            NewTask,
            FinishTaskWait,
            WaitForTask,
            Success,
            UnsetTask,
            BackgroundTaskSet,
            ReopenTask
        }
        
        // to remember last password
        public static string Password { get; set; }
    }
}
