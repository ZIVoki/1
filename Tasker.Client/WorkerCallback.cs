﻿using System;
using System.Collections.Generic;
using Tasker.Client.Managers.Logger;
using Tasker.Tools.CustomEventArgs;
using Tasker.Tools.Managers;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Client
{
    public delegate void ArgumentContainsDelegate(object sender, ContentEventArgs e);
    public delegate void MultiArgumentContainsDelegate(object sender, ContentsEventArgs e);
    public delegate void TimeTableChangedEventHandler(object sender, TimeTableChangedeventArgs e);

    public class TimeTableChangedeventArgs
    {
        public TimeTableDTO TimeTable { get; set; }

        public TimeTableChangedeventArgs(TimeTableDTO timeTable)
        {
            TimeTable = timeTable;
        }
    }

    class WorkerCallback: ServiceReference.IWorkerInterfaceCallback
    {
        public static event ArgumentContainsDelegate LunchFormChangeState;
        public static event ArgumentContainsDelegate CurrentTaskPlainedTimeChanged;
        public static event TimeTableChangedEventHandler TimeTableChanged;
        private delegate void InvokeEventDelegate(Constants.DinnerFormState e);

        public static void InvokeTimeTableChanged(TimeTableChangedeventArgs e)
        {
            TimeTableChangedEventHandler handler = TimeTableChanged;
            if (handler != null) handler(null, e);
        }

        public static MultiArgumentContainsDelegate CurrentTaskState;

        public static List<MessageDTO> MessagesForNullableTask = new List<MessageDTO>();
        
        public void ReceiveMessage(MessageDTO msg)
        {
            if(Repository.CurrentTask != null && msg.Task.Id == Repository.CurrentTask.Id)
            {
                MessageManager.GetMessage(msg, Repository.CurrentTask, Repository.CurrentTask.Messages.Messages, Repository.Users, Repository.LoginUser.Id);
            }
            else if (Repository.CurrentTask == null)
            {
                MessagesForNullableTask.Add(msg);
            }
        }

        public void UserEnter(int userId)
        {
            //throw new NotImplementedException();
        }

        public void UserLogoff(int userId)
        {
            //throw new NotImplementedException();
        }

        public void TaskUpdated(TaskDTO task)
        {
            if (task.CurrentWorker != null && task.CurrentWorker.Id == Repository.LoginUser.Id)
            {
                if (Repository.CurrentTask != null && Repository.CurrentTask.Id == task.Id)
                {
                    if (task.PlannedTime != Repository.CurrentTask.PlannedTime)
                    {
                        invokeCurrentTaskPlannedTimeUpdated(task.PlannedTime);
                    }
                    TaskManager.UpdateTask(Repository.CurrentTask, task, Repository.Customers, Repository.TaskTypes, Repository.Users, Repository.Channels);
                }
            }
        }

        public void FileAdded(int taskId, FileDTO attachFile)
        {
            if (Repository.CurrentTask != null && Repository.CurrentTask.Id == taskId)
            {
                TaskManager.AddFile(attachFile, Repository.CurrentTask);
            }
        }

        public void TaskTypeAdded(TaskTypeDTO taskType)
        {
            TaskTypeManager.GetTaskType(taskType, Repository.TaskTypes);
        }

        public void TaskTypeUpdated(TaskTypeDTO taskType)
        {
            TaskTypeView taskTypeView = TaskTypeManager.GetTaskType(taskType, Repository.TaskTypes);
            TaskTypeManager.UpdateTaskType(taskTypeView,taskType);
        }

        public void UserTaskTypeAdded(UserTaskTypeDTO userTaskType)
        {
            if (Repository.LoginUser != null && Repository.LoginUser.Id == userTaskType.User.Id)
            {
                UserTaskTypeManager.GetUserTaskType(userTaskType, Repository.LoginUser, Repository.TaskTypes);
            }
        }

        public void UserTaskTypeUpdated(UserTaskTypeDTO userTaskType)
        {
            var utt = UserTaskTypeManager.GetUserTaskType(userTaskType, Repository.LoginUser, Repository.TaskTypes);
            UserTaskTypeManager.UserTaskTypeUpdate(utt, userTaskType, Repository.TaskTypes);
        }

        public void FaultExeptionServerAlarm(FaultDetail createFaultDetail)
        {
            string message = string.Format("Произошла ошибка на стороне сервера {0}", createFaultDetail.Message);
            Log.Write(LogLevel.Error, message);
            MainWindow.ShowServerError(message);
        }

        public void UserStateChanged(int userId, ConstantsNetworkStates stateId)
        {
            if(Repository.LoginUser != null && Repository.LoginUser.Id == userId)
            {
                switch (stateId)
                {
                    case ConstantsNetworkStates.Online:
                        break;
                    case ConstantsNetworkStates.Lunch:
                        invokeLunchFormChangeState(Constants.DinnerFormState.Started);
                        break;
                    case ConstantsNetworkStates.Depard:
                        break;
                    case ConstantsNetworkStates.Offline:
                        break;
                }
            }
        }

        public void CustomerUpdated(CustomerDTO customerDTO)
        {
            CustomerManager.CustomerUpdate(Repository.Customers, Repository.Channels, customerDTO);
        }

        public void TimeTableAdded(TimeTableDTO timeTable)
        {
            TimeTableManager.GetTimeTable(timeTable, Repository.TimeTable, Repository.Users, Repository.Shifts);
            Log.Write(LogLevel.Debug, String.Format("Добавлена новая ячейка в расписании на {0}.", timeTable.StartDate.Date));
        }

        public void UserUpdated(UserDTO userDTO)
        {
            if (Repository.LoginUser != null && Repository.LoginUser.Id == userDTO.Id)
            {
                UserManager.UserUpdate(Repository.LoginUser, userDTO);
            }
        }

        public void TaskSet(TaskDTO task)
        {
            //TODO Event! Show accept form and send apply from it after that set task to Repository.CurrentTask
            invokeCurrentTaskState(Constants.TaskFormType.NewTask, task);
        }

        public void TaskUnsetSet(int taskId)
        {
            if(Repository.CurrentTask != null && Repository.CurrentTask.Id == taskId)
            {
                invokeCurrentTaskState(Constants.TaskFormType.UnsetTask, Repository.CurrentTask);
            }
        }

        public void NewTask(TaskDTO task)
        {}

        public void SetStatusLunchTime(bool isLunch)
        {
            // if isLunch is true then automaticaly invoke UserStateChanged by server
            if(isLunch)
            {
                Repository.LoginUser.CurrentState = ConstantsNetworkStates.Lunch;
            }
            else
            {
                invokeLunchFormChangeState(Constants.DinnerFormState.Reject);
            }
        }

        public void SetTaskReviewResult(bool isSuccess)
        {
            if (isSuccess)
            {
                invokeCurrentTaskState(Constants.TaskFormType.Success, Repository.CurrentTask);
            }
            else
            {
                invokeCurrentTaskState(Constants.TaskFormType.ReopenTask, Repository.CurrentTask);
            }
        }

        public void TimeTableUpdated(TimeTableDTO timeTable)
        {
            TimeTableManager.UpdateTimeTableView(timeTable, Repository.TimeTable, Repository.Users, Repository.Shifts);
            InvokeTimeTableChanged(new TimeTableChangedeventArgs(timeTable));
            Log.Write(LogLevel.Debug, String.Format("Расписание от {0} изменено.", timeTable.StartDate.Date));
        }

        public void ShiftUpdated(ShiftDTO shift)
        {
            ShiftManager.ShiftUpdate(shift, Repository.Shifts);
        }

        public static void invokeLunchFormChangeState(Constants.DinnerFormState e)
        {
            ArgumentContainsDelegate handler = LunchFormChangeState;
            if (handler != null) handler(null, new ContentEventArgs(e));
        }

        public static void invokeCurrentTaskState(Constants.TaskFormType e, object dataContext)
        {
            MultiArgumentContainsDelegate handler = CurrentTaskState;
            if (handler != null) handler(null, new ContentsEventArgs(new[] { e, dataContext }));
        }

        public static void invokeCurrentTaskPlannedTimeUpdated(TimeSpan time)
        {
            ArgumentContainsDelegate handler = CurrentTaskPlainedTimeChanged;
            if (handler != null) handler(null, new ContentEventArgs(time));
        }
    }
}

