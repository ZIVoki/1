﻿using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Tasker.Client.Managers;
using Tasker.Client.Managers.Logger;
using Tasker.Tools.Extensions;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;
using Tasker.Tools.ViewModel.Base;

namespace Tasker.Client
{
    public class MainWindowViewModel : ObservableObject
    {
        private WorkerView _loginUser;
        private TaskView _currentTask;
        private readonly UniqBindableCollection<CustomerView> _customers = new UniqBindableCollection<CustomerView>();
        private readonly UniqBindableCollection<UserView> _users = new UniqBindableCollection<UserView>();
        private readonly UniqBindableCollection<TaskTypeView> _taskTypes = new UniqBindableCollection<TaskTypeView>();
        private readonly UniqBindableCollection<TimeTableView> _timeTable = new UniqBindableCollection<TimeTableView>();
        private readonly UniqBindableCollection<ShiftView> _shifts = new UniqBindableCollection<ShiftView>();
        private DateTime _lastLoginDateTime;

        #region Properties

        public WorkerView LoginUser
        {
            get { return _loginUser; }
            set
            {
                _loginUser = value;
                RaisePropertyChanged("LoginUser");
            }
        }

        public TaskView CurrentTask
        {
            get { return _currentTask; }
            set
            {
                _currentTask = value;
                RaisePropertyChanged("CurrentTask");
            }
        }

        #endregion

        #region Commands

        public ICommand LogOff
        {
            get { return new RelayCommand(LogOffExcecute, CanLogOffExecute); }
        }

        public ICommand AddFile
        {
            get { return new RelayCommand<String[]>(AddFileExecute, CanAddFileExecute); }
        }

        public ICommand SendMessage
        {
            get { return new RelayCommand<String>(SendMessageExecute, CanSendMessageExecute); }
        }

        public ICommand SendTaskForReview
        {
            get { return new RelayCommand(SendTaskForReviewExecute, CanSendTaskForReview); }
        }

        public ICommand GoLunch
        {
            get { return new RelayCommand(GoLunchExecute); }
        }

        public ICommand ShowTimeTable
        {
            get { return new RelayCommand(ShowTimeTableExecute); }
        }

        public ICommand ShowEvents
        {
            get { return new RelayCommand(ShowEventsExecute); }
        }

        public ICommand HideEvents
        {
            get { return new RelayCommand(HideEventsExecute); }
        }

        #endregion

        private void LogOffExcecute()
        {
            _currentTask = null;
            LoginManager.Logoff();
            ShowLoginForm();
        }

        private bool CanLogOffExecute()
        {
            return _loginUser != null;
        }

        private void AddFileExecute(string[] paths)
        {
            var dialogResult =
                    MessageBox.Show("Пометить последние добавленные файлы как 'Результирующие файлы задачи'?",
                                    "Итоговые файлы", MessageBoxButton.YesNo, MessageBoxImage.Question);
            foreach (var filePath in paths)
            {
                var fi = new FileInfo(filePath);
//                var ac = fi.GetAccessControl();

                FileDataView exist = null;
                if (Repository.CurrentTask.Files != null)
                {
                    exist = _currentTask.Files.FirstOrDefault(a => a.FilePath == filePath);
                }
                else
                {
                    _currentTask.Files = new UniqBindableCollection<FileDataView>();
                }

                if (exist == null)
                {
                    var file = new FileDataView(Repository.CurrentTask)
                    {
                        CreatedAt = DateTime.Now,
                        FilePath = filePath,
                        Title = fi.Name,
                        IsFinalyFile = dialogResult == MessageBoxResult.Yes,
                        Status = ConstantsStatuses.Enabled
                    };

                    _currentTask.Files.Add(file);
                    LoginManager.ServiceChannel.AddFileToTask(file.BuildDTO());
                }
            }
        }

        private bool CanAddFileExecute(string[] strings)
        {
            return (_currentTask != null && strings.Count() > 0);
        }

        private void SendMessageExecute(string param)
        {
            MessageView msg = new MessageView(_currentTask)
            {
                Text = param,
                UserDestination = _currentTask.Creator,
                UserFrom = _loginUser,
                SendDate = DateTime.Now,
                Status = ConstantsStatuses.Enabled
            };

            int id = LoginManager.ServiceChannel.SendMessage(msg.BuildDTO());
            if (id == 0)
            {
                const string message = "Не удалось отправить сообщение.";
                MessageBox.Show(message);
                Log.Write(LogLevel.Error, message);
            }
            else
            {
                msg.Id = id;
                msg.IsMy = true;
                msg.IsReaded = true;

                _currentTask.Messages.Messages.Add(msg);
            }
        }

        private bool CanSendMessageExecute(string obj)
        {
            return (_currentTask != null);
        }
        
        private void SendTaskForReviewExecute()
        {
            LoginManager.ServiceChannel.SendForReview(_currentTask.BuildDTO(), _currentTask.Creator.Id);
        }

        private bool CanSendTaskForReview()
        {
            return (_currentTask != null);
        }

        private void GoLunchExecute()
        {
            TimeTableView firstOrDefault = _timeTable.FirstOrDefault(t => t.StartDate <= DateTime.Now && t.EndDate >= DateTime.Now);
            if (firstOrDefault != null && firstOrDefault.FirstLunchStart.HasValue && firstOrDefault.SecondLunchStart.HasValue)
            {
                // todo limit execute cant send request
            }
            else
            {
                LoginManager.ServiceChannel.GoForLunchRequest(_currentTask.Creator.Id);
            }
        }

        private void ShowTimeTableExecute()
        {
            throw new NotImplementedException();
        }

        private void ShowEventsExecute()
        {
            throw new NotImplementedException();
        }

        private void HideEventsExecute()
        {
            throw new NotImplementedException();
        }

        private void ShowLoginForm()
        {
            if (Repository.LoginUser == null ||
                Repository.LoginUser.Role != ConstantsRoles.Worker)
            {
                var loginWindow = new ClientLoginWindow();
                bool? dilogResult = loginWindow.ShowDialog();
                if (dilogResult == false)
                {
                    Environment.Exit(0);
                }
                else
                {
                    _lastLoginDateTime = DateTime.Now;
                    LoadData();
                }
            }
        }

        private void LoadData()
        {
            LoadManager.LoadData();
        }
    }

}
