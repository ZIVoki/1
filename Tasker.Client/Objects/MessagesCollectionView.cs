﻿/*
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using Tasker.Tools.Extensions;
using Tasker.Tools.ViewModel;

namespace Tasker.Client.Objects
{
    public class MessagesCollectionView : INotifyPropertyChanged
    {
        private UniqBindableCollection<MessageView> m_messages = new UniqBindableCollection<MessageView>();

        public MessagesCollectionView()
        {
            m_messages.CollectionChanged += messagesChanges; 
        }

        private void messagesChanges(object sender, NotifyCollectionChangedEventArgs e)
        {
            if(e.Action == NotifyCollectionChangedAction.Add || e.Action == NotifyCollectionChangedAction.Remove)
            {
                OnPropertyChanged("UnreadCount");
                OnPropertyChanged("HasUnread");
            }
        }


        public int UnreadCount
        {
            get
            {
                return m_messages.Where(a=>!a.IsReaded).ToList().Count;
            }
        }

        public bool HasUnread
        {
            get
            {
                if(UnreadCount > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public UniqBindableCollection<MessageView> Messages
        {
            get
            {
                return m_messages;
            }
            set
            {
                m_messages = value;
                OnPropertyChanged("Messages");
            }
        }

        public void SetAllAsRead()
        {
            foreach(var message in m_messages)
            {
                message.IsReaded = true;
            }
            OnPropertyChanged("HasUnread");
        }

        #region INotifyPropertyChanged Members

        public virtual event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string info)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(info));
            }
        }
        #endregion
    }
}
*/
