﻿using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Tasker.Client.Managers;
using Tasker.Tools.Controls;
using Tasker.Tools.CustomEventArgs;
using Tasker.Tools.Extensions;
using Tasker.Tools.Helpers;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;
using System.Collections.Generic;
using Log = Tasker.Client.Managers.Logger.Log;
using Tasker.Client.Managers.Logger;

namespace Tasker.Client.Controls
{
    public delegate void ArgumentContainsDelegate(object sender, ContentEventArgs e);
    public delegate void MultiArgumentContainsDelegate(object sender, ContentsEventArgs e);
    /// <summary>
    /// Interaction logic for WorkerTaskControl.xaml
    /// </summary>
    public partial class WorkerTaskControl : UserControl
    {
        public TaskView CurrentTaskView
        {
            get { return (TaskView) DataContext; }
            set { DataContext = value; }
        }

        public static event MultiArgumentContainsDelegate NewUnreadMessageComeHandler;
        public event MultiArgumentContainsDelegate HasUnreadChangedHandler;

        public WorkerTaskControl()
        {
            InitializeComponent();
            DataContextChanged += WorkerTaskControl_DataContextChanged;
            WorkerCallback.CurrentTaskPlainedTimeChanged += OnCurrentTaskPlainedTimeChanged;
        }

        void WorkerTaskControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (CurrentTaskView == null) return;

            if (
                (CurrentTaskView.DeadLine.HasValue && 
                !CurrentTaskView.CreationDate.Date.Equals(CurrentTaskView.DeadLine.Value.Date) ||
                !CurrentTaskView.CreationDate.Date.Equals(DateTime.Now.Date)))
            {
                taskStartDateTimeLabel.Visibility = Visibility.Visible;
                taskDeadlineDateTimeLabel.Visibility = Visibility.Visible;
            }
            else
            {
                taskStartDateTimeLabel.Visibility = Visibility.Hidden;
                taskDeadlineDateTimeLabel.Visibility = Visibility.Hidden;
            }
        }

        private void OnCurrentTaskPlainedTimeChanged(object sender, ContentEventArgs e)
        {
            var newTime = (TimeSpan)e.Arg;
            var changeAvailableTime = new ArgumentContainsDelegate(changeTaskTimeAlarmTime);
            this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, changeAvailableTime, null,new ContentEventArgs(newTime));
        }

        private void changeTaskTimeAlarmTime(object sender, ContentEventArgs e)
        {
            var timer = (TaskTimeAlarm)Resources["taskTimeAlarm"];
            timer.AvailableTime = (TimeSpan)e.Arg;
        }

        private delegate void CheckMessageUserReadHandler(List<MessageView> arg);
        private void messageCollectionChanged(object sender, ContentEventArgs contentEventArgs)
        {
            var loginSuccessDelegate = new CheckMessageUserReadHandler(checkMessageReadedByUser);
            this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, loginSuccessDelegate, (List<MessageView>)contentEventArgs.Arg);
        }

        private void checkMessageReadedByUser(List<MessageView> messages)
        {
            var ti = tabControl1.SelectedItem as TabItem;
            var index = tabControl1.Items.IndexOf(ti);

            if (index == 2 && Application.Current.MainWindow.WindowState != WindowState.Minimized && Application.Current.MainWindow.IsActive)
            {
                Repository.CurrentTask.Messages.SetAllAsRead();
            }

            foreach (var item in messages)
            {
                if (!item.IsReaded)
                {
                    invokeSystemEventCome(Tools.Constants.BaloonType.Message, item.UserFrom.Title, item.Text);
                }
            }
        }

        private void UserControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            TaskTimeAlarm taskTimeAlarm = (TaskTimeAlarm)Resources["taskTimeAlarm"];
            if (DataContext != null)
            {
                Repository.CurrentTask.NewMessagesAddedHandler += messageCollectionChanged;
                Repository.CurrentTask.HasUnreadMessagesChangedHandler += hasUnreadeadChanged;
                IsEnabled = true;

                if (taskTimeAlarm == null) return;

                taskTimeAlarm.AvailableTime = ((TaskView)DataContext).PlannedTime;
                taskTimeAlarm.StartTime = ((TaskView)DataContext).CreationDate;
            }
            else
            {
                IsEnabled = false;

                if (taskTimeAlarm == null) return;
                taskTimeAlarm.AvailableTime = null;
                taskTimeAlarm.StartTime = null;
            }
        }

        private void hasUnreadeadChanged(object sender, ContentsEventArgs e)
        {
            var task = (int) e.Args[0];
            var has = (bool) e.Args[1];
            invokeHasUnreadChanged(task, has);
        }

        private void tabControl1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var ti = tabControl1.SelectedItem as TabItem;
            var index = tabControl1.Items.IndexOf(ti);
            if (index == 2)
            {
                Repository.CurrentTask.Messages.SetAllAsRead();
            }
        }

        private void SendNewMessage_MessageControl(object sender, ContentEventArgs contentEventArgs)
        {
            var task = DataContext as TaskView;
            if (task != null)
            {
                var msg = new MessageView(task)
                              {
                                  Text = (string) contentEventArgs.Arg,
                                  UserDestination = Repository.CurrentManager,
                                  SendDate = DateTime.Now,
                                  Status = ConstantsStatuses.Enabled
                              };
                
                var id = LoginManager.ServiceChannel.SendMessage(msg.BuildDTO());
                if(id ==0)
                {
                    const string message = "Не удалось отправить сообщение.";
                    MessageBox.Show(message);
                    Log.Write(LogLevel.Error, message);
                }
                else
                {
                    msg.Id = id;
                    msg.IsMy = true;
                    msg.UserFrom = Repository.LoginUser;
                    msg.IsReaded = true;

                    Repository.CurrentTask.Messages.Messages.Add(msg);
                }
            }
            else
            {
                const string message = "Невозможно отправить сообщение в данный момент.";
                MessageBox.Show(message);
                Log.Write(LogLevel.Error, message);
            }
        }

        private void NewFilesCatcher_AddNewFilePathEvent(object sender, AddNewFilePathEventArgs e)
        {
            if (Repository.CurrentTask != null)
            {
                var dialogResult =
                    MessageBox.Show("Пометить последние добавленные файлы как 'Результирующие файлы задачи'?",
                                    "Итоговые файлы", MessageBoxButton.YesNo, MessageBoxImage.Question);
                foreach (var filePath in e.FilePath)
                {
                    var fi = new FileInfo(filePath);
                    var ac = fi.GetAccessControl();

                    FileDataView exist = null;
                    if(Repository.CurrentTask.Files != null)
                    {
                        exist = Repository.CurrentTask.Files.FirstOrDefault(a => a.FilePath == filePath);
                    }
                    else
                    {
                        Repository.CurrentTask.Files = new UniqBindableCollection<FileDataView>();
                    }
                    
                    if (exist == null)
                    {
                        var file = new FileDataView(Repository.CurrentTask)
                                       {
                                           CreatedAt = DateTime.Now,
                                           FilePath = filePath,
                                           Title = fi.Name,
                                           IsFinalyFile = dialogResult == MessageBoxResult.Yes,
                                           Status = ConstantsStatuses.Enabled
                                       };

                        
                        Repository.CurrentTask.Files.Add(file);
                        LoginManager.ServiceChannel.AddFileToTask(file.BuildDTO());
                    }
                }
            }
        }

        public static void invokeSystemEventCome(Tools.Constants.BaloonType eventType, string title, string text)
        {
            var handler = NewUnreadMessageComeHandler;
            if (handler != null) handler(null, new ContentsEventArgs(new object[] { eventType, title, text }));
        }

        public void invokeHasUnreadChanged(int taskId, bool value)
        {
            var handler = HasUnreadChangedHandler;
            if (handler != null) handler(null, new ContentsEventArgs(new object[]{taskId, value}));
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            var tt = (TaskTimeAlarm)Resources["taskTimeAlarm"];
            
        }

        private void textBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (sender is TextBox)
            {
                ((TextBox)sender).SelectAll();
            }
        }

        private void titleTextBlockCopy_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(taskTitleTextBlock.Text))
                Clipboard.SetText(taskTitleTextBlock.Text);
        }

        private void categoryTextBlockCopy_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(categoryTextBlock.Text))
                Clipboard.SetText(categoryTextBlock.Text);
        }

        private void customerTextBlockCopy_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(customerTextBlock.Text))
                Clipboard.SetText(customerTextBlock.Text);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }
}