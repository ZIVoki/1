﻿using System;
using System.Windows.Controls;
using System.Windows.Data;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Client.Controls.Schedule
{
    /// <summary>
    /// Interaction logic for ScheduleControl.xaml
    /// </summary>
    public partial class ScheduleControl : UserControl
    {
        public ScheduleControl()
        {
            InitializeComponent();

//            Repository.TimeTable.CollectionChanged += TimeTable_CollectionChanged;
            WorkerCallback.TimeTableChanged += WorkerCallback_TimeTableChanged;
        }

        void WorkerCallback_TimeTableChanged(object sender, TimeTableChangedeventArgs e)
        {
            RefreshView();
        }


        private delegate void RefreshEventHandler();
        void RefreshView()
        {
            RefreshEventHandler refreshEventHandler = RefreshSafely;
            this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, refreshEventHandler);
        }

        private void RefreshSafely()
        {
            CollectionViewSource taskCollectionViewSource = (CollectionViewSource)Resources["scheduleCollectionViewSource"];
            if (taskCollectionViewSource == null) return;
            try
            {
                taskCollectionViewSource.View.Refresh();
            }
            catch (InvalidOperationException e)
            {
            }
            AutoResizeColumns();

        }

        void TimeTable_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            AutoResizeColumns();
        }

        private void CollectionViewSource_Filter(object sender, FilterEventArgs e)
        {
            TimeTableView timeTableView = (TimeTableView)e.Item;
            if (timeTableView == null) return;
            if (timeTableView.Status != ConstantsStatuses.Deleted && 
                timeTableView.User.Id == Repository.LoginUser.Id &&
                timeTableView.EndDate >= DateTime.Now) e.Accepted = true;
            else e.Accepted = false;
        }

        public void AutoResizeColumns()
        {
            try
            {
                GridView gv = userShceduleListView.View as GridView;

                if (gv != null)
                {
                    foreach (GridViewColumn gvc in gv.Columns)
                    {
                        // Set width to highest possible value
                        gvc.Width = double.MaxValue;

                        // Set to NaN to get the "real" actual width
                        gvc.Width = double.NaN;
                    }
                }
            }
            catch (InvalidOperationException e)
            { }
        }

    }
}
