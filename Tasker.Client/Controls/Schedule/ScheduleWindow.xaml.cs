﻿using Tasker.Tools.Extensions;

namespace Tasker.Client.Controls.Schedule
{
    /// <summary>
    /// Interaction logic for ScheduleWindow.xaml
    /// </summary>
    public partial class ScheduleWindow : WindowExt
    {
        public ScheduleWindow()
        {
            InitializeComponent();
        }
    }
}
