﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Linq;
using Tasker.Client.Controls.Schedule;
using Tasker.Client.Managers;
using Tasker.Client.Properties;
using Tasker.Tools.Controls;
using Tasker.Tools.CustomEventArgs;
using Tasker.Tools.Managers;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Client.Controls
{
    /// <summary>
    /// Interaction logic for WorkForm.xaml
    /// </summary>
    public partial class WorkForm : UserControl
    {
        public event LogoffEventHandler OnLogoffEventHandler;
        public static event ArgumentContainsDelegate SystemEventChangeOverlay;

        private static int m_modalControlVisibleCount;
        public static int MyPausedTaskId;

        private ScheduleWindow scheduleWindow;
        private delegate void SetValueDelegate(object[] FieldAndValue);

        public WorkForm()
        {
            InitializeComponent();
            //set on open datacontext to null
            tcTaskControl.IsEnabled = false;
            WorkerCallback.LunchFormChangeState += WorkerCallback_LunchResponse;
            WorkerCallback.CurrentTaskState += WorkerCallback_CurrentTaskState;
        }

        public void WorkForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                if (dinnerForm.Visibility == Visibility.Visible)
                {
                    dinnerForm.Visibility = Visibility.Collapsed;
                    ModalControlVisibleCount--;
                }
                if (taskFinishControl.Visibility == Visibility.Visible)
                {
                    taskFinishControl.Visibility = Visibility.Collapsed;
                    ModalControlVisibleCount--;
                }
            }
        }

        private int ModalControlVisibleCount
        {
            get
            {
                return m_modalControlVisibleCount;
            } 
            set
            {
                m_modalControlVisibleCount = value;
                mainGrid.IsEnabled = (m_modalControlVisibleCount <= 0);
            }
        }

       
        private void goDinnerButton_Click(object sender, RoutedEventArgs e)
        {
            ModalControlVisibleCount++;

            TimeTableView firstOrDefault = Repository.TimeTable.FirstOrDefault(t => t.StartDate <= DateTime.Now && t.EndDate >= DateTime.Now);
            if (firstOrDefault != null && firstOrDefault.FirstLunchStart.HasValue && firstOrDefault.SecondLunchStart.HasValue)
            {
                dinnerForm.CurrentState = Constants.DinnerFormState.LimitExceed;
                dinnerForm.Visibility = Visibility.Visible;
            }
            else
            {
                dinnerForm.CurrentState = Constants.DinnerFormState.Request;
                dinnerForm.Visibility = Visibility.Visible;
                if (Repository.CurrentManager != null)
                {
                    LoginManager.ServiceChannel.GoForLunchRequest(Repository.CurrentManager.Id);
                }
                else
                {
                    LoginManager.ServiceChannel.GoForLunchRequest(null);
                }
            }
        }

        private void sendToReviewButton_Click(object sender, RoutedEventArgs e)
        {
            if (Repository.CurrentTask == null) return;
            //waiting for manager response
            ModalControlVisibleCount++;
            taskFinishControl.Visibility = Visibility.Visible; 
            taskFinishControl.StartTimer();
            LoginManager.ServiceChannel.SendForReview(Repository.CurrentTask.BuildDTO(), Repository.CurrentManager.Id);
        }

        private void dinnerForm_OnStopDinnerRequest(object sender, RoutedEventArgs e)
        {
            ModalControlVisibleCount--;
            dinnerForm.Visibility = Visibility.Hidden;
        }

        private void TaskFinishSuccessControl_OnOkButtonClick(object sender, RoutedEventArgs e)
        {
            ModalControlVisibleCount--;
            taskFinishSuccessControl.Visibility = Visibility.Hidden;
            Repository.CurrentTask = null;
            tcTaskControl.DataContext = Repository.CurrentTask;
        }

        private void TaskUnsetControl_OnApplyButtonClick(object sender, RoutedEventArgs e)
        {
            ModalControlVisibleCount--;
            taskUnsetControl.Visibility = Visibility.Hidden;
            UnsetTask();
        }

        private void UnsetTask()
        {
            Repository.CurrentTask = null;
            tcTaskControl.DataContext = Repository.CurrentTask;
        }

        private void NewTaskControl_OnStartNewTaskButtonClick(object sender, RoutedEventArgs e)
        {
            ModalControlVisibleCount--;
            newTaskControl.Visibility = Visibility.Hidden;

            var ntc = (NewTaskControl)sender;
            var task = (TaskDTO) ntc.DataContext;
            if(Repository.LoginUser != null && task.CurrentWorker.Id == Repository.LoginUser.Id)
            {
                    Repository.CurrentTask = TaskManager.CreateTask(task, Repository.Customers,
                                                                               Repository.TaskTypes,
                                                                               null,
                                                                               Repository.Channels,
                                                                               Repository.LoginUser);
                tcTaskControl.DataContext = Repository.CurrentTask;
                LoginManager.ServiceChannel.AcceptTask(task.Id);

                //save aproximately CurrentTaskAcceptTime in settings just in case
                Settings.Default.CurrentTaskAcceptTime = DateTime.Now;

                var loadBackGroundWorker = new BackgroundWorker();
                loadBackGroundWorker.DoWork += loadFilesBackGround_DoWork;
                loadBackGroundWorker.RunWorkerCompleted += loadFilesBackGround_DoWorkComplete;
                if (!loadBackGroundWorker.IsBusy) loadBackGroundWorker.RunWorkerAsync();

                for (int i = 0; i < WorkerCallback.MessagesForNullableTask.Count; i++)
                {
                    var msg = WorkerCallback.MessagesForNullableTask[i]; 
                    
                    if (Repository.CurrentTask != null && msg.Task.Id == Repository.CurrentTask.Id)
                    {
                        MessageManager.GetMessage(msg, Repository.CurrentTask, Repository.CurrentTask.Messages.Messages, Repository.Users, Repository.LoginUser.Id);
                        WorkerCallback.MessagesForNullableTask.Remove(msg);
                        i--;
                    }
                }
            }
        }

        private void WhatchScheduleButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (scheduleWindow == null) scheduleWindow = new ScheduleWindow();
            scheduleWindow.Closed += scheduleWindow_Closed;
            scheduleWindow.Show();
            scheduleWindow.Activate();
        }

        void scheduleWindow_Closed(object sender, EventArgs e)
        {
            scheduleWindow = new ScheduleWindow();
        }

        public void RemoveTask()
        {
            tcTaskControl.DataContext = null;
        }

//        private void InvokeLogoffEventHandler()
//        {
//            tcTaskControl.DataContext = null;
//            userHeaderControl.DataContext = null;
//
//            LogoffEventHandler handler = OnLogoffEventHandler;
//            if (handler != null) handler(this);
//        }
//
//        private void UserHeaderControl_OnOnLogoffEventHandler(object sender)
//        {
//            InvokeLogoffEventHandler();
//        }

        private delegate void CurrentTaskStateDelegate(object arg);
        void WorkerCallback_CurrentTaskState(object sender, ContentsEventArgs e)
        {
            var showForm = new ArgumentContainsDelegate(showWindowInRightThread);
            this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, showForm, null,null);
            var title = string.Empty;
            //var taskDTO = (TaskDTO)e.Args[1];
            switch ((Constants.TaskFormType)e.Args[0])
            {
                case Constants.TaskFormType.NewTask:
                    var currentTaskStateDelegate = new CurrentTaskStateDelegate(NewTaskCome);
                    this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, currentTaskStateDelegate, e.Args[1]);
                    title = "Назначена задача";
                    break;
                case Constants.TaskFormType.Success:
                    var success = new CurrentTaskStateDelegate(SuccessTaskCome);
                    this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, success, e.Args[1]);
                    title = "Задача принята менеджером";
                    break;
                case Constants.TaskFormType.UnsetTask:
                    var unset = new CurrentTaskStateDelegate(UnsetTaskCome);
                    this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, unset, e.Args[1]);
                    title = "Задача снята";
                    break;
                case Constants.TaskFormType.BackgroundTaskSet:
                    var backgroundSet = new CurrentTaskStateDelegate(BackgroundTaskSetCome);
                    this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, backgroundSet, e.Args[1]);
                    return;
                case Constants.TaskFormType.FinishTaskWait:
                    var finishSet = new CurrentTaskStateDelegate(TaskFinishCome);
                    this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, finishSet, Visibility.Visible);
                    return;
                case Constants.TaskFormType.ReopenTask:
                    var finishSet2 = new CurrentTaskStateDelegate(TaskFinishCome);
                    this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, finishSet2, Visibility.Hidden);
                    title = "Задача переоткрыта менеджером";
                    break;
            }

            //invoke baloon
            if (e.Args[1] != null)
            {
                if (e.Args[1] is TaskView)
                    WorkerTaskControl.invokeSystemEventCome(Tools.Constants.BaloonType.Task, title,
                                                            ((TaskView) e.Args[1]).Title);
                else
                    WorkerTaskControl.invokeSystemEventCome(Tools.Constants.BaloonType.Task, title,
                                                            ((TaskDTO) e.Args[1]).Title);
            }

            //invoke taskbar overlay
            invokeSystemEventChangeOverlay(Tools.Constants.OverlayState.Task);
        }

        private void BackgroundTaskSetCome(object arg)
        {
            var taskDTO = (TaskDTO)arg;
            Repository.CurrentTask = TaskManager.CreateTask(taskDTO,
                                                           Repository.Customers,
                                                           Repository.TaskTypes,
                                                           null,
                                                           Repository.Channels,
                                                           Repository.LoginUser);
            Repository.CurrentManager = UserManager.GetUser(taskDTO.Creator, Repository.Users);

            tcTaskControl.DataContext = Repository.CurrentTask;

            var historyMessages = LoginManager.ServiceChannel.GetMessages(taskDTO.Id);

            foreach (var dto in historyMessages)
            {
                var message = MessageManager.GetMessage(dto, Repository.CurrentTask, Repository.CurrentTask.Messages.Messages,
                                              Repository.Users, Repository.LoginUser.Id, true);
                if (message.UserFrom.Id == Repository.LoginUser.Id)
                {
                    message.IsMy = true;
                }
            }

            var showForm = new ArgumentContainsDelegate(showWindowInRightThread);
            this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, showForm, null, null);

            var loadBackGroundWorker = new BackgroundWorker();
            loadBackGroundWorker.DoWork += loadFilesBackGround_DoWork;
            loadBackGroundWorker.RunWorkerCompleted += loadFilesBackGround_DoWorkComplete;
            if (!loadBackGroundWorker.IsBusy) loadBackGroundWorker.RunWorkerAsync();
        }

        private void loadFilesBackGround_DoWorkComplete(object sender, RunWorkerCompletedEventArgs e)
        {}

        private void loadFilesBackGround_DoWork(object sender, DoWorkEventArgs e)
        {
            LoadManager.LoadTaskFiles(Repository.CurrentTask);
        }

        private void UnsetTaskCome(object arg)
        {
//            ModalControlVisibleCount++;
//            taskUnsetControl.Visibility = Visibility.Visible;
//            taskUnsetControl.DataContext = arg;
            UnsetTask();
        }

        private void SuccessTaskCome(object arg)
        {
            ModalControlVisibleCount++;
            taskFinishSuccessControl.Visibility = Visibility.Visible;
            taskFinishSuccessControl.DataContext = (TaskView)arg;

            TaskFinishCome(Visibility.Hidden);
        }

        private void NewTaskCome(object arg)
        {
            if (newTaskControl.Visibility == Visibility.Visible) return;

            ModalControlVisibleCount++;
            newTaskControl.Visibility = Visibility.Visible;
            TaskDTO taskDTO = (TaskDTO)arg;
            newTaskControl.DataContext = taskDTO;
            Repository.CurrentManager = UserManager.GetUser(taskDTO.Creator, Repository.Users);
        }

        private void TaskFinishCome(object arg)
        {
            var visibility = (Visibility) arg;

            if(visibility == Visibility.Visible)
            {
                ModalControlVisibleCount++;
            }
            else
            {
                ModalControlVisibleCount--;
            }
            taskFinishControl.Visibility = visibility;
        }

        private void DinnerForm_Comeback(object sender, RoutedEventArgs e)
        {
            ModalControlVisibleCount--;
            dinnerForm.Visibility = Visibility.Hidden;
            LoginManager.ServiceChannel.Comeback();
        }

        private void lunchResponseCome(object arg)
        {
            dinnerForm.CurrentState = (Constants.DinnerFormState)arg;
            if (dinnerForm.Visibility != Visibility.Visible)
            {
                dinnerForm.Visibility = Visibility.Visible;
                ModalControlVisibleCount++;
            }
            /*if(dinnerForm.CurrentState == Constants.DinnerFormState.Started)
            {
                if (Repository.CurrentTask != null)
                {
                    MyPausedTaskId = Repository.CurrentTask.Id;
                    Repository.CurrentTask = null;
                }
            }*/
        }

        private void WorkerCallback_LunchResponse(object sender, ContentEventArgs contentEventArgs)
        {
            var lunch = new CurrentTaskStateDelegate(lunchResponseCome);
            this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, lunch, contentEventArgs.Arg);

            invokeSystemEventChangeOverlay(Tools.Constants.OverlayState.Lunch);
            WorkerTaskControl.invokeSystemEventCome(Tools.Constants.BaloonType.Lunch, "Результат запроса на обед", "");

            var showForm = new ArgumentContainsDelegate(showWindowInRightThread);
            this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, showForm, null,null);

        }

        private static void invokeSystemEventChangeOverlay(Tools.Constants.OverlayState e)
        {
            ArgumentContainsDelegate handler = SystemEventChangeOverlay;
            if (handler != null) handler(null, new ContentEventArgs(e));
        }

        private void tcTaskControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if(tcTaskControl.DataContext == null)
            {
                tcTaskControl.Visibility = Visibility.Hidden;
                sendToTestButton.IsEnabled = false;
            }
            else
            {
                tcTaskControl.Visibility = Visibility.Visible;
                sendToTestButton.IsEnabled = true;
            }
        }

        private void showWindowInRightThread(object sender, ContentEventArgs contentEventArgs)
        {
            Application.Current.MainWindow.Topmost = true;
            if (Application.Current.MainWindow.WindowState == WindowState.Minimized)
            {
                Application.Current.MainWindow.WindowState = WindowState.Normal;
            }
            Application.Current.MainWindow.Topmost = false;

        }

        private void DinnerForm_OnCancel(object sender, RoutedEventArgs e)
        {
            ModalControlVisibleCount--;
            dinnerForm.Visibility = Visibility.Hidden;
        }

        private void whatchTaskButton_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
