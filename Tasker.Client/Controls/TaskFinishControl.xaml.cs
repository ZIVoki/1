﻿using System;
using System.Windows.Controls;
using System.Windows.Threading;

namespace Tasker.Client.Controls
{
    /// <summary>
    /// Interaction logic for TaskFinishControl.xaml
    /// </summary>
    public partial class TaskFinishControl : UserControl
    {
        readonly DispatcherTimer timer = new DispatcherTimer();
        TimeSpan timeSpan = TimeSpan.Zero;
            
        public TaskFinishControl()
        {
            InitializeComponent();

            timer.Tick += TimerOnTick;
            timer.Interval = TimeSpan.FromSeconds(1);
        }

        public void StartTimer()
        {
            timeSpan = TimeSpan.Zero;
            timer.Start();
        }

        public void StopTimer()
        {
            timer.Stop();
        }

        private void TimerOnTick(object sender, EventArgs e)
        {
            timeSpan = timeSpan.Add(timer.Interval);
            timeCounterLabel.Content = timeSpan.ToString();
        }
    }
}
