﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using Tasker.Client.Managers;

namespace Tasker.Client.Controls
{
    /// <summary>
    /// Interaction logic for SelfStatisticControl.xaml
    /// </summary>
    public partial class SelfStatisticControl : UserControl
    {
        private const string textMask = "За {0} отработано {1}:{2} (часы:минуты)";
        private DateTime currentMonth = DateTime.Now;
        private readonly CultureInfo ruRU = new CultureInfo("ru-RU");
        private readonly DependencyProperty contentTextProperty = DependencyProperty.Register("ContentText", typeof(String), typeof(SelfStatisticControl));

        public SelfStatisticControl()
        {
            LoadManager.LoadSelfStatistic(currentMonth);
            ContentText = GenerateCurrentText();
            InitializeComponent();
        }

        private string GenerateCurrentText()
        {
            string monthText = GetMonthText(currentMonth);
            if (Repository.MonthTotalHours == null) return string.Empty;
            string hours = ((int)Math.Floor(Repository.MonthTotalHours.TotalHours)).ToString();
            string minutes = Repository.MonthTotalHours.Minutes.ToString();
            return string.Format(textMask, monthText, hours, minutes).TrimEnd();
        }

        private string GetMonthText(DateTime dateTime)
        {
            DateTime now = DateTime.Now;
            if (dateTime.Month == now.Month && dateTime.Year == now.Year)
                return "текущий месяц";
            return 
                string.Format("{0} {1} года", dateTime.ToString("MMMM", ruRU).ToLower(), dateTime.ToString("yyyy"));
        }
        
        public string ContentText
        {
            get { return (string) GetValue(contentTextProperty); } 
            private set
            {
                SetValue(contentTextProperty, value);
            }
        }

        private void whatchPrevMonthButton_Click(object sender, RoutedEventArgs e)
        {
            currentMonth = currentMonth.AddMonths(-1);
            UpdateMounthStatistic();
        }
        
        private void whatchNextMonthButton_Click(object sender, RoutedEventArgs e)
        {
            currentMonth = currentMonth.AddMonths(1);
            UpdateMounthStatistic();
        }

        private void UpdateMounthStatistic()
        {
            LoadManager.LoadSelfStatistic(currentMonth);
            ContentText = GenerateCurrentText();
        }
    }
}
