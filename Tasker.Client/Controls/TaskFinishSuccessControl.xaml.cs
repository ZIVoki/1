﻿using System.Windows;
using System.Windows.Controls;

namespace Tasker.Client.Controls
{
    /// <summary>
    /// Interaction logic for TaskFinishSuccessControl.xaml
    /// </summary>
    public partial class TaskFinishSuccessControl : UserControl
    {
        public TaskFinishSuccessControl()
        {
            InitializeComponent();
        }

        public event RoutedEventHandler OkButtonClick;

        public void InvokeOkButtonClick(RoutedEventArgs e)
        {
            RoutedEventHandler handler = OkButtonClick;
            if (handler != null) handler(this, e);
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            InvokeOkButtonClick(e);
        }
    }
}
