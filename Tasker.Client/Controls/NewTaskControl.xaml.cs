﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Client.Controls
{
    /// <summary>
    /// Interaction logic for NewTaskControl.xaml
    /// </summary>
    public partial class NewTaskControl : UserControl
    {
        public NewTaskControl()
        {
            InitializeComponent();
            /*this.DataContext = new TaskDTO()
                                   {
                                       Title = "Почему вырождена антропосоциология?",
                                       Status = ConstantsStatuses.Enabled,
                                       TaskType = new TaskTypeDTO() { Title = "Онтологический гравитационный парадокс: основные моменты",CompletionTime = new TimeSpan(0,10,20)},
                                       CurrentWorker = new UserDTO(){Id=3, FirstName = "dasdasdasd", LastName = "juykukl", RoleId = ConstantsRoles.Worker, Status = ConstantsStatuses.Enabled},
                                       Creator = new UserDTO(){Id=2, FirstName = "dasd", LastName = "dasd", RoleId = ConstantsRoles.Manager, Status = ConstantsStatuses.Enabled},
                                       Description =
                                           "Сомнение осмысляет онтологический здравый смысл, при этом буквы А, В, I, О символизируют соответственно общеутвердительное, общеотрицательное, частноутвердительное и частноотрицательное суждения. Суждение, по определению, трансформирует бабувизм, tertium nоn datur. Можно предположить, что исчисление предикатов методологически принимает во внимание из ряда вон выходящий мир, отрицая очевидное. Сомнение, как принято считать, подрывает сенсибельный структурализм, учитывая опасность, которую представляли собой писания Дюринга для не окрепшего еще немецкого рабочего движения. Атомистика, следовательно, подрывает здравый смысл, изменяя привычную реальность.Согласно предыдущему, реальность трансформирует трагический интеллект, учитывая опасность, которую представляли собой писания Дюринга для не окрепшего еще немецкого рабочего движения. Гравитационный парадокс, по определению, реально оспособляет закон внешнего мира, открывая новые горизонты. Представляется логичным, что акциденция рассматривается сложный гений, хотя в официозе принято обратное. Врожденная интуиция, по определению, представляет собой субъективный предмет деятельности, при этом буквы А, В, I, О символизируют соответственно общеутвердительное, общеотрицательное, частноутвердительное и частноотрицательное суждения. Герменевтика, конечно, вырождена. Интеллект, по определению, абстрактен.",
                                           CreationDate = DateTime.Now,
                                           Customer = new CustomerDTO(){Title = "канал ТВ3"},
                                           DeadLine = new DateTime(2011,1,1)
                                   };*/
        }

        public event RoutedEventHandler StartNewTaskButtonClick;

        public void InvokeStartNewTaskButtonClick(RoutedEventArgs e)
        {
            RoutedEventHandler handler = StartNewTaskButtonClick;
            if (handler != null) handler(this, e);
        }

        private void startNewTaskButton_Click(object sender, RoutedEventArgs e)
        {
            InvokeStartNewTaskButtonClick(e);
        }
    }
}
