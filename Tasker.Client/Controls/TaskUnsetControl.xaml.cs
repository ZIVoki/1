using System.Windows;
using System.Windows.Controls;

namespace Tasker.Client.Controls
{
    /// <summary>
    /// Interaction logic for NewTaskControl.xaml
    /// </summary>
    public partial class TaskUnsetControl : UserControl
    {
        public TaskUnsetControl()
        {
            InitializeComponent();
        }

        public event RoutedEventHandler ApplyTaskUnsetButtonClick;

        public void InvokeApplyButtonClick(RoutedEventArgs e)
        {
            RoutedEventHandler handler = ApplyTaskUnsetButtonClick;
            if (handler != null) handler(this, e);
        }

        private void applyButton_Click(object sender, RoutedEventArgs e)
        {
            InvokeApplyButtonClick(e);
        }
    }
}