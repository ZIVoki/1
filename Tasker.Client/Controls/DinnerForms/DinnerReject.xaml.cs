﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Tasker.Client.Controls.DinnerForms
{
    /// <summary>
    /// Interaction logic for DinnerReject.xaml
    /// </summary>
    public partial class DinnerReject : UserControl
    {
        public DinnerReject()
        {
            InitializeComponent();
        }

        public event RoutedEventHandler ComebackButtonClick;

        public void InvokeStartNewTaskButtonClick(RoutedEventArgs e)
        {
            RoutedEventHandler handler = ComebackButtonClick;
            if (handler != null) handler(this, e);
        }

        private void comebackButton_Click(object sender, RoutedEventArgs e)
        {
            InvokeStartNewTaskButtonClick(e);
        }
    }
}
