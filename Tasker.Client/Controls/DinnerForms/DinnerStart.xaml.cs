﻿using System;
using System.Windows;
using System.Windows.Controls;
using Tasker.Tools.Helpers;

namespace Tasker.Client.Controls.DinnerForms
{
    /// <summary>
    /// Interaction logic for DinnerStart.xaml
    /// </summary>
    public partial class DinnerStart : UserControl
    {
        public DinnerStart()
        {
            InitializeComponent();
        }

        public event RoutedEventHandler ComebackButtonClick;

        public void InvokeComebackButtonClick(RoutedEventArgs e)
        {
            RoutedEventHandler handler = ComebackButtonClick;
            if (handler != null) handler(this, e);
        }

        private void comebackButton_Click(object sender, RoutedEventArgs e)
        {
            InvokeComebackButtonClick(e);
        }

        private void UserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (Visibility == Visibility.Visible)
            {
                DinnerTimeAlarm dinnerTimeAlarm = (DinnerTimeAlarm)Resources["dinnerTimeAlarm"];
                if (dinnerTimeAlarm == null) return;
                dinnerTimeAlarm.StartTime = DateTime.Now;

                dinnerTimeAlarm.DinnerTime = new TimeSpan(0, 0, 30, 0);
            }
        }
    }
}
