﻿using System;
using System.Windows;
using System.Windows.Controls;
using Tasker.Tools.ServiceReference;

namespace Tasker.Client.Controls.DinnerForms
{
    /// <summary>
    /// Interaction logic for DinnerForm.xaml
    /// </summary>
    public partial class DinnerForm : UserControl
    {
        private Constants.DinnerFormState currentState;
        
        public event RoutedEventHandler StopDinnerRequest;
        public event RoutedEventHandler Comeback;
        public event RoutedEventHandler Cancel;



        public Constants.DinnerFormState CurrentState
        {
            get { return currentState; }
            set
            {
                UpdateControls(value);
                currentState = value;
            }
        }

        public DinnerForm()
        {
            InitializeComponent();
            CurrentState = Constants.DinnerFormState.None;
        }

        private void CollapceAllMessagewindows()
        {
            dinnerStart.Visibility = Visibility.Hidden;
            dinnerRequest.Visibility = Visibility.Hidden;
            dinnerReject.Visibility = Visibility.Hidden;
            dinnerRequest.Visibility = Visibility.Hidden;
            dinnerLimitExceed.Visibility = Visibility.Hidden;
        }

        private void UpdateControls(Constants.DinnerFormState value)
        {
            CollapceAllMessagewindows();
            switch (value)
            {
                case Constants.DinnerFormState.None:
                    break;
                case Constants.DinnerFormState.Request:
                    dinnerRequest.Visibility = Visibility.Visible;
                    break;
                case Constants.DinnerFormState.Started:
                    dinnerStart.Visibility = Visibility.Visible;
                    break;
                case Constants.DinnerFormState.Reject:
                    dinnerReject.Visibility = Visibility.Visible;
                    break;
                case Constants.DinnerFormState.LimitExceed:
                    dinnerLimitExceed.Visibility = Visibility.Visible;
                    break;
//                case Constants.DinnerFormState.Late:
//                    dinnerLate.Visibility = Visibility.Visible;
//                    break;
                default:
                    throw new ArgumentOutOfRangeException("value");
            }
        }

        public void InvokeStopDinnerRequest(RoutedEventArgs e)
        {
            RoutedEventHandler handler = StopDinnerRequest;
            if (handler != null) handler(this, e);
        }

        public void InvokeComeback(RoutedEventArgs e)
        {
            RoutedEventHandler handler = Comeback;
            if (handler != null) handler(this, e);
        }

        private void DinnerRequest_OnStopReqest(object sender, RoutedEventArgs e)
        {
            WorkerCallback.invokeLunchFormChangeState(Constants.DinnerFormState.Reject);

            InvokeStopDinnerRequest(e);
            CurrentState = Constants.DinnerFormState.None;
        }

        private void DinnerRequest_OnComeback(object sender, RoutedEventArgs e)
        {
            InvokeComeback(e);
            CurrentState = Constants.DinnerFormState.None;
            Repository.LoginUser.CurrentState = ConstantsNetworkStates.Online;
        }

        public void InvokeCancel(RoutedEventArgs e)
        {
            RoutedEventHandler handler = Cancel;
            if (handler != null) handler(this, e);
        }

        private void DinnerLimitExceed_OnComebackButtonClick(object sender, RoutedEventArgs e)
        {
            InvokeCancel(e);
            CurrentState = Constants.DinnerFormState.None;
        }
    }
}
