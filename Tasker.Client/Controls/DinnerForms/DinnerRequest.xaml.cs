﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Tasker.Client.Controls.DinnerForms
{
    /// <summary>
    /// Interaction logic for DinnerRequest.xaml
    /// </summary>
    public partial class DinnerRequest : UserControl
    {
        public DinnerRequest()
        {
            InitializeComponent();
        }

        public event RoutedEventHandler StopReqest;

        public void InvokeStopReqest(RoutedEventArgs e)
        {
            RoutedEventHandler handler = StopReqest;
            if (handler != null) handler(this, e);
        }

        private void cancelDinnerButton_Click(object sender, RoutedEventArgs e)
        {
            InvokeStopReqest(e);
        }
    }
}
