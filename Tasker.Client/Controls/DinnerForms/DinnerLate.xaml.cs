﻿using System.Windows;
using System.Windows.Controls;

namespace Tasker.Client.Controls.DinnerForms
{
    /// <summary>
    /// Interaction logic for DinnerLate.xaml
    /// </summary>
    public partial class DinnerLate : UserControl
    {
        public DinnerLate()
        {
            InitializeComponent();
        }

        public event RoutedEventHandler ComebackButtonClick;

        public void InvokeComebackButtonClick(RoutedEventArgs e)
        {
            RoutedEventHandler handler = ComebackButtonClick;
            if (handler != null) handler(this, e);
        }

        private void comebackButton_Click(object sender, RoutedEventArgs e)
        {
            InvokeComebackButtonClick(e);
        }
    }
}
