﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.269
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tasker.Client.ServiceReference {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ServiceReference.IWorkerInterface", CallbackContract=typeof(Tasker.Client.ServiceReference.IWorkerInterfaceCallback), SessionMode=System.ServiceModel.SessionMode.Required)]
    public interface IWorkerInterface {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IClientInterface/Ping", ReplyAction="http://tempuri.org/IClientInterface/PingResponse")]
        System.Nullable<System.TimeSpan> Ping(System.DateTime time);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IClientInterface/Login", ReplyAction="http://tempuri.org/IClientInterface/LoginResponse")]
        Tasker.Tools.ServiceReference.LoginResultDTO Login([System.ServiceModel.MessageParameterAttribute(Name="login")] string login1, string password);
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, IsTerminating=true, IsInitiating=false, Action="http://tempuri.org/IClientInterface/Logoff")]
        void Logoff();
        
        [System.ServiceModel.OperationContractAttribute(IsInitiating=false, Action="http://tempuri.org/IClientInterface/GetTaskType", ReplyAction="http://tempuri.org/IClientInterface/GetTaskTypeResponse")]
        Tasker.Tools.ServiceReference.TaskTypeDTO GetTaskType(int taskTypeId);
        
        [System.ServiceModel.OperationContractAttribute(IsInitiating=false, Action="http://tempuri.org/IClientInterface/GetTaskTypes", ReplyAction="http://tempuri.org/IClientInterface/GetTaskTypesResponse")]
        Tasker.Tools.ServiceReference.TaskTypeDTO[] GetTaskTypes();
        
        [System.ServiceModel.OperationContractAttribute(IsInitiating=false, Action="http://tempuri.org/IClientInterface/GetUserTaskTypes", ReplyAction="http://tempuri.org/IClientInterface/GetUserTaskTypesResponse")]
        Tasker.Tools.ServiceReference.UserTaskTypeDTO[] GetUserTaskTypes(int taskType);
        
        [System.ServiceModel.OperationContractAttribute(IsInitiating=false, Action="http://tempuri.org/IClientInterface/GetUserTaskType", ReplyAction="http://tempuri.org/IClientInterface/GetUserTaskTypeResponse")]
        Tasker.Tools.ServiceReference.UserTaskTypeDTO GetUserTaskType(int userTaskTypeId);
        
        [System.ServiceModel.OperationContractAttribute(IsInitiating=false, Action="http://tempuri.org/IClientInterface/GetUnreadMessages", ReplyAction="http://tempuri.org/IClientInterface/GetUnreadMessagesResponse")]
        Tasker.Tools.ServiceReference.MessageDTO[] GetUnreadMessages();
        
        [System.ServiceModel.OperationContractAttribute(IsInitiating=false, Action="http://tempuri.org/IClientInterface/SendMessage", ReplyAction="http://tempuri.org/IClientInterface/SendMessageResponse")]
        int SendMessage(Tasker.Tools.ServiceReference.MessageDTO newMessage);
        
        [System.ServiceModel.OperationContractAttribute(IsInitiating=false, Action="http://tempuri.org/IClientInterface/GetMessages", ReplyAction="http://tempuri.org/IClientInterface/GetMessagesResponse")]
        Tasker.Tools.ServiceReference.MessageDTO[] GetMessages(int taskId);
        
        [System.ServiceModel.OperationContractAttribute(IsInitiating=false, Action="http://tempuri.org/IClientInterface/AddFileToTask", ReplyAction="http://tempuri.org/IClientInterface/AddFileToTaskResponse")]
        int AddFileToTask(Tasker.Tools.ServiceReference.FileDTO file);
        
        [System.ServiceModel.OperationContractAttribute(IsInitiating=false, Action="http://tempuri.org/IClientInterface/GetFileData", ReplyAction="http://tempuri.org/IClientInterface/GetFileDataResponse")]
        Tasker.Tools.ServiceReference.FileDTO GetFileData(int fileId);
        
        [System.ServiceModel.OperationContractAttribute(IsInitiating=false, Action="http://tempuri.org/IClientInterface/GetFilesData", ReplyAction="http://tempuri.org/IClientInterface/GetFilesDataResponse")]
        Tasker.Tools.ServiceReference.FileDTO[] GetFilesData(int taskId);
        
        [System.ServiceModel.OperationContractAttribute(IsInitiating=false, Action="http://tempuri.org/IClientInterface/GetShifts", ReplyAction="http://tempuri.org/IClientInterface/GetShiftsResponse")]
        Tasker.Tools.ServiceReference.ShiftDTO[] GetShifts();
        
        [System.ServiceModel.OperationContractAttribute(IsInitiating=false, Action="http://tempuri.org/IClientInterface/GetShift", ReplyAction="http://tempuri.org/IClientInterface/GetShiftResponse")]
        Tasker.Tools.ServiceReference.ShiftDTO GetShift(int shiftId);
        
        [System.ServiceModel.OperationContractAttribute(IsInitiating=false, Action="http://tempuri.org/IClientInterface/GetMyTimeTables", ReplyAction="http://tempuri.org/IClientInterface/GetMyTimeTablesResponse")]
        Tasker.Tools.ServiceReference.TimeTableDTO[] GetMyTimeTables(System.DateTime startDate, System.DateTime endDate);
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, IsInitiating=false, Action="http://tempuri.org/IWorkerInterface/GoForLunchRequest")]
        void GoForLunchRequest(System.Nullable<int> managerId);
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, IsInitiating=false, Action="http://tempuri.org/IWorkerInterface/Comeback")]
        void Comeback();
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, IsInitiating=false, Action="http://tempuri.org/IWorkerInterface/AcceptTask")]
        void AcceptTask(int taskId);
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, IsInitiating=false, Action="http://tempuri.org/IWorkerInterface/SendForReview")]
        void SendForReview(Tasker.Tools.ServiceReference.TaskDTO taskDTO, int managerId);
        
        [System.ServiceModel.OperationContractAttribute(IsInitiating=false, Action="http://tempuri.org/IWorkerInterface/GetMyCurrentTask", ReplyAction="http://tempuri.org/IWorkerInterface/GetMyCurrentTaskResponse")]
        Tasker.Tools.ServiceReference.TaskDTO GetMyCurrentTask();
        
        [System.ServiceModel.OperationContractAttribute(IsInitiating=false, Action="http://tempuri.org/IWorkerInterface/GetMyTasks", ReplyAction="http://tempuri.org/IWorkerInterface/GetMyTasksResponse")]
        Tasker.Tools.ServiceReference.TaskDTO[] GetMyTasks();
        
        [System.ServiceModel.OperationContractAttribute(IsInitiating=false, Action="http://tempuri.org/IWorkerInterface/GetTaskTotalHoursByDates", ReplyAction="http://tempuri.org/IWorkerInterface/GetTaskTotalHoursByDatesResponse")]
        System.TimeSpan GetTaskTotalHoursByDates(System.DateTime startDate, System.DateTime endDate);
        
        [System.ServiceModel.OperationContractAttribute(IsInitiating=false, Action="http://tempuri.org/IWorkerInterface/GetTasksByDate", ReplyAction="http://tempuri.org/IWorkerInterface/GetTasksByDateResponse")]
        Tasker.Tools.ServiceReference.TaskDTO[] GetTasksByDate(System.DateTime date);
        
        [System.ServiceModel.OperationContractAttribute(IsInitiating=false, Action="http://tempuri.org/IWorkerInterface/GetManager", ReplyAction="http://tempuri.org/IWorkerInterface/GetManagerResponse")]
        Tasker.Tools.ServiceReference.UserDTO GetManager(int userId);
        
        [System.ServiceModel.OperationContractAttribute(IsInitiating=false, Action="http://tempuri.org/IWorkerInterface/GetMyUserTaskTypes", ReplyAction="http://tempuri.org/IWorkerInterface/GetMyUserTaskTypesResponse")]
        Tasker.Tools.ServiceReference.UserTaskTypeDTO[] GetMyUserTaskTypes(int userId);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IWorkerInterfaceCallback {
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/IClientInterface/ReceiveMessage")]
        void ReceiveMessage(Tasker.Tools.ServiceReference.MessageDTO msg);
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/IClientInterface/UserEnter")]
        void UserEnter(int userId);
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/IClientInterface/UserLogoff")]
        void UserLogoff(int userId);
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/IClientInterface/TaskUpdated")]
        void TaskUpdated(Tasker.Tools.ServiceReference.TaskDTO task);
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/IClientInterface/FileAdded")]
        void FileAdded(int taskId, Tasker.Tools.ServiceReference.FileDTO attachFile);
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/IClientInterface/TaskTypeAdded")]
        void TaskTypeAdded(Tasker.Tools.ServiceReference.TaskTypeDTO taskType);
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/IClientInterface/TaskTypeUpdated")]
        void TaskTypeUpdated(Tasker.Tools.ServiceReference.TaskTypeDTO taskType);
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/IClientInterface/UserTaskTypeAdded")]
        void UserTaskTypeAdded(Tasker.Tools.ServiceReference.UserTaskTypeDTO userTaskType);
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/IClientInterface/UserTaskTypeUpdated")]
        void UserTaskTypeUpdated(Tasker.Tools.ServiceReference.UserTaskTypeDTO userTaskType);
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/IClientInterface/FaultExeptionServerAlarm")]
        void FaultExeptionServerAlarm(Tasker.Tools.ServiceReference.FaultDetail createFaultDetail);
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/IClientInterface/UserStateChanged")]
        void UserStateChanged(int userId, Tasker.Tools.ServiceReference.ConstantsNetworkStates stateId);
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/IClientInterface/TimeTableUpdated")]
        void TimeTableUpdated(Tasker.Tools.ServiceReference.TimeTableDTO timeTable);
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/IClientInterface/ShiftUpdated")]
        void ShiftUpdated(Tasker.Tools.ServiceReference.ShiftDTO shift);
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/IClientInterface/CustomerUpdated")]
        void CustomerUpdated(Tasker.Tools.ServiceReference.CustomerDTO customer);
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/IClientInterface/TimeTableAdded")]
        void TimeTableAdded(Tasker.Tools.ServiceReference.TimeTableDTO timeTable);
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/IClientInterface/UserUpdated")]
        void UserUpdated(Tasker.Tools.ServiceReference.UserDTO user);
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/IWorkerInterface/TaskSet")]
        void TaskSet(Tasker.Tools.ServiceReference.TaskDTO task);
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/IWorkerInterface/TaskUnsetSet")]
        void TaskUnsetSet(int taskId);
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/IWorkerInterface/NewTask")]
        void NewTask(Tasker.Tools.ServiceReference.TaskDTO task);
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/IWorkerInterface/SetStatusLunchTime")]
        void SetStatusLunchTime(bool isLunch);
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/IWorkerInterface/SetTaskReviewResult")]
        void SetTaskReviewResult(bool isSuccess);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IWorkerInterfaceChannel : Tasker.Client.ServiceReference.IWorkerInterface, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class WorkerInterfaceClient : System.ServiceModel.DuplexClientBase<Tasker.Client.ServiceReference.IWorkerInterface>, Tasker.Client.ServiceReference.IWorkerInterface {
        
        public WorkerInterfaceClient(System.ServiceModel.InstanceContext callbackInstance) : 
                base(callbackInstance) {
        }
        
        public WorkerInterfaceClient(System.ServiceModel.InstanceContext callbackInstance, string endpointConfigurationName) : 
                base(callbackInstance, endpointConfigurationName) {
        }
        
        public WorkerInterfaceClient(System.ServiceModel.InstanceContext callbackInstance, string endpointConfigurationName, string remoteAddress) : 
                base(callbackInstance, endpointConfigurationName, remoteAddress) {
        }
        
        public WorkerInterfaceClient(System.ServiceModel.InstanceContext callbackInstance, string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(callbackInstance, endpointConfigurationName, remoteAddress) {
        }
        
        public WorkerInterfaceClient(System.ServiceModel.InstanceContext callbackInstance, System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(callbackInstance, binding, remoteAddress) {
        }
        
        public System.Nullable<System.TimeSpan> Ping(System.DateTime time) {
            return base.Channel.Ping(time);
        }
        
        public Tasker.Tools.ServiceReference.LoginResultDTO Login(string login1, string password) {
            return base.Channel.Login(login1, password);
        }
        
        public void Logoff() {
            base.Channel.Logoff();
        }
        
        public Tasker.Tools.ServiceReference.TaskTypeDTO GetTaskType(int taskTypeId) {
            return base.Channel.GetTaskType(taskTypeId);
        }
        
        public Tasker.Tools.ServiceReference.TaskTypeDTO[] GetTaskTypes() {
            return base.Channel.GetTaskTypes();
        }
        
        public Tasker.Tools.ServiceReference.UserTaskTypeDTO[] GetUserTaskTypes(int taskType) {
            return base.Channel.GetUserTaskTypes(taskType);
        }
        
        public Tasker.Tools.ServiceReference.UserTaskTypeDTO GetUserTaskType(int userTaskTypeId) {
            return base.Channel.GetUserTaskType(userTaskTypeId);
        }
        
        public Tasker.Tools.ServiceReference.MessageDTO[] GetUnreadMessages() {
            return base.Channel.GetUnreadMessages();
        }
        
        public int SendMessage(Tasker.Tools.ServiceReference.MessageDTO newMessage) {
            return base.Channel.SendMessage(newMessage);
        }
        
        public Tasker.Tools.ServiceReference.MessageDTO[] GetMessages(int taskId) {
            return base.Channel.GetMessages(taskId);
        }
        
        public int AddFileToTask(Tasker.Tools.ServiceReference.FileDTO file) {
            return base.Channel.AddFileToTask(file);
        }
        
        public Tasker.Tools.ServiceReference.FileDTO GetFileData(int fileId) {
            return base.Channel.GetFileData(fileId);
        }
        
        public Tasker.Tools.ServiceReference.FileDTO[] GetFilesData(int taskId) {
            return base.Channel.GetFilesData(taskId);
        }
        
        public Tasker.Tools.ServiceReference.ShiftDTO[] GetShifts() {
            return base.Channel.GetShifts();
        }
        
        public Tasker.Tools.ServiceReference.ShiftDTO GetShift(int shiftId) {
            return base.Channel.GetShift(shiftId);
        }
        
        public Tasker.Tools.ServiceReference.TimeTableDTO[] GetMyTimeTables(System.DateTime startDate, System.DateTime endDate) {
            return base.Channel.GetMyTimeTables(startDate, endDate);
        }
        
        public void GoForLunchRequest(System.Nullable<int> managerId) {
            base.Channel.GoForLunchRequest(managerId);
        }
        
        public void Comeback() {
            base.Channel.Comeback();
        }
        
        public void AcceptTask(int taskId) {
            base.Channel.AcceptTask(taskId);
        }
        
        public void SendForReview(Tasker.Tools.ServiceReference.TaskDTO taskDTO, int managerId) {
            base.Channel.SendForReview(taskDTO, managerId);
        }
        
        public Tasker.Tools.ServiceReference.TaskDTO GetMyCurrentTask() {
            return base.Channel.GetMyCurrentTask();
        }
        
        public Tasker.Tools.ServiceReference.TaskDTO[] GetMyTasks() {
            return base.Channel.GetMyTasks();
        }
        
        public System.TimeSpan GetTaskTotalHoursByDates(System.DateTime startDate, System.DateTime endDate) {
            return base.Channel.GetTaskTotalHoursByDates(startDate, endDate);
        }
        
        public Tasker.Tools.ServiceReference.TaskDTO[] GetTasksByDate(System.DateTime date) {
            return base.Channel.GetTasksByDate(date);
        }
        
        public Tasker.Tools.ServiceReference.UserDTO GetManager(int userId) {
            return base.Channel.GetManager(userId);
        }
        
        public Tasker.Tools.ServiceReference.UserTaskTypeDTO[] GetMyUserTaskTypes(int userId) {
            return base.Channel.GetMyUserTaskTypes(userId);
        }
    }
}
