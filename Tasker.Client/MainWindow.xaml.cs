﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Timers;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shell;
using System.Windows.Threading;
using Tasker.Client.Controls;
using Tasker.Client.Managers;
using Tasker.Client.Properties;
using Tasker.Tools.Controls.TrayControls;
using Tasker.Tools.CustomEventArgs;
using Tasker.Tools.Helpers;
using Tasker.Tools.ServiceReference;

namespace Tasker.Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly List<int> tasksWithUnreadMessages = new List<int>();
        private DateTime _lastLoginDateTime = new DateTime();
        private Timer TaskbarAnimationTimer;
        private System.Windows.Forms.Timer CheckIdleTimer;

        public MainWindow()
        {
            #if DEBUG
                Settings.Default.AutoLogOff = true;
                Settings.Default.LogOffTime = 1;
            #else
                Settings.Default.AutoLogOff = true;
                Settings.Default.LogOffTime = 15;
            #endif
            Settings.Default.Save();

            this.Title = string.Format("Tasker client (ver. {0})", CurrentVersion);
            this.Loaded += MainWindow_Loaded;

            ShowLoginForm();
            
            KeyUp += MainWindow_KeyUp;
            LoginManager.LostServerConnectionEventHandler += LoginManager_LostServerConnectionEventHandler;
            WorkerTaskControl.NewUnreadMessageComeHandler += ShowCustomBalloon;
            WorkForm.SystemEventChangeOverlay += changeOverlayByEventHandler;
            frmWorkForm.tcTaskControl.HasUnreadChangedHandler += hasUnreadChanged;
        }

        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            InitializeAutoLogoffFeature();
            Activate();
        }

        private void InitializeAutoLogoffFeature()
        {
            if (!IsInitialized) return;
            if (Settings.Default.AutoLogOff)
            {
                if (CheckIdleTimer == null)
                {
                    CheckIdleTimer = new System.Windows.Forms.Timer();
                    this.CheckIdleTimer.Interval = (int)TimeSpan.FromSeconds(1).TotalMilliseconds;
                    this.CheckIdleTimer.Tick += this.CheckIdleTimer_Tick;
                }
                CheckIdleTimer.Start();
            }
        }

        private void CheckIdleTimer_Tick(object sender, EventArgs e)
        {
            uint idleTime = Win32.GetIdleTime();
            if (idleTime > TimeSpan.FromMinutes(Settings.Default.LogOffTime).TotalMilliseconds)
            {
                AutoLogOffHelper_MakeAutoLogOffEvent();
            }
        }

        void AutoLogOffHelper_MakeAutoLogOffEvent()
        {
            Debug.WriteLine("Входим в Log off, проверка");
            if (Repository.LoginUser != null && Repository.LoginUser.Id != 0)
            {
                // выходим из метода если человек на обеде
                if (Repository.LoginUser.CurrentState == ConstantsNetworkStates.Lunch) return;
                Debug.WriteLine("Вызываем Log off");
                LogOff();
            }
        }

        void LoginManager_LostServerConnectionEventHandler(object sender)
        {
            OtherThreadLogOffHandler();
        }

        public static string CurrentVersion
        {
            get
            {
                System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
                Version version = assembly.GetName().Version;
                return version.ToString();
            }
        }

        private void changeOverlayByEventHandler(object sender, ContentEventArgs e)
        {
            ChangeTaskbarItemInfoOverlayHandler((Tools.Constants.OverlayState)e.Arg);
        }

        private void hasUnreadChanged(object sender, ContentsEventArgs e)
        {
            var taskId = (int)e.Args[0];
            var hasUnread = (bool)e.Args[1];

            if(hasUnread)
            {
                if(!tasksWithUnreadMessages.Contains(taskId))
                {
                    tasksWithUnreadMessages.Add(taskId);
                    ChangeTaskbarItemInfoOverlayHandler(Tools.Constants.OverlayState.Message);
                }
            }
            else
            {
                var exist = tasksWithUnreadMessages.FirstOrDefault(a => a == taskId);
                tasksWithUnreadMessages.Remove(exist);
                if(tasksWithUnreadMessages.Count == 0)
                {
                    ChangeTaskbarItemInfoOverlayHandler(Tools.Constants.OverlayState.Nothing);
                }
            }
            
        }

        void MainWindow_KeyUp(object sender, KeyEventArgs e)
        {
            frmWorkForm.WorkForm_KeyUp(sender, e);
        }

        public void ShowLoginForm()
        {
            var showForm = new ArgumentContainsDelegate(SafelyShowLoginForm);
            this.Dispatcher.Invoke(DispatcherPriority.Normal, showForm, null, null);
        }

        private void SafelyShowLoginForm(object sender, ContentEventArgs e)
        {
            this.IsEnabled = false;
            if (Repository.LoginUser == null ||
                Repository.LoginUser.Role != ConstantsRoles.Worker)
            {
                var loginWindow = new ClientLoginWindow();
                bool? dilogResult = loginWindow.ShowDialog();
                if (dilogResult == false)
                {
                    Environment.Exit(0);
                }
                else
                {
                    _lastLoginDateTime = DateTime.Now;
                    if (!IsInitialized)
                        InitializeComponent();
                    else
                    {
                        this.Activate();
                        userHeaderControl.DataContext = Repository.LoginUser;

                    }

                    LoadData();
                }
            }
        }

        private void LoadData()
        {
            var loadBackGroundWorker = new BackgroundWorker();
            loadBackGroundWorker.DoWork += loginBackgroundWorker_DoWork;
            loadBackGroundWorker.RunWorkerCompleted += loginBackgroundWorker_RunWorkerCompleted;
            this.IsEnabled = false;
            
            if (!loadBackGroundWorker.IsBusy) loadBackGroundWorker.RunWorkerAsync();
        }

        void loginBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            busyCircularProgressBar.Visibility = Visibility.Hidden;
            this.IsEnabled = true;
        }

        static void loginBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            LoadManager.LoadData();
        }

        private void LogoffeventHandler(object sender)
        {
            LogOff();
        }

        private void LogOff()
        {
            if (Repository.LoginUser == null) return;
            frmWorkForm.RemoveTask();
            LoginManager.Logoff();
            ShowLoginForm();
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            e.Cancel = true;
            this.WindowState = WindowState.Minimized; 
        }

        public static void ShowServerError(string meassge)
        {
            MessageBox.Show(meassge, "Ошибка на стороне сервера.", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            LoginManager.Logoff();
            tiTaskIcon.Dispose();
            this.Close();
            Application.Current.Shutdown();
        }

        /// <summary>
        /// contentsEventArgs = object[]{Constants.BaloonType, title, text}
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="contentsEventArgs"></param>
        private void ShowCustomBalloon(object sender, ContentsEventArgs contentsEventArgs)
        {
            BalloonCreateHandler((string)contentsEventArgs.Args[2], (string)contentsEventArgs.Args[1], (Tools.Constants.BaloonType)contentsEventArgs.Args[0]);
            var showForm = new ArgumentContainsDelegate(showWindowInRightThread);
            this.Dispatcher.Invoke(DispatcherPriority.Normal, showForm, null, null);
        }

        private void createBalloonInRightThread(string text, string title, Tools.Constants.BaloonType type)
        {
            var balloon = new FancyBalloon(type) {Tag = Tag};
            balloon.OpenAndShowWindowHandler += showWindow;
            balloon.BalloonTitle = title;

            if (text.Length > Tools.Constants.MessagePreviewSignCount)
            {
                balloon.BalloonText = text.Substring(0, Tools.Constants.MessagePreviewSignCount) + "...";
            }
            else
            {
                balloon.BalloonText = text;
            }

            //show balloon and close it after 4 seconds
            tiTaskIcon.ShowCustomBalloon(balloon, PopupAnimation.Slide, 4000);
        }

        
        private void showWindow(object sender, ContentsEventArgs e)
        {
            var balloonClosedType = (Tools.Constants.BaloonType) e.Args[0];

            Application.Current.MainWindow.WindowState =WindowState.Normal;
            Application.Current.MainWindow.Visibility = Visibility.Visible;
            Application.Current.MainWindow.Activate();

            switch (balloonClosedType)
            {
                case Tools.Constants.BaloonType.Lunch:
                case Tools.Constants.BaloonType.Nothing:
                    ChangeTaskbarItemInfoOverlayHandler(Tools.Constants.OverlayState.Nothing);
                    break;
                case Tools.Constants.BaloonType.Message:
                    //2 is number of message tab
                    frmWorkForm.tcTaskControl.tabControl1.SelectedIndex = 2;
                    break;
                case Tools.Constants.BaloonType.Task:
                    //1 is number of message tab
                    frmWorkForm.tcTaskControl.tabControl1.SelectedIndex = 0;
                    ChangeTaskbarItemInfoOverlayHandler(Tools.Constants.OverlayState.Nothing);
                    break;
            }
        }

        private delegate void BalloonCreateHandlerDelegate(string text, string title, Tools.Constants.BaloonType type);
        private void BalloonCreateHandler(string text, string title, Tools.Constants.BaloonType type)
        {
            var showBalloon = new BalloonCreateHandlerDelegate(createBalloonInRightThread);
            this.Dispatcher.Invoke(DispatcherPriority.Normal, showBalloon, text, title, type);
        }

        private delegate void OverlayDelegate(Tools.Constants.OverlayState state);
        private void ChangeTaskbarItemInfoOverlayHandler(Tools.Constants.OverlayState state)
        {
            var changeOverlayDelegate = new OverlayDelegate(changeOverlayStateInRightThread);
            this.Dispatcher.Invoke(DispatcherPriority.Normal, changeOverlayDelegate, state);
        }

        private delegate void LogOffDelegate();
        private void OtherThreadLogOffHandler()
        {
            LogOffDelegate logOffDelegate = LogOff;
            this.Dispatcher.Invoke(DispatcherPriority.Normal, logOffDelegate);
        }

        private void changeOverlayStateInRightThread(Tools.Constants.OverlayState state)
        {
            switch (state)
            {
                case Tools.Constants.OverlayState.Message:
                    this.TaskbarItemInfo.Overlay = Resources["MessageImage"] as ImageSource;
                    TaskbarItemInfo.ProgressState = TaskbarItemProgressState.Error;
                    TaskbarItemInfo.ProgressValue = 100;
                    TaskbarAnimationTimer = new Timer {Interval = 3000};
                    TaskbarAnimationTimer.Start();
                    TaskbarAnimationTimer.Elapsed += taskbarAnimationTimeEllapsed;
                    break;
                case Tools.Constants.OverlayState.NewTask:
                    this.TaskbarItemInfo.Overlay = Resources["TaskImage"] as ImageSource;
                    TaskbarItemInfo.ProgressState = TaskbarItemProgressState.Paused;
                    TaskbarItemInfo.ProgressValue = 100;
                    break;
                case Tools.Constants.OverlayState.Task:
                    this.TaskbarItemInfo.Overlay = Resources["TaskImage"] as ImageSource;
                    TaskbarItemInfo.ProgressState = TaskbarItemProgressState.Paused;
                    TaskbarItemInfo.ProgressValue = 100;
                    break;
                case Tools.Constants.OverlayState.Lunch:
                    this.TaskbarItemInfo.Overlay = Resources["LunchImage"] as ImageSource;
                    TaskbarItemInfo.ProgressState = TaskbarItemProgressState.Paused;
                    TaskbarItemInfo.ProgressValue = 100;
                    break;
                case Tools.Constants.OverlayState.Nothing:
                    this.TaskbarItemInfo.Overlay = null;
                    TaskbarItemInfo.ProgressState = TaskbarItemProgressState.None;
                    TaskbarItemInfo.ProgressValue = 100;

                    if (TaskbarAnimationTimer != null)
                    {
                        TaskbarAnimationTimer.Stop();
                        TaskbarAnimationTimer.Close();
                        TaskbarAnimationTimer.Dispose();
                        TaskbarAnimationTimer = null;
                    }
                    break;
            }
        }

        private delegate void AnimationDelegate();
        private void taskbarAnimationTimeEllapsed(object sender, EventArgs e)
        {
            var changeOverlayDelegate = new AnimationDelegate(OverlayStateAnimatingInRightThread);
            this.Dispatcher.Invoke(DispatcherPriority.Normal, changeOverlayDelegate);
        }

        private void OverlayStateAnimatingInRightThread()
        {
            if (TaskbarItemInfo.ProgressState == TaskbarItemProgressState.Error)
            {
                TaskbarItemInfo.ProgressState = TaskbarItemProgressState.None;
                if (TaskbarAnimationTimer != null)
                {
                    TaskbarAnimationTimer.Interval = 700;
                }
            }
            else
            {
                if (TaskbarAnimationTimer != null)
                {
                    TaskbarItemInfo.ProgressState = TaskbarItemProgressState.Error;
                    TaskbarAnimationTimer.Interval = 700;
                }
            }
        }
        
        private void Window_Activated(object sender, EventArgs e)
        {
            changeOverlayStateInRightThread(Tools.Constants.OverlayState.Nothing);
        }

        private void showWindowInRightThread(object sender, ContentEventArgs contentEventArgs)
        {
            Application.Current.MainWindow.Topmost = true;
            if (Application.Current.MainWindow.WindowState == WindowState.Minimized)
            {
                Application.Current.MainWindow.WindowState = WindowState.Normal;
            }
            Application.Current.MainWindow.Topmost = false;
        }

        private void lunchStatistocSite(object sender, RoutedEventArgs e)
        {
            string taskerWebSite = Settings.Default.TaskerWebSite;
            Process.Start(taskerWebSite);
        }
    }
}