﻿using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Tasker.Tests
{
    
    
    /// <summary>
    ///This is a test class for TaskTypeViewTest and is intended
    ///to contain all TaskTypeViewTest Unit Tests
    ///</summary>
    [TestClass()]
    public class TaskTypeViewTest
    {
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Equals
        ///</summary>
        [TestMethod()]
        public void EqualsTest()
        {
            TaskTypeView target = new TaskTypeView
                                      {
                                          Id =1,
                                          Status = ConstantsStatuses.Enabled,
                                          Title = "1",
                                          Description = "2"
                                      };
            TaskTypeView other = new TaskTypeView
                                     {
                                         Id = 1,
                                         Status = ConstantsStatuses.Enabled,
                                         Title = "1",
                                         Description = "2"
                                     };

            const bool expected = true;
            bool actual = target.Equals(other);
            Assert.AreEqual(expected, actual);
        }
    }
}
