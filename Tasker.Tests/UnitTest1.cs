﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tasker.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
//            A a = new B();
//            a.Do();
//            Assert.IsTrue(a is A);

            string atr = "C068A444-68EE-4940-90FB-BCA9469FCBF8_cx0_cy0_cw0_w800_r1";
            Console.WriteLine(atr + " " + atr.Length);
            if (atr.Length > 50)
            {
                Console.WriteLine(atr.Substring(0, 50));
            }
            Assert.IsTrue(true);
        }
    }

    public class A
    {
        public virtual void Do()
        {
            Console.WriteLine("A.Do()");
        }
    }

    public class B: A
    {
        public override void Do()
        {
            Console.WriteLine("B.Do()");
        }
    }
}
