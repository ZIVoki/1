﻿using System;
using Tasker.Server;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tasker.Tests
{
    [TestClass]
    public class ServiceCloserTest
    {
        [TestMethod]
        public void CloserTest()
        {
            bool sucess = true;
            try
            {
                ServiceCloser.Close();
            }
            catch (Exception e)
            {
                sucess = false;
                Console.WriteLine(e);
            }
            Assert.IsTrue(sucess);
        }
    }
}
