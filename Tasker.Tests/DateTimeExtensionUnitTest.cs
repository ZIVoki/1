﻿using System;
using Tasker.Tools.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tasker.Tests
{
    [TestClass]
    public class DateTimeExtensionUnitTest
    {
        [TestMethod]
        public void StartOfDayTest()
        {
            DateTime date = DateTime.Now;
            DateTime startOfDay = date.StartOfDay();
            Assert.IsTrue(startOfDay.Date == date.Date && startOfDay.Hour == 0 && startOfDay.Minute == 0 && startOfDay.Second == 0);
        }

        [TestMethod]
        public void EndOfDayTest()
        {
            DateTime date = DateTime.Now;
            DateTime endOfDay = date.EndOfDay();
            Assert.IsTrue(endOfDay >= date);
            Assert.IsTrue(endOfDay.Date == date.Date);
            Assert.IsTrue(endOfDay.Hour == 23);
            Assert.IsTrue(endOfDay.Minute == 59);
            Assert.IsTrue(endOfDay.Second == 59);
        }

        [TestMethod]
        public void FirstDayOfMonthTest()
        {
            DateTime date = DateTime.Now;
            DateTime firstDayOfMonth = date.FirstDayOfMonth();
            Assert.IsTrue(firstDayOfMonth.Month == date.Month);
            Assert.IsTrue(firstDayOfMonth.Year == date.Year);
            Assert.IsTrue(firstDayOfMonth <= date);
        }

        [TestMethod]
        public void LastDayOfMonthTest()
        {
            DateTime date = DateTime.Now;
            DateTime lastDayOfMonth = date.LastDayOfMonth();
            Assert.IsTrue(lastDayOfMonth.Month == date.Month);
            Assert.IsTrue(lastDayOfMonth.Year == date.Year);
            Assert.IsTrue(lastDayOfMonth.Hour == 0);
            Assert.IsTrue(lastDayOfMonth.Minute == 0);
            Assert.IsTrue(lastDayOfMonth.Second == 0);
            Assert.IsTrue(lastDayOfMonth.Date >= date.Date);
        }

        [TestMethod]
        public void EndOfLastDayOfMonthTest()
        {
            DateTime date = DateTime.Now;
            DateTime endOfLastDayOfMonth = date.EndOfLastDayOfMonth();
            Assert.IsTrue(endOfLastDayOfMonth.Month == date.Month);
            Assert.IsTrue(endOfLastDayOfMonth.Year == date.Year);
            Assert.IsTrue(endOfLastDayOfMonth.Hour == 23);
            Assert.IsTrue(endOfLastDayOfMonth.Minute == 59);
            Assert.IsTrue(endOfLastDayOfMonth.Second == 59);
            Assert.IsTrue(endOfLastDayOfMonth >= date);
        }

        [TestMethod]
        public void IsInPeriodTest()
        {
            DateTime date = DateTime.Now;
            DateTime startDate = date.AddDays(-1);
            DateTime endDate = date.AddDays(1);
            Assert.IsTrue(date.IsInPeriod(startDate, endDate));
            Assert.IsTrue(startDate.IsInPeriod(startDate, endDate));
            Assert.IsTrue(endDate.IsInPeriod(startDate, endDate));
            Assert.IsFalse(endDate.AddDays(1).IsInPeriod(startDate, endDate));
        }

        [TestMethod]
        public void IsOutOfPeriodTest()
        {
            DateTime date = DateTime.Now;
            DateTime startDate = date.AddDays(-1);
            DateTime endDate = date.AddDays(1);
            Assert.IsFalse(date.IsOutOfPeriod(startDate, endDate));
            Assert.IsFalse(startDate.IsOutOfPeriod(startDate, endDate));
            Assert.IsFalse(endDate.IsOutOfPeriod(startDate, endDate));
            Assert.IsTrue(endDate.IsOutOfPeriod(startDate, date));
            Assert.IsTrue(startDate.IsOutOfPeriod(date, endDate));
        }
    }
}
