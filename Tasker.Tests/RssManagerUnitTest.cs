﻿using System;
using System.Linq;
using System.Collections.Generic;
using Tasker.Tools.Managers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tasker.Tools.ViewModel;

namespace Tasker.Tests
{
    [TestClass]
    public class RssManagerUnitTest
    {
        private string siteUrl = "http://habrahabr.ru/";
        private string rssUrl = "http://habrahabr.ru/rss/best/";
        private string feedName = "Хабрахабр / Лучшие за сутки / Посты";
        private DateTime lastUpdateDate = DateTime.Now.AddDays(-1);

        [TestMethod]
        public void RssManagerTest()
        {
            IEnumerable<NotificationView> notificationViews = RssManager.GetNewRssNotify(rssUrl, lastUpdateDate);
            foreach (var notificationView in notificationViews)
            {
                Console.WriteLine(notificationView.Content);
            }

            Assert.IsNotNull(notificationViews);
            Assert.IsTrue(notificationViews.Count() > 0);
        }
    }
}
