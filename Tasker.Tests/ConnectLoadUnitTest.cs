﻿using System;
using System.ServiceModel;
using System.Collections.Generic;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AttendanceLogItemDTO = Tasker.Tests.ManagerServiceReference.AttendanceLogItemDTO;
using ConstantsNetworkStates = Tasker.Tests.ManagerServiceReference.ConstantsNetworkStates;
using CustomerDTO = Tasker.Tests.ManagerServiceReference.CustomerDTO;
using FaultDetail = Tasker.Tests.ManagerServiceReference.FaultDetail;
using FileDTO = Tasker.Tests.ManagerServiceReference.FileDTO;
using IManagerInterfaceCallback = Tasker.Tests.ManagerServiceReference.IManagerInterfaceCallback;
using IManagerInterfaceChannel = Tasker.Tests.ManagerServiceReference.IManagerInterfaceChannel;
using MessageDTO = Tasker.Tests.ManagerServiceReference.MessageDTO;
using ShiftDTO = Tasker.Tests.ManagerServiceReference.ShiftDTO;
using TaskDTO = Tasker.Tests.ManagerServiceReference.TaskDTO;
using TaskTypeDTO = Tasker.Tests.ManagerServiceReference.TaskTypeDTO;
using TimeTableDTO = Tasker.Tests.ManagerServiceReference.TimeTableDTO;
using UserDTO = Tasker.Tests.ManagerServiceReference.UserDTO;
using UserTaskTypeDTO = Tasker.Tests.ManagerServiceReference.UserTaskTypeDTO;

namespace Tasker.Tests
{
    [TestClass]
    public class ConnectLoadUnitTest
    {
        private static readonly List<string> managersList = new List<string> { "v.sokolov", "d.melnikov", "k.pavlov", "v.tissen" };
        private IManagerInterfaceChannel m_managerInterfaceChannel;

        [TestMethod]
        public void LoginLogoff()
        {
            CreateChanel();
            Random r =new Random();
            string manager = managersList[r.Next(managersList.Count)];
            m_managerInterfaceChannel.Login(manager, "");
            Thread.Sleep(TimeSpan.FromSeconds(r.Next(1, 10)));

//            TaskDTO taskDto = m_managerInterfaceChannel.GetTask(5046);
//            taskDto.Description = string.Format("это сделал я {0}", manager);
//            m_managerInterfaceChannel.UpdateTask(taskDto);
//
//            Thread.Sleep(TimeSpan.FromSeconds(r.Next(1, 10)));
//            int next = r.Next(0, 1);
//            if (next == 1)
//            {
//            }
//            Thread.Sleep(TimeSpan.FromSeconds(r.Next(1, 10)));

            m_managerInterfaceChannel.Logoff();
            m_managerInterfaceChannel.Close();
        }

        public void CreateChanel()
        {
            string URI = string.Format("net.tcp://{0}/manager", "localhost:8732");
            DuplexChannelFactory<IManagerInterfaceChannel> duplexChannelFactory =
                new DuplexChannelFactory<IManagerInterfaceChannel>(new InstanceContext(new ManagerCallback()),
                                                                   "managerEndPoint",
                                                                   new EndpointAddress(URI));
            m_managerInterfaceChannel = duplexChannelFactory.CreateChannel();
            m_managerInterfaceChannel.Open();
        }
    }

    public class ConnectedUser
    {
        public string Name { get; set; }
        public IManagerInterfaceChannel Channel { get; set; }

        private readonly Guid _id = Guid.NewGuid();

        public Guid Id
        {
            get { return _id; }
        }
    }

    [CallbackBehavior(UseSynchronizationContext=false, AutomaticSessionShutdown = false, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class ManagerCallback: IManagerInterfaceCallback
    {
        public void ReceiveMessage(MessageDTO msg)
        {
            throw new NotImplementedException();
        }

        public void UserEnter(int userId)
        {
            throw new NotImplementedException();
        }

        public void UserLogoff(int userId)
        {
            throw new NotImplementedException();
        }

        public void TaskUpdated(TaskDTO task)
        {
            throw new NotImplementedException();
        }

        public void FileAdded(int taskId, FileDTO attachFile)
        {
            throw new NotImplementedException();
        }

        public void TaskTypeAdded(TaskTypeDTO taskType)
        {
            throw new NotImplementedException();
        }

        public void TaskTypeUpdated(TaskTypeDTO taskType)
        {
            throw new NotImplementedException();
        }

        public void UserTaskTypeAdded(UserTaskTypeDTO userTaskType)
        {
            throw new NotImplementedException();
        }

        public void UserTaskTypeUpdated(UserTaskTypeDTO userTaskType)
        {
            throw new NotImplementedException();
        }

        public void FaultExeptionServerAlarm(FaultDetail createFaultDetail)
        {
            throw new NotImplementedException();
        }

        public void UserStateChanged(int userId, ConstantsNetworkStates stateId)
        {
            throw new NotImplementedException();
        }

        public void TimeTableUpdated(TimeTableDTO timeTable)
        {
            throw new NotImplementedException();
        }

        public void ShiftUpdated(ShiftDTO shift)
        {
            throw new NotImplementedException();
        }

        public void CustomerUpdated(CustomerDTO customer)
        {
            throw new NotImplementedException();
        }

        public void TimeTableAdded(TimeTableDTO timeTable)
        {
            throw new NotImplementedException();
        }

        public void UserUpdated(UserDTO user)
        {
            throw new NotImplementedException();
        }

        public void TaskAdded(TaskDTO task)
        {
            throw new NotImplementedException();
        }

        public void CustomerAdded(CustomerDTO customer)
        {
            throw new NotImplementedException();
        }

        public void TaskCheckedIn(TaskDTO taskDTO, string name)
        {
            throw new NotImplementedException();
        }

        public void TaskCheckedOut(TaskDTO taskDTO, string name)
        {
            throw new NotImplementedException();
        }

        public void TaskForReviewRecieved(TaskDTO taskDTO, DateTime sendTime)
        {
            throw new NotImplementedException();
        }

        public void TaskForReviewCancel(TaskDTO taskDto)
        {
            throw new NotImplementedException();
        }

        public void TaskAccepted(int taskId, int userrId, DateTime acceptTime)
        {
            throw new NotImplementedException();
        }

        public void WorkerComeback(int userId)
        {
            throw new NotImplementedException();
        }

        public void LunchRequest(UserDTO fromUser)
        {
            throw new NotImplementedException();
        }

        public void ShiftAdded(ShiftDTO shiftId)
        {
            throw new NotImplementedException();
        }

        public void UserAdded(UserDTO user)
        {
            throw new NotImplementedException();
        }

        public void WorkerFree(UserDTO worker)
        {
            throw new NotImplementedException();
        }

        public void WorkerBusy(UserDTO worker, TaskDTO task)
        {
            throw new NotImplementedException();
        }

        public void WorkerBlocked(UserDTO worker)
        {
            throw new NotImplementedException();
        }

        public void AttendanceLogItemAdded(AttendanceLogItemDTO attendanceLogItemDTO)
        {
            throw new NotImplementedException();
        }
    }
}
