﻿using System;
using Tasker.Tools.Attributes;
using Tasker.Tools.Extensions;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel.Base;

namespace Tasker.Tools.ViewModel
{
    public class UserView : ViewBase<UserDTO>
    {
        private DateTime? _lastActiveDate;

        [BackupField]
        private string _firstName;
        [BackupField]
        private string _lastName;
        [BackupField]
        private ConstantsRoles _role;
        [BackupField]
        private int? _weeklyHours;
        [BackupField]
        private ConstantsNetworkStates _state;
        [BackupField]
        private string _login;
        [BackupField]
        private string _email;
        [BackupField]
        private string _phone;
        [BackupField]
        private UniqBindableCollection<UserTaskTypeView> _taskTypes = new UniqBindableCollection<UserTaskTypeView>();
        [BackupField]
        private BindableCollection<AttestationView> _attestations = new BindableCollection<AttestationView>();

        private Constants.UserWorkState _workState = Constants.UserWorkState.Free;
        private string _domainName;

        public UserView()
        {
            Attestations.Add(new AttestationView { Comments = "blabla", Date = DateTime.Now, Mark = "5 (отлично)", User = this, Status = ConstantsStatuses.Enabled });
        }

        #region Properties

        public override ConstantsStatuses Status
        {
            get { return base.Status; }
            set
            {
                base.Status = value;
                RaisePropertyChanged(() => IsDeleted);
            }
        }

        public UniqBindableCollection<UserTaskTypeView> UserTaskTypes
        {
            get { return _taskTypes; }
            set
            {
                _taskTypes = value;
                RaisePropertyChanged(() => UserTaskTypes);
            }
        }

        public BindableCollection<AttestationView> Attestations
        {
            get { return _attestations; }
            set
            {
                _attestations = value;
                RaisePropertyChanged(() => Attestations);
            }
        }

        public override string Title
        {
            get { return string.Format("{0} {1}", _lastName, _firstName).Trim(); }
            set { }
        }

        public DateTime? LastActiveDate
        {
            get { return _lastActiveDate; }
            set
            {
                _lastActiveDate = value;
                RaisePropertyChanged(() => LastActiveDate);
            }
        }

        public string FirstName
        {
            get { return _firstName; }
            set
            {
                _firstName = value;
                RaisePropertyChanged(() => FirstName);
                RaisePropertyChanged(() => Title);
            }
        }

        public string LastName
        {
            get { return _lastName; }
            set
            {
                _lastName = value;
                RaisePropertyChanged(() => LastName);
                RaisePropertyChanged(() => Title);
            }
        }

        public bool IsDeleted
        {
            get { return Status == ConstantsStatuses.Deleted; }
        }

        public string DomainName
        {
            get { return _domainName; }
            set
            {
                _domainName = value;
                RaisePropertyChanged(() => DomainName);
            }
        }

        public ConstantsRoles Role
        {
            get { return _role; }
            set
            {
                _role = value;
                RaisePropertyChanged(() => Role);
            }
        }

        public int? WeeklyHours
        {
            get { return _weeklyHours; }
            set
            {
                _weeklyHours = value;
                RaisePropertyChanged(() => WeeklyHours);
            }
        }

        public string Login
        {
            get { return _login; }
            set
            {
                _login = value;
                RaisePropertyChanged(() => Login);
            }
        }

        public string Email
        {
            get { return _email; }
            set
            {
                _email = value;
                RaisePropertyChanged(() => Email);
            }
        }

        public string Phone
        {
            get { return _phone; }
            set
            {
                _phone = value;
                RaisePropertyChanged(() => Phone);
            }
        }

        public ConstantsNetworkStates CurrentState
        {
            get { return _state; }
            set
            {
                _state = value;
                RaisePropertyChanged(() => CurrentState);
            }
        }

        public Constants.UserWorkState UserWorkState
        {
            get { return _workState; }
            set
            {
                _workState = value;
                RaisePropertyChanged(() => UserWorkState);
            }
        }

        public string Password { get; set; }

        #endregion

        public override UserDTO BuildDTO()
        {
            var dto = new UserDTO
                          {
                              Id = _id,
                              Status = base.Status,
                              LastName = _lastName,
                              FirstName = _firstName,
                              DomainName = _domainName,
                              WeeklyHours = _weeklyHours,
                              Login = _login,
                              Email = _email,
                              Phone = _phone,
                              LastActiveDate = _lastActiveDate,
                              RoleId = _role
                          };
            return dto;
        }

        public bool Equals(UserView other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return base.Equals(other) && Equals(other._firstName, _firstName) && Equals(other._lastName, _lastName) && Equals(other._role, _role) && other._weeklyHours.Equals(_weeklyHours) && Equals(other._state, _state) && Equals(other._login, _login) && Equals(other._email, _email) && Equals(other._phone, _phone) && Equals(other._taskTypes, _taskTypes);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return Equals(obj as UserView);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int result = base.GetHashCode();
                result = (result * 397) ^ (_firstName != null ? _firstName.GetHashCode() : 0);
                result = (result * 397) ^ (_lastName != null ? _lastName.GetHashCode() : 0);
                result = (result * 397) ^ _role.GetHashCode();
                result = (result * 397) ^ (_weeklyHours.HasValue ? _weeklyHours.Value : 0);
                result = (result * 397) ^ _state.GetHashCode();
                result = (result * 397) ^ (_login != null ? _login.GetHashCode() : 0);
                result = (result * 397) ^ (_email != null ? _email.GetHashCode() : 0);
                result = (result * 397) ^ (_phone != null ? _phone.GetHashCode() : 0);
                result = (result * 397) ^ (_taskTypes != null ? _taskTypes.GetHashCode() : 0);
                return result;
            }
        }

        public static bool operator ==(UserView left, UserView right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(UserView left, UserView right)
        {
            return !Equals(left, right);
        }
    }
}
