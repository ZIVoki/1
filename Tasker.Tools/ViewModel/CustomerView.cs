﻿using System.Collections.ObjectModel;
using System.Globalization;
using Tasker.Tools.Attributes;
using Tasker.Tools.ServiceReference;

namespace Tasker.Tools.ViewModel
{
    public class CustomerView : HighlightItemView<CustomerDTO>
    {
        [BackupField]
        private string _description;
        [BackupField]
        private ChannelView _channel;
        private int _maxWorkers;
        private ObservableCollection<TaskTypeView> _taskTypeViews = new ObservableCollection<TaskTypeView>();

        public override string Title
        {
            get
            {
                return base.Title;
            }
            set
            {
                if (base.Title != value)
                {
                    base.Title = value;
                    RaisePropertyChanged(() => Title);
                }
            }
        }

        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
                RaisePropertyChanged(() => Description);
            }
        }

        public ChannelView Channel
        {
            get { return _channel; }
            set 
            { 
                _channel = value;
                RaisePropertyChanged(() => Channel);
                RaisePropertyChanged(() => Header);
            }
        }

        public string MaxWorkers
        {
            get { return _maxWorkers.ToString(CultureInfo.InvariantCulture); }
            set
            {
                int.TryParse(value, out _maxWorkers);
                RaisePropertyChanged(() => MaxWorkers);
            }
        }

        public ObservableCollection<TaskTypeView> TaskTypeViews
        {
            get { return _taskTypeViews; }
            set
            {
                _taskTypeViews = value;
                RaisePropertyChanged(() => TaskTypeViews);
            }
        }

        public string Header
        {
            get { return ToString(); }
        }

        public bool Equals(CustomerView other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return base.Equals(other) && Equals(other._description, _description) && Equals(other._channel, _channel);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return Equals(obj as CustomerView);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int result = base.GetHashCode();
                result = (result*397) ^ (_description != null ? _description.GetHashCode() : 0);
                result = (result*397) ^ (_channel != null ? _channel.GetHashCode() : 0);
                return result;
            }
        }

        public static bool operator ==(CustomerView left, CustomerView right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(CustomerView left, CustomerView right)
        {
            return !Equals(left, right);
        }

        public override string ToString()
        {
            if (Channel == null)
                return base.ToString();
            return string.Format("{0} [{1}]", Title, Channel);
        }

        public override CustomerDTO BuildDTO()
        {
            var dto = new CustomerDTO
                          {
                              Id = Id,
                              Status = base.Status,
                              Description = _description,
                              MaxWorkers = _maxWorkers,
                              Channel = _channel.BuildDTO(),
                              Title = base.Title,
                          };
            return dto;
        }
    }
}