﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tasker.Tools.ServiceReference;

namespace Tasker.Tools.ViewModel
{
    class TaskStatisticRecord
    {
        public DateTime EventDate { get; set; }
        public ConstantsStatuses TaskState { get; set; }
        //worker at current moment
        public UserDTO Worker { get; set; }
        //manager or another user who change task state
        public UserDTO User { get; set; }
        public TimeSpan Duration { get; set; }
    }
}
