﻿using System;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel.Base;

namespace Tasker.Tools.ViewModel
{
    public class MessageView : ViewBase<MessageDTO>
    {
        private TaskView _task;
        private UserView _user;
        private bool _isMy;
        private UserView _userFrom;
        private DateTime _sendDate;
        private string _text;
        private bool _isReaded;

        public MessageView(TaskView task)
        {
            Task = task;
        }

        public MessageView()
        {
        }

        public UserView UserDestination
        {
	        get 
	        {
                return _user;
	        }
	        set 
	        {
                _user = value;
                RaisePropertyChanged(() => UserDestination);
	        }
        }

        public UserView UserFrom
        {
            get
            {
                return _userFrom;
            }
            set
            {
                _userFrom = value;
                RaisePropertyChanged(() => UserFrom);
            }
        }

        public bool IsMy
        {
            get
            {
                return _isMy;
            }
            set
            {
                _isMy = value;
                RaisePropertyChanged(() => IsMy);
            }
        }

        public bool IsReaded
        {
            get
            {
                return _isReaded;
            }
            set
            {
                _isReaded = value;
                RaisePropertyChanged(() => IsReaded);
            }
        }

        public string Text
        {
            get
            {
                return _text;
            }
            set
            {
                _text = value;
                RaisePropertyChanged(() => Text);
            }
        }

        public DateTime SendDate
        {
            get
            {
                return _sendDate;
            }
            set
            {
                _sendDate = value;
                RaisePropertyChanged(() => SendDate);
            }
        }

        public TaskView Task
        {
            get
            {
                return _task;
            }
            private set
            {
                _task = value;
                RaisePropertyChanged(() => Task);
            }
        }

        public override MessageDTO BuildDTO()
        {
            UserDTO toUser = null;
            if (_user != null)
            {
                toUser = UserDestination.BuildDTO();
            }
            var dto = new MessageDTO
                          {
                              SendDate = _sendDate,
                              Text = _text,
                              ToUser = toUser,
                              Status = base.Status,
                              Task = Task.BuildDTO(),
                              Id = base.Id
                          };
            return dto;
        }
    }
}
