﻿using System;
using System.ComponentModel;
using Tasker.Tools.Attributes;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel.Base;

namespace Tasker.Tools.ViewModel
{
    public class TimeTableView: ViewBase<TimeTableDTO>, IDataErrorInfo
    {
        [BackupField]
        private DateTime m_startDate;
        [BackupField]
        private DateTime m_endDate;
        [BackupField]
        private ShiftView m_shift;
        [BackupField]
        private DateTime? m_firstLunchStart;
        [BackupField]
        private DateTime? m_firstLunchEnd;
        [BackupField]
        private DateTime? m_secondLunchStart;
        [BackupField]
        private DateTime? m_secondLunchEnd;
        [BackupField]
        private UserView m_user;
        [BackupField] 
        private bool m_isResp;

        #region Properties

        public UserView User
        {
            get { return m_user; }
            set
            {
                m_user = value;
                OnPropertyChanged("User");
            }
        }

        public ShiftView Shift
        {
            get { return m_shift; }
            set
            {
                m_shift = value;
                OnPropertyChanged("Shift");
            }
        }

        public DateTime StartDate
        {
            get { return m_startDate; }
            set
            {
                m_startDate = value;
                OnPropertyChanged("StartDate");
            }
        }

        public DateTime EndDate
        {
            get { return m_endDate; }
            set
            {
                m_endDate = value;
                OnPropertyChanged("EndDate");
            }
        }

        public double TotalTime
        {
            get { return EndDate.Subtract(StartDate).TotalHours; }
        }

        public DateTime? FirstLunchStart
        {
            get { return m_firstLunchStart; }
            set
            {
                m_firstLunchStart = value;
                OnPropertyChanged("FirstLunchStart");
            }
        }

        public DateTime? FirstLunchEnd
        {
            get { return m_firstLunchEnd; }
            set
            {
                m_firstLunchEnd = value;
                OnPropertyChanged("FirstLunchEnd");
            }
        }

        public DateTime? SecondLunchStart
        {
            get { return m_secondLunchStart; }
            set
            {
                m_secondLunchStart = value;
                OnPropertyChanged("SecondLunchStart");
            }
        }

        public DateTime? SecondLunchEnd
        {
            get { return m_secondLunchEnd; }
            set
            {
                m_secondLunchEnd = value;
                OnPropertyChanged("SecondLunchEnd");
            }
        }

        // отвественный за эфир
        public bool IsResp
        {
            get { return m_isResp; }
            set 
            { 
                m_isResp = value;
                OnPropertyChanged("IsResp");
            }
        }

        #endregion

        public override string Title
        {
            get
            {
                return string.Format("{0} {1} - {2}", m_user, m_startDate.ToString("dd.MM.yyyy"), m_endDate.ToString("dd.MM.yyyy"));
            }
        }

        public override TimeTableDTO BuildDTO()
        {
            var dto = new TimeTableDTO
                          {
                              StartDate = m_startDate,
                              EndDate = m_endDate,
                              Id = base.Id,
                              Status = base.Status,
                              FirstLunchStart = m_firstLunchStart,
                              FirstLunchEnd = m_firstLunchEnd,
                              SecondLunchStart = m_secondLunchStart,
                              SecondLunchEnd = m_secondLunchEnd,
                              Shift = m_shift.BuildDTO(),
                              User = m_user.BuildDTO(),
                              IsResp = m_isResp
                          };
            
            return dto;
        }

        public string this[string columnName]
        {
            get
            {
                string result = null;
                if (columnName == "StartDate" && !IsDatesAreValid)
                    result = "Start must occure before Finish!";
                else if (columnName == "EndDate" && !IsDatesAreValid)
                    result = "Finish must occure after Start!";
                return result;
            }
        }

        private bool IsDatesAreValid
        {
            get
            {
                return true;

//                return StartDate < EndDate;
            }
        }

        public string Error
        {
            get { throw new NotImplementedException(); }
        }
    }
}
