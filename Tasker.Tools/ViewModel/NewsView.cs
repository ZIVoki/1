﻿using System;
using Tasker.Tools.ViewModel.Base;

namespace Tasker.Tools.ViewModel
{
    public class NewsView : ObservableObject
    {
        private string _title;
        private DateTime _createDate;
        private string _body;

        public string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                RaisePropertyChanged(() => Title);
            }
        }

        public DateTime CreateDate
        {
            get { return _createDate; }
            set
            {
                _createDate = value;
                RaisePropertyChanged(() => CreateDate);
            }
        }

        public string Body
        {
            get { return _body; }
            set
            {
                _body = value;
                RaisePropertyChanged(() => Body);
            }
        }
    }
}