﻿using System;
using Tasker.Tools.Extensions;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel.Base;

namespace Tasker.Tools.ViewModel
{
    public class UserTaskTypeView : ViewBase<UserTaskTypeDTO>
    {
        private TimeSpan m_completionTime;
        private TaskTypeView m_taskType;
        private UserView m_user;

        public UserTaskTypeView(UserView user)
        {
            User = user;
        }

        public UserTaskTypeView()
        {
        }

        public override string Title
        {
            get
            {
                return TaskType.Title;
            }
        }

        public TimeSpan CompletionTime
        {
            get
            {
                return m_completionTime;
            }
            set
            {
                m_completionTime = value;
                OnPropertyChanged("CompletionTime");
            }
        }

        public TaskTypeView TaskType
        {
            get
            {
                return m_taskType;
            }
            set
            {
                m_taskType = value;
                OnPropertyChanged("TaskType");
            }
        }

        public UserView User
        {
            get
            {
                return m_user;
            }
            set
            {
                m_user = value;
                OnPropertyChanged("User");
            }
        }

        public override UserTaskTypeDTO BuildDTO()
        {
            var dto = new UserTaskTypeDTO
                          {
                              CompletionTime = m_completionTime,
                              TaskType = m_taskType.BuildDTO(),
                              User = m_user.BuildDTO(),
                              Status = base.Status,
                              Id = base.Id
                          };
            return dto;
        }
    }
}
