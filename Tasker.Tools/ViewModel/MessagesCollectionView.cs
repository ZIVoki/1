﻿using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows.Data;
using Tasker.Tools.CustomEventArgs;
using Tasker.Tools.Extensions;
using Tasker.Tools.ViewModel.Base;

namespace Tasker.Tools.ViewModel
{
    
    public class MessagesCollectionView : ObservableObject
    {
        public event ArgumentContainsDelegate HasUnreadChangedHandler;

        private UniqBindableCollection<MessageView> _messages = new UniqBindableCollection<MessageView>();

        public MessagesCollectionView()
        {
            _messages.CollectionChanged += MessagesChanges; 
        }

        private void MessagesChanges(object sender, NotifyCollectionChangedEventArgs e)
        {
            if(e.Action == NotifyCollectionChangedAction.Add || e.Action == NotifyCollectionChangedAction.Remove)
            {
                RaisePropertyChanged(() => UnreadCount);
                RaisePropertyChanged(() => HasUnread);

                InvokeHasUnreadChanged(HasUnread);
            }
        }

        public int UnreadCount
        {
            get
            {
                return _messages.Count(a => !a.IsReaded);
            }
        }

        public bool HasUnread
        {
            get
            {
                if(UnreadCount > 0)
                {
                    return true;
                }
                return false;
            }
        }

        public UniqBindableCollection<MessageView> Messages
        {
            get
            {
                return _messages;
            }
            set
            {
                _messages = value;
                RaisePropertyChanged(() => Messages);
            }
        }

        private CollectionViewSource _messagesCollectionView;
        public ICollectionView MessagesCollection
        {
            get
            {
                if (_messagesCollectionView == null)
                {
                    _messagesCollectionView = new CollectionViewSource { Source = Messages };
                    using (_messagesCollectionView.DeferRefresh())
                    {
                        _messagesCollectionView.SortDescriptions.Add(new SortDescription("Id",
                                                                                      ListSortDirection.Ascending));
//                        _messagesCollectionView.Filter += tasksCollectionView_Filter;
                    }
                }
                return _messagesCollectionView.View;
            }
        }

        public void SetAllAsRead()
        {
            foreach(var message in _messages)
            {
                message.IsReaded = true;
            }
            RaisePropertyChanged(() => HasUnread);
            InvokeHasUnreadChanged(HasUnread);
        }

        public void InvokeHasUnreadChanged(bool hasUnread)
        {
            ArgumentContainsDelegate handler = HasUnreadChangedHandler;
            if (handler != null) handler(null, new ContentEventArgs(hasUnread));
        }
    }
}
