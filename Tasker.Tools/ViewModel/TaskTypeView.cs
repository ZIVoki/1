﻿using System;
using Tasker.Tools.ServiceReference;

namespace Tasker.Tools.ViewModel
{
    public class TaskTypeView : HighlightItemView<TaskTypeDTO>
    {
        private string _description;
        private TimeSpan _completionTime;

        public TimeSpan CompletionTime
        {
            get
            {
                return _completionTime;
            }
            set
            {
                _completionTime = value;
                OnPropertyChanged("CompletionTime");
            }
        }

        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
                OnPropertyChanged("Description");
            }
        }

        public override TaskTypeDTO BuildDTO()
        {
            var dto = new TaskTypeDTO
                          {
                              Description = _description,
                              Id = Id,
                              CompletionTime = _completionTime,
                              Status = base.Status,
                              Title = base.Title,
                          };
            return dto;
        }

        public bool Equals(TaskTypeView other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return base.Equals(other) && Equals(other._description, _description) && other._completionTime.Equals(_completionTime);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return Equals(obj as TaskTypeView);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int result = base.GetHashCode();
                result = (result*397) ^ (_description != null ? _description.GetHashCode() : 0);
                result = (result*397) ^ _completionTime.GetHashCode();
                return result;
            }
        }

        public static bool operator ==(TaskTypeView left, TaskTypeView right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(TaskTypeView left, TaskTypeView right)
        {
            return !Equals(left, right);
        }
    }
}
