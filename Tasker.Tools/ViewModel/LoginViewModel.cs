﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using Tasker.Tools.Managers;
using Tasker.Tools.ViewModel.Base;

namespace Tasker.Tools.ViewModel
{
    public abstract class LoginViewModel<T> : ObservableObject where T: IServiceLoginManager
    {
        protected string _login = "";
        protected string _password ="";
        protected string _url ="";
        protected bool? _isAutoLogin = false;
        protected bool _isDomainUser = false;

        protected string _message = "";
        protected string _versionName = "ver. 1.0.0.0";
        protected Visibility _isLoading = Visibility.Hidden;
        protected T _service;
        private BackgroundWorker _loginigWorker;

        protected ICommand _startLogin;
        protected ICommand _saveSettings;

        public event EventHandler RequestClose;

        #region Properties

        public virtual string Login
        {
            get { return _login; }
            set
            {
                _login = value;
                RaisePropertyChanged(() => Login);
                if (IsDoaminName(value))
                    IsDomainUser = true;
                else
                    IsDomainUser = false;
            }
        }

        public virtual string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                RaisePropertyChanged(() => Password);
            }
        }

        public virtual string Url
        {
            get { return _url; }
            set
            {
                _url = value;
                RaisePropertyChanged(() => Url);
            }
        }

        public string Message
        {
            get { return _message; }
            set
            {
                _message = value;
                RaisePropertyChanged(() => Message);
                RaisePropertyChanged(() => MessageVisibility);
            }
        }

        public string VersionName
        {
            get { return _versionName; }
            set
            {
                _versionName = value;
                RaisePropertyChanged(() => VersionName);
            }
        }

        public Visibility MessageVisibility
        {
            get
            {
                if (String.IsNullOrEmpty(_message))
                    return Visibility.Hidden;
                return Visibility.Visible;
            }
        }

        public Visibility IsLoading
        {
            get { return _isLoading; }
            set
            {
                _isLoading = value;
                RaisePropertyChanged(() => IsLoading);
            }
        }

        public ICommand SaveSettingsCommand
        {
            get
            {
                if (_saveSettings == null)
                    _saveSettings = new RelayCommand(DoSaveSettings);
                return _saveSettings;
            }
        }

        public ICommand StartLoginCommand
        {
            get
            {
                if (_startLogin == null)
                    _startLogin = new RelayCommand(DoStartLogin);
                return _startLogin;
            }
        }

        public bool? IsAutoLogin
        {
            get { return _isAutoLogin; }
            set
            {
                _isAutoLogin = value;
                RaisePropertyChanged(() => IsAutoLogin);
            }
        }

        public bool IsDomainUser
        {
            get { return _isDomainUser; }
            set
            {
                _isDomainUser = value;
                RaisePropertyChanged(() => IsDomainUser);
            }
        }

        #endregion

        protected abstract bool IsUserExist();
        protected abstract void DoSaveSettings();

        private void DoStartLogin()
        {
            if (_loginigWorker == null)
            {
                _loginigWorker = new BackgroundWorker();
                _loginigWorker.DoWork += _loginigWorker_DoWork;
                _loginigWorker.WorkerSupportsCancellation = true;
                _loginigWorker.RunWorkerCompleted += _loginigWorker_RunWorkerCompleted;
            }
            if (!_loginigWorker.IsBusy)
            {
                IsLoading = Visibility.Visible;
                _loginigWorker.RunWorkerAsync();
            }
        }

        void _loginigWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            IsLoading = Visibility.Hidden;
            Message = e.Result as String;
            if (IsUserExist())
            {
                DoSaveSettings();
                DoSuccessCancel();
            }
        }

        protected void OnRequestClose()
        {
            EventHandler handler = RequestClose;
            if (handler != null) handler(this, EventArgs.Empty);
        }

        protected void DoSuccessCancel()
        {
            OnRequestClose();
        }

        void _loginigWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            e.Result = _service.Login(Login, Password, Url);
        }

        protected abstract string IdentUser();
        protected abstract bool IsDoaminName(string name);
    }

}
