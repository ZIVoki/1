using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel.Base;

namespace Tasker.Tools.ViewModel
{
    public class ChannelView: ViewBase<ChannelDTO>
    {
        public override ChannelDTO BuildDTO()
        {
            var dto = new ChannelDTO()
                          {
                              Id = base.Id,
                              Status = base.Status,
                              Title = base.Title
                          };
            return dto;
        }
    }
}