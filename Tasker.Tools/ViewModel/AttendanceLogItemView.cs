﻿using System;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel.Base;

namespace Tasker.Tools.ViewModel
{
    public class AttendanceLogItemView : ViewBase<AttendanceLogItemDTO>
    {
        private UserView _user;
        private DateTime _eventDateTime;
        private TimeTableView _timeTable;
        private bool _isFirst = false;

        public UserView User
        {
            get { return _user; }
            set
            {
                _user = value;
                RaisePropertyChanged(() => User);
            }
        }

        public DateTime EventDateTime
        {
            get { return _eventDateTime; }
            set
            {
                _eventDateTime = value;
                RaisePropertyChanged(() => EventDateTime);
            }
        }

        public TimeTableView TimeTable
        {
            get { return _timeTable; }
            set
            {
                _timeTable = value;
                RaisePropertyChanged(() => TimeTable);
            }
        }

        public bool IsFirst
        {
            get { return _isFirst; }
            set
            {
                _isFirst = value;
                RaisePropertyChanged(() => IsFirst);
            }
        }

        public override AttendanceLogItemDTO BuildDTO()
        {
            AttendanceLogItemDTO attendanceLogItemDto = new AttendanceLogItemDTO
                {
                    EventDateTime = this._eventDateTime,
                    Id = base.Id,
                    Status = base.Status,
                    TimeTable = this.TimeTable.BuildDTO(),
                    User = this.User.BuildDTO()
                };
            return attendanceLogItemDto;
        }

        public override string ToString()
        {
            return string.Format("{0}, User: {1}, EventDateTime: {2}, TimeTable: {3}", base.ToString(), _user, _eventDateTime, _timeTable);
        }

        public bool Equals(AttendanceLogItemView other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(other._user, _user) && other._eventDateTime.Equals(_eventDateTime) && Equals(other._timeTable, _timeTable);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(AttendanceLogItemView)) return false;
            return Equals((AttendanceLogItemView)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int result = (_user != null ? _user.GetHashCode() : 0);
                result = (result * 397) ^ _eventDateTime.GetHashCode();
                result = (result * 397) ^ (_timeTable != null ? _timeTable.GetHashCode() : 0);
                return result;
            }
        }

        public static bool operator ==(AttendanceLogItemView left, AttendanceLogItemView right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(AttendanceLogItemView left, AttendanceLogItemView right)
        {
            return !Equals(left, right);
        }
    }
}