using System;
using Tasker.Tools.ViewModel.Base;

namespace Tasker.Tools.ViewModel
{
    public class UserReadView : ObservableObject
    {
        private UserView _user;
        private IFileView _file;
        private DateTime? _readDate;

        public UserView User
        {
            get { return _user; }
            set
            {
                _user = value;
                RaisePropertyChanged(() => User);
            }
        }

        public IFileView File
        {
            get { return _file; }
            set
            {
                _file = value;
                RaisePropertyChanged(() => File);
            }
        }

        public DateTime? ReadDate
        {
            get { return _readDate; }
            set
            {
                _readDate = value;
                RaisePropertyChanged(() => ReadDate);
                RaisePropertyChanged(() => ReadDateString);
            }
        }

        public String ReadDateString
        {
            get
            {
                if (!ReadDate.HasValue) return "";
                return ReadDate.Value.ToString("HH:mm d.MM.yyyy");
            }
        }
    }
}