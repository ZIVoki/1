﻿using Tasker.Tools.Attributes;

namespace Tasker.Tools.ViewModel
{
    public class WorkerView : UserView
    {
        [BackupField]
        private TaskView m_task;

        public bool IsEmpty { get; set; }

        public TaskView CurrentTask
        {
            get
            {
                return m_task;
            }
            set
            {
                m_task = value;
                OnPropertyChanged("CurrentTask");
            }
        }

        public bool IsBusy
        {
            get 
            {
                return m_task != null;
            }
        }
    }
}
