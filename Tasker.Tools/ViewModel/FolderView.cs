using System;
using System.Diagnostics;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel.Base;

namespace Tasker.Tools.ViewModel
{
    public class FolderView : TreeViewItemViewModel
    {
        private string _folderPath;
        private DateTime _creationDateTime;
        private const string _extension = "";

        public string FolderPath
        {
            get { return _folderPath; }
            set
            {
                _folderPath = value;
                RaisePropertyChanged(() => FolderPath);
            }
        }

        public DateTime CreationDateTime
        {
            get { return _creationDateTime; }
            set
            {
                _creationDateTime = value;
                RaisePropertyChanged(() => CreationDateTime);
            }
        }

        public String Extension
        {
            get { return _extension; }
        }

        public FolderView(FolderDTO folderDTO, FolderView parent, bool lazyLoadChildren)
            : base(parent, lazyLoadChildren)
        {
            Id = folderDTO.Id;
            base.Status = folderDTO.Status;
            base.Title = folderDTO.Title;
            _folderPath = folderDTO.Path;
            _creationDateTime = folderDTO.CreateDate;
            Debug.WriteLine(string.Format("Creating new folder {0}", base.Title));
            if (parent != null)
                parent.Children.Add(this);
        }

        public FolderView(int id, string title, FolderView parent, bool lazyLoadChildren)
            : base(parent, lazyLoadChildren)
        {
            Id = id;
            base.Title = title;
            Debug.WriteLine(string.Format("Creating new folder {0}", base.Title));
            if (parent != null)
                parent.Children.Add(this);
        }
    }
}