using System;
using System.Diagnostics;
using System.IO;
using Tasker.Tools.Extensions;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel.Base;

namespace Tasker.Tools.ViewModel
{
    // file with current version
    public interface IFileView: ITreeViewItemViewModel
    {
        string Extension { get; }
        string FilePath { get; set; }
        string SourceFilePath { get; set; }
        string OldSourceFilePath { get; }
        int Version { get; set; }
        string VersionDiff { get; set; }

        /// <summary>
        /// file change date
        /// </summary>
        DateTime ChangesDateTime { get; set; }

        DateTime? ReadDate { get; set; }
        BindableCollection<UserReadView> UsersReadFile { get; set; }
        bool IsRead { get; }
    }

    public class FileView : TreeViewItemViewModel, IFileView
    {
        private string _filePath;
        private int _version;
        private string _versionDiff;
        private DateTime _changesDateTime;
        private string _sourceFilePath;
        private DateTime? _readDate;
        private BindableCollection<UserReadView> _usersReadFile;

        public string Extension
        {
            get { return Path.GetExtension(FilePath); }
        }

        public string FilePath
        {
            get { return _filePath; }
            set
            {
                _filePath = value;
                RaisePropertyChanged(() => FilePath);
                RaisePropertyChanged(() => Extension);
                RaisePropertyChanged(() => OldSourceFilePath);
            }
        }

        public string SourceFilePath
        {
            get { return _sourceFilePath; }
            set
            {
                _sourceFilePath = value;
                RaisePropertyChanged(() => SourceFilePath);
                RaisePropertyChanged(() => OldSourceFilePath);
            }
        }

        public string OldSourceFilePath
        {
            get
            {
                string directoryName = Path.GetDirectoryName(FilePath);
                string sourceFileName = Path.GetFileName(SourceFilePath);
                string sourceFilePath = Path.Combine(directoryName, Constants.LastVersionFolderName,
                                                     sourceFileName);
                return sourceFilePath;
            }
        }

        public int Version
        {
            get { return _version; }
            set
            {
                _version = value;
                RaisePropertyChanged(() => Description);
                RaisePropertyChanged(() => Version);
            }
        }

        public string VersionDiff
        {
            get { return _versionDiff; }
            set
            {
                _versionDiff = value;
                RaisePropertyChanged(() => VersionDiff);
            }
        }

        /// <summary>
        /// file change date
        /// </summary>
        public DateTime ChangesDateTime
        {
            get { return _changesDateTime; }
            set
            {
                _changesDateTime = value;
                RaisePropertyChanged(() => Description);
                RaisePropertyChanged(() => ChangesDateTime);
            }
        }

        public DateTime? ReadDate
        {
            get { return _readDate; }
            set
            {
                _readDate = value;
                RaisePropertyChanged(() => ReadDate);
                RaisePropertyChanged(() => IsRead);
            }
        }

        public BindableCollection<UserReadView> UsersReadFile
        {
            get { return _usersReadFile; }
            set
            {
                _usersReadFile = value;
                RaisePropertyChanged(() => UsersReadFile);
            }
        }

        public bool IsRead
        {
            get { return ReadDate.HasValue; }
        }

        public String Description
        {
            get
            {
                return String.Format("{0} ������ {1} �� {2}", Path.GetFileNameWithoutExtension(Title), Version,
                                     ChangesDateTime.ToString("d.MM.yyyy"));
            }
        }

        public FileView(FileVersionDTO fileVersion, FolderView parent, bool lazyLoadChildren)
            : base(parent, lazyLoadChildren)
        {
            Id = fileVersion.File.Id;
            base.Status = fileVersion.Status;
            base.Title = fileVersion.File.Title;
            _filePath = fileVersion.File.FilePath;
            _sourceFilePath = fileVersion.File.FileSource;
            _version = fileVersion.Version;
            _versionDiff = fileVersion.VersionDiff;
            _changesDateTime = fileVersion.ChangesDate;
            _readDate = fileVersion.ReadDate;
            Debug.WriteLine(string.Format("Creating new file {0}", base.Title));
            if (parent != null)
                parent.Children.Add(this);

            UsersReadFile = new BindableCollection<UserReadView>();
        }
    }
}