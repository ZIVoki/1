﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Tasker.Tools.ViewModel
{
    public class WorkDayView : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged Members

        public virtual event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string info)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(info));
            }
        }

        #endregion

        private DateTime m_date;
        private List<ShiftWorkersView> m_shiftWorkers;

        public DateTime Date
        {
            get { return m_date; }
            set 
            { 
                m_date = value;
                OnPropertyChanged("Date");
            }
        }

        public List<ShiftWorkersView> ShiftWorkers
        {
            get { return m_shiftWorkers; }
            set
            {
                m_shiftWorkers = value;
                OnPropertyChanged("ShiftWorkers");
            }
        }
    }

    public class ShiftWorkersView : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged Members

        public virtual event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string info)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(info));
            }
        }

        #endregion

        private List<UserView> m_users;
        private ShiftView m_shift;

        public ShiftView Shift
        {
            get { return m_shift; }
            set
            {
                m_shift = value;
                OnPropertyChanged("Shift");
            }
        }

        public List<UserView> Users
        {
            get { return m_users; }
            set
            {
                m_users = value;
                OnPropertyChanged("Users");
            }
        }
    }
}
