﻿using System;
using Tasker.Tools.ViewModel.Base;

namespace Tasker.Tools.ViewModel
{
    public class AttestationView : ViewBase
    {
        private DateTime _date;
        private UserView _user;
        private string _comments;
        private string _mark;

        public AttestationView(DateTime date, UserView user, string comments)
        {
            _date = date;
            _user = user;
            _comments = comments;
        }

        public AttestationView()
        { }

        public string Comments
        {
            get { return _comments; }
            set
            {
                _comments = value;
                RaisePropertyChanged(() => Comments);
            }
        }

        public DateTime Date
        {
            get { return _date; }
            set
            {
                _date = value;
                RaisePropertyChanged(() => Date);
            }
        }

        public UserView User
        {
            get { return _user; }
            set
            {
                _user = value;
                RaisePropertyChanged(() => User);
            }
        }

        public string Mark
        {
            get { return _mark; }
            set
            {
                _mark = value;
                RaisePropertyChanged(() => Mark);
            }
        }
    }
}