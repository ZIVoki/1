﻿using System;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel.Base;

namespace Tasker.Tools.ViewModel
{
    public class ShiftView: ViewBase<ShiftDTO>
    {
        private TimeSpan m_startTime;
        private TimeSpan m_endTime;
        private TimeSpan? m_lunchDuration;

        public TimeSpan StartTime
        {
            get { return m_startTime; }
            set
            {
                m_startTime = value;
                OnPropertyChanged("StartTime");
            }
        }

        public TimeSpan EndTime
        {
            get { return m_endTime; }
            set
            {
                m_endTime = value;
                OnPropertyChanged("EndTime");
            }
        }

        public TimeSpan? LunchDuration
        {
            get { return m_lunchDuration; }
            set
            {
                m_lunchDuration = value;
                OnPropertyChanged("LunchDuration");
            }
        }

        public override ShiftDTO BuildDTO()
        {
            var result = new ShiftDTO
                             {
                                 EndTime = m_endTime,
                                 StartTime = m_startTime,
                                 LunchDuration = m_lunchDuration,
                                 Id = Id,
                                 Status = base.Status,
                                 Title = base.Title
                             };
            return result;
        }
    }
}