using System;
using System.Diagnostics;
using System.Windows.Input;
using Tasker.Tools.Managers;
using Tasker.Tools.ViewModel.Base;

namespace Tasker.Tools.ViewModel
{
    public enum NotificationType
    {
        None = 0,
        NewTask =1,
        NewMessage = 2,
        Lunch = 3,
        Rss = 4,
        TaskUpdate
    }

    public class NotificationView : ObservableObject
    {
        private string _content;
        private bool _isRead;
        private DateTime _dateTime;
        private RelayCommand _activateCommnad;
        private NotificationType _notificationType;
        private string _url;
        private ICommand _openUrlCommand;
        private Action _onClickAction;
        public event EventHandler NotificationReadEvent;

        public NotificationView()
        {
            _notificationType = NotificationType.None;
            _dateTime = DateTime.Now;
            _isRead = false;
        }

        public NotificationView(string content): this()
        {
            _content = content;
        }

        public NotificationView(string content, DateTime date):this(content)
        {
            _dateTime = date;
        }

        public NotificationView(string content, DateTime date, NotificationType notificationType): this(content, date)
        {
            _notificationType = notificationType;
        }

        public NotificationView(TaskView taskView): this()
        {
            _content = string.Format("����� ������ {0}.", taskView.Title);
            _notificationType = NotificationType.NewTask;
        }

        public NotificationView(RssItem rssItem): this(rssItem.Title, rssItem.Pubdate, NotificationType.Rss)
        {
            Url = rssItem.Link;
            OnClickAction = delegate
            {
                if (_notificationType == NotificationType.Rss && IsUrl())
                {
                    DoOpenUrl();
                }
            };
        }

        public NotificationView(string content, DateTime dateTime, Action onClickAction): this(content, dateTime)
        {
            _onClickAction = onClickAction;
        }

        #region Properties

        public NotificationType NotificationType
        {
            get { return _notificationType; }
            set
            {
                _notificationType = value;
                RaisePropertyChanged(() => NotificationType);
                RaisePropertyChanged(() => IsRss);
            }
        }

        public DateTime DateTime
        {
            get { return _dateTime; }
            set
            {
                _dateTime = value;
                RaisePropertyChanged(() => DateTime);
            }
        }

        public virtual string Content
        {
            get { return _content; }
            set
            {
                _content = value;
                RaisePropertyChanged(() => Content);
            }
        }

        public bool IsRead
        {
            get { return _isRead; }
            set
            {
                _isRead = value;
                RaisePropertyChanged(() => IsRead);
                OnNotificationReadEvent(new EventArgs());
            }
        }

        private void OnNotificationReadEvent(EventArgs e)
        {
            EventHandler handler = NotificationReadEvent;
            if (handler != null) handler(this, e);
        }

        public bool IsRss
        {
            get { return _notificationType == NotificationType.Rss; }
        }

        public string Url
        {
            get { return _url; }
            set
            {
                _url = value;
                RaisePropertyChanged(() => Url);
            }
        }

        public Action OnClickAction
        {
            get { return _onClickAction; }
            set { _onClickAction = value; }
        }

        #endregion

        #region Commands

        public virtual ICommand ActivateCommand
        {
            get
            {
                if (_activateCommnad == null)
                {
                    _activateCommnad = new RelayCommand(DoReadNotify);
                }

                return _activateCommnad;
            }
        }

        private void DoReadNotify()
        {
            IsRead = true;
            if (OnClickAction != null)
                OnClickAction.Invoke();
        }

        private bool IsUrl()
        {
            if (_url.StartsWith("http"))
                return true;
            return false;
        }

        private void DoOpenUrl()
        {
            Process.Start(_url);
        }

        #endregion

        protected bool Equals(NotificationView other)
        {
            return string.Equals(_content, other._content) && _dateTime.Equals(other._dateTime) && string.Equals(_url, other._url);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((NotificationView) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = (_content != null ? _content.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ _dateTime.GetHashCode();
                hashCode = (hashCode*397) ^ (_url != null ? _url.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}