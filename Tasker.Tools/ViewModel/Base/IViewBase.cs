using Tasker.Tools.ServiceReference;

namespace Tasker.Tools.ViewModel.Base
{
    public interface IViewBase
    {
        int Id { get; set; }
        string Title { get; set; }
        ConstantsStatuses Status { get; set; }
        bool IsClosed { get; }
        bool IsInProcess { get; }
        bool IsChanged { get; set; }
    }
}