using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using Tasker.Shared;
using Tasker.Tools.Attributes;
using Tasker.Tools.CustomEventArgs;
using Tasker.Tools.ServiceReference;

namespace Tasker.Tools.ViewModel.Base
{
    public delegate void StatusChangedDelegate(object sender, ContentsEventArgs e);

    public abstract class ViewBase : ObservableObject, IEditableObject, IViewBase
    {
        protected int _id;
        protected bool _isChanged;
        protected Dictionary<FieldInfo, object> _backupFields;

        public event StatusChangedDelegate StatusChangedEventHandler;

        public void InvokeStatusChangedEventHandler(ContentsEventArgs e)
        {
            StatusChangedDelegate handler = StatusChangedEventHandler;
            if (handler != null) handler(this, e);
        }

        [BackupField]
        protected string _title;
        [BackupField]
        protected ConstantsStatuses _status;

        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
                RaisePropertyChanged(() => Id);
            }
        }

        public virtual string Title
        {
            get
            {
                return _title;
            }
            set
            {
                if (_title != value)
                {
                    _title = value;
                    RaisePropertyChanged(() => Title);
                }
            }
        }

        public virtual ConstantsStatuses Status
        {
            get
            {
                return _status;
            }
            set
            {
                if (_status != value)
                {
                    _status = value;
                    RaisePropertyChanged(() => Status);
                    RaisePropertyChanged(() => IsClosed);
                    RaisePropertyChanged(() => IsInProcess);
                    InvokeStatusChangedEventHandler(new ContentsEventArgs(new object[] { _status }));
                }
            }
        }

        public bool IsClosed
        {
            get { return (Status == ConstantsStatuses.Closed); }
        }

        public bool IsInProcess
        {
            get { return (Status == ConstantsStatuses.InProcess); }
        }


        public bool IsChanged
        {
            get
            {
                return _isChanged;
            }
            set
            {
                _isChanged = value;
                RaisePropertyChanged(() => IsChanged);
            }
        }

        #region EditableObject

        public virtual void BeginEdit()
        {
            _backupFields = new Dictionary<FieldInfo, object>();
            var fields = GetType().GetFields(BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Static);
            foreach (var field in fields)
            {
                var attribute = field.GetCustomAttributes(typeof(BackupFieldAttribute), true);
                if (attribute.Length > 0)
                {
                    var valueClone = field.GetValue(this).ShallowCopy();
                    _backupFields.Add(field, valueClone);
                }
            }
        }

        public virtual void EndEdit()
        {
            if (_backupFields != null) _backupFields.Clear();
        }

        public virtual void CancelEdit()
        {
            if (!IsChanged && _backupFields != null) return;


            if (_backupFields != null)
                foreach (var backupField in _backupFields)
                {
                    backupField.Key.SetValue(this, backupField.Value);
                }


            IsChanged = false;
        }
        #endregion

        #region INotifyPropertyChanged Members

        protected virtual void OnPropertyChanged(string info)
        {
            if (!_isChanged)
            {
                IsChanged = true;
            }

            RaisePropertyChanged(info);
        }

        #endregion

        public override string ToString()
        {
            if (String.IsNullOrEmpty(Title)) return String.Empty;
            return Title.Trim();
        }
    }

    public abstract class ViewBase<T> : ViewBase
    {
        public T DTOObject { get; set; }

        public virtual T BuildDTO()
        {
            throw new NotImplementedException();
        }

        public bool Equals(ViewBase<T> other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return other._id == _id && Equals(other._title, _title) && Equals(other._status, _status);
        }
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(ViewBase<T>)) return false;
            return Equals((ViewBase<T>)obj);
        }

        public static bool operator ==(ViewBase<T> left, ViewBase<T> right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(ViewBase<T> left, ViewBase<T> right)
        {
            return !Equals(left, right);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int result = _id;
                result = (result * 397) ^ (_title != null ? _title.GetHashCode() : 0);
                result = (result * 397) ^ _status.GetHashCode();
                return result;
            }
        }
    }
}
