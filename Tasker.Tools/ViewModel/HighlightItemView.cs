﻿using Tasker.Tools.Attributes;
using Tasker.Tools.ViewModel.Base;

namespace Tasker.Tools.ViewModel
{
    public class HighlightItemView<T> : ViewBase<T>
    {
        [BackupField]
        protected System.Drawing.Color? m_hightlightBrush;
        
        public System.Drawing.Color? HighLightBrush
        {
            get
            {
                if(m_hightlightBrush == null)
                {
                    return null;// System.Drawing.Color.White;
                }
                return m_hightlightBrush;
            }
            set
            {
                m_hightlightBrush = value;
                OnPropertyChanged("HighLightBrush");
            }
        }

        public void UpdateHighlightBindings()
        {
            OnPropertyChanged("HighLightBrush");
        }
    }
}
