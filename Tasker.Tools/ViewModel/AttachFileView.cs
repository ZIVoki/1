﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Input;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel.Base;

namespace Tasker.Tools.ViewModel
{
    public class AttachFileView : ViewBase<AttachFileDTO>
    {
        private const int MaxSymbolsCount = 25;

        private string _filePath;
        private DateTime _createdAt;
        private bool _isFinalyFile;
        private TaskView _task;
        private ICommand _openFileCommand;
        private ICommand _openFilePath;

        public AttachFileView(TaskView task)
        {
            Task = task;
        }

        public DateTime CreatedAt
        {
            get
            {
                return _createdAt;
            }
            set
            {
                _createdAt = value;
                RaisePropertyChanged(() => CreatedAt);
            }
        }

        public override string Title
        {
            get { return base.Title; }
            set
            {
                base.Title = value;
                RaisePropertyChanged(() => FileName);
            }
        }

        public string FilePath
        {
            get
            {
                return _filePath;
            }
            set
            {
                _filePath = value;
                RaisePropertyChanged(() => FilePath);
            }
        }

        public bool IsFinalyFile
        {
            get
            {
                return _isFinalyFile;
            }
            set
            {
                _isFinalyFile = value;
                RaisePropertyChanged(() => IsFinalyFile);
            }
        }

        public TaskView Task
        {
            get
            {
                return _task;
            }
            private set
            {
                _task = value;
                RaisePropertyChanged(() => Task);
            }
        }

        public ICommand OpenFile
        {
            get
            {
                if (_openFileCommand == null)
                    return _openFileCommand = new RelayCommand(DoOpenFile);
                return _openFileCommand;
            }
        }

        private void DoOpenFile()
        {
            try
            {
                Process.Start(FilePath);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public ICommand OpenFilePath
        {
            get
            {
                if (_openFilePath == null)
                    _openFilePath = new RelayCommand(OnExecuteOpenFilePath);
                return _openFilePath;
            }
        }

        public string FileName
        {
            get
            {
                if (Title.Length > MaxSymbolsCount)
                    return Title.Substring(0, MaxSymbolsCount) + "...";
                return Title;
            }
        }

        private bool OnCanOpenfilePath()
        {
            string directoryName = Path.GetDirectoryName(FilePath);
            bool exists = Directory.Exists(directoryName);
            return exists;
        }

        private void OnExecuteOpenFilePath()
        {
            try
            {
                string directoryName = Path.GetDirectoryName(FilePath);
                Process.Start(directoryName);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public override AttachFileDTO BuildDTO()
        {
            var dto = new AttachFileDTO
                          {
                              CreatedAt = _createdAt,
                              FilePath = _filePath,
                              Task = Task.BuildDTO(),
                              IsFinalyFile = _isFinalyFile,
                              Id = base.Id,
                              Status = base.Status,
                              Title = base.Title
                          };
            return dto;
        }
    }
}
