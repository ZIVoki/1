using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.Specialized;
using Tasker.Tools.Attributes;
using Tasker.Tools.CustomEventArgs;
using Tasker.Tools.Extensions;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel.Base;

namespace Tasker.Tools.ViewModel
{
    public enum PriorityType
    {
        Background = 0,
        Quick = 1
    }

    public delegate void ArgumentContainsDelegate(object sender, ContentEventArgs e);
    public delegate void MultiArgumentContainsDelegate(object sender, ContentsEventArgs e);
    public class TaskView : ViewBase<TaskDTO>
    {
        public event ArgumentContainsDelegate NewMessagesAddedHandler;
        public event MultiArgumentContainsDelegate HasUnreadMessagesChangedHandler;

        [BackupField]
        private string _description;
        [BackupField]
        private string _comment;
        [BackupField]
        private UserView _currentWorker;
        [BackupField]
        private UserView _creator;
        [BackupField]
        private CustomerView _customer;
        [BackupField]
        private TaskTypeView _taskType;
        [BackupField]
        private DateTime? _deadLine;
        [BackupField]
        private int _priority;
        [BackupField]
        private TimeSpan _plannedTime;
        [BackupField]
        private DateTime _creationDate;
        [BackupField]
        private DateTime? _completeDate;
        [BackupField]
        private TimeSpan _inProcessTime;

        private UniqBindableCollection<AttachFileView> _files = new UniqBindableCollection<AttachFileView>();

        private MessagesCollectionView _messages = new MessagesCollectionView();
        private Constants.TaskWorkState _workState = Constants.TaskWorkState.Available;

        #region Propetries

        public DateTime CreationDate
        {
            get
            {
                return _creationDate;
            }
            set
            {
                _creationDate = value;
                RaisePropertyChanged(() => CreationDate);
            }
        }

        public DateTime? CompleteDate
        {
            get
            {
                return _completeDate;
            }
            set
            {
                _completeDate = value;
                RaisePropertyChanged(() => CompleteDate);
            }
        }

        public UserView Creator
        {
            get
            {
                return _creator;
            }
            set
            {
                _creator = value;
                RaisePropertyChanged(() => Creator);
            }
        }

        public UserView CurrentWorker
        {
            get
            {
                return _currentWorker;
            }
            set
            {
                _currentWorker = value;
                RaisePropertyChanged(() => CurrentWorker);
            }
        }

        public CustomerView Customer
        {
            get
            {
                return _customer;
            }
            set
            {
                _customer = value;
                RaisePropertyChanged(() => Customer);
            }
        }

        public DateTime? DeadLine
        {
            get
            {
                return _deadLine;
            }
            set
            {
                _deadLine = value;
                RaisePropertyChanged(() => DeadLine);
            }
        }

        public int Priority
        {
            get
            {
                return _priority;
            }
            set
            {
                _priority = value;
                RaisePropertyChanged(() => Priority);
                RaisePropertyChanged(() => PriorityType);
            }
        }

        public TaskTypeView TaskType
        {
            get
            {
                return _taskType;
            }
            set
            {
                if (_taskType != value)
                {
                    _taskType = value;
                    RaisePropertyChanged(() => TaskType);
                }
            }
        }

        public PriorityType PriorityType
        {
            get
            {
                if (_priority >= 1)
                    return PriorityType.Quick;
                return PriorityType.Background;
            }
            set
            {
                switch (value)
                {
                    case PriorityType.Background:
                        Priority = 0;
                        break;
                    case PriorityType.Quick:
                        Priority = 1;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException("value");
                }
                RaisePropertyChanged(() => Priority);
            }
        }

        public string Description
        {
            get
            {
                if (String.IsNullOrEmpty(_description))
                    return "��� ��������";
                return _description;
            }
            set
            {
                _description = value;
                RaisePropertyChanged(() => Description);
            }
        }

        public string Comment
        {
            get { return _comment; }
            set
            {
                _comment = value;
                RaisePropertyChanged(() => Comment);
            }
        }

        public TimeSpan PlannedTime
        {
            get
            {
                return _plannedTime;
            }
            set
            {
                _plannedTime = value;
                RaisePropertyChanged(() => PlannedTime);
            }
        }

        public UniqBindableCollection<AttachFileView> Files
        {
            get
            {
                return _files;
            }
            set
            {
                _files = value;
                RaisePropertyChanged(() => Files);
            }
        }

        public MessagesCollectionView Messages
        {
            get
            {
                return _messages;
            }
            set
            {
                _messages = value;
                _messages.Messages.CollectionChanged += messageCollectionChangedHandler;
                _messages.HasUnreadChangedHandler += hasUnreadMessagesChangedHandler;
                RaisePropertyChanged(() => Messages);
            }
        }

        public Constants.TaskWorkState WorkState
        {
            get
            {
                return _workState;
            }
            set
            {
                _workState = value;
                RaisePropertyChanged(() => WorkState);
            }
        }

        public TimeSpan InProcessTime
        {
            get { return _inProcessTime; }
            set
            {
                _inProcessTime = value;
                RaisePropertyChanged(() => InProcessTime);
            }
        }

        #endregion

        public TaskView()
        {
            _title = "";
            _messages.Messages.CollectionChanged += messageCollectionChangedHandler;
            _messages.HasUnreadChangedHandler += hasUnreadMessagesChangedHandler;
        }

        private void hasUnreadMessagesChangedHandler(object sender, ContentEventArgs e)
        {
            InvokeHasUnreadMessagesChanged((bool) e.Arg);
        }

        private void messageCollectionChangedHandler(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                var lst = new List<MessageView>();
                foreach (var item in e.NewItems)
                {
                    var message = (MessageView) item;
                    lst.Add(message);
                }
                if(lst.Count > 0)
                {
                    InvokeNewMessageAdded(lst);
                }
            }
        }

        public override TaskDTO BuildDTO()
        {
            var dto = new TaskDTO
                          {
                              Id = Id,
                              CreationDate = _creationDate,
                              Customer = _customer.BuildDTO(),
                              Creator = _creator.BuildDTO(),
                              DeadLine = _deadLine,
                              PlannedTime = _plannedTime,
                              Description = _description,
                              Priority = _priority,
                              Status = base.Status,
                              Title = base.Title,
                          };

            if (_taskType != null)
                dto.TaskType = _taskType.BuildDTO();

            if (_currentWorker != null)
                dto.CurrentWorker = _currentWorker.BuildDTO();

            return dto;
        }

        private void InvokeNewMessageAdded(List<MessageView> newItems)
        {
            var handler = NewMessagesAddedHandler;
            if (handler != null) handler(null, new ContentEventArgs(newItems));
        }

        private void InvokeHasUnreadMessagesChanged(bool hasUnreaded)
        {
            var handler = HasUnreadMessagesChangedHandler;
            if (handler != null) handler(null, new ContentsEventArgs(new object[]{Id, hasUnreaded}));
        }

        public bool Equals(TaskView other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return base.Equals(other) && Equals(other._description, _description) && Equals(other._comment, _comment) &&
                   Equals(other._currentWorker, _currentWorker) && Equals(other._creator, _creator) &&
                   Equals(other._customer, _customer) && other._deadLine.Equals(_deadLine) &&
                   other._priority == _priority && other._plannedTime.Equals(_plannedTime) &&
                   Equals(other._files, _files) && other._creationDate.Equals(_creationDate) &&
                   Equals(other._messages, _messages) && Equals(other._workState, _workState);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return Equals(obj as TaskView);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int result = base.GetHashCode();
                result = (result*397) ^ (_description != null ? _description.GetHashCode() : 0);
                result = (result*397) ^ (_comment != null ? _comment.GetHashCode() : 0);
                result = (result*397) ^ (_currentWorker != null ? _currentWorker.GetHashCode() : 0);
                result = (result*397) ^ (_creator != null ? _creator.GetHashCode() : 0);
                result = (result*397) ^ (_customer != null ? _customer.GetHashCode() : 0);
                result = (result*397) ^ (_deadLine.HasValue ? _deadLine.Value.GetHashCode() : 0);
                result = (result*397) ^ _priority;
                result = (result*397) ^ _plannedTime.GetHashCode();
                result = (result*397) ^ (_files != null ? _files.GetHashCode() : 0);
                result = (result*397) ^ _creationDate.GetHashCode();
                result = (result*397) ^ (_messages != null ? _messages.GetHashCode() : 0);
                result = (result*397) ^ _workState.GetHashCode();
                return result;
            }
        }

        public static bool operator ==(TaskView left, TaskView right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(TaskView left, TaskView right)
        {
            return !Equals(left, right);
        }
    }
}