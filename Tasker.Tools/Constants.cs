﻿namespace Tasker.Tools
{
    public static class Constants
    {
        public const string LastVersionFolderName = "prev";
        public const int MessagePreviewSignCount = 30;

        public enum ShiftType
        {
            Weekday,
            Saturday,
            Sunday
        }

        public enum BaloonType
        {
            Nothing,
            Message,
            Task,
            Lunch,
            RSS
        }

        public enum OverlayState
        {
            Nothing,
            NewTask,
            Message,
            File,
            Task,
            Lunch,
            Rss
        }

        public enum UserWorkState
        {
            Free,
            BackgroundTask,
            Busy,
            NotAvailable            
        }

        public enum TaskWorkState
        {
            Available,
            CheckedOut,
            NotAvailable,
        }
    }
}
