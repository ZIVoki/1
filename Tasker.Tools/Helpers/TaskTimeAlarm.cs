﻿using System;
using System.Windows;

namespace Tasker.Tools.Helpers
{
    public class TaskTimeAlarm: Clock
    {
        private DateTime? endTime
        {
            get
            {
                if (StartTime.HasValue && AvailableTime.HasValue)
                    return StartTime.Value.Add(AvailableTime.Value);
                return null;
            }
        }

        public static DependencyProperty StartTimeProperty =
            DependencyProperty.Register("StartTime", typeof(DateTime?), typeof(TaskTimeAlarm));

        public static DependencyProperty AvailableTimeProperty =
            DependencyProperty.Register("AvailableTime", typeof(TimeSpan?), typeof(TaskTimeAlarm));

        public static DependencyProperty MinutesLeftProperty =
            DependencyProperty.Register("MinutesLeft", typeof(int), typeof(TaskTimeAlarm));

        public static DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(double), typeof(TaskTimeAlarm));

        public static DependencyProperty TimeLeftProperty =
            DependencyProperty.Register("TimeLeft", typeof(TimeSpan), typeof(TaskTimeAlarm));


        public DateTime? StartTime
        {
            get
            {
                return ((DateTime?)(this.GetValue(StartTimeProperty)));
            }
            set
            {
                this.SetValue(StartTimeProperty, value);
            }
        }

        public TimeSpan? AvailableTime
        {
            get
            {
                return ((TimeSpan?)(this.GetValue(AvailableTimeProperty)));
            }
            set
            {
                this.SetValue(AvailableTimeProperty, value);
            }
        }

        public double Value
        {
            get
            {
                return (double) this.GetValue(ValueProperty);
            }
            set
            {
                if (value != null) 
                    this.SetValue(ValueProperty, value);
            }
        }

        public int MinutesLeft
        {
            get
            {
                return (int) this.GetValue(MinutesLeftProperty);
            }
            set
            {
                if (value != null) 
                    this.SetValue(MinutesLeftProperty, value);
            }
        }

        public TimeSpan TimeLeft
        {
            get { return (TimeSpan) this.GetValue(TimeLeftProperty); }
            set
            {
                this.SetValue(TimeLeftProperty, value);
            }
        }

        protected override void AfterTick()
        {
            if (AvailableTime.HasValue && StartTime.HasValue)
            {
                TimeSpan lostTime = AvailableTime.Value.Subtract(endTime.Value.Subtract(DateTimeNow));
                double d = (lostTime.TotalSeconds/AvailableTime.Value.TotalSeconds)*100;

                if (d > 0)
                    Value = d;
                else
                    Value = 100;

                TimeSpan timeSpan = endTime.Value.Subtract(DateTimeNow);
                if (timeSpan.TotalSeconds >= 0)
                {
                    TimeLeft = timeSpan;
                    MinutesLeft = (int)TimeLeft.TotalMinutes;
                }
                else
                {
                    MinutesLeft = 0;
                    TimeLeft = TimeSpan.Zero;
                }
            }
            else
            {
                Value = 0;
                MinutesLeft = 0;
                TimeLeft = TimeSpan.Zero;
            }
        }
    }
}
