﻿using System;
using System.Windows;
using System.Windows.Threading;

namespace Tasker.Tools.Helpers
{
    public class Clock : DependencyObject
    {
        public static DependencyProperty DateTimeProperty =
            DependencyProperty.Register("DateTimeNow", typeof(DateTime), typeof(Clock));

        public Clock()
        {
            DateTimeNow = DateTime.Now;
            DispatcherTimer timer = new DispatcherTimer();
            timer.Tick += TimerOnTick;
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Start();
        }

        public DateTime DateTimeNow
        {
            get
            {
                return ((DateTime)(this.GetValue(DateTimeProperty)));
            }
            set
            {
                this.SetValue(DateTimeProperty, value);
            }
        }

        private void TimerOnTick(object sender, EventArgs args)
        {
            DateTimeNow = DateTime.Now;
            AfterTick();
        }

        protected virtual void AfterTick()
        {
        }
    }

}
