﻿using System;
using System.Windows;

namespace Tasker.Tools.Helpers
{
    public class DinnerTimeAlarm: Clock
    {
        public DateTime? StartTime
        {
            get; set;
        }

        public TimeSpan? DinnerTime
        {
            get; set;
        }

        public static DependencyProperty LateProperty = DependencyProperty.Register("Late", typeof(bool), typeof(DinnerTimeAlarm));
        public static DependencyProperty TimeLeftProperty = DependencyProperty.Register("TimeLeft", typeof(TimeSpan), typeof(DinnerTimeAlarm));

        private DateTime? EndTime 
        {
            get
            {
                if (StartTime.HasValue && DinnerTime.HasValue)
                    return StartTime.Value.Add(DinnerTime.Value);
                else
                    return null;
            }
        }

        public bool Late
        {
            get
            {
                return (bool) this.GetValue(LateProperty);
            }
            set
            {
                this.SetValue(LateProperty, value);
            }
        }

        public TimeSpan TimeLeft
        {
            get
            {
                return (TimeSpan) this.GetValue(TimeLeftProperty);
            }
            set
            {
                this.SetValue(TimeLeftProperty, value);
            }
        }

        protected override void AfterTick()
        {
            if (!EndTime.HasValue) 
            { 
                TimeLeft = new TimeSpan(0, 0, 0, 0);
                Late = false; 
                return;
            }

            if (EndTime >= DateTimeNow)
            {
                Late = false;
                TimeLeft = EndTime.Value.Subtract(DateTimeNow);
            }
            else
            {
                Late = true;
                TimeLeft = DateTimeNow.Subtract(EndTime.Value);
            }
        }
    }
}
