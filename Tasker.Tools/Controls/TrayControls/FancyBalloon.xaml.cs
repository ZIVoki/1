﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using Hardcodet.Wpf.TaskbarNotification;
using Tasker.Tools.CustomEventArgs;

namespace Tasker.Tools.Controls.TrayControls
{
    public delegate void ArgumentContainsDelegate(object sender, ContentEventArgs e);
    /// <summary>
    /// Interaction logic for FancyBalloon.xaml
    /// </summary>
    public partial class FancyBalloon : UserControl
    {
        private bool _isClosing = false;
        private readonly Constants.BaloonType _currentType;
        public Action OnClickAction { get; set; }

        #region BalloonText dependency property

        /// <summary>
        /// Description
        /// </summary>
        public static readonly DependencyProperty BalloonTextProperty =
            DependencyProperty.Register("BalloonText",
                                        typeof(string),
                                        typeof(FancyBalloon),
                                        new FrameworkPropertyMetadata(""));

        /// <summary>
        /// Title
        /// </summary>
        public static readonly DependencyProperty BalloonTitleProperty =
            DependencyProperty.Register("BalloonTitle",
                                        typeof(string),
                                        typeof(FancyBalloon),
                                        new FrameworkPropertyMetadata(""));

        /// <summary>
        /// A property wrapper for the <see cref="BalloonTextProperty"/>
        /// dependency property:<br/>
        /// Description
        /// </summary>
        public string BalloonText
        {
            get { return (string)GetValue(BalloonTextProperty); }
            set { SetValue(BalloonTextProperty, value); }
        }

        /// <summary>
        /// A property wrapper for the <see cref="BalloonTextProperty"/>
        /// dependency property:<br/>
        /// Description
        /// </summary>
        public string BalloonTitle
        {
            get { return (string)GetValue(BalloonTitleProperty); }
            set { SetValue(BalloonTitleProperty, value); }
        }

        #endregion

        public FancyBalloon(Constants.BaloonType type)
        {
            InitializeComponent();
            this._currentType = type;
            switch (_currentType)
            {
                case Constants.BaloonType.Message:
                    imgIcon.Source = Resources["imgMessage"] as ImageSource;
                    break;
                case Constants.BaloonType.Lunch:
                    imgIcon.Source = Resources["imgLunch"] as ImageSource;
                    break;
                case Constants.BaloonType.Task:
                    imgIcon.Source = Resources["imgTask"] as ImageSource;
                    break;
                case Constants.BaloonType.RSS:
                    imgIcon.Source = Resources["imgRss"] as ImageSource;
                    break;
                case Constants.BaloonType.Nothing:
                    imgIcon.Source = null;
                    break;
            }
            TaskbarIcon.AddBalloonClosingHandler(this, OnBalloonClosing);
            this.MouseRightButtonUp += balloon_MouseRightClick;
            this.MouseLeftButtonUp += balloon_MouseLeftClick;
        }

        private void balloon_MouseLeftClick(object sender, MouseButtonEventArgs e)
        {
            //send 2 = index of message tab
            var taskbarIcon = TaskbarIcon.GetParentTaskbarIcon(this);
            taskbarIcon.CloseBalloon();
        }

        private void balloon_MouseRightClick(object sender, MouseButtonEventArgs e)
        {
            //close balloon
            var taskbarIcon = TaskbarIcon.GetParentTaskbarIcon(this);
            taskbarIcon.CloseBalloon();
        }

        /// <summary>
        /// By subscribing to the <see cref="TaskbarIcon.BalloonClosingEvent"/>
        /// and setting the "Handled" property to true, we suppress the popup
        /// from being closed in order to display the fade-out animation.
        /// </summary>
        private void OnBalloonClosing(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            _isClosing = true;
        }

        private bool _forceClose;
        /// <summary>
        /// Resolves the <see cref="TaskbarIcon"/> that displayed
        /// the balloon and requests a close action.
        /// </summary>
        private void imgClose_MouseDown(object sender, MouseButtonEventArgs e)
        {
            _forceClose = true;
            //the tray icon assigned this attached property to simplify access
            TaskbarIcon taskbarIcon = TaskbarIcon.GetParentTaskbarIcon(this);
            taskbarIcon.CloseBalloon();
        }

        /// <summary>
        /// If the users hovers over the balloon, we don't close it.
        /// </summary>
        private void grid_MouseEnter(object sender, MouseEventArgs e)
        {
            //if we're already running the fade-out animation, do not interrupt anymore
            //(makes things too complicated for the sample)
            if (_isClosing) return;

            //the tray icon assigned this attached property to simplify access
            TaskbarIcon taskbarIcon = TaskbarIcon.GetParentTaskbarIcon(this);
            taskbarIcon.ResetBalloonCloseTimer();
        }


        /// <summary>
        /// Closes the popup once the fade-out animation completed.
        /// The animation was triggered in XAML through the attached
        /// BalloonClosing event.
        /// </summary>
        private void OnFadeOutCompleted(object sender, EventArgs e)
        {
            var pp = (Popup)Parent;
            pp.IsOpen = false;
        }

        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            base.OnMouseUp(e);
            if (_forceClose) return;
            if (OnClickAction != null)
                OnClickAction.Invoke();
        }
    }
}
