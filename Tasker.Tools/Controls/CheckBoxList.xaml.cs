﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;

namespace Tasker.Tools.Controls
{
    /// <summary>
    /// Interaction logic for CheckBoxList.xaml
    /// </summary>
    public partial class CheckBoxList : UserControl
    {
        public CheckBoxList()
        {
            InitializeComponent();
        }

        public Object ItemsSource
        {
            get { return GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ItemSource.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register("ItemsSource", typeof(Object), typeof(CheckBoxList),
                                        new UIPropertyMetadata(null, (sender, args) => Debug.WriteLine(args)));

        public IList SelectedItems
        {
            get { return (IList)GetValue(SelectedItemsProperty); }
            set { SetValue(SelectedItemsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedItems.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedItemsProperty =
            DependencyProperty.Register("SelectedItems", typeof(IList), typeof(CheckBoxList),
                                        new UIPropertyMetadata(null, SelectedChanged));

        /// <summary>
        /// This is called when selected property changed.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="args"></param>
        private static void SelectedChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            INotifyCollectionChanged ncc = args.NewValue as INotifyCollectionChanged;
            if (ncc != null)
            {
                ncc.CollectionChanged += (sender, e) =>
                {
                    CheckBoxList thiscontrol = (CheckBoxList)obj;
                    RebindAllCheckbox(thiscontrol.host);
                };
            }
        }

        private static void RebindAllCheckbox(DependencyObject de)
        {
            try
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(de); i++)
                {
                    DependencyObject dobj = VisualTreeHelper.GetChild(de, i);
                    if (dobj is CheckBox)
                    {
                        CheckBox cb = (CheckBox)dobj;
                        var bexpression = BindingOperations.GetMultiBindingExpression(cb, MyCheckBox.IsCheckedProperty);
                        if (bexpression != null) bexpression.UpdateTarget();
                    }
                    RebindAllCheckbox(dobj);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public String DisplayPropertyPath
        {
            get { return (String)GetValue(DisplayPropertyPathProperty); }
            set { SetValue(DisplayPropertyPathProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DisplayPropertyPath.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DisplayPropertyPathProperty =
            DependencyProperty.Register("DisplayPropertyPath", typeof(String), typeof(CheckBoxList),
                                        new UIPropertyMetadata("", (sender, args) => Debug.WriteLine(args)));

        private PropertyInfo DisplayPropertyPathPropertyInfo;

        public void SelectAll()
        {
            foreach (object related in (IEnumerable)ItemsSource)
            {
                if (DisplayPropertyPathPropertyInfo == null)
                {
                    DisplayPropertyPathPropertyInfo =
                        related.GetType().GetProperty(DisplayPropertyPath, BindingFlags.Instance | BindingFlags.Public);
                }

                Object propertyValue;
                if (DisplayPropertyPath == ".")
                    propertyValue = related;
                else
                    propertyValue = DisplayPropertyPathPropertyInfo.GetValue(related, null); 
                
                if (!SelectedItems.Cast<Object>()
                         .Any(o => propertyValue.Equals(
                             DisplayPropertyPath == "." ? o : DisplayPropertyPathPropertyInfo.GetValue(o, null))))
                {
                    SelectedItems.Add(related);
                }
            }
        }

        public void UnselectAll()
        {
            foreach (object related in (IEnumerable)ItemsSource)
            {
                if (DisplayPropertyPathPropertyInfo == null)
                {
                    DisplayPropertyPathPropertyInfo =
                        related.GetType().GetProperty(DisplayPropertyPath, BindingFlags.Instance | BindingFlags.Public);
                }

                Object propertyValue;
                if (DisplayPropertyPath == ".")
                    propertyValue = related;
                else
                    propertyValue = DisplayPropertyPathPropertyInfo.GetValue(related, null);

                Object toDeselect = SelectedItems.Cast<Object>()
                    .Where(o => propertyValue.Equals(DisplayPropertyPath == "." ? o : DisplayPropertyPathPropertyInfo.GetValue(o, null)))
                    .FirstOrDefault();
                if (toDeselect != null)
                {
                    SelectedItems.Remove(toDeselect);
                }
            }

        }

        private void MyCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            if (SelectedItems == null)
                return;

            MyCheckBox chb = (MyCheckBox)sender;
            Object related = chb.Tag;
            if (DisplayPropertyPathPropertyInfo == null)
            {

                DisplayPropertyPathPropertyInfo =
                    related.GetType().GetProperty(
                        DisplayPropertyPath, BindingFlags.Instance | BindingFlags.Public);
            }

            Object propertyValue;
            if (DisplayPropertyPath == ".")
                propertyValue = related;
            else
                propertyValue = DisplayPropertyPathPropertyInfo.GetValue(related, null);

            if (chb.IsChecked == true)
            {
                //Debbo inserire l'elemento nella lista, sempre che non ci sia già
                if (!SelectedItems.Cast<Object>()
                        .Any(o => propertyValue.Equals(
                                    DisplayPropertyPath == "." ? o : DisplayPropertyPathPropertyInfo.GetValue(o, null))))
                {
                    SelectedItems.Add(related);
                }
            }
            else
            {
                Object toDeselect = SelectedItems.Cast<Object>()
                    .Where(o => propertyValue.Equals(DisplayPropertyPath == "." ? o : DisplayPropertyPathPropertyInfo.GetValue(o, null)))
                    .FirstOrDefault();
                if (toDeselect != null)
                {
                    SelectedItems.Remove(toDeselect);
                }
            }
        }
    }

    public class MyCheckBox : CheckBox
    {
        public String DisplayMemberPath
        {
            get { return (String)GetValue(DisplayMemberPathProperty); }
            set { SetValue(DisplayMemberPathProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DisplayMemberPath.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DisplayMemberPathProperty =
             DependencyProperty.Register("DisplayMemberPath",
             typeof(String),
             typeof(MyCheckBox),
             new UIPropertyMetadata(String.Empty, (sender, args) =>
             {
                 MyCheckBox item = (MyCheckBox)sender;
                 Binding contentBinding = new Binding((String)args.NewValue);
                 item.SetBinding(ContentProperty, contentBinding);
             }));
    }

    public class IsCheckedValueConverter : IMultiValueConverter
    {
        private PropertyInfo PropertyInfo { get; set; }
        private Type ObjectType { get; set; }

        #region IMultiValueConverter Members

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values[1] == null) return false; //IF I do not have no value for selected simply return false

            String PropertyName = values[2] as string;
            if (PropertyName == null) return false;

            if (String.IsNullOrEmpty(PropertyName)) return false;
            if (!targetType.IsAssignableFrom(typeof(Boolean))) throw new NotSupportedException("Can convert only to boolean");
            IEnumerable collection = values[1] as IEnumerable;
            Object value = values[0];
            if (value.GetType() != ObjectType)
            {
                PropertyInfo = value.GetType().GetProperty(PropertyName, BindingFlags.Instance | BindingFlags.Public);
                ObjectType = value.GetType();
            }
            foreach (var obj in collection)
            {
                if (PropertyName == ".")
                {
                    if (value.Equals(obj)) return true;
                }
                else
                {
                    if (PropertyInfo.GetValue(value, null).Equals(PropertyInfo.GetValue(obj, null))) return true;
                }

            }
            return false;
        }

        /// <summary>
        /// Questa funzione viene chiamata quando voglio convertire indietro dal controllo all'oggetto di binding.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetTypes"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

}
