﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Tasker.Tools.Controls
{
    /// <summary>
    /// Interaction logic for BaseLoginControl.xaml
    /// </summary>
    public partial class  BaseLoginControl : UserControl
    {
        public static readonly DependencyProperty DefaultLoginProperty = DependencyProperty.Register("LoginTxt", typeof (string), typeof (BaseLoginControl), new PropertyMetadata(default(string)));
        public static readonly DependencyProperty PasswordProperty = DependencyProperty.Register("Password", typeof (string), typeof (BaseLoginControl), new PropertyMetadata(default(string)));
        public static readonly DependencyProperty LoginCmdProperty = DependencyProperty.Register("LoginCmd", typeof (ICommand), typeof (BaseLoginControl), new PropertyMetadata(default(ICommand)));
        public static readonly DependencyProperty NetTcpAddressProperty = DependencyProperty.Register("NetTcpAddress", typeof(string), typeof(BaseLoginControl), new PropertyMetadata(default(string)));
        public static readonly DependencyProperty SaveSettingsCommandProperty = DependencyProperty.Register("SaveSettingsCommand", typeof (ICommand), typeof (BaseLoginControl), new PropertyMetadata(default(ICommand)));
        public static readonly DependencyProperty TitleProperty = DependencyProperty.Register("Title", typeof (string), typeof (BaseLoginControl), new PropertyMetadata(default(string)));
        public static readonly DependencyProperty VersionNameProperty = DependencyProperty.Register("VersionName", typeof (string), typeof (BaseLoginControl), new PropertyMetadata(default(string)));
        public static readonly DependencyProperty SystemMessageContentProperty = DependencyProperty.Register("SystemMessageContent", typeof (string), typeof (BaseLoginControl), new PropertyMetadata(default(string)));
        public static readonly DependencyProperty MessageVisibilityProperty = DependencyProperty.Register("MessageVisibility", typeof (Visibility), typeof (BaseLoginControl), new PropertyMetadata(default(Visibility)));
        public static readonly DependencyProperty LoadingVisibilityProperty = DependencyProperty.Register("LoadingVisibility", typeof (Visibility), typeof (BaseLoginControl), new PropertyMetadata(default(Visibility)));
        public static readonly DependencyProperty IaAutoLogoffProperty = DependencyProperty.Register("IaAutoLogin", typeof (bool?), typeof (BaseLoginControl), new PropertyMetadata(default(bool?)));
        public static readonly DependencyProperty IsDoaminUserProperty = DependencyProperty.Register("IsDoaminUser", typeof (bool), typeof (BaseLoginControl), new PropertyMetadata(default(bool)));

        public BaseLoginControl()
        {
            InitializeComponent();
            LoadingVisibility = Visibility.Hidden;
        }

        public string LoginTxt
        {
            get { return (string) GetValue(DefaultLoginProperty); }
            set
            {
                SetValue(DefaultLoginProperty, value);
            }
        }

        public string Password
        {
            get { return (string) GetValue(PasswordProperty); }
            set
            {
                SetValue(PasswordProperty, value);
            }
        }

        public string NetTcpAddress
        {
            get { return (string)GetValue(NetTcpAddressProperty); }
            set
            {
                SetValue(NetTcpAddressProperty, value);
            }
        }

        public string Title
        {
            get { return (string) GetValue(TitleProperty); }
            set
            {
                SetValue(TitleProperty, value);
            }
        }

        public string VersionName
        {
            get { return (string) GetValue(VersionNameProperty); }
            set
            {
                SetValue(VersionNameProperty, value);
            }
        }

        public string SystemMessageContent
        {
            get { return (string) GetValue(SystemMessageContentProperty); }
            set
            {
                SetValue(SystemMessageContentProperty, value);
            }
        }

        public Visibility MessageVisibility
        {
            get { return (Visibility) GetValue(MessageVisibilityProperty); }
            set
            {
                SetValue(MessageVisibilityProperty, value);
            }
        }

        public Visibility LoadingVisibility
        {
            get { return (Visibility) GetValue(LoadingVisibilityProperty); }
            set
            {
                SetValue(LoadingVisibilityProperty, value);
            }
        }

        public ICommand SaveSettingsCommand
        {
            get { return (ICommand) GetValue(SaveSettingsCommandProperty); }
            set
            {
                SetValue(SaveSettingsCommandProperty, value);
            }
        }

        public ICommand LoginCmd
        {
            get { return (ICommand) GetValue(LoginCmdProperty); }
            set
            {
                SetValue(LoginCmdProperty, value);
            }
        }

        public bool? IaAutoLogin
        {
            get { return (bool?) GetValue(IaAutoLogoffProperty); }
            set { SetValue(IaAutoLogoffProperty, value); }
        }

        public bool IsDoaminUser
        {
            get { return (bool) GetValue(IsDoaminUserProperty); }
            set { SetValue(IsDoaminUserProperty, value); }
        }

        private void TextBoxGotFocus(object sender, RoutedEventArgs e)
        {
            TextBox box = sender as TextBox;
            if (box != null)
            {
                box.SelectAll();
            }
        }

        private void OpenOptionsButtonClick(object sender, RoutedEventArgs e)
        {
            MainTabControl.SelectedItem = OptionsTabItem;
        }

        private void SaveOptionsButtonOnClick(object sender, RoutedEventArgs e)
        {
            MainTabControl.SelectedItem = LoginTabItem;
        }

        private void CancelButton_OnClick(object sender, RoutedEventArgs e)
        {
            MainTabControl.SelectedItem = LoginTabItem;
        }
    }

    public static class PasswordHelper
    {
        public static readonly DependencyProperty PasswordProperty =
            DependencyProperty.RegisterAttached("Password",
            typeof(string), typeof(PasswordHelper),
            new FrameworkPropertyMetadata(string.Empty, OnPasswordPropertyChanged));

        public static readonly DependencyProperty AttachProperty =
            DependencyProperty.RegisterAttached("Attach",
            typeof(bool), typeof(PasswordHelper), new PropertyMetadata(false, Attach));

        private static readonly DependencyProperty IsUpdatingProperty =
           DependencyProperty.RegisterAttached("IsUpdating", typeof(bool),
           typeof(PasswordHelper));


        public static void SetAttach(DependencyObject dp, bool value)
        {
            dp.SetValue(AttachProperty, value);
        }

        public static bool GetAttach(DependencyObject dp)
        {
            return (bool)dp.GetValue(AttachProperty);
        }

        public static string GetPassword(DependencyObject dp)
        {
            return (string)dp.GetValue(PasswordProperty);
        }

        public static void SetPassword(DependencyObject dp, string value)
        {
            dp.SetValue(PasswordProperty, value);
        }

        private static bool GetIsUpdating(DependencyObject dp)
        {
            return (bool)dp.GetValue(IsUpdatingProperty);
        }

        private static void SetIsUpdating(DependencyObject dp, bool value)
        {
            dp.SetValue(IsUpdatingProperty, value);
        }

        private static void OnPasswordPropertyChanged(DependencyObject sender,
            DependencyPropertyChangedEventArgs e)
        {
            PasswordBox passwordBox = sender as PasswordBox;
            if (passwordBox != null)
            {
                passwordBox.PasswordChanged -= PasswordChanged;

                if (!(bool)GetIsUpdating(passwordBox))
                {
                    passwordBox.Password = (string)e.NewValue;
                }
                passwordBox.PasswordChanged += PasswordChanged;
            }
        }

        private static void Attach(DependencyObject sender,
            DependencyPropertyChangedEventArgs e)
        {
            PasswordBox passwordBox = sender as PasswordBox;

            if (passwordBox == null)
                return;

            if ((bool)e.OldValue)
            {
                passwordBox.PasswordChanged -= PasswordChanged;
            }

            if ((bool)e.NewValue)
            {
                passwordBox.PasswordChanged += PasswordChanged;
            }
        }

        private static void PasswordChanged(object sender, RoutedEventArgs e)
        {
            PasswordBox passwordBox = sender as PasswordBox;
            SetIsUpdating(passwordBox, true);
            if (passwordBox != null)
            {
                SetPassword(passwordBox, value: passwordBox.Password);
                SetIsUpdating(passwordBox, false);
            }
        }
    }
}
