﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using DataFormats = System.Windows.DataFormats;
using DragEventArgs = System.Windows.DragEventArgs;
using MessageBox = System.Windows.MessageBox;
using UserControl = System.Windows.Controls.UserControl;

namespace Tasker.Tools.Controls
{
    public delegate void AddNewFilePathEventHandler(object sender, AddNewFilePathEventArgs e);

    public class AddNewFilePathEventArgs: EventArgs
    {
        public AddNewFilePathEventArgs(string[] filePath)
        {
            FilePath = filePath;
        }

        public string[] FilePath { get; set; } 
    }

    /// <summary>
    /// Interaction logic for NewFilesCatcher.xaml
    /// </summary>
    public partial class NewFilesCatcher : UserControl, ICommandSource
    {
        private OpenFileDialog _browserDialog;
        public event AddNewFilePathEventHandler AddNewFilePathEvent;

        public void InvokeAddNewFilePathEvent(AddNewFilePathEventArgs e)
        {
            AddNewFilePathEventHandler handler = AddNewFilePathEvent;
            if (handler != null) handler(this, e);

            RaiseCommand(e.FilePath);
        }

        public NewFilesCatcher()
        {
            InitializeComponent();
        }

        private OpenFileDialog BrowserDialog
        {
            get
            {
                if (_browserDialog == null)
                {
                    _browserDialog = new OpenFileDialog
                                         {
                                             Title = "Выберите файл",
                                             Multiselect = true
                                         };
                }
                return _browserDialog;
            }
        }

        private void addNewFileButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult dialogResult = BrowserDialog.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                AddNewFiles(BrowserDialog.FileNames);
            }
        }
        
        private void resultMoveGroupBox_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                //Do work. Can special-case logic based on Copy, Move, etc. 
                string[] fileNames = e.Data.GetData(DataFormats.FileDrop, true) as string[];

                if (fileNames != null) AddNewFiles(fileNames);
            }

            e.Handled = true; 
        }

        private void AddNewFiles(IEnumerable<string> filePathes)
        {
            foreach (string filePath in filePathes)
            {
                if (string.IsNullOrEmpty(filePath)) continue;

                // check that this is server disk
                string machineName = string.Format(@"\\{0}", Environment.MachineName);
                string doubleSlash = filePath.Substring(0, 2);
                string substring = filePath.Substring(0, machineName.Length);

#if DEBUG
                InvokeAddNewFilePathEvent(new AddNewFilePathEventArgs(new[] { filePath }));
#else
                if (doubleSlash == @"\\" && substring != machineName)
                {
                    InvokeAddNewFilePathEvent(new AddNewFilePathEventArgs(new[] { filePath }));
                    continue;
                }
                string diskName = filePath.Substring(0, 3);
                if (NetworkDriveCheck(diskName))
                {
                    InvokeAddNewFilePathEvent(new AddNewFilePathEventArgs(new[] { filePath }));
                    continue;
                }

                MessageBox.Show("Файл с локальной машины не может быть прикреплен к задаче.",
                                               "Прикрепление локального файла.", MessageBoxButton.OK,
                                               MessageBoxImage.Asterisk);
#endif

            }
        }

        private static bool NetworkDriveCheck(string diskName)
        {
            foreach (var driveInfo in DriveInfo.GetDrives())
            {
                if (diskName == driveInfo.Name && 
                    driveInfo.DriveType == DriveType.Network)
                    return true;
            }
            return false;
        }

        private void UIElement_OnDrop(object sender, DragEventArgs e)
        {
            resultMoveGroupBox_Drop(sender, e);
        }


        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        public static readonly DependencyProperty CommandProperty =
            DependencyProperty.Register("Command", typeof(ICommand), typeof(NewFilesCatcher), new UIPropertyMetadata(null));


        public object CommandParameter
        {
            get { return (object)GetValue(CommandParameterProperty); }
            set { SetValue(CommandParameterProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CommandParameter.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CommandParameterProperty =
            DependencyProperty.Register("CommandParameter", typeof(object), typeof(NewFilesCatcher), new UIPropertyMetadata(null));

        public IInputElement CommandTarget
        {
            get { return (IInputElement)GetValue(CommandTargetProperty); }
            set { SetValue(CommandTargetProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CommandTarget.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CommandTargetProperty =
            DependencyProperty.Register("CommandTarget", typeof(IInputElement), typeof(NewFilesCatcher), new UIPropertyMetadata(null));

        private void RaiseCommand()
        {
            object parameter = CommandParameter;
            
            RaiseCommand(parameter);
        }

        private void RaiseCommand(object parameter)
        {
            ICommand command = Command;
            IInputElement target = CommandTarget;

            var routedCmd = command as RoutedCommand;
            if (routedCmd != null && routedCmd.CanExecute(parameter, target))
            {
                routedCmd.Execute(parameter, target);
            }
            else if (command != null && command.CanExecute(parameter))
            {
                command.Execute(parameter);
            }
        }
    }
}
