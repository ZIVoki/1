﻿using System.Windows;
using System.Windows.Controls;

namespace Tasker.Tools.Controls
{
    /// <summary>
    /// Interaction logic for ColoredHyperlink.xaml
    /// </summary>
    public partial class ColoredHyperlink : UserControl
    {
        public static DependencyProperty LabelProperty = DependencyProperty.Register("Label", typeof(string), typeof(ColoredHyperlink));

        public ColoredHyperlink()
        {
            InitializeComponent();
        }

        public RoutedEventHandler ClickHandler
        {
            set
            {
                if (value != null)
                {
                    LklCreateNewDepartment.Click += value;
                }
            }
        }


        public string Label
        {
            get
            {
                return (string)GetValue(LabelProperty);
            }
            set
            {
                SetValue(LabelProperty, value);
            }

        }
    }
}
