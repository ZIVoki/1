﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Tasker.Tools.CustomEventArgs;
using Tasker.Tools.ViewModel.Base;

namespace Tasker.Tools.Controls
{
    /// <summary>
    /// Interaction logic for MessageControl.xaml
    /// </summary>
    public delegate void ArgumentContainsDelegate(object sender, ContentEventArgs e);
    public partial class MessageControl : UserControl
    {
        public MessageControl()
        {
            InitializeComponent();
            MainScrollViewer.ScrollToBottom();
            KeyBinding newKeyBinding = new KeyBinding()
                {
                    Command = new RelayCommand(() =>  InvokeSendMessageButtonClick(TxtNewMessageTextBox.Text)),
                    Key = Key.Enter
                };
            TxtNewMessageTextBox.InputBindings.Add(newKeyBinding);
        }

        private bool _isActiveChat;
        public bool IsActiveChat
        {
            get { return _isActiveChat; }
            set 
            { 
                _isActiveChat = value;
                TxtNewMessageTextBox.IsEnabled = _isActiveChat;
                BtnMendMessageButton.IsEnabled = _isActiveChat;
            }
        }

        private void NewMessageTextBoxGotFocus(object sender, RoutedEventArgs e)
        {
            TxtNewMessageTextBox.Text = string.Empty;
        }

        private void txtNewMessageTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
            {
                InvokeSendMessageButtonClick(TxtNewMessageTextBox.Text);
            }
        }

        public event ArgumentContainsDelegate SendMessageButtonClick;
        public void InvokeSendMessageButtonClick(string e)
        {
            TxtNewMessageTextBox.Text = string.Empty;
            ArgumentContainsDelegate handler = SendMessageButtonClick;
            if (handler != null) handler(null, new ContentEventArgs(e));
        }

        private void sendMessageButton_Click(object sender, RoutedEventArgs e)
        {
            InvokeSendMessageButtonClick(TxtNewMessageTextBox.Text);
        }
    }
}
