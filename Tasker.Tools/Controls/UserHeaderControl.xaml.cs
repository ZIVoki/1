﻿using System.Windows;
using System.Windows.Controls;

namespace Tasker.Tools.Controls
{
    public delegate void LogoffEventHandler(object sender);

    /// <summary>
    /// Interaction logic for UserHeaderControl.xaml
    /// </summary>
    public partial class UserHeaderControl : UserControl
    {
        public UserHeaderControl()
        {
            InitializeComponent();
        }

        public event LogoffEventHandler OnLogoffEventHandler;

        private void InvokeLogoffEventHandler()
        {
            LogoffEventHandler handler = OnLogoffEventHandler;
            if (handler != null) handler(this);
        }

        private void logOffButton_Click(object sender, RoutedEventArgs e)
        {
            InvokeLogoffEventHandler();
        }
    }
}
