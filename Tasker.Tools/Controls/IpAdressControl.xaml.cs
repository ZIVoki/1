﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Tasker.Tools.Controls
{
    /// <summary>
    /// Interaction logic for IpAdressControl.xaml
    /// </summary>
    public partial class IpAdressControl : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public string Ip 
        {
            get {
                return string.Format("{0}.{1}.{2}.{3}", ipPart1TextBox.Text, 
                    ipPart2TextBox.Text, ipPart3TextBox.Text,
                    ipPart4TextBox.Text); }
            set
            {
                string[] elements = value.Split(new[] { '.' });
                if (elements.Length == 4)
                {
                    uint parse;
                    if (uint.TryParse(elements[0], out parse))
                        ipPart1TextBox.Text = elements[0];
                    if (uint.TryParse(elements[1], out parse))
                        ipPart2TextBox.Text = elements[1];
                    if (uint.TryParse(elements[2], out parse))
                        ipPart3TextBox.Text = elements[2];
                    if (uint.TryParse(elements[3], out parse))
                        ipPart4TextBox.Text = elements[3];
                }
            }
        }

        public IpAdressControl()
        {
            InitializeComponent();
        }

        private void ipPart1TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (!System.Text.RegularExpressions.Regex.IsMatch(e.Key.ToString(), "\\d+"))
            {
                
                    e.Handled = true;
            }
            else
            {
                uint isNumber;
                uint.TryParse((sender as TextBox).Text + e.Key, out isNumber);
                if (isNumber > 256)
                    e.Handled = true;
            }
        }

        private void UserControl_KeyUp(object sender, KeyEventArgs e)
        {
/*
            if (e.Key == Key.Left)
                MoveFocus(new TraversalRequest(FocusNavigationDirection.Previous));
            if (e.Key == Key.Right)
                MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
*/
        }

        
    }
}
