﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using Tasker.Tools.ViewModel;

namespace Tasker.Tools.Controls
{
    /// <summary>
    /// Interaction logic for AttachedFileListControl.xaml
    /// </summary>
    public partial class AttachedFileListControl : UserControl
    {
        public AttachedFileListControl()
        {
            InitializeComponent();
        }

        private void lklShowFile_Click(object sender, RoutedEventArgs e)
        {
            var fileData = (AttachFileView)((Hyperlink)sender).DataContext;

            var fi = new FileInfo(fileData.FilePath);
            try
            {
                Process.Start(fi.FullName);
            }
            catch (Exception exception)
            {
                if (fi.Directory != null) ShowFileInFolder(fileData.FilePath);
            }
        }

        private static void ShowFileInFolder(string filePath)
        {
            if (!string.IsNullOrEmpty(filePath))
            {
                string fileArg = string.Format(@"/select,{0}", filePath);
                Process.Start("explorer.exe", fileArg);
            }
        }

        private void Image_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            AttachFileView attachFile = (AttachFileView) ((Image) sender).DataContext;
            if (attachFile == null) return;

            string filePath = attachFile.FilePath;
            ShowFileInFolder(filePath);
        }
    }
}
