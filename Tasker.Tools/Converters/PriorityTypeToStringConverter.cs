﻿using System;
using System.Globalization;
using System.Windows.Data;
using Tasker.Tools.ViewModel;

namespace Tasker.Tools.Converters
{
    public class PriorityTypeToStringConverter: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is PriorityType)
            {
                PriorityType currentPriorityType = (PriorityType) value;
                switch (currentPriorityType)
                {
                    case PriorityType.Background:
                        return "Фоновая";
                    case PriorityType.Quick:
                        return "Срочная";
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            return string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}