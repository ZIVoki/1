﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Tasker.Tools.Converters
{
    public class TimeSpanToStringConverter : IValueConverter
    {
//        private const string defaultResult = null;

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return null;
            if (value is TimeSpan)
            {
                TimeSpan span = (TimeSpan) value;
                if (span == TimeSpan.Zero) return null;

                string result = string.Format("{0}{1}{2}",
                                                 span.Days > 0 ? string.Format("{0:0} {1} ", span.Days, DigitToSpellConverter.GetDaysSpell(span.Days)) : string.Empty,
                                                 span.Hours > 0 ? string.Format("{0:0} {1} ", span.Hours, DigitToSpellConverter.GetHoursSpell(span.Hours)) : string.Empty,
                                                 span.Minutes > 0 ? string.Format("{0:0} {1} ", span.Minutes, DigitToSpellConverter.GetMinutesSpell(span.Minutes)) : string.Empty).Trim();

                if (String.IsNullOrEmpty(result))
                    result = string.Format("{0}",
                                           span.Seconds > 0
                                               ? string.Format("{0:0} {1}", span.Seconds,
                                                               DigitToSpellConverter.GetSecondsSpell(span.Seconds))
                                               : string.Empty);
                
                return result;
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
