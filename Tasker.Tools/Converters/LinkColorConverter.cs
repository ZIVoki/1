﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace Tasker.Tools.Converters
{
    public class LinkColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var isenabled = (bool)value;

            if (isenabled)
            {
                return new SolidColorBrush(Colors.Blue);
            }
            else
            {
                return new SolidColorBrush(Colors.Gray);
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}