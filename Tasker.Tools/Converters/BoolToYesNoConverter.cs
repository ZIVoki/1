﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Tasker.Tools.Converters
{
    public class BoolToYesNoConverter: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool)
            {
                if ((bool) value)
                    return "Да";
                return "Нет";
            }
            return "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}