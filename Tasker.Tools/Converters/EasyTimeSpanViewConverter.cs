﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Tasker.Tools.Converters
{
    public class EasyTimeSpanViewConverter: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return "NaN";
            if (value is TimeSpan)
            {
                TimeSpan timeSpan = (TimeSpan) value;

                if (timeSpan == new TimeSpan(0)) return "NaN";

                string result = string.Format("{0}:{1}:{2}", timeSpan.Hours.ToString("00"),
                                              timeSpan.Minutes.ToString("00"), timeSpan.Seconds.ToString("00"));
                if (timeSpan.Days > 0) result = string.Format("{0}.{1}", timeSpan.Days, result);
                return result;
            }
            return "Nan";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
