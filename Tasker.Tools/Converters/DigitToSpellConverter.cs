using System;

namespace Tasker.Tools.Converters
{
    /// <summary>
    /// ���������� ������ ������������  ������������ (������, ������, �����)
    /// </summary>
    public class DigitToSpellConverter
    {
        enum DigitSpellModif { One, FromTwoToFour, More }
        
        public static string GetMinutesSpell(int number)
        {
            DigitSpellModif digitSpellModif = ParseDigit(number);
            switch (digitSpellModif)
            {
                case DigitSpellModif.One:
                    return "������";
                case DigitSpellModif.FromTwoToFour:
                    return "������";
                case DigitSpellModif.More:
                    return "�����";
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public static string GetHoursSpell(int number)
        {
            DigitSpellModif digitSpellModif = ParseDigit(number);
            switch (digitSpellModif)
            {
                case DigitSpellModif.One:
                    return "���";
                case DigitSpellModif.FromTwoToFour:
                    return "����";
                case DigitSpellModif.More:
                    return "�����";
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public static string GetSecondsSpell(int number)
        {
            DigitSpellModif digitSpellModif = ParseDigit(number);
            switch (digitSpellModif)
            {
                case DigitSpellModif.One:
                    return "�������";
                case DigitSpellModif.FromTwoToFour:
                    return "�������";
                case DigitSpellModif.More:
                    return "������";
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public static string GetDaysSpell(int number)
        {
            DigitSpellModif digitSpellModif = ParseDigit(number);
            switch (digitSpellModif)
            {
                case DigitSpellModif.One:
                    return "����";
                case DigitSpellModif.FromTwoToFour:
                    return "���";
                case DigitSpellModif.More:
                    return "����";
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private static DigitSpellModif ParseDigit(int number)
        {
            int lastDigit = number % 10;
            number = number % 100;
            if (lastDigit == 1 && (number < 10 || number > 20)) return DigitSpellModif.One;
            if (lastDigit > 1 && lastDigit < 5 && (number < 10 || number > 20)) return DigitSpellModif.FromTwoToFour;
            return DigitSpellModif.More;
        }
    }
}