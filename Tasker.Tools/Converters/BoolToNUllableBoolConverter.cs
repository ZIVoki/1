﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Tasker.Tools.Converters
{
    class BoolToNUllableBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool? result = (bool)value;
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool? mainValue = (bool?) value;
            if (!mainValue.HasValue) return false;
            return mainValue.Value;
        }
    }
}
