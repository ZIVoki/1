﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Tasker.Tools.Converters
{
    public class CalculateTaskDuration : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is DateTime)
            {
                return DateTime.Now.Subtract((DateTime) value);
            }
            return TimeSpan.FromHours(0);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
