using System.Linq;
using Tasker.Tools.Extensions;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Tools.Managers
{
    public class ChannelsManager
    {
        public static ChannelView GetChannel(ChannelDTO dto, UniqBindableCollection<ChannelView> repository)
        {
            if (dto == null) return null;
            ChannelView channelView = repository.FirstOrDefault(t => t.Id == dto.Id);
            if (channelView == null)
            {
                return CreateChannel(dto, repository);
            }
            return channelView;
        }

        private static ChannelView CreateChannel(ChannelDTO dto, UniqBindableCollection<ChannelView> repository)
        {
            if (dto == null) return null;

            ChannelView channelView = new ChannelView
                                          {
                                              Id = dto.Id,
                                              Title = dto.Title,
                                              Status = dto.Status
                                          };

            if (repository != null) repository.Add(channelView);
            return channelView;
        }

        public static void ChannelUpdate(ChannelDTO dto, UniqBindableCollection<ChannelView> repository)
        {
            ChannelView channelView = repository.FirstOrDefault(c => c.Id == dto.Id);

            if (channelView == null)
            {
                CreateChannel(dto, repository);
            }
            else
            {
                ChannelUpdate(channelView, dto);
            }
        }

        public static void ChannelUpdate(ChannelView channel, ChannelDTO dto)
        {
            if (channel != null)
            {
                channel.Title = dto.Title;
                channel.Status = dto.Status;
                channel.DTOObject = dto;
            }
        }
    }
}