﻿using System.Collections;
using System.Linq;
using Tasker.Tools.Extensions;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Tools.Managers
{
    public class ShiftManager
    {
        public static ShiftView GetShift(ShiftDTO shiftsDTO, UniqBindableCollection<ShiftView> shifts)
        {
            ShiftView shiftView;
            
            shiftView = shifts.FirstOrDefault(s => s.Id == shiftsDTO.Id);
            
            if (shiftView == null) 
                return CreateNewShift(shiftsDTO, shifts);
            return shiftView;
        }

        public static ShiftView CreateNewShift(ShiftDTO shiftDTO, UniqBindableCollection<ShiftView> shifts)
        {
            if (shiftDTO == null) return null;
            ShiftView newShiftView = new ShiftView
                                         {
                                             Id = shiftDTO.Id,
                                             Title = shiftDTO.Title,
                                             Status = shiftDTO.Status,
                                             StartTime = shiftDTO.StartTime,
                                             EndTime = shiftDTO.EndTime,
                                             LunchDuration = shiftDTO.LunchDuration
                                         };

            
                shifts.Add(newShiftView);
            
            return newShiftView;
        }

        public static void ShiftUpdate(ShiftDTO shiftDto, UniqBindableCollection<ShiftView> shifts)
        {
            ShiftView shiftView = shifts.FirstOrDefault(s => s.Id == shiftDto.Id);  

            if (shiftView != null)
            {
                ShiftUpdate(shiftDto, shiftView);
            }
            else
            {
                CreateNewShift(shiftDto, shifts);
            }
        }

        private static void ShiftUpdate(ShiftDTO shiftDTO, ShiftView shiftView)
        {
            if (shiftDTO != null)
            {
                shiftView.Title = shiftDTO.Title;
                shiftView.Status = shiftDTO.Status;
                shiftView.StartTime = shiftDTO.StartTime;
                shiftView.EndTime = shiftDTO.EndTime;
                shiftView.LunchDuration = shiftDTO.LunchDuration;
            }
        }

    }
}
