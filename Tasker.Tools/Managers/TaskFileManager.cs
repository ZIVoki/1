﻿using System.Linq;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Tools.Managers
{
    public class TaskFileManager
    {
        public static AttachFileView GetFileData(AttachFileDTO dto, TaskView task)
        {
            if (dto == null) return null;
            var exist = task.Files.FirstOrDefault(a => a.Id == dto.Id);
            if(exist == null)
            {
                return CreateFileData(dto, task);
            }
            return exist;
        }

        public static AttachFileView CreateFileData(AttachFileDTO dto, TaskView task)
        {
            if (dto == null) return null;
            var taskType = new AttachFileView(task)
                               {
                                   Id = dto.Id,
                                   DTOObject = dto,
                                   Status = dto.Status,
                                   Title = dto.Title,
                                   CreatedAt = dto.CreatedAt,
                                   FilePath = dto.FilePath,
                                   IsFinalyFile = dto.IsFinalyFile,
                               };
            task.Files.Add(taskType);
            return taskType;
        }
    }
}
