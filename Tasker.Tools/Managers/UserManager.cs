﻿using System.Linq;
using Tasker.Tools.Extensions;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Tools.Managers
{
    public class UserManager
    {
        public static UserView GetUser(UserDTO userDto, UniqBindableCollection<UserView> repository)
        {
            if (userDto == null) return null;
            UserView exist;
            exist = repository.FirstOrDefault(a => a.Id == userDto.Id);
            
            if (exist == null)
            {
                return CreateUser(userDto, repository);
            }
            return exist;
        }

        public static void UserUpdate(UserView user, UserDTO dto)
        {
            if (user != null)
            {
                user.FirstName = dto.FirstName;
                user.LastName = dto.LastName;
                user.LastActiveDate = dto.LastActiveDate;
                user.DomainName = dto.DomainName;
                user.Role = dto.RoleId;
                user.Login = dto.Login;
                user.Email = dto.Email;
                user.Phone = dto.Phone;
                user.WeeklyHours = dto.WeeklyHours;
                user.CurrentState = dto.NetworkState;
            }
        }

        public static void UserUpdate(UserDTO dto, UniqBindableCollection<UserView> repository)
        {
            if (dto == null) return;
            UserView user;
            user = repository.FirstOrDefault(u => u.Id == dto.Id);

            if (user == null)
            {
                CreateUser(dto, repository);
            }
            else
            {
                UserUpdate(user, dto);
            }
        }

        public static UserView CreateUser(UserDTO dto, UniqBindableCollection<UserView> repository = null)
        {
            if (dto == null) return null;
            UserView newUserView = new UserView
                                       {
                                           Id = dto.Id,
                                           Status = dto.Status,
                                           FirstName = dto.FirstName,
                                           LastName = dto.LastName,
                                           DomainName = dto.DomainName,
                                           LastActiveDate = dto.LastActiveDate,
                                           Login = dto.Login,
                                           Email = dto.Email,
                                           Phone = dto.Phone,
                                           CurrentState = dto.NetworkState,
                                           Role = dto.RoleId,
                                           WeeklyHours = dto.WeeklyHours
                                       };
            if (repository != null)
            {
                repository.Add(newUserView);
            }
            return newUserView;
        }
    }
}
