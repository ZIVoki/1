﻿using System.Linq;
using Tasker.Tools.Extensions;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Tools.Managers
{
    public class AttendanceLogItemManager
    {
        public static AttendanceLogItemView GetAttendance(AttendanceLogItemDTO dto,
                                                          UniqBindableCollection<AttendanceLogItemView> repository,
                                                          UniqBindableCollection<UserView> users,
                                                          UniqBindableCollection<TimeTableView> timeTables,
                                                          UniqBindableCollection<ShiftView> shifts)
        {
            if (dto == null) return null;
            AttendanceLogItemView exist = repository.FirstOrDefault(r => r.Id == dto.Id);
            if (exist == null)
                return CreateAttendance(dto, repository, users, timeTables, shifts);
            return exist;
        }

        private static AttendanceLogItemView CreateAttendance(AttendanceLogItemDTO dto,
                                                              UniqBindableCollection<AttendanceLogItemView> repository,
                                                              UniqBindableCollection<UserView> users,
                                                              UniqBindableCollection<TimeTableView> timeTables,
                                                              UniqBindableCollection<ShiftView> shifts)
        {
            if (dto == null) return null;

            AttendanceLogItemView logItemView = new AttendanceLogItemView
                {
                    Id = dto.Id,
                    Status = dto.Status,
                    EventDateTime = dto.EventDateTime,
                    User = UserManager.GetUser(dto.User, users),
                    TimeTable = TimeTableManager.GetTimeTable(dto.TimeTable, timeTables, users, shifts)
                };


            if (repository != null)
            {
                repository.Add(logItemView);
            }

            return logItemView;
        }
    }
}