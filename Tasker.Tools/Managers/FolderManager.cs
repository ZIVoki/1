using System.Collections.ObjectModel;
using System.Linq;
using Tasker.Tools.Extensions;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Tools.Managers
{
    public class FolderManager
    {
        public static FolderView GetFolderView(FolderDTO dto, ObservableCollection<FolderView> folders)
        {
            if (dto == null) 
                return null;
            FolderView exist = folders.FirstOrDefault(f => f.Id == dto.Id);
            if (exist == null)
            {
                return CreateNewFolder(dto, folders);
            }
            return exist;
        }

        public static FolderView CreateNewFolder(FolderDTO dto, ObservableCollection<FolderView> folders)
        {
            if (dto == null)
                return null;
            FolderView parentFolderView = null;
            if (dto.ParentFolder != null)
                parentFolderView = GetFolderView(dto.ParentFolder, folders);

            FolderView folderView = new FolderView(dto, parentFolderView, false);
            folders.Add(folderView);
            return folderView;
        }

        public static void LoadBoardStaticStructure(ObservableCollection<FolderView> folders)
        {
            if (folders.Count == 10) return;

            FolderView bibleFolderView = new FolderView(6, "������", null, false);
            FolderView otherFolderView = new FolderView(7, "����� ���������", bibleFolderView, false);
            FolderView procinfoFolderView = new FolderView(9, "���������������� ����������", bibleFolderView, false);
            FolderView specOptionsFolderView = new FolderView(8, "����������� ������������ �������", bibleFolderView, false);
            FolderView criticlaFolderView = new FolderView(10, "����������� ������", bibleFolderView, false);

            FolderView bagsFolderView = new FolderView(4, "����", null, false);
            FolderView airBFolderView = new FolderView(2, "Air Book", null, false);
            FolderView contactsFolderView = new FolderView(3, "��������", null, false);
            FolderView docTemplatesFolderView = new FolderView(1, "������� ����������", null, false);
            FolderView updatesFolderView = new FolderView(5, "����������", null, false);

            folders.Add(bibleFolderView);
            folders.Add(otherFolderView);
            folders.Add(procinfoFolderView);
            folders.Add(specOptionsFolderView);
            folders.Add(criticlaFolderView);
            folders.Add(bagsFolderView);
            folders.Add(airBFolderView);
            folders.Add(contactsFolderView);
            folders.Add(docTemplatesFolderView);
            folders.Add(updatesFolderView);
        }
    }
}