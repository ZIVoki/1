﻿using System.Linq;
using Tasker.Tools.Extensions;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Tools.Managers
{
    public class TaskTypeManager
    {
        public static TaskTypeView GetTaskType(TaskTypeDTO dto, UniqBindableCollection<TaskTypeView> repository, System.Drawing.Color? highlightColor = null)
        {
            if (dto == null) return null;
            TaskTypeView exist;
            exist = repository.FirstOrDefault(a => a.Id == dto.Id);
            
            if(exist == null)
            {
                return CreateTaskType(dto, repository, highlightColor);
            }
            return exist;
        }

        public static TaskTypeView CreateTaskType(TaskTypeDTO dto, 
                                                    UniqBindableCollection<TaskTypeView> repository, 
                                                    System.Drawing.Color? highlightColor = null)
        {
            if (dto == null) return null;
            var taskType = new TaskTypeView
                               {
                                   Id = dto.Id,
                                   DTOObject = dto,
                                   Status = dto.Status,
                                   Title = dto.Title,
                                   Description = dto.Description,
                                   CompletionTime = dto.CompletionTime,
                                   HighLightBrush = highlightColor
                               };

            repository.Add(taskType);
            
            return taskType;
        }

        public static void UpdateTaskType(TaskTypeView taskType, TaskTypeDTO dto)
        {
            if (taskType != null)
            {
                taskType.Title = dto.Title;
                taskType.Description = dto.Description;
                taskType.Status = dto.Status;
                taskType.CompletionTime = dto.CompletionTime;
                taskType.DTOObject = dto;
            }
        }
        
        public static void UpdateTaskType(UniqBindableCollection<TaskTypeView> repository, TaskTypeDTO dto)
        {
            TaskTypeView taskTypeView = repository.FirstOrDefault(t => t.Id == dto.Id);

            if (taskTypeView == null)
            {
                CreateTaskType(dto, repository);
            }
            else
            {
                UpdateTaskType(taskTypeView, dto);
            }
        }
    }
}
