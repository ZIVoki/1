﻿using System.Linq;
using Tasker.Tools.Extensions;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Tools.Managers
{
    public class CustomerManager
    {
        public static CustomerView GetCustomer(CustomerDTO dto, 
                                               UniqBindableCollection<CustomerView> repository, 
                                               UniqBindableCollection<ChannelView> channelViews, 
                                               System.Drawing.Color? color = null)
        {
            if (dto == null) return null;
            var exist = repository.FirstOrDefault(a => a.Id == dto.Id);
            if(exist == null)
            {
                return CreateCustomer(dto, channelViews, repository, color);
            }
            return exist;
        }

        public static CustomerView CreateCustomer(CustomerDTO dto, 
                                                  UniqBindableCollection<ChannelView> channelViews, 
                                                  UniqBindableCollection<CustomerView> repository = null, 
                                                  System.Drawing.Color? highlightColor = null)
        {
            if (dto == null) return null;

            var customer = new CustomerView
                               {
                                   Id = dto.Id,
                                   Description = dto.Description,
                                   Channel = ChannelsManager.GetChannel(dto.Channel, channelViews),
                                   MaxWorkers = dto.MaxWorkers.ToString(),
                                   DTOObject = dto,
                                   Status = dto.Status,
                                   Title = dto.Title,
                                   HighLightBrush = highlightColor
                               };

            if (repository != null) 
                repository.Add(customer);
            return customer;
        }

        private static void CustomerUpdate(CustomerView customer, CustomerDTO dto)
        {
            if (customer != null)
            {
                customer.Title = dto.Title;
                customer.Description = dto.Description;
                customer.MaxWorkers = dto.MaxWorkers.ToString();
                customer.Status = dto.Status;
                customer.DTOObject = dto;
            }
        }

        public static void CustomerUpdate(UniqBindableCollection<CustomerView> repository, 
                                          UniqBindableCollection<ChannelView> channelViews, 
                                          CustomerDTO dto)
        {
            CustomerView firstOrDefault = repository.FirstOrDefault(c => c.Id == dto.Id);

            if (firstOrDefault == null)
            {
                CreateCustomer(dto, channelViews, repository);
            }
            else
            {
                CustomerUpdate(firstOrDefault, dto);
            }
        }
    }
}
