﻿using System;
using System.Linq;
using Tasker.Tools.Extensions;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Tools.Managers
{
    public class UserTaskTypeManager
    {
        public static UserTaskTypeView GetUserTaskType(UserTaskTypeDTO dto, UniqBindableCollection<UserView> users, UniqBindableCollection<TaskTypeView> taskTypes)
        {
            UserView userView = UserManager.GetUser(dto.User, users);
            if (userView == null) return null;

            var exist = userView.UserTaskTypes.FirstOrDefault(a => a.Id == dto.Id);
            if(exist == null)
            {
                return CreateUserTaskType(dto, users, taskTypes);
            }
            return exist;
        }

        public static UserTaskTypeView CreateUserTaskType(UserTaskTypeDTO dto, UniqBindableCollection<UserView> users, UniqBindableCollection<TaskTypeView> taskTypes)
        {
            UserView userView = UserManager.GetUser(dto.User, users);
            if (userView == null) return null;

            var userTaskType = new UserTaskTypeView(userView)
                               {
                                   Id = dto.Id,
                                   DTOObject = dto,
                                   Status = dto.Status,
                                   TaskType = TaskTypeManager.GetTaskType(dto.TaskType, taskTypes),
                                   CompletionTime = dto.CompletionTime,
                               };
            userView.UserTaskTypes.Add(userTaskType);
            return userTaskType;
        }

        public static void UserTaskTypeUpdate(UserTaskTypeView usertaskType, UserTaskTypeDTO dto, UniqBindableCollection<TaskTypeView> taskTypes)
        {
            if (usertaskType != null)
            {
                usertaskType.Status = dto.Status;
                usertaskType.CompletionTime = dto.CompletionTime;
                usertaskType.DTOObject = dto;
                usertaskType.TaskType = TaskTypeManager.GetTaskType(dto.TaskType, taskTypes);

                if(dto.Status == ConstantsStatuses.Deleted)
                {
                    usertaskType.User.UserTaskTypes.Remove(usertaskType);
                }
            }
        }

        public static UserTaskTypeView GetUserTaskType(UserTaskTypeDTO userTaskType, UserView worker, UniqBindableCollection<TaskTypeView> taskTypes)
        {
            var exist = worker.UserTaskTypes.FirstOrDefault(a => a.Id == userTaskType.Id);
            if (exist == null)
            {
                return CreateUserTaskType(userTaskType, worker, taskTypes);
            }
            return exist;
        }

        private static UserTaskTypeView CreateUserTaskType(UserTaskTypeDTO dto, UserView worker, UniqBindableCollection<TaskTypeView> taskTypes)
        {
            var userTaskType = new UserTaskTypeView(worker)
            {
                Id = dto.Id,
                DTOObject = dto,
                Status = dto.Status,
                TaskType = TaskTypeManager.GetTaskType(dto.TaskType, taskTypes),
                CompletionTime = dto.CompletionTime,
            };
            worker.UserTaskTypes.Add(userTaskType);
            return userTaskType;
        }

        public static void UserTaskTypeUpdate(UserTaskTypeDTO userTaskTypeDto,
                                              UniqBindableCollection<UserView> users, 
                                              UniqBindableCollection<TaskTypeView> taskTypes)
        {
            if (userTaskTypeDto == null) return;
            UserView userView = UserManager.GetUser(userTaskTypeDto.User, users);
            if (userView == null) return;
            UserTaskTypeView usertaskType = GetUserTaskType(userTaskTypeDto, users, taskTypes);
            if (usertaskType == null) return;

            if (userTaskTypeDto.Status == ConstantsStatuses.Deleted)
            {
                usertaskType.User.UserTaskTypes.Remove(usertaskType);
            }
            else
            {
                usertaskType.Status = userTaskTypeDto.Status;
                usertaskType.CompletionTime = userTaskTypeDto.CompletionTime;
                usertaskType.DTOObject = userTaskTypeDto;
                usertaskType.TaskType = TaskTypeManager.GetTaskType(userTaskTypeDto.TaskType, taskTypes);
            }
        }
    }
}
