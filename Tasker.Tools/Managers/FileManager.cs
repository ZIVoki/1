using System;
using System.Collections.ObjectModel;
using System.Linq;
using Tasker.Tools.Extensions;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Tools.Managers
{
    public class FileManager<T> where T : FileView
    {
        public static T GetFileView(FileVersionDTO dto, UniqBindableCollection<T> files, ObservableCollection<FolderView> folders)
        {
            if (dto == null) 
                return null;
            T exist = files.FirstOrDefault(f => f.Id == dto.File.Id);
            if (exist == null)
            {
                return CreateNewFile(dto, files, folders);
            }
            return exist;
        }

        public static T CreateNewFile(FileVersionDTO dto, UniqBindableCollection<T> files, ObservableCollection<FolderView> folders) 
        {
            if (dto == null) 
                return null;
            FolderView folderView = FolderManager.GetFolderView(dto.File.Folder, folders);

            T fileView = (T)Activator.CreateInstance(typeof(T), dto, folderView, false);
            files.Add(fileView);
            return fileView;
        }

        public static void UpdateFileView(FileVersionDTO dto, T view)
        {
            if (dto != null)
            {
                view.Title = dto.File.Title;
                view.FilePath = dto.File.FilePath;
                view.SourceFilePath = dto.File.FileSource;
                view.Version = dto.Version;
                view.ReadDate = dto.ReadDate;
                view.VersionDiff = dto.VersionDiff;
                view.ChangesDateTime = dto.ChangesDate;

                view.UsersReadFile.Clear();
            }
        }

        public static void UpdateFileView(FileVersionDTO dto, UniqBindableCollection<T> files, ObservableCollection<FolderView> folders)
        {
            if (dto == null) return;

            T exist = files.FirstOrDefault(f => f.Id == dto.File.Id);

            if (exist == null)
            {
                CreateNewFile(dto, files, folders);
            }
            else
            {
                UpdateFileView(dto, exist);
            }
        }

        public static void SetUsersReadFile(FileVersionDTO fileVersionDto, UniqBindableCollection<T> file, ObservableCollection<FolderView> folders, UniqBindableCollection<UserView> users)
        {
            T currentFile = GetFileView(fileVersionDto, file, folders);
            foreach (var userReadFileDTO in fileVersionDto.UsersReadFile)
            {
                SetUserReadeFile(users, userReadFileDTO, currentFile);
            }
        }

        public static void SetUserReadeFile(UniqBindableCollection<UserView> users, UserReadFileDTO userReadFileDTO, T currentFile)
        {
            UserView user = users.FirstOrDefault(u => u.Id == userReadFileDTO.UserId);
            UserReadView userReadView = new UserReadView
                {
                    ReadDate = userReadFileDTO.ReadDate,
                    User = user,
                    File = currentFile
                };

            currentFile.UsersReadFile.Add(userReadView);
        }
    }
}