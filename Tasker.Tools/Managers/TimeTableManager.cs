﻿using System.Linq;
using Tasker.Tools.Extensions;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Tools.Managers
{
    public class TimeTableManager
    {
        public static TimeTableView GetTimeTable(TimeTableDTO timeTableDTO, UniqBindableCollection<TimeTableView> repository, UniqBindableCollection<UserView> users, UniqBindableCollection<ShiftView> shifts)
        {
            if (timeTableDTO == null || repository == null) return null;
            TimeTableView resultTableView = repository.FirstOrDefault(t => t.Id == timeTableDTO.Id);
            if (resultTableView == null)
            {
                return CreateTimeTableView(timeTableDTO, repository, users, shifts);
            }
            return resultTableView;
        }

        public static TimeTableView CreateTimeTableView(TimeTableDTO timeTableDTO, UniqBindableCollection<TimeTableView> repository, UniqBindableCollection<UserView> users, UniqBindableCollection<ShiftView> shifts)
        {
            if (timeTableDTO == null) return null;
            
            TimeTableView newTimeTableView = new TimeTableView
                                                 {
                                                     Id = timeTableDTO.Id,
                                                     StartDate = timeTableDTO.StartDate,
                                                     EndDate = timeTableDTO.EndDate,
                                                     Status = timeTableDTO.Status,
                                                     User = UserManager.GetUser(timeTableDTO.User, users),
                                                     Shift = ShiftManager.GetShift(timeTableDTO.Shift, shifts),
                                                     FirstLunchStart = timeTableDTO.FirstLunchStart,
                                                     FirstLunchEnd = timeTableDTO.FirstLunchEnd,
                                                     SecondLunchStart = timeTableDTO.SecondLunchStart,
                                                     SecondLunchEnd = timeTableDTO.SecondLunchEnd,
                                                     IsResp = timeTableDTO.IsResp
                                                 };

            if (repository != null)
            {
                repository.Add(newTimeTableView);
            }
            return newTimeTableView;
        }

        public static void UpdateTimeTableView(TimeTableDTO timeTableDTO, 
                                               UniqBindableCollection<TimeTableView> repository, 
                                               UniqBindableCollection<UserView> users, 
                                               UniqBindableCollection<ShiftView> shifts)
        {
            if (timeTableDTO == null || repository == null) return;
            TimeTableView updateTimeTableView = repository.FirstOrDefault(t => t.Id == timeTableDTO.Id);

            if (updateTimeTableView == null)
            {
                CreateTimeTableView(timeTableDTO, repository, users, shifts);
            }
            else
            {
                updateTimeTableView.StartDate = timeTableDTO.StartDate;
                updateTimeTableView.EndDate = timeTableDTO.EndDate;
                updateTimeTableView.Shift = ShiftManager.GetShift(timeTableDTO.Shift, shifts);
                updateTimeTableView.User = UserManager.GetUser(timeTableDTO.User, users);
                updateTimeTableView.Status = timeTableDTO.Status;
                updateTimeTableView.FirstLunchStart = timeTableDTO.FirstLunchStart;
                updateTimeTableView.FirstLunchEnd = timeTableDTO.FirstLunchEnd;
                updateTimeTableView.SecondLunchStart = timeTableDTO.SecondLunchStart;
                updateTimeTableView.SecondLunchEnd = timeTableDTO.SecondLunchEnd;
                updateTimeTableView.IsResp = timeTableDTO.IsResp;
            }
        }

        public static TimeTableView Clone(TimeTableView timeTableView)
        {
            return new TimeTableView
                       {
                                EndDate = timeTableView.EndDate,
                                StartDate = timeTableView.StartDate,
                                Id = timeTableView.Id,
                                FirstLunchEnd = timeTableView.FirstLunchEnd,
                                FirstLunchStart = timeTableView.FirstLunchStart,
                                SecondLunchEnd = timeTableView.SecondLunchEnd,
                                SecondLunchStart = timeTableView.SecondLunchStart,
                                Shift = timeTableView.Shift,
                                User = timeTableView.User,
                                Status = timeTableView.Status,
                                Title = timeTableView.Title,
                                IsResp = timeTableView.IsResp
                            };
        }
    }
}
