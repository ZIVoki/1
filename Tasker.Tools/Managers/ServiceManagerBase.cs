﻿using System;
using System.ComponentModel;
using System.ServiceModel;
using System.Threading;
using System.Windows;
using Tasker.Tools.Resources;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Tools.Managers
{
    public delegate void WaitingForConnectionDelegate(object sender);
    public delegate void ConnectionReestablishedDelegate(object sender);

    public interface IServiceLoginManager
    {
        string Login(string login, string password, string serverURI = null);
    }

    /// <summary>
    /// Абстрактный класс подключения дуплексногое соединения и входа в систему
    /// </summary>
    /// <typeparam name="TCl">ClientChannel</typeparam>
    /// <typeparam name="TCb">Callback</typeparam>
    public abstract class ServiceManagerBase<TCl, TCb> : IServiceLoginManager where TCl : IClientChannel
        where TCb : class, new()
    {
        protected string NetTcpConnectionMask = "";
        protected string EndpointConfigurationName = "";
        protected TimeSpan PingTimeOut = new TimeSpan(0, 0, 0, 15);
        protected LoginParams LastLoginParams;
        protected bool LoginSync = false;

        protected class LoginParams
        {
            public LoginParams(string loginName, string password, string loginURI)
            {
                LoginName = loginName;
                Password = password;
                LoginURI = loginURI;
            }

            public string LoginName { get; set; }
            public string Password { get; set; }
            public string LoginURI { get; set; }
        }

        private BackgroundWorker _pingServicesBackgroundWorker;
        private int _reconnectCounter = 0;

        protected TCl _serviceChannel = default(TCl);
        protected TCb _callback;

        public event WaitingForConnectionDelegate WaitingForConnection;
        public event ConnectionReestablishedDelegate ConectionReestablished;

        #region EnetsInvocers

        private void OnWaitingForConnection()
        {
            WaitingForConnectionDelegate handler = WaitingForConnection;
            if (handler != null) handler(null);
        }

        private void OnConectionReestablished()
        {
            ConnectionReestablishedDelegate handler = ConectionReestablished;
            if (handler != null) handler(null);
        }

        #endregion

        protected IClientChannel ClientChannel
        {
            get { return ServiceChannel; }
        }

        public TCb Callback
        {
            get { return _callback; }
        }

        public TCl ServiceChannel
        {
            get
            {
                if (_serviceChannel == null)
                    return default(TCl);
                while (_serviceChannel.State == CommunicationState.Faulted)
                {
                    if (_reconnectCounter > 5) DeadClose();

                    LogInfo(string.Format("Relogining to server with user {0}", LastLoginParams.LoginName));
                    Thread.Sleep(PingTimeOut);
                    _reconnectCounter++;
                }
                OnConectionReestablished();
                _reconnectCounter = 0;

                return _serviceChannel;
            }
            set { _serviceChannel = value; }
        }

        private void DeadClose()
        {
            CloseAndLogoff();
            MessageBox.Show("Не удалось связаться с сервером. Приложение будет закрыто",
                            "Ошибка связи с сервером", MessageBoxButton.OK, MessageBoxImage.Error);
            Environment.Exit(0);
        }

        public string Login(string login, string password, string serverURI = null)
        {
            LoginSync = true;
            // remeber last login
            LastLoginParams = new LoginParams(login, password, serverURI);

            LogInfo("Loginig begins..");
            Connect(serverURI);

            LoginResultDTO loginSuccess;

            try
            {
                Ping();
            }
            catch (Exception ex)
            {
                LogErrorEx(LoginManagerResources.NoServerResponse, ex);
                LoginSync = false;
                return LoginManagerResources.NoServerResponse;
            }

            try
            {
                loginSuccess = Login(login, password);
            }
            catch (Exception ex)
            {
                LogErrorEx(LoginManagerResources.AuthorizationError, ex);
                LoginSync = false;
                return LoginManagerResources.AuthorizationError;
            }

            if (!loginSuccess.Success)
            {
                LogInfo(LoginManagerResources.AuthorizationError);
                LoginSync = false;
                return LoginManagerResources.AuthorizationError;
            }

            if (ClientTypeCheck(loginSuccess))
            {
                Logoff();
                LogInfo(LoginManagerResources.WrongClientError);
                LoginSync = false;
                return LoginManagerResources.WrongClientError;
            }

            UserView workerView = new UserView
                {
                    Id = loginSuccess.User.Id,
                    LastName = loginSuccess.User.LastName,
                    FirstName = loginSuccess.User.FirstName,
                    Status = loginSuccess.User.Status,
                    WeeklyHours = loginSuccess.User.WeeklyHours,
                    Role = loginSuccess.Role,
                    CurrentState = ConstantsNetworkStates.Online,
                    LastActiveDate = loginSuccess.User.LastActiveDate,
                    Login = login,
                    Password = password
                };

            LoadData(workerView);
            StartServicePing();

            LogInfo(LoginManagerResources.AuthorizationSuccess);
            LoginSync = false;
            return LoginManagerResources.AuthorizationSuccess;
        }

        public abstract void LoadData(UserView currentUser);

        public bool Connect(string serverURI)
        {
            bool result = true;
            LogInfo("Connecting to remoute server begins..");
            try
            {
                DisposeCallback();
                _callback = new TCb();
                if (string.IsNullOrEmpty(serverURI))
                {
                    DuplexChannelFactory<TCl> duplexChannelFactory =
                        new DuplexChannelFactory<TCl>(new InstanceContext(_callback));
                    _serviceChannel = duplexChannelFactory.CreateChannel();
                }
                else
                {

                    string uri = string.Format(NetTcpConnectionMask, serverURI);


                    DuplexChannelFactory<TCl> duplexChannelFactory =
                        new DuplexChannelFactory<TCl>(new InstanceContext(_callback),
                                                      EndpointConfigurationName,
                                                      new EndpointAddress(uri));
                    _serviceChannel = duplexChannelFactory.CreateChannel();
                }
            }
            catch (Exception e)
            {
                LogErrorEx("Connection to remoute server failed", e);
                result = false;
            }
            return result;
        }

        public virtual void CloseAndLogoff()
        {
            LogInfo("Stoping ping..");
            _pingServicesBackgroundWorker.CancelAsync();
            if (_serviceChannel == null || _serviceChannel.State == CommunicationState.Faulted)
            {
                LogInfo("Service channel is in faulte state or null, can not logoff.");
                return;
            }
            try
            {
                LogInfo("Logining off");
                Logoff();
                ClientChannel.Close();
                DisposeCallback();
            }
            catch (Exception e)
            {
                LogErrorEx("Error while logoff", e);
            }
        }

        private void StartServicePing()
        {
            LogInfo("Starting service pinging..");
            if (_pingServicesBackgroundWorker == null)
            {
                _pingServicesBackgroundWorker = new BackgroundWorker();
                _pingServicesBackgroundWorker.DoWork += DoPingWork;
                _pingServicesBackgroundWorker.WorkerSupportsCancellation = true;
            }

            if (!_pingServicesBackgroundWorker.IsBusy)
                _pingServicesBackgroundWorker.RunWorkerAsync();
        }

        public abstract bool DoesUserExist();

        private void DoPingWork(object sender, DoWorkEventArgs eventArgs)
        {
            while (DoesUserExist())
            {
                if (!LoginSync)
                {
                    try
                    {
                        Ping();
                        if (_reconnectCounter > 0)
                        {
                            _reconnectCounter = 0;
                            OnConectionReestablished();
                        }
                    }
                    catch (Exception ex)
                    {
                        if (_reconnectCounter > 5) DeadClose();

                        OnWaitingForConnection();
                        // relogin
                        try
                        {
                            Login(LastLoginParams.LoginName, LastLoginParams.Password,
                                  LastLoginParams.LoginURI);
                        }
                        catch (Exception e)
                        {
                            LogFatal("Exception while server connection", e);
                        }
                        _reconnectCounter++;
                    }
                }
                Thread.Sleep(PingTimeOut);
            }
        }

        protected abstract bool ClientTypeCheck(LoginResultDTO loginResult);
        protected abstract void LogInfo(string message);
        protected abstract void LogError(string message);
        protected abstract void LogErrorEx(string message, Exception e);
        protected abstract void LogFatal(string message);
        protected abstract void LogFatal(string message, Exception e);

        protected abstract void DisposeCallback();

        protected abstract LoginResultDTO Login(string login, string password);
        protected abstract void Ping();
        protected abstract void Logoff();
    }
}