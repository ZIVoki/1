﻿using System.Linq;
using Tasker.Tools.Extensions;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Tools.Managers
{
    public class MessageManager
    {
        public static MessageView GetMessage(MessageDTO dto, TaskView task,
                                            UniqBindableCollection<MessageView> repository,
                                            UniqBindableCollection<UserView> userRepository, int userViewer, bool isRead = false)
        {
            var exist = repository.FirstOrDefault(a => a.Id == dto.Id);
            if(exist == null)
            {
                return CreateMessage(dto, task, userRepository, isRead, userViewer, repository);
            }
            exist.IsReaded = isRead;
            return exist;
        }

        public static MessageView CreateMessage(MessageDTO dto, TaskView task, 
                                                UniqBindableCollection<UserView> userRepository, 
                                                bool isRead, int userViewId,
                                                UniqBindableCollection<MessageView> repository = null)
        {
            
            var message = new MessageView(task)
                               {
                                   Id = dto.Id,
                                   DTOObject = dto,
                                   Status = dto.Status,
                                   UserFrom = UserManager.GetUser(dto.FromUser, userRepository),
                                   SendDate = dto.SendDate,
                                   Text = dto.Text,
                                   IsReaded = isRead,
                                   UserDestination = UserManager.GetUser(dto.ToUser, userRepository),
                               };
            
            if(userViewId == message.UserFrom.Id)
            {
                message.IsMy = true;
            }
            if (repository != null) repository.Add(message);
            return message;
        }
    }
}
