﻿using System.Collections;
using System.Drawing;
using System.Linq;
using Tasker.Tools.Extensions;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Tools.Managers
{
    public class TaskManager
    {
        public static TaskView GetTask(TaskDTO dto,
                                UniqBindableCollection<TaskView> repository,
                                UniqBindableCollection<CustomerView> customers,
                                UniqBindableCollection<TaskTypeView> taskTypes,
                                UniqBindableCollection<ChannelView> channels,
                                UniqBindableCollection<UserView> users, 
                                Color? taskTypeColor = null, 
                                Color? customerColor = null)
        {
            TaskView exist = repository.FirstOrDefault(taskView => taskView.Id == dto.Id);
            if (exist == null)
            {
                return CreateTask(dto, customers, taskTypes, users, channels, null, repository);
            }
            return exist;                                                            
            
        }

        public static void UpdateTask(TaskView task, 
                                        TaskDTO dto,
                                        UniqBindableCollection<CustomerView> customers,
                                        UniqBindableCollection<TaskTypeView> taskTypes,
                                        UniqBindableCollection<UserView> users,
                                        UniqBindableCollection<ChannelView> channels,
                                        Color? taskTypeColor = null, 
                                        Color? customerColor = null)
        {
            if (task != null)
            {
                task.Title = dto.Title;
                task.Customer = CustomerManager.GetCustomer(dto.Customer, customers, channels, customerColor);
                task.CreationDate = dto.CreationDate;
                task.CompleteDate = dto.CompleteDate;
                task.DTOObject = dto;
                task.DeadLine = dto.DeadLine;
                task.Description = dto.Description;
                task.PlannedTime = dto.PlannedTime;
                task.Priority = dto.Priority;
                task.Status = dto.Status;
                task.TaskType = TaskTypeManager.GetTaskType(dto.TaskType, taskTypes, taskTypeColor);
                task.Creator = UserManager.GetUser(dto.Creator, users);
                task.CurrentWorker = UserManager.GetUser(dto.CurrentWorker, users);
                task.Comment = dto.Comment;
                task.InProcessTime = dto.InProcessDuration;
            }

            return;
        }

        /// <summary>
        /// Create task from DTO, load other view objects to repository if once exist
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="customers"></param>
        /// <param name="taskTypes"></param>
        /// <param name="users"></param>
        /// <param name="channels"></param>
        /// <param name="worker"></param>
        /// <param name="repository"></param>
        /// <param name="taskTypeColor"></param>
        /// <param name="customerColor"></param>
        /// <returns></returns>
        public static TaskView CreateTask(TaskDTO dto,
                                            UniqBindableCollection<CustomerView> customers,
                                            UniqBindableCollection<TaskTypeView> taskTypes,
                                            UniqBindableCollection<UserView> users,
                                            UniqBindableCollection<ChannelView> channels,
                                            UserView worker = null,
                                            UniqBindableCollection<TaskView> repository = null,
                                            Color? taskTypeColor = null, Color? customerColor = null)
        {
            TaskView task = new TaskView
                           {
                               Id = dto.Id,
                               Description = dto.Description,
                               DTOObject = dto,
                               Status = dto.Status,
                               Priority = dto.Priority,
                               Title = dto.Title,
                               PlannedTime = dto.PlannedTime,
                               DeadLine = dto.DeadLine,
                               CreationDate = dto.CreationDate,
                               CompleteDate = dto.CompleteDate,
                               Customer = CustomerManager.GetCustomer(dto.Customer, customers, channels, customerColor),
                               TaskType = TaskTypeManager.GetTaskType(dto.TaskType, taskTypes, taskTypeColor),
                               Comment = dto.Comment,
                               InProcessTime = dto.InProcessDuration
                            };

            if (worker == null)
                task.CurrentWorker = UserManager.GetUser(dto.CurrentWorker, users);
            else
                task.CurrentWorker = worker;

            if (task.Status == ConstantsStatuses.InProcess && task.CurrentWorker != null)
            {
                if (task.PriorityType == PriorityType.Quick)
                    task.CurrentWorker.UserWorkState = Constants.UserWorkState.Busy;
                if (task.PriorityType == PriorityType.Background)
                    task.CurrentWorker.UserWorkState = Constants.UserWorkState.BackgroundTask;
            }

            if (users == null)
                task.Creator = UserManager.CreateUser(dto.Creator);
            else
                task.Creator = UserManager.GetUser(dto.Creator, users);

            if(repository != null && repository.All(t => t.Id != task.Id))
            {
                repository.Add(task);
            }
            return task;
        }

        public static void AddFile(AttachFileDTO dto, TaskView task)
        {
            if (task != null)
            {
                TaskFileManager.CreateFileData(dto, task);
            }
        }

        public static TaskView UpdateTask(TaskDTO dto,
                                            UniqBindableCollection<CustomerView> customers,
                                            UniqBindableCollection<TaskTypeView> taskTypes,
                                            UniqBindableCollection<UserView> users,
                                            UniqBindableCollection<ChannelView> channels,
                                            UserView worker = null,
                                            UniqBindableCollection<TaskView> repository = null,
                                            Color? taskTypeColor = null, Color? customerColor = null)
        {
            if (repository == null) return null;
            var task = repository.FirstOrDefault(taskView => taskView.Id == dto.Id);

            if (task == null)
            {
                task = CreateTask(dto, customers, taskTypes, users, channels, worker, repository, taskTypeColor, customerColor);
            }
            else
            {
                task.Title = dto.Title;
                task.Customer = CustomerManager.GetCustomer(dto.Customer, customers, channels, customerColor);
                task.CreationDate = dto.CreationDate;
                task.CompleteDate = dto.CompleteDate;
                task.DTOObject = dto;
                task.DeadLine = dto.DeadLine;
                task.Description = dto.Description;
                task.PlannedTime = dto.PlannedTime;
                task.Priority = dto.Priority;
                task.Status = dto.Status;
                task.TaskType = TaskTypeManager.GetTaskType(dto.TaskType, taskTypes, taskTypeColor);
                task.Creator = UserManager.GetUser(dto.Creator, users);
                task.CurrentWorker = UserManager.GetUser(dto.CurrentWorker, users);
                task.Comment = dto.Comment;
                task.InProcessTime = dto.InProcessDuration;
            }
            return task;
        }
    }
}
