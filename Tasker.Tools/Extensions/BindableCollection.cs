using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Threading;
using System.Windows;
using System.Windows.Threading;

namespace Tasker.Tools.Extensions
{
    /// <summary>
    /// Thread-safe dispatcher-safe Obsevablecollection fore WPF
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [Serializable]
    public class BindableCollection<T> : IList<T>, INotifyCollectionChanged
    {
        protected readonly IList<T> Collection;
        protected readonly Dispatcher Dispatcher;
        public event NotifyCollectionChangedEventHandler CollectionChanged;
        public ReaderWriterLockSlim ReaderWriterLock = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);

        public BindableCollection()
        {
            Collection = new List<T>();
            Dispatcher = Application.Current.Dispatcher;
        }
        public BindableCollection(List<T> list)
        {
            Collection = list;
            Dispatcher = Application.Current.Dispatcher;
        }

        public void Add(T item)
        {
            if (Thread.CurrentThread == Dispatcher.Thread)
                DoAdd(item);
            else
                Dispatcher.BeginInvoke((Action)(() => DoAdd(item)));
        }

        protected virtual void DoAdd(T item)
        {
            ReaderWriterLock.EnterWriteLock();
            try
            {
                Collection.Add(item);
                if (CollectionChanged != null)
                    CollectionChanged(this,
                                      new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item));
            }
            finally
            {
                ReaderWriterLock.ExitWriteLock();
            }
        }

        public void Clear()
        {
            if (Thread.CurrentThread == Dispatcher.Thread)
                DoClear();
            else
                Dispatcher.BeginInvoke((Action)(() => { DoClear(); }));
        }

        private void DoClear()
        {
            ReaderWriterLock.EnterWriteLock();
            try
            {
                Collection.Clear();
                if (CollectionChanged != null)
                    CollectionChanged(this,
                                      new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            }
            finally
            {
                ReaderWriterLock.ExitWriteLock();
            }
        }

        public bool Contains(T item)
        {
            ReaderWriterLock.EnterReadLock();
            try
            {
                bool result = Collection.Contains(item);
                return result;
            }
            finally
            {
                ReaderWriterLock.ExitReadLock();
            }
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            ReaderWriterLock.EnterWriteLock();
            try
            {
                Collection.CopyTo(array, arrayIndex);
            }
            finally
            {
                ReaderWriterLock.ExitWriteLock();
            }
        }

        public int Count
        {
            get
            {
                ReaderWriterLock.EnterReadLock();
                try
                {
                    var result = Collection.Count;
                    return result;
                }
                finally
                {
                    ReaderWriterLock.ExitReadLock();
                }
            }
        }

        public bool IsReadOnly
        {
            get { return Collection.IsReadOnly; }
        }

        public bool Remove(T item)
        {
            if (Thread.CurrentThread == Dispatcher.Thread)
                return DoRemove(item);
            DispatcherOperation dispatcherOperation = Dispatcher.BeginInvoke(new Func<T, bool>(DoRemove), item);
            if (dispatcherOperation == null || dispatcherOperation.Result == null)
                return false;
            return (bool)dispatcherOperation.Result;
        }

        private bool DoRemove(T item)
        {
            ReaderWriterLock.EnterWriteLock();
            try
            {
                var index = Collection.IndexOf(item);
                if (index == -1)
                {
                    return false;
                }
                var result = Collection.Remove(item);
                if (result && CollectionChanged != null)
                    CollectionChanged(this, 
                        new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
                return result;
            }
            finally
            {
                ReaderWriterLock.ExitWriteLock();
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            ReaderWriterLock.EnterReadLock();
            try
            {
                return new SafeEnumerator<T>(Collection.GetEnumerator(), ReaderWriterLock);
            }
            finally
            {
                ReaderWriterLock.ExitReadLock();
            }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return Collection.GetEnumerator();
        }

        public int IndexOf(T item)
        {
            ReaderWriterLock.EnterReadLock();
            try
            {
                int result = Collection.IndexOf(item);
                return result;
            }
            finally
            {
                ReaderWriterLock.ExitReadLock();
            }
        }

        public void Insert(int index, T item)
        {
            if (Thread.CurrentThread == Dispatcher.Thread)
                DoInsert(index, item);
            else
                Dispatcher.BeginInvoke((Action)(() => { DoInsert(index, item); }));
        }

        private void DoInsert(int index, T item)
        {
            ReaderWriterLock.EnterUpgradeableReadLock();
            try
            {
                if (Collection.Count == 0 || Collection.Count <= index)
                {
                    return;
                }
                ReaderWriterLock.EnterWriteLock();
                try
                {
                    Collection.Insert(index, item);
                    if (CollectionChanged != null)
                        CollectionChanged(this,
                                          new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item,
                                                                               index));
                }
                finally
                {
                    ReaderWriterLock.ExitWriteLock();
                }
            }
            finally
            {
                ReaderWriterLock.ExitUpgradeableReadLock();
            }
        }

        public void RemoveAt(int index)
        {
            if (Thread.CurrentThread == Dispatcher.Thread)
                DoRemoveAt(index);
            else
                Dispatcher.BeginInvoke((Action)(() => { DoRemoveAt(index); }));
        }

        private void DoRemoveAt(int index)
        {
            ReaderWriterLock.EnterUpgradeableReadLock();
            try
            {
                if (Collection.Count == 0 || Collection.Count <= index)
                    return;
                ReaderWriterLock.EnterWriteLock();
                try
                {
                    Collection.RemoveAt(index);
                    if (CollectionChanged != null)
                        CollectionChanged(this,
                                          new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
                }
                finally
                {
                    ReaderWriterLock.ExitWriteLock();
                }
            }
            finally
            {
                ReaderWriterLock.ExitUpgradeableReadLock();
            }
        }

        public T this[int index]
        {
            get
            {
                ReaderWriterLock.EnterReadLock();
                try
                {
                    T result = Collection[index];
                    return result;
                }
                finally
                {
                    ReaderWriterLock.ExitReadLock();
                }
            }
            set
            {
                ReaderWriterLock.EnterUpgradeableReadLock();
                try
                {
                    if (Collection.Count == 0 || Collection.Count <= index)
                    {
                        return;
                    }
                    ReaderWriterLock.EnterWriteLock();
                    try
                    {
                        Collection[index] = value;
                    }
                    finally
                    {
                        ReaderWriterLock.ExitWriteLock();
                    }
                }
                finally
                {
                    ReaderWriterLock.ExitUpgradeableReadLock();
                }
            }
        }

    }
}

