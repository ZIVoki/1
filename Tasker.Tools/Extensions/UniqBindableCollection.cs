using System.Linq;
using Tasker.Tools.ViewModel.Base;

namespace Tasker.Tools.Extensions
{
    /// <summary>
    /// Bindable collection with unique values with using ViewBase objects
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class UniqBindableCollection<T> : BindableCollection<T> where T : IViewBase
    {
        protected override void DoAdd(T item)
        {
            ReaderWriterLock.EnterUpgradeableReadLock();
            try
            {
                if (item.Id == 0 || this.All(t => t.Id != item.Id))
                    base.DoAdd(item);
            }
            finally
            {
                ReaderWriterLock.EnterUpgradeableReadLock();
            }
        }
    }
}