﻿using System;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;

namespace Tasker.Tools.Extensions
{
    /// <summary>
    /// create links inside textblock or richtextbox
    /// </summary>
    public static class NavigationService
    {
        private static readonly Regex ReUrl =
            new Regex(@"http(|s):\/\/w{0,3}[a-zA-Z0-9_\-.:#/~\?=}&!%ığüşiöç\+,'@]+");
               //"(?i)\\b((?:[a-z][\\w-]+:(?:/{1,3}|[a-z0-9%])|www\\d{0,3}[.]|[a-z0-9.\\-]+[.][a-z]{2,4}/)(?:[^\\s()<>]+|\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\))+(?:\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\)|[^\\s`!()\\[\\]{};:'\".,<>?«»“”‘’]))");

        public static readonly DependencyProperty TextProperty = DependencyProperty.RegisterAttached(
               "Text",
               typeof(string),
               typeof(NavigationService),
               new PropertyMetadata(null, OnTextChanged)
        );

        public static string GetText(DependencyObject d)
        {
            return d.GetValue(TextProperty) as string;
        }

        public static void SetText(DependencyObject d, string value)
        {
            d.SetValue(TextProperty, value);
        }

        private static void OnTextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RichTextBox richTextBox = d as RichTextBox;
            if (richTextBox != null)
            {
                SetLinksInRichTextBox(e, richTextBox);
                return;
            }

            TextBlock textBlock = d as TextBlock;
            if (textBlock != null)
            {
                SetLinksInTextBlock(e, textBlock);
            }
        }

        private static void SetLinksInTextBlock(DependencyPropertyChangedEventArgs e, TextBlock textBlock)
        {
            textBlock.Inlines.Clear();

            var newText = (string)e.NewValue;
            if (string.IsNullOrEmpty(newText))
                return;

            // Find all URLs using a regular expression
            int lastPos = 0;
            foreach (Match match in ReUrl.Matches(newText))
            {
                // Copy raw string from the last position up to the match
                if (match.Index != lastPos)
                {
                    string rawText = newText.Substring(lastPos, match.Index - lastPos);
                    textBlock.Inlines.Add(new Run(rawText));
                }

                // Create a hyperlink for the match
                Hyperlink link = CreateNewHyperlink(match);

                textBlock.Inlines.Add(link);

                // Update the last matched position
                lastPos = match.Index + match.Length;
            }

            // Finally, copy the remainder of the string
            if (lastPos < newText.Length)
                textBlock.Inlines.Add(new Run(newText.Substring(lastPos)));
        }

        private static void SetLinksInRichTextBox(DependencyPropertyChangedEventArgs e, RichTextBox richTextBox)
        {
            Paragraph newParagraph = new Paragraph();

            var newText = (string)e.NewValue;
            if (String.IsNullOrEmpty(newText) || String.IsNullOrWhiteSpace(newText)) return;

            // Find all URLs using a regular expression
            int lastPos = 0;
            foreach (Match match in ReUrl.Matches(newText))
            {
                // Copy raw string from the last position up to the match
                if (match.Index != lastPos)
                {
                    var rawText = newText.Substring(lastPos, match.Index - lastPos);
                    newParagraph.Inlines.Add(new Run(rawText));
                }

                // Create a hyperlink for the match
                Hyperlink link = CreateNewHyperlink(match);
                if (link != null)
                {
                    newParagraph.Inlines.Add(link);

                    // Update the last matched position
                    lastPos = match.Index + match.Length;
                }
            }

            // Finally, copy the remainder of the string
            if (lastPos < newText.Length)
                newParagraph.Inlines.Add(new Run(newText.Substring(lastPos)));

            // remove old
            richTextBox.Document.Blocks.Clear();
            // add new
            richTextBox.Document.Blocks.Add(newParagraph);
        }

        private static Hyperlink CreateNewHyperlink(Match match)
        {
            try
            {
                Hyperlink link = new Hyperlink(new Run(match.Value))
                {
                    NavigateUri = new UriBuilder(match.Value).Uri,
                    ToolTip = "Click",
                };
                link.MouseLeftButtonDown += link_MouseUp;
                link.Cursor = Cursors.Hand;
                return link;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        static void link_MouseUp(object sender, MouseButtonEventArgs e)
        {
            Hyperlink link = (Hyperlink)sender;
            ShowLinkInBrowser(link);
        }

        private static void ShowLinkInBrowser(Hyperlink link)
        {
            if (link != null)
                System.Diagnostics.Process.Start(link.NavigateUri.ToString());
        }
    }
}
