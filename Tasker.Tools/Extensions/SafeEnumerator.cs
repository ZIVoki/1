using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace Tasker.Tools.Extensions
{
    public class SafeEnumerator<T> : IEnumerator<T>
    {
        // this is the (thread-unsafe)
        // enumerator of the underlying collection
        private readonly IEnumerator<T> _Inner;
        // this is the object we shall readerWriterLock on. 
        private readonly ReaderWriterLockSlim _readerWriterLock;

        public SafeEnumerator(IEnumerator<T> inner, ReaderWriterLockSlim readerWriterLock)
        {
            _Inner = inner;
            _readerWriterLock = readerWriterLock;
            // entering readerWriterLock in constructor
            _readerWriterLock.EnterReadLock();
        }

        #region Implementation of IDisposable

        public void Dispose()
        {
            // .. and exiting readerWriterLock on Dispose()
            // This will be called when foreach loop finishes
            _readerWriterLock.ExitReadLock();
        }

        #endregion

        #region Implementation of IEnumerator

        // we just delegate actual implementation
        // to the inner enumerator, that actually iterates
        // over some collection

        public bool MoveNext()
        {
            return _Inner.MoveNext();
        }

        public void Reset()
        {
            _Inner.Reset();
        }

        public T Current
        {
            get { return _Inner.Current; }
        }

        object IEnumerator.Current
        {
            get { return Current; }
        }

        #endregion
    }
}