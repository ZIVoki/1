﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Threading;

namespace Tasker.Tools.Extensions
{
/*
    [Serializable]
    public class ObservableCollectionEx<T> : ObservableCollection<T>//, ICloneable where T : ViewBase<>
    {
        public override event System.Collections.Specialized.NotifyCollectionChangedEventHandler CollectionChanged;

        public ObservableCollectionEx(IEnumerable<T> collection) : base(collection) {}

        public ObservableCollectionEx() {}

        public ObservableCollectionEx(List<T> list) : base(list) { }

        protected override void OnCollectionChanged(System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            // Be nice - use BlockReentrancy like MSDN said
            using (BlockReentrancy())
            {
                System.Collections.Specialized.NotifyCollectionChangedEventHandler eventHandler = CollectionChanged;
                if (eventHandler == null)
                    return;

                Delegate[] delegates = eventHandler.GetInvocationList();
                // Walk thru invocation list
                foreach (System.Collections.Specialized.NotifyCollectionChangedEventHandler handler in delegates)
                {
                    DispatcherObject dispatcherObject = handler.Target as DispatcherObject;
                    // If the subscriber is a DispatcherObject and different thread
                    if (dispatcherObject != null && dispatcherObject.CheckAccess() == false)
                    {
                        // Invoke handler in the target dispatcher's thread
                        dispatcherObject.Dispatcher.Invoke(DispatcherPriority.DataBind, handler, this, e);
                    }
                    else // Execute handler as is
                        handler(this, e);
                }
            }
        }

        public object Clone()
        {
            var tmpR = new T[this.Count];
            this.CopyTo(tmpR, 0);
            var clone = new ObservableCollectionEx<T>();
            foreach (var item in tmpR)
            {
                clone.Add(item);
            }
            return clone;
        }
    }
*/
}