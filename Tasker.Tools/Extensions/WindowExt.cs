﻿using System.Windows.Input;

namespace Tasker.Tools.Extensions
{
    /// <summary>
    /// On esc closable window
    /// </summary>
    public class WindowExt: System.Windows.Window
    {
        protected override void OnKeyUp(KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.Close();
            }
            if (e.KeyboardDevice.IsKeyDown(Key.RightCtrl) || e.KeyboardDevice.IsKeyDown(Key.LeftCtrl))
            {
                if (e.Key == Key.S)
                    SaveData();
            }
            base.OnKeyUp(e);
        }

        protected virtual void SaveData()
        {
        }
    }
}
