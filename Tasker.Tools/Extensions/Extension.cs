﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Tasker.Shared
{
    public static class Extensions
    {
        public static T ShallowCopy<T>(this T a)
        {
            if (a is IEnumerable<T>)
            {
                var b = a as dynamic;

                return b.Clone();
            }
            else
            {
                return a;
            }
        }
      
    }
}