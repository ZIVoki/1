﻿namespace Tasker.Tools.CustomEventArgs
{
    public class ContentEventArgs : System.EventArgs
    {
        public ContentEventArgs(object arg)
        {
            Arg = arg;
        }
        public object Arg;
    }
}
