﻿namespace Tasker.Tools.CustomEventArgs
{
    public class ContentsEventArgs : System.EventArgs
    {
        public ContentsEventArgs(object[] arg)
        {
            Args = arg;
        }
        public object[] Args;
    }
}
