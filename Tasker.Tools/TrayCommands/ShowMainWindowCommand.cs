﻿using System;
using System.Windows;
using System.Windows.Input;

namespace Tasker.Tools.TrayCommands
{
    public class ShowMainWindowCommand : ICommand
    {
        public void Execute(object parameter)
        {
            if (Application.Current.MainWindow.WindowState == WindowState.Minimized)
                Application.Current.MainWindow.WindowState = WindowState.Normal;
            else
                Application.Current.MainWindow.WindowState = WindowState.Minimized;
        }
        
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged;
    } 
}
