﻿using Tasker.Server.ServiceInterfaces;

namespace Tasker.Server.Services
{
    public class ConnectUser
    {
        public ConnectUser(user user)
        {
            Id = user.userId;
            Role = (Constants.Roles)user.roleId;
            Surname = user.lastName;
            NetWorkState = Constants.NetworkStates.Online;
        }

        public int Id { get; private set; }
        public Constants.Roles Role { get; private set; }
        public string SessionId { get; set; }
        public string Surname { get; set; }
        public IClientCallback Callback { get; set; }
        public Constants.NetworkStates NetWorkState { get ; set; }
    }
}