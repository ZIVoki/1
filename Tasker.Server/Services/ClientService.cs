﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using Microsoft.Office.Interop.Word;
using Tasker.Server.DTO;
using Tasker.Server.Managers;
using Tasker.Server.Managers.Logger;
using Tasker.Server.SecurityAndValidation.ServiceValidation;
using Tasker.Server.ServiceInterfaces;
using System.Data.Entity;

namespace Tasker.Server.Services
{
    [ServiceBehavior
            (InstanceContextMode = InstanceContextMode.PerSession, ConcurrencyMode = ConcurrencyMode.Multiple, IncludeExceptionDetailInFaults = true, UseSynchronizationContext = false)]
    public class ClientService : IClientInterface, IDisposable
    {
        protected internal ConnectUser CurrentUser;

        #region ConnectionAndStatuses

        public virtual LoginResultDTO Login(string login, string password)
        {
            // check connection 
            string errorMessage;
            if (!ConnectionCheckManager.CheckConnection(out errorMessage))
            {
                Log.Write(LogLevel.Fatal, errorMessage);
                return new LoginResultDTO { Success = false };
            }

            using (var context = new TaskerEntities())
            {
                // check login and password
                string passHash = UsersManager.GetMd5HashFromString(password);
                user newUser;

                if (String.IsNullOrEmpty(password))
                    newUser =
                        context.users.FirstOrDefault(
                            user => user.dLogin == login && user.statusId == (int)Constants.Statuses.Enabled);
                else
                {
#if DEBUG
                    newUser =
                        context.users.FirstOrDefault(
                            user => user.login == login && user.statusId == (int)Constants.Statuses.Enabled);
#else
                newUser =
                    context.users.Where(
                        user =>
                        user.login == login && user.password == passHash &&
                        user.statusId == (int) Constants.Statuses.Enabled).FirstOrDefault();
#endif
                }
                
                if (newUser == null) return new LoginResultDTO { Success = false };

                //check user role
                Type currentServiceType = GetType();
                //1 - admin, 2 - manage, 3 - worker
                if (currentServiceType == typeof (ManagerService) && newUser.roleId > 2)
                    return new LoginResultDTO {Success = false};
                if (currentServiceType == typeof (WorkerService) && newUser.roleId < 3)
                    return new LoginResultDTO {Success = false};

                // add new user to users list
                CurrentUser = UsersManager.Connect(newUser);

                //handle channel closing
                OperationContext.Current.Channel.Closing += UsersManager.LostConnection;

                string newMessage = string.Format("{2} User enter: {0} ({1})", CurrentUser.Id, CurrentUser.Surname, DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"));
                Console.WriteLine(newMessage);
                Console.WriteLine(ConnectedUserManager.GetCount());
                Log.Write(LogLevel.Info, newMessage);
                var role = (Constants.Roles)newUser.roleId;

                return new LoginResultDTO
                           {
                               Success = true,
                               Role = role,
                               User = UserDTO.UserDTOBuilder(newUser)
                           };
            }
        }

        public void Logoff()
        {
            if (CurrentUser != null)
            {
                string newMessage = string.Format("{2} User disconnected: {0} ({1})", CurrentUser.Id, CurrentUser.Surname,
                                              DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"));

                Log.Write(LogLevel.Info, newMessage);
                Console.WriteLine(newMessage);
                Console.WriteLine(ConnectedUserManager.GetCount());
                UsersManager.Disconnect(CurrentUser);
            }
            CurrentUser = null;

            //Close channel
            //            try
            //            {
            //                OperationContext.Current.Channel.Close();
            //            }
            //            catch (Exception ex)
            //            {
            //                Log.Write(LogLevel.Error, "Client service throw exception while logoff.", ex);
            //            }
            //            finally
            //            {
            //                OperationContext.Current.Channel.Abort();
            //            }
        }

        public TimeSpan? Ping(DateTime time)
        {
            return DateTime.Now - time;
        }

        #endregion

        #region files

        public int AddFileToTask(AttachFileDTO attachFile)
        {
            string message = String.Format("Adding new file {0} to task num {1}.", attachFile.Title, attachFile.TaskId);
            Console.WriteLine(message);
            Log.Write(LogLevel.Info, message);
            //validate file
            if (ClientOperationClientDataValidation.ValidateFile(attachFile))
            {
                // cut title if it more then 50
                if (attachFile.Title.Length > 50)
                {
                    attachFile.Title = attachFile.Title.Substring(0, 50);
                }

                using (var context = new TaskerEntities())
                {
                    //create file entity
                    attachFile efFile = new attachFile
                    {
                        taskId = attachFile.TaskId,
                        createdAt = attachFile.CreatedAt,
                        filepath = attachFile.FilePath,
                        isFinalFile = attachFile.IsFinalyFile,
                        title = attachFile.Title,
                        statusId = (int)Constants.Statuses.Enabled
                    };

                    context.attachFiles.ApplyChanges(efFile);
                    context.SaveChanges();

                    attachFile.Id = efFile.attachFileId;

                    UsersManager.AllAlerter.FileAdded(attachFile.TaskId, attachFile, CurrentUser);
                }

                return attachFile.Id;
            }
            return 0;
        }

        public AttachFileDTO GetFileData(int fileId)
        {
            using (var context = new TaskerEntities())
            {
                var attachedFile =
                    context.attachFiles.Include("task").Where(
                        f => f.attachFileId == fileId && f.statusId != (int)Constants.Statuses.Deleted).FirstOrDefault();
                if (attachedFile != null)
                {

                    return AttachFileDTO.FileDTOBuilder(attachedFile);
                }
            }
            return null;
        }

        public IEnumerable<AttachFileDTO> GetFilesData(int taskId)
        {
            var result = new List<AttachFileDTO>();

            using (var context = new TaskerEntities())
            {
                var attachFiles =
                    context.attachFiles.Include("task").Where(f => f.taskId == taskId && f.statusId != (int)Constants.Statuses.Deleted);

                foreach (var attachFile in attachFiles)
                {
                    result.Add(AttachFileDTO.FileDTOBuilder(attachFile));
                }

            }

            return result;
        }

        #endregion

        #region messages

        IEnumerable<MessageDTO> IClientInterface.GetUnreadMessages()
        {
            var result = new List<MessageDTO>();

            using (var context = new TaskerEntities())
            {
                var messages =
                    context.messages.Include("user").Include("task").Where(
                        m => m.arriveDate == null && m.toUserId == CurrentUser.Id);

                foreach (var message in messages)
                {
                    message.arriveDate = DateTime.Now;
                    context.messages.ApplyChanges(message);
                    result.Add(MessageDTO.MessageDTOBuilder(message));
                }

                context.SaveChanges();
            }

            return result;
        }

        public int SendMessage(MessageDTO newMessage)
        {
            //#if DEBUG
            //Thread.Sleep(TimeSpan.FromSeconds(3));
            //#endif

            int result = 0;
            //validate message
            if (ClientOperationClientDataValidation.ValidateMessage(newMessage))
            {
                using (var context = new TaskerEntities())
                {
                    user fromUser = context.users.FirstOrDefault(a => a.userId == CurrentUser.Id);

                    List<int> senders =
                        context.messages.Where(t => t.taskId == newMessage.TaskId)
                               .GroupBy(m => m.fromUserId)
                               .Select(k => k.Key)
                               .ToList();

                    if (fromUser != null)
                    {
                        newMessage.FromUser = UserDTO.UserDTOBuilder(fromUser);

                        //create message entity
                        var efMessage = new message
                                            {
                                                taskId = newMessage.TaskId,
                                                fromUserId = newMessage.FromUserId,
                                                toUserId = newMessage.ToUserId,
                                                text = newMessage.Text,
                                                statusId = (int)Constants.Statuses.Enabled,
                                                sendDate = DateTime.Now,
                                            };

                        context.messages.ApplyChanges(efMessage);
                        context.SaveChanges();
                        newMessage.Id = efMessage.messageId;
                        // Try to send message and set arriveDate if sended
                        //                        efMessage.arriveDate = UsersManager.SendMessageToUser(newMessage);
                        efMessage.arriveDate = UsersManager.SendMessageToUsers(newMessage, senders);
                        context.messages.ApplyChanges(efMessage);
                        context.SaveChanges();

                        result = efMessage.messageId;
                    }
                }
            }
            return result;
        }

        public IEnumerable<MessageDTO> GetMessages(int taskId)
        {
            var result = new List<MessageDTO>();

            using (var context = new TaskerEntities())
            {
                //get all messages for this user in task context
                var messages =
                    context.messages.Include("user").Include("task").Where(
                        m =>
                        m.taskId == taskId &&
                        m.arriveDate != null &&
                        m.statusId != (int)Constants.Statuses.Deleted);

                foreach (var message in messages)
                {
                    result.Add(MessageDTO.MessageDTOBuilder(message));
                }
            }

            return result;
        }

        #endregion

        #region taskType

        public TaskTypeDTO GetTaskType(int taskTypeId)
        {
            using (var context = new TaskerEntities())
            {
                var taskType =
                    context.taskTypes.FirstOrDefault(t => t.taskTypeId == taskTypeId);
                if (taskType != null)
                    return TaskTypeDTO.TaskTypeDTOBuilder(taskType);
            }
            return null;
        }

        public IEnumerable<TaskTypeDTO> GetTaskTypes()
        {
            var result = new List<TaskTypeDTO>();

            using (var context = new TaskerEntities())
            {
                var taskTypes = context.taskTypes.Where(tt => tt.statusId != (int)Constants.Statuses.Deleted);
                foreach (var taskType in taskTypes)
                {
                    result.Add(TaskTypeDTO.TaskTypeDTOBuilder(taskType));
                }
            }
            return result;
        }

        public UserTaskTypeDTO GetUserTaskType(int userTaskTypeId)
        {
            using (var context = new TaskerEntities())
            {
                var userTaskType =
                    context.userTaskTypes.Include("taskType").Include("user").FirstOrDefault(
                        t => t.userTaskTypeId == userTaskTypeId && t.statusId != (int)Constants.Statuses.Deleted);
                if (userTaskType != null)
                    return UserTaskTypeDTO.UserTaskTypeDTOBuilder(userTaskType);
            }
            return null;
        }

        public IEnumerable<UserTaskTypeDTO> GetUserTaskTypes(int taskType)
        {
            var result = new List<UserTaskTypeDTO>();

            using (var context = new TaskerEntities())
            {
                var userTaskTypes =
                    context.userTaskTypes.Include("taskType").Include("user").Where(
                        a => a.taskTypeId == taskType && a.statusId != (int)Constants.Statuses.Deleted);
                foreach (var userTaskType in userTaskTypes)
                {
                    result.Add(UserTaskTypeDTO.UserTaskTypeDTOBuilder(userTaskType));
                }
            }
            return result;
        }

        #endregion

        #region timeTable

        public IEnumerable<ShiftDTO> GetShifts()
        {
            var result = new List<ShiftDTO>();

            using (var context = new TaskerEntities())
            {
                IQueryable<shift> queryable = context.shifts.Where(a => a.statusId != (int)Constants.Statuses.Deleted);
                foreach (var shift in queryable)
                {
                    result.Add(ShiftDTO.ShiftDTOBuilder(shift));
                }
            }
            return result;
        }

        public ShiftDTO GetShift(int shiftId)
        {
            using (var context = new TaskerEntities())
            {
                var shift = context.shifts.FirstOrDefault(t => t.shiftId == shiftId);
                if (shift != null)
                    return ShiftDTO.ShiftDTOBuilder(shift);
            }
            return null;
        }

        public IEnumerable<TimeTableDTO> GetMyTimeTables(DateTime startDate, DateTime endDate)
        {
            var result = new List<TimeTableDTO>();

            using (var context = new TaskerEntities())
            {
                IQueryable<timeTable> timeTables = context.timeTables.Include("shift").Include("user").Where(
                                                                            a => a.userId == CurrentUser.Id &&
                                                                                 a.statusId != (int)Constants.Statuses.Deleted &&
                                                                                 a.endDate >= startDate.Date &&
                                                                                 a.endDate <= endDate.Date);

                foreach (var timeTable in timeTables)
                {
                    result.Add(TimeTableDTO.WorkersTimeTableDTOBuilder(timeTable));
                }
            }
            return result;
        }

        #endregion

        #region Board

        public virtual IEnumerable<FileVersionDTO> GetBoardFiles()
        {
            List<FileVersionDTO> result = new List<FileVersionDTO>();

            using (var context = new TaskerEntities())
            {
                List<fileVersion> filesVersions =
                    context.fileVersions.Include(t => t.file)
                        .Include(t => t.file.folder)
                        .Include(t => t.file.folder.folder1)
                        .Include(t => t.file.folder.folder1.folder1)
                        .Include(t => t.usersFileVersions)
                        .Where(a => a.file.statusId == (int)Constants.Statuses.Enabled &&
                                    a.statusId == (int)Constants.Statuses.Enabled)
                        .ToList();

                foreach (var versions in filesVersions.GroupBy(f => f.fileId))
                {
                    fileVersion lastFileVersion = versions.OrderByDescending(t => t.version).FirstOrDefault();
                    if (lastFileVersion == null) continue;

                    FileVersionDTO fileVersionDTO = FileVersionDTO.FileVersionDTOBuilder(lastFileVersion);

                    usersFileVersion lastVersionRead = lastFileVersion.usersFileVersions.FirstOrDefault(u => u.userId == CurrentUser.Id);
                    if (lastVersionRead != null && lastVersionRead.readDate.HasValue)
                        fileVersionDTO.ReadDate = lastVersionRead.readDate.Value;

                    result.Add(fileVersionDTO);
                }
            }

            return result;
        }

        public virtual FileVersionDTO GetBoardFileById(int fileId)
        {
            FileVersionDTO fileVersionDTO = null;
            using (var context = new TaskerEntities())
            {
                fileVersion fileVersion =
                    context.fileVersions.Include(t => t.file)
                        .Include(t => t.file.folder)
                        .Include(t => t.file.folder.folder1)
                        .Include(t => t.file.folder.folder1.folder1)
                        .Include(t => t.usersFileVersions)
                        .Where(a => a.fileId == fileId &&
                                    a.file.statusId == (int)Constants.Statuses.Enabled &&
                                    a.statusId == (int)Constants.Statuses.Enabled)
                        .OrderByDescending(t => t.version).FirstOrDefault();


                if (fileVersion != null)
                {
                    fileVersionDTO = FileVersionDTO.FileVersionDTOBuilder(fileVersion);

                    usersFileVersion lastVersionRead = fileVersion.usersFileVersions.FirstOrDefault(u => u.userId == CurrentUser.Id);
                    if (lastVersionRead != null && lastVersionRead.readDate.HasValue)
                        fileVersionDTO.ReadDate = lastVersionRead.readDate.Value;
                }

            }

            return fileVersionDTO;
        }

        public void SetBoardFileAsRead(int fileId)
        {
            using (TaskerEntities context = new TaskerEntities())
            {
                usersFileVersion newfileVersionReadItem =
                    context.usersFileVersions.Include(fv => fv.fileVersion)
                           .OrderByDescending(f => f.fileVersion.version)
                           .FirstOrDefault(
                               u => u.userId == CurrentUser.Id && u.fileVersion.fileId == fileId);
                if (newfileVersionReadItem == null)
                {
                    fileVersion currentFileVersion =
                        context.fileVersions.Where(t => t.fileId == fileId).OrderByDescending(f => f.version).
                            FirstOrDefault();
                    newfileVersionReadItem = new usersFileVersion
                        {
                            userId = CurrentUser.Id,
                            statusId = (int)Constants.Statuses.Enabled,
                            readDate = DateTime.Now,
                            fileVersionId = currentFileVersion.fileVersionId
                        };
                }
                else
                {
                    newfileVersionReadItem.readDate = DateTime.Now;
                }
                context.usersFileVersions.ApplyChanges(newfileVersionReadItem);

                // оповещаем менеджеров кто и что прочитал
                UserReadFileDTO userReadFileDTO = UserReadFileDTO.UserReadFileDTOBuilder(newfileVersionReadItem);
                UsersManager.ManagersAlerter.UserReadFile(userReadFileDTO, CurrentUser);

                context.SaveChanges();
            }
        }

        public FileVersionDTO UpdateBoardFile(int fileId)
        {
            FileVersionDTO newFileVersionDTO = null;

            using (TaskerEntities context = new TaskerEntities())
            {
                file fileToUpdate =
                    context.files.Include(f => f.fileVersions)
                                 .Include(fv => fv.folder)
                                 .Include(fvv => fvv.folder.folder1)
                                 .FirstOrDefault(f => f.fileId == fileId);
                if (fileToUpdate != null)
                {
                    newFileVersionDTO = DoUpdateBoardFile(fileToUpdate, context);
                }
            }

            if (newFileVersionDTO != null)
            {
                UsersManager.ManagersAlerter.NewBoardFileVersionAdded(newFileVersionDTO, CurrentUser);
                UsersManager.WorkerAlert.NewBoardFileVersionAdded(newFileVersionDTO, CurrentUser);
            }

            return newFileVersionDTO;
        }

        public void UpdateAllBoardFiles()
        {
            BackgroundWorker updateAsyncWorker = new BackgroundWorker();
            updateAsyncWorker.DoWork += updateAsyncWorker_DoWork;
            updateAsyncWorker.RunWorkerAsync();
        }

        void updateAsyncWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            UpdateAllBoardFilesAsync();
        }

        protected void UpdateAllBoardFilesAsync()
        {
            using (TaskerEntities context = new TaskerEntities())
            {
                List<file> eFiles =
                    context.files.Include(f => f.fileVersions).Include(fv => fv.folder).Include(
                        fvv => fvv.folder.folder1).Where(t => t.statusId == (int)Constants.Statuses.Enabled).ToList();

                foreach (var file in eFiles)
                {
                    var newFileVersionDTO = DoUpdateBoardFile(file, context);
                    if (newFileVersionDTO == null) continue;

                    UsersManager.ManagersAlerter.NewBoardFileVersionAdded(newFileVersionDTO);
                    UsersManager.WorkerAlert.NewBoardFileVersionAdded(newFileVersionDTO);
                }
            }
        }

        private FileVersionDTO DoUpdateBoardFile(file fileToUpdate, TaskerEntities context)
        {
            FileVersionDTO newFileVersionDTO = null;
            string oldSourceFilePath = GetOldSourceFilePath(fileToUpdate);
            FileInfo oldSourceFile = new FileInfo(oldSourceFilePath);
            FileInfo currentSourceFile = new FileInfo(fileToUpdate.fileSource);

            if (currentSourceFile.LastWriteTimeUtc > oldSourceFile.LastWriteTimeUtc)
            {
                // исходный файл документа был изменен
                string diffString = "";
                if (File.Exists(oldSourceFilePath) &&
                    Constants.AvailableDiffFormats.Contains(oldSourceFile.Extension))
                {
                    diffString = CretateDiff(oldSourceFilePath, fileToUpdate.fileSource);
                }

                fileVersion lastFileVersion = fileToUpdate.fileVersions.OrderByDescending(v => v.version).FirstOrDefault();
                fileVersion newFileVesion = CreateNewFileVesion(lastFileVersion, diffString);

                try
                {
                    CreatePrevDirectoryIfNotExist(fileToUpdate.filePath);

                    // сохраняем новую версию файла
                    File.Copy(currentSourceFile.FullName, oldSourceFile.FullName, true);
                    // сохраняем pdf если это word файл
                    SaveDocToPdf(fileToUpdate.fileSource, fileToUpdate.filePath);

                    context.fileVersions.ApplyChanges(newFileVesion);
                    context.SaveChanges();

                    // создаем записи о прочтении документа
                    SetUread(newFileVesion, context);
                }
                catch (Exception e)
                {
                    String message = String.Format("Ошибка во время обновленяи файла {0}", e.Message);
                    Console.WriteLine(message);
                    Log.Write(LogLevel.Error, message);
                }

                if (newFileVesion != null)
                {
                    // создаем новый DTO
                    newFileVersionDTO = FileVersionDTO.FileVersionDTOBuilder(newFileVesion);
                    foreach (var usersFileVersion in newFileVesion.usersFileVersions)
                    {
                        UserReadFileDTO userReadFileDTO = UserReadFileDTO.UserReadFileDTOBuilder(usersFileVersion);
                        newFileVersionDTO.UsersReadFile.Add(userReadFileDTO);
                    }
                }
            }

            return newFileVersionDTO;
        }

        private static void CreatePrevDirectoryIfNotExist(string currentSourceFile)
        {
            string fileDirName = Path.GetDirectoryName(currentSourceFile);
            string lastFileVersionDir = Path.Combine(fileDirName, Constants.LastVersionFolderName);
            bool exists = Directory.Exists(lastFileVersionDir);
            if (!exists)
            {
                DirectoryInfo directoryInfo = Directory.CreateDirectory(lastFileVersionDir);
                directoryInfo.Attributes = FileAttributes.Directory | FileAttributes.Hidden;
            }
        }

        private void SetUread(fileVersion newFileVesion, TaskerEntities context)
        {
            IQueryable<user> queryable = context.users.Where(u => u.statusId == (int)Constants.Statuses.Enabled);
            foreach (var user in queryable)
            {
                usersFileVersion usersFileVersion = new usersFileVersion
                    {
                        userId = user.userId,
                        fileVersionId = newFileVesion.fileVersionId,
                        statusId = (int)Constants.Statuses.Enabled
                    };

                newFileVesion.usersFileVersions.Add(usersFileVersion);
                //                context.usersFileVersions.ApplyChanges(usersFileVersion);
            }
            context.fileVersions.ApplyChanges(newFileVesion);

            context.SaveChanges();
        }

        private fileVersion CreateNewFileVesion(fileVersion lastFileVersion, string diffString)
        {
            fileVersion newFileVesion = new fileVersion
                {
                    fileId = lastFileVersion.fileId,
                    version = lastFileVersion.version + 1,
                    statusId = (int)Constants.Statuses.Enabled
                };
            FileInfo fileInfo = new FileInfo(lastFileVersion.file.filePath);
            // запоминаем дату последней записи
            newFileVesion.changesDate = fileInfo.LastWriteTime;
            newFileVesion.versionsDiff = diffString;

            return newFileVesion;
        }

        private void SaveDocToPdf(string wordSourcePath, string destPath)
        {
            const string availableConvertFormats = ".doc .docx";
            FileInfo wordFile = new FileInfo(wordSourcePath);

            if (!availableConvertFormats.Contains(wordFile.Extension)) return;

            // Create a new Microsoft Word application object
            Application word = new Application();

            // C# doesn't have optional arguments so we'll need a dummy value
            object oMissing = System.Reflection.Missing.Value;

            //            word.Visible = false;
            word.ScreenUpdating = false;

            // Cast as Object for word Open method
            Object filename = (Object)wordFile.FullName;

            // Use the dummy value as a placeholder for optional arguments
            Document doc = word.Documents.Open(ref filename, ref oMissing,
                ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                ref oMissing, ref oMissing, ref oMissing, ref oMissing);
            doc.Activate();

            object outputFileName = destPath;
            /*
                        if (wordFile.Extension == ".doc")
                             outputFileName = wordFile.FullName.Replace(".doc", ".pdf");
                        else
                            outputFileName = wordFile.FullName.Replace(".docx", ".pdf");
            */

            object fileFormat = WdSaveFormat.wdFormatPDF;

            // Save document into PDF Format
            doc.SaveAs(ref outputFileName,
                ref fileFormat, ref oMissing, ref oMissing,
                ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                ref oMissing, ref oMissing, ref oMissing, ref oMissing);

            // Close the Word document, but leave the Word application open.
            // doc has to be cast to type _Document so that it will find the
            // correct Close method.                
            object saveChanges = WdSaveOptions.wdDoNotSaveChanges;
            ((_Document)doc).Close(ref saveChanges, ref oMissing, ref oMissing);
            doc = null;


            // word has to be cast to type _Application so that it will find
            // the correct Quit method.
            ((_Application)word).Quit(ref oMissing, ref oMissing, ref oMissing);
            word = null;
        }

        private string CretateDiff(string oldSourceFilePath, string sourceFilePath)
        {
            /*
            XmlWriter diffgramWriter = new XmlTextWriter();
            XmlDiff xmldiff = new XmlDiff(XmlDiffOptions.IgnoreChildOrder |
                                    XmlDiffOptions.IgnoreNamespaces |
                                    XmlDiffOptions.IgnorePrefixes);
            bool bIdentical = xmldiff.Compare(oldSourceFilePath, sourceFilePath, false, diffgramWriter);
            diffgramWriter.Close();
            */

            return "";
        }

        public string GetOldSourceFilePath(file file)
        {
            string directoryName = Path.GetDirectoryName(file.filePath);
            string sourceFileName = Path.GetFileName(file.fileSource);
            string sourceFilePath = Path.Combine(directoryName, Constants.LastVersionFolderName,
                                                 sourceFileName);
            return sourceFilePath;
        }

        #endregion

        public IEnumerable<AttendanceLogItemDTO> GetMyLatesByDates(DateTime startDate, DateTime endDate)
        {
            return GetUserLatesByDates(startDate, endDate, CurrentUser.Id);
        }

        private IEnumerable<AttendanceLogItemDTO> GetUserLatesByDates(DateTime startDate, DateTime endDate, int userId)
        {
            DateTime end = endDate.Date.AddDays(1).Subtract(TimeSpan.FromSeconds(1));
            DateTime start = startDate.AddDays(-1);
            List<AttendanceLogItemDTO> data = new List<AttendanceLogItemDTO>();
            userId = CurrentUser.Id;

            using (var context = new TaskerEntities())
            {
                IQueryable<attendanceLog> log = context.attendanceLogs.Include(a => a.user).Include(a => a.timeTable).Include(a => a.timeTable.shift)
                                                                        .Where(c => c.EventDateTime <= end &&
                                                                                    c.EventDateTime >= start &&
                                                                                    c.userId == userId &&
                                                                                    (c.statusId == (int)Constants.Statuses.UserEnter || c.statusId == (int)Constants.Statuses.UserLeave))
                                                                        .OrderBy(a => a.EventDateTime);

                IQueryable<timeTable> schedule = context.timeTables.Include(tt => tt.user).
                                              Where(
                                                  c =>
                                                  c.startDate >= start &&
                                                  c.userId == userId &&
                                                  c.endDate <= end && c.statusId != (int)Constants.Statuses.Deleted)
                                             .OrderBy(a => a.startDate);

                PrepareDataForLateAnalys(log, data, schedule);
            }

            List<AttendanceLogItemDTO> result = GetLates(startDate, data).OrderBy(t => t.User.LastName).ToList(); ;

            return result;
        }

        protected static List<AttendanceLogItemDTO> GetLates(DateTime startDate, List<AttendanceLogItemDTO> data)
        {
            List<AttendanceLogItemDTO> result = new List<AttendanceLogItemDTO>();
            for (int i = 0; i < data.Count; i++)
            {
                if (data[i].EventDateTime < startDate)
                    continue;
                if (
                    data[i].TimeTable != null
                    && data[i].EventDateTime > data[i].TimeTable.StartDate.AddMinutes(10)
                    && data[i].TimeTable.StatusId != (int)Constants.Statuses.Deleted
                    )
                {
                    if (i == 0)
                    {
                        result.Add(data[i]);
                        continue;
                    }

                    if (data[i - 1].User.Id != data[i].User.Id)
                    {
                        result.Add(data[i]);
                    }
                    else
                    {
                        if (data[i - 1].StatusId == (int)Constants.Statuses.UserLeave)
                        {
                            if (data[i - 1].TimeTable != null)
                            {
                                if (data[i - 1].TimeTable.Id == data[i].TimeTable.Id)
                                {
                                    continue;
                                }
                            }
                            result.Add(data[i]);
                        }
                    }
                }
            }

            return result;
        }

        protected static void PrepareDataForLateAnalys(IEnumerable<attendanceLog> log, List<AttendanceLogItemDTO> data, IEnumerable<timeTable> schedule)
        {
            // для того чтобы учесть тех, кто зашел и сразу вышел по какой либо причине в течении 10 минут до начала смены
            const int minutesBeforeSHiftStart = -60;
            foreach (attendanceLog item in log)
            {
                data.Add(AttendanceLogItemDTO.AttendanceLogItemDTOBuilder(item));
            }

            foreach (timeTable timeTable in schedule)
            {
                List<AttendanceLogItemDTO>.Enumerator enumerator = data.GetEnumerator();

                while (enumerator.MoveNext() && enumerator.Current.User.Id != timeTable.userId)
                {
                }
                if (enumerator.Current == null) break;
                while (enumerator.Current.User.Id == timeTable.userId)
                {
                    if (enumerator.Current.EventDateTime >= timeTable.startDate.AddMinutes(minutesBeforeSHiftStart) &&
                        enumerator.Current.EventDateTime <= timeTable.endDate)
                    {
                        enumerator.Current.TimeTable = TimeTableDTO.WorkersTimeTableDTOBuilder(timeTable);
                    }
                    if (!enumerator.MoveNext())
                    {
                        break;
                    }
                }
            }
        }

        public void Dispose()
        {
            Logoff();
        }
    }
}