﻿using System.ServiceModel;
using Tasker.Server.ServiceInterfaces.Admin;

namespace Tasker.Server.Services
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class AdminService : ManagerService, IAdminInterface
    {

    }
}
