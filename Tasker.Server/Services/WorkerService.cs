﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using Tasker.Server.DTO;
using Tasker.Server.Managers;
using Tasker.Server.Managers.Logger;
using Tasker.Server.ServiceInterfaces.Worker;
using System.Data.Entity;

namespace Tasker.Server.Services
{
    [ServiceBehavior
            (InstanceContextMode = InstanceContextMode.PerSession, ConcurrencyMode = ConcurrencyMode.Multiple, IncludeExceptionDetailInFaults = true, UseSynchronizationContext = false)]
    public class WorkerService : ClientService, IWorkerInterface
    {
        #region Users

        public UserDTO GetManager(int userId)
        {
            using (var context = new TaskerEntities())
            {
                var manager =
                    context.users.FirstOrDefault(
                        c =>
                        c.roleId == (int)Constants.Roles.Manager && c.userId == userId &&
                        c.statusId != (int)Constants.Statuses.Deleted);

                if (manager != null)
                {
                    UserDTO.UserDTOBuilder(manager);
                }
            }
            return null;
        }

        #endregion

        #region connectionAndStatuses

        public void GoForLunchRequest(int? managerId = null)
        {
            Log.Write(LogLevel.Info, string.Format("User request dinner {0}", CurrentUser.Surname));
            using (var context = new TaskerEntities())
            {
                timeTable timeTable = context.timeTables.Include("shift").Include("user").FirstOrDefault(
                                                        t =>
                                                        t.statusId != (int) Constants.Statuses.Deleted && 
                                                        t.userId == CurrentUser.Id &&
                                                        t.startDate <= DateTime.Now && 
                                                        t.endDate >= DateTime.Now);

                //  в случае если работник работает в данный моментв рамках расписания 
                if (timeTable != null)
                {
                    // проверяем ходил ли он на обед до этого
                    if (timeTable.firstLunchStart.HasValue && timeTable.secondLunchStart.HasValue)
                    {
                        Log.Write(LogLevel.Info, string.Format("User request dinner {0} DENIED", CurrentUser.Surname));
                        // если ходил -  запрещаем
                        UsersManager.WorkerAlert.LunchRequestResult(false, CurrentUser.Id);
                        return;
                    }

                    // уходить на обед за 35 минут до конца смены нельзя
                    TimeSpan timeSpan = timeTable.endDate.Subtract(DateTime.Now);
                    if (timeSpan.TotalMinutes <= 35)
                    {
                        Log.Write(LogLevel.Info, string.Format("User request dinner {0} DENIED", CurrentUser.Surname));
                        UsersManager.WorkerAlert.LunchRequestResult(false, CurrentUser.Id);
                        return;
                    }
                }

                var userFrom = context.users.FirstOrDefault(c => c.userId == CurrentUser.Id);

                if (userFrom != null)
                {
                    var userFromDTO = UserDTO.UserDTOBuilder(userFrom);
                    UsersManager.ManagersAlerter.SendNewLunchRequest(userFromDTO, managerId);
                }
            }
        }

        public void Comeback()
        {
          /*List<ConnectUser> connectionsUserById = ConnectedUserManager.GetConnectionsUserById(CurrentUser.Id);

            foreach (var connectUser in connectionsUserById)
            {
                if (connectUser != null && connectUser.NetWorkState != Constants.NetworkStates.Online)
                {
                    connectUser.NetWorkState = Constants.NetworkStates.Online;
                }
            }*/
            if (CurrentUser.NetWorkState == Constants.NetworkStates.Online) return;

            UsersManager.AllAlerter.UserStateChanged(CurrentUser.Id, Constants.NetworkStates.Online, CurrentUser);
            AttendanceLogManager.SaveAttendanceItem(CurrentUser.Id, Constants.Statuses.UserEnter);
            UsersManager.ManagersAlerter.UserComeBack(CurrentUser.Id, base.CurrentUser);
            using (var context = new TaskerEntities())
            {
                var currentTask =
                    context.tasks.Include(t => t.taskType).Include(t => t.user).Include(t => t.user1).Include(t => t.customer).
                        Include(t => t.customer.channel).Where(
                            t =>
                            t.currentUserId == CurrentUser.Id &&
                            (t.statusId == (int)Constants.Statuses.InProcess || 
                            t.statusId == (int)Constants.Statuses.Disabled ||
                            t.statusId == (int)Constants.Statuses.Enabled || 
                            t.statusId == (int)Constants.Statuses.Planing ||
                            t.statusId == (int)Constants.Statuses.SendedForReview)).
                        OrderByDescending(t => t.priority).ThenBy(t => t.creationDate).FirstOrDefault();

                if (currentTask != null)
                    TaskManager.ChangeTaskStatus(currentTask.taskId, Constants.Statuses.InProcess, base.CurrentUser.Id);

            }

            using (var context = new TaskerEntities())
            {
                timeTable timeTable = context.timeTables.Include(t => t.shift).Include(t => t.user).FirstOrDefault(
                                                                            t =>
                                                                            t.statusId != (int) Constants.Statuses.Deleted && t.userId == CurrentUser.Id &&
                                                                            t.startDate <= DateTime.Now && t.endDate >= DateTime.Now);
                if (timeTable != null)
                {
                    if (timeTable.firstLunchEnd.HasValue)
                        timeTable.secondLunchEnd = DateTime.Now;
                    else
                        timeTable.firstLunchEnd = DateTime.Now;
                    context.timeTables.ApplyChanges(timeTable);
                    context.SaveChanges();

                    UsersManager.AllAlerter.TimeTableUpdated(TimeTableDTO.WorkersTimeTableDTOBuilder(timeTable), CurrentUser);
                }
            }
        }

        #endregion

        #region task

        public TaskDTO GetMyCurrentTask()
        {
            using (var context = new TaskerEntities())
            {
                var currentTask =
                    context.tasks.Include(t => t.taskType)
                                 .Include(t => t.user)
                                 .Include(t => t.user1)
                                 .Include(t => t.customer)
                                 .Include(t => t.customer.channel)
                                 .FirstOrDefault(t => t.currentUserId == CurrentUser.Id &&
                                                 (t.statusId == (int) Constants.Statuses.InProcess || t.statusId == (int) Constants.Statuses.Planing));
                if (currentTask != null)
                {
                    TaskDTO taskDTO = TaskDTO.TaskDTOBuilder(currentTask);
                    TaskManager.SetTaskInProcessTime(taskDTO, context);
                    return taskDTO;
                }
            }
            return null;
        }

        public IEnumerable<TaskDTO> GetMyTasks()
        {
            List<TaskDTO> result = new List<TaskDTO>();
            using (var context = new TaskerEntities())
            {
                var currentTasks =
                    context.tasks.Include(t => t.taskType)
                                 .Include(t => t.user)
                                 .Include(t => t.user1)
                                 .Include(t => t.customer)
                                 .Include(t => t.customer.channel)
                                 .Where(t => t.currentUserId == CurrentUser.Id && 
                                     (t.statusId != (int) Constants.Statuses.Closed && t.statusId != (int) Constants.Statuses.Deleted));
                if (!currentTasks.Any()) return result;

                foreach (var task in currentTasks)
                {
                    TaskDTO taskDTO = TaskDTO.TaskDTOBuilder(task);
                    TaskManager.SetTaskInProcessTime(taskDTO, context);
                    result.Add(taskDTO);
                }
            }
            return result;
        }

        public IEnumerable<TaskDTO> GetTasksByDate(DateTime date)
        {
            List<TaskDTO> result = new List<TaskDTO>();
            DateTime startDate = date.Date;
            DateTime endDate = date.Date.AddDays(1).AddSeconds(-1);
            using (var context = new TaskerEntities())
            {
                var currentTasks =
                    context.tasks.Include(t => t.taskType).Include(t => t.user).Include(t => t.user1).Include(
                        t => t.customer).Include(t => t.customer.channel).Where(
                            t =>
                            t.currentUserId == CurrentUser.Id &&
                            t.creationDate >= startDate && t.creationDate <= endDate &&
                            t.statusId != (int)Constants.Statuses.Deleted);
                if (!currentTasks.Any()) return result;

                foreach (var task in currentTasks)
                {
                    TaskDTO taskDTO = TaskDTO.TaskDTOBuilder(task);
                    TaskManager.SetTaskInProcessTime(taskDTO, context);
                    result.Add(taskDTO);
                }
            }
            return result;
        }

        public TimeSpan GetTaskTotalHoursByDates(DateTime startDate, DateTime endDate)
        {
            TimeSpan result = new TimeSpan(0);
            using (var context = new TaskerEntities())
            {
                IQueryable<long> planned =
                        context.tasks.Where(
                           t => t.currentUserId == CurrentUser.Id && 
                           (t.statusId == (int) Constants.Statuses.Closed && t.creationDate <= endDate && t.creationDate >= startDate))
                           .Select(t => t.plannedTime);

                foreach (var l in planned)
                {
                   result = result.Add(TimeSpan.FromTicks(l));
                }
            }
            return result;
        }

        public void AcceptTask(int taskId)
        {
            if (TaskManager.ChangeTaskStatusIfNotClose(taskId, Constants.Statuses.InProcess, base.CurrentUser.Id))
                UsersManager.ManagersAlerter.TaskAccepted(taskId, CurrentUser.Id, DateTime.Now);
        }

        public void SendForReview(TaskDTO taskDTO, int managerId)
        {
            TaskForAcceptListManager.AddNewTask(taskDTO, CurrentUser.Id);
            TaskManager.ChangeTaskStatus(taskDTO.Id, Constants.Statuses.SendedForReview, CurrentUser.Id);
        }
        
        public IEnumerable<UserTaskTypeDTO> GetMyUserTaskTypes(int userId)
        {
            var result = new List<UserTaskTypeDTO>();
            
            if(userId == CurrentUser.Id && CurrentUser.Role == Constants.Roles.Worker)
            {
                using (var context = new TaskerEntities())
                {
                    foreach (var userTaskType in context.userTaskTypes.Include("user").Include("taskType").Where(a => a.userId == userId && a.statusId != (int)Constants.Statuses.Deleted))
                    {
                        result.Add(UserTaskTypeDTO.UserTaskTypeDTOBuilder(userTaskType));
                    }
                }
            }
            
            return result;
        }
       
        #endregion
    }

}