﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Objects;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using Tasker.Server.DTO;
using Tasker.Server.Managers;
using Tasker.Server.Managers.Logger;
using Tasker.Server.SecurityAndValidation.ServiceValidation;
using Tasker.Server.ServiceInterfaces.Manager;
using Statuses = Tasker.Server.Constants.Statuses;
using System.Data.Entity;

namespace Tasker.Server.Services
{
    [ServiceBehavior

    (InstanceContextMode = InstanceContextMode.PerSession, ConcurrencyMode = ConcurrencyMode.Multiple, IncludeExceptionDetailInFaults = true, UseSynchronizationContext = false)]
    public class
        ManagerService : ClientService, IManagerInterface
    {
        public void GetSleepRequests()
        {
            TaskForAcceptListManager.SendRequestAgain(CurrentUser);
            LunchListManager.SendRequestsAgain(CurrentUser);
        }

        #region tasks

        public int AddTask(TaskDTO taskDTO)
        {
            Debug.WriteLine(string.Format("Creating new task {0}", taskDTO.Title));

            if (!ManagerOperationClientDataValidation.ValidateTask(taskDTO, CurrentUser.Id))
                return 0;

            if (taskDTO.CreationDate.Year == 1) taskDTO.CreationDate = DateTime.Now;

            using (var context = new TaskerEntities())
            {
                var task = new task
                {
                    statusId = (int)taskDTO.Status,
                    title = taskDTO.Title,
                    deadLine = taskDTO.DeadLine,
                    customerId = taskDTO.CustomerId,
                    taskTypeId = taskDTO.TaskTypeId,
                    description = taskDTO.Description,
                    currentUserId = taskDTO.CurrentWorkerId,
                    priority = taskDTO.Priority,
                    plannedTime = taskDTO.PlannedTime.Ticks,
                    creationDate = taskDTO.CreationDate,
                    creatorId = CurrentUser.Id,
                    comments = taskDTO.Comment
                };

                context.tasks.ApplyChanges(task);
                context.SaveChanges();

                taskDTO.Id = task.taskId;
                WriteTaskHistory(taskDTO, context, CurrentUser.Id);
                context.SaveChanges();

                Debug.WriteLine(string.Format("Informating managers about new task {0}", taskDTO.Title));
                UsersManager.ManagersAlerter.TaskAdded(taskDTO, base.CurrentUser);
                Debug.WriteLine(string.Format("Informating worker about new task {0}", taskDTO.Title));
                UsersManager.WorkerAlert.NewTask(taskDTO);
            }
            BackgroundWorker updateTAskUserBackgroundWorker = new BackgroundWorker();
            updateTAskUserBackgroundWorker.DoWork += UpdateUserTasksBackgroundWorkerDoWork;
            updateTAskUserBackgroundWorker.RunWorkerAsync(taskDTO.CurrentWorkerId);

#if DEBUG
            Thread.Sleep(TimeSpan.FromSeconds(5));
#endif

            return taskDTO.Id;
        }

        /// <summary>
        /// inside e workerID
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void UpdateUserTasksBackgroundWorkerDoWork(object sender, DoWorkEventArgs e)
        {
            if (e.Argument == null) return;
            int workerID = (int)e.Argument;

            if (workerID != 0)
                UpdateUserTask(workerID, CurrentUser.Id);
        }

        #region taskStatistic

        public List<TaskStatisticRecordDTO> GetTaskStatistic(int taskId)
        {
            var lst = new List<TaskStatisticRecordDTO>();
            using (var context = new TaskerEntities())
            {
                var changes = context.taskChanges.Include(t => t.user).Where(t => t.taskId == taskId);
                foreach (var taskChanx in changes)
                {
                    var dto = TaskStatisticRecordDTO.TaskStatisticRecordDTOBuilder(taskChanx);
                    lst.Add(dto);
                }
            }

            return lst;
        }

        public List<TaskStatisticRecordDTO> GetTaskStatisticByUserId(int userId, DateTime startDate, DateTime endDate)
        {
            List<TaskStatisticRecordDTO> taskStatisticRecordDtos = new List<TaskStatisticRecordDTO>();
            using (var context = new TaskerEntities())
            {
                var changes =
                    context.taskChanges.Include(t => t.user).Include(t => t.task).Where(
                        t =>
                        t.userId == userId &&
                        t.task.statusId == (int)Statuses.Closed &&
                        t.task.creationDate >= startDate &&
                        t.task.creationDate <= endDate);

                foreach (var taskChanx in changes)
                {
                    var dto = TaskStatisticRecordDTO.TaskStatisticRecordDTOBuilder(taskChanx);
                    taskStatisticRecordDtos.Add(dto);
                }
            }

            return taskStatisticRecordDtos;
        }

        #endregion

        public void UpdateTask(TaskDTO taskDTO)
        {
            ManagerOperationClientDataValidation.ValidateTask(taskDTO);
            int? oldWorkerId = null;
            int? newWorkerId = taskDTO.CurrentWorkerId;

            using (var context = new TaskerEntities())
            {
                var efTask = context.tasks.FirstOrDefault(a => a.taskId == taskDTO.Id);

                if (efTask != null)
                {
                    efTask.statusId = (int)taskDTO.Status;
                    if (taskDTO.Status == Statuses.Closed)
                    {
                        DateTime nowDateTime = DateTime.Now;
                        efTask.completedAt = nowDateTime;
                        efTask.duration = nowDateTime.Subtract(efTask.creationDate).Ticks;
                        oldWorkerId = efTask.currentUserId;
                        newWorkerId = null;
                    }
                    efTask.title = taskDTO.Title;
                    efTask.deadLine = taskDTO.DeadLine;
                    efTask.plannedTime = taskDTO.PlannedTime.Ticks;
                    efTask.customerId = taskDTO.CustomerId;
                    efTask.description = taskDTO.Description;
                    efTask.priority = taskDTO.Priority;
                    efTask.comments = taskDTO.Comment;
                    efTask.taskTypeId = taskDTO.TaskTypeId;

                    if (efTask.currentUserId != taskDTO.CurrentWorkerId)
                        oldWorkerId = efTask.currentUserId;
                    if (taskDTO.CurrentWorkerId.HasValue)
                        efTask.currentUserId = taskDTO.CurrentWorker.Id;
                    else if (taskDTO.Status == Statuses.Closed && !efTask.currentUserId.HasValue)
                    {
                        taskChanx lastStatisticUser =
                            context.taskChanges.Where(t => t.taskId == taskDTO.Id && t.userId != null).OrderByDescending(
                                t => t.endDate).FirstOrDefault();
                        if (lastStatisticUser != null)
                            efTask.currentUserId = lastStatisticUser.userId;
                    }

                    context.tasks.ApplyChanges(efTask);
                    WriteTaskHistory(taskDTO, context, CurrentUser.Id);
                    context.SaveChanges();
                }

                TaskManager.SetTaskInProcessTime(taskDTO, context);
            }

            UsersManager.AllAlerter.TaskUpdated(taskDTO);

            if (taskDTO.Status == Statuses.Closed)
            {
                TaskForAcceptListManager.RemoveTask(taskDTO.Id, taskDTO.CurrentWorker.Id, true);
            }

            if (oldWorkerId.HasValue)
            {
                UsersManager.WorkerAlert.TaskUnset(oldWorkerId.Value, taskDTO.Id);
                BackgroundWorker updateTAskUserBackgroundWorker = new BackgroundWorker();
                updateTAskUserBackgroundWorker.DoWork += UpdateUserTasksBackgroundWorkerDoWork;
                updateTAskUserBackgroundWorker.RunWorkerAsync(oldWorkerId);
            }
            if (newWorkerId.HasValue)
            {
                BackgroundWorker updateTAskUserBackgroundWorker = new BackgroundWorker();
                updateTAskUserBackgroundWorker.DoWork += UpdateUserTasksBackgroundWorkerDoWork;
                updateTAskUserBackgroundWorker.RunWorkerAsync(newWorkerId);
            }
        }

        public void SetTaskReviewResult(int taskId, bool isClosed, string comment)
        {
            TaskForAcceptListManager.RemoveTask(taskId);
            TaskDTO taskDto;
            using (var context = new TaskerEntities())
            {
                task efTask =
                    context.tasks.Include(t => t.taskType).Include(t => t.user).Include(t => t.user1).Include(t => t.customer)
                    .Include(t => t.customer.channel).FirstOrDefault(t => t.taskId == taskId);
                taskDto = TaskDTO.TaskDTOBuilder(efTask);
            }

            if (taskDto.CurrentWorkerId != null)
                UsersManager.WorkerAlert.SetTaskReviewResult(isClosed, taskDto.CurrentWorkerId.Value);

            if (isClosed)
            {
                taskDto.Status = Statuses.Closed;
                if (!string.IsNullOrEmpty(comment)) taskDto.Comment = comment;
            }
            else
            {
                var messageDto = new MessageDTO
                                            {
                                                Task = taskDto,
                                                ToUser = taskDto.CurrentWorker,
                                                SendDate = DateTime.Now,
                                                Text = String.Format("{0}: {1}.", "Задача переоткрыта, причина", comment)
                                            };
                SendMessage(messageDto);
                taskDto.Status = Statuses.InProcess;
            }

            // убираем сообщение о задаче на других клиентах данного юзера
            List<ConnectUser> currentUserConnections = ConnectedUserManager.GetCurrentUserConnections();
            foreach (var currentUserConnection in currentUserConnections)
            {
                if (OperationContext.Current.SessionId == currentUserConnection.SessionId) continue;
                ((IManagerCallback)currentUserConnection.Callback).TaskForReviewCancel(taskDto);
            }

            UpdateTask(taskDto);
        }

        public static void UpdateUserTask(int workerId, int currentUserId)
        {
            using (var context = new TaskerEntities())
            {
                var userTasksQuery = context.tasks.Include(t => t.taskType).Include(t => t.user).Include(t => t.user1).Include(t => t.customer).Include(
                        t => t.customer.channel).Where(
                        t =>
                        t.currentUserId == workerId &&
                        (t.statusId == (int)Statuses.InProcess ||
                        t.statusId == (int)Statuses.Disabled ||
                        t.statusId == (int)Statuses.Enabled ||
                        t.statusId == (int)Statuses.Planing ||
                        t.statusId == (int)Statuses.SendedForReview)).
                        OrderByDescending(t => t.priority).ThenBy(t => t.creationDate);

                List<task> userTasks = userTasksQuery.ToList();

                user user = context.users.FirstOrDefault(u => u.userId == workerId);
                UserDTO userDTO = UserDTO.UserDTOBuilder(user);

                if (!userTasks.Any())
                {
                    if (user == null) return;
                    UsersManager.ManagersAlerter.WorkerFree(userDTO, currentUserId);
                    // exit becouse no any more tasks
                    return;
                }

                // set whait new tasks
                IEnumerable<task> planingTasksIds =
                    userTasks.Skip(1);
                foreach (var task in planingTasksIds)
                {
                    if (task.statusId == (int)Statuses.SendedForReview) continue;
                    if (task.statusId == (int)Statuses.Disabled) continue;
                    Debug.WriteLine(task);
                    TaskManager.ChangeTaskStatus(task.taskId, Statuses.Disabled, currentUserId);
                }

                task firstTask = userTasks.FirstOrDefault();

                if (firstTask.statusId == (int)Statuses.InProcess)
                    // skip everything as it was becouse main task at the top
                    return;

                // set top task as current
                if (firstTask.statusId != (int)Statuses.Planing)
                    TaskManager.ChangeTaskStatus(firstTask.taskId, Statuses.Planing, currentUserId);
                TaskDTO taskDTO = TaskDTO.TaskDTOBuilder(firstTask);

                TaskManager.SetTaskInProcessTime(taskDTO, context);
                UsersManager.ManagersAlerter.WorkerBusy(taskDTO.CurrentWorker, taskDTO);
                UsersManager.WorkerAlert.TaskSet(taskDTO);
            }
        }

        public static void WriteTaskHistory(TaskDTO taskDto, TaskerEntities context, int editByUser)
        {
            // exit if task does not exist in db
            if (taskDto.Id == 0) return;

            //find and update last history item
            taskChanx lastChangesItem =
                context.taskChanges.Where(t => t.taskId == taskDto.Id).OrderByDescending(t => t.startDate).FirstOrDefault();
            if (lastChangesItem != null)
            {
                DateTime newEndDate = DateTime.Now;
                lastChangesItem.endDate = newEndDate;
                lastChangesItem.duration = newEndDate.Subtract(lastChangesItem.startDate).Ticks;

                context.taskChanges.ApplyChanges(lastChangesItem);
            }

            //create new history iten
            int? userId;
            if (taskDto.CurrentWorkerId != null) { userId = taskDto.CurrentWorkerId.Value; }
            else { userId = taskDto.CurrentWorkerId; }

            var newChange = new taskChanx
                                      {
                                          taskId = taskDto.Id,
                                          statusId = taskDto.StatusId,
                                          userId = userId,
                                          editBy = editByUser,
                                          startDate = DateTime.Now
                                      };
            if (lastChangesItem != null)
            {
                newChange.previousStatusId = lastChangesItem.statusId;
                newChange.previousUserId = lastChangesItem.previousUserId;
            }

            context.taskChanges.ApplyChanges(newChange);
        }

        public IEnumerable<TaskDTO> GetTasks()
        {
            var result = new List<TaskDTO>();

            using (var context = new TaskerEntities())
            {
                var tasks =
                    context.tasks.Include(t => t.taskType).Include(t => t.user).Include(t => t.user1).Include(
                        t => t.customer).Include(t => t.customer.channel).Where(c => c.statusId != (int)Statuses.Deleted).OrderBy(t => t.taskId);
                foreach (var task in tasks)
                {
                    result.Add(TaskDTO.TaskDTOBuilder(task));
                }
            }
            return result;
        }

        /// <summary>
        /// Take only last 200 tasks
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TaskDTO> GetTasks200Top()
        {
            var result = new List<TaskDTO>();

            using (var context = new TaskerEntities())
            {
                var tasks = context.tasks.Include(t => t.taskType).Include(t => t.user).Include(t => t.user1).Include(
                        t => t.customer).Include(t => t.customer.channel).Where(c => c.statusId != (int)Statuses.Deleted).OrderByDescending(t => t.taskId).Take(200);
                foreach (var task in tasks)
                {
                    result.Add(TaskDTO.TaskDTOBuilder(task));
                }
            }
            return result;
        }

        public IEnumerable<TaskDTO> GetClosedTasksByWorkerAndDate(int workerId, DateTime startDate, DateTime endDate)
        {
            var result = new List<TaskDTO>();

            using (var context = new TaskerEntities())
            {
                //get task id list in wich worker involved
                var taskIds =
                    context.taskChanges.Where(a => a.userId == workerId).Select(a => a.taskId);

                //get this tasks
                var tasks =
                    context.tasks.Include(t => t.taskType).Include(t => t.user).Include(t => t.user1).Include(
                        t => t.customer).Include(t => t.customer.channel).Where(
                            c =>
                            c.statusId == (int)Statuses.Closed &&
                            c.creationDate >= startDate &&
                            c.creationDate <= endDate &&
                            c.currentUserId == workerId &&
                            taskIds.Contains(c.taskId));

                foreach (var task in tasks)
                {
                    Debug.WriteLine(task.taskId);
                    result.Add(TaskDTO.TaskDTOBuilder(task));
                }
            }
            return result;
        }

        public IEnumerable<TaskDTO> GetClosedTasks()
        {
            var result = new List<TaskDTO>();

            using (var context = new TaskerEntities())
            {
                //get this tasks
                var tasks =
                    context.tasks.Include(t => t.taskType).Include(t => t.user).Include(t => t.user1).Include(
                        t => t.customer).Include(t => t.customer.channel).Where(
                        c => c.statusId == (int)Statuses.Closed);

                foreach (var task in tasks)
                {
                    result.Add(TaskDTO.TaskDTOBuilder(task));
                }
            }
            return result;
        }

        public IEnumerable<TaskDTO> GetActiveTasks()
        {
            var result = new List<TaskDTO>();

            using (var context = new TaskerEntities())
            {
                //get this tasks
                var tasks =
                    context.tasks.Include(t => t.taskType).Include(t => t.user).Include(t => t.user1).Include(
                        t => t.customer).Include(t => t.customer.channel).Where(
                        c => c.statusId != (int)Statuses.Closed);

                foreach (var task in tasks)
                {
                    result.Add(TaskDTO.TaskDTOBuilder(task));
                }
            }
            return result;
        }

        public IEnumerable<TaskDTO> GetUserTasks(int userId)
        {
            var result = new List<TaskDTO>();

            using (var context = new TaskerEntities())
            {
                //get this tasks
                var tasks =
                    context.tasks.Include(t => t.taskType).Include(t => t.user).Include(t => t.user1).Include(
                        t => t.customer).Include(t => t.customer.channel).Where(
                        c => c.creatorId == userId);

                foreach (var task in tasks)
                {
                    result.Add(TaskDTO.TaskDTOBuilder(task));
                }
            }
            return result;
        }

        public IEnumerable<TaskDTO> GetTasksByDates(DateTime startDate, DateTime endDate)
        {
            var result = new List<TaskDTO>();

            using (var context = new TaskerEntities())
            {

                //get this tasks
                var tasks =
                    context.tasks.Include(t => t.taskType).Include(t => t.user).Include(t => t.user1).Include(
                        t => t.customer).Include(t => t.customer.channel).Where(
                        c => c.creationDate >= startDate && c.creationDate <= endDate);

                foreach (var task in tasks)
                {
                    result.Add(TaskDTO.TaskDTOBuilder(task));
                }
            }
            return result;
        }

        public void CheckInTask(TaskDTO task)
        {
            new BlockedResource(task, base.CurrentUser);
        }

        public void CheckOutTask(TaskDTO tsk)
        {
            var res = BlockedResource.GetResource(Constants.BlockedResourceType.Task, tsk.Id);
            if (res != null)
            {
                res.UnlockResource();
            }
        }

        public TaskDTO GetTask(int taskId)
        {
            using (var context = new TaskerEntities())
            {
                var task =
                    context.tasks.Include(t => t.taskType).Include(t => t.user).Include(t => t.user1).Include(
                        t => t.customer).Include(t => t.customer.channel).FirstOrDefault(c => c.taskId == taskId && c.statusId != (int)Statuses.Deleted);

                if (task != null)
                {
                    return TaskDTO.TaskDTOBuilder(task);
                }
            }
            return null;
        }

        #endregion

        #region users

        public void LunchRequestResult(bool changeState, int workerId)
        {
            UsersManager.WorkerAlert.LunchRequestResult(changeState, workerId);
            LunchListManager.RemoveRequest(workerId);
            if (changeState)
            {
                ChangeState(Constants.NetworkStates.Lunch, workerId);

                using (var context = new TaskerEntities())
                {
                    int taskId =
                        context.tasks.Where(a => a.currentUserId == workerId && a.statusId == (int)Statuses.InProcess).
                            Select(b => b.taskId).FirstOrDefault();

                    if (taskId != 0)
                        TaskManager.ChangeTaskStatus(taskId, Statuses.Disabled, base.CurrentUser.Id);

                    timeTable timeTable = context.timeTables.Include("shift").Include("user").FirstOrDefault(
                                                                    t =>
                                                                    t.statusId != (int)Statuses.Deleted && t.userId == workerId &&
                                                                    t.startDate <= DateTime.Now && t.endDate >= DateTime.Now);

                    if (timeTable != null && timeTable.startDate.Date == DateTime.Now.Date)
                    {
                        if (timeTable.firstLunchStart.HasValue)
                            timeTable.secondLunchStart = DateTime.Now;
                        else
                            timeTable.firstLunchStart = DateTime.Now;
                        context.timeTables.ApplyChanges(timeTable);
                        context.SaveChanges();

                        UsersManager.AllAlerter.TimeTableUpdated(TimeTableDTO.WorkersTimeTableDTOBuilder(timeTable), CurrentUser);
                    }
                }

                string message = String.Format("User {0} RELEASE to lunch", workerId);
                Console.WriteLine(message);
                Log.Write(LogLevel.Info, message);
            }
            else
            {
                string message = String.Format("User {0} banned for lunch", workerId);
                Console.WriteLine(message);
                Log.Write(LogLevel.Info, message);
            }
        }

        public void ChangeState(Constants.NetworkStates state, int workerId = 0)
        {
            workerId = workerId != 0 ? workerId : CurrentUser.Id;

            var connectedUser = ConnectedUserManager.GetConnectionsUserById(workerId);
            foreach (var connectUser in connectedUser)
            {
                if (connectUser == null) continue;
                connectUser.NetWorkState = state;
            }

            UsersManager.AllAlerter.UserStateChanged(workerId, state, CurrentUser);
        }

        public IEnumerable<UserDTO> GetWorkers()
        {
            var result = new List<UserDTO>();

            using (var context = new TaskerEntities())
            {
                var users = context.users;
                foreach (var user in users)
                {
                    var dto = UserDTO.UserDTOBuilder(user);

                    List<ConnectUser> connectionsUserById = ConnectedUserManager.GetConnectionsUserById(dto.Id);
                    foreach (var connectUser in connectionsUserById)
                    {
                        if (connectUser == null)
                        {
                            dto.NetworkState = Constants.NetworkStates.Offline;
                        }
                        else
                        {
                            dto.NetworkState = connectUser.NetWorkState;
                        }
                    }

                    result.Add(dto);
                }
            }

            return result;
        }

        public UserDTO GetWorker(int userId)
        {
            using (var context = new TaskerEntities())
            {
                var customer =
                    context.users.FirstOrDefault(c => c.userId == userId && c.statusId != (int)Statuses.Deleted);
                if (customer != null)
                    return UserDTO.UserDTOBuilder(customer);
            }
            return null;
        }

        public void CheckInWorker(UserDTO userDTO)
        {
            new BlockedResource(userDTO, base.CurrentUser);
        }

        public void CheckOutWorker(UserDTO userDTO)
        {
            if (userDTO.Id == 0)
            {
                Log.Write(LogLevel.Error, "While CheckOutWorker UserDTO Id == 0");
                return;
            }
            BlockedResource blockedResource = BlockedResource.GetResource(Constants.BlockedResourceType.Worker, userDTO.Id);
            if (blockedResource != null)
            {
                blockedResource.UnlockResource();
            }
            UsersManager.ManagersAlerter.WorkerFree(userDTO, CurrentUser.Id);
        }

        #endregion

        #region AttendanceLog
        public AttendanceLogItemDTO GetAttendanceLogItem(int itemId)
        {
            using (var context = new TaskerEntities())
            {
                var item =
                    context.attendanceLogs.Include("user").Include("status").Include("timeTable").Include("timeTable.shift").FirstOrDefault(
                        c => c.itemId == itemId && c.statusId != (int)Statuses.Deleted);
                if (item != null)
                    return AttendanceLogItemDTO.AttendanceLogItemDTOBuilder(item);
            }
            return null;
        }

        public IEnumerable<AttendanceLogItemDTO> GetAttendanceLogByDate(DateTime day)
        {
            var start = day.Date;
            var end = day.Date.AddDays(1).Subtract(TimeSpan.FromSeconds(1));

            var result = new List<AttendanceLogItemDTO>();
            using (var context = new TaskerEntities())
            {
                IQueryable<attendanceLog> log;


                log = context.attendanceLogs.Include("user").Include("status").Include("timeTable").Include(
                    "timeTable.shift").Where(c => c.statusId != (int)Statuses.Deleted &&
                                                  c.EventDateTime <= end && c.EventDateTime >= start).OrderBy(
                                                      a => a.EventDateTime);

                foreach (var item in log)
                {
                    result.Add(AttendanceLogItemDTO.AttendanceLogItemDTOBuilder(item));
                }
            }

            return result;
        }

        public IEnumerable<AttendanceLogItemDTO> GetLatesByDates(DateTime startDate, DateTime endDate)
        {
            DateTime start = startDate.Date;
            DateTime end = endDate.Date.AddDays(1).Subtract(TimeSpan.FromSeconds(1));
            List<AttendanceLogItemDTO> data = new List<AttendanceLogItemDTO>();

            using (var context = new TaskerEntities())
            {
                IQueryable<attendanceLog> log = context.attendanceLogs.Include(a => a.user).Include(a => a.timeTable).Include(a => a.timeTable.shift)
                                                                    .Where(c => c.EventDateTime <= end &&
                                                                                c.EventDateTime >= start &&
                                                                                (c.statusId == (int)Statuses.UserEnter || c.statusId == (int)Statuses.UserLeave))
                                                                    .OrderBy(a => a.userId).ThenBy(a => a.EventDateTime);

                IQueryable<timeTable> schedule = context.timeTables.Include(tt => tt.user)
                                                                    .Where(c =>
                                                                            c.startDate >= start &&
                                                                            c.endDate <= end &&
                                                                            c.statusId != (int)Statuses.Deleted)
                                                                    .OrderBy(a => a.userId).ThenBy(a => a.startDate);

                PrepareDataForLateAnalys(log, data, schedule);
            }

            List<AttendanceLogItemDTO> result = GetLates(startDate, data).OrderBy(t => t.User.LastName).ToList(); ;

            return result;
        }

        #endregion

        #region taskType

        public int AddTaskType(TaskTypeDTO taskType)
        {
            ManagerOperationClientDataValidation.ValidateTaskType(taskType);

            using (var context = new TaskerEntities())
            {
                //create taskType entity
                var type = new taskType
                {
                    completionTime = taskType.CompletionTime.Ticks,
                    description = taskType.Description,
                    title = taskType.Title,
                    statusId = (int)Statuses.Enabled,
                };

                //context.taskTypes.Attach(type);
                context.taskTypes.ApplyChanges(type);
                context.SaveChanges();

                //alert managers and workers
                taskType.Id = type.taskTypeId;
                UsersManager.AllAlerter.TaskTypeAdded(taskType, base.CurrentUser);
            }
            return taskType.Id;
        }

        public void UpdateTaskType(TaskTypeDTO taskType)
        {
            ManagerOperationClientDataValidation.ValidateTaskType(taskType);
            using (var context = new TaskerEntities())
            {
                var efTaskType = context.taskTypes.Where(a => a.taskTypeId == taskType.Id).FirstOrDefault();

                efTaskType.completionTime = taskType.CompletionTime.Ticks;
                efTaskType.description = taskType.Description;
                efTaskType.title = taskType.Title;
                efTaskType.statusId = (int)taskType.Status;

                //context.taskTypes.Attach(efTaskType);
                context.taskTypes.ApplyChanges(efTaskType);
                context.SaveChanges();

                UsersManager.AllAlerter.TaskTypeUpdated(taskType, base.CurrentUser);
            }
        }

        public int AddUserTaskType(UserTaskTypeDTO userTaskType)
        {
            ManagerOperationClientDataValidation.ValidateUserTaskType(userTaskType);

            using (var context = new TaskerEntities())
            {
                //create userTaskType entity
                var type = new userTaskType
                {
                    completionTime = userTaskType.CompletionTime.Ticks,
                    taskTypeId = userTaskType.TaskTypeId,
                    userId = userTaskType.UserId,
                    statusId = (int)Statuses.Enabled,
                };

                //context.userTaskTypes.Attach(type);
                context.userTaskTypes.ApplyChanges(type);
                context.SaveChanges();

                userTaskType.Id = type.userTaskTypeId;
                UsersManager.AllAlerter.UserTaskTypeAdded(userTaskType, base.CurrentUser);
            }
            return userTaskType.Id;
        }

        public void UpdateUserTaskType(UserTaskTypeDTO userTaskType)
        {
            ManagerOperationClientDataValidation.ValidateUserTaskType(userTaskType);

            using (var context = new TaskerEntities())
            {
                var efUserTaskType = context.userTaskTypes.Where(a => a.userTaskTypeId == userTaskType.Id).FirstOrDefault();

                if (efUserTaskType != null)
                {
                    efUserTaskType.completionTime = userTaskType.CompletionTime.Ticks;
                    efUserTaskType.taskTypeId = userTaskType.TaskTypeId;
                    //efUserTaskType.userId = userTaskType.UserId;
                    efUserTaskType.statusId = (int)userTaskType.Status;

                    //context.userTaskTypes.Attach(efUserTaskType);
                    context.userTaskTypes.ApplyChanges(efUserTaskType);
                    context.SaveChanges();
                }

                UsersManager.AllAlerter.UserTaskTypeUpdated(userTaskType, base.CurrentUser);
            }
        }

        public IEnumerable<UserTaskTypeDTO> GetUserTaskTypesByUser(int userId)
        {
            var result = new List<UserTaskTypeDTO>();

            using (var context = new TaskerEntities())
            {
                foreach (var userTaskType in context.userTaskTypes.Include("user").Include("taskType").Where(a => a.userId == userId && a.statusId != (int)Statuses.Deleted))
                {
                    result.Add(UserTaskTypeDTO.UserTaskTypeDTOBuilder(userTaskType));
                }
            }
            return result;
        }


        #endregion

        #region customers

        public int AddCustomer(CustomerDTO customer)
        {
            ManagerOperationClientDataValidation.ValidateCustomer(customer);
            using (var context = new TaskerEntities())
            {
                //create userTaskType entity
                var type = new customer
                {
                    description = customer.Description,
                    channelId = customer.Channel.Id,
                    title = customer.Title,
                    statusId = (int)Statuses.Enabled,
                };

                //context.customers.Attach(type);
                context.customers.ApplyChanges(type);
                context.SaveChanges();

                customer.Id = type.customerId;
                UsersManager.ManagersAlerter.CustomerAdded(customer, base.CurrentUser);
            }
            return customer.Id;
        }

        public void UpdateCustomer(CustomerDTO customer)
        {
            ManagerOperationClientDataValidation.ValidateCustomer(customer);

            using (var context = new TaskerEntities())
            {
                var efCustomer = context.customers.Where(a => a.customerId == customer.Id).FirstOrDefault();

                if (efCustomer != null)
                {
                    efCustomer.description = customer.Description;
                    efCustomer.title = customer.Title;
                    efCustomer.maxWorkers = customer.MaxWorkers;
                    efCustomer.statusId = (int)customer.Status;
                    efCustomer.channelId = customer.Channel.Id;

                    context.customers.ApplyChanges(efCustomer);
                    context.SaveChanges();
                }

                UsersManager.AllAlerter.CustomerUpdated(customer, base.CurrentUser);
            }
        }

        public CustomerDTO GetCustomer(int customerId)
        {
            using (var context = new TaskerEntities())
            {
                var customer =
                    context.customers.Include(t => t.channel).FirstOrDefault(
                        c => c.customerId == customerId && c.statusId != (int)Statuses.Deleted);
                if (customer != null)
                    return CustomerDTO.CustomerDTOBuilder(customer);
            }
            return null;
        }

        public IEnumerable<CustomerDTO> GetCustomers()
        {
            var result = new List<CustomerDTO>();

            using (var context = new TaskerEntities())
            {
                var customers = context.customers.Include(t => t.channel).Where(c => c.statusId != (int)Statuses.Deleted);
                foreach (var customer in customers)
                {
                    result.Add(CustomerDTO.CustomerDTOBuilder(customer));
                }
            }

            return result;
        }

        #region CustomersTaskTypes

        public IEnumerable<CustomerTaskTypeDTO> GetCustomersTaskTypes()
        {
            var result = new List<CustomerTaskTypeDTO>();

            using (var context = new TaskerEntities())
            {
                var customersTaskTypes = context.customersTaskTypes.Include("customer").Include("taskType").Where(
                        t => t.statusId == (int)Statuses.Enabled &&
                             t.customer.statusId == (int)Statuses.Enabled &&
                             t.taskType.statusId == (int)Statuses.Enabled);
                foreach (var item in customersTaskTypes)
                {
                    result.Add(CustomerTaskTypeDTO.CustomerTaskTypeDTOBuilder(item));
                }
            }

            return result;
        }

        public IEnumerable<CustomerTaskTypeDTO> GetCustomersTaskTypesByCustomerId(int customerId)
        {
            var result = new List<CustomerTaskTypeDTO>();

            using (var context = new TaskerEntities())
            {
                var customersTaskTypes =
                    context.customersTaskTypes.Include("customer").Include("taskType").Where(
                        t => t.statusId == (int)Statuses.Enabled &&
                             t.customer.statusId == (int)Statuses.Enabled &&
                             t.taskType.statusId == (int)Statuses.Enabled &&
                             t.customerId == customerId);
                foreach (var item in customersTaskTypes)
                {
                    result.Add(CustomerTaskTypeDTO.CustomerTaskTypeDTOBuilder(item));
                }
            }

            return result;
        }

        public int AddCustomersTaskTypes(CustomerTaskTypeDTO customerTaskTypeDto)
        {
            using (var context = new TaskerEntities())
            {
                var customersTaskType = new customersTaskType
                                            {
                                                customerId = customerTaskTypeDto.Customer.Id,
                                                taskTypeId = customerTaskTypeDto.TaskType.Id,
                                                statusId = (int)Statuses.Enabled
                                            };

                context.customersTaskTypes.ApplyChanges(customersTaskType);

                context.SaveChanges();
                customerTaskTypeDto.Id = customersTaskType.customersTaskTypesId;
            }

            UsersManager.ManagersAlerter.CustomerTaskTypeAdded(customerTaskTypeDto);
            return customerTaskTypeDto.Id;
        }

        public void UpdateCustomersTaskTypes(CustomerTaskTypeDTO customerTaskTypeDto)
        {
            using (var context = new TaskerEntities())
            {
                customersTaskType customersTaskType = context.customersTaskTypes.FirstOrDefault(t => t.customersTaskTypesId == customerTaskTypeDto.Id);

                if (customersTaskType != null)
                {
                    customersTaskType.customerId = customerTaskTypeDto.Customer.Id;
                    customersTaskType.taskTypeId = customerTaskTypeDto.TaskType.Id;
                    customersTaskType.statusId = (int)customerTaskTypeDto.Status;

                    context.customersTaskTypes.ApplyChanges(customersTaskType);
                    context.SaveChanges();
                }
            }

            UsersManager.ManagersAlerter.CustomerTaskTypeUpdated(customerTaskTypeDto);
        }

        #endregion


        #endregion

        #region Channels

        public ChannelDTO GetChannel(int channelId)
        {
            using (var context = new TaskerEntities())
            {
                var channel =
                    context.channels.FirstOrDefault(
                        c => c.channelId == channelId && c.statusId != (int)Statuses.Deleted);
                if (channel != null)
                    return ChannelDTO.ChannelDTOBuilder(channel);
            }
            return null;
        }

        public IEnumerable<ChannelDTO> GetChannels()
        {
            var result = new List<ChannelDTO>();

            using (var context = new TaskerEntities())
            {
                var channels = context.channels.Where(c => c.statusId != (int)Statuses.Deleted);
                foreach (var channel in channels)
                {
                    result.Add(ChannelDTO.ChannelDTOBuilder(channel));
                }
            }

            return result;
        }

        #endregion

        #region TimeTable

        public int AddTimeTable(TimeTableDTO timeTableDTO)
        {
            ManagerOperationClientDataValidation.ValidateTimeTable(timeTableDTO);
            using (var context = new TaskerEntities())
            {
                //create timeTable entity
                var timeTable = new timeTable
                {
                    endDate = timeTableDTO.EndDate,
                    startDate = timeTableDTO.StartDate,
                    firstLunchEnd = timeTableDTO.FirstLunchEnd,
                    firstLunchStart = timeTableDTO.FirstLunchStart,
                    secondLunchEnd = timeTableDTO.SecondLunchEnd,
                    secondLunchStart = timeTableDTO.SecondLunchStart,
                    shiftId = timeTableDTO.ShiftId,
                    userId = timeTableDTO.UserId,
                    statusId = (int)Statuses.Enabled,
                    isResp = timeTableDTO.IsResp
                };

                //context.timeTables.Attach(timeTable);
                context.timeTables.ApplyChanges(timeTable);
                context.SaveChanges();

                timeTableDTO.Id = timeTable.timeTableId;
                UsersManager.AllAlerter.TimeTableAdded(timeTableDTO, base.CurrentUser);
            }
            return timeTableDTO.Id;
        }

        public void UpdateTimeTable(TimeTableDTO updatedTimeTable)
        {
            ManagerOperationClientDataValidation.ValidateTimeTable(updatedTimeTable);

            using (var context = new TaskerEntities())
            {
                var table = context.timeTables.Where(a => a.timeTableId == updatedTimeTable.Id).FirstOrDefault();

                if (table != null)
                {
                    table.endDate = updatedTimeTable.EndDate;
                    table.startDate = updatedTimeTable.StartDate;
                    table.firstLunchEnd = updatedTimeTable.FirstLunchEnd;
                    table.firstLunchStart = updatedTimeTable.FirstLunchStart;
                    table.secondLunchEnd = updatedTimeTable.SecondLunchEnd;
                    table.secondLunchStart = updatedTimeTable.SecondLunchStart;
                    table.shiftId = updatedTimeTable.ShiftId;
                    //table.userId = updatedTimeTable.UserId;
                    table.isResp = updatedTimeTable.IsResp;
                    if (updatedTimeTable.Status == Statuses.Nothing)
                    {
                        table.statusId = (int)Statuses.Enabled;
                    }
                    else
                    {
                        table.statusId = (int)updatedTimeTable.Status;
                    }

                    context.timeTables.ApplyChanges(table);
                    context.SaveChanges();

                    UsersManager.AllAlerter.TimeTableUpdated(updatedTimeTable, base.CurrentUser);
                }
            }
        }

        public TimeTableDTO GetTimeTable(int timeTableId)
        {
            using (var context = new TaskerEntities())
            {
                var timeTable = context.timeTables.Include("shift").Include("user").FirstOrDefault(c => c.timeTableId == timeTableId && c.statusId != (int)Statuses.Deleted);
                if (timeTable != null)
                    return TimeTableDTO.WorkersTimeTableDTOBuilder(timeTable);
            }
            return null;
        }

        public IEnumerable<TimeTableDTO> GetTimeTables()
        {
            var result = new List<TimeTableDTO>();

            using (var context = new TaskerEntities())
            {
                var timeTables =
                    context.timeTables.Include("shift").Include("user").Where(c => c.statusId != (int)Statuses.Deleted)
                        .OrderBy(t => t.startDate);
                foreach (var timeTable in timeTables)
                {
                    result.Add(TimeTableDTO.WorkersTimeTableDTOBuilder(timeTable));
                }
            }

            return result;
        }

        public IEnumerable<TimeTableDTO> GetTimeTablesByDates(DateTime startDate, DateTime endDate)
        {
            var result = new List<TimeTableDTO>();

            using (var context = new TaskerEntities())
            {
                IQueryable<timeTable> timeTables = context.timeTables.Include("shift").Include("user").Where(
                    t => t.statusId != (int)Statuses.Deleted && t.startDate >= startDate && t.endDate <= endDate).
                    OrderBy(t => t.startDate);

                foreach (var timeTable in timeTables)
                {
                    result.Add(TimeTableDTO.WorkersTimeTableDTOBuilder(timeTable));
                }
            }

            return result;
        }

        public IEnumerable<TimeTableDTO> GetUserTimeTables(int userId, DateTime startDate, DateTime endDate)
        {
            var result = new List<TimeTableDTO>();

            using (var context = new TaskerEntities())
            {
                var timeTables =
                    context.timeTables.Include("shift").Include("user").Where(
                        c => c.userId == userId &&
                             c.statusId != (int)Statuses.Deleted &&
                              c.endDate >= startDate &&
                              c.endDate <= endDate).OrderBy(t => t.startDate);

                foreach (var timeTable in timeTables)
                {
                    result.Add(TimeTableDTO.WorkersTimeTableDTOBuilder(timeTable));
                }
            }
            return result;
        }

        public IEnumerable<TimeTableDTO> GetTimeTablesByDate(DateTime date)
        {
            var result = new List<TimeTableDTO>();
            DateTime minDate = date.Date;
            DateTime maxDate = minDate.AddDays(1).AddSeconds(-1);
            using (var context = new TaskerEntities())
            {
                IQueryable<timeTable> timeTables =
                    context.timeTables.Include("shift").Include("user").Where(
                        a => a.startDate <= maxDate &&
                             a.endDate >= minDate &&
                             a.statusId != (int)Statuses.Deleted).OrderBy(t => t.startDate);

                foreach (var timeTable in timeTables)
                {
                    result.Add(TimeTableDTO.WorkersTimeTableDTOBuilder(timeTable));
                }
            }
            return result;
        }

        public int AddShift(ShiftDTO shiftDTO)
        {
            ManagerOperationClientDataValidation.ValidateShift(shiftDTO);
            using (var context = new TaskerEntities())
            {
                //create shift entity
                var @shift = new shift
                {
                    startTime = shiftDTO.StartTime,
                    endTime = shiftDTO.EndTime,

                    statusId = (int)Statuses.Enabled
                };
                if (shiftDTO.LunchDuration.HasValue)
                    @shift.lunchDuration = shiftDTO.LunchDuration.Value.Ticks;

                //context.shifts.Attach(shift);
                context.shifts.ApplyChanges(shift);
                context.SaveChanges();


                shiftDTO.Id = shift.shiftId;
                UsersManager.ManagersAlerter.ShiftAdded(shiftDTO, base.CurrentUser);
            }
            return shiftDTO.Id;
        }

        public void UpdateShift(ShiftDTO shiftDTO)
        {
            ManagerOperationClientDataValidation.ValidateShift(shiftDTO);

            using (var context = new TaskerEntities())
            {
                var shift = context.shifts.Where(a => a.shiftId == shiftDTO.Id).FirstOrDefault();

                if (shift != null)
                {
                    shift.startTime = shiftDTO.StartTime;
                    shift.endTime = shiftDTO.EndTime;
                    if (shiftDTO.LunchDuration.HasValue)
                        shift.lunchDuration = shiftDTO.LunchDuration.Value.Ticks;
                    shift.statusId = (int)shiftDTO.Status;

                    //context.shifts.Attach(shift);
                    context.shifts.ApplyChanges(shift);
                    context.SaveChanges();

                    UsersManager.AllAlerter.ShiftUpdated(shiftDTO, base.CurrentUser);
                }
            }
        }

        public IEnumerable<TimeTableDTO> GetShiftTimeTables(int shiftId, DateTime date)
        {
            var result = new List<TimeTableDTO>();

            using (var context = new TaskerEntities())
            {
                var timeTables =
                    context.timeTables.Where(
                        c =>
                        c.shiftId == shiftId && c.startDate.Date == date.Date && c.statusId != (int)Statuses.Deleted).OrderBy(t => t.startDate);
                result.AddRange(timeTables.Select(a => TimeTableDTO.WorkersTimeTableDTOBuilder(a)));
            }

            return result;
        }

        public IEnumerable<UserDTO> GetUsersByShift(int shiftId, DateTime date)
        {
            var result = new List<UserDTO>();

            using (var context = new TaskerEntities())
            {
                var users =
                    context.timeTables.Include("shift").Include("user").Where(
                        c =>
                        c.shiftId == shiftId && c.startDate.Date == date.Date && c.statusId != (int)Statuses.Deleted).
                        Select(a => UserDTO.UserDTOBuilder(a.user));
                result.AddRange(users);
            }

            return result;
        }

        #endregion

        #region LoadStatistic

        public IEnumerable<LoadItemDTO> GetLoadStatistic(DateTime date, int minutesInterval)
        {
            List<LoadItemDTO> result = new List<LoadItemDTO>();
            date = date.Date;

            DateTime prevDate = date.AddDays(-1);
            DateTime nextDate = date.Date.AddDays(1).AddSeconds(-1);

            using (var context = new TaskerEntities())
            {
                List<taskChanx> taskChanxes =
                    context.taskChanges.Include(a => a.user)
                        .Where(t => t.startDate <= nextDate && t.endDate >= date)
                        .ToList();
                List<timeTable> timeTables =
                    context.timeTables.Include(a => a.user)
                        .Where(t => t.startDate <= nextDate && t.endDate >= date)
                        .ToList();
                List<attendanceLog> attendanceLog =
                    context.attendanceLogs.Include(a => a.user)
                        .Where(t => t.EventDateTime >= prevDate && t.EventDateTime <= nextDate)
                        .ToList();

                for (DateTime countTime = date.Date;
                    countTime < nextDate;
                    countTime = countTime.AddMinutes(minutesInterval))
                {
                    DateTime addMinutes = countTime.AddMinutes(minutesInterval);
                    var loadItemDTO = GetLoadItem(addMinutes, countTime, timeTables, taskChanxes, attendanceLog,
                        prevDate);

                    result.Add(loadItemDTO);
                }
            }

            return result;
        }

        public IEnumerable<LoadItemDTO> GetLoadStatisticByCustomer(DateTime startDate, DateTime endDate,
            int minutesInterval, int customerId)
        {
            List<LoadItemDTO> result = new List<LoadItemDTO>();
            DateTime prevDate = startDate.AddDays(-1);

            using (var context = new TaskerEntities())
            {

                List<taskChanx> taskChanxes =
                    context.taskChanges.Include(a => a.user)
                        .Include(a => a.task)
                        .Include(a => a.task.customer)
                        .Where(
                            t =>
                                t.startDate <= endDate && t.endDate >= startDate &&
                                (customerId == 0 || t.task.customerId == customerId))
                        .ToList();

                List<timeTable> timeTables =
                    context.timeTables.Include(a => a.user)
                        .Where(t => t.startDate <= endDate && t.endDate >= startDate)
                        .ToList();
                List<attendanceLog> attendanceLog =
                    context.attendanceLogs.Include(a => a.user)
                        .Where(t => t.EventDateTime >= prevDate && t.EventDateTime <= endDate)
                        .ToList();

                for (DateTime countTime = startDate.Date;
                    countTime < endDate;
                    countTime = countTime.AddMinutes(minutesInterval))
                {
                    DateTime addMinutes = countTime.AddMinutes(minutesInterval);
                    var loadItemDTO = GetLoadItem(addMinutes, countTime, timeTables, taskChanxes, attendanceLog,
                        prevDate);

                    result.Add(loadItemDTO);
                }
            }

            return result;
        }

        private static LoadItemDTO GetLoadItem(DateTime addMinutes, DateTime countTime,
            IEnumerable<timeTable> timeTables, IEnumerable<taskChanx> taskChanxes,
            IEnumerable<attendanceLog> attendanceLog, DateTime prevDate)
        {
            // дступные по рассписанию ресурсы (работники)
            int timeTableWorkers =
                timeTables.Count(
                    t =>
                        t.startDate < addMinutes && t.endDate > countTime &&
                        t.user.roleId == (int)Constants.Roles.Worker &&
                        t.statusId == (int)Statuses.Enabled);

            // id всех работников задачи которых находятся в разработке
            List<int> inWorkUsers =
                taskChanxes.Where(
                    t =>
                        t.startDate < addMinutes &&
                        t.endDate > countTime &&
                        t.userId.HasValue &&
                        (t.statusId == (int)Statuses.InProcess || t.statusId == (int)Statuses.Planing))
                    .GroupBy(t => t.userId)
                    .Select(t => t.Key.Value)
                    .ToList();

            // id всех работников которые зашли, но ещё не вышли из сети на данный момент
            IEnumerable<IGrouping<int, attendanceLog>> groupBy =
                attendanceLog.Where(
                    a =>
                        a.user.roleId == (int)Constants.Roles.Worker && a.EventDateTime < addMinutes &&
                        a.EventDateTime > prevDate).OrderBy(a => a.itemId).GroupBy(a => a.userId);

            List<int> inNetWorkUsers = new List<int>();
            foreach (IGrouping<int, attendanceLog> attendanceLogs in groupBy)
            {
                attendanceLog log = attendanceLogs.Last();
                if (log == null) continue;
                if (log.statusId == (int)Statuses.UserEnter || log.statusId == (int)Statuses.UserLunch)
                {
                    inNetWorkUsers.Add(attendanceLogs.Key);
                }
            }

            // теоритически ноль, потому что все кто работают должны быть в сети, но если нет
            int exceptCount = inWorkUsers.Except(inNetWorkUsers).Count();

            int workingUsers = inWorkUsers.Count - exceptCount;
            LoadItemDTO loadItemDTO = new LoadItemDTO(countTime, workingUsers, timeTableWorkers, inNetWorkUsers.Count);
            return loadItemDTO;
        }

        #endregion

        public IEnumerable<UserStatItemDTO> GetUserStatistic(int userId, DateTime startTime, DateTime endTime)
        {
            List<UserStatItemDTO> result = new List<UserStatItemDTO>();

            List<task> tasks;
            using (var context = new TaskerEntities())
            {
                tasks = context.tasks.Include(t => t.taskType).Include(t => t.user).Include(t => t.user1).Include(
                                    t => t.customer).Include(t => t.taskChanges)
                                    .Include(t => t.customer.channel)
                                    .Where(t => t.currentUserId == userId && t.creationDate >= startTime && t.creationDate <= endTime && t.statusId == (int) Statuses.Closed)
                                    .OrderBy(t => t.taskTypeId)
                                    .ToList();
            }

            IEnumerable<IGrouping<taskType, task>> groupBy = tasks.GroupBy(t => t.taskType);
            foreach (var taskSetView in groupBy)
            {
                long? sum = taskSetView.Sum(
                    task =>
                        task.taskChanges.Where(
                            taskChanx =>
                                taskChanx.statusId == (int) Statuses.InProcess ||
                                taskChanx.statusId == (int) Statuses.Planing)
                            .Where(t => t.duration.HasValue)
                            .Sum(taskCh => taskCh.duration) - task.plannedTime);
                int tasksCount = taskSetView.Count();

                if (!sum.HasValue) continue;
                long avgDuartion = (long) (sum/tasksCount);
                int  minsAvgDuration = (int) TimeSpan.FromTicks(avgDuartion).TotalMinutes;
                if (minsAvgDuration == 0) continue;
                result.Add(new UserStatItemDTO(taskSetView.Key, minsAvgDuration, tasksCount));
            }

            return result;
        }

        #region admin operation

        public int AddUser(UserDTO userDTO)
        {
            if (CurrentUser.Role != Constants.Roles.Administrator)
            {
                string accessDeniedWrongCredentials =
                    string.Format("User adding operation by {0}. Access denied - wrong credentials", CurrentUser.Surname);
                Log.Write(LogLevel.Warning, accessDeniedWrongCredentials);
                return 0;
            }

            ManagerOperationClientDataValidation.ValidateUser(userDTO);

            using (var context = new TaskerEntities())
            {
                //create user entity
                var user = new user
                               {
                                   firstName = userDTO.FirstName,
                                   lastName = userDTO.LastName,
                                   login = userDTO.Login,
                                   email = userDTO.Email,
                                   phone = userDTO.Phone,
                                   dLogin = userDTO.DomainName,
                                   lastActiveDate = userDTO.LastActiveDate,
                                   weeklyHours = userDTO.WeeklyHours,
                                   roleId = (int)userDTO.RoleId,
                                   statusId = (int)userDTO.Status,
                               };

                // set default credentials
                if (string.IsNullOrEmpty(user.login))
                    user.login = Guid.NewGuid().ToString();
                if (string.IsNullOrEmpty(user.password))
                    user.password = Guid.NewGuid().ToString();

                context.users.ApplyChanges(user);
                context.SaveChanges();

                CreateBoardForNewUser(context, user.userId);

                userDTO.Id = user.userId;
                UsersManager.ManagersAlerter.UserAdded(userDTO, base.CurrentUser);
            }
            return userDTO.Id;
        }

        private void CreateBoardForNewUser(TaskerEntities context, int newUserId)
        {
            List<fileVersion> filesVersions =
                context.fileVersions.Include(t => t.file)
                       .Include(t => t.file.folder)
                       .Include(t => t.file.folder.folder1)
                       .Include(t => t.file.folder.folder1.folder1)
                       .Include(t => t.usersFileVersions)
                       .Where(a => a.file.statusId == (int)Constants.Statuses.Enabled &&
                                   a.statusId == (int)Constants.Statuses.Enabled)
                       .ToList();

            foreach (var versions in filesVersions.GroupBy(f => f.fileId))
            {
                fileVersion lastFileVersion = versions.OrderByDescending(t => t.version).FirstOrDefault();
                if (lastFileVersion == null) continue;

                usersFileVersion usersFileVersion = new usersFileVersion
                   {
                       userId = newUserId,
                       fileVersionId = lastFileVersion.fileVersionId,
                       statusId = (int)Constants.Statuses.Enabled
                   };

                lastFileVersion.usersFileVersions.Add(usersFileVersion);
                context.fileVersions.ApplyChanges(lastFileVersion);
            }

            context.SaveChanges();
        }

        public void UpdateUser(UserDTO userDTO)
        {
            if (CurrentUser.Role != Constants.Roles.Administrator)
            {
                string accessDeniedWrongCredentials =
                    string.Format("User updating  operation by {0}. Access denied - wrong credentials", CurrentUser.Surname);
                Log.Write(LogLevel.Warning, accessDeniedWrongCredentials);
                return;
            }
            ManagerOperationClientDataValidation.ValidateUser(userDTO);

            using (var context = new TaskerEntities())
            {
                var efUser = context.users.Where(a => a.userId == userDTO.Id).FirstOrDefault();

                if (efUser == null)
                {
                    string userWithWasNotFound = string.Format("User {0} {1} with Id {2} was not found",
                                                               userDTO.LastName, userDTO.FirstName, userDTO.Id);
                    Log.Write(LogLevel.Error, userWithWasNotFound);
                    return;
                }

                efUser.firstName = userDTO.FirstName;
                efUser.lastName = userDTO.LastName;
                efUser.phone = userDTO.Phone;
                efUser.email = userDTO.Email;
                efUser.dLogin = userDTO.DomainName;
                efUser.lastActiveDate = userDTO.LastActiveDate;
                efUser.weeklyHours = userDTO.WeeklyHours;
                efUser.roleId = (int)userDTO.RoleId;
                efUser.statusId = (int)userDTO.Status;
                if (!string.IsNullOrEmpty(userDTO.Login))
                    efUser.login = userDTO.Login;

                context.users.ApplyChanges(efUser);
                context.SaveChanges();


                UsersManager.AllAlerter.UserUpdated(userDTO, base.CurrentUser);
            }
        }

        public void SetNewUserCredentials(UserDTO userDTO, string login, string password)
        {
            if (CurrentUser.Role != Constants.Roles.Administrator)
            {
                string accessDeniedWrongCredentials =
                    string.Format("User new credentials operation by {0}. Access denied - wrong credentials", CurrentUser.Surname);
                Log.Write(LogLevel.Warning, accessDeniedWrongCredentials);
                return;
            }

            using (var context = new TaskerEntities())
            {
                var efUser = context.users.FirstOrDefault(u => u.userId == userDTO.Id);
                if (efUser == null)
                {
                    string userWithWasNotFound = string.Format("User {0} {1} with Id {2} was not found",
                                                               userDTO.LastName, userDTO.FirstName, userDTO.Id);
                    Log.Write(LogLevel.Error, userWithWasNotFound);
                    return;
                }

                efUser.login = login;
                efUser.password = password;

                context.users.ApplyChanges(efUser);
                context.SaveChanges();
            }
        }

        #endregion

        #region Board

        public override IEnumerable<FileVersionDTO> GetBoardFiles()
        {
            List<FileVersionDTO> result = new List<FileVersionDTO>();

            using (var context = new TaskerEntities())
            {
                List<fileVersion> filesVersions =
                    context.fileVersions.Include(t => t.file)
                        .Include(t => t.file.folder)
                        .Include(t => t.file.folder.folder1)
                        .Include(t => t.file.folder.folder1.folder1)
                        .Include(t => t.usersFileVersions)
                        .Where(a => a.file.statusId == (int)Statuses.Enabled &&
                                    a.statusId == (int)Statuses.Enabled)
                        .ToList();

                foreach (var versions in filesVersions.GroupBy(f => f.fileId))
                {
                    int max = versions.Max(v => v.version);
                    fileVersion lastVersion = versions.FirstOrDefault(v => v.version == max);
                    if (lastVersion == null) continue;

                    FileVersionDTO fileVersionDTO = FileVersionDTO.FileVersionDTOBuilder(lastVersion);

                    usersFileVersion lastVersionRead =
                        lastVersion.usersFileVersions.FirstOrDefault(u => u.userId == CurrentUser.Id);
                    if (lastVersionRead != null && lastVersionRead.readDate.HasValue)
                        fileVersionDTO.ReadDate = lastVersionRead.readDate.Value;

                    foreach (var usersFileVersion in lastVersion.usersFileVersions)
                    {
                        UserReadFileDTO userReadFileDTO = UserReadFileDTO.UserReadFileDTOBuilder(usersFileVersion);
                        fileVersionDTO.UsersReadFile.Add(userReadFileDTO);
                    }

                    result.Add(fileVersionDTO);
                }
            }

            return result;
        }

        public override FileVersionDTO GetBoardFileById(int fileId)
        {
            FileVersionDTO fileVersionDTO = null;
            using (var context = new TaskerEntities())
            {
                fileVersion fileVersion =
                    context.fileVersions.Include(t => t.file)
                        .Include(t => t.file.folder)
                        .Include(t => t.file.folder.folder1)
                        .Include(t => t.file.folder.folder1.folder1)
                        .Include(t => t.usersFileVersions)
                        .Where(a => a.fileId == fileId &&
                                    a.file.statusId == (int)Statuses.Enabled &&
                                    a.statusId == (int)Statuses.Enabled)
                        .OrderByDescending(t => t.version).FirstOrDefault();


                if (fileVersion != null)
                {
                    fileVersionDTO = FileVersionDTO.FileVersionDTOBuilder(fileVersion);

                    usersFileVersion lastVersionRead =
                        fileVersion.usersFileVersions.FirstOrDefault(u => u.userId == CurrentUser.Id);
                    if (lastVersionRead != null && lastVersionRead.readDate.HasValue)
                        fileVersionDTO.ReadDate = lastVersionRead.readDate.Value;

                    foreach (var usersFileVersion in fileVersion.usersFileVersions)
                    {
                        UserReadFileDTO userReadFileDTO = UserReadFileDTO.UserReadFileDTOBuilder(usersFileVersion);
                        fileVersionDTO.UsersReadFile.Add(userReadFileDTO);
                    }
                }
            }

            return fileVersionDTO;
        }

        #endregion

    }


}
