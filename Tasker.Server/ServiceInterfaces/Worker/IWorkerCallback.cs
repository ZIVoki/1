﻿using System.ServiceModel;
using Tasker.Server.DTO;

namespace Tasker.Server.ServiceInterfaces.Worker
{
    [ServiceContract]
    public interface IWorkerCallback: IClientCallback
    {
        [OperationContract(IsOneWay = true)]
        void TaskSet(TaskDTO task);

        [OperationContract(IsOneWay = true)]
        void TaskUnsetSet(int taskId);

        /// <summary>
        /// Special for new workers client to add new task immediatly
        /// </summary>
        /// <param name="task"></param>
        [OperationContract(IsOneWay = true)]
        void NewTask(TaskDTO task);

        [OperationContract(IsOneWay = true)]
        void SetStatusLunchTime(bool isLunch);

        [OperationContract(IsOneWay = true)]
        void SetTaskReviewResult(bool isSuccess);
    }
}