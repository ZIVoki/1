﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using Tasker.Server.Attributes;
using Tasker.Server.DTO;

namespace Tasker.Server.ServiceInterfaces.Worker
{
    [ServiceContract(SessionMode = SessionMode.Required, CallbackContract = typeof(IWorkerCallback))]  
    public interface IWorkerInterface: IClientInterface
    {
        /// <summary>
        /// Send lunch request to manager. Deliver request with LunchRequest() callback 
        /// </summary>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = true, IsTerminating = false)]
        void GoForLunchRequest(int? managerId = null);

        /// <summary>
        /// Message for server that this worker comeback to work. Change it status to "on-line at work". Alert all about it with WorkerComeback() callback
        /// </summary>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = true, IsTerminating = false)]
        void Comeback();

        /// <summary>
        /// Message for all managers. Deliver request with TaskAccepted() callback 
        /// </summary>
        /// <param name="taskId"></param>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = true, IsTerminating = false)]
        void AcceptTask(int taskId);

        /// <summary>
        /// Send task for review message to manager. Deliver request with TaskForReviewRecieved() callback 
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="managerId"></param>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = true, IsTerminating = false)]
        void SendForReview(TaskDTO taskDTO, int managerId);

        /// <summary>
        /// After authorization. 
        /// Method for get offline messages. 
        /// In the presence of the current task. Will be triggered by a call TaskSet() callback.
        /// </summary>
        /// <returns></returns>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        TaskDTO GetMyCurrentTask();

        /// <summary>
        /// Returns all worker tasks sleeping and in process
        /// </summary>
        /// <returns></returns>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        IEnumerable<TaskDTO> GetMyTasks();

        /// <summary>
        /// Returns total planned hours by dates 
        /// </summary>
        /// <returns></returns>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        TimeSpan GetTaskTotalHoursByDates(DateTime startDate, DateTime endDate);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        IEnumerable<TaskDTO> GetTasksByDate(DateTime date);

        /// <summary>
        /// Gets manager info by userId
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        UserDTO GetManager(int userId);

        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        IEnumerable<UserTaskTypeDTO> GetMyUserTaskTypes(int userId);
    }
}
