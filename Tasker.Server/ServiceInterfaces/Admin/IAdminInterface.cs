﻿using System.ServiceModel;
using Tasker.Server.ServiceInterfaces.Manager;

namespace Tasker.Server.ServiceInterfaces.Admin
{
    // use manager calback in this version
    [ServiceContract(SessionMode = SessionMode.Required, CallbackContract = typeof(IAdminCallback))]
    public interface IAdminInterface : IManagerInterface
    {
        
    }
}
