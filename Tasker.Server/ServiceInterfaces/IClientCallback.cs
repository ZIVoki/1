﻿using System.ServiceModel;
using Tasker.Server.DTO;
using Tasker.Server.SecurityAndValidation.SecurityValidationObjects;

namespace Tasker.Server.ServiceInterfaces
{
    [ServiceContract]
    public interface IClientCallback
    {
        /// <summary>
        /// Calls on new message recive
        /// </summary>
        /// <param name="msg"></param>
        [OperationContract(IsOneWay = true)]
        void ReceiveMessage(MessageDTO msg);

        /// <summary>
        /// Calls when user enter system
        /// </summary>
        /// <param name="userId"></param>
        [OperationContract(IsOneWay = true)]
        void UserEnter(int userId);

        /// <summary>
        /// Calls when user log off from system
        /// </summary>
        /// <param name="userId"></param>
        [OperationContract(IsOneWay = true)]
        void UserLogoff(int userId);

        /// <summary>
        /// Calls when some task was updated
        /// </summary>
        /// <param name="task">updated TaskDTO</param>
        [OperationContract(IsOneWay = true)]
        void TaskUpdated(TaskDTO task);

        /// <summary>
        /// Calls on new file added to task
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="attachFile"></param>
        [OperationContract(IsOneWay = true)]
        void FileAdded(int taskId, AttachFileDTO attachFile);

        /// <summary>
        /// Calls on new task type added
        /// </summary>
        [OperationContract(IsOneWay = true)]
        void TaskTypeAdded(TaskTypeDTO taskType);

        /// <summary>
        /// Calls on task type changed
        /// </summary>
        [OperationContract(IsOneWay = true)]
        void TaskTypeUpdated(TaskTypeDTO taskType);

        /// <summary>
        /// Calls on new task type added
        /// </summary>
        [OperationContract(IsOneWay = true)]
        void UserTaskTypeAdded(UserTaskTypeDTO userTaskType);

        /// <summary>
        /// Calls on task type changed
        /// </summary>
        [OperationContract(IsOneWay = true)]
        void UserTaskTypeUpdated(UserTaskTypeDTO userTaskType);

        /// <summary>
        /// Service error handler
        /// Log here. Show here. 
        /// </summary>
        /// <param name="createFaultDetail"></param>
        [OperationContract(IsOneWay = true)]
        void FaultExeptionServerAlarm(FaultDetail createFaultDetail);

        [OperationContract(IsOneWay = true)]
        void UserStateChanged(int userId, Constants.NetworkStates stateId);

        [OperationContract(IsOneWay = true)]
        void TimeTableUpdated(TimeTableDTO timeTable);

        [OperationContract(IsOneWay = true)]
        void ShiftUpdated(ShiftDTO shift);

        [OperationContract(IsOneWay = true)]
        void CustomerUpdated(CustomerDTO customer);

        [OperationContract(IsOneWay = true)]
        void TimeTableAdded(TimeTableDTO timeTable);

        [OperationContract(IsOneWay = true)]
        void UserUpdated(UserDTO user);

        [OperationContract(IsOneWay = true)]
        void BoardFileNewVersionAdded(FileVersionDTO newFileVersionDTO);
    }
}