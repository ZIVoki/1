﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using Tasker.Server.Attributes;
using Tasker.Server.DTO;

namespace Tasker.Server.ServiceInterfaces
{
    [ServiceContract(SessionMode = SessionMode.Required, CallbackContract = typeof(IClientCallback))]
    public interface IClientInterface
    {
        /// <summary>
        /// Try to connect to server, and get delay
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        [OperationContract(IsOneWay = false, IsTerminating = false)]
        TimeSpan? Ping(DateTime time);

        /// <summary>
        /// Login user
        /// Alert all about it with UserEnter() callback
        /// </summary>
        /// <param name="login"></param>
        /// <param name="password"></param>
        /// <returns>login result</returns>
        [OperationContract(IsInitiating = true, IsOneWay = false, IsTerminating = false)]
        LoginResultDTO Login(string login, string password);

        /// <summary>
        /// Logoff current user (close current connection, until new success login)
        /// If this last user connection alert all about it with UserLogoff() callback
        /// </summary>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = true, IsTerminating = true)]
        void Logoff();

        /// <summary>
        /// Get task type by id
        /// </summary>
        /// <param name="taskTypeId"></param>
        /// <returns></returns>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        TaskTypeDTO GetTaskType(int taskTypeId);

        /// <summary>
        /// Gets all task types
        /// </summary>
        /// <returns></returns>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        IEnumerable<TaskTypeDTO> GetTaskTypes();

        /// <summary>
        /// Gets users taskTypes
        /// </summary>
        /// <returns></returns>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        IEnumerable<UserTaskTypeDTO> GetUserTaskTypes(int taskType);

        /// <summary>
        /// Get users task type by id
        /// </summary>
        /// <param name="userTaskTypeId"></param>
        /// <returns></returns>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        UserTaskTypeDTO GetUserTaskType(int userTaskTypeId);

        /// <summary>
        /// Get all user unread messages
        /// </summary>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        IEnumerable<MessageDTO> GetUnreadMessages();

        /// <summary>
        /// SendMessage new message
        /// Deliver with ReceiveMessage() callback
        /// </summary>
        /// <param name="newMessage">some new MessageDTO</param>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        int SendMessage(MessageDTO newMessage);

        /// <summary>
        /// Task message history
        /// Get all task message by taskId
        /// </summary>
        /// <param name="taskId"></param>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        IEnumerable<MessageDTO> GetMessages(int taskId);

        /// <summary>
        /// Adds new file to task. Alert all with FileDataAdded() callback
        /// </summary>
        /// <param name="attachFile">new file FileDTO object</param>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        int AddFileToTask(AttachFileDTO attachFile);

        /// <summary>
        /// Gets file info by id
        /// </summary>
        /// <param name="fileId"></param>
        /// <returns>FileDTO</returns>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        AttachFileDTO GetFileData(int fileId);

        /// <summary>
        /// Get all files info by task id
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        IEnumerable<AttachFileDTO> GetFilesData(int taskId);

        /// <summary>
        /// Gets all shifts
        /// </summary>
        /// <returns></returns>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        IEnumerable<ShiftDTO> GetShifts();

        /// <summary>
        /// Get shift by id
        /// </summary>
        /// <returns></returns>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        ShiftDTO GetShift(int shiftId);

        /// <summary>
        /// Get timeTable by id
        /// </summary>
        /// <returns></returns>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        IEnumerable<TimeTableDTO> GetMyTimeTables(DateTime startDate, DateTime endDate);

        /// <summary>
        /// Returns user personal lates in set range of dates
        /// </summary>
        /// <returns></returns>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        IEnumerable<AttendanceLogItemDTO> GetMyLatesByDates(DateTime startDate, DateTime endDate);

        /// <summary>
        /// Return all board file with structure
        /// </summary>
        /// <returns></returns>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        IEnumerable<FileVersionDTO> GetBoardFiles();

        /// <summary>
        /// Return board file by id
        /// </summary>
        /// <returns></returns>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        FileVersionDTO GetBoardFileById(int fileId);

        /// <summary>
        /// Seting file as read by id 
        /// </summary>
        /// <returns></returns>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        void SetBoardFileAsRead(int fileId);

        /// <summary>
        /// Runing updater for file by id 
        /// </summary>
        /// <returns></returns>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        FileVersionDTO UpdateBoardFile(int fileId);

        /// <summary>
        /// Runing updater for all files
        /// </summary>
        /// <returns></returns>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = true, IsTerminating = false)]
        void UpdateAllBoardFiles();
    }
}
