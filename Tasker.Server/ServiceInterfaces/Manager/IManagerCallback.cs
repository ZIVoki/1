﻿using System;
using System.ServiceModel;
using Tasker.Server.DTO;

namespace Tasker.Server.ServiceInterfaces.Manager
{
    public interface IManagerCallback : IClientCallback
    {
        [OperationContract(IsOneWay = true)]
        void TaskAdded(TaskDTO task);

        [OperationContract(IsOneWay = true)]
        void CustomerAdded(CustomerDTO customer);

        [OperationContract(IsOneWay = true)]
        void TaskCheckedIn(TaskDTO taskDTO, string name);

        [OperationContract(IsOneWay = true)]
        void TaskCheckedOut(TaskDTO taskDTO, string name);

        [OperationContract(IsOneWay = true)]
        void TaskForReviewRecieved(TaskDTO taskDTO, DateTime sendTime);

        // отменяет ревью пользователя на данный таск
        [OperationContract(IsOneWay = true)]
        void TaskForReviewCancel(TaskDTO taskDto);

        [OperationContract(IsOneWay = true)]
        void TaskAccepted(int taskId, int userrId, DateTime acceptTime);

        [OperationContract(IsOneWay = true)]
        void WorkerComeback(int userId);

        [OperationContract(IsOneWay = true)]
        void LunchRequest(UserDTO fromUser);

        [OperationContract(IsOneWay = true)]
        void LunchRequestCancel(int userId);
        
        [OperationContract(IsOneWay = true)]
        void ShiftAdded(ShiftDTO shiftId);

        [OperationContract(IsOneWay = true)]
        void UserAdded(UserDTO user);

        [OperationContract(IsOneWay = true)]
        void WorkerFree(UserDTO worker);

        [OperationContract(IsOneWay = true)]
        void WorkerBusy(UserDTO worker, TaskDTO task);

        [OperationContract(IsOneWay = true)]
        void WorkerBlocked(UserDTO worker);

        [OperationContract(IsOneWay = true)]
        void AttendanceLogItemAdded(AttendanceLogItemDTO attendanceLogItemDTO);

        [OperationContract(IsOneWay = true)]
        void CustomerTaskTypeAdded(CustomerTaskTypeDTO customerTaskTypeDto);

        [OperationContract(IsOneWay = true)]
        void CustomerTaskTypeUpdated(CustomerTaskTypeDTO customerTaskTypeDto);

        [OperationContract(IsOneWay = true)]
        void UserReadFile(UserReadFileDTO userReadFileDTO);
    }
}