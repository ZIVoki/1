﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using Tasker.Server.Attributes;
using Tasker.Server.DTO;
using Tasker.Server.Extensions;

namespace Tasker.Server.ServiceInterfaces.Manager
{
    [ServiceContract(SessionMode = SessionMode.Required, CallbackContract = typeof(IManagerCallback))]
    public interface IManagerInterface : IClientInterface
    {
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = true, IsTerminating = false)]
        void GetSleepRequests();

        #region Tasks

        /// <summary>
        /// Add new task
        /// Alert managers with TaskUpdated() callback
        /// </summary>
        /// <param name="taskDTO"></param>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        int AddTask(TaskDTO taskDTO);

        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        List<TaskStatisticRecordDTO> GetTaskStatistic(int taskId);

        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        List<TaskStatisticRecordDTO> GetTaskStatisticByUserId(int userId, DateTime startDate, DateTime endDate);

        /// <summary>
        /// Get all tasks
        /// </summary>
        /// <returns></returns>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        IEnumerable<TaskDTO> GetTasks();

        /// <summary>
        /// Get last 200 tasks
        /// </summary>
        /// <returns></returns>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        IEnumerable<TaskDTO> GetTasks200Top();

        /// <summary>
        /// Get closed tasks in which worker involved by date range
        /// </summary>
        /// <param name="workerId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        IEnumerable<TaskDTO> GetClosedTasksByWorkerAndDate(int workerId, DateTime startDate, DateTime endDate);

        /// <summary>
        /// Get closed tasks
        /// </summary>
        /// <returns></returns>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        IEnumerable<TaskDTO> GetClosedTasks();

        /// <summary>
        /// get all not closed tasks
        /// </summary>
        /// <returns></returns>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        IEnumerable<TaskDTO> GetActiveTasks();

        /// <summary>
        /// get all users tasks
        /// </summary>
        /// <returns></returns>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        IEnumerable<TaskDTO> GetUserTasks(int userId);

        /// <summary>
        /// get all tasks from interval
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        IEnumerable<TaskDTO> GetTasksByDates(DateTime startDate, DateTime endDate);

        /// <summary>
        /// Get task by id
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        TaskDTO GetTask(int taskId);

        /// <summary>
        /// Update some task
        /// Alert all with TaskUpdated() callback
        /// </summary>
        /// <param name="taskDto">updated TaskDTO</param>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = true, IsTerminating = false)]
        void UpdateTask(TaskDTO taskDto);


        /// <summary>
        /// Close task
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="isClosed"></param>
        /// <param name="comment"></param>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = true, IsTerminating = false)]
        void SetTaskReviewResult(int taskId, bool isClosed, string comment);

        #endregion

        /// <summary>
        /// Message for server that this worker change state( "lunch" or "departed"). Change it status. Alert all about it with WorkerComeback() callback
        /// </summary>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = true, IsTerminating = false)]
        void ChangeState(Constants.NetworkStates state, int workerId = 0);

        /// <summary>
        /// Block resource for all. Checkout for edit
        /// Concurrent access
        /// </summary>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = true, IsTerminating = false)]
        void CheckOutTask(TaskDTO Task);

        /// <summary>
        /// Commit changes of blocked(checkouted) task
        /// Concurrent access
        /// </summary>
        /// <param name="taskId"></param>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = true, IsTerminating = false)]
        void CheckInTask(TaskDTO taskId);

        #region TaskTypes

        /// <summary>
        /// Add taskType.
        /// Alert all with TaskTypeAdded() callback
        /// </summary>
        /// <param name="taskType"></param>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        int AddTaskType(TaskTypeDTO taskType);

        /// <summary>
        /// Update taskType
        /// Alert all with TaskTypeUpdated() callback
        /// </summary>
        /// <param name="taskType"></param>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = true, IsTerminating = false)]
        void UpdateTaskType(TaskTypeDTO taskType);

        /// <summary>
        /// Add userTaskType.
        /// Alert all with UserTaskTypeAdded() callback
        /// </summary>
        /// <param name="taskType"></param>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        int AddUserTaskType(UserTaskTypeDTO taskType);

        /// <summary>
        /// Update UserTaskType
        /// Alert all with UserTaskTypeUpdated() callback
        /// </summary>
        /// <param name="taskType"></param>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = true, IsTerminating = false)]
        void UpdateUserTaskType(UserTaskTypeDTO taskType);

        #endregion
        
        #region Customers

        /// <summary>
        /// Add customer
        /// Alert all with customerAdded() callback
        /// </summary>
        /// <param name="customer"></param>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        int AddCustomer(CustomerDTO customer);

        /// <summary>
        /// Update customer
        /// Alert all with CustomerUpdated() callback
        /// </summary>
        /// <param name="customer"></param>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = true, IsTerminating = false)]
        void UpdateCustomer(CustomerDTO customer);

        /// <summary>
        /// Get customer
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        CustomerDTO GetCustomer(int customerId);

        /// <summary>
        /// Get all customers
        /// </summary>
        /// <returns></returns>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        IEnumerable<CustomerDTO> GetCustomers();

        #endregion

        #region Channels

        /// <summary>
        /// Get channel by id
        /// </summary>
        /// <param name="channelId"></param>
        /// <returns></returns>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        ChannelDTO GetChannel(int channelId);

        /// <summary>
        /// Get all channels
        /// </summary>
        /// <returns></returns>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        IEnumerable<ChannelDTO> GetChannels();

        #endregion

        
        #region CustomersTaskTypes

        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        IEnumerable<CustomerTaskTypeDTO> GetCustomersTaskTypes();

        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        IEnumerable<CustomerTaskTypeDTO> GetCustomersTaskTypesByCustomerId(int customerId);

        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        int AddCustomersTaskTypes(CustomerTaskTypeDTO customerTaskTypeDto);

        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        void UpdateCustomersTaskTypes(CustomerTaskTypeDTO customerTaskTypeDto);

        #endregion
        
        #region TimeTables

        /// <summary>
        /// Get timeTable by id
        /// </summary>
        /// <param name="timeTableId"></param>
        /// <returns></returns>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        TimeTableDTO GetTimeTable(int timeTableId);

        /// <summary>
        /// Gets all timetables
        /// </summary>
        /// <returns></returns>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        IEnumerable<TimeTableDTO> GetTimeTables();

        /// <summary>
        /// Gets timetables by dates
        /// </summary>
        /// <returns></returns>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        IEnumerable<TimeTableDTO> GetTimeTablesByDates(DateTime startDate, DateTime endDate);

        /// <summary>
        /// Gets user's timeTables
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        IEnumerable<TimeTableDTO> GetUserTimeTables(int userId, DateTime startDate, DateTime endDate);

        /// <summary>
        /// Gets timeTables of shift
        /// </summary>
        /// <param name="shiftId"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        IEnumerable<TimeTableDTO> GetShiftTimeTables(int shiftId, DateTime date);

        /// <summary>
        /// /// Add timeTable
        /// Alert all with timeTableAdded() callback
        /// </summary>
        /// <param name="timeTable"></param>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        int AddTimeTable(TimeTableDTO timeTable);

        /// <summary>
        /// Update timeTable
        /// Alert all with TimeTableUpdated() callback
        /// </summary>
        /// <param name="timeTable"></param>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = true, IsTerminating = false)]
        void UpdateTimeTable(TimeTableDTO timeTable);

        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        IEnumerable<TimeTableDTO> GetTimeTablesByDate(DateTime date);

        #endregion

        #region Shifts

        /// <summary>
        /// Gets users of shift in date
        /// </summary>
        /// <param name="shiftId"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        IEnumerable<UserDTO> GetUsersByShift(int shiftId, DateTime date);

        /// <summary>
        /// Add shift
        /// Alert all with shiftAdded() callback
        /// </summary>
        /// <param name="shift"></param>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        int AddShift(ShiftDTO shift);

        /// <summary>
        /// Update shift
        /// Alert all with shiftUpdated() callback
        /// </summary>
        /// <param name="shift"></param>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = true, IsTerminating = false)]
        void UpdateShift(ShiftDTO shift);

        #endregion

        #region Users

        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        IEnumerable<UserDTO> GetWorkers();

        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        UserDTO GetWorker(int workereId);

        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        IEnumerable<UserTaskTypeDTO> GetUserTaskTypesByUser(int userId);

        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        int AddUser(UserDTO userDTO);

        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        void UpdateUser(UserDTO userDTO);

        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        void SetNewUserCredentials(UserDTO userDTO, string login, string password);

        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = true, IsTerminating = false)]
        void CheckInWorker(UserDTO userDTO);

        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = true, IsTerminating = false)]
        void CheckOutWorker(UserDTO userDTO);

        #endregion
        
        /// <summary>
        /// Add new task
        /// Alert managers with TaskUpdated() callback
        /// </summary>
        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = true, IsTerminating = false)]
        void LunchRequestResult(bool changeState, int workerId);

        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        AttendanceLogItemDTO GetAttendanceLogItem(int itemId);

        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        IEnumerable<AttendanceLogItemDTO> GetAttendanceLogByDate(DateTime day);

        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        [CacheBehavior(600)] 
        IEnumerable<LoadItemDTO> GetLoadStatistic(DateTime date, int minutesInterval);

        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        IEnumerable<LoadItemDTO> GetLoadStatisticByCustomer(DateTime startDate, DateTime endDate, int minutesInterval, int customerId);

        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        IEnumerable<AttendanceLogItemDTO> GetLatesByDates(DateTime startDate, DateTime endDate);

        [SecurityOperationBehavior]
        [OperationContract(IsInitiating = false, IsOneWay = false, IsTerminating = false)]
        IEnumerable<UserStatItemDTO> GetUserStatistic(int userId, DateTime startTime, DateTime endTime);
    }
}
