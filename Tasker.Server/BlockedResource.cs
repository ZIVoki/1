﻿using System.Collections.Generic;
using System.Timers;
using Tasker.Server.DTO;
using Tasker.Server.Managers;
using Tasker.Server.Services;

namespace Tasker.Server
{
    class BlockedResource
    {
        private readonly Timer _timer;
        private readonly ConnectUser _userBlocker;
        private static readonly object _flag = new object();
        private static SynchronizedCollection<BlockedResource> _lockedResources = new SynchronizedCollection<BlockedResource>(_flag);

        public static SynchronizedCollection<BlockedResource> LockedResources
        {
            get { return _lockedResources; }
            set { _lockedResources = value; }
        }

        public BlockedResource(BaseDTO dto, ConnectUser userBlocker)
        {
            _userBlocker = userBlocker;
            if (_timer != null)
            {
                _timer.Stop();
            }

            _timer = new Timer(Constants.BlockTime);

            _timer.Elapsed += _timer_Elapsed;
            _timer.Enabled = true;
            _timer.Start();

            Resource = dto;

            if(dto is UserDTO)
            {
                BlockedResourceType = Constants.BlockedResourceType.Worker;
                UsersManager.ManagersAlerter.WorkerBlocked((UserDTO)dto, userBlocker);
            }
            else if (dto is TaskDTO)
            {
                BlockedResourceType = Constants.BlockedResourceType.Task;
                UsersManager.ManagersAlerter.TaskChecked(true, (TaskDTO)dto, userBlocker);
            }

            lock (_lockedResources.SyncRoot)
            {
                _lockedResources.Add(this);
            }
        }

        public BaseDTO Resource { get; private set; }

        public Constants.BlockedResourceType BlockedResourceType { get; set; }
        
        private void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            UnlockResource();
        }

        public static BlockedResource GetResource(Constants.BlockedResourceType type, int id)
        {
            lock (_lockedResources.SyncRoot)
            {
                foreach (var lockedResource in _lockedResources)
                {
                    if (lockedResource.Resource.Id == id && lockedResource.BlockedResourceType == type)
                        return lockedResource;
                }
            }
            return null;
        }

        public void UnlockResource()
        {
            var dto = this.Resource;
            var resource = GetResource(BlockedResourceType, this.Resource.Id);
            if (resource != null) RemoveFromBlockedCollection();

            switch (BlockedResourceType)
            {
                case Constants.BlockedResourceType.Task:
                    UsersManager.ManagersAlerter.TaskChecked(false, (TaskDTO)dto, _userBlocker);
                    break;
                case Constants.BlockedResourceType.Worker:
                    /*//todo invoke alert
                    UsersManager.ManagersAlerter.WorkTaskChecked(false, (TaskDTO)dto, UserBlocker);*/
                    break;
            }
        }

        private void RemoveFromBlockedCollection()
        {
            if (_timer != null) _timer.Dispose();

            BlockedResource blockedResourceToDelete = GetResource(this.BlockedResourceType, this.Resource.Id);

            lock (_lockedResources.SyncRoot)
            {
                _lockedResources.Remove(blockedResourceToDelete);
            }
        }

        public static void TargetAlertUserAboutBlockedReousrces(ConnectUser currentUser)
        {
            lock (_lockedResources.SyncRoot)
            {
                foreach (var blockedResource in _lockedResources)
                {
                    switch (blockedResource.BlockedResourceType)
                    {
                        case Constants.BlockedResourceType.Task:
                            UsersManager.ManagersAlerter.TaskCheckedTarget(true, (TaskDTO)blockedResource.Resource, currentUser);
                            break;
                        case Constants.BlockedResourceType.Worker:
                            UsersManager.ManagersAlerter.WorkerBlockedTarget((UserDTO)blockedResource.Resource, currentUser);
                            break;
                    }
                }
            }
        }
    }
}
