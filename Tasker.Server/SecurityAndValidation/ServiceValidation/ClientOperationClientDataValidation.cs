﻿using System;
using System.Security.Authentication;
using Tasker.Server.DTO;
using Tasker.Server.SecurityAndValidation.Validators;
using Tasker.Server.Services;
using Tasker.Server.SecurityAndValidation.SecurityValidationObjects;

namespace Tasker.Server.SecurityAndValidation.ServiceValidation
{
    class ClientOperationClientDataValidation
    {
        public static bool ValidateAuthorisation(ClientService sender)
        {
            if (sender.CurrentUser != null)
                return true;

            SharedMethodContainer.CheckValidCondition<AuthenticationException>(false, "Ошибка авторизации");
            return false;
        }

        public static bool ValidateMessage(MessageDTO msg)
        {
            /*var fromUserValidResult = ValidateIdExist.UserIdExistValidate(msg.FromUser);
            SharedMethodContainer.CheckValidCondition<ArgumentException>(fromUserValidResult.Key, "От кого: " + fromUserValidResult.Value);

            if(!fromUserValidResult.Key)
            {
                return false;
            }*/

            var toUserValidResult = ValidateIdExist.UserIdExistValidate(msg.ToUserId);
            SharedMethodContainer.CheckValidCondition<ArgumentException>(toUserValidResult.Key, "Кому: " + toUserValidResult.Value);

            if (!toUserValidResult.Key)
            {
                return false;
            }

            var taskValidResult = ValidateIdExist.TaskIdExistValidate(msg.Task.Id);
            SharedMethodContainer.CheckValidCondition<ArgumentException>(taskValidResult.Key, taskValidResult.Value);


            if (!taskValidResult.Key)
            {
                return false;
            }

            var sendDateValidResult = SimpleTypesValidator.DateTimeValidate(msg.SendDate);
            SharedMethodContainer.CheckValidCondition<ArgumentException>(sendDateValidResult.Key, sendDateValidResult.Value);


            if (!sendDateValidResult.Key)
            {
                return false;
            }

            var msgTextValidResult = SimpleTypesValidator.StringValidate(msg.Text);
            SharedMethodContainer.CheckValidCondition<ArgumentException>(msgTextValidResult.Key, msgTextValidResult.Value);

            if (!msgTextValidResult.Key)
            {
                return false;
            }

            return true;
        }

        public static bool ValidateFile(AttachFileDTO attachFile)
        {
                // TODO validate is file exist

                var taskValidResult = ValidateIdExist.TaskIdExistValidate(attachFile.Task.Id);
                SharedMethodContainer.CheckValidCondition<ArgumentException>(taskValidResult.Key, taskValidResult.Value);

            
                if (!taskValidResult.Key)
                {
                    return false;
                }

                var createdAtDateValidResult = SimpleTypesValidator.DateTimeValidate(attachFile.CreatedAt);
                SharedMethodContainer.CheckValidCondition<ArgumentException>(createdAtDateValidResult.Key, createdAtDateValidResult.Value);

                if (!createdAtDateValidResult.Key)
                {
                    return false;
                }

                var fileTitleValidResult = SimpleTypesValidator.StringValidate(attachFile.Title);
                SharedMethodContainer.CheckValidCondition<ArgumentException>(fileTitleValidResult.Key, fileTitleValidResult.Value);

                if (!fileTitleValidResult.Key)
                {
                    return false;
                }

                return true;
       }

        
    }
}

     