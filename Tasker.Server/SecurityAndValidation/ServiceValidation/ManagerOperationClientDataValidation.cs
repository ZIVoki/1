﻿using System;
using Tasker.Server.DTO;
using Tasker.Server.SecurityAndValidation.SecurityValidationObjects;
using Tasker.Server.SecurityAndValidation.Validators;

namespace Tasker.Server.SecurityAndValidation.ServiceValidation
{
    internal class ManagerOperationClientDataValidation
    {
        //validate TaskTypeDTO
        public static bool ValidateTaskType(TaskTypeDTO @taskType)
        {
            var compTimeValidResult = SimpleTypesValidator.NullValueValidate(@taskType.CompletionTime);
            SharedMethodContainer.CheckValidCondition<ArgumentException>(compTimeValidResult.Key,
                                                                         compTimeValidResult.Value);

            if (!compTimeValidResult.Key)
            {
                return false;
            }

            var taskTypeTitleValidResult = SimpleTypesValidator.StringValidate(@taskType.Title);
            SharedMethodContainer.CheckValidCondition<ArgumentException>(taskTypeTitleValidResult.Key,
                                                                         taskTypeTitleValidResult.Value);

            if (!taskTypeTitleValidResult.Key)
            {
                return false;
            }
            return true;
        }

        //UserTaskTypeDTO
        public static bool ValidateUserTaskType(UserTaskTypeDTO @usertaskType)
        {
            var userValidResult = ValidateIdExist.UserIdExistValidate(@usertaskType.UserId);
            SharedMethodContainer.CheckValidCondition<ArgumentException>(userValidResult.Key, userValidResult.Value);

            if (!userValidResult.Key)
            {
                return false;
            }

            var taskTypeValidResult = ValidateIdExist.TaskTypeIdExistValidate(@usertaskType.TaskTypeId);
            SharedMethodContainer.CheckValidCondition<ArgumentException>(taskTypeValidResult.Key,
                                                                         taskTypeValidResult.Value);

            if (!taskTypeValidResult.Key)
            {
                return false;
            }

            var compTimeValidResult = SimpleTypesValidator.NullValueValidate(@usertaskType.CompletionTime);
            SharedMethodContainer.CheckValidCondition<ArgumentException>(compTimeValidResult.Key,
                                                                         compTimeValidResult.Value);

            if (!compTimeValidResult.Key)
            {
                return false;
            }

            return true;
        }


        public static bool ValidateCustomer(CustomerDTO customer)
        {
            var customerTitleValidResult = SimpleTypesValidator.StringValidate(customer.Title);
            SharedMethodContainer.CheckValidCondition<ArgumentException>(customerTitleValidResult.Key,
                                                                         customerTitleValidResult.Value);

            if (!customerTitleValidResult.Key)
            {
                return false;
            }

            return true;
        }

        
        public static bool ValidateShift(ShiftDTO @shift)
        {
            /*var endTimeValidate = SimpleTypesValidator.DateTimeValidate(shift.EndTime);
            SharedMethodContainer.CheckValidCondition<ArgumentException>(endTimeValidate.Key, endTimeValidate.Value);

            if (!endTimeValidate.Key)
            {
                return false;
            }

            var startTimeValidate = SimpleTypesValidator.DateTimeValidate(shift.StartTime);
            SharedMethodContainer.CheckValidCondition<ArgumentException>(startTimeValidate.Key, startTimeValidate.Value);

            if (!startTimeValidate.Key)
            {
                return false;
            }

            if (shift.LunchDuration.HasValue)
            {
                var lunchDuration = SimpleTypesValidator.DateTimeValidate(shift.LunchDuration.Value);
                SharedMethodContainer.CheckValidCondition<ArgumentException>(lunchDuration.Key, lunchDuration.Value);

                if (!lunchDuration.Key)
                {
                    return false;
                }
            }*/

            return true;
        }

        public static bool ValidateTimeTable(TimeTableDTO @timeTable)
        {
            var shiftValidateResult = ValidateIdExist.ShiftIdExistValidate(@timeTable.ShiftId);
            SharedMethodContainer.CheckValidCondition<ArgumentException>(shiftValidateResult.Key,
                                                                         shiftValidateResult.Value);

            if (!shiftValidateResult.Key)
            {
                return false;
            }

            var userValidResult = ValidateIdExist.UserIdExistValidate(@timeTable.UserId);
            SharedMethodContainer.CheckValidCondition<ArgumentException>(userValidResult.Key, userValidResult.Value);

            if (!userValidResult.Key)
            {
                return false;
            }


            var sd = SimpleTypesValidator.DateTimeValidate(@timeTable.StartDate);
            SharedMethodContainer.CheckValidCondition<ArgumentException>(sd.Key, sd.Value);

            if (!sd.Key)
            {
                return false;
            }
            var ed = SimpleTypesValidator.DateTimeValidate(@timeTable.EndDate);
            SharedMethodContainer.CheckValidCondition<ArgumentException>(ed.Key, ed.Value);

            if (!ed.Key)
            {
                return false;
            }


           /* var flsn = SimpleTypesValidator.NullValueValidate(@timeTable.FirstLunchStart);
            SharedMethodContainer.CheckValidCondition<ArgumentException>(flsn.Key, flsn.Value);
            var flen = SimpleTypesValidator.NullValueValidate(@timeTable.FirstLunchEnd);
            SharedMethodContainer.CheckValidCondition<ArgumentException>(flen.Key, flen.Value);
            var slsn = SimpleTypesValidator.NullValueValidate(@timeTable.SecondLunchStart);
            SharedMethodContainer.CheckValidCondition<ArgumentException>(slsn.Key, slsn.Value);
            var slen = SimpleTypesValidator.NullValueValidate(@timeTable.SecondLunchEnd);
            SharedMethodContainer.CheckValidCondition<ArgumentException>(slen.Key, slen.Value);*/

            if (@timeTable.FirstLunchStart.HasValue)
            {
                var fls = SimpleTypesValidator.DateTimeValidate(@timeTable.FirstLunchStart.Value);
                SharedMethodContainer.CheckValidCondition<ArgumentException>(fls.Key, fls.Value);

                if (!fls.Key)
                {
                    return false;
                }
            }

            if (@timeTable.FirstLunchEnd.HasValue)
            {
                var fle = SimpleTypesValidator.DateTimeValidate(@timeTable.FirstLunchEnd.Value);
                SharedMethodContainer.CheckValidCondition<ArgumentException>(fle.Key, fle.Value);

                if (!fle.Key)
                {
                    return false;
                }
            }
            if (@timeTable.SecondLunchEnd.HasValue)
            {
                var sle = SimpleTypesValidator.DateTimeValidate(@timeTable.SecondLunchEnd.Value);
                SharedMethodContainer.CheckValidCondition<ArgumentException>(sle.Key, sle.Value);

                if (!sle.Key)
                {
                    return false;
                }
            }
            if (@timeTable.SecondLunchStart.HasValue)
            {
                var sls = SimpleTypesValidator.DateTimeValidate(@timeTable.SecondLunchStart.Value);
                SharedMethodContainer.CheckValidCondition<ArgumentException>(sls.Key, sls.Value);

                if (!sls.Key)
                {
                    return false;
                }
            }

            return true;
        }

        public static bool ValidateTask(TaskDTO task, int creatorId = 0)
        {
            if(creatorId != 0)
            {
                var userValidResult = ValidateIdExist.UserIdExistValidate(creatorId);
                SharedMethodContainer.CheckValidCondition<ArgumentException>(userValidResult.Key, userValidResult.Value);

                if (!userValidResult.Key)
                {
                    return false;
                }
            }

            /*var cUserValidResult = ValidateIdExist.UserIdExistValidate(task.CurrentWorkerId);
            SharedMethodContainer.CheckValidCondition<ArgumentException>(cUserValidResult.Key, cUserValidResult.Value);*/

            var customer = ValidateIdExist.UserIdExistValidate(task.CustomerId);
            SharedMethodContainer.CheckValidCondition<ArgumentException>(customer.Key, customer.Value);

            if (!customer.Key)
            {
                return false;
            }

            var textValidResult = SimpleTypesValidator.StringValidate(task.Title);
            SharedMethodContainer.CheckValidCondition<ArgumentException>(textValidResult.Key, textValidResult.Value);

            if (!textValidResult.Key)
            {
                return false;
            }
            
            /*var cd = SimpleTypesValidator.DateTimeValidate(task.CreationDate);
            SharedMethodContainer.CheckValidCondition<ArgumentException>(cd.Key, cd.Value);*/
            
            if (task.DeadLine.HasValue)
            {
                var dlt = SimpleTypesValidator.DateTimeValidate(task.DeadLine.Value);
                SharedMethodContainer.CheckValidCondition<ArgumentException>(dlt.Key, dlt.Value);

                if (!dlt.Key)
                {
                    return false;
                }
            }

            return true;
        }

        public static bool ValidateUser(UserDTO @user)
        {
            if (user.LastActiveDate.HasValue)
            {
                var lastActiveDateValidResult = SimpleTypesValidator.DateTimeValidate(user.LastActiveDate.Value);
                SharedMethodContainer.CheckValidCondition<ArgumentException>(lastActiveDateValidResult.Key, lastActiveDateValidResult.Value);

                if (!lastActiveDateValidResult.Key)
                {
                    return false;
                }
            }

            var firstNameValidRes = SimpleTypesValidator.StringValidate(user.FirstName);
            SharedMethodContainer.CheckValidCondition<ArgumentException>(firstNameValidRes.Key, firstNameValidRes.Value);

            if (!firstNameValidRes.Key)
            {
                return false;
            }

            var lastNameValidRes = SimpleTypesValidator.StringValidate(user.LastName);
            SharedMethodContainer.CheckValidCondition<ArgumentException>(lastNameValidRes.Key, lastNameValidRes.Value);

            if (!lastNameValidRes.Key)
            {
                return false;
            }

            return true;
        }
    }
}