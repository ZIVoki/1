﻿using System;

namespace Tasker.Server.SecurityAndValidation.SecurityValidationObjects
{
    public class FaultDetail
    {
        public String Message { get; set; }
        
        public static FaultDetail Create(string message)
        {
            return new FaultDetail
                       {
                           Message = message
                       };
        }


    }
}
