﻿using System;
using System.ComponentModel;
using System.ServiceModel;
using System.Threading;
using Tasker.Server.ServiceInterfaces;
using Tasker.Server.Services;

namespace Tasker.Server.SecurityAndValidation.SecurityValidationObjects
{
    class SharedMethodContainer
    {
        /// <summary>
        /// Check valid condition and if error - alert client
        /// </summary>
        /// <typeparam name="T">ExceptionType</typeparam>
        /// <param name="condition"></param>
        /// <param name="message">error message</param>
        public static void CheckValidCondition<T>(bool condition, string message)
        {
            if (!condition)
            {
                var details = Activator.CreateInstance<T>();
                var ex = new FaultException<T>(details, message);
                IClientCallback callback = OperationContext.Current.GetCallbackChannel<IClientCallback>();

                var bw = new BackgroundWorker();
                bw.DoWork += InvokeCallback;
                bw.RunWorkerAsync(new object[]{callback, ex});           
                
            }
        }

        static void InvokeCallback(object sender, DoWorkEventArgs doWorkEventArgs)
        {
            var callback = (IClientCallback)((object[]) doWorkEventArgs.Argument)[0];
            var ex = (Exception)((object[])doWorkEventArgs.Argument)[1];
            var retvalue = FaultDetail.Create(ex.Message);
            callback.FaultExeptionServerAlarm(retvalue);
        }
    }
}
