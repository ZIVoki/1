﻿using System;
using System.Collections.Generic;

namespace Tasker.Server.SecurityAndValidation.Validators
{
    class SimpleTypesValidator
    {
        static readonly DateTime minSqlDateTime = new DateTime(1753, 1, 1);
        static readonly DateTime maxSqlDateTime = new DateTime(9999, 1, 1);

        //private static char[] illegalCharacters = new char[]{};

        public static KeyValuePair<bool, string> DateTimeValidate(DateTime value)
        {
            if (value > maxSqlDateTime || value < minSqlDateTime)
            {
                return new KeyValuePair<bool, string>(false, "Неверный формат даты");
            }
            return new KeyValuePair<bool, string>(true, string.Empty);
        }

        public static KeyValuePair<bool, string> NullValueValidate(object value)
        {
            if (value == null)
            {
                return new KeyValuePair<bool, string>(false, "Значение не может быть нулевым");
            }
            return new KeyValuePair<bool, string>(true, string.Empty);
        }

        public static KeyValuePair<bool, string> StringValidate(string value, int min = 0, int max = 4000)
        {
            if(string.IsNullOrEmpty(value))
                return new KeyValuePair<bool, string>(false, "Строка пуста");

            if (value.Length == min || value.Length > max)
                return new KeyValuePair<bool, string>(false, "Неподходящая длина");

            return new KeyValuePair<bool, string>(true, string.Empty);
        }
    }
}
