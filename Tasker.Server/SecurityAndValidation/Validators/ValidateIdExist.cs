﻿using System.Collections.Generic;
using System.Linq;

namespace Tasker.Server.SecurityAndValidation.Validators
{
    class ValidateIdExist
    {
        public static KeyValuePair<bool, string> UserIdExistValidate(int userId)
        {
            using (var context = new TaskerEntities())
            {
                var user = context.users.Where(c => c.userId == userId).SingleOrDefault();
                if(user != null)
                {
                    return new KeyValuePair<bool, string>(true, string.Empty);
                }
                else
                {
                    return new KeyValuePair<bool, string>(false, "Пользователь не существует");
                }
            }
        }

        public static KeyValuePair<bool, string> TaskIdExistValidate(int taskId)
        {
            using (var context = new TaskerEntities())
            {
                var task = context.tasks.Where(c => c.taskId == taskId).SingleOrDefault();
                if (task != null)
                {
                    return new KeyValuePair<bool, string>(true, string.Empty);
                }
                else
                {
                    return new KeyValuePair<bool, string>(false, "Задача не существует");
                }
            }
        }

        public static KeyValuePair<bool, string> TaskTypeIdExistValidate(int taskTypeId)
        {
            using (var context = new TaskerEntities())
            {
                var user = context.taskTypes.Where(c => c.taskTypeId == taskTypeId).SingleOrDefault();
                if (user != null)
                {
                    return new KeyValuePair<bool, string>(true, string.Empty);
                }
                else
                {
                    return new KeyValuePair<bool, string>(false, "Тип задачи не существует");
                }
            }
        }

        public static KeyValuePair<bool, string> ShiftIdExistValidate(int shiftId)
        {
            using (var context = new TaskerEntities())
            {
                var user = context.shifts.Where(c => c.shiftId == shiftId).SingleOrDefault();
                if (user != null)
                {
                    return new KeyValuePair<bool, string>(true, string.Empty);
                }
                else
                {
                    return new KeyValuePair<bool, string>(false, "Смена не существует");
                }
            }
        }
    }
}
