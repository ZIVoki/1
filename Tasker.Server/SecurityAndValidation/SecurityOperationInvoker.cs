﻿using System;
using System.Data;
using System.ServiceModel;
using System.ServiceModel.Dispatcher;
using Tasker.Server.Managers.Logger;
using Tasker.Server.SecurityAndValidation.SecurityValidationObjects;
using Tasker.Server.SecurityAndValidation.ServiceValidation;
using Tasker.Server.ServiceInterfaces;
using Tasker.Server.Services;
using System.Linq;

namespace Tasker.Server.SecurityAndValidation
{
    public class SecurityOperationInvoker : IOperationInvoker
    {
        private IOperationInvoker InnerOperationInvoker { get; set; }
        
        public SecurityOperationInvoker(IOperationInvoker operationInvoker)
        {
            this.InnerOperationInvoker = operationInvoker;
        }

        public Object[] AllocateInputs()
        {
            return InnerOperationInvoker.AllocateInputs();
        }

        public Object Invoke(Object instance, Object[] inputs, out Object[] outputs)
        {
            outputs = new Object[] { };
            var sender = (ClientService)instance;

            var result = ClientOperationClientDataValidation.ValidateAuthorisation(sender);

            // check connection before every invoke
            using(var context = new TaskerEntities())
            {
                try
                {
                    var test = context.customers.Where(a => a.customerId == 1).FirstOrDefault();
                }
                catch (EntityException e)
                {
                    Log.Write(LogLevel.Fatal, e.Message);
                    IClientCallback callback = OperationContext.Current.GetCallbackChannel<IClientCallback>();
                    callback.FaultExeptionServerAlarm(new FaultDetail
                                            {Message = "Произошел сбой соединения с БД."});
                    return null;
                }
            }
            
            return result ? InnerOperationInvoker.Invoke(instance, inputs, out outputs) : null;
        }

        public IAsyncResult InvokeBegin(Object instance, Object[] inputs, AsyncCallback callback, Object state)
        {
            return InnerOperationInvoker.InvokeBegin(instance, inputs, callback, state);
        }

        public Object InvokeEnd(Object instance, out Object[] outputs, IAsyncResult result)
        {
            return InnerOperationInvoker.InvokeEnd(instance, out outputs, result);
        }

        public Boolean IsSynchronous
        {
            get { return InnerOperationInvoker.IsSynchronous; }
        }
    }
}
