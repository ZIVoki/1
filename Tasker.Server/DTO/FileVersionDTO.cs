﻿using System;
using System.Collections.Generic;

namespace Tasker.Server.DTO
{
    public class FileVersionDTO: BaseDTO<fileVersion>
    {
        public FileDTO File { get; set; }
        public int Version { get; set; }
        public String VersionDiff { get; set; }
        public DateTime ChangesDate { get; set; }
        public DateTime? ReadDate { get; set; }
        // mostly for managers? not fill for workers
        public List<UserReadFileDTO> UsersReadFile { get; set; }

        public static FileVersionDTO FileVersionDTOBuilder(fileVersion fileVersion)
        {
            FileVersionDTO fileVersionDTO = new FileVersionDTO
                {
                    Id = fileVersion.fileVersionId,
                    File = FileDTO.FileDTOBuilder(fileVersion.file),
                    VersionDiff = fileVersion.versionsDiff,
                    ChangesDate = fileVersion.changesDate,
                    Status = (Constants.Statuses) fileVersion.statusId,
                    Version = fileVersion.version,
                    UsersReadFile = new List<UserReadFileDTO>(),
                };

            return fileVersionDTO;
        }

        public override BaseDTO<fileVersion> Build(fileVersion t)
        {
            return FileVersionDTOBuilder(t);
        }
    }
}