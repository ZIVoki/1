﻿using System;

namespace Tasker.Server.DTO
{
    public class TaskTypeDTO: BaseDTO<taskType>
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public TimeSpan CompletionTime { get; set; }

        public static TaskTypeDTO TaskTypeDTOBuilder(taskType taskType)
        {
            if (taskType == null) return null;
            return new TaskTypeDTO
                       {
                           Id = taskType.taskTypeId,
                           Description = taskType.description,
                           Status = (Constants.Statuses)taskType.statusId,
                           Title = taskType.title,
                           CompletionTime = TimeSpan.FromTicks(taskType.completionTime)
                       };
        }

        public override BaseDTO<taskType> Build(taskType t)
        {
            return TaskTypeDTOBuilder(t);
        }
    }
}
