﻿using System;

namespace Tasker.Server.DTO
{
    public class UserTaskTypeDTO : BaseDTO<userTaskType>
    {
        public TaskTypeDTO TaskType { get; set; }
        public TimeSpan CompletionTime { get; set; }
        public UserDTO User { get; set; }
        
        public int UserId
        {
            get
            {
                return User != null ? User.Id : 0;
            }
        }
        public int TaskTypeId
        {
            get
            {
                if (TaskType != null)
                    return TaskType.Id;
                return 0;
            }
        }

        public static UserTaskTypeDTO UserTaskTypeDTOBuilder(userTaskType newUserTaskType)
        {
            return new UserTaskTypeDTO
            {
                Id = newUserTaskType.userTaskTypeId,
                Status = (Constants.Statuses)newUserTaskType.statusId,
                CompletionTime = TimeSpan.FromTicks(newUserTaskType.completionTime),
                TaskType = TaskTypeDTO.TaskTypeDTOBuilder(newUserTaskType.taskType),
                User = UserDTO.UserDTOBuilder(newUserTaskType.user)
            };
        }

        public override BaseDTO<userTaskType> Build(userTaskType t)
        {
            return UserTaskTypeDTOBuilder(t);
        }
    }
}
