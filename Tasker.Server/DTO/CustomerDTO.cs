﻿namespace Tasker.Server.DTO
{
    public class CustomerDTO : BaseDTO<customer>
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public int MaxWorkers { get; set; }
        public ChannelDTO Channel { get; set; }

        public static CustomerDTO CustomerDTOBuilder(customer customer)
        {
            if (customer == null) return null;
            return new CustomerDTO
                       {
                           Id = customer.customerId,
                           Title = customer.title,
                           Description = customer.description,
                           MaxWorkers = customer.maxWorkers,
                           Channel = ChannelDTO.ChannelDTOBuilder(customer.channel),
                           Status = (Constants.Statuses)customer.statusId
                       };
        }

        public override BaseDTO<customer> Build(customer t)
        {
            return CustomerDTOBuilder(t);
        }
    }
}
