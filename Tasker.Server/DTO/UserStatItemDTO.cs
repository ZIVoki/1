namespace Tasker.Server.DTO
{
    public class UserStatItemDTO
    {
        public TaskTypeDTO TaskType { get; set; }
        public int AvgMinutes { get; set; }
        public int TasksCount { get; set; }

        public UserStatItemDTO(taskType taskType, int avgMints, int tasksCount)
        {
            TaskType = TaskTypeDTO.TaskTypeDTOBuilder(taskType);
            AvgMinutes = avgMints;
            TasksCount = tasksCount;
        }

        public UserStatItemDTO()
        {
            AvgMinutes = 0;
            TasksCount = 0;
        }
    }
}