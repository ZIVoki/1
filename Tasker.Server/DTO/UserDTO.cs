﻿using System;

namespace Tasker.Server.DTO
{
    public class UserDTO : BaseDTO<user>
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string DomainName { get; set; }
        public CustomerDTO Customer { get; set; }
        public int? WeeklyHours { get; set; }
        public DateTime? LastActiveDate { get; set; }
        public Constants.Roles RoleId { get; set; }
        public Constants.NetworkStates NetworkState { get; set; }

        // attention does not send password
        public static UserDTO UserDTOBuilder(user user)
        {
            if (user == null) return null;
            return new UserDTO
            {
                Id = user.userId,
                RoleId = (Constants.Roles)user.roleId,
                Status = (Constants.Statuses)user.statusId,
                WeeklyHours = user.weeklyHours,
                LastActiveDate = user.lastActiveDate,
                FirstName = user.firstName,
                LastName = user.lastName,
                Login = user.login,
                Email = user.email,
                Phone = user.phone,
                DomainName = user.dLogin,
                NetworkState = Constants.NetworkStates.Offline,
                Customer = CustomerDTO.CustomerDTOBuilder(user.customer),
            };
        }

        public override BaseDTO<user> Build(user t)
        {
            return UserDTOBuilder(t);
        }

        public override string ToString()
        {
            return String.Format("{0} {1}", LastName, FirstName);
        }

        public bool Equals(UserDTO other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(other.LastName, LastName) && Equals(other.FirstName, FirstName) && Equals(other.Login, Login) && Equals(other.Password, Password) && Equals(other.Email, Email) && Equals(other.Phone, Phone) && Equals(other.Customer, Customer) && other.WeeklyHours.Equals(WeeklyHours) && other.LastActiveDate.Equals(LastActiveDate) && Equals(other.RoleId, RoleId) && Equals(other.NetworkState, NetworkState);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (UserDTO)) return false;
            return Equals((UserDTO) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int result = (LastName != null ? LastName.GetHashCode() : 0);
                result = (result*397) ^ (FirstName != null ? FirstName.GetHashCode() : 0);
                result = (result*397) ^ (Login != null ? Login.GetHashCode() : 0);
                result = (result*397) ^ (Password != null ? Password.GetHashCode() : 0);
                result = (result*397) ^ (Email != null ? Email.GetHashCode() : 0);
                result = (result*397) ^ (Phone != null ? Phone.GetHashCode() : 0);
                result = (result*397) ^ (Customer != null ? Customer.GetHashCode() : 0);
                result = (result*397) ^ (WeeklyHours.HasValue ? WeeklyHours.Value : 0);
                result = (result*397) ^ (LastActiveDate.HasValue ? LastActiveDate.Value.GetHashCode() : 0);
                result = (result*397) ^ RoleId.GetHashCode();
                result = (result*397) ^ NetworkState.GetHashCode();
                return result;
            }
        }
    }
}
