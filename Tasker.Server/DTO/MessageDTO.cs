﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Tasker.Server.DTO
{
    public class MessageDTO: BaseDTO<message>
    {
        //auto complete property by server
        public UserDTO FromUser { get; set; }
        
        public int FromUserId { get { return FromUser.Id; }}

        public UserDTO ToUser { get; set; }
        public int ToUserId { get { return ToUser.Id; } }
        public string Text { get; set; }
        public DateTime SendDate { get; set; }
        public DateTime? ArriveDate { get; set; }
        public TaskDTO Task { get; set; }
        public int TaskId
        {
            get
            {
                return Task != null ? Task.Id : 0;
            }
        }

        public static MessageDTO MessageDTOBuilder(message newMessage)
        {
            user userDestination;
            using (var context = new TaskerEntities())
            {
                //var efTask = context.tasks.Where(a => a.taskId == taskDTO.Id).FirstOrDefault();
                userDestination = context.users.FirstOrDefault(a => a.userId == newMessage.toUserId);
            }
            return new MessageDTO
                       {
                           Id = newMessage.messageId,
                           Status = (Constants.Statuses)newMessage.statusId,
                           //message.user - fromUser
                           FromUser = UserDTO.UserDTOBuilder(newMessage.user),
                           ToUser = UserDTO.UserDTOBuilder(userDestination),
                           SendDate = newMessage.sendDate,
                           ArriveDate = newMessage.arriveDate,
                           Task = TaskDTO.TaskDTOBuilder(newMessage.task),
                           Text = newMessage.text
                       };
        }

        public override string ToString()
        {
            return string.Format("FromUser: {0}, FromUserId: {1}, ToUser: {2}, ToUserId: {3}, Text: {4}, SendDate: {5}, ArriveDate: {6}, Task: {7}, TaskId: {8}", FromUser, FromUserId, ToUser, ToUserId, Text, SendDate, ArriveDate, Task, TaskId);
        }

        public override BaseDTO<message> Build(message t)
        {
            return MessageDTOBuilder(t);
        }
    }
}
