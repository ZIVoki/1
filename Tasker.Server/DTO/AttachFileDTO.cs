﻿using System;

namespace Tasker.Server.DTO
{
    public class AttachFileDTO : BaseDTO<attachFile>
    {
        public TaskDTO Task { get; set; }

        public int TaskId
        {
            get
            {
                return Task != null ? Task.Id : 0;
            }
        }
        
        public string Title { get; set; }
        public string FilePath { get; set; }
        public DateTime CreatedAt { get; set; }
        public bool IsFinalyFile { get; set; }

        public static AttachFileDTO FileDTOBuilder(attachFile attachFile)
        {
            return new AttachFileDTO
                       {
                           Id = attachFile.attachFileId,
                           Task = TaskDTO.TaskDTOBuilder(attachFile.task),
                           CreatedAt = attachFile.createdAt,
                           IsFinalyFile = attachFile.isFinalFile,
                           FilePath = attachFile.filepath,
                           Status = (Constants.Statuses)attachFile.statusId,
                           Title = attachFile.title
                       };
        }

        public override BaseDTO<attachFile> Build(attachFile t)
        {
            return FileDTOBuilder(t);
        }
    }
}