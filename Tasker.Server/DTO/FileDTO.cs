﻿using System;

namespace Tasker.Server.DTO
{
    public class FileDTO: BaseDTO<file>
    {
        public String Title { get; set; }
        public String FilePath { get; set; }
        public String FileSource { get; set; }
        public FolderDTO Folder { get; set; }

        public static FileDTO FileDTOBuilder(file file)
        {
            return new FileDTO
                {
                    Id = file.fileId,
                    Folder = FolderDTO.FolderDTOBuilder(file.folder),
                    FileSource = file.fileSource,
                    FilePath = file.filePath,
                    Status = (Constants.Statuses)file.statusId,
                    Title = file.title
                };
        }

        public override BaseDTO<file> Build(file t)
        {
            return FileDTOBuilder(t);
        }
    }
}