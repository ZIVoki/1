using System;

namespace Tasker.Server.DTO
{
    public class LoadItemDTO
    {
        public DateTime TimeStamp { get; set; }
        public int InWorkWorkers { get; set; }
        public int TimeTableWorkers { get; set; }
        public int InNetWorkWorkers { get; set; }

        public LoadItemDTO(DateTime timeStamp, int inWorkWorkers, int timeTableWorkers, int inNetWorkWorkers)
        {
            TimeStamp = timeStamp;
            InWorkWorkers = inWorkWorkers;
            TimeTableWorkers = timeTableWorkers;
            InNetWorkWorkers = inNetWorkWorkers;
        }

        public LoadItemDTO()
        {
            TimeStamp = DateTime.MinValue;
        }
    }
}