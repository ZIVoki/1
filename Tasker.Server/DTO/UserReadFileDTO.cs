﻿using System;

namespace Tasker.Server.DTO
{
    public class UserReadFileDTO: BaseDTO<usersFileVersion>
    {
        public int UserId { get; set; }
        public int FileId { get; set; }
        public int Version { get; set; }
        public DateTime? ReadDate { get; set; }

        public static UserReadFileDTO UserReadFileDTOBuilder(usersFileVersion usersFileVersion)
        {
            if (usersFileVersion == null) return null;
            UserReadFileDTO userReadFileDTO = new UserReadFileDTO
                {
                    Id = usersFileVersion.usersFileVersionId,
                    UserId = usersFileVersion.userId,
                    FileId = usersFileVersion.fileVersion.fileId,
                    Version = usersFileVersion.fileVersion.version,
                };
            if (usersFileVersion.readDate.HasValue)
                userReadFileDTO.ReadDate = usersFileVersion.readDate.Value;
            return userReadFileDTO;
        }

        public override BaseDTO<usersFileVersion> Build(usersFileVersion t)
        {
            return UserReadFileDTOBuilder(t);
        }
    }
}