namespace Tasker.Server.DTO
{
    public class CustomerTaskTypeDTO: BaseDTO<customersTaskType>
    {
        public CustomerDTO Customer { get; set; }
        public TaskTypeDTO TaskType { get; set; }

        public static CustomerTaskTypeDTO CustomerTaskTypeDTOBuilder(customersTaskType customersTaskType)
        {
            if (customersTaskType == null) return null;
            return new CustomerTaskTypeDTO()
                       {
                           Id= customersTaskType.customersTaskTypesId,
                           Customer = CustomerDTO.CustomerDTOBuilder(customersTaskType.customer),
                           TaskType = TaskTypeDTO.TaskTypeDTOBuilder(customersTaskType.taskType),
                           Status = (Constants.Statuses) customersTaskType.statusId
                       };
        }

        public override BaseDTO<customersTaskType> Build(customersTaskType t)
        {
            return CustomerTaskTypeDTOBuilder(t);
        }
    }
}