﻿namespace Tasker.Server.DTO
{
    public class BaseDTO
    {
        public int Id { get; set; }
        public Constants.Statuses Status { get; set; }
        public int StatusId
        {
            get
            {
                return (int)Status;
            }
        }
    }

    public class BaseDTO<T>:BaseDTO where T : IObjectWithChangeTracker
    {
        public virtual BaseDTO<T> Build(T t)
        {
            return null;
        }
    }
}



