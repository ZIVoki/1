﻿using System;
using System.Collections.Generic;

namespace Tasker.Server.DTO
{
    public class ShiftDTO : BaseDTO<shift>
    {
        public string Title { get; set; }
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; }
        public TimeSpan? LunchDuration { get; set; }

        public static ShiftDTO ShiftDTOBuilder(shift shift)
        {
            if (shift == null) return null;
            ShiftDTO shiftDto = new ShiftDTO
                                    {
                                        StartTime = shift.startTime,
                                        EndTime = shift.endTime,
                                        Title = shift.title,
                                        Status = (Constants.Statuses)shift.statusId,
                                        Id = shift.shiftId,
                                    };

            if (shift.lunchDuration.HasValue)
                shiftDto.LunchDuration = TimeSpan.FromTicks(shift.lunchDuration.Value);

            return shiftDto;
        }

        public override BaseDTO<shift> Build(shift t)
        {
            return ShiftDTOBuilder(t);
        }
    }


}
