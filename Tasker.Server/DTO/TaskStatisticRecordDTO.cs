﻿using System;
using System.Linq;

namespace Tasker.Server.DTO
{
    public class TaskStatisticRecordDTO
    {
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public Constants.Statuses TaskState { get; set; }
        //worker at current moment
        public UserDTO Worker { get; set; }
        //manager or another user who change task state
        public UserDTO User { get; set; }
        public TimeSpan Duration { get; set; }
        public UserDTO EditBy { get; set; }

        public static TaskStatisticRecordDTO TaskStatisticRecordDTOBuilder(taskChanx t)
        {
            if (t == null) return null;

            user worker;
            user editBy = null;
            using (var context = new TaskerEntities())
            {
                //var efTask = context.tasks.Where(a => a.taskId == taskDTO.Id).FirstOrDefault();
                worker = context.users.FirstOrDefault(a => a.userId == t.userId);
                if (t.editBy.HasValue)
                    editBy = context.users.FirstOrDefault(u => u.userId == t.editBy.Value);
            }

            TaskStatisticRecordDTO result = new TaskStatisticRecordDTO
                                                                {
                                                                    TaskState = (Constants.Statuses)t.statusId,
                                                                    Duration = t.duration.HasValue ? new TimeSpan(t.duration.Value) : new TimeSpan(0),
                                                                    StartDate = t.startDate,
                                                                    EndDate = t.endDate,
                                                                    Worker = UserDTO.UserDTOBuilder(worker),
                                                                    EditBy = UserDTO.UserDTOBuilder(editBy)
                                                                    //User = UserDTO.UserDTOBuilder(t.userId),
                                                                };
            return result;
        }
    }
}
