﻿namespace Tasker.Server.DTO
{
    public class LoginResultDTO
    {
        public UserDTO User { get; set; }
        public bool Success { get; set; }
        public Constants.Roles Role { get; set; }
    }
}
