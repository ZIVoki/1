﻿using System;

namespace Tasker.Server.DTO
{
    public class TaskDTO : BaseDTO<task>
    {
        public TaskDTO()
        {
            InProcessDuration = TimeSpan.Zero;
        }

        public UserDTO Creator { get; set; }
        public UserDTO CurrentWorker { get; set; }
        public TaskTypeDTO TaskType { get; set; }
        public CustomerDTO Customer { get; set; }
        public int Priority { get; set; }

        public string Title { get; set; }
        public string Description { get; set; }
        public string Comment { get; set; }

        public DateTime CreationDate { get; set; }
        public DateTime? CompleteDate { get; set; }
        public DateTime? DeadLine { get; set; }

        public TimeSpan PlannedTime { get; set; }

        // time while task was in process or planing
        public TimeSpan InProcessDuration { get; set; }

        #region ids

        public int? CurrentWorkerId
        {
            get
            {
                return CurrentWorker != null ? CurrentWorker.Id : (int?) null;
            }
        }
        public int TaskTypeId
        {
            get
            {
                return TaskType != null ? TaskType.Id : 0;
            }
        }
        public int CustomerId
        {
            get
            {
                return Customer != null ? Customer.Id : 0;
            }
        }
        public int CreatorId
        {
            get
            {
                return Creator != null ? Creator.Id : 0;
            }
        }

        #endregion

        public static TaskDTO TaskDTOBuilder(task newTask)
        {
            return
                new TaskDTO
                    {
                        Id = newTask.taskId,
                        Title = newTask.title,
                        Customer = CustomerDTO.CustomerDTOBuilder(newTask.customer),
                        DeadLine = newTask.deadLine,
                        CreationDate = newTask.creationDate,
                        CompleteDate = newTask.completedAt,
                        Creator = UserDTO.UserDTOBuilder(newTask.user),
                        PlannedTime = TimeSpan.FromTicks(newTask.plannedTime),
                        Description = newTask.description,
                        Priority = newTask.priority,
                        Status = (Constants.Statuses) newTask.statusId,
                        TaskType = TaskTypeDTO.TaskTypeDTOBuilder(newTask.taskType),
                        CurrentWorker = UserDTO.UserDTOBuilder(newTask.user1),
                        Comment = newTask.comments
                    };
        }

        public override BaseDTO<task> Build(task t)
        {
            return TaskDTOBuilder(t);
        }
    }
}