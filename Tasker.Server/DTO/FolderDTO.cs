﻿using System;

namespace Tasker.Server.DTO
{
    public class FolderDTO: BaseDTO<folder>
    {
        public String Title { get; set; }
        public String Path { get; set; }
        public DateTime CreateDate { get; set; }
        public FolderDTO ParentFolder { get; set; }

        public static FolderDTO FolderDTOBuilder(folder folder)
        {
            FolderDTO folderDTO = new FolderDTO
                {
                    Id = folder.folderId,
                    CreateDate = folder.createDate,
                    Path = folder.path,
                    Status = (Constants.Statuses) folder.statusId,
                    Title = folder.title
                };
            if (folder.folder1 != null)
                folderDTO.ParentFolder = FolderDTOBuilder(folder.folder1);
            return folderDTO;
        }

        public override BaseDTO<folder> Build(folder t)
        {
            return FolderDTOBuilder(t);
        }
    }
}