﻿using System;

namespace Tasker.Server.DTO
{
    public class TimeTableDTO : BaseDTO<timeTable>
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime? FirstLunchStart { get; set; }
        public DateTime? FirstLunchEnd { get; set; }
        public DateTime? SecondLunchStart { get; set; }
        public DateTime? SecondLunchEnd { get; set; }
        public ShiftDTO Shift { get; set; }
        public UserDTO User { get; set; }
        public bool IsResp { get; set; }

        public int ShiftId
        {
            get
            {
                return Shift != null ? Shift.Id : 0;
            }
        }

        public int UserId
        {
            get
            {
                return User != null ? User.Id : 0;
            }
        }

        public static TimeTableDTO WorkersTimeTableDTOBuilder(timeTable timeTable)
        {
            if (timeTable == null) return null;

            return new TimeTableDTO
            {
                Id = timeTable.timeTableId,
                Shift = ShiftDTO.ShiftDTOBuilder(timeTable.shift),
                User = UserDTO.UserDTOBuilder(timeTable.user),
                IsResp = timeTable.isResp,
                StartDate = timeTable.startDate,
                EndDate = timeTable.endDate,
                FirstLunchEnd = timeTable.firstLunchEnd,
                FirstLunchStart = timeTable.firstLunchStart,
                SecondLunchEnd = timeTable.secondLunchEnd,
                SecondLunchStart = timeTable.secondLunchStart,
                Status = (Constants.Statuses)timeTable.statusId
            };
        }

        public override BaseDTO<timeTable> Build(timeTable t)
        {
            return WorkersTimeTableDTOBuilder(t);
        }
    }


}
