namespace Tasker.Server.DTO
{
    public class ChannelDTO: BaseDTO<channel>
    {
        public string Title { get; set; }

        public static ChannelDTO ChannelDTOBuilder(channel channel)
        {
            if (channel == null) return null;
            return new ChannelDTO
                       {
                           Id = channel.channelId,
                           Title = channel.title,
                           Status = (Constants.Statuses) channel.statusId
                       };
        }

        public override BaseDTO<channel> Build(channel t)
        {
            return ChannelDTOBuilder(t);
        }
    }
}