﻿using System;

namespace Tasker.Server.DTO
{
    public class AttendanceLogItemDTO : BaseDTO<attendanceLog>
    {
        //auto complete property by server
        public UserDTO User { get; set; }
        public DateTime EventDateTime { get; set; }
        public TimeTableDTO TimeTable { get; set; }

        public static AttendanceLogItemDTO AttendanceLogItemDTOBuilder(attendanceLog attendanceLogItem)
        {
            /*user user;
            using (var context = new TaskerEntities())
            {
                user = context.users.FirstOrDefault(a => a.userId == attendanceLogItem.userId);
            }
            timeTable timeTable;
            using (var context = new TaskerEntities())
            {
                timeTable = context.timeTables.FirstOrDefault(a => a.timeTableId == attendanceLogItem.timeTableId);
            }*/
            
            return new AttendanceLogItemDTO
                       {
                           Id = attendanceLogItem.itemId,
                           Status = (Constants.Statuses)attendanceLogItem.statusId,
                           //message.user - fromUser
                           User = UserDTO.UserDTOBuilder(attendanceLogItem.user),
                           TimeTable = TimeTableDTO.WorkersTimeTableDTOBuilder(attendanceLogItem.timeTable),
                           EventDateTime = attendanceLogItem.EventDateTime
                       };
        }

        public override BaseDTO<attendanceLog> Build(attendanceLog t)
        {
            return AttendanceLogItemDTOBuilder(t);
        }
    }
}
