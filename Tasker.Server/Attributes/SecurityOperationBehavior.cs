﻿using System;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using Tasker.Server.SecurityAndValidation;

namespace Tasker.Server.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public class SecurityOperationBehavior : Attribute, IOperationBehavior
    {
        public void AddBindingParameters(OperationDescription operationDescription, System.ServiceModel.Channels.BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyClientBehavior(OperationDescription operationDescription, ClientOperation clientOperation)
        {
        }

        public void ApplyDispatchBehavior(OperationDescription operationDescription, DispatchOperation dispatchOperation)
        {
            dispatchOperation.Invoker = new SecurityOperationInvoker(dispatchOperation.Invoker);
        }

        public void Validate(OperationDescription operationDescription)
        {
        }
    }
}
