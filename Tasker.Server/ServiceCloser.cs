﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using Tasker.Server.Managers;

namespace Tasker.Server
{
    public class ServiceCloser
    {
        public static void Close()
        {
            SetExitsToUsers();
            SetTasksPause();
        }

        private static void SetTasksPause()
        {
            using (TaskerEntities context = new TaskerEntities())
            {
                List<task> tasks = context.tasks.Where(t => t.statusId == (int)Constants.Statuses.InProcess).ToList();
                foreach (var taskToUpd in tasks)
                {
                    Console.WriteLine("Updating {0}", taskToUpd.title);
                    taskToUpd.statusId = (int)Constants.Statuses.Disabled;
                    context.tasks.ApplyChanges(taskToUpd);
                }
                context.SaveChanges();
            }
        }

        private static void SetExitsToUsers()
        {
            DateTime weekAgoDateTime = DateTime.Now.AddDays(-7);
            using (TaskerEntities context = new TaskerEntities())
            {
                List<attendanceLog> attendanceLogs =
                    context.attendanceLogs.Include(t => t.user)
                           .Where(
                               att =>
                               att.EventDateTime > weekAgoDateTime &&
                               att.user.statusId == (int)Constants.Statuses.Enabled)
                           .OrderBy(dt => dt.userId)
                           .ThenBy(at => at.EventDateTime)
                           .ToList();

                for (int index = 1; index <= attendanceLogs.Count; index++)
                {
                    if (index == attendanceLogs.Count)
                    {
                        if (attendanceLogs[index - 1].statusId == (int)Constants.Statuses.UserEnter)
                            AttendanceLogManager.SaveAttendanceItem(attendanceLogs[index - 1].userId, Constants.Statuses.UserLeave, context);
                        break;
                    }
                    if (attendanceLogs[index-1].userId != attendanceLogs[index].userId)
                    {
                        if (attendanceLogs[index-1].statusId == (int) Constants.Statuses.UserEnter)
                        {
                            Console.WriteLine("User leavinig {0}", attendanceLogs[index-1].user.lastName);
                            AttendanceLogManager.SaveAttendanceItem(attendanceLogs[index - 1].userId, Constants.Statuses.UserLeave, context);
                        }
                    }
                }

                attendanceLog lastOrDefault = attendanceLogs.LastOrDefault();
                if (lastOrDefault != null && lastOrDefault.statusId == (int)Constants.Statuses.UserEnter)
                    AttendanceLogManager.SaveAttendanceItem(lastOrDefault.userId, Constants.Statuses.UserLeave, context);
            }
        }
    }
}
