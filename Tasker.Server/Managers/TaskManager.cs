﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tasker.Server.DTO;
using Tasker.Server.Services;
using System.Data.Entity;

namespace Tasker.Server.Managers
{
    public class TaskManager
    {
        public static void ChangeTaskStatus(int taskId, Constants.Statuses status, int currentUserId)
        {
            //Change task status
            using (var context = new TaskerEntities())
            {
                ChangeTaskStatus(taskId, status, currentUserId, context);
            }
        }

        public static void ChangeTaskStatus(int taskId, Constants.Statuses status, int currentUserId,
                                            TaskerEntities context)
        {
            task efTask =
                context.tasks.Include(t => t.taskType).Include(t => t.user).Include(t => t.user1).Include(
                    t => t.customer).Include(t => t.customer.channel).FirstOrDefault(a => a.taskId == taskId);

            if (efTask == null) return;

            ChangeTaskStatus(efTask, status, currentUserId, context);
        }

        /// <summary>
        /// Get time while user work with task
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="currentUserId"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static long GetTaskInProcessTime(int taskId, int currentUserId, TaskerEntities context)
        {
            List<long?> longs =
                context.taskChanges.Where(
                    t =>
                    t.taskId == taskId && t.duration.HasValue && t.userId == currentUserId &&
                    (t.statusId == (int) Constants.Statuses.InProcess || t.statusId == (int) Constants.Statuses.Planing))
                       .Select(tch => tch.duration)
                       .ToList();
            long sum = longs.Sum(g => g.Value);

            return sum;
        }

        /// <summary>
        /// Set inprocess time to task dto
        /// </summary>
        /// <param name="taskDTO"></param>
        /// <param name="context"></param>
        public static void SetTaskInProcessTime(TaskDTO taskDTO, TaskerEntities context)
        {
            int userId = 0;
            if (taskDTO.CurrentWorkerId.HasValue) userId = taskDTO.CurrentWorkerId.Value;
            long sum = GetTaskInProcessTime(taskDTO.Id, userId, context);
            taskDTO.InProcessDuration = TimeSpan.FromTicks(sum);
        }


        private static void ChangeTaskStatus(task task, Constants.Statuses status, int currentUserId,
                                             TaskerEntities context)
        {
            task.statusId = (int) status;
            //Save it
            context.tasks.ApplyChanges(task);
            TaskDTO taskDTO = TaskDTO.TaskDTOBuilder(task);
            //Write task history
            ManagerService.WriteTaskHistory(taskDTO, context, currentUserId);
            context.SaveChanges();
            //Alert all about it
            UsersManager.AllAlerter.TaskUpdated(taskDTO);
        }

        /// <summary>
        /// Pause task if exit user (userId property) have once 
        /// if it background task - onlypause
        /// if it opertation task it paused and user removes
        /// </summary>
        /// <param name="userId"></param>
        public static void SmartPauseTask(int userId)
        {
            List<ConnectUser> connectionsUserById = ConnectedUserManager.GetConnectionsUserById(userId);
            if (connectionsUserById.Count > 0) return;

            using (var context = new TaskerEntities())
            {
                task currentUserTask =
                    context.tasks.Include(t => t.taskType).Include(t => t.user).Include(t => t.user1).Include(
                        t => t.customer).Include(t => t.customer.channel).FirstOrDefault(
                            t => t.currentUserId == userId && t.statusId == (int) Constants.Statuses.InProcess);
                if (currentUserTask == null) return;

                DateTime now = DateTime.Now;
                int count =
                    context.timeTables.Count(tt => tt.userId == userId && 
                                                   tt.endDate >= now && 
                                                   tt.startDate <= now &&
                                                   tt.statusId == (int) Constants.Statuses.Enabled);
                if (count > 0) return;

                // if it operative
                if (currentUserTask.priority == 1)
                    currentUserTask.currentUserId = null;

                ChangeTaskStatus(currentUserTask, Constants.Statuses.Disabled, userId, context);
            }
        }

        public static bool ChangeTaskStatusIfNotClose(int taskId, Constants.Statuses status, int currentUserId)
        {
            using (var context = new TaskerEntities())
            {
                // getting this task
                task efTask =
                    context.tasks.Include(t => t.taskType).Include(t => t.user).Include(t => t.user1).Include(
                        t => t.customer).Include(t => t.customer.channel).
                            FirstOrDefault(a => a.taskId == taskId);
                // check if exist
                if (efTask == null) return false;
                // check if not closed, we not working with closed tasks
                if (efTask.statusId == (int) Constants.Statuses.Closed) return false;

                if (status == Constants.Statuses.InProcess)
                {
                    bool anyLikeThat =
                        context.tasks.Any(
                            t =>
                            t.currentUserId.HasValue && t.currentUserId == currentUserId && t.statusId == (int) status &&
                            t.taskId != taskId);
                    if (anyLikeThat)
                    {
                        ChangeTaskStatus(efTask, Constants.Statuses.Disabled, currentUserId, context);
                        return false;
                    }
                }

                ChangeTaskStatus(efTask, status, currentUserId, context);
                return true;
            }
        }
    }
}
