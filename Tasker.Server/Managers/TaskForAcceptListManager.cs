﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using Tasker.Server.DTO;
using Tasker.Server.Managers.Logger;
using Tasker.Server.Services;

namespace Tasker.Server.Managers
{
    public enum TaskForAccpetState
    {
        None,
        FirstManager,
        SecondManager,
        Administartor,
    }

    public class TaskForAccpet
    {
        public TaskForAccpet(TaskDTO taskId)
        {
            Task = taskId;
            State = TaskForAccpetState.None;
        }

        public Timer Timer { get; set; }
        public TaskDTO Task { get; set; }
        public TaskForAccpetState State { get; set; }
        public int AcceptUserId { get; set; }
    }

    class TaskForAcceptListManager
    {
        private static readonly List<TaskForAccpet> TaskForAcceptList = new List<TaskForAccpet>();

#if DEBUG
        private static readonly double FirstSendInterval = TimeSpan.FromSeconds(10).TotalMilliseconds;
        private static readonly double SecondSendInterval = TimeSpan.FromSeconds(5).TotalMilliseconds;
#else
        private static readonly double FirstSendInterval = TimeSpan.FromMinutes(10).TotalMilliseconds;
        private static readonly double SecondSendInterval = TimeSpan.FromMinutes(5).TotalMilliseconds;
#endif

        public static void SendRequestAgain(ConnectUser connectUser)
        {
            IEnumerable<TaskForAccpet> taskForAccpets = TaskForAcceptList.Where(t => t.AcceptUserId == connectUser.Id);
            foreach (var taskForAccpet in taskForAccpets)
            {
                UsersManager.ManagersAlerter.SendTaskForReview(taskForAccpet.Task, connectUser, DateTime.Now);
            }
        }

        public static void UserLeave(int userLeaveId)
        {
            List<TaskForAccpet> taskForAccpets = TaskForAcceptList.Where(t => t.AcceptUserId == userLeaveId).ToList();
            foreach (var taskForAccpet in taskForAccpets)
            {
                if (taskForAccpet.Timer == null) continue;
                SetTaskToFirstManager(taskForAccpet);
            }
        }

        public static void AddNewTask(TaskDTO task, int currentUserId)
        {
            TaskForAccpet accpet = TaskForAcceptList.FirstOrDefault(t => t.Task.Id == task.Id);
            if (accpet == null)
            {
                TaskForAccpet taskForAccpet = new TaskForAccpet(task);
                TaskForAcceptList.Add(taskForAccpet);

                IEnumerable<ConnectUser> thisTaskAdmins =
                    ConnectedUserManager.GetConnectedAdmins().Where(a => a.Id == task.CreatorId);

                if (thisTaskAdmins.Any())
                {
                    taskForAccpet.Timer = null;
                    foreach (var admin in thisTaskAdmins)
                    {
                        taskForAccpet.AcceptUserId = admin.Id;
                        UsersManager.ManagersAlerter.SendTaskForReview(task, admin, DateTime.Now);
                    }
                }
                else if (task.CreatorId == ConnectedUserManager.GetResponsibleForAirUserId())
                {
                    taskForAccpet.AcceptUserId = task.CreatorId;
                }
                else
                {
                    // иначе пускаем по стандартному кругу
                    taskForAccpet.Timer = CreateNewTimer();
                    SetTaskToFirstManager(taskForAccpet);
                    taskForAccpet.Timer.Start();
                }
            }
            string mes = string.Format("{0} {1} tasks for review left.", DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"),
                TaskForAcceptList.Count);
            Console.WriteLine(mes);
            Log.Write(LogLevel.Info, mes);
        }

        public static void RemoveTask(int taskId, int currentUserId, bool isClosed)
        {
            List<TaskForAccpet> allThisTaskForAccpets = TaskForAcceptList.Where(t => t.Task.Id == taskId).ToList();
            foreach (var taskForAccpet in allThisTaskForAccpets)
            {
                if (taskForAccpet != null)
                {
                    if (taskForAccpet.AcceptUserId != null)
                    {
                        UsersManager.ManagersAlerter.TaskForReviewCancel(taskForAccpet.Task, taskForAccpet.AcceptUserId);
                        UsersManager.WorkerAlert.SetTaskReviewResult(isClosed, currentUserId);
                    }
                    if (taskForAccpet.Timer != null)
                    {
                        taskForAccpet.Timer.Stop();
                        taskForAccpet.Timer.Dispose();
                    }
                    TaskForAcceptList.Remove(taskForAccpet);
                }
            }
            string mes = String.Format("{0} {1} tasks for review left.", DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"),
                TaskForAcceptList.Count);
            Console.WriteLine(mes);
            Log.Write(LogLevel.Info, mes);
        }

        public static void RemoveTask(int taskId)
        {
            List<TaskForAccpet> allThisTaskForAccpets = TaskForAcceptList.Where(t => t.Task.Id == taskId).ToList();
            foreach (var taskForAccpet in allThisTaskForAccpets)
            {
                if (taskForAccpet != null)
                {
                    if (taskForAccpet.Timer != null)
                    {
                        taskForAccpet.Timer.Stop();
                        taskForAccpet.Timer.Dispose();
                    }
                    TaskForAcceptList.Remove(taskForAccpet);
                }
            }

            string mes = String.Format("{0} {1} tasks for review left.", DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"),
                TaskForAcceptList.Count);
            Log.Write(LogLevel.Info, mes);
            Console.WriteLine(mes);
        }

        private static Timer CreateNewTimer()
        {
            Timer timer = new Timer { AutoReset = true };
            timer.Elapsed += timer_Elapsed;
            return timer;
        }

        static void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Timer timer = (Timer)sender;
            // ищем запись о таске по экземпляру Timer (!)
            TaskForAccpet taskForAccpet = TaskForAcceptList.FirstOrDefault(t => t.Timer == timer);

            if (taskForAccpet == null) return;

            switch (taskForAccpet.State)
            {
                case TaskForAccpetState.None:
                    break;
                case TaskForAccpetState.FirstManager:
                    SetTaskToSecondManager(taskForAccpet);
                    break;
                case TaskForAccpetState.SecondManager:
                    SetTaskToAdmin(taskForAccpet);
                    break;
                case TaskForAccpetState.Administartor:
                    timer.Stop();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private static void SetTaskToFirstManager(TaskForAccpet taskForAccpet)
        {
            ConnectUser connectUser = ConnectedUserManager.GetConnectedManagers().FirstOrDefault(u => u.Id == taskForAccpet.Task.Creator.Id);

            if (connectUser == null)
            {
                connectUser = ConnectedUserManager.GetConnectedManagers().FirstOrDefault();
                if (connectUser == null)
                {
                    SetTaskToSecondManager(taskForAccpet);
                    return;
                }
            }

            // десять минут
            taskForAccpet.Timer.Interval = FirstSendInterval;
            // отменяем у текущего пользователя
            UsersManager.ManagersAlerter.TaskForReviewCancel(taskForAccpet.Task, taskForAccpet.AcceptUserId);
            taskForAccpet.State = TaskForAccpetState.FirstManager;
            taskForAccpet.AcceptUserId = connectUser.Id;
            UsersManager.ManagersAlerter.SendTaskForReview(taskForAccpet.Task, taskForAccpet.AcceptUserId, DateTime.Now);
        }

        private static void SetTaskToSecondManager(TaskForAccpet taskForAccpet)
        {
            ConnectUser connectUser = ConnectedUserManager.GetConnectedManagers().FirstOrDefault(u => u.Id != taskForAccpet.AcceptUserId);

            if (connectUser != null)
            {
                // пять минут
                taskForAccpet.Timer.Interval = SecondSendInterval;
                // отменяем у текущего пользователя
                UsersManager.ManagersAlerter.TaskForReviewCancel(taskForAccpet.Task, taskForAccpet.AcceptUserId);
                taskForAccpet.State = TaskForAccpetState.SecondManager;
                taskForAccpet.AcceptUserId = connectUser.Id;
                UsersManager.ManagersAlerter.SendTaskForReview(taskForAccpet.Task, taskForAccpet.AcceptUserId, DateTime.Now);
            }
            else
            {
                SetTaskToAdmin(taskForAccpet);
            }
        }

        private static void SetTaskToAdmin(TaskForAccpet taskForAccpet)
        {
            int responsibleForAirUserId = ConnectedUserManager.GetResponsibleForAirUserIdIfExist();
            if (responsibleForAirUserId == 0)
            {
                var firstOrDefault = ConnectedUserManager.GetConnectedAdmins().FirstOrDefault(u => u.Id != taskForAccpet.AcceptUserId);
                if (firstOrDefault != null)
                    responsibleForAirUserId = firstOrDefault.Id;
            }

            if (responsibleForAirUserId == 0)
            {
                taskForAccpet.State = TaskForAccpetState.None;
                return;
            }

            taskForAccpet.Timer.Stop();
            UsersManager.ManagersAlerter.TaskForReviewCancel(taskForAccpet.Task, taskForAccpet.AcceptUserId);

            taskForAccpet.AcceptUserId = responsibleForAirUserId;
            taskForAccpet.State = TaskForAccpetState.Administartor;
            UsersManager.ManagersAlerter.SendTaskForReview(taskForAccpet.Task, taskForAccpet.AcceptUserId, DateTime.Now);
        }
    }
}