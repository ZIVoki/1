using System;
using Tasker.Server.DTO;

namespace Tasker.Server.Managers
{
    public class LunchRequest
    {
        public LunchRequest(UserDTO fromUser, int toUserId)
        {
            FromUser = fromUser;
            ToUserId = toUserId;
            CreateDate = DateTime.Now;
        }

        public UserDTO FromUser { get; set; }
        public int ToUserId { get; set; }
        public DateTime CreateDate { get; private set; }
    }
}