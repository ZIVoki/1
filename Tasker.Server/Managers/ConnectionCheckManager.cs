﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace Tasker.Server.Managers
{
    internal class ConnectionCheckManager
    {
        public static bool CheckConnection(out string message)
        {
            message = String.Empty;
            bool result = true;
            SqlConnection connection = new SqlConnection(Connector.GetConnStr());
            try
            {
                connection.Open();
            }
            catch (Exception exception)
            {
                message = string.Format("Ошибка соединения с базой данных {0}. {1}", connection.ConnectionString, exception.Message);
                result = false;
            }
            finally
            {
                connection.Close();
            }
            return result;
        }
    }

    internal static class Connector
    {
            public static string GetConnStr()
            {
                var dbConnString = LoadDBConnString();

                string server;
                string userId;
                string catalog;
                string pass;
                bool trust;

                if (ParseConnString(dbConnString, out server, out userId, out catalog, out pass, out trust))
                {
                    var connectionString = string.Format(Constants.Connection.Server, server);

                    if (trust)
                        connectionString += Constants.Connection.TrustConn;
                    else
                    {
                        connectionString += string.Format(Constants.Connection.UserId, userId);
                        connectionString += string.Format(Constants.Connection.Password, pass);
                    }

                    connectionString += string.Format(Constants.Connection.Catalog, catalog);
                    return connectionString;
                }
                return string.Empty;
            }

            /// <summary>
            /// load database connection string
            /// </summary>
            public static string LoadDBConnString()
            {
                return ConfigurationManager.ConnectionStrings[Constants.Connection.ConnectionStringName].ConnectionString;
            }

            /// <summary>
            /// Parse connection string from app.config
            /// </summary>
            /// <param name="connString">connection string</param>
            /// <param name="server">out server</param>
            /// <param name="userId">out userId</param>
            /// <param name="catalog">out catalog</param>
            /// <param name="pass">out password</param>
            /// <param name="trust">is trust connection with windows auth</param>
            /// <returns></returns>
            private static bool ParseConnString(
                string connString, out string server, out string userId, out string catalog, out string pass, out bool trust)
            {
                server = string.Empty;
                userId = string.Empty;
                catalog = string.Empty;
                pass = string.Empty;
                trust = false;
                var metaData = string.Empty;
                //provider = string.Empty;

                string[] connParams = connString.Split(';');

                foreach (string s in connParams)
                {
                    if (Regex.IsMatch(s, "metadata=(.+)"))
                    {
                        metaData = Regex.Match(s, "metadata=(.+)").Groups[1].Value;
                    }
                    if (Regex.IsMatch(s, "provider=(.+)"))
                    {
                        //provider = Regex.Match(s, "provider=(.+)").Groups[1].Value;
                    }


                    if (Regex.IsMatch(s, @"[P|p]assword\s*=(.+)"))
                    {
                        pass = Regex.Match(s, @"[P|p]assword\s*=(.+)").Groups[1].Value;
                    }
                    else if (Regex.IsMatch(s, "Data Source=(.+)"))
                    {
                        server = Regex.Match(s, "Data Source=(.+)").Groups[1].Value;
                    }
                    else if (Regex.IsMatch(s, @"[C|c]atalog\s*=(.+)"))
                    {
                        catalog = Regex.Match(s, @"[C|c]atalog\s*=(.+)").Groups[1].Value;
                    }
                    else if (Regex.IsMatch(s, @"[I|i][D|d]\s*=(.+)"))
                    {
                        userId = Regex.Match(s, @"[I|i][D|d]\s*=(.+)").Groups[1].Value;
                    }
                    else if (Regex.IsMatch(s, "Integrated Security=(.+)"))
                    {
                        trust = true;
                    }
                }
                return connParams.Length != 0;
            }

        }
}
