﻿using System;
using System.Linq;
using System.Data.Entity;

namespace Tasker.Server.Managers
{
    class AttendanceLogManager
    {
        public static void SaveAttendanceItem(int userId, Constants.Statuses status)
        {
            using (var context = new TaskerEntities())
            {
                SaveAttendanceItem(userId, status, context);
            }
        }

        public static void SaveAttendanceItem(int userId, Constants.Statuses status, TaskerEntities context)
        {
            DateTime now = DateTime.Now;
            int? timeTableId = null;
            //get all messages for this user in task context
            timeTable timeTable = context.timeTables.Include(tt => tt.shift).Include(t => t.user).FirstOrDefault(c => c.startDate <= now &&
                                                                                                                      c.endDate >= now &&
                                                                                                                      c.userId == userId);

            if (timeTable != null) timeTableId = timeTable.timeTableId;

            attendanceLog item = new attendanceLog
            {
                timeTableId = timeTableId,
                userId = userId,
                EventDateTime = now,
                statusId = (int)status,
            };

            context.attendanceLogs.ApplyChanges(item);
            context.SaveChanges();
        }
    }
}
