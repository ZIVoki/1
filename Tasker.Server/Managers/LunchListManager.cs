using System;
using System.Collections.Generic;
using System.Linq;
using Tasker.Server.DTO;
using Tasker.Server.Managers.Logger;
using Tasker.Server.Services;

namespace Tasker.Server.Managers
{
    class LunchListManager
    {
        private static readonly List<LunchRequest> LunchRequestsList = new List<LunchRequest>();

        /// <summary>
        /// do not send request
        /// </summary>
        /// <param name="userDto"></param>
        /// <param name="userId"></param>
        public static void AddNewRequest(UserDTO userDto, int userId)
        {
            LunchRequestsList.Add(new LunchRequest(userDto, userId));
            string message = String.Format("{0} {1} lunch requset for review left.",
                                           DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"), LunchRequestsList.Count);

            Console.WriteLine(message);
            Log.Write(LogLevel.Info, message);
        }

        /// <summary>
        /// ����������� ����� ������� �� ����� ����������� � ������ Id
        /// </summary>
        /// <param name="userDto"></param>
        /// <param name="connectUser"></param>
        public static void AddNewRequest(UserDTO userDto, ConnectUser connectUser)
        {
            LunchRequestsList.Add(new LunchRequest(userDto, connectUser.Id));
            UsersManager.ManagersAlerter.SendLunchRequest(userDto, connectUser);

            string message = String.Format("Adding request from {2} - {0} {1} lunch requset for review left.",
                                           DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"), LunchRequestsList.Count, userDto.LastName);
            Console.WriteLine(message);
            Log.Write(LogLevel.Info, message);
        }

        public static void RemoveRequest(int workerId)
        {
            List<LunchRequest> allUserLunchRequests = LunchRequestsList.Where(rr => rr.FromUser.Id == workerId).ToList();
            foreach (var lunchRequestToRemove in allUserLunchRequests)
            {
                if (lunchRequestToRemove != null)
                    LunchRequestsList.Remove(lunchRequestToRemove);

                if (lunchRequestToRemove != null)
                {
                    string message = String.Format("Removing request from {2} - {0} {1} lunch requset for review left.",
                                                   DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"), LunchRequestsList.Count,
                                                   lunchRequestToRemove.FromUser.LastName);
                    Console.WriteLine(message);
                    Log.Write(LogLevel.Info, message);
                }
            }
        }

        public static void SendRequestsAgain(ConnectUser connectUser)
        {
            IEnumerable<LunchRequest> lunchRequests = LunchRequestsList.Where(lr => lr.ToUserId == connectUser.Id);
            foreach (var lunchRequest in lunchRequests)
            {
                UsersManager.ManagersAlerter.SendLunchRequest(lunchRequest.FromUser, connectUser);

                string message = String.Format("Sending request again for {2} - {0} {1} lunch requset for review left.",
                               DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"), LunchRequestsList.Count, lunchRequest.FromUser.LastName);
                Console.WriteLine(message);
                Log.Write(LogLevel.Info, message);
            }


            //            BackgroundWorker cleaner = new BackgroundWorker();
            //            cleaner.DoWork += cleaner_DoWork;
            //            cleaner.RunWorkerAsync();
        }

        /*
                static void cleaner_DoWork(object sender, DoWorkEventArgs e)
                {
                    IEnumerable<LunchRequest> lunchRequests = _lunchRequestsList.Where(lr => lr.CreateDate.Date < DateTime.Now.Date);
                    foreach (var lunchRequestToDelete in lunchRequests)
                    {
                        _lunchRequestsList.Remove(lunchRequestToDelete);
                    }
                }
        */
    }
}