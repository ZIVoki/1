﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.ServiceModel;
using System.Text;
using Tasker.Server.DTO;
using Tasker.Server.Managers.Logger;
using Tasker.Server.ServiceInterfaces;
using Tasker.Server.ServiceInterfaces.Manager;
using Tasker.Server.ServiceInterfaces.Worker;
using Tasker.Server.Services;

namespace Tasker.Server.Managers
{
    internal class UsersManager
    {
        public static string GetMd5HashFromString(string forCrypt)
        {
            if (forCrypt == null) forCrypt = "";
            MD5 md5 = new MD5CryptoServiceProvider();
            var encoder = new UTF8Encoding();
            var hashedBytes = md5.ComputeHash(encoder.GetBytes(forCrypt));

            var sb = new StringBuilder();
            for (var i = 0; i < hashedBytes.Length; i++)
            {
                sb.Append(hashedBytes[i].ToString("x2"));
            }
            return sb.ToString();
        }

        public static ConnectUser Connect(user worker)
        {
            //Получаем Интерфейс обратного вызова  
            var callback = OperationContext.Current.GetCallbackChannel<IClientCallback>();
            var sessionId = OperationContext.Current.SessionId;

            //Создаем новый экземплар пользователя и заполняем все его поля  
            var chatUser = new ConnectUser(worker)
            {
                Callback = callback,
                SessionId = sessionId,
                NetWorkState = Constants.NetworkStates.Online
            };

            List<ConnectUser> connectionsUserById = ConnectedUserManager.GetConnectionsUserById(chatUser.Id);

            if (connectionsUserById.Count == 0)
            {
                AttendanceLogManager.SaveAttendanceItem(worker.userId, Constants.Statuses.UserEnter);

                //Оповещаем всех клиентов что в онлайн вошел новый пользователь 
                foreach (var user in ConnectedUserManager.GetAllConnectedUsers())
                {
                    try
                    {
                        if (OperationContext.Current == null) break;
                        if (OperationContext.Current.Channel == null) break;
                        if (OperationContext.Current.Channel.State == CommunicationState.Opened)
                        {
                            user.Callback.UserEnter(chatUser.Id);
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        Log.Write(LogLevel.Warning, string.Format("{0}/n {1}", e.Message, e.StackTrace));
                        // удаляем так как сессия устарела
                        ConnectedUserManager.RemoveUser(user);
                    }
                }
            }

            ConnectedUserManager.AddUser(chatUser);

            ManagerService.UpdateUserTask(worker.userId, worker.userId);
            return chatUser;
        }

        public static void LostConnection(object sender, EventArgs e)
        {
            //            var channel = (IClientChannel) sender;
            //            Disconnect(channel.SessionId);
        }

        public static void Disconnect(ConnectUser connectUser)
        {
            ConnectedUserManager.RemoveUser(connectUser);

            if (!ConnectedUserManager.GetConnectionsUserById(connectUser.Id).Any())
            {
                AttendanceLogManager.SaveAttendanceItem(connectUser.Id, Constants.Statuses.UserLeave);

                //Оповещаем всех клиентов о том что пользователь вышел
                foreach (var user in ConnectedUserManager.GetAllConnectedUsers())
                {
                    try
                    {
                        if (OperationContext.Current == null) break;
                        if (OperationContext.Current.Channel.State == CommunicationState.Opened)
                            user.Callback.UserLogoff(connectUser.Id);
                    }
                    catch (Exception e)
                    {
                        Log.Write(LogLevel.Warning, e.StackTrace);
                        Console.WriteLine(e);
                        // не актуальная сессия, удаляем
                        ConnectedUserManager.RemoveUser(user);
                    }
                }

                if (connectUser.Role != Constants.Roles.Worker)
                    TaskForAcceptListManager.UserLeave(connectUser.Id);
                AllAlerter.UserStateChanged(connectUser.Id, Constants.NetworkStates.Offline);

                TaskManager.SmartPauseTask(connectUser.Id);
            }
        }

        public static DateTime? SendMessageToUser(MessageDTO newMessage)
        {
            DateTime? recieveTime = null;

            List<ConnectUser> connectionsUserById = ConnectedUserManager.GetConnectionsUserById(newMessage.ToUserId);

            foreach (var connectUser in connectionsUserById)
            {
                if (connectUser == null) continue;
                connectUser.Callback.ReceiveMessage(newMessage);
                recieveTime = DateTime.Now;
            }

            return recieveTime;
        }

        public static DateTime? SendMessageToUsers(MessageDTO newMessage, List<int> sendersIds)
        {
            DateTime? recieveTime = null;

            List<ConnectUser> connectedUsers =
                ConnectedUserManager.GetAllConnectedUsers()
                                    .Where(
                                        connectUser =>
                                        sendersIds.Contains(connectUser.Id) || newMessage.ToUserId == connectUser.Id)
                                    .ToList();

            foreach (var connectUser in connectedUsers)
            {
                if (connectUser == null) continue;
                if (connectUser.SessionId == OperationContext.Current.SessionId) continue;
                connectUser.Callback.ReceiveMessage(newMessage);
                recieveTime = DateTime.Now;
            }

            return recieveTime;
        }


        #region Nested type: AllAlerter

        public static class AllAlerter
        {
            public static void UserStateChanged(int userId, Constants.NetworkStates state,
                                                ConnectUser currentUser = null)
            {
                try
                {
                    Log.Write(LogLevel.Info,
                              string.Format("class {0} method {1} action {2}", "AllAlerter", "UserStateChanged",
                                            "Get connected user"));

                    List<ConnectUser> connectionsUserById = ConnectedUserManager.GetConnectionsUserById(userId);
                    foreach (var connectUser in connectionsUserById)
                    {
                        if (connectUser == null) continue;
                        connectUser.NetWorkState = state;
                    }

                    foreach (var connectedUser in ConnectedUserManager.GetAllConnectedUsers())
                    {
                        if (currentUser == null)
                        {
                            if (connectedUser == null) continue;
                            if (connectedUser.NetWorkState == Constants.NetworkStates.Offline) continue;

                            if (OperationContext.Current != null && OperationContext.Current.Channel != null)
                                if (OperationContext.Current.Channel.State == CommunicationState.Opened)
                                {
                                    (connectedUser.Callback).UserStateChanged(userId, state);
                                }
                        }
                        else
                        {
                            if (connectedUser == null || connectedUser.SessionId == currentUser.SessionId) continue;
                                (connectedUser.Callback).UserStateChanged(userId, state);
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    Log.Write(LogLevel.Warning, e.StackTrace);
                }
            }

            public static void TaskUpdated(TaskDTO task)
            {
                if (task == null) return;

                Log.Write(LogLevel.Info, string.Format("class {0} method {1} action ", "AllAlerter", "TaskUpdated"));
                Log.Write(LogLevel.Info, string.Format("Task {0} with id {1} updated", task.Title, task.Id));

                // select current task worker managers amd admins
                var managersAndAdminsPlusOne = ConnectedUserManager.GetAllConnectedUsers();

                //alert them
                foreach (var connectUser in managersAndAdminsPlusOne)
                {
                    try
                    {
                        if (OperationContext.Current == null) break;
                        if (OperationContext.Current.Channel.State != CommunicationState.Opened) continue;
                        if (connectUser == null || connectUser.SessionId == OperationContext.Current.Channel.SessionId) continue;
                        (connectUser.Callback).TaskUpdated(task);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        Log.Write(LogLevel.Warning, e.StackTrace);
                        // удаляем подключение так как оно не актуально
                        ConnectedUserManager.RemoveUser(connectUser);
                    }
                }
            }

            public static void ShiftUpdated(ShiftDTO shiftId, ConnectUser currentUser)
            {
                foreach (var connectUser in ConnectedUserManager.GetAllConnectedUsers())
                {
                    if (connectUser != null && connectUser.SessionId != currentUser.SessionId)
                    {
                        (connectUser.Callback).ShiftUpdated(shiftId);
                    }
                }
            }

            public static void TaskTypeAdded(TaskTypeDTO taskType, ConnectUser currentUser)
            {
                foreach (var connectUser in ConnectedUserManager.GetAllConnectedUsers())
                {
                    if (connectUser != null && connectUser.SessionId != currentUser.SessionId)
                    {
                        (connectUser.Callback).TaskTypeAdded(taskType);
                    }
                }
            }

            public static void TaskTypeUpdated(TaskTypeDTO taskTypeId, ConnectUser currentUser)
            {
                foreach (var connectUser in ConnectedUserManager.GetAllConnectedUsers())
                {
                    if (connectUser != null && connectUser.SessionId != currentUser.SessionId)
                    {
                        (connectUser.Callback).TaskTypeUpdated(taskTypeId);
                    }
                }
            }


            public static void UserTaskTypeAdded(UserTaskTypeDTO userTaskType, ConnectUser currentUser)
            {
                foreach (var connectUser in ConnectedUserManager.GetAllConnectedUsers())
                {
                    if (connectUser != null && connectUser.SessionId != currentUser.SessionId)
                    {
                        (connectUser.Callback).UserTaskTypeAdded(userTaskType);
                    }
                }
            }

            public static void UserTaskTypeUpdated(UserTaskTypeDTO userTaskTypeId, ConnectUser currentUser)
            {
                foreach (var connectUser in ConnectedUserManager.GetAllConnectedUsers())
                {
                    if (connectUser != null && connectUser.SessionId != currentUser.SessionId)
                    {
                        (connectUser.Callback).UserTaskTypeUpdated(userTaskTypeId);
                    }
                }
            }

            public static void CustomerUpdated(CustomerDTO userTaskTypeId, ConnectUser currentUser)
            {
                foreach (var connectUser in ConnectedUserManager.GetAllConnectedUsers())
                {
                    if (connectUser != null && connectUser.SessionId != currentUser.SessionId)
                    {
                        (connectUser.Callback).CustomerUpdated(userTaskTypeId);
                    }
                }
            }

            public static void TimeTableAdded(TimeTableDTO timeTable, ConnectUser currentUser)
            {
                List<ConnectUser> managersAndAdminsPlusOne =
                    ConnectedUserManager.GetConnectManagersAndAdminsPlusOne(timeTable.UserId);
                //alert calendar owner managers and admins
                foreach (var connectUser in managersAndAdminsPlusOne)
                {
                    if (connectUser != null && connectUser.SessionId != currentUser.SessionId)
                    {
                        (connectUser.Callback).TimeTableAdded(timeTable);
                    }
                }
            }

            public static void TimeTableUpdated(TimeTableDTO updatedTimeTable, ConnectUser currentUser)
            {
                var workers = ConnectedUserManager.GetConnectManagersAndAdminsPlusOne(updatedTimeTable.UserId);
                //alert calendar owner
                foreach (var connectUser in workers)
                {
                    if (connectUser != null && connectUser.SessionId != currentUser.SessionId)
                    {
                        (connectUser.Callback).TimeTableUpdated(updatedTimeTable);
                    }
                }
            }

            public static void UserUpdated(UserDTO userId, ConnectUser currentUser)
            {
                var workers = ConnectedUserManager.GetConnectManagersAndAdminsPlusOne(userId.Id);
                foreach (var connectUser in workers)
                {
                    if (connectUser != null && connectUser.SessionId != currentUser.SessionId)
                    {
                        (connectUser.Callback).UserUpdated(userId);
                    }
                }
            }

            public static void FileAdded(int taskId, AttachFileDTO attachFile, ConnectUser currentUser)
            {
                // todo get only users we need
                foreach (var connectUser in ConnectedUserManager.GetAllConnectedUsers())
                {
                    if (connectUser != null && currentUser.SessionId != connectUser.SessionId)
                    {
                        (connectUser.Callback).FileAdded(taskId, attachFile);
                    }
                }
            }
        }

        #endregion

        #region Nested type: ManagersAlerter

        public static class ManagersAlerter
        {
            public static void UserComeBack(int userId, ConnectUser currentUser)
            {
                foreach (var connectedUser in ConnectedUserManager.GetAllConnectedUsers())
                {
                    if (connectedUser != null)
                    {
                        if (connectedUser.Id == currentUser.Id)
                            connectedUser.NetWorkState = Constants.NetworkStates.Online;
                        else if (connectedUser.Role == Constants.Roles.Manager ||
                                 connectedUser.Role == Constants.Roles.Administrator)
                            ((IManagerCallback)connectedUser.Callback).WorkerComeback(userId);
                    }
                }
            }

            public static void TaskAccepted(int taskId, int byUserId, DateTime acceptedTime)
            {
                foreach (var connectedUser in ConnectedUserManager.GetConnectManagersAndAdmins())
                {
                    ((IManagerCallback)connectedUser.Callback).TaskAccepted(taskId, byUserId, acceptedTime);
                }
            }

            public static void SendTaskForReview(TaskDTO taskDTO, int managerId, DateTime sendTime)
            {
                List<ConnectUser> connectionsUserById = ConnectedUserManager.GetConnectionsUserById(managerId);

                foreach (var connectUser in connectionsUserById)
                {
                    SendTaskForReview(taskDTO, connectUser, sendTime);
                }
            }

            public static void SendTaskForReview(TaskDTO taskDTO, ConnectUser connectUser, DateTime sendTime)
            {
                if (connectUser == null)
                {
                    string message =
                        String.Format("Available manager was not found during task review request by task: {0}",
                                      taskDTO.Title);
                    Log.Write(LogLevel.Warning, message);
                    return;
                }

                ((IManagerCallback)connectUser.Callback).TaskForReviewRecieved(taskDTO, sendTime);
            }

            public static void TaskForReviewCancel(TaskDTO taskDto, int userId)
            {
                List<ConnectUser> connectionsUserById = ConnectedUserManager.GetConnectionsUserById(userId);

                foreach (var connectUser in connectionsUserById)
                {
                    if (connectUser != null)
                    {
                        ((IManagerCallback)connectUser.Callback).TaskForReviewCancel(taskDto);
                    }
                }
            }

            public static void SendNewLunchRequest(UserDTO fromUserDTO, int? managerId = null)
            {
                List<ConnectUser> responsibleForAir = ConnectedUserManager.GetResponsibleForAir();

                if (responsibleForAir == null || responsibleForAir.Count == 0)
                {
                    int responsibleForAirUserId = ConnectedUserManager.GetResponsibleForAirUserId();
                    if (responsibleForAirUserId == 0)
                    {
                        ConnectUser connectUser = ConnectedUserManager.GetConnectManagersAndAdmins().FirstOrDefault();
                        if (connectUser != null)
                            LunchListManager.AddNewRequest(fromUserDTO, connectUser);
                        else
                            Log.Write(LogLevel.Warning, "Any managers during lunch request was not found.");
                    }
                    else
                        LunchListManager.AddNewRequest(fromUserDTO, responsibleForAirUserId);
                }
                else
                {
                    try
                    {
                        foreach (var connectUser in responsibleForAir)
                        {
                            LunchListManager.AddNewRequest(fromUserDTO, connectUser);
                        }
                    }
                    catch (Exception e)
                    {
                        Log.Write(LogLevel.Warning, e.Message);
                        Console.WriteLine(e.Message);
                    }
                }
            }

            public static void SendLunchRequest(UserDTO fromUserDTO, ConnectUser connectUser)
            {
                ((IManagerCallback)connectUser.Callback).LunchRequest(fromUserDTO);
            }

            public static void TaskAdded(TaskDTO task, ConnectUser currentUser)
            {
                foreach (var connectUser in ConnectedUserManager.GetConnectManagersAndAdmins())
                {
                    if (connectUser != null && connectUser.SessionId != currentUser.SessionId)
                    {
                        ((IManagerCallback)connectUser.Callback).TaskAdded(task);
                    }
                }
            }

            public static void TaskChecked(bool isLocked, TaskDTO taskDTO, ConnectUser currentUser)
            {
                foreach (var connectUser in ConnectedUserManager.GetConnectManagersAndAdmins())
                {
                    if (connectUser == null || connectUser.SessionId == currentUser.SessionId) continue;
                    if (isLocked)
                    {
                        ((IManagerCallback)connectUser.Callback).TaskCheckedIn(taskDTO, currentUser.Surname);
                    }
                    else
                    {
                        ((IManagerCallback)connectUser.Callback).TaskCheckedOut(taskDTO, currentUser.Surname);
                    }
                }
            }

            public static void TaskCheckedTarget(bool isLocked, TaskDTO taskDTO, ConnectUser currentUser)
            {
                if (currentUser.Callback is IManagerCallback)
                {
                    var callback = ((IManagerCallback)currentUser.Callback);
                    if (isLocked)
                    {
                        callback.TaskCheckedIn(taskDTO, currentUser.Surname);
                    }
                    else
                    {
                        callback.TaskCheckedOut(taskDTO, currentUser.Surname);
                    }
                }
            }

            public static void CustomerAdded(CustomerDTO customer, ConnectUser currentUser)
            {
                foreach (var connectUser in ConnectedUserManager.GetConnectManagersAndAdmins())
                {
                    if (connectUser != null && connectUser.SessionId != currentUser.SessionId)
                        ((IManagerCallback)connectUser.Callback).CustomerAdded(customer);
                }
            }

            public static void UserAdded(UserDTO user, ConnectUser currentUser)
            {
                foreach (var connectUser in ConnectedUserManager.GetConnectManagersAndAdmins())
                {
                    if (connectUser != null && connectUser.SessionId != currentUser.SessionId)
                    {
                        ((IManagerCallback)connectUser.Callback).UserAdded(user);
                    }
                }
            }

            public static void ShiftAdded(ShiftDTO shift, ConnectUser currentUser)
            {
                foreach (var connectUser in ConnectedUserManager.GetAllConnectedUsers())
                {
                    if (connectUser != null && connectUser.SessionId != currentUser.SessionId)
                    {
                        ((IManagerCallback)connectUser.Callback).ShiftAdded(shift);
                    }
                }
            }

            public static void WorkerBusy(UserDTO currentWorker, TaskDTO taskDTO)
            {
                foreach (var connectUser in ConnectedUserManager.GetConnectManagersAndAdmins())
                {
                    if (connectUser != null)
                    {
                        ((IManagerCallback)connectUser.Callback).WorkerBusy(currentWorker, taskDTO);
                    }
                }
            }

            public static void WorkerFree(UserDTO currentWorker, int currentUserId)
            {
                foreach (var connectUser in ConnectedUserManager.GetConnectManagersAndAdmins())
                {
                    if (connectUser != null)
                    {
                        ((IManagerCallback)connectUser.Callback).WorkerFree(currentWorker);
                    }
                }
            }

            public static void WorkerBlocked(UserDTO userDTO, ConnectUser currentUser)
            {
                foreach (var connectUser in ConnectedUserManager.GetConnectManagersAndAdmins())
                {
                    if (connectUser != null)
                    {
                        ((IManagerCallback)connectUser.Callback).WorkerBlocked(userDTO);
                    }
                }
            }

            public static void WorkerBlockedTarget(UserDTO userDTO, ConnectUser currentUser)
            {
                if (currentUser.Callback is IManagerCallback)
                {
                    ((IManagerCallback)currentUser.Callback).WorkerBlocked(userDTO);
                }
            }

            public static void CustomerTaskTypeAdded(CustomerTaskTypeDTO customerTaskTypeDto)
            {
                foreach (var connectedUser in ConnectedUserManager.GetAllConnectedUsers())
                {
                    if (connectedUser == null) continue;
                    if (OperationContext.Current.Channel.SessionId == connectedUser.SessionId) continue;

                    if (connectedUser.Role == Constants.Roles.Manager ||
                        connectedUser.Role == Constants.Roles.Administrator)
                        ((IManagerCallback)connectedUser.Callback).CustomerTaskTypeAdded(customerTaskTypeDto);
                }
            }

            public static void CustomerTaskTypeUpdated(CustomerTaskTypeDTO customerTaskTypeDto)
            {
                foreach (var connectedUser in ConnectedUserManager.GetAllConnectedUsers())
                {
                    if (connectedUser == null) continue;
                    if (OperationContext.Current.Channel.SessionId == connectedUser.SessionId) continue;

                    if (connectedUser.Role == Constants.Roles.Manager ||
                        connectedUser.Role == Constants.Roles.Administrator)
                        ((IManagerCallback)connectedUser.Callback).CustomerTaskTypeUpdated(customerTaskTypeDto);
                }
            }

            public static void UserReadFile(UserReadFileDTO userReadFileDTO, ConnectUser currentUser)
            {
                foreach (var connectUser in ConnectedUserManager.GetConnectManagersAndAdmins())
                {
                    if (connectUser != null && connectUser.SessionId != currentUser.SessionId)
                    {
                        ((IManagerCallback)connectUser.Callback).UserReadFile(userReadFileDTO);
                    }
                }
            }

            public static void NewBoardFileVersionAdded(FileVersionDTO newFileVersionDTO, ConnectUser currentUser = null)
            {
                foreach (var connectUser in ConnectedUserManager.GetConnectManagersAndAdmins())
                {
                    if (connectUser != null)
                    {
                        if (currentUser != null && currentUser.SessionId != connectUser.SessionId) continue;
                        (connectUser.Callback).BoardFileNewVersionAdded(newFileVersionDTO);
                    }
                }
            }
        }

        #endregion

        #region Nested type: WorkerAlert

        public static class WorkerAlert
        {
            public static void LunchRequestResult(bool changeState, int workerId)
            {
                List<ConnectUser> connectionsUserById = ConnectedUserManager.GetConnectionsUserById(workerId);

                foreach (var worker in connectionsUserById)
                {
                    if (worker != null)
                    {
                        ((IWorkerCallback)worker.Callback).SetStatusLunchTime(changeState);
                        if (changeState)
                            AttendanceLogManager.SaveAttendanceItem(worker.Id, Constants.Statuses.UserLunch);
                    }
                    else
                    {
                        string message =
                            string.Format(
                                "Worker with id {0} can not get lunch response. Reason - worker stend offline.",
                                workerId);
                        Log.Write(LogLevel.Warning, message);
                    }
                }

                // отменяем запрос на обед напрочих клиентахданного юзера
                List<ConnectUser> currentUserConnections = ConnectedUserManager.GetCurrentUserConnections();
                foreach (var currentUserConnection in currentUserConnections)
                {
                    if (currentUserConnection.SessionId == OperationContext.Current.SessionId) continue;
                    ((IManagerCallback)currentUserConnection.Callback).LunchRequestCancel(workerId);
                }
            }

            public static void TaskSet(TaskDTO task)
            {
                if (!task.CurrentWorkerId.HasValue) return;
                try
                {
                    var targets = ConnectedUserManager.GetConnectionsUserById(task.CurrentWorkerId.Value);

                    foreach (var connectUser in targets)
                    {
                        if (connectUser != null)
                        {
                            ((IWorkerCallback)connectUser.Callback).TaskSet(task);
                        }
                    }
                }
                catch (Exception e)
                {
                    Log.Write(LogLevel.Warning, e.StackTrace);
                    Console.WriteLine(e);
                }
            }

            public static void NewTask(TaskDTO task)
            {
                if (!task.CurrentWorkerId.HasValue) return;
                try
                {
                    var targets = ConnectedUserManager.GetConnectionsUserById(task.CurrentWorkerId.Value);

                    foreach (var connectUser in targets)
                    {
                        if (connectUser != null)
                        {
                            Debug.WriteLine(string.Format("Send to {0} about {1} - {2}", connectUser.Surname, task.Title, task.Id));
                            ((IWorkerCallback)connectUser.Callback).NewTask(task);
                        }
                    }
                }
                catch (Exception e)
                {
                    Log.Write(LogLevel.Warning, e.StackTrace);
                    Console.WriteLine(e);
                }
            }

            public static void ChangeTaskCurrentWorker(TaskDTO task, int? oldWorkerId = null)
            {
                TaskSet(task);
                if (oldWorkerId.HasValue)
                    TaskUnset(oldWorkerId.Value, task.Id);
            }

            public static void TaskUnset(int oldWorkerId, int taskId)
            {
                var targets = ConnectedUserManager.GetConnectionsUserById(oldWorkerId);

                foreach (var connectUser in targets)
                {
                    if (connectUser != null)
                    {
                        if (connectUser.Callback is IWorkerCallback)
                            ((IWorkerCallback)connectUser.Callback).TaskUnsetSet(taskId);
                    }
                }
            }

            public static void SetTaskReviewResult(bool isClosed, int workerId)
            {
                List<ConnectUser> connectionsUserById = ConnectedUserManager.GetConnectionsUserById(workerId);
                foreach (var connectUser in connectionsUserById)
                {
                    if (connectUser != null)
                        ((IWorkerCallback)(connectUser.Callback)).SetTaskReviewResult(isClosed);

                }
            }

            public static void NewBoardFileVersionAdded(FileVersionDTO newFileVersionDTO, ConnectUser currentUser = null)
            {
                foreach (var connectUser in ConnectedUserManager.GetConnectedWorkers())
                {
                    if (connectUser != null)
                    {
                        if (currentUser != null && currentUser.SessionId != connectUser.SessionId) continue;
                        (connectUser.Callback).BoardFileNewVersionAdded(newFileVersionDTO);
                    }
                }
            }
        }

        #endregion

    }
}