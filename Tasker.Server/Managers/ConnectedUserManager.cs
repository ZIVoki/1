using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using Tasker.Server.Services;

namespace Tasker.Server.Managers
{
    class ConnectedUserManager
    {
        private static readonly object Flag = new object();
        private static readonly SynchronizedCollection<ConnectUser> ConnectedUsers = new SynchronizedCollection<ConnectUser>(Flag);

        public static void AddUser(ConnectUser user)
        {
            lock (ConnectedUsers.SyncRoot)
            {
                ConnectedUsers.Add(user);
            }
        }

        public static void RemoveUser(ConnectUser user)
        {
            lock (ConnectedUsers.SyncRoot)
            {
                ConnectedUsers.Remove(user);
            }
        }

        public static List<ConnectUser> GetCurrentUserConnections()
        {
            ConnectUser connectionBySessionId = GetConnectionBySessionId(OperationContext.Current.SessionId);
            if (connectionBySessionId == null)
                return new List<ConnectUser>();
            return GetConnectionsUserById(connectionBySessionId.Id);
        }

        public static List<ConnectUser> GetResponsibleForAir()
        {
            List<ConnectUser> result;
            int userId = GetResponsibleForAirUserId();

            if (userId == 0) return null;

            lock (ConnectedUsers.SyncRoot)
            {
                result = ConnectedUsers.Where(c => c.Id == userId).ToList();
            }
            return result;
        }

        public static int GetResponsibleForAirUserId()
        {
            int userId = 0;

            using (var context = new TaskerEntities())
            {
                // �������� �������������� �� ����
                var responsibleForAir = context.timeTables.FirstOrDefault(
                    tt =>
                        tt.statusId == (int)Constants.Statuses.Enabled &&
                        tt.isResp &&
                        tt.startDate <= DateTime.Now &&
                        tt.endDate >= DateTime.Now);

                if (responsibleForAir != null) { userId = responsibleForAir.userId; }
            }

            return userId;
        }

        /// <summary>
        /// Returns 0 if not exist
        /// </summary>
        /// <returns></returns>
        public static int GetResponsibleForAirUserIdIfExist()
        {
            int userId = GetResponsibleForAirUserId();

            bool b;
            lock (ConnectedUsers.SyncRoot)
            {
                b = ConnectedUsers.Any(c => c.Id == userId);
            }

            if (b) return userId;

            return 0;
        }

        public static ConnectUser GetConnectionBySessionId(string sessionId)
        {
            ConnectUser result;
            lock (ConnectedUsers.SyncRoot)
            {
                result = ConnectedUsers.FirstOrDefault(c => c.SessionId == sessionId);
            }
            return result;
        }

        public static List<ConnectUser> GetConnectionsUserById(int userId)
        {
            List<ConnectUser> result;
            lock (ConnectedUsers.SyncRoot)
            {
                result = ConnectedUsers.Where(c => c.Id == userId).ToList();
            }
            return result;
        }

        public static List<ConnectUser> GetAllConnectedUsers()
        {
            List<ConnectUser> result;
            lock (ConnectedUsers.SyncRoot)
            {
                result = ConnectedUsers.ToList();
            }
            return result;
        }

        public static List<ConnectUser> GetConnectedWorkers()
        {
            List<ConnectUser> result;
            lock (ConnectedUsers.SyncRoot)
            {
                result = ConnectedUsers.Where(c => c.Role == Constants.Roles.Worker).ToList();
            }
            return result;
        }

        public static List<ConnectUser> GetConnectedManagers()
        {
            List<ConnectUser> result;
            lock (ConnectedUsers.SyncRoot)
            {
                result = ConnectedUsers.Where(c => c.Role == Constants.Roles.Manager).ToList();
            }
            return result;
        }

        public static List<ConnectUser> GetConnectManagersAndAdmins()
        {
            List<ConnectUser> result;
            lock (ConnectedUsers.SyncRoot)
            {
                result = ConnectedUsers.Where(c => c.Role == Constants.Roles.Administrator || c.Role == Constants.Roles.Manager).ToList();
            }
            return result;
        }

        public static List<ConnectUser> GetConnectedAdmins()
        {
            List<ConnectUser> result;
            lock (ConnectedUsers.SyncRoot)
            {
                result = ConnectedUsers.Where(c => c.Role == Constants.Roles.Administrator).ToList();
            }
            return result;
        }

        public static List<ConnectUser> GetConnectManagersAndAdminsPlusOne(int? userId)
        {
            List<ConnectUser> result;
            lock (ConnectedUsers.SyncRoot)
            {
                if (userId.HasValue)
                    result =
                        ConnectedUsers.Where(
                            c =>
                            c.Role == Constants.Roles.Administrator || c.Role == Constants.Roles.Manager ||
                            c.Id == userId)
                            .ToList();

                result = GetConnectManagersAndAdmins();
            }
            return result;
        }

        public static int GetCount()
        {
            int result;
            lock (ConnectedUsers.SyncRoot)
            {
                result = ConnectedUsers.Count;
            }
            return result;
        }
    }
}