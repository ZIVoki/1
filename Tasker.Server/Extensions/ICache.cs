﻿using System;

namespace Tasker.Server.Extensions
{
    public interface ICache
    {
        bool TryGet(string key, out object value);
        void Add(string key, object value, TimeSpan timeout);
    }
}