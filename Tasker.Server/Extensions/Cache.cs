using System;
using System.Collections.Generic;

namespace Tasker.Server.Extensions
{
    public class Cache : ICache
    {
        readonly Dictionary<string, CacheItem> _cacheDictionary = new Dictionary<string, CacheItem>();
        private static Cache _cache;

        private Cache()
        {}

        public static Cache Instance()
        {
            if (_cache == null)
            {
                _cache = new Cache();
            }
            return _cache;
        }

        public bool TryGet(string key, out object value)
        {
            if (_cacheDictionary.ContainsKey(key))
            {
                CacheItem cacheItem = _cacheDictionary[key];
                if (cacheItem.DeadTime >= DateTime.Now)
                {
                    value = cacheItem.Value;
                    return true;
                }
                else
                {
                    _cacheDictionary.Remove(key);
                }
            }
            value = null;
            return false;
        }

        public void Add(string key, object value, TimeSpan timeout)
        {
            if  (!_cacheDictionary.ContainsKey(key))
                _cacheDictionary.Add(key, new CacheItem {Value = value, DeadTime = DateTime.Now.Add(timeout)});
        }

        class CacheItem
        {
            public object Value { get; set; }
            public DateTime DeadTime { get; set; }
        }
    }
}