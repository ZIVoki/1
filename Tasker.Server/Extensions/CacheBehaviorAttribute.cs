﻿using System;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace Tasker.Server.Extensions
{
    public class CacheBehaviorAttribute : Attribute, IOperationBehavior
    {
        TimeSpan _timeout;

        public CacheBehaviorAttribute(int seconds)
        {
            _timeout = TimeSpan.FromSeconds(seconds);
        }

        public void ApplyDispatchBehavior(OperationDescription operationDescription, DispatchOperation dispatchOperation)
        {
            // Стандартный OperationInvoker
            IOperationInvoker invoker = dispatchOperation.Invoker;

            ICache cache = Cache.Instance();

            // Подменим стандартный OperationInvoker расширенным.
            dispatchOperation.Invoker = new CacheOperationInvoker(invoker, cache, _timeout);
        }

        public void Validate(OperationDescription operationDescription)
        {
        }

        public void AddBindingParameters(OperationDescription operationDescription, BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyClientBehavior(OperationDescription operationDescription, ClientOperation clientOperation)
        {
        }
    }
}