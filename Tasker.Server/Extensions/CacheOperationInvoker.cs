﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Dispatcher;
using System.Text;

namespace Tasker.Server.Extensions
{
    public class CacheOperationInvoker : IOperationInvoker
    {
        IOperationInvoker _invoker;
        ICache _cache;
        TimeSpan _timeout;

        public CacheOperationInvoker(IOperationInvoker invoker, ICache cache, TimeSpan timeout)
        {
            _invoker = invoker; // стандартный OperationInvoker
            _cache = cache;     // наш объект кэша
            _timeout = timeout; // как долго объект будет находиться в кэше
        }

        public object Invoke(object instance, object[] inputs, out object[] outputs)
        {
            // Хэш ключ.
            // Разумно вычислять его на основе
            // входных параметров и названия кешируемого метода.
            // Но мы для простоты обойдемся константой.

            string key = inputs.Aggregate((a,b) => a.ToString() + ' ' + b.ToString()).ToString();

            Tuple<object, object[]> result;
            object cacheItem;
            // Попробуем достать данные из кэша
            if (!_cache.TryGet(key, out cacheItem))
            {
                // Данные в кэше отсутствуют,
                // получим новые данные, вызовем базовый Invoke
                object returnValue = _invoker.Invoke(instance, inputs, out outputs);

                // Подготовим новый элемент кэша (упакуем returnValue и outputs)
                result = Tuple.Create<object, object[]>(returnValue, outputs);

                // Выполним кэширование на указанный timeout
                _cache.Add(key, result, _timeout);
            }
            else
            {
                // Данные нашлись в кэше
                result = cacheItem as Tuple<object, object[]>;
            }
            outputs = result.Item2;
            return result.Item1;
        }

        public object[] AllocateInputs()
        {
            // просто пробрасываем вызов
            return _invoker.AllocateInputs();
        }

        public IAsyncResult InvokeBegin(object instance, object[] inputs, AsyncCallback callback, object state)
        {
            // просто пробрасываем вызов
            return _invoker.InvokeBegin(instance, inputs, callback, state);
        }

        public object InvokeEnd(object instance, out object[] outputs, IAsyncResult result)
        {
            // просто пробрасываем вызов
            return _invoker.InvokeEnd(instance, out outputs, result);
        }

        public bool IsSynchronous
        {
            // просто пробрасываем вызов
            get { return _invoker.IsSynchronous; }
        }
    }
}
