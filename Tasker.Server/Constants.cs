﻿using System;

namespace Tasker.Server
{
    public class Constants
    {
        public enum Statuses
        {
            Nothing = 0,
            Enabled = 1,
            Disabled = 2,
            Deleted = 3,
            Planing = 4,
            Closed = 5,
            InProcess = 6,
            SendedForReview = 7,
            UserEnter = 8,
            UserLeave = 9,
            UserLunch = 10
        }

        public enum BlockedResourceType
        {
            Worker,
            Task
        }

        public enum NetworkStates
        {
            Nothing = 0,
            Online = 1,
            Offline = 2,
            Lunch = 3,
            Depard = 4,
        }

        public enum Roles
        {
            Administrator = 1,
            Manager = 2,
            Worker = 3,
        }

        /// <summary>
        /// Constants and format strings for build connection
        /// </summary>
        public struct Connection
        {
            public static string Server = "Server={0};";
            public static string TrustConn = "Trusted_Connection=yes;";
            public static string UserId = "User ID={0};";
            public static string Password = "Password={0};";
            public static string Catalog = "Initial Catalog={0}";
            public static string ConnectionStringName = "TaskerEntities";
        }

        public const double BlockTime = 180000;

        public static string LastVersionFolderName
        {
            get { return "prev"; }
        }

        public static String AvailableDiffFormats
        {
            get { return ".docx .xlsx"; }
        }
    }
}
