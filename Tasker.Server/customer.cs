//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;

namespace Tasker.Server
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(status))]
    [KnownType(typeof(user))]
    [KnownType(typeof(task))]
    [KnownType(typeof(customersTaskType))]
    [KnownType(typeof(channel))]
    public partial class customer: IObjectWithChangeTracker, INotifyPropertyChanged
    {
        #region Primitive Properties
    
        [DataMember]
        public int customerId
        {
            get { return _customerId; }
            set
            {
                if (_customerId != value)
                {
                    if (ChangeTracker.ChangeTrackingEnabled && ChangeTracker.State != ObjectState.Added)
                    {
                        throw new InvalidOperationException("The property 'customerId' is part of the object's key and cannot be changed. Changes to key properties can only be made when the object is not being tracked or is in the Added state.");
                    }
                    _customerId = value;
                    OnPropertyChanged("customerId");
                }
            }
        }
        private int _customerId;
    
        [DataMember]
        public string title
        {
            get { return _title; }
            set
            {
                if (_title != value)
                {
                    _title = value;
                    OnPropertyChanged("title");
                }
            }
        }
        private string _title;
    
        [DataMember]
        public string description
        {
            get { return _description; }
            set
            {
                if (_description != value)
                {
                    _description = value;
                    OnPropertyChanged("description");
                }
            }
        }
        private string _description;
    
        [DataMember]
        public int statusId
        {
            get { return _statusId; }
            set
            {
                if (_statusId != value)
                {
                    ChangeTracker.RecordOriginalValue("statusId", _statusId);
                    if (!IsDeserializing)
                    {
                        if (status != null && status.statusId != value)
                        {
                            status = null;
                        }
                    }
                    _statusId = value;
                    OnPropertyChanged("statusId");
                }
            }
        }
        private int _statusId;
    
        [DataMember]
        public int channelId
        {
            get { return _channelId; }
            set
            {
                if (_channelId != value)
                {
                    ChangeTracker.RecordOriginalValue("channelId", _channelId);
                    if (!IsDeserializing)
                    {
                        if (channel != null && channel.channelId != value)
                        {
                            channel = null;
                        }
                    }
                    _channelId = value;
                    OnPropertyChanged("channelId");
                }
            }
        }
        private int _channelId;
    
        [DataMember]
        public int maxWorkers
        {
            get { return _maxWorkers; }
            set
            {
                if (_maxWorkers != value)
                {
                    _maxWorkers = value;
                    OnPropertyChanged("maxWorkers");
                }
            }
        }
        private int _maxWorkers;

        #endregion

        #region Navigation Properties
    
        [DataMember]
        public status status
        {
            get { return _status; }
            set
            {
                if (!ReferenceEquals(_status, value))
                {
                    var previousValue = _status;
                    _status = value;
                    Fixupstatus(previousValue);
                    OnNavigationPropertyChanged("status");
                }
            }
        }
        private status _status;
    
        [DataMember]
        public TrackableCollection<user> users
        {
            get
            {
                if (_users == null)
                {
                    _users = new TrackableCollection<user>();
                    _users.CollectionChanged += Fixupusers;
                }
                return _users;
            }
            set
            {
                if (!ReferenceEquals(_users, value))
                {
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        throw new InvalidOperationException("Cannot set the FixupChangeTrackingCollection when ChangeTracking is enabled");
                    }
                    if (_users != null)
                    {
                        _users.CollectionChanged -= Fixupusers;
                    }
                    _users = value;
                    if (_users != null)
                    {
                        _users.CollectionChanged += Fixupusers;
                    }
                    OnNavigationPropertyChanged("users");
                }
            }
        }
        private TrackableCollection<user> _users;
    
        [DataMember]
        public TrackableCollection<task> tasks
        {
            get
            {
                if (_tasks == null)
                {
                    _tasks = new TrackableCollection<task>();
                    _tasks.CollectionChanged += Fixuptasks;
                }
                return _tasks;
            }
            set
            {
                if (!ReferenceEquals(_tasks, value))
                {
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        throw new InvalidOperationException("Cannot set the FixupChangeTrackingCollection when ChangeTracking is enabled");
                    }
                    if (_tasks != null)
                    {
                        _tasks.CollectionChanged -= Fixuptasks;
                    }
                    _tasks = value;
                    if (_tasks != null)
                    {
                        _tasks.CollectionChanged += Fixuptasks;
                    }
                    OnNavigationPropertyChanged("tasks");
                }
            }
        }
        private TrackableCollection<task> _tasks;
    
        [DataMember]
        public TrackableCollection<customersTaskType> customersTaskTypes
        {
            get
            {
                if (_customersTaskTypes == null)
                {
                    _customersTaskTypes = new TrackableCollection<customersTaskType>();
                    _customersTaskTypes.CollectionChanged += FixupcustomersTaskTypes;
                }
                return _customersTaskTypes;
            }
            set
            {
                if (!ReferenceEquals(_customersTaskTypes, value))
                {
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        throw new InvalidOperationException("Cannot set the FixupChangeTrackingCollection when ChangeTracking is enabled");
                    }
                    if (_customersTaskTypes != null)
                    {
                        _customersTaskTypes.CollectionChanged -= FixupcustomersTaskTypes;
                    }
                    _customersTaskTypes = value;
                    if (_customersTaskTypes != null)
                    {
                        _customersTaskTypes.CollectionChanged += FixupcustomersTaskTypes;
                    }
                    OnNavigationPropertyChanged("customersTaskTypes");
                }
            }
        }
        private TrackableCollection<customersTaskType> _customersTaskTypes;
    
        [DataMember]
        public channel channel
        {
            get { return _channel; }
            set
            {
                if (!ReferenceEquals(_channel, value))
                {
                    var previousValue = _channel;
                    _channel = value;
                    Fixupchannel(previousValue);
                    OnNavigationPropertyChanged("channel");
                }
            }
        }
        private channel _channel;

        #endregion

        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (ChangeTracker.State != ObjectState.Added && ChangeTracker.State != ObjectState.Deleted)
            {
                ChangeTracker.State = ObjectState.Modified;
            }
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
        private ObjectChangeTracker _changeTracker;
    
        [DataMember]
        public ObjectChangeTracker ChangeTracker
        {
            get
            {
                if (_changeTracker == null)
                {
                    _changeTracker = new ObjectChangeTracker();
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
                return _changeTracker;
            }
            set
            {
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging -= HandleObjectStateChanging;
                }
                _changeTracker = value;
                if(_changeTracker != null)
                {
                    _changeTracker.ObjectStateChanging += HandleObjectStateChanging;
                }
            }
        }
    
        private void HandleObjectStateChanging(object sender, ObjectStateChangingEventArgs e)
        {
            if (e.NewState == ObjectState.Deleted)
            {
                ClearNavigationProperties();
            }
        }
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
            ChangeTracker.ChangeTrackingEnabled = true;
        }
    
        protected virtual void ClearNavigationProperties()
        {
            status = null;
            users.Clear();
            tasks.Clear();
            customersTaskTypes.Clear();
            channel = null;
        }

        #endregion

        #region Association Fixup
    
        private void Fixupstatus(status previousValue)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.customers.Contains(this))
            {
                previousValue.customers.Remove(this);
            }
    
            if (status != null)
            {
                if (!status.customers.Contains(this))
                {
                    status.customers.Add(this);
                }
    
                statusId = status.statusId;
            }
            if (ChangeTracker.ChangeTrackingEnabled)
            {
                if (ChangeTracker.OriginalValues.ContainsKey("status")
                    && (ChangeTracker.OriginalValues["status"] == status))
                {
                    ChangeTracker.OriginalValues.Remove("status");
                }
                else
                {
                    ChangeTracker.RecordOriginalValue("status", previousValue);
                }
                if (status != null && !status.ChangeTracker.ChangeTrackingEnabled)
                {
                    status.StartTracking();
                }
            }
        }
    
        private void Fixupchannel(channel previousValue)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.customers.Contains(this))
            {
                previousValue.customers.Remove(this);
            }
    
            if (channel != null)
            {
                if (!channel.customers.Contains(this))
                {
                    channel.customers.Add(this);
                }
    
                channelId = channel.channelId;
            }
            if (ChangeTracker.ChangeTrackingEnabled)
            {
                if (ChangeTracker.OriginalValues.ContainsKey("channel")
                    && (ChangeTracker.OriginalValues["channel"] == channel))
                {
                    ChangeTracker.OriginalValues.Remove("channel");
                }
                else
                {
                    ChangeTracker.RecordOriginalValue("channel", previousValue);
                }
                if (channel != null && !channel.ChangeTracker.ChangeTrackingEnabled)
                {
                    channel.StartTracking();
                }
            }
        }
    
        private void Fixupusers(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (e.NewItems != null)
            {
                foreach (user item in e.NewItems)
                {
                    item.customer = this;
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        if (!item.ChangeTracker.ChangeTrackingEnabled)
                        {
                            item.StartTracking();
                        }
                        ChangeTracker.RecordAdditionToCollectionProperties("users", item);
                    }
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (user item in e.OldItems)
                {
                    if (ReferenceEquals(item.customer, this))
                    {
                        item.customer = null;
                    }
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        ChangeTracker.RecordRemovalFromCollectionProperties("users", item);
                    }
                }
            }
        }
    
        private void Fixuptasks(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (e.NewItems != null)
            {
                foreach (task item in e.NewItems)
                {
                    item.customer = this;
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        if (!item.ChangeTracker.ChangeTrackingEnabled)
                        {
                            item.StartTracking();
                        }
                        ChangeTracker.RecordAdditionToCollectionProperties("tasks", item);
                    }
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (task item in e.OldItems)
                {
                    if (ReferenceEquals(item.customer, this))
                    {
                        item.customer = null;
                    }
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        ChangeTracker.RecordRemovalFromCollectionProperties("tasks", item);
                    }
                }
            }
        }
    
        private void FixupcustomersTaskTypes(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (e.NewItems != null)
            {
                foreach (customersTaskType item in e.NewItems)
                {
                    item.customer = this;
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        if (!item.ChangeTracker.ChangeTrackingEnabled)
                        {
                            item.StartTracking();
                        }
                        ChangeTracker.RecordAdditionToCollectionProperties("customersTaskTypes", item);
                    }
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (customersTaskType item in e.OldItems)
                {
                    if (ReferenceEquals(item.customer, this))
                    {
                        item.customer = null;
                    }
                    if (ChangeTracker.ChangeTrackingEnabled)
                    {
                        ChangeTracker.RecordRemovalFromCollectionProperties("customersTaskTypes", item);
                    }
                }
            }
        }

        #endregion

    }
}
