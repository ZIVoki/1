﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using Tasker.Manager.Configuration;
using Tasker.Manager.Managers;
using Tasker.Manager.Model;
using Tasker.Manager.Objects;
using Tasker.Manager.ServiceReference1;
using Tasker.Tools.Extensions;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Manager
{
    public delegate void CurrentUserChangedEventHandler();

    internal class Repository
    {
        public static event CurrentUserChangedEventHandler UserChanged;
        public static Config UserConfiguration;
        private static int _loadCounter = 0;

        #region LoadManagers

        public static readonly TasksLoadManager TasksLoadManager;
        public static readonly CustomersLoadManager CustomersLoadManager;
        public static readonly TaskTypesLoadManager TaskTypesLoadManager;
        public static readonly TimeTableLoadManager TimeTableLoadManager;
        public static readonly UsersLoadManager UsersLoadManager;
        public static readonly ShiftLoadManager ShiftLoadManager;
        public static readonly AttendanceLogLoadManager AttendanceLogLoadLoadManager;
        public static readonly CustomerTaskTypesLoadManager CustomerTaskTypesLoadManager;
        public static readonly ChannelsLoadManager ChannelsLoadManager;
        public static readonly BoardLoadManager BoardLoadManager;
        
        #endregion

        static Repository()
        {
            TasksLoadManager = new TasksLoadManager();
            CustomersLoadManager = new CustomersLoadManager();
            TaskTypesLoadManager = new TaskTypesLoadManager();
            TimeTableLoadManager = new TimeTableLoadManager();
            UsersLoadManager = new UsersLoadManager();
            ShiftLoadManager = new ShiftLoadManager();
            AttendanceLogLoadLoadManager = new AttendanceLogLoadManager();
            CustomerTaskTypesLoadManager = new CustomerTaskTypesLoadManager();
            ChannelsLoadManager = new ChannelsLoadManager();
            BoardLoadManager = new BoardLoadManager();

            AttendanceLog.CollectionChanged += AttendanceLogCollectionChanged;
        }

        static void AttendanceLogCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                IList newItems = e.NewItems;
                AttendanceLogItemView newItem = (AttendanceLogItemView) newItems[0];
                if (newItem == null) return;

                List<AttendanceLogItemView> attendances =
                    AttendanceLog.Where(
                        r =>
                        r.User.Id == newItem.User.Id && r.EventDateTime.Date == newItem.EventDateTime.Date &&
                        r.Status == ConstantsStatuses.UserEnter).OrderBy(r => r.Id).ToList();

                if (attendances.Any())
                {
                    AttendanceLogItemView firstOrDefault = attendances.FirstOrDefault();
                    if (firstOrDefault != null) firstOrDefault.IsFirst = true;

                    foreach (var logItemView in attendances.Skip(1))
                    {
                        logItemView.IsFirst = false;
                    }
                }
            }
        }

        #region Properties

        private static UserView _loginUser = new UserView();

        public static UserView LoginUser
        {
            get { return _loginUser; }
            set
            {
                _loginUser = value;
                InvokeUserChanged();
            }
        }

        private static UniqBindableCollection<TaskView> _tasks = new UniqBindableCollection<TaskView>();

        public static UniqBindableCollection<TaskView> Tasks
        {
            get { return _tasks; }
            set { _tasks = value; }
        }

        private static UniqBindableCollection<AttendanceLogItemView> _attendanceLog =
            new UniqBindableCollection<AttendanceLogItemView>();

        public static UniqBindableCollection<AttendanceLogItemView> AttendanceLog
        {
            get { return _attendanceLog; }
            set { _attendanceLog = value; }
        }

        private static UniqBindableCollection<CustomerView> _customers = new UniqBindableCollection<CustomerView>();

        public static UniqBindableCollection<CustomerView> Customers
        {
            get { return _customers; }
            set { _customers = value; }
        }

        private static UniqBindableCollection<TaskTypeView> _taskTypes = new UniqBindableCollection<TaskTypeView>();

        public static UniqBindableCollection<TaskTypeView> TaskTypes
        {
            get { return _taskTypes; }
            set { _taskTypes = value; }
        }

        private static UniqBindableCollection<UserView> _users = new UniqBindableCollection<UserView>();

        public static UniqBindableCollection<UserView> Users
        {
            get { return _users; }
            set { _users = value; }
        }

        private static UniqBindableCollection<TimeTableView> _timeTables = new UniqBindableCollection<TimeTableView>();

        public static UniqBindableCollection<TimeTableView> TimeTables
        {
            get { return _timeTables; }
            set { _timeTables = value; }
        }

        private static UniqBindableCollection<ShiftView> _shifts = new UniqBindableCollection<ShiftView>();

        public static UniqBindableCollection<ShiftView> Shifts
        {
            get { return _shifts; }
            set { _shifts = value; }
        }

        private static BindableCollection<CustomerTaskTypeDTO> _customerTaskTypes =
            new BindableCollection<CustomerTaskTypeDTO>();

        public static BindableCollection<CustomerTaskTypeDTO> CustomerTaskTypes
        {
            get { return _customerTaskTypes; }
            set { _customerTaskTypes = value; }
        }

        private static UniqBindableCollection<ChannelView> _channels = new UniqBindableCollection<ChannelView>();

        public static UniqBindableCollection<ChannelView> Channels
        {
            get { return _channels; }
            set { _channels = value; }
        }

        private static UniqBindableCollection<ManagerFileView> _files = new UniqBindableCollection<ManagerFileView>();

        public static UniqBindableCollection<ManagerFileView> Files
        {
            get { return _files; }
        }

        private static ObservableCollection<FolderView> _folders = new ObservableCollection<FolderView>();

        public static ObservableCollection<FolderView> Folders
        {
            get { return _folders; }
        }

        private static ObservableCollection<NotificationView> _rssNotifications =
            new ObservableCollection<NotificationView>();

        public static ObservableCollection<NotificationView> RssNotifications
        {
            get { return _rssNotifications; }
        }

        public static LoadSettings LoadSettings
        {
            get
            {
                if (UserConfiguration == null)
                    return null;
                return UserConfiguration.LoadSettings;
            }
        }

        public static HighlightSettings HighlightSettings
        {
            get
            {
                if (UserConfiguration == null) return null;
                return UserConfiguration.HighlightSettings;
            }
        }

        public static MailSettings MailSettings
        {
            get
            {
                if (UserConfiguration == null)
                    return null;
                return UserConfiguration.MailSettings;
            }
        }

        #endregion

        private static void InvokeUserChanged()
        {
            CurrentUserChangedEventHandler handler = UserChanged;
            if (handler != null) handler();
        }

        public static void LoadData()
        {
            if (_loadCounter > 0)
            {
                UpdateData();
                return;
            }
            _loadCounter++;

            Debug.WriteLine("Loading users");
            UsersLoadManager.LoadData();
            Debug.WriteLine("Loading tasktypes");
            TaskTypesLoadManager.LoadData();
            Debug.WriteLine("Loading channels");
            ChannelsLoadManager.LoadData();
            Debug.WriteLine("Loading customers");
            CustomersLoadManager.LoadData();
            Debug.WriteLine("Loading customers tasktypes");
            CustomerTaskTypesLoadManager.LoadData();
            Debug.WriteLine("Loading TASKS");
            TasksLoadManager.LoadData();
            Debug.WriteLine("Loading SHIFTS");
            ShiftLoadManager.LoadData();
            Debug.WriteLine("Loading today timetables");
            TimeTableLoadManager.LoadTodaysTimeTable();
            Debug.WriteLine("Loading self timetables");
            TimeTableLoadManager.LoadSelfTimeTable(ServiceManager.Inst.ServiceChannel);
            Debug.WriteLine("Loading board");
            BoardLoadManager.LoadData();

            UpdateUsersLoad();
        }

        public static void UpdateData()
        {
            Debug.WriteLine("Updating data");
            UsersLoadManager.UpdateData();
            TaskTypesLoadManager.UpdateData();
            ChannelsLoadManager.UpdateData();
            CustomersLoadManager.UpdateData();
            CustomerTaskTypesLoadManager.UpdateData();
            TasksLoadManager.UpdateData();
            ShiftLoadManager.UpdateData();
            Debug.WriteLine("Loading today timetables");
            TimeTableLoadManager.LoadTodaysTimeTable();
            Debug.WriteLine("Loading self timetables");
            TimeTableLoadManager.LoadSelfTimeTable(ServiceManager.Inst.ServiceChannel);
            Debug.WriteLine("Loading board");
            UpdateUsersLoad();
            BoardLoadManager.UpdateData();
        }

        private static void UpdateUsersLoad()
        {
            foreach (var task in _tasks)
            {
                if (task.Status == ConstantsStatuses.InProcess && task.CurrentWorker != null)
                {
                    if (task.PriorityType == PriorityType.Quick)
                        task.CurrentWorker.UserWorkState = Tools.Constants.UserWorkState.Busy;
                    if (task.PriorityType == PriorityType.Background)
                        task.CurrentWorker.UserWorkState = Tools.Constants.UserWorkState.BackgroundTask;
                }
            }
        }
    }
}

