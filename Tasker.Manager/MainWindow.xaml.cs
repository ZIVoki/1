﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shell;
using System.Windows.Threading;
using Microsoft.Win32;
using Tasker.Manager.BackgroundWorkers;
using Tasker.Manager.Base;
using Tasker.Manager.Controls;
using Tasker.Manager.Controls.AttendanceLog;
using Tasker.Manager.Controls.Customers;
using Tasker.Manager.Controls.Response;
using Tasker.Manager.Controls.Task;
using Tasker.Manager.Controls.TaskListHighlight;
using Tasker.Manager.Controls.TaskTypes;
using Tasker.Manager.Controls.TimeTable;
using Tasker.Manager.Controls.TimeTableReports;
using Tasker.Manager.Controls.Users;
using Tasker.Manager.Controls.WeekCalendar;
using Tasker.Manager.Controls.WeekCalendar.Controls;
using Tasker.Manager.Interfaces;
using Tasker.Manager.Managers;
using Tasker.Manager.Managers.Logger;
using Tasker.Manager.Properties;
using Tasker.Tools.Controls.TrayControls;
using Tasker.Tools.CustomEventArgs;
using Tasker.Tools.Helpers;
using Tasker.Tools.Managers;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;
using System.Windows.Forms;
using Application = System.Windows.Application;
using Clipboard = System.Windows.Clipboard;
using MessageBox = System.Windows.MessageBox;
using MouseEventArgs = System.Windows.Input.MouseEventArgs;
using PrintDialog = System.Windows.Controls.PrintDialog;
using SaveFileDialog = System.Windows.Forms.SaveFileDialog;
using Timer = System.Timers.Timer;
using UserControl = System.Windows.Controls.UserControl;

namespace Tasker.Manager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Fluent.RibbonWindow, IDataHandlerInterface
    {
        private BackgroundWorker _updateDataBackgroundWorker;
        private BackgroundWorker _loadArchiveBAckgroundWorker;
        private WorkersStateWindow _workersStateWindow;

        private readonly List<int> _tasksWithUnreadMessages = new List<int>();
        private Timer _taskbarAnimationTimer;
        private uint _countDinnerHours = 0;

        private readonly List<LunchResponseWindow> _lunchResponseWindows = new List<LunchResponseWindow>();
        private readonly List<TaskFinishedWindow> _taskFinishedWindows = new List<TaskFinishedWindow>();
        private readonly List<Window> _childWindows = new List<Window>();
        private System.Windows.Forms.Timer _checkIdleTimer;
        private bool _autoLogOff = false;

        public MainWindow()
        {
            this.Title = string.Format("Tasker manager (ver. {0})", App.CurrentVersion);
            this.Initialized += RibbonWindow_Initialized;
            this.Loaded += MainWindowLoaded;

            ServiceManager.Inst.Callback.LunchRequestEvent += ManagerCallbackLunchRequestEvent;
            ServiceManager.Inst.Callback.TaskFinishedEvent += ManagerCallbackTaskFinishedEvent;
            ServiceManager.Inst.Callback.TimeTableChangedEvent += ManagerCallback_TimeTableChangedEvent;
            ServiceManager.Inst.Callback.TaskReviewCancel += ManagerCallback_TaskReviewCancel;
            ServiceManager.Inst.Callback.LunchRequestCloseEvent += ManagerCallbackLunchRequestCloseEvent;
            ServiceManager.Inst.WaitingForConnection += Inst_WaitingForConnection;
            ServiceManager.Inst.ConectionReestablished += Inst_ConectionReestablished;

            Application.Current.DispatcherUnhandledException += Current_DispatcherUnhandledException;

            if (Settings.Default.ReportsDestFolder == string.Empty)
            {
                Settings.Default.ReportsDestFolder = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                Settings.Default.Save();
            }

            InitializeComponent();

            SetAllCalendarsControls();

            LoadDaysInp.Value = Repository.LoadSettings.LoadAge;

            TaskListControl.NewUnreadMessageComeHandler += ShowUnreadMesseageBaloon;
            TaskListControl.HasUnreadChangedHandler += TaskListControlOnHasUnreadChangedHandler;
            Loaded += MainFormLoaded;
            Repository.TimeTables.CollectionChanged += TimeTables_CollectionChanged;
            Controls.WeekCalendar.Controls.Calendar.WeekLoadedHandler += CalendarWeekLoaded;

            ActiveFilterToggleButton.IsChecked = true;
            ServiceManager.Inst.ServiceChannel.GetSleepRequests();

            CheckRss();

            SystemEvents.SessionSwitch += SystemEvents_SessionSwitch;
        }

        void SystemEvents_SessionSwitch(object sender, SessionSwitchEventArgs e)
        {
            if (e.Reason == SessionSwitchReason.SessionLock || e.Reason == SessionSwitchReason.SessionLogoff)
            {
                _autoLogOff = true;
                this.Close();
            }
        }

        void Inst_ConectionReestablished(object sender)
        {
            //            this.Dispatcher.Invoke((Action)(() => connectionSpan.Visibility = Visibility.Hidden));
        }

        void Inst_WaitingForConnection(object sender)
        {
            //            this.Dispatcher.Invoke((Action)(() => connectionSpan.Visibility = Visibility.Visible));
        }

        private void SetAllCalendarsControls()
        {
            DateTime now = DateTime.Now.Date;
            DateTime firstDayOfMonth = new DateTime(now.Year, now.Month, 1);
            DateTime lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddSeconds(-1);
            TaskFilterStartDatePicker.SelectedDate = firstDayOfMonth;
            TaskFilterEndDatePicker.SelectedDate = lastDayOfMonth;
            UserReportStartDatePicker.SelectedDate = firstDayOfMonth;
            UserReportEndDatePicker.SelectedDate = lastDayOfMonth;
            DelayReportStartDate.SelectedDate = firstDayOfMonth;
            DelayReportEndDate.SelectedDate = lastDayOfMonth;
        }

        private void MainFormLoaded(object sender, RoutedEventArgs e)
        {
            var drawLoadedTimeTables = new Thread(DrawLoadedTimeTables);
            drawLoadedTimeTables.Start();
        }

        public static void DrawLoadedTimeTables()
        {
            foreach (var timeTable in Repository.TimeTables)
            {
                TimeTableAppointmentManager.TimeTableCreateRedrawAppointment(timeTable);
            }
        }

        void MainWindowLoaded(object sender, RoutedEventArgs e)
        {
            InitializeAutoLogoffFeature();
        }

        private void InitializeAutoLogoffFeature()
        {
            if (!IsInitialized) return;

            if (_checkIdleTimer == null)
            {
                _checkIdleTimer = new System.Windows.Forms.Timer();
                this._checkIdleTimer.Interval = (int)TimeSpan.FromSeconds(1).TotalMilliseconds;
                this._checkIdleTimer.Tick += this.CheckIdleTimer_Tick;
            }
            _checkIdleTimer.Start();
        }

        private void CheckIdleTimer_Tick(object sender, EventArgs e)
        {
            uint idleTime = Win32.GetIdleTime();

            // cheicking time to autologoff
            TimeSpan fromMinutes = TimeSpan.FromMinutes(Settings.Default.LogOffTime);
            if (idleTime > fromMinutes.TotalMilliseconds)
            {
                AutoLogOffHelper_MakeAutoLogOffEvent();
            }

            // checking time to check rss
            if (_lastRssTimeUpdate.AddSeconds(Settings.Default.RSSCheckingTimeSec) < DateTime.Now)
            {
                CheckRss();
            }
        }

        #region Rss
        DateTime _lastRssTimeUpdate = DateTime.Now.AddSeconds(Settings.Default.RSSCheckingTimeSec);
        DateTime _lastRssDate = Repository.UserConfiguration.LastRssDate;
        BackgroundWorker _checkRssBackgroundWorker;

        private void CheckRss()
        {
            if (_checkRssBackgroundWorker == null)
            {
                _checkRssBackgroundWorker = new BackgroundWorker();
                _checkRssBackgroundWorker.DoWork += CheckRssAsync;
            }
            if (!_checkRssBackgroundWorker.IsBusy)
                _checkRssBackgroundWorker.RunWorkerAsync();
        }

        private void CheckRssAsync(object sender, DoWorkEventArgs e)
        {
            _lastRssDate = Repository.UserConfiguration.LastRssDate;

            IEnumerable<NotificationView> newRssNotify = RssManager.GetLast10RssNotify(Settings.Default.RssUrl).OrderByDescending(n => n.DateTime);
            if (newRssNotify == null) return;

            if (!Repository.RssNotifications.Any())
            {
                foreach (var rssNotify in newRssNotify.OrderByDescending(t => t.DateTime))
                {
                    NotificationView notify = rssNotify;
                    this.Dispatcher.Invoke((Action)(() => Repository.RssNotifications.Add(notify)));
                }
            }
            else
            {
                NotificationView notificationView = newRssNotify.FirstOrDefault(rss => rss.DateTime > _lastRssDate);
                if (notificationView != null)
                {
                    ChangeTaskbarItemInfoOverlayHandler(Tools.Constants.OverlayState.Rss);
                    CreateAndShowBalloon(notificationView.Content, "Новая запись в блоге",
                                         Tools.Constants.BaloonType.RSS,
                                         () => Process.Start(notificationView.Url));
                    Repository.UserConfiguration.LastRssDate = notificationView.DateTime;
                    if (Repository.RssNotifications.All(n => n.Content != notificationView.Content))
                        Dispatcher.Invoke((Action)(() => Repository.RssNotifications.Add(notificationView)));
                    Log.Write(LogLevel.Info, string.Format("Новая запись в блоге: {0}", notificationView.Content));
                }
            }
            _lastRssTimeUpdate = DateTime.Now;
        }

        #endregion

        void AutoLogOffHelper_MakeAutoLogOffEvent()
        {
            Debug.WriteLine("Входим в Log off, проверка");
            if (Repository.LoginUser != null && Repository.LoginUser.Id != 0)
            {
                Debug.WriteLine("Вызываем Log off");
                _autoLogOff = true;
                AutoLogOff();
            }
        }

        void ManagerCallback_TimeTableChangedEvent(object sender, TimeTableEventArgs e)
        {
            UpdateUserLoadTable();
        }

        void TimeTables_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            UpdateUserLoadTable();
            if (e.NewItems == null) return;
            foreach (var item in e.NewItems)
            {
                TimeTableView timeTableView = (TimeTableView)item;
                Debug.WriteLine(string.Format("{0} - {1}", timeTableView, timeTableView.User));
                TimeTableAppointmentManager.TimeTableCreateRedrawAppointment(timeTableView);
            }
        }

        static void Current_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            Exception exception = e.Exception;
            Log.Write(LogLevel.Fatal, exception.StackTrace);
        }

        #region TaskReviewReqestProcessors

        private delegate void TaskReviewRequestCurrentThread(TaskFinishedEventArgs arg);
        void ManagerCallbackTaskFinishedEvent(object sender, TaskFinishedEventArgs e)
        {
            var taskReview = new TaskReviewRequestCurrentThread(TaskReviewRequestCome);

            this.Dispatcher.Invoke(DispatcherPriority.Normal, taskReview, e);

            //            var showForm = new ArgumentContainsDelegate(ShowWindowInRightThread);
            //            this.Dispatcher.Invoke(DispatcherPriority.Normal, showForm, null, null);
        }

        private void TaskReviewRequestCome(TaskFinishedEventArgs arg)
        {
            if (_taskFinishedWindows.Where(tfw => tfw.CurrentTask.Id == arg.Task.Id).Count() == 0)
            {
                TaskFinishedWindow newFinishedWindow = new TaskFinishedWindow(_taskFinishedWindows);
                newFinishedWindow.DataContext = TaskManager.GetTask(arg.Task, Repository.Tasks, Repository.Customers,
                                                                    Repository.TaskTypes,
                                                                    Repository.Channels,
                                                                    Repository.Users,
                                                                    Repository.HighlightSettings.GetTaskTypeHighlightColor
                                                                        (arg.Task.TaskType.Id),
                                                                    Repository.HighlightSettings.GetCustomerHighlightColor
                                                                        (arg.Task.Customer.Id));
                newFinishedWindow.ShowActivated = true;
                newFinishedWindow.Show();
                ChangeTaskbarItemInfoOverlayHandler(Tools.Constants.OverlayState.Task);
                CreateAndShowBalloon(arg.Task.Title, "Запрос на проверку задачи", Tools.Constants.BaloonType.Task, () => newFinishedWindow.Activate());
                _taskFinishedWindows.Add(newFinishedWindow);
            }
        }

        private void TaskReviewControl_OnResponseSended(object sender, EventArgs eventArgs)
        {
            //            ModalControlVisibleCount--;
            //            taskFinishedControl.Visibility = Visibility.Hidden;
            //            RunSleepEvents();
        }

        private delegate void TaskReviewCancelEventHandler(TaskCancelReviewEventArgs e);
        void ManagerCallback_TaskReviewCancel(object sender, TaskCancelReviewEventArgs e)
        {

            TaskReviewCancelEventHandler taskReviewCancel = CancelTaskReview;
            this.Dispatcher.Invoke(DispatcherPriority.Normal, taskReviewCancel, e);
        }

        private void CancelTaskReview(TaskCancelReviewEventArgs e)
        {
            TaskFinishedWindow taskFinishedWindow = _taskFinishedWindows.FirstOrDefault(w => w.CurrentTask.Id == e.TaskId);
            if (taskFinishedWindow != null)
            {
                taskFinishedWindow.ForceClose();
            }
        }

        void CloseTaskFinishWindows()
        {
            foreach (var taskFinishedWindow in _taskFinishedWindows.ToList())
            {
                taskFinishedWindow.ForceClose();
            }
        }

        #endregion

        #region LunchRequestProcessors

        private delegate void LunchRequestEventHandler(LunchRequestEventArgs arg);
        void ManagerCallbackLunchRequestEvent(object sender, LunchRequestEventArgs e)
        {
            var lunch = new LunchRequestEventHandler(LunchRequestCome);
            this.Dispatcher.Invoke(DispatcherPriority.Normal, lunch, e);

            //            var showForm = new ArgumentContainsDelegate(ShowWindowInRightThread);
            //            this.Dispatcher.Invoke(DispatcherPriority.Normal, showForm, null, null);
        }

        private void LunchRequestCome(LunchRequestEventArgs arg)
        {
            if (_lunchResponseWindows.Where(lrw => lrw.CurrentUserView.Id == arg.UserDTO.Id).Count() == 0)
            {
                LunchResponseWindow lunchResponseWindow = new LunchResponseWindow(_lunchResponseWindows)
                                                              {
                                                                  DataContext =
                                                                      UserManager.GetUser(arg.UserDTO, Repository.Users)
                                                              };
                lunchResponseWindow.ShowActivated = true;
                lunchResponseWindow.Show();

                var user = UserManager.GetUser(arg.UserDTO, Repository.Users);
                ChangeTaskbarItemInfoOverlayHandler(Tools.Constants.OverlayState.Lunch);
                CreateAndShowBalloon(user.Title, "Запрос на обед", Tools.Constants.BaloonType.Lunch, () => lunchResponseWindow.Activate());

                _lunchResponseWindows.Add(lunchResponseWindow);
            }
        }

        private void LunchResponseControl_OnRequestSended(object sender)
        {
            //            ModalControlVisibleCount--;
            //            lunchResponseControl.Visibility = Visibility.Hidden;
            //            RunSleepEvents();
        }

        private delegate void LunchRequestCloseEventHandler(LunchRequestEventCloseArgs arg);

        void ManagerCallbackLunchRequestCloseEvent(object sender, LunchRequestEventCloseArgs e)
        {
            LunchRequestCloseEventHandler lunchRequestCancel = CancelLunchRequest;
            this.Dispatcher.Invoke(DispatcherPriority.Normal, lunchRequestCancel, e);
        }

        private void CancelLunchRequest(LunchRequestEventCloseArgs arg)
        {
            LunchResponseWindow lunchResponseWindow =
                _lunchResponseWindows.FirstOrDefault(w => w.CurrentUserView.Id == arg.UserId);
            if (lunchResponseWindow != null)
            {
                lunchResponseWindow.ForceClose();
            }
        }

        void CloseLunchResponseWindows()
        {
            foreach (var lunchResponseWindow in _lunchResponseWindows.ToList())
            {
                lunchResponseWindow.ForceClose();
            }
        }

        #endregion

        private void LogoffeventHandler(object sender)
        {
            this.Close();
        }

        private void AutoLogOff()
        {
            _autoLogOff = true;
            this.Close();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
#if !DEBUG
            if (!_autoLogOff)
            {
                MessageBoxResult messageBoxResult = MessageBox.Show("Вы уверены, что хотите выйти из приложения",
                                                                    "Выход",
                                                                    MessageBoxButton.YesNo, MessageBoxImage.Asterisk);
                if (messageBoxResult == MessageBoxResult.Yes)
                    e.Cancel = false;
                else
                    e.Cancel = true;
            }
#endif
            if (e.Cancel == false)
                base.OnClosing(e);
        }

        protected override void OnClosed(EventArgs e)
        {
            ServiceManager.Inst.Callback.LunchRequestEvent -= ManagerCallbackLunchRequestEvent;
            ServiceManager.Inst.Callback.TaskFinishedEvent -= ManagerCallbackTaskFinishedEvent;
            ServiceManager.Inst.Callback.TimeTableChangedEvent -= ManagerCallback_TimeTableChangedEvent;
            ServiceManager.Inst.Callback.TaskReviewCancel -= ManagerCallback_TaskReviewCancel;
            ServiceManager.Inst.Callback.LunchRequestCloseEvent -= ManagerCallbackLunchRequestCloseEvent;
            TaskListControl.HasUnreadChangedHandler -= TaskListControlOnHasUnreadChangedHandler;
            Repository.TimeTables.CollectionChanged -= TimeTables_CollectionChanged;
            Controls.WeekCalendar.Controls.Calendar.WeekLoadedHandler -= CalendarWeekLoaded;
            SystemEvents.SessionSwitch -= SystemEvents_SessionSwitch;
            WeekCalendar.Unsign();
            TaskListControl.Unsign();
            SelfScheduleControl.Unsign();
            StatusBar.Unsign();

            CloseChildWindows();
            TaskBarIcon.CloseBalloon();

            Repository.LoginUser = null;
            ServiceManager.Inst.CloseAndLogoff();
            TaskBarIcon.Dispose();
            base.OnClosed(e);
        }

        private void CloseChildWindows()
        {
            CloseTaskFinishWindows();
            CloseLunchResponseWindows();
            if (_workersStateWindow != null)
                _workersStateWindow.Close();
            AttendanceLogWindow.CloseCurrentWindow();

            foreach (var ownedWindow in Application.Current.Windows)
            {
                Window childWindow = ownedWindow as Window;
                if (childWindow != null && this.Title != childWindow.Title) childWindow.Close();
            }

            /*
                        foreach (var childWindow in _childWindows)
                        {
                            if (childWindow != null) childWindow.Close();
                        }
            */
        }

        private void CheckAndOnAdminRoleTabs()
        {
            if (Repository.LoginUser == null) return;
            if (Repository.LoginUser.Role == ConstantsRoles.Administrator)
            {
                UserActionsRibbonGroupBox.IsEnabled = true;
                TimeTableRibbonTabItem.Visibility = Visibility.Visible;
                ReportDestFolderGroupBoxUsers.IsEnabled = true;
                StartReportGeneratorButton.IsEnabled = true;
                UpdateUserLoadTable();
            }
            else
            {
                UserActionsRibbonGroupBox.IsEnabled = false;
                TimeTableRibbonTabItem.Visibility = Visibility.Collapsed;
                ReportDestFolderGroupBoxUsers.IsEnabled = false;
                StartReportGeneratorButton.IsEnabled = false;
                EditSelectedFileButton.IsEnabled = false;
            }
            MainRibbon.SelectedTabItem = TasksRibbonTabItem;
        }

        #region tabs

        private void SelectNewTab<T>(Constants.TabType tabType) where T : UserControl, new()
        {
            foreach (var tab in MainTabControl.Items.Cast<TabItem>().Where(
                tab => (Constants.TabType)tab.Tag == tabType)
                )
            {
                MainTabControl.SelectedItem = tab;
                return;
            }

            var departmentsControl = new T { Margin = new Thickness(0, 0, 0, 0) };

            var newTab = new TabItem
                             {
                                 Margin = new Thickness(0, 0, 0, 0),
                                 Content = departmentsControl,
                                 Tag = tabType
                             };
            MainTabControl.Items.Add(newTab);
            MainTabControl.SelectedItem = newTab;
        }

        private void Ribbon_SelectedTabChanged(object sender, SelectionChangedEventArgs e)
        {
            SetCurrentTabItem();
        }

        private void RibbonWindow_Initialized(object sender, EventArgs e)
        {
            UserHeaderControl.DataContext = Repository.LoginUser;
            CheckAndOnAdminRoleTabs();
            SetCurrentTabItem();
        }

        public void SetCurrentTabItem()
        {
            if (MainRibbon.SelectedTabItem == TasksRibbonTabItem)
            {
                MainTabControl.SelectedItem = TasksTabItem;
                return;
            }
            if (MainRibbon.SelectedTabItem == TimeTableRibbonTabItem)
            {
                MainTabControl.SelectedItem = TimeTableTabItem;
                return;
            }
            if (MainRibbon.SelectedTabItem == CustomersRibbonTabItem)
            {
                MainTabControl.SelectedItem = CustomersTabItem;
                return;
            }
            if (MainRibbon.SelectedTabItem == UsersRibbonTabItem)
            {
                MainTabControl.SelectedItem = UsersListTabItem;
                return;
            }
            if (MainRibbon.SelectedTabItem == TaskTypesRibbonTabItem)
            {
                MainTabControl.SelectedItem = TaskTypeTabItem;
                return;
            }
            if (MainRibbon.SelectedTabItem == BoardRibbonTabItem)
            {
                MainTabControl.SelectedItem = BoardTabItem;
                return;
            }
        }

        #endregion

        #region tasks

        private void CreateNewTaskButton_OnClick(object sender, RoutedEventArgs e)
        {
            NewTaskWindow newTaskWindow = new NewTaskWindow();
            newTaskWindow.Show();
            _childWindows.Add(newTaskWindow);
        }

        #endregion

        #region Baloon

        private void CreateAndShowBalloon(string text, string title, Tools.Constants.BaloonType type, Action onClickAction, object tag = null)
        {
            this.Dispatcher.Invoke((Action)(() => CreateBalloonInRightThread(text, title, type, onClickAction, tag)));
        }

        private void CreateBalloonInRightThread(string text, string title, Tools.Constants.BaloonType baloonType, Action onClickAction, object tag)
        {
            FancyBalloon balloon = new FancyBalloon(baloonType)
                {
                    Tag = tag,
                    OnClickAction = onClickAction,
                    BalloonTitle = title
                };

            if (text.Length > Tools.Constants.MessagePreviewSignCount)
            {
                balloon.BalloonText = string.Format("{0}...", text.Substring(0, Tools.Constants.MessagePreviewSignCount));
            }
            else
            {
                balloon.BalloonText = text;
            }

            //show balloon and close it after 10 seconds
            if (!TaskBarIcon.IsDisposed)
                TaskBarIcon.ShowCustomBalloon(balloon, PopupAnimation.Slide, 10000);
        }

        private void ShowUnreadMesseageBaloon(object sender, ContentsEventArgs contentsEventArgs)
        {
            if (contentsEventArgs.Args[0] is Tools.Constants.BaloonType)
            {
                TaskView taskView = contentsEventArgs.Args[3] as TaskView;
                if (taskView != null)
                {
                    ChangeTaskbarItemInfoOverlayHandler(Tools.Constants.OverlayState.Message);
                    CreateAndShowBalloon((string)contentsEventArgs.Args[2],
                                         (string)contentsEventArgs.Args[1],
                                         (Tools.Constants.BaloonType)contentsEventArgs.Args[0],
                                         () => ChatWindowsManager.ShowChatWindow(taskView, TaskListControl.OpenedChatWindows));
                }
            }
        }

        #endregion

        #region TaskBarAnimation

        private delegate void OverlayDelegate(Tools.Constants.OverlayState state);
        private void ChangeTaskbarItemInfoOverlayHandler(Tools.Constants.OverlayState state)
        {
            var changeOverlayDelegate = new OverlayDelegate(ChangeOverlayStateInRightThread);
            this.Dispatcher.Invoke(DispatcherPriority.Normal, changeOverlayDelegate, state);
        }

        private void ChangeOverlayStateInRightThread(Tools.Constants.OverlayState state)
        {
            switch (state)
            {
                case Tools.Constants.OverlayState.Message:
                    this.TaskbarItemInfo.Overlay = Resources["MessageImage"] as ImageSource;
                    TaskbarItemInfo.ProgressState = TaskbarItemProgressState.Error;
                    TaskbarItemInfo.ProgressValue = 100;
                    _taskbarAnimationTimer = new Timer { Interval = 3000 };
                    _taskbarAnimationTimer.Start();
                    _taskbarAnimationTimer.Elapsed += TaskbarAnimationTimeEllapsed;
                    break;
                case Tools.Constants.OverlayState.NewTask:
                    this.TaskbarItemInfo.Overlay = Resources["TaskImage"] as ImageSource;
                    TaskbarItemInfo.ProgressState = TaskbarItemProgressState.Error;
                    TaskbarItemInfo.ProgressValue = 100;
                    break;
                case Tools.Constants.OverlayState.Task:
                    this.TaskbarItemInfo.Overlay = Resources["TaskImage"] as ImageSource;
                    TaskbarItemInfo.ProgressState = TaskbarItemProgressState.Error;
                    TaskbarItemInfo.ProgressValue = 100;
                    break;
                case Tools.Constants.OverlayState.Rss:
                    this.TaskbarItemInfo.Overlay = Resources["RssImage"] as ImageSource;
                    TaskbarItemInfo.ProgressState = TaskbarItemProgressState.Error;
                    TaskbarItemInfo.ProgressValue = 100;
                    break;
                case Tools.Constants.OverlayState.Lunch:
                    this.TaskbarItemInfo.Overlay = Resources["LunchImage"] as ImageSource;
                    TaskbarItemInfo.ProgressState = TaskbarItemProgressState.Error;
                    TaskbarItemInfo.ProgressValue = 100;
                    break;
                case Tools.Constants.OverlayState.Nothing:
                    this.TaskbarItemInfo.Overlay = null;
                    TaskbarItemInfo.ProgressState = TaskbarItemProgressState.None;
                    TaskbarItemInfo.ProgressValue = 100;
                    if (_taskbarAnimationTimer != null)
                    {
                        _taskbarAnimationTimer.Stop();
                        _taskbarAnimationTimer.Close();
                        _taskbarAnimationTimer.Dispose();
                        _taskbarAnimationTimer = null;
                    }
                    break;
            }
        }

        private delegate void AnimationDelegate();
        private void TaskbarAnimationTimeEllapsed(object sender, EventArgs e)
        {
            var changeOverlayDelegate = new AnimationDelegate(OverlayStateAnimatingInRightThread);
            this.Dispatcher.Invoke(DispatcherPriority.Normal, changeOverlayDelegate);
        }

        private void OverlayStateAnimatingInRightThread()
        {
            if (TaskbarItemInfo.ProgressState == TaskbarItemProgressState.Error)
            {
                TaskbarItemInfo.ProgressState = TaskbarItemProgressState.None;
                if (_taskbarAnimationTimer != null)
                {
                    _taskbarAnimationTimer.Interval = 700;
                }
            }
            else
            {
                if (_taskbarAnimationTimer != null)
                {
                    TaskbarItemInfo.ProgressState = TaskbarItemProgressState.Error;
                    _taskbarAnimationTimer.Interval = 700;
                }
            }
        }

        #endregion

        #region EasyFilters

        private void ActiveFilterToggleButtonChecked(object sender, RoutedEventArgs e)
        {
            if (!IsInitialized)
                return;
            TaskListControl.ActiveTaskFilter = true;
            if (CloseFilterToggleButton.IsChecked.HasValue && CloseFilterToggleButton.IsChecked.Value)
            {
                CloseFilterToggleButton.IsChecked = false;
            }
        }

        private void ActiveFilterToggleButton_OnUnchecked(object sender, RoutedEventArgs e)
        {
            if (IsInitialized)
                TaskListControl.ActiveTaskFilter = false;
        }

        private void CloseFilterToggleButtonChecked(object sender, RoutedEventArgs e)
        {
            if (!IsInitialized)
                return;

            TaskListControl.CloseTaskFilter = true;
            if (ActiveFilterToggleButton.IsChecked.HasValue && ActiveFilterToggleButton.IsChecked.Value)
            {
                ActiveFilterToggleButton.IsChecked = false;
            }
        }

        private void CloseFilterToggleButtonUnchecked(object sender, RoutedEventArgs e)
        {
            if (IsInitialized)
                TaskListControl.CloseTaskFilter = false;
        }

        private void mineFilterToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            if (IsInitialized)
                TaskListControl.MineTaskFilter = true;
        }

        private void mineFilterToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            if (IsInitialized)
                TaskListControl.MineTaskFilter = false;
        }

        #endregion

        #region ClientFilter

        private void clientFilterToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            TaskListControl.FilterCustomer = (CustomerView)ClientsComboBox.SelectedValue;
            ClientsComboBox.IsEnabled = true;
        }

        private void clientFilterToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            TaskListControl.FilterCustomer = null;
            ClientsComboBox.IsEnabled = false;
        }

        private void clientsComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ClientFilterToggleButton.IsChecked.HasValue)
                if (ClientFilterToggleButton.IsChecked.Value)
                {
                    TaskListControl.FilterCustomer = (CustomerView)ClientsComboBox.SelectedValue;
                }
        }

        #endregion

        #region DateTimeFilter

        private void dateTimeFilterToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            TodaysFilterRadioButton.IsEnabled = true;
            YesterdayFilterRadioButton.IsEnabled = true;
            CustomDateTimeFilterRadioButton.IsEnabled = true;

            if (CustomDateTimeFilterRadioButton.IsChecked.HasValue &&
                CustomDateTimeFilterRadioButton.IsChecked.Value)
            {
                customDateTimeFilterRadioButton_Checked(this, e);
                return;
            }

            if (YesterdayFilterRadioButton.IsChecked.HasValue &&
                YesterdayFilterRadioButton.IsChecked.Value)
            {
                TaskListControl.YesterdayTaskFilter = true;
                return;
            }

            TodaysFilterRadioButton.IsChecked = true;
            TaskListControl.TodayTaskFilter = true;
        }

        private void dateTimeFilterToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            TodaysFilterRadioButton.IsEnabled = false;
            YesterdayFilterRadioButton.IsEnabled = false;
            CustomDateTimeFilterRadioButton.IsEnabled = false;

            TaskFilterStartDatePicker.IsEnabled = false;
            TaskFilterEndDatePicker.IsEnabled = false;

            TaskListControl.TodayTaskFilter = false;
            TaskListControl.YesterdayTaskFilter = false;
            TaskListControl.CustomDatesTaskFilter = false;
        }

        private void todaysFilterRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            TaskListControl.TodayTaskFilter = true;
            TaskFilterStartDatePicker.IsEnabled = false;
            TaskFilterEndDatePicker.IsEnabled = false;
        }

        private void yesterdayFilterRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            TaskListControl.YesterdayTaskFilter = true;
            TaskFilterStartDatePicker.IsEnabled = false;
            TaskFilterEndDatePicker.IsEnabled = false;
        }

        private void tbHighlightCustomers_Checked(object sender, RoutedEventArgs e)
        {
            TbHighlightTaskTypes.IsChecked = false;
            TaskListControl.CustomerHighLight = true;
        }

        private void tbHighlightCustomers_Unchecked(object sender, RoutedEventArgs e)
        {
            TaskListControl.CustomerHighLight = false;
        }

        private void tbHighlightTaskTypes_Checked(object sender, RoutedEventArgs e)
        {
            TbHighlightCustomers.IsChecked = false;
            TaskListControl.TaskTypeHighLight = true;
        }

        private void tbHighlightTaskTypes_Unchecked(object sender, RoutedEventArgs e)
        {
            TaskListControl.TaskTypeHighLight = false;
        }

        private void btnHighlightSettings_Click(object sender, RoutedEventArgs e)
        {
            foreach (var customer in Repository.Customers)
            {
                customer.BeginEdit();
            }
            foreach (var taskType in Repository.TaskTypes)
            {
                taskType.BeginEdit();
            }
            HightlightSettingsWindow hightlightSettingsWindow = new HightlightSettingsWindow();
            hightlightSettingsWindow.ShowDialog();
        }

        private void customDateTimeFilterRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            TaskFilterStartDatePicker.IsEnabled = true;
            TaskFilterEndDatePicker.IsEnabled = true;
            if (TaskFilterStartDatePicker.SelectedDate.HasValue && TaskFilterEndDatePicker.SelectedDate.HasValue)
            {
                TaskListControl.CustomFilterStartDate = TaskFilterStartDatePicker.SelectedDate.Value;
                TaskListControl.CustomFilterEndDate = TaskFilterEndDatePicker.SelectedDate.Value;
            }
            TaskListControl.CustomDatesTaskFilter = true;
        }

        private void FilterStartDatePickerLostFocus(object sender, RoutedEventArgs e)
        {
            if (TaskFilterStartDatePicker.SelectedDate.HasValue)
            {
                TaskListControl.CustomFilterStartDate = TaskFilterStartDatePicker.SelectedDate.Value;
                Settings.Default.Save();
            }
        }

        private void FilterEndDatePickerLostFocus(object sender, RoutedEventArgs e)
        {
            if (TaskFilterEndDatePicker.SelectedDate.HasValue)
            {
                TaskListControl.CustomFilterEndDate = TaskFilterEndDatePicker.SelectedDate.Value;
                Settings.Default.Save();
            }
        }

        private void FilterStartDatePickerLostMouseCapture(object sender, MouseEventArgs e)
        {
            FilterStartDatePickerLostFocus(sender, e);
        }

        private void FilterEndDatePickerMouseCapture(object sender, MouseEventArgs e)
        {
            FilterEndDatePickerLostFocus(sender, e);
        }

        #endregion

        #region CreatorFilter

        private void channelsFilterToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            TaskChannelsComboBox.IsEnabled = true;
            TaskListControl.ChannelViewFilter = (ChannelView)TaskChannelsComboBox.SelectedValue;
        }

        private void channelsFilterToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            TaskChannelsComboBox.IsEnabled = false;
            TaskListControl.ChannelViewFilter = null;
        }

        private void taskChannelsComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ChannelsFilterToggleButton.IsChecked.HasValue)
                if (ChannelsFilterToggleButton.IsChecked.Value)
                    TaskListControl.ChannelViewFilter = (ChannelView)TaskChannelsComboBox.SelectedValue;
        }

        #endregion

        private void ManagersCollectionViewSource_Filter(object sender, System.Windows.Data.FilterEventArgs e)
        {
            UserView userView = (UserView)e.Item;
            if (userView == null) return;

            if (userView.Role == ConstantsRoles.Administrator || userView.Role == ConstantsRoles.Manager)
                e.Accepted = true;
            else
                e.Accepted = false;
        }

        public static void ShowServerError(string message)
        {
            MessageBox.Show(message, "Ошибка на стороне сервера.", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void BtnExit_Click(object sender, RoutedEventArgs e)
        {
            CloseChildWindows();
            this.DialogResult = true;
            _autoLogOff = true;
            this.Close();
            TaskBarIcon.Dispose();

            //            Application.Current.Shutdown();
        }

        private void TaskListControlOnHasUnreadChangedHandler(object sender, ContentsEventArgs e)
        {
            var taskId = (int)e.Args[0];
            var hasUnread = (bool)e.Args[1];

            if (hasUnread)
            {
                if (!_tasksWithUnreadMessages.Contains(taskId))
                {
                    _tasksWithUnreadMessages.Add(taskId);
                    ChangeTaskbarItemInfoOverlayHandler(Tools.Constants.OverlayState.Message);
                }
            }
            else
            {
                var exist = _tasksWithUnreadMessages.FirstOrDefault(a => a == taskId);
                _tasksWithUnreadMessages.Remove(exist);
                if (_tasksWithUnreadMessages.Count == 0)
                {
                    ChangeTaskbarItemInfoOverlayHandler(Tools.Constants.OverlayState.Nothing);
                }
            }
        }

        private void AddNewCustomerButton_OnClick(object sender, RoutedEventArgs e)
        {
            EditCustomerWindow editCustomerWindow = new EditCustomerWindow();
            editCustomerWindow.Show();
            _childWindows.Add(editCustomerWindow);
        }

        private void DeleteCustomerButton_Click(object sender, RoutedEventArgs e)
        {
            CustomersListControl.DeleteSelectedItems();
        }

        private void AddNewTaskTypeButton_Click(object sender, RoutedEventArgs e)
        {
            EditTaskTypeWindow editTaskTypeWindow = new EditTaskTypeWindow();
            editTaskTypeWindow.Show();
            _childWindows.Add(editTaskTypeWindow);
        }

        private void DeleteTaskTypeButton_Click(object sender, RoutedEventArgs e)
        {
            TaskTypeListControl.DeleteSelectedItems();
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            ChangeOverlayStateInRightThread(Tools.Constants.OverlayState.Nothing);
            //            if (wasLogOff == true)
            //            {
            //                ServiceManager.Inst.ServiceChannel.GetSleepRequests();
            //                wasLogOff = false;
            //            }
        }

        private void AddNewUserButton_Click(object sender, RoutedEventArgs e)
        {
            EditUserWindow editUserWindow = new EditUserWindow();
            editUserWindow.Show();
            _childWindows.Add(editUserWindow);
        }

        private void deleteUserButton_Click(object sender, RoutedEventArgs e)
        {
            UsersListControl.DeleteSelectedItems();
        }

        private void AddNewTimeTableItemButton_Click(object sender, RoutedEventArgs e)
        {
            AddTimeTableItemWindow addTimeTableItemForm = new AddTimeTableItemWindow();
            addTimeTableItemForm.ShowDialog();
            _childWindows.Add(addTimeTableItemForm);
        }

        private void showPrewiousWeekButton_Click(object sender, RoutedEventArgs e)
        {
            TimeTableActionsGroupBox.IsEnabled = false;
            StatisticInRibbonGallery.SelectedItem = null;
            Controls.WeekCalendar.Controls.Calendar.PreviousDay.Execute(WeekCalendar, WeekCalendar);
            UpdateUserLoadTable();
        }

        private void showNextWeekButton_Click(object sender, RoutedEventArgs e)
        {
            TimeTableActionsGroupBox.IsEnabled = false;
            StatisticInRibbonGallery.SelectedItem = null;
            Controls.WeekCalendar.Controls.Calendar.NextDay.Execute(WeekCalendar, WeekCalendar);
            UpdateUserLoadTable();
        }

        private void CalendarWeekLoaded(object sender, ContentEventArgs e)
        {
            TimeTableActionsGroupBox.IsEnabled = true;
        }

        private delegate void UpdateUsersLoadEventHandler();
        private void UpdateUserLoadTable()
        {
            var update = new UpdateUsersLoadEventHandler(UpdateLoadUserLoadForDipatch);
            this.Dispatcher.Invoke(DispatcherPriority.Normal, update);
        }

        private void UpdateLoadUserLoadForDipatch()
        {
            if (WeekCalendar.CurrentMonday.HasValue)
            {
                DateTime currentMonday = WeekCalendar.CurrentMonday.Value.Date;

                IEnumerable<UserHour> timeTableViews = from timeTable in Repository.TimeTables
                                                       where
                                                           timeTable.StartDate >= currentMonday &&
                                                           timeTable.StartDate <= currentMonday.AddDays(7) &&
                                                           timeTable.Status != ConstantsStatuses.Deleted
                                                       group timeTable by timeTable.User
                                                           into g
                                                           select new UserHour { UserId = g.Key.Id, User = g.Key.Title, Hours = g.Sum(t => t.TotalTime - _countDinnerHours) };


                StatisticInRibbonGallery.ItemsSource = timeTableViews.OrderByDescending(t => t.Hours);

                if (CalendarRepository.UserIdHighlight != 0)
                {
                    var highlightUserExist = timeTableViews.FirstOrDefault(a => a.UserId == CalendarRepository.UserIdHighlight);

                    if (highlightUserExist.User != null)
                    {
                        StatisticInRibbonGallery.SelectedItem = highlightUserExist;
                    }
                }
            }
        }

        private void CWeekCalendarAddAppointment(object sender, RoutedEventArgs e)
        {
            int index = int.Parse((string)((CalendarTimeslotItem)e.OriginalSource).Tag);
            DateTime clickedDate = ((CalendarDay)((CalendarTimeslotItem)e.OriginalSource).TemplatedParent).CurrentDate;
            TimeSpan timeOfDay = TimeSpan.FromMinutes(30 * (index - 1));
            DateTime finalDateTime = clickedDate.Date + timeOfDay;
            AddTimeTableItemWindow addTimeTableItemWindow = new AddTimeTableItemWindow { SelectedDate = finalDateTime };
            addTimeTableItemWindow.ShowDialog();
        }

        private void StatisticInRibbonGallerySelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                var user = (UserHour)e.AddedItems[0];
                CalendarRepository.UserIdHighlight = user.UserId;
            }
        }

        private void BtnClearHighlightClick(object sender, RoutedEventArgs e)
        {
            CalendarRepository.UserIdHighlight = 0;
            StatisticInRibbonGallery.SelectedItem = null;
        }


        #region ExcelReportGenerator

        private ExcelReportGeneartor _excelReportGeneartor;

        private void CallReportGeneartor(object sender, DoWorkEventArgs e)
        {
            ReportButtonAvailable(false);
            int loadId = LoadManager.InvokeLoadStartStatic(new LoadStartEventArgs("Формирование отчета"));
            var args = (object[])e.Argument;
            _excelReportGeneartor.GenerateTaskReport((List<TaskView>)args[0], (string)args[1]);
            //put report path to result
            e.Result = new[] { loadId, args[1] };
        }

        private void ReportButtonAvailable(bool isEnabled)
        {
            Dispatcher.Invoke(new Action(
                                          delegate
                                          {
                                              StartReportWordMounthGeneratorButtonUsers.IsEnabled = isEnabled;
                                              StartReportGeneratorButtonUsers.IsEnabled = isEnabled;
                                              StartReportGeneratorButton.IsEnabled = isEnabled;
                                          }
                                              ));
        }

        private void CallUsersReportGeneartor(object sender, DoWorkEventArgs e)
        {
            ReportButtonAvailable(false);
            int loadId = LoadManager.InvokeLoadStartStatic(new LoadStartEventArgs("Формирование отчета"));
            object[] args = (object[])e.Argument;
            string generateUserReport = _excelReportGeneartor.GenerateUserReport((List<UserView>)args[0], (DateTime)args[2], (DateTime)args[3], (string)args[1]);
            //put report path to result
            e.Result = new object[] { loadId, generateUserReport };
        }

        private void CallUserPeriodWordsReportGeneartor(object sender, DoWorkEventArgs e)
        {
            ReportButtonAvailable(false);
            int loadId = LoadManager.InvokeLoadStartStatic(new LoadStartEventArgs("Формирование отчета"));
            object[] args = (object[])e.Argument;
            string generateUserReport = WordReportGenerator.GenerateUserReport((List<UserView>)args[0], (DateTime)args[2], (DateTime)args[3], (string)args[1]);
            //put report path to result
            e.Result = new object[] { loadId, generateUserReport };
        }

        private void CallReportGenerateEnds(object sender, RunWorkerCompletedEventArgs e)
        {
            ReportButtonAvailable(true);
            if (e.Result != null && e.Result is object[])
            {
                var res = (object[])e.Result;
                if (res[1] != null && res[1] is string && !string.IsNullOrEmpty(res[1].ToString()))
                {
                    try
                    {
                        Process.Start(res[1].ToString());
                    }
                    catch (Exception exception)
                    {
                        Log.Write(LogLevel.Error, exception.Message);
                    }
                }

                if (res[0] != null && res[0] is int)
                {
                    LoadManager.InvokeLoadEndStatic((int)res[0]);
                }
            }
        }

        public int DoWorkCount { get; set; }

        private void StartReportGeneratorButtonUsers_Click(object sender, RoutedEventArgs e)
        {
            if (UsersListControl.UsersListView.SelectedItems.Count == 0) return;
            var tmp = UsersListControl.UsersListView.SelectedItems;
            var selectedUsers = new List<UserView>();
            foreach (var item in tmp)
            {
                var user = (UserView)item;
                if (user.Role == ConstantsRoles.Worker)
                {
                    selectedUsers.Add(user);
                }
            }

            var saveFileDialog = new SaveFileDialog
            {
                Title = @"Сохранить отчет как...",
                Filter = @"Excel files (*.xls)|*.xls",
                FileName = ExcelReportGeneartor.GenearteFileName("users_report"),
                RestoreDirectory = true,
                InitialDirectory = Settings.Default.ReportsDestFolder
            };

            DialogResult result = saveFileDialog.ShowDialog();

            if (result == System.Windows.Forms.DialogResult.OK)
            {
                var path = saveFileDialog.FileName;
                var file = new FileInfo(path);
                if (file.DirectoryName != Settings.Default.ReportsDestFolder)
                {
                    Settings.Default.ReportsDestFolder = file.DirectoryName;
                    Settings.Default.Save();
                }

                if (_excelReportGeneartor == null)
                    _excelReportGeneartor = new ExcelReportGeneartor();

                MultiThreadBackgroundTask backgroundTask = new MultiThreadBackgroundTask(this, CallUsersReportGeneartor, CallReportGenerateEnds);
                backgroundTask.Run(new object[]
                                       {
                                           selectedUsers,
                                           path, 
                                           UserReportStartDatePicker.SelectedDate.Value,
                                           UserReportEndDatePicker.SelectedDate.Value
                                       });
            }
        }

        private void StartReportGeneratorButton_Click(object sender, RoutedEventArgs e)
        {
            if (TaskListControl.TaskListView.SelectedItems.Count == 0) return;
            var selectedTasks = new List<TaskView>();
//            var tmp = TaskListControl.TaskListView.SelectedItems;
            ItemCollection itemCollection = TaskListControl.TaskListView.Items;
            foreach (var item in itemCollection)
            {
                selectedTasks.Add((TaskView)item);
            }

            if (_excelReportGeneartor == null)
                _excelReportGeneartor = new ExcelReportGeneartor();

            var saveFileDialog = new SaveFileDialog
            {
                Title = @"Сохранить отчет как...",
                Filter = @"Excel files (*.xls)|*.xls",
                FileName = ExcelReportGeneartor.GenearteFileName("tasks_report"),
                RestoreDirectory = true,
                InitialDirectory = Settings.Default.ReportsDestFolder
            };

            DialogResult result = saveFileDialog.ShowDialog();

            if (result == System.Windows.Forms.DialogResult.OK)
            {
                var path = saveFileDialog.FileName;
                var file = new FileInfo(path);
                if (file.DirectoryName != Settings.Default.ReportsDestFolder)
                {
                    Settings.Default.ReportsDestFolder = file.DirectoryName;
                    Settings.Default.Save();
                }

                var backgroundTask = new MultiThreadBackgroundTask(this, CallReportGeneartor, CallReportGenerateEnds);
                backgroundTask.Run(new object[] { selectedTasks, path });
            }
        }

        #endregion

        #region wordReportGeneration

        private void startWordMunthReportGeneratorButtonUsers_Click(object sender, RoutedEventArgs e)
        {
            if (UsersListControl.UsersListView.SelectedItems.Count == 0) return;
            var tmp = UsersListControl.UsersListView.SelectedItems;
            var selectedUsers = new List<UserView>();
            foreach (var item in tmp)
            {
                var user = (UserView)item;
                if (user.Role == ConstantsRoles.Worker)
                {
                    selectedUsers.Add(user);
                }
            }

            var saveFileDialog = new SaveFileDialog
            {
                Title = @"Сохранить отчет как...",
                Filter = @"MS Word files (*.doc)|*.doc",
                FileName = ExcelReportGeneartor.GenearteFileName("users_period_report"),
                RestoreDirectory = true,
                InitialDirectory = Settings.Default.ReportsDestFolder
            };

            DialogResult result = saveFileDialog.ShowDialog();

            if (result == System.Windows.Forms.DialogResult.OK)
            {
                var path = saveFileDialog.FileName;
                var file = new FileInfo(path);
                if (file.DirectoryName != Settings.Default.ReportsDestFolder)
                {
                    Settings.Default.ReportsDestFolder = file.DirectoryName;
                    Settings.Default.Save();
                }

                if (_excelReportGeneartor == null)
                    _excelReportGeneartor = new ExcelReportGeneartor();

                var backgroundTask = new MultiThreadBackgroundTask(this, CallUserPeriodWordsReportGeneartor, CallReportGenerateEnds);
                backgroundTask.Run(new object[]
                                       {
                                           selectedUsers, 
                                           path, 
                                           UserReportStartDatePicker.SelectedDate.Value,
                                           UserReportEndDatePicker.SelectedDate.Value
                                       });
            }
        }
        #endregion

        #region SchedulePrinting

        private void PrintTimeTable()
        {
            PrintDialog dialog = new PrintDialog();
            try
            {
                if (dialog.ShowDialog() == true)
                {
                    dialog.PrintVisual(WeekCalendar, "Расписание");
                }
            }
            catch (Exception e)
            {
                Log.Write(LogLevel.Error, e.StackTrace);
            }
        }

        private void printTimeTableItemButton_Click(object sender, RoutedEventArgs e)
        {
            PrintTimeTable();
        }

        #endregion

        private void CopyWeekTimeTablesItemButtonClick(object sender, RoutedEventArgs e)
        {
            PasteWeekTimeTablesItemButton.IsEnabled = WeekCalendar.CopyThisWeek();
        }

        private void PasteWeekTimeTablesItemButtonClick(object sender, RoutedEventArgs e)
        {
            TimeTableAppointmentManager.PasteFromClipboardToThisWeek(WeekCalendar.AppointmentClipboard, WeekCalendar.CurrentMonday, PasteAppointmentComplete);
            PasteWeekTimeTablesItemButton.IsEnabled = false;
            CopyWeekTimeTablesItemButton.IsEnabled = false;
        }

        private void PasteAppointmentComplete(object sender, RunWorkerCompletedEventArgs e)
        {
            CopyWeekTimeTablesItemButton.IsEnabled = true;
            // clear clipboard
            WeekCalendar.AppointmentClipboard = null;
        }

        private void ClearWeekTimeTablesItemButtonClick(object sender, RoutedEventArgs e)
        {
            MessageBoxResult messageBoxResult = MessageBox.Show(
                                                        "Расписание текущей недели будет полностью удалено. Вы уверены, в том что хотите продолжить действие?",
                                                        "Очитска текущего расписания.",
                                                        MessageBoxButton.YesNo, MessageBoxImage.Information);
            if (messageBoxResult == MessageBoxResult.Yes)
                WeekCalendar.ClearCurrentWeek();
        }

        private void IncludeDinnerHoursToggleButtonChecked(object sender, RoutedEventArgs e)
        {
            _countDinnerHours = 1;
            UpdateUserLoadTable();
        }

        private void IncludeDinnerHoursToggleButtonUnchecked(object sender, RoutedEventArgs e)
        {
            _countDinnerHours = 0;
            UpdateUserLoadTable();
        }

        private void LoadStatistcShowButtonClick(object sender, RoutedEventArgs e)
        {
            if (_workersStateWindow == null)
            {
                _workersStateWindow = new WorkersStateWindow();
                _workersStateWindow.Closed += ClosedworkerStateWindow;
                _workersStateWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                _workersStateWindow.Show();
            }
            if (_workersStateWindow.WindowState == WindowState.Minimized)
                _workersStateWindow.WindowState = WindowState.Normal;
            _workersStateWindow.Activate();
        }

        private void ClosedworkerStateWindow(object sender, EventArgs e)
        {
            _workersStateWindow = null;
        }

        private void MainWindowKeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.F5)
            {
                if (LoadManager.IsBusy) return;
                if (_updateDataBackgroundWorker == null)
                {
                    _updateDataBackgroundWorker = new BackgroundWorker();
                    _updateDataBackgroundWorker.DoWork += UpdateDataBackgroundWorkerDoWork;
                }
                _updateDataBackgroundWorker.RunWorkerAsync();
            }

            if (e.Key == Key.F4)
            {
                ServiceManager.Inst.ServiceChannel.GetSleepRequests();
            }

            if (e.Key == Key.A && Keyboard.IsKeyDown(Key.LeftCtrl) && Keyboard.IsKeyDown(Key.LeftAlt))
            {
                TaskListControl.TaskListView.UnselectAll();
            }
        }

        static void UpdateDataBackgroundWorkerDoWork(object sender, DoWorkEventArgs e)
        {
            Repository.LoadData();
        }

        private void LoadArchiveTasksClick(object sender, RoutedEventArgs e)
        {
            if (LoadManager.IsBusy) return;
            if (_loadArchiveBAckgroundWorker == null)
            {
                _loadArchiveBAckgroundWorker = new BackgroundWorker();
                _loadArchiveBAckgroundWorker.DoWork += LoadArchiveBAckgroundWorkerDoWork;
                _loadArchiveBAckgroundWorker.WorkerSupportsCancellation = true;
                _loadArchiveBAckgroundWorker.RunWorkerCompleted += LoadArchiveBAckgroundWorkerRunWorkerCompleted;
            }
            int days;
            try
            {
                days = (int)LoadDaysInp.Value;
            }
            catch
            {
                days = 0;
            }
            _loadArchiveBAckgroundWorker.RunWorkerAsync(days);
            TaskListControl.StopRefresh = true;
        }

        void LoadArchiveBAckgroundWorkerRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            TaskListControl.StopRefresh = false;
        }

        static void LoadArchiveBAckgroundWorkerDoWork(object sender, DoWorkEventArgs e)
        {
            Repository.TasksLoadManager.LoadArchive((int)e.Argument);
        }

        private void BtnShowAttendanceLogClick(object sender, RoutedEventArgs e)
        {
            AttendanceLogWindow.AttendanceShowDayLogCommand.Execute(DateTime.Now.Date, this);
        }

        private void MainTabControlPreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (((Keyboard.Modifiers & ModifierKeys.Control) != 0 && e.Key == Key.Tab) ||
                ((Keyboard.Modifiers & ModifierKeys.Control) != 0 && (Keyboard.Modifiers & ModifierKeys.Shift) != 0 && e.Key == Key.Tab))
            {
                e.Handled = true;
            }
        }

        private void TaskListControl_OnShowViewTaskWindow(object sender, object datacontext)
        {
            ViewTaskWindow viewTaskWindow = new ViewTaskWindow { DataContext = datacontext };
            viewTaskWindow.Show();
        }

        private void ChannelsCollectionViewSource_OnFilter(object sender, FilterEventArgs e)
        {
            ChannelView userView = (ChannelView)e.Item;
            if (userView == null) return;

            if (userView.Status == ConstantsStatuses.Enabled) e.Accepted = true;
            else e.Accepted = false;
        }

        private void WorkerCompetitonShowButton_OnClick(object sender, RoutedEventArgs e)
        {
            string taskerWebSite = Settings.Default.TaskerWebSite;
            Process.Start(taskerWebSite);
        }

        TimeTableReportsWindow _newReportsWindow;
        private void SendMailsButtonClick(object sender, RoutedEventArgs e)
        {
            if (_newReportsWindow == null)
                _newReportsWindow = new TimeTableReportsWindow();
            _newReportsWindow.Closed += NewReportsWindow_Closed;
            DateTime? currentMonday = WeekCalendar.CurrentMonday;
            if (currentMonday.HasValue) _newReportsWindow.Data.StartDate = currentMonday.Value;

            _newReportsWindow.Show();
            _newReportsWindow.Activate();
        }

        void NewReportsWindow_Closed(object sender, EventArgs e)
        {
            _newReportsWindow = new TimeTableReportsWindow();
        }

        private void BtnGenerateDelayReport_Click(object sender, RoutedEventArgs e)
        {
            if (!DelayReportStartDate.SelectedDate.HasValue || !DelayReportEndDate.SelectedDate.HasValue)
            {
                return;
            }

            var saveFileDialog = new SaveFileDialog
            {
                Title = @"Сохранить отчет как...",
                Filter = @"Word document files (*.doc)|*.doc",
                FileName = DelayReportGenerator.GenearteFileName(DelayReportStartDate.SelectedDate.Value, DelayReportEndDate.SelectedDate.Value),
                RestoreDirectory = true,
                InitialDirectory = Settings.Default.ReportsDestFolder
            };

            DialogResult result = saveFileDialog.ShowDialog();

            if (result == System.Windows.Forms.DialogResult.OK)
            {
                var path = saveFileDialog.FileName;
                var file = new FileInfo(path);
                if (file.DirectoryName != Settings.Default.ReportsDestFolder)
                {
                    Settings.Default.ReportsDestFolder = file.DirectoryName;
                    Settings.Default.Save();
                }

                var backgroundTask = new MultiThreadBackgroundTask(this, CallDelayReportGeneartor, CallReportGenerateEnds);
                backgroundTask.Run(new object[] { DelayReportStartDate.SelectedDate.Value, DelayReportEndDate.SelectedDate.Value, path });
            }
        }

        private void CallDelayReportGeneartor(object sender, DoWorkEventArgs e)
        {
            ReportButtonAvailable(false);
            int loadId = LoadManager.InvokeLoadStartStatic(new LoadStartEventArgs("Формирование отчета"));
            object[] args = (object[])e.Argument;
            string generateUserReport = DelayReportGenerator.GenerateDelayReportDoc((DateTime)args[0], (DateTime)args[1], (string)args[2]);
            //put report path to result
            e.Result = new object[] { loadId, generateUserReport };
        }

        private void UpdateAllFilesClick(object sender, RoutedEventArgs e)
        {
            ServiceManager.Inst.ServiceChannel.UpdateAllBoardFiles();
            /*ManagerFileView selectedFileView = boardTreeControl.CurrentBoard.SelectedFileView;
            if (selectedFileView != null)
            {
                FileVersionDTO updateBoardFile = ServiceManager.Inst.ServiceChannel.UpdateBoardFile(selectedFileView.Id);
                if (updateBoardFile == null)
                    Log.Write(LogLevel.Info, "Не удалось обновить файл.");
                else
                {
                    FileManager<ManagerFileView>.UpdateFileView(updateBoardFile, selectedFileView);
                    FileManager<ManagerFileView>.SetUsersReadFile(updateBoardFile, Repository.Files, Repository.Folders, Repository.Users);
                    selectedFileView.UserReadFileCollectionView.Refresh();
                }
            }*/
        }

        private void EditSelectedFileButton_OnClick(object sender, RoutedEventArgs e)
        {
            BoardTreeControl.EditSelectedFile();
        }

        private void ShowDelteUserButton_OnClick(object sender, RoutedEventArgs e)
        {
            UsersListControl.ShowDeletedUsers = (bool)ShowDelteUserButton.IsChecked;
        }

        private void TaskListControl_OnSelectionChangedHandler(object sender, SelectionChangedArgs selectionChangedArgs)
        {
            string message = "";
            if (selectionChangedArgs.SelectedItems > 0)
            {
                string s = "";
                switch (selectionChangedArgs.SelectedItems)
                {
                    case 1:
                        s = "задача";
                        break;
                    case 2:
                    case 3:
                    case 4:
                        s = "задачи";
                        break;
                    default:
                        s = "задач";
                        break;
                }

                message = string.Format("{0} {2} из {1}", selectionChangedArgs.SelectedItems,
                                        selectionChangedArgs.TotalItems, s);
            }
            StatusBar.SetSelectionText(message);
        }

        private void CustomersListControl_OnSelectionChangedHandler(object sender, SelectionChangedArgs args)
        {
            string message = "";
            if (args.SelectedItems > 0)
            {
                string s = "";
                switch (args.SelectedItems)
                {
                    case 1:
                        s = "заказчик";
                        break;
                    case 2:
                    case 3:
                    case 4:
                        s = "заказчика";
                        break;
                    default:
                        s = "заказчиков";
                        break;
                }

                message = string.Format("{0} {2} из {1}", args.SelectedItems,
                                        args.TotalItems, s);
            }
            StatusBar.SetSelectionText(message);
        }

        private void UsersListControl_OnSelectionChangedHandler(object sender, SelectionChangedArgs args)
        {
            string message = "";
            if (args.SelectedItems > 0)
            {
                string s = "";
                switch (args.SelectedItems)
                {
                    case 1:
                        s = "пользователь";
                        break;
                    case 2:
                    case 3:
                    case 4:
                        s = "пользователя";
                        break;
                    default:
                        s = "пользователей";
                        break;
                }

                message = string.Format("{0} {2} из {1}", args.SelectedItems,
                                        args.TotalItems, s);
            }
            StatusBar.SetSelectionText(message);
        }

        private void TaskTypeListControl_OnSelectionChangedHandler(object sender, SelectionChangedArgs args)
        {
            string message = "";
            if (args.SelectedItems > 0)
            {
                string s = "";
                switch (args.SelectedItems)
                {
                    case 1:
                        s = "тип";
                        break;
                    case 2:
                    case 3:
                    case 4:
                        s = "типа";
                        break;
                    default:
                        s = "типов";
                        break;
                }

                message = string.Format("{0} {2} из {1}", args.SelectedItems,
                                        args.TotalItems, s);
            }
            StatusBar.SetSelectionText(message);

        }

        private void CopyUserEmailsButton_OnClick(object sender, RoutedEventArgs e)
        {
            string result = "";

            result = Repository.Users.Where(u => u.Status == ConstantsStatuses.Enabled && !String.IsNullOrEmpty(u.Email))
                                     .Select(t => t.Email)
                                     .Aggregate((prev, next) => prev + "; " + next);
            try
            {
                Clipboard.SetText(result);
            }
            catch (Exception exception)
            {
                Debug.WriteLine(exception);
            }
        }

        private void ProgressStatistcShowButton_OnClick(object sender, RoutedEventArgs e)
        {
            UserStatWindow userStatWindow = new UserStatWindow();
            userStatWindow.Show();
        }
    }

    public delegate void SelectionChangedDelegate(object sender, SelectionChangedArgs args);

    public class SelectionChangedArgs
    {
        public SelectionChangedArgs(int selectedItems, int totalItems)
        {
            SelectedItems = selectedItems;
            TotalItems = totalItems;
        }

        public int SelectedItems { get; set; }
        public int TotalItems { get; set; }
    }

    public class UserHour
    {
        public int UserId { get; set; }
        public string User { get; set; }
        public double Hours { get; set; }
    }
}
