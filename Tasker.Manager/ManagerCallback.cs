﻿    using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using Tasker.Manager.Managers;
using Tasker.Manager.Managers.Logger;
using Tasker.Manager.Model;
using Tasker.Manager.ServiceReference1;
using Tasker.Tools.Managers;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;
using ConstantsNetworkStates = Tasker.Tools.ServiceReference.ConstantsNetworkStates;
using CustomerDTO = Tasker.Tools.ServiceReference.CustomerDTO;
using FaultDetail = Tasker.Tools.ServiceReference.FaultDetail;
using MessageDTO = Tasker.Tools.ServiceReference.MessageDTO;
using ShiftDTO = Tasker.Tools.ServiceReference.ShiftDTO;
using TaskDTO = Tasker.Tools.ServiceReference.TaskDTO;
using TaskTypeDTO = Tasker.Tools.ServiceReference.TaskTypeDTO;
using UserDTO = Tasker.Tools.ServiceReference.UserDTO;
using UserTaskTypeDTO = Tasker.Tools.ServiceReference.UserTaskTypeDTO;
using Tasker.Manager.Objects;
using AttendanceLogItemManager = Tasker.Tools.Managers.AttendanceLogItemManager;

namespace Tasker.Manager
{
    internal delegate void LunchRequestEventHandler(object sender, LunchRequestEventArgs e);
    internal delegate void LunchRequestCloseEventHandler(object sender, LunchRequestEventCloseArgs e);
    internal delegate void WorkerChangeStateEventHandler(object sender, WorkerBusyEventArgs e);
    internal delegate void TaskFinishedEventHandler(object sender, TaskFinishedEventArgs e);
    internal delegate void TaskUpdateEventHandler(object sender, TaskUpdatedEventArgs e);
    internal delegate void TaskCancelReviewEventHandler(object sender, TaskCancelReviewEventArgs e);
    internal delegate void ShiftAddedEventHandler(object sender, ShiftAddedEventArgs e);
    internal delegate void TimeTableAddedEventHandler(object sender, TimeTableEventArgs e);
    internal delegate void TimeTableUpdatedEventHandler(object sender, TimeTableEventArgs e);

    #region EventArgs

    internal class TaskCancelReviewEventArgs
    {
        public TaskCancelReviewEventArgs(int taskId)
        {
            TaskId = taskId;
        }

        public int TaskId { get; set; }
    }

    internal class TimeTableEventArgs
    {
        public TimeTableEventArgs(TimeTableDTO timeTableDto)
        {
            this.TimeTableDto = timeTableDto;
        }

        public TimeTableDTO TimeTableDto { get; set; }
    }

    internal class ShiftAddedEventArgs: EventArgs
    {
        public ShiftAddedEventArgs(ShiftDTO shiftDto)
        {
            this.ShiftDTO = shiftDto;
        }

        public ShiftDTO ShiftDTO { get; set; }
    }

    internal class LunchRequestEventArgs: EventArgs
    {
        public LunchRequestEventArgs(UserDTO userDTO)
        {
            UserDTO = userDTO;
        }

        public UserDTO UserDTO { get; set; }
    }


    internal class LunchRequestEventCloseArgs
    {
        public LunchRequestEventCloseArgs(int userId)
        {
            UserId = userId;
        }

        public int UserId { get; set; }
    }

    internal class WorkerBusyEventArgs : EventArgs
    {
        public WorkerBusyEventArgs(UserView userView)
        {
            WorkerUserView = userView;
        }

        public UserView WorkerUserView { get; set; }
    }

    internal class TaskFinishedEventArgs : EventArgs
    {
        public TaskFinishedEventArgs(TaskDTO task, DateTime sendTime)
        {
            this.Task = task;
            this.SendTime = sendTime;
        }

        public TaskDTO Task { get; set; }
        public DateTime SendTime { get; set;}
    }

    internal class TaskUpdatedEventArgs
    {
        public TaskUpdatedEventArgs(TaskDTO taskDto)
        {
            TaskDto = taskDto;
        }

        public TaskDTO TaskDto { get; set; }
    }

    #endregion

    [CallbackBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, AutomaticSessionShutdown = true)]
    internal class ManagerCallback : IManagerInterfaceCallback, IDisposable
    {
        #region events

        public event LunchRequestEventHandler LunchRequestEvent;
        public event LunchRequestCloseEventHandler LunchRequestCloseEvent;
        public event TaskFinishedEventHandler TaskFinishedEvent;
        public event TaskUpdateEventHandler TaskUpdatedEvent;
        public event TaskCancelReviewEventHandler TaskReviewCancel;
        public event WorkerChangeStateEventHandler WorkerChangedStateEvent;
        public event ShiftAddedEventHandler ShiftAddedEvent;
        public event TimeTableAddedEventHandler TimeTableAddedEvent;
        public event TimeTableUpdatedEventHandler TimeTableChangedEvent;

        public void InvokeTaskReviewCancel(TaskCancelReviewEventArgs e)
        {
            TaskCancelReviewEventHandler handler = TaskReviewCancel;
            if (handler != null) handler(null, e);
        }

        public void InvokeTimeTableChangedEvent(TimeTableEventArgs e)
        {
            TimeTableUpdatedEventHandler handler = TimeTableChangedEvent;
            if (handler != null) handler(null, e);
        }

        public void InvokeTimeTableAddedEvent(TimeTableEventArgs e)
        {
            TimeTableAddedEventHandler handler = TimeTableAddedEvent;
            if (handler != null) handler(null, e);
        }

        public void InvokeShiftAddedEvent(ShiftAddedEventArgs e)
        {
            ShiftAddedEventHandler handler = ShiftAddedEvent;
            if (handler != null) handler(null, e);
        }

        public void InvokeTaskUpdatedEvent(TaskUpdatedEventArgs e)
        {   
            TaskUpdateEventHandler handler = TaskUpdatedEvent;
            if (handler != null) handler(null, e);
        }

        public void InvokeWorkerChangeStateEvent(WorkerBusyEventArgs e)
        {
            var handler = WorkerChangedStateEvent;
            if (handler != null) handler(null, e);
        }

        private void InvokeTaskFinishedEventHadler(TaskFinishedEventArgs e)
        {
            TaskFinishedEventHandler handler = TaskFinishedEvent;
            if (handler != null) handler(null, e);
        }

        private void InvokeLunchRequestEventHandler(LunchRequestEventArgs e)
        {
            LunchRequestEventHandler handler = LunchRequestEvent;
            if (handler != null) handler(null, e);
        }

        public void InvokeLunchRequestCloseEvent(LunchRequestEventCloseArgs e)
        {
            LunchRequestCloseEventHandler handler = LunchRequestCloseEvent;
            if (handler != null) handler(null, e);
        }

        #endregion

        /// <summary>
        /// message arrive handler
        /// </summary>
        /// <param name="msg"></param>
        public void ReceiveMessage(MessageDTO msg)
        {
            TaskView taskView = TaskManager.GetTask(msg.Task, Repository.Tasks, Repository.Customers,
                                                    Repository.TaskTypes, Repository.Channels,
                                                    Repository.Users,
                                                    Repository.HighlightSettings.GetTaskTypeHighlightColor(
                                                        msg.Task.TaskType.Id),
                                                    Repository.HighlightSettings.GetCustomerHighlightColor(
                                                        msg.Task.Customer.Id));

            MessageManager.GetMessage(msg, taskView, taskView.Messages.Messages, Repository.Users, Repository.LoginUser.Id);
        }

        /// <summary>
        /// throw lunch request
        /// </summary>
        /// <param name="userDTO"></param>
        public void LunchRequest(UserDTO userDTO)
        {
            InvokeLunchRequestEventHandler(new LunchRequestEventArgs(userDTO));

            UserView userView = UserManager.GetUser(userDTO, Repository.Users);

            string message;
            if (userView != null)
                message = string.Format("{0} попросился на обед.", userView.Title);
            else
                message = string.Format("Неизвестный пользователь с ID {0} попросился на обед.", userDTO.Id);

            Log.Write(LogLevel.Info, message);
        }

        public void LunchRequestCancel(int userId)
        {
            InvokeLunchRequestCloseEvent(new LunchRequestEventCloseArgs(userId));
        }

        /// <summary>
        /// Server error handler
        /// </summary>
        /// <param name="createFaultDetail"></param>
        public void FaultExeptionServerAlarm(FaultDetail createFaultDetail)
        {
            string message = string.Format("Произошла ошибка на стороне сервера :'{0}'", createFaultDetail.Message);
            Log.Write(LogLevel.Fatal, message);
            MainWindow.ShowServerError(message);
        }

        #region usersState

        public void UserEnter(int userId)
        {
            UserView userView = Repository.Users.FirstOrDefault(u => u.Id == userId);
            if (userView != null)
                Log.Write(LogLevel.Info, string.Format("{0} в сети.", userView.Title));
            else
            {
                Log.Write(LogLevel.Info, string.Format("Неизвестный пользователь с ID {0} в сети.", userId));
                return;
            }

            userView.CurrentState = ConstantsNetworkStates.Online;
            InvokeWorkerChangeStateEvent(new WorkerBusyEventArgs(userView));
        }

        public void UserLogoff(int userId)
        {
            UserView userView = Repository.Users.FirstOrDefault(u => u.Id == userId);
            if (userView != null)
            {
                userView.CurrentState = ConstantsNetworkStates.Offline;
                InvokeWorkerChangeStateEvent(new WorkerBusyEventArgs(userView));
                Log.Write(LogLevel.Info, string.Format("{0} покнинул  сеть.", userView.Title));
                
            }
            else
                Log.Write(LogLevel.Info, string.Format("Неизвестный пользователь с ID {0} покинул сеть.", userId));
        }

        public void UserStateChanged(int userId, ConstantsNetworkStates constantsNetworkStates)
        {
            UserView userView = Repository.Users.FirstOrDefault(u => u.Id == userId);

            string message;
            if (userView != null)
            {
                userView.CurrentState = constantsNetworkStates;
                InvokeWorkerChangeStateEvent(new WorkerBusyEventArgs(userView));
                message = string.Format("{0} поменял статус на {1}", userView.Title, constantsNetworkStates);
            }
            else
                message = string.Format("Неизвестный пользователь с ID {0} поменял статус на {1}.", userId, constantsNetworkStates);

            Log.Write(LogLevel.Info, message);
        }

        public void WorkerComeback(int userId)
        {
            UserView userView = Repository.Users.FirstOrDefault(u => u.Id == userId);
            if (userView != null)
            {
                userView.CurrentState = ConstantsNetworkStates.Online;
                InvokeWorkerChangeStateEvent(new WorkerBusyEventArgs(userView));
            }
            string message;
            if (userView != null)
                message = string.Format("{0} вернулся на рабочее место.", userView.Title);
            else
                message = string.Format("Неизвестный пользователь с ID {0} вернулся на рабочее место.", userId);

            Log.Write(LogLevel.Info, message);
        }

        #endregion
        
        /// <summary>
        /// file added handler
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="attachFile"></param>
        public void FileAdded(int taskId, AttachFileDTO attachFile)
        {
            string message;

            if (attachFile.Task == null || attachFile.Task.Id == 0)
            {
                TaskView view = Repository.Tasks.FirstOrDefault(t => t.Id == taskId);
                if (view != null) 
                    attachFile.Task = view.BuildDTO();
                else
                {
                    message = string.Format("Не найдена задача с ID {0}", taskId);
                    Log.Write(LogLevel.Error, message);
                    return;
                }
            }

            TaskView taskView = TaskManager.GetTask(attachFile.Task, 
                                                    Repository.Tasks, 
                                                    Repository.Customers, 
                                                    Repository.TaskTypes,
                                                    Repository.Channels,
                                                    Repository.Users, 
                                                    Repository.HighlightSettings.GetTaskTypeHighlightColor(attachFile.Task.TaskType.Id), 
                                                    Repository.HighlightSettings.GetCustomerHighlightColor(attachFile.Task.Customer.Id));
            TaskFileManager.GetFileData(attachFile, taskView);

            message = string.Format("К задаче {0} прикреплен файл {1}", attachFile.Task.Title, attachFile.Title);
            Log.Write(LogLevel.Info, message);
        }

        #region taskTypes

        public void TaskTypeAdded(TaskTypeDTO taskType)
        {
            if (taskType.Id == 0)
                Log.Write(LogLevel.Error, "Id нового типа задач не определен.");
            else
            {
                var taskTypeView = TaskTypesLoadManager.GetTaskTypeWithHightlight(taskType, Repository.TaskTypes);
                //TaskTypeView taskTypeView = TaskTypeManager.GetTaskType(taskType, Repository.TaskTypes);
                string message = string.Format("Добавлен новый тип задач {0}", taskTypeView.Title);
                Log.Write(LogLevel.Info, message);
            }
        }

        public void TaskTypeUpdated(TaskTypeDTO taskType)
        {
            if (taskType.Id == 0)
                Log.Write(LogLevel.Error, "Id нового типа задач не определен.");
            else
            {
                var taskTypeView = TaskTypesLoadManager.GetTaskTypeWithHightlight(taskType, Repository.TaskTypes);
                //TaskTypeView taskTypeView = TaskTypeManager.GetTaskType(taskType, Repository.TaskTypes);
                TaskTypeManager.UpdateTaskType(taskTypeView, taskType);
                string message = string.Format("Добавлен новый тип задач {0}", taskTypeView.Title);
                Log.Write(LogLevel.Info, message);
            }
        }

        #endregion

        #region userTaskTypes

        public void UserTaskTypeAdded(UserTaskTypeDTO userTaskType)
        {
            if (userTaskType.Id == 0)
                Log.Write(LogLevel.Error, "Id нового типа задач пользователя не определен.");
            else
            {
                var userTaskTypeView = UserTaskTypeManager.GetUserTaskType(userTaskType, Repository.Users,Repository.TaskTypes);
                string message = string.Format("Добавлен новый тип задач пользователя {0}, {1}", userTaskTypeView.TaskType.Title, userTaskTypeView.User.Title);
                Log.Write(LogLevel.Info, message);
            }
        }

        public void UserTaskTypeUpdated(UserTaskTypeDTO userTaskType)
        {

            if (userTaskType.Id == 0)
                Log.Write(LogLevel.Error, "Id типа задач пользователя не определен.");
            else
            {
                var userTaskTypeView = UserTaskTypeManager.GetUserTaskType(userTaskType, Repository.Users,Repository.TaskTypes);
                UserTaskTypeManager.UserTaskTypeUpdate(userTaskTypeView, userTaskType, Repository.TaskTypes);
                string message = string.Format("Обновлен тип задач пользователя {0}, {1}", userTaskTypeView.User.Title, userTaskTypeView.TaskType.Title);
                Log.Write(LogLevel.Info, message);
            }
        }

        #endregion

        #region customers

        public void CustomerAdded(CustomerDTO customer)
        {
            if (customer.Id == 0)
                Log.Write(LogLevel.Error, "Отсутсвует идентификатор нового Клиента.");
            else
            {
                CustomerView customerView = CustomersLoadManager.GetCustomerWithHightlight(customer,
                                                                                           Repository.Customers);
                //CustomerView customerView = CustomerManager.GetCustomer(customer, Repository.Customers);
                string message = string.Format("Добавлен новый клиент {0}", customerView.Title);
                Log.Write(LogLevel.Info, message);
            }
        }

        public void 
            CustomerUpdated(CustomerDTO customer)
        {
            if (customer.Id == 0)
                Log.Write(LogLevel.Error, "Отсутсвует идентификатор измененного Клиента.");
            else
            {
                CustomerManager.CustomerUpdate(Repository.Customers, Repository.Channels, customer);
                string message = string.Format("Клиент {0} изменен", customer.Title);
                Log.Write(LogLevel.Info, message);
            }
        }

        public void CustomerTaskTypeAdded(CustomerTaskTypeDTO customerTaskTypeDto)
        {
            if (customerTaskTypeDto == null) return;

            BackgroundWorker backgroundWorker = new BackgroundWorker();
            backgroundWorker.DoWork += backgroundWorker_DoWork;
            backgroundWorker.RunWorkerAsync(customerTaskTypeDto);
        }

        void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            CustomerTaskTypeDTO customerTaskTypeDto = (CustomerTaskTypeDTO) e.Argument;
            CustomerTaskTypeManager.SetNewCusomerTaskType(customerTaskTypeDto);
        }

        public void CustomerTaskTypeUpdated(CustomerTaskTypeDTO customerTaskTypeDto)
        {
            if (customerTaskTypeDto == null) return;

            BackgroundWorker backgroundWorker =new BackgroundWorker();
            backgroundWorker.DoWork += updateCustomerTaskType_DoWork;
            backgroundWorker.RunWorkerAsync(customerTaskTypeDto);
        }

        public void UserReadFile(UserReadFileDTO userReadFileDTO)
        {
            ManagerFileView fileView = Repository.Files.FirstOrDefault(f => f.Id == userReadFileDTO.FileId);
            if (fileView != null)
            {
                UserReadView userReadView = fileView.UsersReadFile.FirstOrDefault(u => u.User != null && u.User.Id == userReadFileDTO.UserId);
                if (userReadView != null)
                {
                    userReadView.ReadDate = userReadFileDTO.ReadDate;

                    string message = string.Format("Пользователь {0} прочитал {1}.", userReadView.User.LastName, userReadView.File.Title);
                    Log.Write(LogLevel.Info, message);
                }
                else
                {
                    FileManager<ManagerFileView>.SetUserReadeFile(Repository.Users, userReadFileDTO, fileView);
                }
            }
        }

        public void BoardFileNewVersionAdded(FileVersionDTO newFileVersionDTO)
        {
            FileManager<ManagerFileView>.UpdateFileView(newFileVersionDTO, Repository.Files, Repository.Folders);
            FileManager<ManagerFileView>.SetUsersReadFile(newFileVersionDTO, Repository.Files, Repository.Folders, Repository.Users);
            string message = String.Format("New file version {0} arrived id: {1}", newFileVersionDTO.File.Title, newFileVersionDTO.Id);
            Log.Write(LogLevel.Info, message);
        }

        private void updateCustomerTaskType_DoWork(object sender, DoWorkEventArgs e)
        {
            CustomerTaskTypeDTO customerTaskTypeDto = (CustomerTaskTypeDTO) e.Argument;
            CustomerTaskTypeManager.UpdateCustomerTaskType(customerTaskTypeDto);
        }

        #endregion

        #region timeTable

        public void TimeTableAdded(TimeTableDTO timeTable)
        {
            InvokeTimeTableAddedEvent(new TimeTableEventArgs(timeTable));

            TimeTableManager.GetTimeTable(timeTable, Repository.TimeTables,Repository.Users, Repository.Shifts);
            string message = string.Format("Пользователю {0} добавлена новая запись в рассписании.", timeTable.User.LastName);
            Log.Write(LogLevel.Info, message);
        }

        public void TimeTableUpdated(TimeTableDTO timeTable)
        {
            TimeTableView timeTableView = Repository.TimeTables.FirstOrDefault(t => t.Id == timeTable.Id);
            if (timeTableView == null)
            {
                TimeTableAdded(timeTable);
                return;
            }

            TimeTableManager.UpdateTimeTableView(timeTable, Repository.TimeTables, Repository.Users, Repository.Shifts);
            if(timeTable.Status != ConstantsStatuses.Deleted)
            {
                TimeTableAppointmentManager.ApplyTimeTableEditRedrawAppointment(timeTable.IsResp,timeTable.StartDate, timeTable.EndDate,
                                                                            TimeTableAppointmentManager.
                                                                                GetAppointmentsByTimeTableId(
                                                                                    timeTable.Id));
            }
            else
            {
                TimeTableAppointmentManager.DeleteTimeTableRedrawAppointment(timeTable.Id, false);
            }
            InvokeTimeTableChangedEvent(new TimeTableEventArgs(timeTable));
            string message = string.Format("Изменено расписание пользователя {0}.", timeTable.User.LastName);
            Log.Write(LogLevel.Info, message);
        }

        public void AttendanceLogItemAdded(AttendanceLogItemDTO attendanceLogItemDTO)
        {
            // if was loaded some time
            if (Repository.AttendanceLogLoadLoadManager.LastLoadDateTime.HasValue)
                AttendanceLogItemManager.GetAttendance(attendanceLogItemDTO, Repository.AttendanceLog, Repository.Users,
                                                       Repository.TimeTables, Repository.Shifts);
        }

        #endregion

        #region task

        public void TaskAdded(TaskDTO task)
        {
            Color? customerHighlightColor = Repository.HighlightSettings.GetCustomerHighlightColor(task.Customer.Id);
            Color? taskTypeHighlightColor = Repository.HighlightSettings.GetTaskTypeHighlightColor(task.TaskType.Id);
            TaskManager.CreateTask(task, Repository.Customers,Repository.TaskTypes, 
                                   Repository.Users, Repository.Channels, null, Repository.Tasks, 
                                   taskTypeHighlightColor, 
                                   customerHighlightColor);

            if (task.Id == 0)
                Log.Write(LogLevel.Error, "Пустой идентификатор новой задчи");
            else
            {
                string message = string.Format("Добавлена новая задача: {0}", task.Title);
                Log.Write(LogLevel.Info, message);
            }

            InvokeTaskUpdatedEvent(new TaskUpdatedEventArgs(task));
        }

        public void TaskUpdated(TaskDTO task)
        {
            TaskView taskView = Repository.Tasks.FirstOrDefault(t => t.Id == task.Id);
            Color? customerHighlightColor = Repository.HighlightSettings.GetCustomerHighlightColor(task.Customer.Id);
            Color? taskTypeHighlightColor = Repository.HighlightSettings.GetTaskTypeHighlightColor(task.TaskType.Id);
            TaskManager.UpdateTask(taskView, task, Repository.Customers, Repository.TaskTypes, Repository.Users, Repository.Channels, taskTypeHighlightColor, customerHighlightColor);

            if (taskView != null)
                if (taskView.Status != task.Status && task.Status == ConstantsStatuses.Closed)
                    Log.Write(LogLevel.Info, string.Format("Задача {0} закрыта.", taskView.Title));

            InvokeTaskUpdatedEvent(new TaskUpdatedEventArgs(task));
        }

        public void TaskCheckedIn(TaskDTO taskDTO, string name)
        {
            Color? taskTypeHighlightColor = Repository.HighlightSettings.GetTaskTypeHighlightColor(taskDTO.TaskType.Id);
            Color? customerHighlightColor = Repository.HighlightSettings.GetCustomerHighlightColor(taskDTO.Customer.Id);
            var taskView = TaskManager.GetTask(taskDTO, 
                                                Repository.Tasks,
                                                Repository.Customers, Repository.TaskTypes, Repository.Channels, 
                                                Repository.Users,
                                                taskTypeHighlightColor, customerHighlightColor);
            string message;

            if (taskView != null)
            {
                taskView.WorkState = Tools.Constants.TaskWorkState.NotAvailable;
                message = string.Format(" Задача {0} извлечена.", taskView.Title);
            }
            else
                message = string.Format("Неизвестная задча с ID {0} извлечена.", taskDTO.Id);

            Log.Write(LogLevel.Info, message);
        }

        public void TaskCheckedOut(TaskDTO taskDTO, string name)
        {
            var taskView = TaskManager.GetTask(taskDTO, Repository.Tasks, 
                                                Repository.Customers, Repository.TaskTypes, 
                                                Repository.Channels, Repository.Users, 
                                                Repository.HighlightSettings.GetTaskTypeHighlightColor(taskDTO.TaskType.Id), 
                                                Repository.HighlightSettings.GetCustomerHighlightColor(taskDTO.Customer.Id));
            string message;

            if (taskView != null)
            {
                taskView.WorkState = Tools.Constants.TaskWorkState.Available;
                message = string.Format(" Задача {0} возвращена.", taskView.Title);
            }
            else
                message = string.Format("Неизвестная задча с ID {0} возвращена.", taskDTO.Id);

            Log.Write(LogLevel.Info, message);
        }

        /// <summary>
        /// On task finished by client
        /// </summary>
        /// <param name="taskDTO"></param>
        /// <param name="sendTime"></param>
        public void TaskForReviewRecieved(TaskDTO taskDTO, DateTime sendTime)
        {
            InvokeTaskFinishedEventHadler(new TaskFinishedEventArgs(taskDTO, sendTime));

            var taskView = TaskManager.GetTask(taskDTO, Repository.Tasks, 
                Repository.Customers, Repository.TaskTypes, 
                Repository.Channels, Repository.Users, 
                Repository.HighlightSettings.GetTaskTypeHighlightColor(taskDTO.TaskType.Id), 
                Repository.HighlightSettings.GetCustomerHighlightColor(taskDTO.Customer.Id));
            string message;

            if (taskView != null)
                message = string.Format("Задача {0} отправлена на проверку.", taskView.Title);
            else
                message = string.Format("Неизвестная задча с ID {0} отправлена на проверку.", taskDTO.Id);

            Log.Write(LogLevel.Info, message);
        }

        public void TaskForReviewCancel(TaskDTO taskDto)
        {
            InvokeTaskReviewCancel(new TaskCancelReviewEventArgs(taskDto.Id));
        }

        public void TaskAccepted(int taskId, int userrId, DateTime acceptTime)
        {
            lock (Repository.Users)
            {
                UserView userView = Repository.Users.FirstOrDefault(u => u.Id == userrId);
                lock (Repository.Tasks)
                {
                    TaskView taskView = Repository.Tasks.FirstOrDefault(t => t.Id == taskId);

                    string userName = userView != null ? userView.Title : "None";

                    string message;
                    if (taskView != null)
                    {
                        message = string.Format("Задача {0} принята пользователем {1}.", taskView.Title, userName);
                        taskView.CurrentWorker = userView;
                        if (taskView.Status != ConstantsStatuses.InProcess)
                            taskView.Status = ConstantsStatuses.InProcess;

                        IEnumerable<TaskView> taskViews = Repository.Tasks.Where(
                            t => t.CurrentWorker == userView && t.Status == ConstantsStatuses.InProcess && t.Id != taskId);
                        foreach (var view in taskViews)
                        {
                            if (view == null) continue;
                            view.Status = ConstantsStatuses.Disabled;
                        }
                    }
                    else
                        message = string.Format("Неизвестная задча с ID {0} принята пользователем {1}.", taskId, userName);

                    Log.Write(LogLevel.Info, message);
                    
                }
            }
        }

        #endregion

        #region shifts

        public void ShiftAdded(ShiftDTO shiftId)
        {
            InvokeShiftAddedEvent(new ShiftAddedEventArgs(shiftId));
            string message = string.Format("Добавлен новый тип рабочей смены {0}", shiftId.Title);
            Log.Write(LogLevel.Debug, message);
        }

        public void ShiftUpdated(ShiftDTO shift)
        {
            ShiftView shiftView = Repository.Shifts.FirstOrDefault(s => s.Id == shift.Id);
            if (shiftView == null)
            {
                ShiftAdded(shift);
                return;
            }

            ShiftManager.ShiftUpdate(shift, Repository.Shifts);
            string message = String.Format("Изменен тип рабочей смены {0}", shift.Title);
            Log.Write(LogLevel.Debug, message);
        }

        #endregion

        #region users

        public void UserAdded(UserDTO user)
        {
            if (user.Id == 0)
                Log.Write(LogLevel.Error, "Id нового пользователя не определен.");
            else
            {
                UserView userView = UserManager.GetUser(user, Repository.Users);
                string message = string.Format("Добавлен пользователь {0}", userView.Title);
                Log.Write(LogLevel.Info, message);
            }
        }

        public void UserUpdated(UserDTO user)
        {
            if (user.Id == 0)
                Log.Write(LogLevel.Error, "Id измененного пользователя не определен.");
            else
            {
                UserView userView = UserManager.GetUser(user, Repository.Users);
                UserManager.UserUpdate(userView, user);
                string message = string.Format("Изменен пользователь {0}", userView.Title);
                Log.Write(LogLevel.Info, message);
            }
        }

        #endregion

        #region workerStates

        public void WorkerFree(UserDTO worker)
        {
            if (worker == null) return;
            var user = UserManager.GetUser(worker, Repository.Users);
            user.UserWorkState = Tools.Constants.UserWorkState.Free;
            InvokeWorkerChangeStateEvent(new WorkerBusyEventArgs(user));
        }

        public void WorkerBusy(UserDTO worker, TaskDTO task)
        {
            var user = UserManager.GetUser(worker, Repository.Users);
            if (task.Priority == 1)
                user.UserWorkState = Tools.Constants.UserWorkState.Busy;
            if (task.Priority == 0)
                user.UserWorkState = Tools.Constants.UserWorkState.BackgroundTask;
            InvokeWorkerChangeStateEvent(new WorkerBusyEventArgs(user));
        }

        public void WorkerBlocked(UserDTO worker)
        {
            var user = UserManager.GetUser(worker, Repository.Users);
            user.UserWorkState = Tools.Constants.UserWorkState.NotAvailable;
        }

        #endregion

        public void Dispose()
        {
            
        }
    }
}