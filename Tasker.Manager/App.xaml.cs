﻿using System;
using System.IO;
using System.Windows;
using Tasker.Manager.Controls;
using Tasker.Manager.Managers.Logger;
using Tasker.Manager.Properties;

namespace Tasker.Manager
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            bool? exit;

            while (true)
            {
                LoadSettings();
                LoginWindow loginWindow = new LoginWindow();
                bool? showDialog = loginWindow.ShowDialog();

                if (showDialog.HasValue && showDialog.Value)
                {
                    try
                    {
                        MainWindow mainWindow = new MainWindow();
                        exit = mainWindow.ShowDialog();
                    }
                    catch (Exception exception)
                    {
                        exit = false;
                        Console.WriteLine(exception);
                    }
                }
                else
                {
                    break;
                }
                SaveSettings();
                if (exit.HasValue && exit.Value) break;
            }

            this.Shutdown();
        }

        private void ApplicationExit(object sender, ExitEventArgs e)
        {
            SaveSettings();
        }

        private static void SaveSettings()
        {
//            Repository.UserConfiguration.LastLogin = Settings.Default.LastLogin;
            Configuration.Config.Save();
            Settings.Default.Save();
        }

        private static void LoadSettings()
        {
            Repository.UserConfiguration = Configuration.Config.Load();

            Settings.Default.AutoLogOff = true;
            Settings.Default.LogOffTime = 15;
            Settings.Default.LastLogin = Repository.UserConfiguration.LastLogin;
            
            Settings.Default.Save();

#if DEBUG
            Settings.Default.NetTcpAddress = "localhost:8732";
            Settings.Default.RssUrl = "http://habrahabr.ru/rss/hubs//";
#endif
        }

        private void ApplicationStartup(object sender, StartupEventArgs e)
        {
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            Version version = assembly.GetName().Version;
            Log.Write(LogLevel.Info, String.Format("Current version: {0}", version));
        }

        public static string GetSettingsFilesPath()
        {
            string localAppData = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            string userFilePath = Path.Combine(localAppData, "[V.M.K.]_Team");
            if (!Directory.Exists(userFilePath)) Directory.CreateDirectory(userFilePath);
            return userFilePath;
        }

        public static string CurrentVersion
        {
            get
            {
                System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
                Version version = assembly.GetName().Version;
                return version.ToString();
            }
        }

    }
}
