using System;
using System.Globalization;
using System.Windows.Controls;

namespace Tasker.Manager.Validator
{
    public class DeadlineTimeValidator : ValidationRule
    {
        public TimeSpan PlannedTime { get; set; }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            DateTime deadlineTime = (DateTime) value;
            if (deadlineTime == null || PlannedTime == null) new ValidationResult(false, "������ ������.");

            DateTime minEndTime = DateTime.Now.Add(PlannedTime);
            if (deadlineTime >= minEndTime)
            {
                return new ValidationResult(true, null);
            }
            else
            {
                return new ValidationResult(false, "������� �� ����� ���� ������ ����������� ���� ����������. ��������� ����������� �����.");
            }
        }
    }
}