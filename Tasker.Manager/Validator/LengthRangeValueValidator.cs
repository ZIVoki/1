﻿using System.Globalization;
using System.Windows.Controls;
using Tasker.Manager.Resources;

namespace Tasker.Manager.Validator
{
    class LengthRangeValueValidator : ValidationRule
    {
        public int Min { get; set; }
        public int Max { get; set; }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (value != null)
            {
                var length = ((string) value).Trim().Length;
                string message = string.Empty;
                if (length < Min)
                    message = string.Format(ValidatorMessages.ValueMustBeLonger, Min);
                if (length > Max)
                    message = string.Format(ValidatorMessages.ValueMustShorter, Max);
                if (!string.IsNullOrEmpty(message))
                    return new ValidationResult(false, message);
            }
            return new ValidationResult(true, null);
        }
    }
}
