using System.Globalization;
using System.Windows.Controls;
using Tasker.Manager.Resources;

namespace Tasker.Manager.Validator
{
    class NullValueValidator : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (value == null) return new ValidationResult(false, ValidatorMessages.ValueCantBeNull);
    
            string inputString = value.ToString();
            if (string.IsNullOrEmpty(inputString) || string.IsNullOrWhiteSpace(inputString))
                return new ValidationResult(false, ValidatorMessages.ValueCantBeNull);

            return new ValidationResult(true, null);
        }
    }
}
