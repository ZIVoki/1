using System;
using System.Globalization;
using System.Windows.Controls;
using Tasker.Manager.Resources;

namespace Tasker.Manager.Validator
{
    class IsInPastDateValidator : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if(value != null)
            {
                DateTime date = (DateTime) value;
                if (date.Date >= DateTime.Now.Date)
                    return new ValidationResult(true, null);
            }
            return new ValidationResult(false, ValidatorMessages.DateIsInPast);
        }
    }
}
