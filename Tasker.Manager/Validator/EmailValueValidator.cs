using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using Tasker.Manager.Resources;

namespace Tasker.Manager.Validator
{
    class EmailValueValidator : ValidationRule
    {
        const string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                                    @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                                    @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            //if (value == null) return new ValidationResult(true, null);
            string inputEmail = value as string;
            if (string.IsNullOrEmpty(inputEmail)) return new ValidationResult(true, null);
            
            Regex regex = new Regex(strRegex);
            return regex.IsMatch(inputEmail) ? 
                new ValidationResult(true, null) :
                new ValidationResult(false, ValidatorMessages.ValueMustBeEmail);
        }
    }
}
