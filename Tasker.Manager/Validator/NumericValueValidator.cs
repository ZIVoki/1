using System;
using System.Globalization;
using System.Windows.Controls;
using Tasker.Manager.Resources;

namespace Tasker.Manager.Validator
{
    class NumericValueValidator : ValidationRule
    {
        public int Min { get; set; }
        public int Max { get; set; }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
       {
           if (value == null) return new ValidationResult(true, null);
            int parameter = 0;
            string inputString = value.ToString();
            try
            {
                if (inputString.Length > 0)
                    parameter = int.Parse(inputString);
            }
            catch (FormatException)
            {
                return new ValidationResult(false, ValidatorMessages.ValueMustBeNumerable);
            }

            if ((parameter < Min) || (parameter > Max))
            {
                return new ValidationResult(false, String.Format("{0} {1}-{2}.", ValidatorMessages.ValueMustBeIn, Min, Max));
            }
            return new ValidationResult(true, null);
        }
    }
}
