using System.Globalization;
using System.Windows.Controls;
using Tasker.Manager.Resources;
using Tasker.Tools.ViewModel;

namespace Tasker.Manager.Validator
{
    class UserWorkStateValidator : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if(value != null)
            {
                var user = (UserView) value;
                var state = (Tools.Constants.UserWorkState)user.UserWorkState;
                switch (state)
                {
                    case Tools.Constants.UserWorkState.Busy:
                    case Tools.Constants.UserWorkState.BackgroundTask:
                    case Tools.Constants.UserWorkState.NotAvailable:
                        return new ValidationResult(false, ValidatorMessages.UserBusyOrNotAvailable);
                        break;
                    case Tools.Constants.UserWorkState.Free:
                    default:
                        return new ValidationResult(true, null);
                        break;
                }
            }
            else
            {
                return new ValidationResult(true, null);
            }
        }
    }
}
