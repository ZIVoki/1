﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Reflection;
using Microsoft.Office.Interop.Excel;
using Tasker.Manager.Managers.Logger;

namespace Tasker.Manager.Helpers
{
    /// <summary>
    /// Summary description for MyExcel.
    /// </summary>
    public class ExcelHelper
    {
        #region Static Fields
//        private static readonly double cmToPointsHeight = 29.26016;
//        private static readonly double cmToPointsWidth = 4.86;
        public static readonly object COM_FALSE = false;
        public static readonly object COM_TRUE = true;
        public static readonly object HORIZONTAL_ALIGNMENT_VALUE = 1;
        public static readonly object HORIZONTAL_ALIGNMENT_RIGHT = -4152;
        public static readonly object HORIZONTAL_ALIGNMENT_LEFT = -4131;
        public static readonly object HORIZONTAL_ALIGNMENT_CENTER = -4108;

        public static readonly object VERTICAL_ALIGNMENT_VALUE = -4117;
        public static readonly object VERTICAL_ALIGNMENT_BOTTOM = -4107;
        public static readonly object VERTICAL_ALIGNMENT_TOP = -4160;
        public static readonly object VERTICAL_ALIGNMENT_CENTER = -4108;

        #endregion

        readonly object oExcel;
        readonly object oWorkbooks;
        readonly object oWorkbook;
        readonly object oWorksheets;
        object oWorksheet;

        public Application ExcelApplication
        {
            get { return (Application)oExcel; }
        }

        public Workbooks Workbooks
        {
            get { return (Workbooks)oWorkbooks; }
        }

        public Workbook Workbook
        {
            get { return (Workbook)oWorkbook; }
        }

        public Sheets Worksheets
        {
            get { return (Sheets)oWorksheets; }
        }

        public Worksheet Worksheet
        {
            get { return (Worksheet)oWorksheet; }
        }

        public ExcelHelper(out bool creationFailed)
        {
            try
            {
                oExcel = Activator.CreateInstance(Type.GetTypeFromProgID("Excel.Application"));
                oWorkbooks = oExcel.GetType().InvokeMember("Workbooks", BindingFlags.GetProperty, null, oExcel, null);
                oWorkbook = oWorkbooks.GetType().InvokeMember("Add", BindingFlags.InvokeMethod, null, oWorkbooks, null, new System.Globalization.CultureInfo(1033));
                oWorksheets =
                    oWorkbook.GetType().InvokeMember("Worksheets", BindingFlags.GetProperty, null, oWorkbook, null);
                oWorksheet =
                    oWorksheets.GetType().InvokeMember(
                        "Item", BindingFlags.GetProperty, null, oWorksheets, new object[] { 1 });
                creationFailed = false;
            }
            catch (Exception ex)
            {
                Log.Write(LogLevel.Error, "Excel application not found.", ex);
                creationFailed = true;
                return;
            }
            
        }

        /// <summary>
        /// Reads excel file.
        /// </summary>
        /// <param name="fileName"></param>
        public ExcelHelper(string fileName)
        {
            oExcel = Activator.CreateInstance(Type.GetTypeFromProgID("Excel.Application"));
            oWorkbooks = ExcelApplication.Workbooks;
            //oExcel.GetType().InvokeMember( "Workbooks", BindingFlags.GetProperty, null, oExcel, null );
            oWorkbook = oWorkbooks.GetType().InvokeMember("Open", BindingFlags.InvokeMethod, null, oWorkbooks, new object[] { fileName });
            oWorksheets = this.Workbook.Worksheets;
            //    oWorkbook.GetType().InvokeMember( "Worksheets", BindingFlags.GetProperty, null, oWorkbook, null );
            oWorksheet =
                oWorksheets.GetType().InvokeMember(
                    "Item", BindingFlags.GetProperty, null, oWorksheets, new object[] { 1 });
        }

        public ExcelHelper(String[] listNames)
        {
            oExcel = Activator.CreateInstance(Type.GetTypeFromProgID("Excel.Application"));
            oWorkbooks = ExcelApplication.Workbooks;
            //oExcel.GetType().InvokeMember( "Workbooks", BindingFlags.GetProperty, null, oExcel, null );
            oWorkbook = oWorkbooks.GetType().InvokeMember("Add", BindingFlags.InvokeMethod, null, oWorkbooks, null);
            oWorksheets = this.Workbook.Worksheets;
            //    oWorkbook.GetType().InvokeMember( "Worksheets", BindingFlags.GetProperty, null, oWorkbook, null );
            oWorksheet =
                oWorksheets.GetType().InvokeMember(
                    "Item", BindingFlags.GetProperty, null, oWorksheets, new object[] { 1 });

            // Deletes all list except first
            for (int i = Worksheets.Count; i > 1; i--)
            {
                ((Worksheet)(Worksheets.Item[i])).Delete();
            }

            for (int i = 0; i < listNames.Length; i++)
            {
                Worksheet tmpSheet;

                if (i > 0)
                {
                    tmpSheet =
                        (Worksheet)
                        Worksheets.GetType().InvokeMember(
                            "Add", BindingFlags.InvokeMethod, null, Worksheets, new object[] { });
                }
                else
                {
                    tmpSheet = Worksheet;
                }

                tmpSheet.Name = listNames[listNames.Length - 1 - i];

                Marshal.ReleaseComObject(tmpSheet);
            }

            oWorksheet =
                oWorksheets.GetType().InvokeMember(
                    "Item", BindingFlags.GetProperty, null, oWorksheets, new object[] { 1 });
        }

        public void SelectWorksheet(int position)
        {
            oWorksheet = Worksheets.Item[position];
            this.Worksheet.Select(Type.Missing);
        }

        public void Show(bool showOrNot)
        {
            ExcelApplication.Visible = showOrNot;
        }

        public void SetOrientation(XlPageOrientation orientation, Worksheet workSheet)
        {
            var pageSetup = workSheet.GetType().InvokeMember("PageSetup", BindingFlags.GetProperty,
                null, workSheet, null);
            
            var tt1 = pageSetup.GetType().InvokeMember("Orientation", BindingFlags.GetProperty,
                null, pageSetup, null);
            
            pageSetup.GetType().InvokeMember("Orientation", BindingFlags.SetProperty,
                null, pageSetup, new object[] { orientation });

            try
            {
                var tt2 = pageSetup.GetType().InvokeMember("Orientation", BindingFlags.GetProperty,
                                                           null, pageSetup, null);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        public Range GetRange(Worksheet sheet, string from, string to)
        {
            object[] args = new object[2];
            args[0] = from;
            args[1] = to;

            return sheet.Range[from, to];
        }

        public Range GetRange(Worksheet sheet, int x1, int y1, int x2, int y2)
        {
            return sheet.GetType().InvokeMember(
                       "Range"
                       , BindingFlags.GetProperty
                       , null
                       , sheet
                       , new object[] { GetRangeString(x1, y1, x2, y2) }
                       ) as Range;
        }

        public Range GetRange(string from, string to)
        {
            return (GetRange(Worksheet, from, to));
        }

        public Range GetRange(int x1, int y1, int x2, int y2)
        {
            return (GetRange(Worksheet, x1, y1, x2, y2));
        }


        public Range getCell(int row, int col, Worksheet sheet)
        {
            object[] args = new object[2];
            args[0] = row;
            args[1] = col;
            return (Range)sheet.Cells[row, col];
        }

        public Range getCell(int row, int col)
        {
            return getCell(row, col, Worksheet);
        }

        public void MergeRange(Worksheet sheet, String fromCell, String toCell)
        {
            Range rng = GetRange(sheet, fromCell, toCell);
            rng.Merge(COM_FALSE);
            Marshal.ReleaseComObject(rng);
        }

        public void MergeRange(String fromCell, String toCell)
        {
            MergeRange(Worksheet, fromCell, toCell);
        }

        public void MergeRange(Worksheet sheet, int x1, int y1, int x2, int y2)
        {
            Range rng = GetRange(sheet, x1, y1, x2, y2);
            rng.Merge(COM_FALSE);
            Marshal.ReleaseComObject(rng);
        }

        public void MergeRange(int x1, int y1, int x2, int y2)
        {
            MergeRange(Worksheet, x1, y1, x2, y2);
        }

        public Range GetCell(Worksheet sheet, int row, int col)
        {
            return
                (Range)
                sheet.GetType().InvokeMember("Cells", BindingFlags.GetProperty, null, sheet, new object[] { row, col });
        }

        public Range GetCell(int row, int col)
        {
            return GetCell(this.Worksheet, row, col);
        }

        public void SetCellValue(Worksheet sheet, int row, int col, string value)
        {
            Range oCell = GetCell(sheet, row, col);

            oCell.GetType().InvokeMember(
                "Value"
                , BindingFlags.SetProperty
                , null
                , oCell
                , new object[] { value }
                );
            Marshal.ReleaseComObject(oCell);
        }

        public string GetCellValue(Worksheet sheet, int row, int col)
        {
            Range oCell = GetCell(sheet, row, col);

            object result = oCell.GetType().InvokeMember(
                "Value"
                , BindingFlags.GetProperty
                , null
                , oCell
                , null
            );

            Marshal.ReleaseComObject(oCell);

            return ((result != null) ? result.ToString() : string.Empty);
        }

        public void SetCellValue(int row, int col, string value)
        {
            SetCellValue(this.Worksheet, row, col, value);
        }

        public Object GetColumns(Worksheet sheet, int x1, int x2)
        {
            return
                (sheet.GetType().InvokeMember(
                    "Columns", BindingFlags.GetProperty, null, sheet, new object[] { GetRowsString(x1, x2) }));
        }

        public Object GetRows(Worksheet sheet, int x1, int x2)
        {
            return
                (sheet.GetType().InvokeMember(
                    "Rows", BindingFlags.GetProperty, null, sheet, new object[] { GetRowsString(x1, x2) }));
        }

        public String GetRangeString(int x1, int y1, int x2, int y2)
        {
            return GetRangeString(x1, y1, x2, y2, true);
        }

        public static string GetCharEquivalent(int x)
        {
            int a = x;
            int charCount = (int)'Z' - (int)'A' + 1;
            string resultString = "";

            while ((a / (double)charCount) > 1.0)
            {
                if (a > 0)
                {
                    if (a % charCount > 0)
                    {
                        resultString = (char)((a % charCount) + 64) + resultString;
                    }
                    else if (a % charCount == 0)
                    {
                        resultString = (char)(90) + resultString;
                        a--;
                    }
                }

                a = a / charCount;
            }

            if (a > 0)
                resultString = (char)(a + 64) + resultString;

            return resultString;//.Replace( (char)64, (char)90 );
        }

        public String GetRangeString(int x1, int y1, int x2, int y2, bool isRelative)
        {
            if (isRelative)
            {
                return (GetCharEquivalent(x1) + y1 + ":" + GetCharEquivalent(x2) + y2);
            }
            return "$" + (GetCharEquivalent(x1) + "$" + y1 + ":" + "$" + GetCharEquivalent(x2) + "$" + y2);
        }

        public String GetColumnsString(int x1, int x2)
        {
            return GetCharEquivalent(x1) + ":" + GetCharEquivalent(x1);
        }

        public String GetRowsString(int x1, int x2)
        {
            return x1.ToString() + ':' + x2;
        }

        public void SetRangeFontStyle(
            Worksheet sheet, string from, string toCell, string fontName, int size, bool isItalic, bool isBold,
            bool isWrap, object vertAlignemenet, object horAlignement)
        {
            Range oRange = GetRange(sheet, from, toCell);

            oRange.Font.Name = fontName;

            if (isItalic == true)
            {
                oRange.Font.Italic = COM_TRUE;
            }
            else
            {
                oRange.Font.Italic = COM_FALSE;
            }

            oRange.Font.Bold = isBold;
            oRange.WrapText = isWrap;

            if (vertAlignemenet != null)
            {
                oRange.VerticalAlignment = vertAlignemenet;
            }

            if (horAlignement != null)
            {
                oRange.HorizontalAlignment = horAlignement;
            }

            oRange.Font.Size = size.ToString();

            Marshal.ReleaseComObject(oRange);
        }

        public void SetRangeFontStyle(
            string from, string toCell, string fontName, int size, bool isItalic, bool isBold, bool isWrap)
        {
            SetRangeFontStyle(this.Worksheet, from, toCell, fontName, size, isItalic, isBold, isWrap, null, null);
        }

        public void SetRangeFontStyle(
            string from, string toCell, string fontName, int size, bool isItalic, bool isBold, bool isWrap, object vert, object hor)
        {
            SetRangeFontStyle(this.Worksheet, from, toCell, fontName, size, isItalic, isBold, isWrap, vert, hor);
        }
        public void SetRangeBgColor(
            string from, string toCell, int color)
        {
            SetRangeBgColor(Worksheet, from, toCell, color);
        }
        public void SetRangeBgColor(
            Worksheet sheet, string from, string toCell, int color)
        {
            Range oRange = GetRange(sheet, from, toCell);

            oRange.Interior.ColorIndex = color;

            Marshal.ReleaseComObject(oRange);
        }

        public void SetRangeBorders(
            string from, string toCell)
        {
            SetRangeBorders(Worksheet, from, toCell);
        }
        public void SetRangeBorders(Worksheet sheet, string from, string toCell)
        {
            Range oRange = GetRange(sheet, from, toCell);
            oRange.Borders.LineStyle = 1;
            oRange.Borders.Weight = 2;
            oRange.Borders.ColorIndex = -4105;

            Marshal.ReleaseComObject(oRange);
        }

        public void SetRangeBordersSquare(int x1, int y1, int x2, int y2)
        {
            SetRangeBordersSquare(Worksheet, x1, y1, x2, y2);
        }

        public void SetRangeBordersSquare(Worksheet sheet, int x1, int y1, int x2, int y2)
        {
            // TOP
            Range oRange = GetRange(sheet, x1, y1, x2, y1);

            oRange.Borders.Item[XlBordersIndex.xlEdgeTop].LineStyle = 1;
            oRange.Borders.Item[XlBordersIndex.xlEdgeLeft].LineStyle = 1;
            oRange.Borders.Item[XlBordersIndex.xlEdgeRight].LineStyle = 1;

            oRange.Borders.Weight = 2;
            oRange.Borders.ColorIndex = -4105;

            Marshal.ReleaseComObject(oRange);

            // Middle
            oRange = GetRange(sheet, x1, y1 + 1, x2, y2 - 1);

            oRange.Borders.Item[XlBordersIndex.xlEdgeTop].LineStyle = 1;
            oRange.Borders.Item[XlBordersIndex.xlEdgeLeft].LineStyle = 1;
            oRange.Borders.Item[XlBordersIndex.xlEdgeRight].LineStyle = 1;

            oRange.Borders.Weight = 2;
            oRange.Borders.ColorIndex = -4105;

            Marshal.ReleaseComObject(oRange);

            // Bottom
            oRange = GetRange(sheet, x1, y2, x2, y2);

            oRange.Borders.Item[XlBordersIndex.xlEdgeBottom].LineStyle = 1;
            oRange.Borders.Item[XlBordersIndex.xlEdgeLeft].LineStyle = 1;
            oRange.Borders.Item[XlBordersIndex.xlEdgeRight].LineStyle = 1;

            oRange.Borders.Weight = 2;
            oRange.Borders.ColorIndex = -4105;

            Marshal.ReleaseComObject(oRange);
        }

        public void SetPageHeader(string value)
        {
            SetPageHeader(Worksheet, value);
        }

        public void SetPageHeader(Worksheet sheet, string value)
        {
            try
            {
                sheet.PageSetup.CenterHeader = value;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        public void SetColumnWidth(int columnNumber, double columnWidth)
        {
            SetColumnWidth(GetCharEquivalent(columnNumber), columnWidth);
        }

        public void SetColumnWidth(string columnName, double columnWidth)
        {
            SetColumnWidth(this.Worksheet, columnName, columnWidth);
        }

        public void SetColumnWidth(Worksheet sheet, int columnNumber, double columnWidth)
        {
            SetColumnWidth(sheet, GetCharEquivalent(columnNumber), columnWidth);
        }

        public void SetColumnWidth(Worksheet sheet, string columnName, double columnWidth)
        {
            object oCol = sheet.GetType().InvokeMember(
                "Columns"
                , BindingFlags.GetProperty
                , null
                , sheet
                , new object[] { columnName + ":" + columnName }
                );
            oCol.GetType().InvokeMember(
                "ColumnWidth"
                , BindingFlags.SetProperty
                , null
                , oCol
                , new object[] { columnWidth }
                );

            // прибираем за собой связи
            Marshal.ReleaseComObject(oCol);
        }

        public void SetPageMargins(Worksheet sheet, double left, double right, double top, double bottom, double headerMargin)
        {
            sheet.PageSetup.BottomMargin = bottom;
            sheet.PageSetup.LeftMargin = left;
            sheet.PageSetup.TopMargin = top;
            sheet.PageSetup.RightMargin = right;
            sheet.PageSetup.HeaderMargin = headerMargin;
        }
        public void SetPageMargins(double left, double right, double top, double bottom, double headerMargin)
        {
            SetPageMargins(Worksheet, left, right, top, bottom, headerMargin);
        }

        public void delete(bool closeIt)
        {
            if (closeIt)
            {
                this.Workbook.Close(false, null, null);
            }
            
            #region Уничтожение объектов Excel

            Marshal.ReleaseComObject(oWorksheet);
            Marshal.ReleaseComObject(oWorksheets);
            Marshal.ReleaseComObject(oWorkbook);
            Marshal.ReleaseComObject(oWorkbooks);
            Marshal.ReleaseComObject(oExcel);

            #endregion

            #region Вызов сборщика мусора для немедленной очистки памяти

            GC.GetTotalMemory(true);

            #endregion
        }

        public void SaveAs(string fileName)
        {
            if (File.Exists(fileName))
                File.Delete(fileName);

//            Workbook.GetType().InvokeMember(
//                "SaveAs", BindingFlags.InvokeMethod, null, Workbook, new object[] { fileName });
            Workbook.SaveAs(fileName, XlFileFormat.xlWorkbookNormal, null, null, false, false,
                            XlSaveAsAccessMode.xlShared, XlSaveConflictResolution.xlUserResolution, false, false, false);
            Workbook.Save();
        }

        public void SaveAsAndClose(string fileName)
        {
            SaveAs(fileName);
            delete(true);
        }
    }
}
