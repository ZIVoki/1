﻿using System;
using Tasker.Tools.ViewModel.Base;

namespace Tasker.Manager.Configuration
{
    [Serializable]
    public class LoadSettings : ObservableObject
    {
        private int _loadAge = 5;

        public int LoadAge
        {
            get { return _loadAge; }
            set
            {
                _loadAge = value;
                RaisePropertyChanged(() => LoadAge);
            }
        }
    }
}
