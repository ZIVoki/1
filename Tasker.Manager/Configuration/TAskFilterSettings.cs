﻿using System;
using System.Xml.Serialization;
using Tasker.Tools.ViewModel;
using Tasker.Tools.ViewModel.Base;

namespace Tasker.Manager.Objects
{
    [Serializable]
    public class TaskFilterSettings: ObservableObject
    {
        private bool m_isChanged;

        [XmlIgnore]
        public bool IsChanged
        {
            get
            {
                return m_isChanged;
            }
            set
            {
                m_isChanged = value;
                RaisePropertyChanged("IsChanged");
            }
        }

        #region ProtectedFileds

        protected bool? filterByActive;
        protected bool filterByClosed;
        protected bool filterByMy;
        protected bool filterByCustomers;
        protected bool filterByCreators;

        protected CustomerView selectedCustomer;
        protected TaskTypeView selectedCreators;

        protected bool filterByDateTime;

        protected bool filterTodaysTasks;
        protected bool filterYesturdayTasks;
        protected bool filterCustomDateTime;

        protected DateTime filterCustomStartDate;
        protected DateTime filterCustomEndDate;

        #endregion

        #region Properties

        public bool? FilterByActive
        {
            get { return filterByActive; }
            set
            {
                filterByActive = value;
                RaisePropertyChanged("FilterByActive");
            }
        }

        public bool FilterByClosed
        {
            get { return filterByClosed; }
            set 
            { 
                filterByClosed = value;
                RaisePropertyChanged("FilterByClosed");
            }
        }

        public bool FilterByMy
        {
            get { return filterByMy; }
            set
            {
                filterByMy = value;
                RaisePropertyChanged("FilterByMy");
            }
        }

        public bool FilterByCustomers
        {
            get { return filterByCustomers; }
            set
            {
                filterByCustomers = value;
                RaisePropertyChanged("FilterByCustomers");
            }
        }

        public bool FilterByCreators
        {
            get { return filterByCreators; }
            set
            {
                filterByCreators = value;
                RaisePropertyChanged("FilterByCreators");
            }
        }

        public CustomerView SelectedCustomer
        {
            get { return selectedCustomer; }
            set
            {
                selectedCustomer = value;
                RaisePropertyChanged("SelectedCustomer");
            }
        }

        public TaskTypeView SelectedCreators
        {
            get { return selectedCreators; }
            set
            {
                selectedCreators = value;
                RaisePropertyChanged("SelectedCreators");
            }
        }

        public bool FilterByDateTime
        {
            get { return filterByDateTime; }
            set
            {
                filterByDateTime = value;
                RaisePropertyChanged("FilterByDateTime");
            }
        }

        public bool FilterTodaysTasks
        {
            get { return filterTodaysTasks; }
            set
            {
                filterTodaysTasks = value;
                RaisePropertyChanged("FilterTodaysTasks");
            }
        }

        public bool FilterYesturdayTasks
        {
            get { return filterYesturdayTasks; }
            set
            {
                filterYesturdayTasks = value;
                RaisePropertyChanged("FilterYesturdayTasks");
            }
        }

        public bool FilterCustomDateTime
        {
            get { return filterCustomDateTime; }
            set
            {
                filterCustomDateTime = value;
                RaisePropertyChanged("FilterCustomDateTime");
            }
        }

        public DateTime FilterCustomStartDate
        {
            get { return filterCustomStartDate; }
            set
            {
                filterCustomStartDate = value;
                RaisePropertyChanged("FilterCustomStartDate");
            }
        }

        public DateTime FilterCustomEndDate
        {
            get { return filterCustomEndDate; }
            set
            {
                filterCustomEndDate = value;
                RaisePropertyChanged("FilterCustomEndDate");
            }
        }

        #endregion
    }

}
