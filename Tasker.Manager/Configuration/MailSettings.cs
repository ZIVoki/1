using System;
using Tasker.Tools.ViewModel.Base;

namespace Tasker.Manager.Configuration
{
    [Serializable]
    public class MailSettings: ObservableObject
    {
        private string host = "";
        private int port = 25;
        private string fromEmail = "";
//        private string login;
        private string password = "";

        public string Host
        {
            get { return host; }
            set
            {
                host = value;
                RaisePropertyChanged("Host");
            }
        }

        public int Port
        {
            get { return port; }
            set
            {
                port = value;
                RaisePropertyChanged("Port");
            }
        }

        public string FromEmail
        {
            get { return fromEmail; }
            set
            {
                fromEmail = value;
                RaisePropertyChanged("FromEmail");
            }
        }

        /*public string Login
        {
            get { return login; }
            set
            {
                login = value;
                RaisePropertyChanged("Login");
            }
        }*/

        public string Password
        {
            get { return password; }
            set
            {
                password = value;
                RaisePropertyChanged("Password");
            }
        }
    }
}