﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Xml.Serialization;
using Tasker.Tools.ServiceReference;

namespace Tasker.Manager.Objects
{
    [Serializable]
    public class HighlightSettings
    {
        [XmlArray("CustomerHighlightColors"), XmlArrayItem("color", typeof(KeyValuePairSerializable<int, int>))]
        public List<KeyValuePairSerializable<int, int>> CustomerHighlightColors = new List<KeyValuePairSerializable<int, int>>();

        [XmlArray("TaskTypesHighlightColors"), XmlArrayItem("color", typeof(KeyValuePairSerializable<int, int>))]
        public List<KeyValuePairSerializable<int, int>> TaskTypesHighlightColors = new List<KeyValuePairSerializable<int, int>>();
        
        public Color? GetCustomerHighlightColor(int customerId)
        {
            foreach (var customerHighlightColor in CustomerHighlightColors)
            {
                if(customerHighlightColor.Key == customerId)
                {
                    return Color.FromArgb(customerHighlightColor.Value);
                }
            }

            return null;
        }

        public Color? GetTaskTypeHighlightColor(int taskTypeId)
        {
            foreach (var taskTypesHighlightColor in TaskTypesHighlightColors)
            {
                if (taskTypesHighlightColor.Key == taskTypeId)
                {
                    return Color.FromArgb(taskTypesHighlightColor.Value);
                }
            }

            return null;
        }

        public void InsertCustomerColor(KeyValuePairSerializable<int, int> item)
        {
            for (int i = 0; i < CustomerHighlightColors.Count; i++)
            {
                var customerHighlightColor = CustomerHighlightColors[i];
                if (customerHighlightColor.Key == item.Key)
                {
                    CustomerHighlightColors.Remove(customerHighlightColor);
                    i--;
                }
            }
            CustomerHighlightColors.Add(item);
        }

        public void InsertTaskTypeColor(KeyValuePairSerializable<int, int> item)
        {
            for (int i = 0; i < TaskTypesHighlightColors.Count; i++)
            {
                var taskTypeHighlightColor = TaskTypesHighlightColors[i];
                if (taskTypeHighlightColor.Key == item.Key)
                {
                    TaskTypesHighlightColors.Remove(taskTypeHighlightColor);
                    i--;
                }
            }
            TaskTypesHighlightColors.Add(item);
        }


        public Color? GetTaskTypeHighlightColor(TaskTypeDTO taskTypeDto)
        {
            if (taskTypeDto == null) return null;
            return GetTaskTypeHighlightColor(taskTypeDto.Id);
        }

        public Color? GetCustomerHighlightColor(CustomerDTO customerDto)
        {
            if (customerDto == null) return null;
            return GetCustomerHighlightColor(customerDto.Id);
        }
    }

    [Serializable]
    [XmlType(TypeName = "KeyValue")]
    public struct KeyValuePairSerializable<TK, TV>
    {
        public TK Key
        { get; set; }

        public TV Value
        { get; set; }
    }
}
