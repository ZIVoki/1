﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Tasker.Manager.Managers.Logger;
using Tasker.Manager.Objects;
using Tasker.Manager.Properties;
using Tasker.Tools.ViewModel.Base;

namespace Tasker.Manager.Configuration
{
    [Serializable]
    public class Config : ObservableObject
    {
        private static readonly XmlSerializer Serializer = new XmlSerializer(typeof(Config));
        static Config _config;

        private string _lastLogin = "";
        private bool? _isAutoLogin = false;
        private HighlightSettings _highlightSettings;
        private TaskFilterSettings _taskFilterSettings;
        private List<int> _mailOffersId;
        private DateTime _lastRssDate;
        private LoadSettings _loadSettings;
        private MailSettings _mailSettings;

        #region Properties

        public HighlightSettings HighlightSettings
        {
            get { return _highlightSettings; }
            set
            {
                _highlightSettings = value;
                RaisePropertyChanged(() => HighlightSettings);
            }
        }

        public TaskFilterSettings TaskFilterSettings
        {
            get { return _taskFilterSettings; }
            set
            {
                _taskFilterSettings = value;
                RaisePropertyChanged(() => TaskFilterSettings);
            }
        }

        public MailSettings MailSettings
        {
            get { return _mailSettings; }
            set
            {
                _mailSettings = value;
                RaisePropertyChanged(() => MailSettings);
            }
        }

        public LoadSettings LoadSettings
        {
            get { return _loadSettings; }
            set
            {
                _loadSettings = value;
                RaisePropertyChanged(() => LoadSettings);
            }
        }

        public List<int> MailOffersId
        {
            get { return _mailOffersId; }
            set
            {
                _mailOffersId = value;
                RaisePropertyChanged(() => MailOffersId);
            }
        }

        public string LastLogin
        {
            get { return _lastLogin; }
            set
            {
                _lastLogin = value;
                RaisePropertyChanged(() => LastLogin);
            }
        }

        public DateTime LastRssDate
        {
            get { return _lastRssDate; }
            set
            {
                _lastRssDate = value;
                RaisePropertyChanged(() => LastRssDate);
            }
        }

        public bool? IsAutoLogin
        {
            get { return _isAutoLogin; }
            set
            {
                _isAutoLogin = value;
                RaisePropertyChanged(() => IsAutoLogin);
            }
        }

        #endregion

        protected Config()
        {
            HighlightSettings = new HighlightSettings();
            TaskFilterSettings = new TaskFilterSettings();
            MailSettings = new MailSettings();
            LoadSettings = new LoadSettings();
            MailOffersId = new List<int>();
            LastLogin = "";
            IsAutoLogin = false;
            LastRssDate = DateTime.Now.AddDays(-1);
        }

        public static void Save()
        {
            try
            {
                var settingsPath = GetSettingsPath();
                TextWriter streamWriter = new StreamWriter(settingsPath);
                Serializer.Serialize(streamWriter, _config);
                streamWriter.Close();
            }
            catch (Exception ex)
            {
                Log.Write(LogLevel.Error, "Ошибка сохранения пользовательского конфигурационного файла", ex);
            }
        }

        private static string GetSettingsPath()
        {
            string settingsFilesPath = App.GetSettingsFilesPath();
            string settingsPath = Path.Combine(settingsFilesPath, Settings.Default.TaskerUserSettings);
            return settingsPath;
        }

        public static Config Load()
        {
            if (_config == null)
            {
                string settingsPath = GetSettingsPath();

                if (File.Exists(settingsPath))
                {
                    try
                    {
                        TextReader streamReader = new StreamReader(settingsPath);
                        _config = (Config)Serializer.Deserialize(streamReader);
                        streamReader.Close();
                        streamReader.Dispose();
                    }
                    catch (Exception ex)
                    {
                        _config = new Config();
                        Log.Write(LogLevel.Error, "Ошибка загрузки пользовательского конфигурационного файла", ex);
                    }
                }
                else
                {
                    _config = new Config();
                }
            }

            return _config;
        }
    }
}
