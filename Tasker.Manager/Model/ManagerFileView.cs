﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Data;
using System.Windows.Input;
using Tasker.Manager.Managers;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;
using Tasker.Tools.ViewModel.Base;

namespace Tasker.Manager.Model
{
    public class ManagerFileView : FileView
    {
        private CollectionViewSource _usersReadFileCollectionView;

        public ManagerFileView(FileVersionDTO fileVersion, FolderView parent, bool lazyLoadChildren)
            : base(fileVersion, parent, lazyLoadChildren)
        { }


        public ICollectionView UserReadFileCollectionView
        {
            get
            {
                if (_usersReadFileCollectionView == null)
                {
                    _usersReadFileCollectionView = new CollectionViewSource { Source = UsersReadFile };
                    using (_usersReadFileCollectionView.DeferRefresh())
                    {
                        _usersReadFileCollectionView.SortDescriptions.Add(new SortDescription("User.Title",
                                                                                      ListSortDirection.Ascending));
                        _usersReadFileCollectionView.Filter += _usersReadFileCollectionView_Filter;

                    }
                }
                return _usersReadFileCollectionView.View;
            }
        }

        void _usersReadFileCollectionView_Filter(object sender, FilterEventArgs e)
        {
            e.Accepted = false;
            UserReadView userReadView = e.Item as UserReadView;
            if (userReadView == null) return;
            if (userReadView.User == null) return;
            if (userReadView.User.FirstName == "admin") return;
            e.Accepted = true;
        }

        private ICommand _openFileCommand;

        public ICommand OpenFileCommand
        {
            get
            {
                if (_openFileCommand == null)
                {
                    _openFileCommand = new RelayCommand(DoOpenFile);
                }
                return _openFileCommand;
            }
        }

        private ICommand _viewFileCommand;

        public ICommand ViewFileCommand
        {
            get
            {
                if (_viewFileCommand == null)
                {
                    _viewFileCommand = new RelayCommand(DoViewFile);
                }
                return _viewFileCommand;
            }
        }

        private void DoOpenFile()
        {
            if (Repository.LoginUser.Role == ConstantsRoles.Manager)
                DoViewFile();
            if (Repository.LoginUser.Role == ConstantsRoles.Administrator)
                DoEditFile();
        }

        private void DoEditFile()
        {
            try
            {
                Process.Start(SourceFilePath);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            SetFileAsRead();
        }

        private void DoViewFile()
        {
            try
            {
                Process.Start(FilePath);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            SetFileAsRead();
        }

        private void SetFileAsRead()
        {
            if (!ReadDate.HasValue)
            {
                ServiceManager.Inst.ServiceChannel.SetBoardFileAsRead(Id);
                ReadDate = DateTime.Now;
            }
        }
    }
}