using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using Tasker.Tools.ViewModel.Base;

namespace Tasker.Manager.Model
{
    public class UserMonthStatView : ObservableObject
    {
        private DateTime _month;
        private ObservableCollection<UserStatItemDataView> _statItems = new ObservableCollection<UserStatItemDataView>();

        public UserMonthStatView()
        {
            _statItems.CollectionChanged += _statItems_CollectionChanged;
        }

        void _statItems_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add || e.Action == NotifyCollectionChangedAction.Remove)
            {
                RaisePropertyChanged(() => AvgK);
            }
        }

        public ObservableCollection<UserStatItemDataView> StatItems
        {
            get { return _statItems; }
            set
            {
                _statItems = value;
                RaisePropertyChanged(() => StatItems);
            }
        }
        
        public DateTime Month
        {
            get { return _month; }
            set
            {
                _month = value;
                RaisePropertyChanged(() => Month);
            }
        }

        public float AvgK
        {
            get { return (float)(StatItems.Sum(t => t.K) / StatItems.Count); }
        }

        public override string ToString()
        {
            return string.Format("{0:MMMM yyyy}", Month);
        }
    }
}