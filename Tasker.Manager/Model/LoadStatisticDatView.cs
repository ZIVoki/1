using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Tasker.Manager.Base;
using Tasker.Manager.Managers;
using Tasker.Manager.ServiceReference1;
using Tasker.Tools.ViewModel;
using Tasker.Tools.ViewModel.Base;
using Tasker.Tools.Extensions;

namespace Tasker.Manager.Model
{
    public class LoadStatisticDatView: ObservableObject
    {
        // ����� ��������� ����, �.�. ��� ���� ���������� �������� ������
//        private DateTime _currentDate = DateTime.Now.AddDays(-1);
        private static DateTime _startDate = DateTime.Now.AddDays(-1).StartOfDay();
        private static DateTime _endDate = DateTime.Now.AddDays(-1).EndOfDay();

        private static CustomerView _customer;

        private readonly ObservableCollection<LoadItemExt> _loadItems = new ObservableCollection<LoadItemExt>();
        private BackgroundWorker _backgroundWorker;
        private Visibility _loaderVisibility = Visibility.Hidden;
        private Visibility _viewIndex = Visibility.Visible;
        private Visibility _viewTimeTableUsers = Visibility.Visible;
        private Visibility _viewInWorkUsers = Visibility.Visible;
        private Visibility _viewInNetworkUsers = Visibility.Visible;
        private bool _datePickerEnabled = true;
        private int _loadId;
        private static int _interval = 15;
        private static bool _statisicEnabled = true;
        private static bool _indexOn = false;
        private static bool _timeTabkeWorkersOn = true;
        private static bool _inNetworkWorkersOn = false;
        private static bool _busyWorkersOn = true;
        private decimal _averageIndex;

        private static bool _customerFiterOn = false;
        private ICommand _refreshCommand;
        private ICommand _showNextDateCommand;
        private ICommand _showPrevDateCommand;

        public LoadStatisticDatView()
        {
            UpdateStatisctic();
        }

        #region Properties

        public DateTime StartDate
        {
            get { return _startDate; }
            set
            {
                _startDate = value;
                if (_startDate.Date != _endDate.Date)
                {
                    EndDate = new DateTime(value.Year, value.Month, value.Day, _endDate.Hour, _endDate.Minute,
                                           _endDate.Second);
                }
                else
                {
                    RaisePropertyChanged(() => Header);
                    UpdateStatisctic();
                }
                RaisePropertyChanged(() => StartDate);
            }
        }

        public DateTime EndDate
        {
            get { return _endDate; }
            set
            {
                _endDate = value;
                if (_startDate.Date != _endDate.Date)
                {
                    StartDate = new DateTime(value.Year, value.Month, value.Day, _startDate.Hour, _startDate.Minute,
                                             _startDate.Second);
                }
                else
                {
                    RaisePropertyChanged(() => Header);
                    UpdateStatisctic();
                }
                RaisePropertyChanged(() => EndDate);
            }
        }

        public CustomerView Customer
        {
            get { return _customer; }
            set
            {
                _customer = value;
                RaisePropertyChanged(() => Customer);
                UpdateStatisctic();
            }
        }

        public string Header
        {
            get { return string.Format("���������� �� �������� �� {0}", _startDate.ToString("d MMMM", new CultureInfo("ru-RU"))); }
        }

        public bool StatisicEnabled
        {
            get { return _statisicEnabled; }
            set
            {
                _statisicEnabled = value;
                RaisePropertyChanged(() => StatisicEnabled);
            }
        }

        public int Interval
        {
            get { return _interval; }
            set
            {
                _interval = value;
                RaisePropertyChanged(() => Interval);
            }
        }

        public Visibility ViewIndex
        {
            get { return _viewIndex; }
            set
            {
                _viewIndex = value;
                RaisePropertyChanged(() => ViewIndex);
            }
        }

        public Visibility ViewInWorkUsers
        {
            get { return _viewInWorkUsers; }
            set
            {
                _viewInWorkUsers = value;
                RaisePropertyChanged(() => ViewInWorkUsers);
            }
        }

        public Visibility ViewInNetworkUsers
        {
            get { return _viewInNetworkUsers; }
            set
            {
                _viewInNetworkUsers = value;
                RaisePropertyChanged(() => ViewInNetworkUsers);
            }
        }

        public Visibility ViewTimeTableUsers
        {
            get { return _viewTimeTableUsers; }
            set
            {
                _viewTimeTableUsers = value;
                RaisePropertyChanged(() => ViewTimeTableUsers);
            }
        }

        public Visibility LoaderVisibility
        {
            get { return _loaderVisibility; }
            set
            {
                _loaderVisibility = value;
                RaisePropertyChanged(() => LoaderVisibility);
            }
        }

        public bool DatePickerEnabled
        {
            get { return _datePickerEnabled; }
            set
            {
                _datePickerEnabled = value;
                RaisePropertyChanged(() => DatePickerEnabled);
            }
        }

        public bool CustomerFiterOn
        {
            get { return _customerFiterOn; }
            set
            {
                _customerFiterOn = value;
                RaisePropertyChanged(() => CustomerFiterOn);
                UpdateStatisctic();
            }
        }

        public ObservableCollection<LoadItemExt> LoadItems
        {
            get { return _loadItems; }
        }

        public bool IndexOn
        {
            get
            {
                return _indexOn;
            }
            set
            {
                _indexOn = value;
                RaisePropertyChanged(() => IndexOn);
            }
        }

        public bool TimeTabkeWorkersOn
        {
            get
            {
                return _timeTabkeWorkersOn;
            }
            set
            {
                _timeTabkeWorkersOn = value;
                RaisePropertyChanged(() => TimeTabkeWorkersOn);
                RaisePropertyChanged(() => HumanResourcesOn);
            }
        }

        public bool InNetworkWorkersOn
        {
            get
            {
                return _inNetworkWorkersOn;
            }
            set
            {
                _inNetworkWorkersOn = value;
                RaisePropertyChanged(() => InNetworkWorkersOn);
                RaisePropertyChanged(() => HumanResourcesOn);
            }
        }

        public bool BusyWorkersOn
        {
            get
            {
                return _busyWorkersOn;
            }
            set
            {
                _busyWorkersOn = value;
                RaisePropertyChanged(() => BusyWorkersOn);
                RaisePropertyChanged(() => HumanResourcesOn);
            }
        }

        public bool HumanResourcesOn
        {
            get
            {
                return _busyWorkersOn || _inNetworkWorkersOn || _timeTabkeWorkersOn;
            }
        }

        public decimal AverageIndex
        {
            get { return _averageIndex; }
            set
            {
                _averageIndex = value;
                RaisePropertyChanged(() => AverageIndex);
                RaisePropertyChanged(() => AverageIndexString);
            }
        }

        public String AverageIndexString
        {
            get { return string.Format("������� �������� �� ������ {0}%", AverageIndex.ToString("0.00")); }
        }

        #endregion

        #region Commands

        public ICommand RefreshCommand
        {
            get
            {
                if (_refreshCommand == null)
                {
                    _refreshCommand = new RelayCommand(DoRefresh);
                }

                return _refreshCommand;
            }
        }

        public ICommand ShowNextDateCommand
        {
            get
            {
                if (_showNextDateCommand == null)
                    _showNextDateCommand = new RelayCommand(DoShowNextDate, CanShowNextDate);
                return _showNextDateCommand;
            }
        }

        public ICommand ShowPrevDateCommand
        {
            get
            {
                if (_showPrevDateCommand == null)
                    _showPrevDateCommand = new RelayCommand(DoShowPrevDate);
                return _showPrevDateCommand;
            }
        }

        private bool CanShowNextDate()
        {
//            return CurrentDate.Date < DateTime.Now.Date;
            return StartDate.Date < DateTime.Now.Date;
        }

        private void DoShowPrevDate()
        {
//            CurrentDate = CurrentDate.AddDays(-1);
            StartDate = StartDate.AddDays(-1);
//            EndDate = EndDate.AddDays(-1);
        }

        private void DoShowNextDate()
        {
//            CurrentDate = CurrentDate.AddDays(1);
            StartDate = StartDate.AddDays(1);
//            EndDate = EndDate.AddDays(1);
        }

        private void DoRefresh()
        {
            UpdateStatisctic();
        }

        #endregion
        
        private void UpdateStatisctic()
        {
            if (_backgroundWorker == null)
            {
                _backgroundWorker = new BackgroundWorker();
                _backgroundWorker.DoWork += BackgroundWorkerDoWork;
                _backgroundWorker.RunWorkerCompleted += BackgroundWorkerRunWorkerCompleted;
            }
            if (!_backgroundWorker.IsBusy)
            {
                DatePickerEnabled = false;
                LoaderVisibility = Visibility.Visible;
                StatisicEnabled = false;
                _backgroundWorker.RunWorkerAsync();
            }
        }

        void BackgroundWorkerRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            _loadItems.Clear();

            LoadManager.InvokeLoadEndStatic(_loadId);
            if (e.Result != null)
            {
                IEnumerable<LoadItemDTO> loadItemDtos = (IEnumerable<LoadItemDTO>) e.Result;
                foreach (LoadItemDTO nextDto in loadItemDtos)
                {
                    LoadItemExt loadItemExt = new LoadItemExt(nextDto);
                    _loadItems.Add(loadItemExt);
                }
                decimal inWorkWorkersSum = (decimal)loadItemDtos.Sum(t => t.InWorkWorkers);
                decimal timeTablesWorkes = loadItemDtos.Sum(t => t.TimeTableWorkers);
                if (timeTablesWorkes == 0)
                    AverageIndex = 0;
                else
                    AverageIndex = inWorkWorkersSum/timeTablesWorkes*100;
            }
            DatePickerEnabled = true;
            LoaderVisibility = Visibility.Hidden;
            StatisicEnabled = true;
        }

        void BackgroundWorkerDoWork(object sender, DoWorkEventArgs e)
        {
            _loadId = LoadManager.InvokeLoadStartStatic(new LoadStartEventArgs("�������� ����������.."));

            int channelId = 0;
            if (CustomerFiterOn)
                channelId = Customer.Id;

            LoadItemDTO[] loadItemDtos = ServiceManager.Inst.ServiceChannel.GetLoadStatisticByCustomer(StartDate, EndDate, Interval, channelId);
            e.Result = loadItemDtos;
        }
    }
}