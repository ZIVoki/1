using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Tasker.Manager.Base;
using Tasker.Manager.Managers;
using Tasker.Manager.ServiceReference1;
using Tasker.Tools.Extensions;
using Tasker.Tools.ViewModel;
using Tasker.Tools.ViewModel.Base;

namespace Tasker.Manager.Model
{
    public class UsersStatDataView : ObservableObject
    {
        private DateTime _startDate = DateTime.Now.AddMonths(-1);
        private DateTime _endDate = DateTime.Now;
        private UserView _currentUserView;
        private ObservableCollection<UserMonthStatView> _userMonthStatViews = new ObservableCollection<UserMonthStatView>(); 
        
        private ICommand _updateViewCommand;
        private RelayCommand _cretateDetailsReport;

        public UsersStatDataView()
        {
            _startDate = _startDate.FirstDayOfMonth();
            _endDate = _endDate.EndOfLastDayOfMonth();
        }
        
        public DateTime StartDate
        {
            get { return _startDate; }
            set
            {
                _startDate = value.FirstDayOfMonth();
                RaisePropertyChanged(() => StartDate);
            }
        }

        public DateTime EndDate
        {
            get { return _endDate; }
            set
            {
                _endDate = value.EndOfLastDayOfMonth();
                RaisePropertyChanged(() => EndDate);
            }
        }

        public UserView CurrentUserView
        {
            get { return _currentUserView; }
            set
            {
                _currentUserView = value;
                RaisePropertyChanged(() => CurrentUserView);
            }
        }

        public ObservableCollection<UserMonthStatView> UserMonthStatViews
        {
            get { return _userMonthStatViews; }
            set
            {
                _userMonthStatViews = value;
                RaisePropertyChanged(() => UserMonthStatViews);
            }
        }

        public ICommand UpdateViewCommand
        {
            get
            {
                if (_updateViewCommand == null)
                    _updateViewCommand = new RelayCommand(DoUpdateView);
                return _updateViewCommand;
            }
        }

        public ICommand CreateDetailsReport
        {
            get
            {
                if (_cretateDetailsReport == null)
                    _cretateDetailsReport = new RelayCommand(DoCreateDetailsReport);
                return _cretateDetailsReport;
            }
        }

        private void DoCreateDetailsReport()
        {
            
        }

        private async void DoUpdateView()
        {
            int invokeLoadStartStatic = LoadManager.InvokeLoadStartStatic(new LoadStartEventArgs("���������� ���������� ������������.."));
            _userMonthStatViews.Clear();
            for (DateTime startDate = _startDate.FirstDayOfMonth(); startDate < _endDate; startDate = startDate.AddMonths(1))
            {
                DateTime endDate = startDate.EndOfLastDayOfMonth();
                UserStatItemDTO[] userStatItemDtos = await ServiceManager.Inst.ServiceChannel.GetUserStatisticAsync(_currentUserView.Id, startDate, endDate);
                UserMonthStatView newUserMonthStatView = new UserMonthStatView {Month = startDate};
                foreach (var userStatItemDto in userStatItemDtos)
                {
                    newUserMonthStatView.StatItems.Add(new UserStatItemDataView(userStatItemDto));
                }
               
                _userMonthStatViews.Add(newUserMonthStatView);
            }
            LoadManager.InvokeLoadEndStatic(invokeLoadStartStatic);
        }
    }
}