using Tasker.Manager.ServiceReference1;
using Tasker.Tools.Managers;
using Tasker.Tools.ViewModel;
using Tasker.Tools.ViewModel.Base;

namespace Tasker.Manager.Model
{
    public class UserStatItemDataView : ObservableObject
    {
        private TaskTypeView _taskTypeView;
        
        private int _avgTime;
        private int _totalTasksCount;

        public UserStatItemDataView(UserStatItemDTO dto)
        {
            _taskTypeView = TaskTypeManager.GetTaskType(dto.TaskType, Repository.TaskTypes);
            _avgTime = dto.AvgMinutes;
            _totalTasksCount = dto.TasksCount;
        }

        public TaskTypeView TaskTypeView
        {
            get { return _taskTypeView; }
            set
            {
                _taskTypeView = value;
                RaisePropertyChanged(() => TaskTypeView);
            }
        }

        public int TotalTasksCount
        {
            get { return _totalTasksCount; }
            set
            {
                _totalTasksCount = value;
                RaisePropertyChanged(() => TotalTasksCount);
            }
        }

        public int AvgTime
        {
            get { return _avgTime; }
            set
            {
                _avgTime = value;
                RaisePropertyChanged(() => AvgTime);
                RaisePropertyChanged(() => K);
            }
        }

        public double K
        {
            get
            {
                return _taskTypeView.CompletionTime.TotalMinutes/_avgTime;
            }
        }
    }
}