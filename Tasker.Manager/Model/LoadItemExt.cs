using System;
using Tasker.Manager.ServiceReference1;
using Tasker.Tools.ViewModel.Base;

namespace Tasker.Manager.Model
{
    public class LoadItemExt: ObservableObject
    {
        private DateTime _timeStamp;
        private int _inWorkWorkers;
        private int _timeTableWorkers;
        private int _inNetWorkWorkers;

        public DateTime TimeStamp
        {
            get { return _timeStamp; }
            set
            {
                _timeStamp = value;
                RaisePropertyChanged("TimeStamp");
                RaisePropertyChanged("Index");
                RaisePropertyChanged("IndexString");
            }
        }

        public int InWorkWorkers
        {
            get { return _inWorkWorkers; }
            set
            {
                _inWorkWorkers = value;
                RaisePropertyChanged("InWorkWorkers");
                RaisePropertyChanged("Index");
                RaisePropertyChanged("IndexString");
            }
        }

        public int TimeTableWorkers
        {
            get { return _timeTableWorkers; }
            set
            {
                _timeTableWorkers = value;
                RaisePropertyChanged("TimeTableWorkers");
                RaisePropertyChanged("Index");
                RaisePropertyChanged("IndexString");
            }
        }

        public int InNetWorkWorkers
        {
            get { return _inNetWorkWorkers; }
            set
            {
                _inNetWorkWorkers = value;
                RaisePropertyChanged("InNetWorkWorkers");
                RaisePropertyChanged("Index");
                RaisePropertyChanged("IndexString");
            }
        }

        public LoadItemExt(LoadItemDTO loadItemDTO)
        {
            _inNetWorkWorkers = loadItemDTO.InNetWorkWorkers;
            _inWorkWorkers = loadItemDTO.InWorkWorkers;
            _timeStamp = loadItemDTO.TimeStamp;
            _timeTableWorkers = loadItemDTO.TimeTableWorkers;
        }

        public decimal Index
        {
            get
            {
                if (_inNetWorkWorkers == 0) return 0;
                decimal d = 0;
                if (_timeTableWorkers != 0)
                    d = ((decimal)_inWorkWorkers)/_timeTableWorkers*100;

                return Math.Round(d, 2);
            }
        }

        public string IndexString
        {
            get { return string.Format("{0}/{1}* 100", _inNetWorkWorkers, _timeTableWorkers); }
        }
    }
}