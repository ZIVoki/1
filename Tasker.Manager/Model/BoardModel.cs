﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Tasker.Tools.ViewModel;
using Tasker.Tools.ViewModel.Base;

namespace Tasker.Manager.Model
{
    public class BoardModel : ObservableObject
    {
        ObservableCollection<FolderView> _foldersView = new ObservableCollection<FolderView>();
        private ManagerFileView _selectedFileView;

        public ObservableCollection<FolderView> FoldersView
        {
            get { return _foldersView; }
            set
            {
                _foldersView = value;
                RaisePropertyChanged(() => FoldersView);
            }
        }

        public ManagerFileView SelectedFileView
        {
            get { return _selectedFileView; }
            set
            {
                _selectedFileView = value;
                RaisePropertyChanged(() => SelectedFileView);
            }
        }

        public BoardModel()
        {
            List<FolderView> folderViews = Repository.Folders.Where(f => f.Parent == null).ToList();
            if (folderViews != null)
            {
                foreach (var rootFolder in folderViews)
                {
                    _foldersView.Add(rootFolder);
                }
            }
        }

        public void UpdateSelectedItem()
        {
            ManagerFileView firstOrDefault = Repository.Files.FirstOrDefault(f => f.IsSelected);
            SelectedFileView = firstOrDefault;
        }

        public void OpenSelectedFile()
        {
            SelectedFileView.OpenFileCommand.Execute(this);
        }
    }

    public class TreeViewHelper
    {
        private static Dictionary<DependencyObject, TreeViewSelectedItemBehavior> behaviors = new Dictionary<DependencyObject, TreeViewSelectedItemBehavior>();

        public static object GetSelectedItem(DependencyObject obj)
        {
            return (object)obj.GetValue(SelectedItemProperty);
        }

        public static void SetSelectedItem(DependencyObject obj, object value)
        {
            obj.SetValue(SelectedItemProperty, value);
        }

        // Using a DependencyProperty as the backing store for SelectedItem.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedItemProperty =
            DependencyProperty.RegisterAttached("SelectedItem", typeof(object), typeof(TreeViewHelper), new UIPropertyMetadata(null, SelectedItemChanged));

        private static void SelectedItemChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if (!(obj is TreeView))
                return;

            if (!behaviors.ContainsKey(obj))
                behaviors.Add(obj, new TreeViewSelectedItemBehavior(obj as TreeView));

            TreeViewSelectedItemBehavior view = behaviors[obj];
            view.ChangeSelectedItem(e.NewValue);
        }

        private class TreeViewSelectedItemBehavior
        {
            TreeView view;
            public TreeViewSelectedItemBehavior(TreeView view)
            {
                this.view = view;
                view.SelectedItemChanged += (sender, e) => SetSelectedItem(view, e.NewValue);
            }

            internal void ChangeSelectedItem(object p)
            {
                TreeViewItem item = (TreeViewItem)view.ItemContainerGenerator.ContainerFromItem(p);
                item.IsSelected = true;
            }
        }
    }
}
