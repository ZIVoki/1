﻿using System;
using System.Drawing;
using System.IO;
using ExportToRTF;
using Tasker.Tools.ServiceReference;

namespace Tasker.Manager.Managers
{
    class DelayReportGenerator
    {
        /// <summary>
        /// Generate user report
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="fullFileName"></param>
        public static string GenerateDelayReportDoc(DateTime startDate, DateTime endDate, string fullFileName)
        {
            var fileName = new FileInfo(fullFileName).Name;

            var wordDocument = new WordDocument(WordDocumentFormat.A4);

            var docHeaderStyle = new Font("Calibri", 20, FontStyle.Bold);
            var colheaderStyle = new Font("Calibri", 12, FontStyle.Bold);
            var normalStyle = new Font("Calibri", 12, FontStyle.Bold);
            var smallStyle = new Font("Calibri", 8, FontStyle.Bold);
            const ContentAlignment centerAligment = ContentAlignment.TopCenter;
            const ContentAlignment leftAligment = ContentAlignment.TopLeft;

            AttendanceLogItemDTO[] lates = ServiceManager.Inst.ServiceChannel.GetLatesByDates(startDate, endDate);

            wordDocument.SetFont(docHeaderStyle);
            wordDocument.SetTextAlign(WordTextAlign.Center);
            wordDocument.WriteLine(string.Format("Список сотрудников допускавших опoздания в период с {0} по {1}", startDate.ToShortDateString(), endDate.ToShortDateString()));

            if (lates.Length > 0)
            {
                var table = wordDocument.NewTable(normalStyle, Color.Black, lates.Length + 1, 5, 10);
                table.Columns[0].SetContentAlignment(leftAligment);
                table.Columns[1].SetContentAlignment(centerAligment);
                table.Columns[2].SetContentAlignment(centerAligment);

                table.SetColumnsWidth(new[] { 60 });
                table.Rows[0].SetFont(colheaderStyle);

                table.Cell(0, 0).Write("ФИО");
                table.Cell(0, 1).Write("ДАТА");
                table.Cell(0, 2).Write("ВРЕМЯ НАЧАЛА СМЕНЫ");
                table.Cell(0, 3).Write("ВРЕМЯ ПРИХОДА");
                table.Cell(0, 4).Write("ВРЕМЯ ОПОЗДАНИЯ");

                string prevName = "";
                //int pos = 0;
                for (int i = 0; i < lates.Length; i++)
                {
                    var name = String.Format("{0} {1}", lates[i].User.LastName, lates[i].User.FirstName);
                    if (prevName != name)
                    {
                        table.Cell(i + 1, 0).Write(name);
                        prevName = name;
                        //if (pos != i)
                        //{
                        //    System.Console.WriteLine(String.Format("Merging {0}-{1}",pos, i));
                        //    table.CellRange(pos, 0, i, 0).MergeCells(); 
                        //}
                        //pos = i + 1; 
                    }

                    table.Cell(i + 1, 1).Write(lates[i].TimeTable.StartDate.ToString("dd.MM.yyyy"));

                    table.Cell(i + 1, 2).Write(lates[i].TimeTable.StartDate.ToString("HH:mm"));
                    table.Cell(i + 1, 2).SetFont(smallStyle);
                    table.Cell(i + 1, 2).Write("\n");
                    table.Cell(i + 1, 2).Write(lates[i].TimeTable.Shift.Title);

                    table.Cell(i + 1, 3).Write(lates[i].EventDateTime.ToString("HH:mm"));

                    TimeSpan ticks = new TimeSpan(lates[i].EventDateTime.Ticks - lates[i].TimeTable.StartDate.Ticks);
                    int hours = (int)ticks.TotalHours;
                    int minutes = (int)ticks.TotalMinutes % 60;

                    if (hours >= 8)
                    {
                        table.Cell(i + 1, 4).Write(String.Format("Отсутств."));
                    }
                    else
                    {
                        if (hours != 0)
                            table.Cell(i + 1, 4).Write(String.Format("{0} ч ", hours));
                        table.Cell(i + 1, 4).Write(String.Format("{0} мин", minutes));
                    }
                }

                table.SetBorders(Color.Black, 1);
                table.SaveToDocument(10000, 0);
            }
            else
            {
                wordDocument.SetFont(normalStyle);
                wordDocument.SetTextAlign(WordTextAlign.Left);
                wordDocument.WriteLine(string.Format("За вышеуказанный период опозданий допущено не было."));
            }

            string resultFile = ReportFileManager.CheckIfFileExist(fileName);
            wordDocument.SaveToFile(resultFile);

            return resultFile;
        }
        
        static public string GenearteFileName(DateTime startDate, DateTime endDate)
        {
            return String.Format("delay_report_{0}-{1}",
                                              startDate.ToString("dd.MM.yyyy"),
                                              endDate.ToString("dd.MM.yyyy"));
        }
    }
}