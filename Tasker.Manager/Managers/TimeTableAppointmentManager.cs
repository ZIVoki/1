﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Tasker.Manager.BackgroundWorkers;
using Tasker.Manager.Base;
using Tasker.Manager.Controls.TimeTable;
using Tasker.Manager.Controls.WeekCalendar;
using Tasker.Manager.Controls.WeekCalendar.Model;
using Tasker.Manager.Managers.Logger;
using Tasker.Manager.ServiceReference1;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Manager.Managers
{
    
    class TimeTableAppointmentManager: LoadManager
    {
        public static event EventHandler AppointmentEditedEvent;
        
        public static void DeleteTimeTableRedrawAppointment(int id, bool alertAll)
        {
            var timeTable = Repository.TimeTables.FirstOrDefault(a => a.Id == id);

            if(alertAll)
            {
                timeTable.Status = ConstantsStatuses.Deleted;
                var dto = timeTable.BuildDTO();
                ServiceManager.Inst.ServiceChannel.UpdateTimeTable(dto);
            }
            
            Repository.TimeTables.Remove(timeTable);

            var appointmentsToDelete = GetAppointmentsByTimeTableId(id);

            for (int i = 0; i < appointmentsToDelete.Count; i++)
            {
                var a = appointmentsToDelete[i];
                CalendarRepository.Appointments.Remove(a);
                appointmentsToDelete.Remove(a);
                i--;
            }

            InvokeAppointmentEditedEventHandler();
        }

        public static void EditTimeTableRedrawAppointment(Appointment appointment)
        {
            TimeTableView timeTable = Repository.TimeTables.FirstOrDefault(a => a.Id == appointment.TimeTableId);
            //open edit form here
            AddTimeTableItemWindow addTimeTableItemForm = new AddTimeTableItemWindow {CurrentTimeTable = timeTable};

            addTimeTableItemForm.ShowDialog();

            ApplyTimeTableEditRedrawAppointment(timeTable.IsResp, timeTable.StartDate, timeTable.EndDate, GetAppointmentsByTimeTableId(appointment.TimeTableId));
        }

        public static void ApplyTimeTableEditRedrawAppointment(bool isResp, DateTime startTime, DateTime endTime, List<Appointment> appointments = null)
        {
                if(appointments.Count == 1)
                {
                    if (startTime.Date == endTime.Date)
                    {
                        //edit one to one
                        appointments[0].StartTime = startTime;
                        appointments[0].EndTime = endTime;
                        appointments[0].IsResponsable = isResp;
                    }
                    else if (endTime.Date - TimeSpan.FromDays(1) == startTime.Date)
                    {
                        //edit one to two
                        appointments[0].StartTime = startTime;
                        appointments[0].EndTime = startTime.Date + TimeSpan.FromHours(23.99f);
                        appointments[0].IsResponsable = isResp;

                        Appointment appointment2 = new Appointment(appointments[0])
                                               {
                                                   StartTime = endTime.Date,
                                                   EndTime = endTime,
                                               };
                        CalendarRepository.Appointments.Add(appointment2);
                    }
                }
                else if (appointments.Count == 2)
                {
                    if (startTime.Date == endTime.Date)
                    {
                        //edit two to one
                        appointments[0].StartTime = startTime;
                        appointments[0].EndTime = endTime;
                        appointments[0].IsResponsable = isResp;

                        var app = CalendarRepository.Appointments.FirstOrDefault(
                            a =>
                            a.Id == appointments[1].Id && a.StartTime == appointments[1].StartTime &&
                            a.EndTime == appointments[1].EndTime);

                        CalendarRepository.Appointments.Remove(app);
                    }
                    else if (endTime.Date - TimeSpan.FromDays(1) == startTime.Date)
                    {
                        //edit two to two
                        appointments[0].StartTime = startTime;
                        appointments[0].EndTime = startTime.Date + TimeSpan.FromHours(23.99f);
                        appointments[0].IsResponsable = isResp;

                        appointments[1].StartTime = endTime.Date;
                        appointments[1].EndTime = endTime;
                        appointments[1].IsResponsable = isResp;
                    }
                }
          
            InvokeAppointmentEditedEventHandler();
        }

        public static List<Appointment> GetAppointmentsByTimeTableId(int id)
        {
            return CalendarRepository.Appointments.Where(a => a.TimeTableId == id).ToList();
        }

        public static void TimeTableCreateRedrawAppointment(TimeTableView ttv)
        {
            var isHighlight = false;
            if (CalendarRepository.UserIdHighlight == ttv.User.Id) isHighlight = true;
            //one square in one day
            if (ttv.StartDate.Date == ttv.EndDate.Date)
            {
                Appointment appointment = new Appointment(ttv, isHighlight);
                CalendarRepository.Appointments.Add(appointment);
            }
            else if (ttv.EndDate.Date - TimeSpan.FromDays(1) == ttv.StartDate.Date)
            {
                var appointment1 = new Appointment(ttv, isHighlight);

                var appointment2 = new Appointment(ttv, isHighlight, 10000);
                
                CalendarRepository.Appointments.Add(appointment1);
                CalendarRepository.Appointments.Add(appointment2);
            }
            InvokeAppointmentEditedEventHandler();
        }

        private static void InvokeAppointmentEditedEventHandler()
        {
            var handler = AppointmentEditedEvent;
            if (handler != null) handler(null, null);
        }

        public static void ChangeHighlightAppointments(int value)
        {
            foreach (var appointment in CalendarRepository.Appointments)
            {
                if (appointment.UserId == value)
                {
                    appointment.IsHighlight = true;
                }
                else
                {
                    appointment.IsHighlight = false;
                }
            }
            //InvokeAppointmentEditedEventHandler();
        }

        private static void PasteFromClipboardToThisWeek(object sender, DoWorkEventArgs doWorkEventArgs)
        {
            var loadId = InvokeLoadStartStatic(new LoadStartEventArgs("Вставка расписания"));
            var _appointmentClipboard = (List<TimeTableView>)((object[])doWorkEventArgs.Argument)[0];
            var currentMonday = (DateTime?)((object[])doWorkEventArgs.Argument)[1];
            
            if (_appointmentClipboard != null && currentMonday.HasValue)
            {
                var completedIds = new List<int>();
                foreach (var appointment in _appointmentClipboard)
                {
                    if (appointment != null)
                    {
                        if (!completedIds.Contains(appointment.Id))
                        {
                            completedIds.Add(appointment.Id);

                            if (appointment.User == null)
                            {
                                Log.Write(LogLevel.Warning, "Не удалось создать запись в календаре. Пользователь отсутствует.");
                            }
                            else
                            {
                                var exist = Repository.Users.FirstOrDefault(a => a.Id == appointment.User.Id);
                                if (exist == null)
                                {
                                    Log.Write(LogLevel.Warning, string.Format("Не удалось создать запись в календаре. Пользователь {0} отсутствует.", appointment.User.Title));
                                }
                            }

                            if (appointment.Shift == null)
                            {
                                Log.Write(LogLevel.Warning, "Не удалось создать запись в календаре. Смена отсутствует.");
                            }
                            else
                            {
                                var exist = Repository.Shifts.FirstOrDefault(a => a.Id == appointment.Shift.Id);
                                if (exist == null)
                                {
                                    Log.Write(LogLevel.Warning, string.Format("Не удалось создать запись в календаре. Смена {0} отсутствует.", appointment.Shift.Title));
                                }
                            }
                            
                            var currentTimeTable = new TimeTableView();

                            //get time of day
                            var startTimeSpan = appointment.StartDate.TimeOfDay;
                            var endTimeSpan = appointment.EndDate.TimeOfDay;

                            //get new dates
                            var curSunday = (currentMonday + TimeSpan.FromDays(6)).Value.Date;
                            var originalStartDate = appointment.StartDate.Date;

                            DateTime? newStartDate = null;
                            DateTime? newEndDate = null;
                            if (originalStartDate <= curSunday && originalStartDate >= currentMonday.Value.Date)
                            {
                                //Paste into the same week that was copied
                                newStartDate = originalStartDate;
                                newEndDate = appointment.EndDate.Date;
                            }
                            else if (originalStartDate > curSunday)
                            {
                                //week step counter
                                var weekStepCount = 0;
                                //paste into past
                                while (originalStartDate > curSunday)
                                {
                                    weekStepCount++;
                                    //go back for week
                                    originalStartDate = originalStartDate - TimeSpan.FromDays(7);
                                }
                                newStartDate = originalStartDate.Date;
                                newEndDate = appointment.EndDate.Date - TimeSpan.FromDays((7 * weekStepCount));
                            }
                            else if (originalStartDate < currentMonday.Value.Date)
                            {
                                //paste into future
                                //week step counter
                                var weekStepCount = 0;
                                //paste into past
                                while (originalStartDate < currentMonday.Value.Date)
                                {
                                    weekStepCount++;
                                    //go back for week
                                    originalStartDate = originalStartDate + TimeSpan.FromDays(7);
                                }
                                newStartDate = originalStartDate.Date;
                                newEndDate = appointment.EndDate.Date + TimeSpan.FromDays((7 * weekStepCount));
                            }

                            //set dates and times
                            if (newStartDate.HasValue && newEndDate.HasValue)
                            {
                                //copy all
                                currentTimeTable.User = appointment.User;
                                currentTimeTable.Status = appointment.Status;
                                currentTimeTable.Shift = appointment.Shift;
                                currentTimeTable.StartDate = newStartDate.Value.Date.Add(startTimeSpan);
                                currentTimeTable.EndDate = newEndDate.Value.Date.Add(endTimeSpan);
                                currentTimeTable.IsResp = appointment.IsResp;
                                // clear lunch 
                                currentTimeTable.FirstLunchEnd = null;
                                currentTimeTable.FirstLunchStart = null;
                                currentTimeTable.SecondLunchEnd = null;
                                currentTimeTable.SecondLunchStart = null;
                            }
                            else
                            {
                                continue;
                            }

                            /*if (currentTimeTable.EndDate <= currentTimeTable.StartDate)
                                currentTimeTable.EndDate = currentTimeTable.EndDate.AddDays(1);*/

                            var timeTableDto = currentTimeTable.BuildDTO();

                            currentTimeTable.Id = ServiceManager.Inst.ServiceChannel.AddTimeTable(timeTableDto);
                            if (currentTimeTable.Id > 0)
                            {
                                Repository.TimeTables.Add(currentTimeTable);
                                string message = String.Format("Добавлени новая ячейка в расписании пользователя {0} на {1}.",
                                                                timeTableDto.User.LastName, timeTableDto.StartDate.ToString("dd.MM.yyyy"));
                                Log.Write(LogLevel.Debug, message);
                            }
                            else
                                Log.Write(LogLevel.Error, "Ошибка при добавлении новой ячейки в рассписании.");
                        }
                    }

                }
            }
            InvokeLoadEndStatic(loadId);
        }
        public static void PasteFromClipboardToThisWeek(List<TimeTableView> clipboard, DateTime? monday, RunWorkerCompletedEventHandler completedWorkEventHandler = null)
        {
            var backgroundTask = new BackgroundTask(PasteFromClipboardToThisWeek, completedWorkEventHandler);
            backgroundTask.Run(new object[]{clipboard,monday});
        }

        protected override void UpdateAction(IManagerInterface serviceChannel)
        {
            throw new NotImplementedException();
        }

        protected override void LoadAction(IManagerInterface serviceChannel)
        {
            throw new NotImplementedException();
        }
    }
}
