﻿namespace Tasker.Manager.Managers.Logger
{
    public enum LogLevel
    {
        Debug = 0,
        Error = 1,
        Fatal = 2,
        Info = 3,
        Warning = 4
    }
}