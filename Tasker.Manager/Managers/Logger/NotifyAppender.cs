﻿using System.ComponentModel;
using System.Globalization;
using System.IO;
using Tasker.Tools.Extensions;

namespace Tasker.Manager.Managers.Logger
{
    public class NotifyAppender : log4net.Appender.AppenderSkeleton, INotifyPropertyChanged
    {
        #region Members and events

        private static BindableCollection<log4net.Core.LoggingEvent> _notification = new BindableCollection<log4net.Core.LoggingEvent>();
        private event PropertyChangedEventHandler _propertyChanged;

        public event PropertyChangedEventHandler PropertyChanged
        {
            add { _propertyChanged += value; }
            remove { _propertyChanged -= value; }
        }

        #endregion

        /// <summary>
        /// Get or set the notification message.
        /// </summary>
        public BindableCollection<log4net.Core.LoggingEvent> Notification
        {
            get
            {
                return _notification;
            }
            set
            {
                if (_notification != value)
                {
                    _notification = value;
                    OnChange();
                }
            }
        }

        /// <summary>
        /// Raise the change notification.
        /// </summary>
        private void OnChange()
        {
            PropertyChangedEventHandler handler = _propertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(string.Empty));
            }
        }

        /// <summary>
        /// Get a reference to the log instance.
        /// </summary>
        public NotifyAppender Appender
        {
            get
            {
                return Log.Appender;
            }

        }

        /// <summary>
        /// Append the log information to the notification.
        /// </summary>
        /// <param name="loggingEvent">The log event.</param>
        protected override void Append(log4net.Core.LoggingEvent loggingEvent)
        {
            var writer = new StringWriter(CultureInfo.InvariantCulture);
            Layout.Format(writer, loggingEvent);
            Notification.Add(loggingEvent);
        }
    }
}