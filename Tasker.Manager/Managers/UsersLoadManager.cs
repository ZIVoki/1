﻿using Tasker.Manager.Base;
using Tasker.Manager.ServiceReference1;
using Tasker.Tools.Managers;
using Tasker.Tools.ServiceReference;
using System.Linq;

namespace Tasker.Manager.Managers
{
    class UsersLoadManager: LoadManager
    {
        protected override void UpdateAction(IManagerInterface serviceChannel)
        {
            UserDTO[] userDtos = serviceChannel.GetWorkers();

            foreach (var userDto in userDtos.OrderBy(u => u.LastName))
            {
                UserManager.UserUpdate(userDto, Repository.Users);
                // load user task types
//                UserTaskTypeDTO[] userTaskTypesByUser = serviceChannel.GetUserTaskTypesByUser(userDto.Id);
//                foreach (var userTaskTypeDto in userTaskTypesByUser)
//                {
//                    UserTaskTypeManager.UserTaskTypeUpdate(userTaskTypeDto, Repository.Users, Repository.TaskTypes);
//                }
            }
            
        }

        protected override void LoadAction(IManagerInterface serviceChannel)
        {
            UserDTO[] userDtos = serviceChannel.GetWorkers();

            foreach (var userDto in userDtos.OrderBy(u => u.LastName))
            {
                UserManager.GetUser(userDto, Repository.Users);
                // load user task types
//                UserTaskTypeDTO[] userTaskTypesByUser = serviceChannel.GetUserTaskTypesByUser(userDto.Id);
//                foreach (var userTaskTypeDto in userTaskTypesByUser)
//                {
//                    UserTaskTypeManager.GetUserTaskType(userTaskTypeDto, Repository.Users, Repository.TaskTypes);
//                }
            }
        }
    }
}
