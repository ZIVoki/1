﻿using Tasker.Manager.Base;
using Tasker.Manager.ServiceReference1;
using Tasker.Tools.Managers;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Manager.Managers
{
    class TasksLoadManager : LoadManager
    {
        public static void UpdateTaskList(TaskDTO taskDto)
        {
            if (taskDto == null) return;
            TaskManager.GetTask(taskDto,
                                Repository.Tasks,
                                Repository.Customers,
                                Repository.TaskTypes,
                                Repository.Channels,
                                Repository.Users,
                                Repository.HighlightSettings.GetTaskTypeHighlightColor(taskDto.TaskType),
                                Repository.HighlightSettings.GetCustomerHighlightColor(taskDto.Customer));
        }

        protected override void UpdateAction(IManagerInterface serviceChannel)
        {
            int loadId = InvokeLoadStart(new LoadStartEventArgs("Обновление задач."));
            TaskDTO[] taskDtos = (Repository.LoadSettings.LoadAge == 0) ? ServiceManager.Inst.ServiceChannel.GetTasks() :
                ServiceManager.Inst.ServiceChannel.GetTasksByDates(System.DateTime.Now.AddDays((-1) * Repository.LoadSettings.LoadAge), System.DateTime.Now);
            if (taskDtos == null) return;

            foreach (var taskDto in taskDtos)
            {
                TaskView taskView = TaskManager.GetTask(taskDto,
                                                        Repository.Tasks,
                                                        Repository.Customers,
                                                        Repository.TaskTypes,
                                                        Repository.Channels,
                                                        Repository.Users,
                                                        Repository.HighlightSettings.GetTaskTypeHighlightColor(taskDto.TaskType.Id),
                                                        Repository.HighlightSettings.GetCustomerHighlightColor(taskDto.Customer.Id));
                if (taskView != null)
                {
                    taskView = TaskManager.UpdateTask(taskDto,
                                                      Repository.Customers,
                                                      Repository.TaskTypes,
                                                      Repository.Users,
                                                      Repository.Channels,
                                                      null,
                                                      Repository.Tasks,
                                                      Repository.HighlightSettings.GetTaskTypeHighlightColor(taskDto.TaskType.Id),
                                                      Repository.HighlightSettings.GetCustomerHighlightColor(taskDto.Customer.Id));
                }

                LoadTaskFiles(taskView);
            }
            InvokeLoadEnd(loadId);
            var loadId2 = InvokeLoadStart(new LoadStartEventArgs("Загрузка истории сообщений."));
            LoadUnreadMessages();
            InvokeLoadEnd(loadId2);
        }

        protected override void LoadAction(IManagerInterface serviceChannel)
        {
            int loadId = InvokeLoadStart(new LoadStartEventArgs("Загрузка задач."));

            TaskDTO[] taskDtos = (Repository.LoadSettings.LoadAge == 0) ? ServiceManager.Inst.ServiceChannel.GetTasks() :
                ServiceManager.Inst.ServiceChannel.GetTasksByDates(System.DateTime.Now.AddDays((-1) * Repository.LoadSettings.LoadAge), System.DateTime.Now);
            if (taskDtos == null) return;

            foreach (var taskDto in taskDtos)
            {
                var task = TaskManager.CreateTask(taskDto,
                                        Repository.Customers,
                                        Repository.TaskTypes,
                                        Repository.Users,
                                        Repository.Channels,
                                        null,
                                        Repository.Tasks,
                                        Repository.HighlightSettings.GetTaskTypeHighlightColor(taskDto.TaskType.Id),
                                        Repository.HighlightSettings.GetCustomerHighlightColor(taskDto.Customer.Id));
                LoadTaskFiles(task);
            }

            taskDtos = ServiceManager.Inst.ServiceChannel.GetActiveTasks();
            if (taskDtos != null)
            {
                foreach (var taskDto in taskDtos)
                {
                    var task = TaskManager.GetTask(taskDto,
                                                Repository.Tasks,
                                                Repository.Customers,
                                                Repository.TaskTypes,
                                                Repository.Channels,
                                                Repository.Users,
                                                Repository.HighlightSettings.GetTaskTypeHighlightColor(taskDto.TaskType.Id),
                                                Repository.HighlightSettings.GetCustomerHighlightColor(taskDto.Customer.Id));
                    LoadTaskFiles(task);
                }

            }

            InvokeLoadEnd(loadId);
            var loadId2 = InvokeLoadStart(new LoadStartEventArgs("Загрузка истории сообщений."));
            LoadUnreadMessages();
            InvokeLoadEnd(loadId2);
        }

        public static void LoadUnreadMessages()
        {
            MessageDTO[] unreadMessages = ServiceManager.Inst.ServiceChannel.GetUnreadMessages();

            foreach (MessageDTO messageDto in unreadMessages)
            {
                TaskView taskView = TaskManager.GetTask(messageDto.Task, Repository.Tasks, Repository.Customers,
                                                        Repository.TaskTypes, Repository.Channels,
                                                        Repository.Users);

                MessageManager.GetMessage(messageDto, taskView, taskView.Messages.Messages, Repository.Users, Repository.LoginUser.Id);
            }
        }

        public static void LoadTaskFiles(TaskView task)
        {
            if (task == null) return;
            AttachFileDTO[] files = ServiceManager.Inst.ServiceChannel.GetFilesData(task.Id);
            foreach (var fileDTO in files)
            {
                TaskFileManager.GetFileData(fileDTO, task);
            }
        }

        public void LoadArchive(int days = 0)
        {
            var loadId = InvokeLoadStart(new LoadStartEventArgs("Загрузка архивных задач."));
            TaskDTO[] taskDtos = (days == 0) ? ServiceManager.Inst.ServiceChannel.GetTasks() :
                ServiceManager.Inst.ServiceChannel.GetTasksByDates(System.DateTime.Now.AddDays((-1) * days), System.DateTime.Now);
            if (taskDtos == null) return;

            foreach (var taskDto in taskDtos)
            {
                var task = TaskManager.GetTask(taskDto,
                                                Repository.Tasks,
                                                Repository.Customers,
                                                Repository.TaskTypes,
                                                Repository.Channels,
                                                Repository.Users,
                                                Repository.HighlightSettings.GetTaskTypeHighlightColor(taskDto.TaskType.Id),
                                                Repository.HighlightSettings.GetCustomerHighlightColor(taskDto.Customer.Id));
                LoadTaskFiles(task);
            }
            InvokeLoadEnd(loadId);
            var loadId2 = InvokeLoadStart(new LoadStartEventArgs("Загрузка истории сообщений."));
            LoadUnreadMessages();
            InvokeLoadEnd(loadId2);
        }
    }
}
