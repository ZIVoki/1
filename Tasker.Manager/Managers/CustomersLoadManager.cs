﻿using Tasker.Manager.Base;
using Tasker.Manager.ServiceReference1;
using Tasker.Tools.Extensions;
using Tasker.Tools.Managers;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;
using System.Linq;

namespace Tasker.Manager.Managers
{
    class CustomersLoadManager: LoadManager
    {
        protected override void UpdateAction(IManagerInterface serviceChannel)
        {
            CustomerDTO[] customerDtos = serviceChannel.GetCustomers();

            foreach (var customerDto in customerDtos.OrderBy(t => t.Title))
            {
                CustomerManager.CustomerUpdate(Repository.Customers, Repository.Channels, customerDto);
            }
        }

        protected override void LoadAction(IManagerInterface serviceChannel)
        {
            CustomerDTO[] customerDtos = serviceChannel.GetCustomers();
            
            foreach (var customerDto in customerDtos.OrderBy(t => t.Title))
            {
                // getCustomer allow to have not any dublicates
                GetCustomerWithHightlight(customerDto, Repository.Customers);
            }
        }

        public static CustomerView GetCustomerWithHightlight(CustomerDTO dto, UniqBindableCollection<CustomerView> repository)
        {
            return CustomerManager.GetCustomer(dto, repository, Repository.Channels, Repository.HighlightSettings.GetCustomerHighlightColor(dto.Id));
        }
    }
}
