﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using ExportToRTF;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Manager.Managers
{
    class WordReportGenerator
    {
        /// <summary>
        /// Generate user report
        /// </summary>
        /// <param name="users"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="fullFileName"></param>
        public static string GenerateUserReport(List<UserView> users, DateTime startDate, DateTime endDate, string fullFileName)
        {
            var fileName = new FileInfo(fullFileName).Name;

            var wordDocument = new WordDocument(WordDocumentFormat.A4);

            var docHeaderStyle = new Font("Calibri", 14, FontStyle.Bold);
            var colheaderStyle = new Font("Calibri", 16, FontStyle.Bold);
            var normalStyle = new Font("Calibri", 16, FontStyle.Bold);
            var smallStyle = new Font("Calibri", 8, FontStyle.Bold);
            const ContentAlignment centerAligment = ContentAlignment.TopCenter;
            const ContentAlignment leftAligment = ContentAlignment.TopLeft;

            wordDocument.SetFont(docHeaderStyle);
            wordDocument.SetTextAlign(WordTextAlign.Center);
            wordDocument.WriteLine(string.Format("Планируемая и фактическая загрузка работников студии за период с {0} по {1}", startDate.ToShortDateString(), endDate.ToShortDateString()));

            var compCreatDate = new Comparison<UserView>((a, b) => a.LastName.CompareTo(b.LastName));
            users.Sort(compCreatDate);

            // начало дня
            startDate = startDate.Date;
            // конец дня
            endDate = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59);

            var data = new string[users.Count][];
            for (int i = 0; i < users.Count; i++)
            {
                var user = users[i];
                //get users closed tasks by dates
                var tasks = ServiceManager.Inst.ServiceChannel.GetClosedTasksByWorkerAndDate(user.Id, startDate, endDate);
                var row = new string[4];

                if (!string.IsNullOrEmpty(user.FirstName))
                    row[0] = string.Format("{0} {1}", user.LastName, user.FirstName);
                else
                    row[0] = user.LastName;

                var totalPlan = new TimeSpan(0);
                var totalReal = new TimeSpan(0);

                row[3] = tasks.Count().ToString();

                foreach (var taskDto in tasks)
                {
                    var statistics = ServiceManager.Inst.ServiceChannel.GetTaskStatistic(taskDto.Id);

                    foreach (var stat in statistics)
                    {
                        if (stat.TaskState == ConstantsStatuses.InProcess)
                        {
                            if (stat.Worker != null && stat.Worker.Id == user.Id)
                            {
                                totalReal += stat.Duration;
                            }
                        }
                    }

                    totalPlan += taskDto.PlannedTime;
                }

                row[1] = string.Format("{0:00}:{1:00}:{2:00}", Math.Floor(totalPlan.TotalHours), totalPlan.Minutes, totalPlan.Seconds);
                row[2] = string.Format("{0:00}:{1:00}:{2:00}", Math.Floor(totalReal.TotalHours), totalReal.Minutes, totalReal.Seconds);

                data[i] = row;
            }

            var table = wordDocument.NewTable(normalStyle, Color.Black, (data.Length+1), 3, 10);

            table.Columns[0].SetContentAlignment(leftAligment);
            table.Columns[1].SetContentAlignment(centerAligment);
            table.Columns[2].SetContentAlignment(centerAligment);

            table.SetColumnsWidth(new int[] { 70 });
            table.Rows[0].SetFont(colheaderStyle);

            table.Cell(0, 0).Write("ФИО");
            table.Cell(0, 1).Write("ПЛАН");
            table.Cell(0, 2).Write("ФАКТ");

            for (int j = 0; j < data.Length; j++)
            {
                var row = data[j];
                var tableRowIndex = (j + 1);

                table.Cell(tableRowIndex, 0).WriteLine(row[0]);
                table.Rows[tableRowIndex].SetFont(smallStyle);
                table.Cell(tableRowIndex, 0).Write(string.Format("({0} задач{1})", row[3], getNumEnding(row[3])));

                table.Rows[tableRowIndex].SetFont(normalStyle);
                table.Cell(tableRowIndex, 1).Write(row[1]);
                table.Cell(tableRowIndex, 2).Write(row[2]);
            }

            table.SetBorders(Color.Black, 1);
            table.SaveToDocument(10000, 0);
            
            string resultFile = ReportFileManager.CheckIfFileExist(fileName);
            wordDocument.SaveToFile(resultFile);

            return resultFile;
        }

        private static string getNumEnding(string s)
        {
            //it should be int
            int num = int.Parse(s);
            if (num >= 11 && num <= 14) return string.Empty;

            var lastSymbol = s.Substring(s.Length - 1);
            var lastNum = int.Parse(lastSymbol);

            if (lastNum == 1) return "а";
            if (lastNum >= 2 && lastNum <= 4) return "и";
            return string.Empty;
        }
    }
}
