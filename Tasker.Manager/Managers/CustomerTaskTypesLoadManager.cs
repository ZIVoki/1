using Tasker.Manager.Base;
using Tasker.Manager.Objects;
using Tasker.Manager.ServiceReference1;

namespace Tasker.Manager.Managers
{
    class 
        CustomerTaskTypesLoadManager: LoadManager
    {
        protected override void UpdateAction(IManagerInterface serviceChannel)
        {
            // todo implement
        }

        protected override void LoadAction(IManagerInterface serviceChannel)
        {
            CustomerTaskTypeDTO[] customerTaskTypeDtos = serviceChannel.GetCustomersTaskTypes();

            foreach (var customerTaskTypeDto in customerTaskTypeDtos)
            {
                CustomerTaskTypeManager.SetNewCusomerTaskType(customerTaskTypeDto);
            }
        }
    }
}