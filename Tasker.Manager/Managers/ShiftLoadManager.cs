﻿using System.ComponentModel;
using Tasker.Manager.Base;
using Tasker.Manager.ServiceReference1;
using Tasker.Tools.ServiceReference;
using System.Linq;

namespace Tasker.Manager.Managers
{
    class ShiftLoadManager: LoadManager
    {
/*        private BackgroundWorker _shiftListUpdateBackgroundWorker;

        public ShiftLoadManager()
        {
            ServiceManager.Callback.ShiftAddedEvent += ManagerCallbackShiftAddedEvent;
        }

        void ManagerCallbackShiftAddedEvent(object sender, ShiftAddedEventArgs e)
        {
            if (_shiftListUpdateBackgroundWorker == null)
            {
                _shiftListUpdateBackgroundWorker = new BackgroundWorker();
                _shiftListUpdateBackgroundWorker.DoWork += ShiftListUpdateBackgroundWorkerDoWork;
            }
            _shiftListUpdateBackgroundWorker.RunWorkerAsync(e);
        }

        static void ShiftListUpdateBackgroundWorkerDoWork(object sender, DoWorkEventArgs e)
        {
            ShiftDTO shiftDto = (ShiftDTO) e.Argument;
            UpdateShiftList(shiftDto);
        }*/

        protected override void UpdateAction(IManagerInterface serviceChannel)
        {
            ShiftDTO[] shiftDtos = serviceChannel.GetShifts();

            foreach (var shiftDto in shiftDtos.OrderBy(t => t.StartTime))
            {
                Tools.Managers.ShiftManager.ShiftUpdate(shiftDto, Repository.Shifts);
            }
        }

        protected override void LoadAction(IManagerInterface serviceChannel)
        {
            ShiftDTO[] shiftDtos = serviceChannel.GetShifts();

            foreach (var shiftDto in shiftDtos.OrderBy(t => t.StartTime))
            {
                Tools.Managers.ShiftManager.GetShift(shiftDto, Repository.Shifts);
            }
        }

        public static void UpdateShiftList(ShiftDTO shiftDto)
        {
            Tools.Managers.ShiftManager.GetShift(shiftDto, Repository.Shifts);
        }
    }
}