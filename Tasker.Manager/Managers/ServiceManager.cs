﻿using System;
using Tasker.Manager.Managers.Logger;
using Tasker.Manager.ServiceReference1;
using Tasker.Tools.Extensions;
using Tasker.Tools.Managers;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Manager.Managers
{
    internal class ServiceManager : ServiceManagerBase<IManagerInterfaceChannel, ManagerCallback>
    {
        protected ServiceManager()
        {
#if DEBUG
            PingTimeOut = TimeSpan.FromSeconds(10);
#endif
            NetTcpConnectionMask = "net.tcp://{0}/manager";
            EndpointConfigurationName = "managerEndPoint";
        }

        public override void LoadData(UserView currentUser)
        {
            Repository.LoginUser = currentUser;
            ClearBlockedResourcesInfo();
            Repository.LoadData();
        }

        private static ServiceManager _inst;
        public static ServiceManager Inst
        {
            get
            {
                if (_inst == null)
                    _inst = new ServiceManager();
                return _inst;
            }
        }

        /// <summary>
        /// clear info about blocked resources
        /// </summary>
        private static void ClearBlockedResourcesInfo()
        {
            foreach (var taskView in Repository.Tasks)
            {
                taskView.WorkState = Tools.Constants.TaskWorkState.Available;
            }
            foreach (var userView in Repository.Users)
            {
                userView.UserWorkState = Tools.Constants.UserWorkState.Free;
            }
        }

        protected override void Ping()
        {
            _serviceChannel.Ping(DateTime.Now);
        }

        protected override void Logoff()
        {
            _serviceChannel.Logoff();
        }

        public override bool DoesUserExist()
        {
            return Repository.LoginUser != null;
        }

        protected override bool ClientTypeCheck(LoginResultDTO loginResult)
        {
            return loginResult.Role != ConstantsRoles.Administrator && loginResult.Role != ConstantsRoles.Manager;
        }

        protected override void LogInfo(string message)
        {
            Log.Write(LogLevel.Info, message);
        }

        protected override void LogError(string message)
        {
            Log.Write(LogLevel.Error, message);
        }

        protected override void LogErrorEx(string message, Exception e)
        {
            Log.Write(LogLevel.Error, message, e);
        }

        protected override void LogFatal(string message)
        {
            Log.Write(LogLevel.Fatal, message);
        }

        protected override void LogFatal(string message, Exception e)
        {
            Log.Write(LogLevel.Fatal, message, e);
        }

        protected override void DisposeCallback()
        {
            _callback.Do(t => t.Dispose());
        }

        protected override LoginResultDTO Login(string login, string password)
        {
            return _serviceChannel.Login(login, password);
        }

    }
}
