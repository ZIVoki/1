﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Tasker.Manager.Controls;
using Tasker.Tools.CustomEventArgs;
using Tasker.Tools.Managers;
using Tasker.Tools.ViewModel;

namespace Tasker.Manager.Managers
{
    class ChatWindowsManager
    {
        public static event ArgumentContainsDelegate ChatWindowClosingEventHandler;
        public static void ShowChatWindow(TaskView task, List<ChatWindow> openedChatWindows)
        {
            var existChatWindow = openedChatWindows.FirstOrDefault(a => ((TaskView)a.DataContext).Id == task.Id);
            if (existChatWindow != null)
            {
                existChatWindow.WindowState = System.Windows.WindowState.Normal;
                existChatWindow.Activate();
            }
            else
            {
                if(task.Messages.Messages.Count == 0)
                {
                    var bw = new BackgroundWorker();
                    bw.DoWork += LoadTaskMessages;
                    bw.RunWorkerAsync(task);
                }
                ChatWindow chatWindow = new ChatWindow { DataContext = task };
                
                openedChatWindows.Add(chatWindow);
                chatWindow.Closing += ChatWindowClosed;
                chatWindow.Show();
            }
        }

        private static void LoadTaskMessages(object sender, DoWorkEventArgs e)
        {
            var task = (TaskView)e.Argument;
            var messagesDTO = ServiceManager.Inst.ServiceChannel.GetMessages(task.Id);
            foreach (var messageDTO in messagesDTO)
            {
                MessageManager.GetMessage(messageDTO, task, task.Messages.Messages, Repository.Users, Repository.LoginUser.Id, true);
            }
        }

        private static void ChatWindowClosed(object sender, CancelEventArgs e)
        {
            InvokeSystemEventCome((ChatWindow)sender);
        }

        public static void InvokeSystemEventCome(ChatWindow chatWindow)
        {
            var handler = ChatWindowClosingEventHandler;
            if (handler != null) handler(null, new ContentEventArgs(chatWindow));
        }
    }
}
