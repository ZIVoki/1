using Tasker.Manager.Base;
using Tasker.Manager.Model;
using Tasker.Manager.Objects;
using Tasker.Manager.ServiceReference1;
using Tasker.Tools.Managers;
using Tasker.Tools.ServiceReference;
using System.Linq;

namespace Tasker.Manager.Managers
{
    class BoardLoadManager: LoadManager
    {
        protected override void UpdateAction(IManagerInterface serviceChannel)
        {
            FileVersionDTO[] fileVersionDtos = ServiceManager.Inst.ServiceChannel.GetBoardFiles();
            if (fileVersionDtos != null)
            {
                // ��������� �� �������� ��������� ������ �����

                foreach (var fileVersionDto in fileVersionDtos)
                {
                    FileManager<ManagerFileView>.UpdateFileView(fileVersionDto, Repository.Files, Repository.Folders);
                    FileManager<ManagerFileView>.SetUsersReadFile(fileVersionDto, Repository.Files, Repository.Folders, Repository.Users);
                }
            }
        }

        protected override void LoadAction(IManagerInterface serviceChannel)
        {
            int loadId = InvokeLoadStart(new LoadStartEventArgs("�������� �����."));

            FileVersionDTO[] fileVersionDtos = ServiceManager.Inst.ServiceChannel.GetBoardFiles();
            if (fileVersionDtos != null)
            {
                FolderManager.LoadBoardStaticStructure(Repository.Folders);
                foreach (var fileVersionDto in fileVersionDtos.OrderBy(t => t.File.Title))
                {
                    FileManager<ManagerFileView>.SetUsersReadFile(fileVersionDto, Repository.Files, Repository.Folders, Repository.Users);
                }
            }

            InvokeLoadEnd(loadId);
        }
    }
}