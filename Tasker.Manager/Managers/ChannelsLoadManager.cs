using Tasker.Manager.Base;
using Tasker.Manager.ServiceReference1;
using Tasker.Tools.Managers;
using Tasker.Tools.ServiceReference;
using System.Linq;

namespace Tasker.Manager.Managers
{
    class ChannelsLoadManager:LoadManager
    {
        protected override void UpdateAction(IManagerInterface serviceChannel)
        {
            ChannelDTO[] customerDtos = serviceChannel.GetChannels();

            foreach (var customerDto in customerDtos.OrderBy(t => t.Title))
            {
                // getCustomer allow to have not any dublicates
                ChannelsManager.ChannelUpdate(customerDto, Repository.Channels);
            }
        }

        protected override void LoadAction(IManagerInterface serviceChannel)
        {
            ChannelDTO[] customerDtos = serviceChannel.GetChannels();

            foreach (var customerDto in customerDtos.OrderBy(t => t.Title))
            {
                // getCustomer allow to have not any dublicates
                ChannelsManager.GetChannel(customerDto, Repository.Channels);
            }
        }
    }
}