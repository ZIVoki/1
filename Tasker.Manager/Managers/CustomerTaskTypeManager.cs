using System.Diagnostics;
using Tasker.Manager.ServiceReference1;
using Tasker.Tools.Managers;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;
using System.Linq;
using Tasker.Manager.Managers.Logger;

namespace Tasker.Manager.Objects
{
    public class CustomerTaskTypeManager
    {
        /// <summary>
        /// set new item if once not exist
        /// </summary>
        /// <param name="dto"></param>
        public static void SetNewCusomerTaskType(CustomerTaskTypeDTO dto)
        {
            CustomerTaskTypeDTO taskTypeDto;
//            Debug.WriteLine(string.Format("Checking if customerTasktype {0} exist", dto.Id));
            taskTypeDto = null;
            foreach (CustomerTaskTypeDTO t in Repository.CustomerTaskTypes)
            {
                if (t.Id == dto.Id)
                {
                    taskTypeDto = t;
                    break;
                }
            }


            if (taskTypeDto == null)
            {
//                Debug.WriteLine(string.Format("Creating customerTasktype {0}", dto.Id));
                CreateCustomerTaskTypeView(dto);
            }
        }

        private static void CreateCustomerTaskTypeView(CustomerTaskTypeDTO dto)
        {
            // add to repository
//            Debug.WriteLine(string.Format("Adding to repository customerTasktype {0}", dto.Id));
            Repository.CustomerTaskTypes.Add(dto);
            

            CustomerView customerView = CustomerManager.GetCustomer(dto.Customer, Repository.Customers,
                                                                    Repository.Channels,
                                                                    Repository.HighlightSettings.GetCustomerHighlightColor(dto.Customer.Id));

            TaskTypeView taskTypeView = TaskTypeManager.GetTaskType(dto.TaskType, Repository.TaskTypes,
                                                                    Repository.HighlightSettings.GetTaskTypeHighlightColor(dto.TaskType.Id));

            if (customerView == null || taskTypeView == null)
            {
                Log.Write(LogLevel.Warning, "Can not find tasktype customerView or taskTypeView.");
                return;
            }

            customerView.TaskTypeViews.Add(taskTypeView);
        }

        public static void UpdateCustomerTaskType(CustomerTaskTypeDTO customerTaskTypeDto)
        {
            CustomerTaskTypeDTO taskTypeDto;
            taskTypeDto = Repository.CustomerTaskTypes.FirstOrDefault(t => t.Id == customerTaskTypeDto.Id);

            if (taskTypeDto == null)
                CreateCustomerTaskTypeView(customerTaskTypeDto);
            else
            {
                if (customerTaskTypeDto.Status == ConstantsStatuses.Deleted)
                {
                    Repository.CustomerTaskTypes.Remove(taskTypeDto);

                    CustomerView customerView = CustomerManager.GetCustomer(customerTaskTypeDto.Customer,
                                                                            Repository.Customers,
                                                                            Repository.Channels,
                                                                            Repository.HighlightSettings.
                                                                                GetCustomerHighlightColor(
                                                                                    customerTaskTypeDto.Customer.Id));

                    TaskTypeView taskTypeView = TaskTypeManager.GetTaskType(customerTaskTypeDto.TaskType,
                                                                            Repository.TaskTypes,
                                                                            Repository.HighlightSettings.
                                                                                GetTaskTypeHighlightColor(
                                                                                    customerTaskTypeDto.TaskType.Id));

                    
                    if (customerView.TaskTypeViews.Contains(taskTypeView))
                        customerView.TaskTypeViews.Remove(taskTypeView);
                }
            }
        }
    }
}