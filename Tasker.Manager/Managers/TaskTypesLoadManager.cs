﻿using Tasker.Manager.ServiceReference1;
using Tasker.Tools.Extensions;
using Tasker.Tools.Managers;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;
using System.Linq;

namespace Tasker.Manager.Managers
{
    class TaskTypesLoadManager: Base.LoadManager
    {
        protected override void UpdateAction(IManagerInterface serviceChannel)
        {
            TaskTypeDTO[] taskTypeDtos = serviceChannel.GetTaskTypes();
            foreach (var taskTypeDto in taskTypeDtos.OrderBy(tt => tt.Title))
            {
                TaskTypeManager.UpdateTaskType(Repository.TaskTypes, taskTypeDto);
            }
        }

        protected override void LoadAction(IManagerInterface serviceChannel)
        {
            TaskTypeDTO[] taskTypeDtos = serviceChannel.GetTaskTypes();
            foreach (var taskTypeDto in taskTypeDtos.OrderBy(tt => tt.Title))
            {
                GetTaskTypeWithHightlight(taskTypeDto, Repository.TaskTypes);
            }
        }

        public static TaskTypeView GetTaskTypeWithHightlight(TaskTypeDTO dto, UniqBindableCollection<TaskTypeView> repository)
        {
            return TaskTypeManager.GetTaskType(dto, repository, Repository.HighlightSettings.GetTaskTypeHighlightColor(dto.Id));
        }

        public UniqBindableCollection<TaskTypeView> GetTaskTypes()
        {
            return Repository.TaskTypes;
        }
    }
}
