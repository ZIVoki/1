﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading;
using System.Windows;
using Tasker.Manager.Base;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;
using Tasker.Tools.Extensions;

namespace Tasker.Manager.Managers
{
    class TimeTabelReportsManager
    {
        static readonly CultureInfo CultureInfo = new CultureInfo("ru-RU");

        public static void SendReports(List<UserView> users, DateTime startDate, DateTime endDate)
        {
            startDate = startDate.StartOfDay();
            endDate = endDate.EndOfDay();

            int loadId = LoadManager.InvokeLoadStartStatic(new LoadStartEventArgs("Подключение к почтовому серверу"));
            SmtpClient client = new SmtpClient(Repository.MailSettings.Host, Repository.MailSettings.Port)
            {
                Credentials = new NetworkCredential(Repository.MailSettings.FromEmail, Repository.MailSettings.Password)
            };
            LoadManager.InvokeLoadEndStatic(loadId);

            loadId = LoadManager.InvokeLoadStartStatic(new LoadStartEventArgs("Отправка писем"));
            foreach (var user in users)
            {
                if (user == null) continue;
                if (string.IsNullOrEmpty(user.Email)) continue;

                MailMessage newMessage = new MailMessage(Repository.MailSettings.FromEmail, user.Email)
                                             {
                                                 IsBodyHtml = false,
                                             };

                string co = "со";
                if (startDate.Date.Day == 1) co = "c";
                // set subject
                if (startDate.Month == endDate.Month)
                    newMessage.Subject = string.Format("Расписание {2} {0} по {1}", startDate.ToString("d"),
                                                       endDate.ToString("d MMMM", CultureInfo), co);
                else
                    newMessage.Subject = string.Format("Расписание {2} {0} по {1}",
                                                       startDate.ToString("d MMMM", CultureInfo),
                                                       endDate.ToString("d MMMM", CultureInfo), co);

                List<TimeTableView> timeTableViews =
                    Repository.TimeTables.Where(
                        tt =>
                        tt.User.Id == user.Id && tt.StartDate >= startDate && tt.EndDate <= endDate &&
                        tt.Status == ConstantsStatuses.Enabled).ToList();

                if (!timeTableViews.Any()) continue;

                newMessage.Body = "Рассписание на неделю \n \n";
                foreach (var tableView in timeTableViews)
                {

                    newMessage.Body += string.Format("{0} {1} {2} c {3} до {4} \n",
                                                     tableView.StartDate.ToString("ddd", CultureInfo),
                                                     tableView.StartDate.ToString("d MMMM", CultureInfo),
                                                     tableView.Shift.Title,
                                                     tableView.StartDate.ToString("HH:mm"),
                                                     tableView.EndDate.ToString("HH:mm"));
                }

                newMessage.Body += string.Format("\nНаписал робот + {0}.", Repository.LoginUser.Title);

                try
                {
                    client.Send(newMessage);
                }
                catch (SmtpException ex)
                {
                    //В случае ошибки при отсылке сообщения можем увидеть, в чем проблема
                    if (ex.InnerException != null)
                    {
                        MessageBox.Show(ex.InnerException.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                        Console.WriteLine(ex.InnerException.Message);
                    }
                    else
                    {
                        MessageBox.Show(ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                        Console.WriteLine(ex.Message);
                    }
                    client.Dispose();
                    return;
                }
                catch (Exception ex)
                {
                    //В случае ошибки при отсылке сообщения можем увидеть, в чем проблема
                    MessageBox.Show(ex.Message + " " + ex.StackTrace, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                    Console.WriteLine(ex.InnerException.Message);
                    client.Dispose();
                    return;
                }
                finally
                {
                    newMessage.Dispose();
                }
                Thread.Sleep(TimeSpan.FromSeconds(2));
            }

            LoadManager.InvokeLoadEndStatic(loadId);
            MessageBox.Show("Письма отправлены", "Завершение опреции", MessageBoxButton.OK, MessageBoxImage.Information);

            client.Dispose();
        }
    }
}
