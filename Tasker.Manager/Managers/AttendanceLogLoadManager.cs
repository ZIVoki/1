﻿using System;
using Tasker.Manager.Base;
using Tasker.Manager.Objects;
using Tasker.Manager.ServiceReference1;
using System.Linq;
using Tasker.Tools.Managers;

namespace Tasker.Manager.Managers
{
    class AttendanceLogLoadManager : LoadManager
    {
        private const string LoadMessage = "Загрузка посещаемости";
        private DateTime _currentDay;

        public void LoadAttendanceLogOnDay(DateTime day)
        {
            _currentDay = day;
            base.lastLoadDateTime = null;
            LoadData();
        }

        protected override void UpdateAction(IManagerInterface serviceChannel)
        {}

        protected override void LoadAction(IManagerInterface serviceChannel)
        {
            var loadId = InvokeLoadStart(new LoadStartEventArgs(LoadMessage));
            var attendanceLogItemDtos = serviceChannel.GetAttendanceLogByDate(_currentDay);

            foreach (var dto in attendanceLogItemDtos.OrderBy(a => a.User.LastName))
            {
                AttendanceLogItemManager.GetAttendance(dto, Repository.AttendanceLog, Repository.Users,
                                                       Repository.TimeTables, Repository.Shifts);
            }
            InvokeLoadEnd(loadId);
        }
    }
}
