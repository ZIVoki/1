﻿using System.IO;
using Tasker.Manager.Properties;

namespace Tasker.Manager.Managers
{
    class ReportFileManager
    {
        public static string CheckIfFileExist(string fileName)
        {
            string filePath = Path.Combine(Settings.Default.ReportsDestFolder, fileName);
            int i = 0;
            while (File.Exists(filePath))
            {
//                try
//                {
//                    FileInfo fileInfo = new FileInfo(filePath);
//                    fileInfo.Delete();
//                    return fileName;
//                }
//                catch
//                {
                    i++;
                    fileName = string.Format("{0}_({1}){2}", Path.GetFileNameWithoutExtension(fileName), i, Path.GetExtension(fileName));
                    filePath = Path.Combine(Settings.Default.ReportsDestFolder,  fileName);
//                }
            }

            return filePath;
        }
    }
}
