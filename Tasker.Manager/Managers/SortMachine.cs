﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Linq;

namespace Tasker.Manager.Managers
{
    public class SortMachine
    {
        GridViewColumnHeader _lastHeaderClicked = null;
        ListSortDirection _lastDirection = ListSortDirection.Ascending;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyName">Sort by</param>
        /// <param name="direction">asc or desc</param>
        /// <param name="collection"></param>
        public static void Sort(string propertyName, ListSortDirection direction, IEnumerable collection)
        {
            if (string.IsNullOrEmpty(propertyName)) return;

            ICollectionView dataView = CollectionViewSource.GetDefaultView(collection);

            if (dataView == null) return;

            List<SortDescription> sortDescriptionCollection = dataView.SortDescriptions.ToList();
            using (dataView.DeferRefresh())
            {
                dataView.SortDescriptions.Clear();

                SortDescription newSortDescription = new SortDescription(propertyName, direction);
                SortDescription firstOrDefault =
                    sortDescriptionCollection.FirstOrDefault(t => t.PropertyName == propertyName);
                if (firstOrDefault.PropertyName != null)
                {
                    sortDescriptionCollection.Remove(firstOrDefault);
                }
                dataView.SortDescriptions.Add(newSortDescription);
                foreach (SortDescription sortDescription in sortDescriptionCollection)
                {
                    dataView.SortDescriptions.Add(sortDescription);
                }
            }
        }

        public void GridViewColumnHeaderClickedHandler(IEnumerable collection, GridViewColumnHeader headerClicked, Control control)
        {
            if (headerClicked == null) return;
            var header = headerClicked.Tag as string;
            {
                if (headerClicked.Role != GridViewColumnHeaderRole.Padding)
                {
                    ListSortDirection direction;
                    if (headerClicked != _lastHeaderClicked)
                    {
                        direction = ListSortDirection.Ascending;
                    }
                    else
                    {
                        if (_lastDirection == ListSortDirection.Ascending)
                        {
                            direction = ListSortDirection.Descending;
                        }
                        else
                        {
                            direction = ListSortDirection.Ascending;
                        }
                    }

                    Sort(header, direction, collection);

                    if (direction == ListSortDirection.Ascending)
                    {
                        headerClicked.Column.HeaderTemplate =
                            control.Resources["HeaderTemplateArrowUp"] as DataTemplate;
                    }
                    else
                    {
                        headerClicked.Column.HeaderTemplate =
                            control.Resources["HeaderTemplateArrowDown"] as DataTemplate;
                    }

                    // Remove arrow from previously sorted header
                    if (_lastHeaderClicked != null && _lastHeaderClicked != headerClicked)
                    {
                        _lastHeaderClicked.Column.HeaderTemplate = null;
                    }


                    _lastHeaderClicked = headerClicked;
                    _lastDirection = direction;
                }
            }
        }
    }
}
