﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Office.Interop.Excel;
using Tasker.Manager.Helpers;
using Tasker.Manager.Model;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;
using Tasker.Manager.Managers.Logger;
using Tasker.Tools.Extensions;

namespace Tasker.Manager.Managers
{
    public class ExcelReportGeneartor
    {
        public void GenerateTaskReport(List<TaskView> tasks, string fullFileName)
        {
            //create document and doc hat
            bool createExcelInstenceFailed;
            var excelHelper = new ExcelHelper(out createExcelInstenceFailed);

            if (createExcelInstenceFailed)
            {
                return;
            }

            var fileName = new FileInfo(fullFileName).Name;

            SetExcelReportHeaderAndNameTaskReport(excelHelper, fileName);

            //get task info here
            //get task start end date
            //get real duration
            //get plan duration
            //get task workers separated by comma
            var y = 5;
            excelHelper = setSheetCellsWithTasksInfo(excelHelper, tasks.Select(taskView => taskView.BuildDTO()).ToList(), out y);
            SetExcelStyle(excelHelper, y);

            string resultFile = ReportFileManager.CheckIfFileExist(fileName);
            excelHelper.SaveAsAndClose(resultFile);
        }

        public void CreateUserExpirianceReport(List<UsersStatDataView> usersStatDataViews, string fullFileName)
        {
            //create document and doc hat
            bool createExcelInstenceFailed;
            var excelHelper = new ExcelHelper(out createExcelInstenceFailed);

            if (createExcelInstenceFailed)
            {
                return;
            }

            var fileName = new FileInfo(fullFileName).Name;

            SetUserExpirianceReportHeader(excelHelper, fileName);
            SetUserExpiranceReportStat(excelHelper, usersStatDataViews);

            string resultFile = ReportFileManager.CheckIfFileExist(fileName);
            excelHelper.SaveAsAndClose(resultFile);
        }

        private void SetUserExpiranceReportStat(ExcelHelper excelHelper, List<UsersStatDataView> usersStatDataViews)
        {
            // todo
        }

        private void SetUserExpirianceReportHeader(ExcelHelper excelHelper, string fileName)
        {
            // todo
        }

        public string GenerateUserReport(List<UserView> users, DateTime startDate, DateTime endDate, string fullFileName)
        {
            var fileName = new FileInfo(fullFileName).Name;

            startDate = startDate.StartOfDay();
            endDate = endDate.EndOfDay();

            //create document and doc hat
            bool createExcelInstenceFailed;
            ExcelHelper excelHelper = new ExcelHelper(out createExcelInstenceFailed);
            

            if (createExcelInstenceFailed)
            {
                Log.Write(LogLevel.Error, "Failed while create excel instance!");
                return "";
            }

            int i = 0;
            int y = 5;
            foreach (var user in users)
            {
                i++;
                //make for each users new excel sheet
                if (excelHelper.Worksheets.Count < i)
                {
                    excelHelper.Worksheets.Add(Type.Missing, excelHelper.Worksheets[i - 1], Type.Missing, Type.Missing);
                }
                
                excelHelper.SelectWorksheet(i);
                
                excelHelper.Worksheet.Name = user.LastName;
                SetExcelReportHeaderAndNameTaskReport(excelHelper, user.LastName);
                
                //get users closed tasks by dates
                TaskDTO[] tasks = ServiceManager.Inst.ServiceChannel.GetClosedTasksByWorkerAndDate(user.Id, startDate, endDate);
                
                excelHelper = setSheetCellsWithTasksInfo(excelHelper, tasks.ToList(), out y);
                SetExcelStyle(excelHelper, y);
            }

            string resultFile = ReportFileManager.CheckIfFileExist(fileName);
            excelHelper.SaveAsAndClose(resultFile);
            return resultFile;
        }

        private ExcelHelper setSheetCellsWithTasksInfo(ExcelHelper excelHelper, List<TaskDTO> tasks, out int y)
        {
            y = 5;

            var compagedel = new Comparison<TaskDTO>((a, b) => a.CreationDate.CompareTo(b.CreationDate));
            
            tasks.Sort(compagedel);

            var number = 1;
            foreach (var task in tasks)
            {
                if (task.Status != ConstantsStatuses.Closed) continue;
                var statistics = ServiceManager.Inst.ServiceChannel.GetTaskStatistic(task.Id);

                var taskStartDate = statistics[0].StartDate;
                var taskEndDateDate = statistics[statistics.Length - 1].StartDate;
                var planDuration = task.PlannedTime;

                var realDuration = new TimeSpan(0);
                foreach (var stat in statistics)
                {
                    if (stat.TaskState == ConstantsStatuses.InProcess)
                    {
                        //if (stat.Worker != null && stat.Worker.Id == user.Id)
                        {
                            realDuration += stat.Duration;
                        }
                    }
                }

                var workersLine = string.Join(";", (statistics.Where(statistic => statistic.Worker != null).Select(statistic => statistic.Worker.LastName).Distinct()).ToArray());

                excelHelper.GetCell(y, 1).Value = number;
                excelHelper.GetCell(y, 2).Value = task.TaskType.Title;
                
                
                excelHelper.GetCell(y, 3).Value = task.Title;
                excelHelper.GetCell(y, 4).Value = taskStartDate;
                excelHelper.GetCell(y, 5).Value = taskEndDateDate;


                excelHelper.GetCell(y, 6).Value = string.Format("{0:00}:{1:00}:{2:00}", (int)planDuration.TotalHours, planDuration.Minutes, planDuration.Seconds);


                excelHelper.GetCell(y, 7).Value = string.Format("{0:00}:{1:00}:{2:00}", (int)realDuration.TotalHours, realDuration.Minutes, realDuration.Seconds);
                excelHelper.GetCell(y, 8).Value = workersLine;
                number++;
                y++;
            }
            
            return excelHelper;
        }

        private static void SetExcelStyle(ExcelHelper excelHelper, int lastRowIndex)
        {
            DrawThickBorder(excelHelper.GetRange(1, 4, 4, lastRowIndex - 1));
            DrawMediumBorderAround(excelHelper.GetRange(1, 4, 8, lastRowIndex - 1));
            excelHelper.GetRange(1, 1, 5, lastRowIndex).VerticalAlignment = XlHAlign.xlHAlignCenter;

        }

        public static string GenearteFileName(string title)
        {
            string resultName = String.Format("{0}_{1}",
                                              title,
                                              DateTime.Now.ToString("dd.MM.yyyy HH-mm"));
            return resultName;
        }

        private static void SetExcelReportHeaderAndNameTaskReport(ExcelHelper excelHelper, string excelName)
        {
            excelHelper.SetOrientation(XlPageOrientation.xlPortrait, excelHelper.Worksheet);
/*            double leftBorder = 0.25*100;
            double topBorder = 0.75*100;
            double headerBorder = 0.23*100;
            excelHelper.SetPageMargins(leftBorder, leftBorder, topBorder, topBorder, headerBorder);*/
            excelHelper.SetPageHeader(excelName);
            excelHelper.MergeRange(1, 2, 6, 2);
            Range range = excelHelper.GetCell(2, 1);
            range.Value = excelName;
            range.Font.Size = 24;
            range.ColumnWidth = 120;
            excelHelper.GetRange(1, 1, 4, 3).Font.Bold = true;

            //create rows and columns or headers
            excelHelper.GetCell(4, 1).Value = "№";
            excelHelper.GetCell(4, 1).Font.Bold = true;
            excelHelper.GetCell(4, 1).ColumnWidth = 4;
            DrawMediumBorderAround(excelHelper.GetCell(4, 1));

            excelHelper.GetCell(4, 2).Value = "Тип";
            excelHelper.GetCell(4, 2).Font.Bold = true;
            excelHelper.GetCell(4, 2).ColumnWidth = 27;
            DrawMediumBorderAround(excelHelper.GetCell(4, 2));

            excelHelper.GetCell(4, 3).Value = "Название";
            excelHelper.GetCell(4, 3).Font.Bold = true;
            excelHelper.GetCell(4, 3).ColumnWidth = 50;
            DrawMediumBorderAround(excelHelper.GetCell(4, 3));

            excelHelper.GetCell(4, 4).Value = "Начало";
            excelHelper.GetCell(4, 4).ColumnWidth = 15;
            excelHelper.GetCell(4, 4).Font.Bold = true;
            DrawMediumBorderAround(excelHelper.GetCell(4, 4));

            excelHelper.GetCell(4, 5).Value = "Конец";
            excelHelper.GetCell(4, 5).ColumnWidth = 17;
            excelHelper.GetCell(4, 5).Font.Bold = true;
            DrawMediumBorderAround(excelHelper.GetCell(4, 5));

            excelHelper.GetCell(3, 6).ColumnWidth = 10;
            excelHelper.GetCell(3, 7).ColumnWidth = 13;
            excelHelper.MergeRange(6, 3, 7, 3);
            excelHelper.GetCell(3, 6).Value = "Время выполнения";
            excelHelper.GetCell(3, 6).Font.Bold = true;
            DrawMediumBorderAround(excelHelper.GetCell(3, 6));
            DrawMediumBorderAround(excelHelper.GetCell(3, 7));
            

            excelHelper.GetCell(4, 6).Value = "По плану";
            excelHelper.GetCell(4, 6).Font.Bold = true;
            DrawMediumBorderAround(excelHelper.GetCell(4, 6));
            excelHelper.GetCell(4, 6).ColumnWidth = 10;

            excelHelper.GetCell(4, 7).Value = "Фактическое";
            excelHelper.GetCell(4, 7).Font.Bold = true;
            DrawMediumBorderAround(excelHelper.GetCell(4, 7));
            excelHelper.GetCell(4, 7).ColumnWidth = 13;

            excelHelper.GetCell(4, 8).Value = "Работники";
            excelHelper.GetCell(4, 8).Font.Bold = true;
            excelHelper.GetCell(4, 8).WrapText = true;
            DrawMediumBorderAround(excelHelper.GetCell(4, 8));
            excelHelper.GetCell(4, 8).ColumnWidth = 26;

            //excelHelper.GetCell(5, 4).ColumnWidth = 35;
        }

        private static void DrawMediumBorderAround(Range cells)
        {
            cells.BorderAround(Type.Missing, XlBorderWeight.xlMedium, XlColorIndex.xlColorIndexAutomatic, Type.Missing);
        }

        private static void DrawThickBorder(Range cells)
        {
            cells.Borders.Item[XlBordersIndex.xlEdgeLeft].Weight = 1;
            cells.Borders.Item[XlBordersIndex.xlEdgeLeft].LineStyle = XlLineStyle.xlContinuous;
            cells.Borders.Item[XlBordersIndex.xlEdgeRight].Weight = 1;
            cells.Borders.Item[XlBordersIndex.xlEdgeRight].LineStyle = XlLineStyle.xlContinuous;
            cells.Borders.Item[XlBordersIndex.xlEdgeTop].Weight = 1;
            cells.Borders.Item[XlBordersIndex.xlEdgeTop].LineStyle = XlLineStyle.xlContinuous;
            cells.Borders.Item[XlBordersIndex.xlEdgeBottom].Weight = 1;
            cells.Borders.Item[XlBordersIndex.xlEdgeBottom].LineStyle = XlLineStyle.xlContinuous;
        }

        
    }
}