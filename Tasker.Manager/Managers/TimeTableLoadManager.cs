﻿using System;
using System.Collections.Generic;
using Tasker.Manager.Base;
using Tasker.Manager.ServiceReference1;
using Tasker.Tools.Managers;
using Tasker.Tools.ServiceReference;

namespace Tasker.Manager.Managers
{
    class TimeTableLoadManager: LoadManager
    {
        private const string LoadMessage = "Загрузка расписаний";
        // первый день недели текущей загрузки
        private static DateTime _currentWeekStartDate = GetThisWeekMonday();
        // чтобы не грузить лишнего
        private readonly List<DateTime> _loadedeMondays = new List<DateTime>();

        public void ClearLoadHistory()
        {
            _loadedeMondays.Clear();
        }


        /// <summary>
        ////загружает рассписание на указанную неделю в качестве аргумента берет понедельник необходимой недели
        /// </summary>
        /// <param name="firstDayOfWeekDateTime">понедельник загружаемой недели</param>
        public void LoadTimeTableOnWeek(DateTime firstDayOfWeekDateTime)
        {
            lastLoadDateTime = null;
            firstDayOfWeekDateTime = firstDayOfWeekDateTime.Date;
            if (_loadedeMondays.Contains(firstDayOfWeekDateTime.Date)) return;
            _loadedeMondays.Add(firstDayOfWeekDateTime);
            _currentWeekStartDate = firstDayOfWeekDateTime;
            LoadData();
        }

        public void LoadSelfTimeTable(IManagerInterface serviceChanel)
        {
            var loadId = InvokeLoadStart(new LoadStartEventArgs(LoadMessage));
            TimeTableDTO[] userTimeTables = serviceChanel.GetUserTimeTables(Repository.LoginUser.Id, DateTime.Now.Date, DateTime.Now.Date.AddDays(366));
            foreach (var userTimeTable in userTimeTables)
            {
                TimeTableManager.GetTimeTable(userTimeTable, Repository.TimeTables, Repository.Users, Repository.Shifts);
            }
            InvokeLoadEnd(loadId);
        }

        public void LoadTodaysTimeTable()
        {
            LoadTimeTableOnWeek(_currentWeekStartDate);
/*
            var loadId = InvokeLoadStart(new LoadStartEventArgs(LoadMessage));
            TimeTableDTO[] userTimeTables = serviceChanel.GetTimeTablesByDate(DateTime.Now);
            foreach (var userTimeTable in userTimeTables)
            {
                TimeTableManager.GetTimeTable(userTimeTable, Repository.TimeTables, Repository.Users, Repository.Shifts);
            }
            InvokeLoadEnd(loadId);
*/
        }

        protected override void UpdateAction(IManagerInterface serviceChannel)
        {
            var loadId = InvokeLoadStart(new LoadStartEventArgs(LoadMessage));
            //if we add only 6 days we load all tables until sunday 00:00. Its wrong
            var tt = _currentWeekStartDate.AddDays(7);
            TimeTableDTO[] workersTimeTableDtos = serviceChannel.GetTimeTablesByDates(_currentWeekStartDate, tt);

            foreach (var workersTimeTableDto in workersTimeTableDtos)
            {
                TimeTableManager.UpdateTimeTableView(workersTimeTableDto, Repository.TimeTables, Repository.Users, Repository.Shifts);
            }
            InvokeLoadEnd(loadId);
        }

        /// <summary> 
        /// загружает timeTable только на текущую неделю, которая определяется датой статического поля _currentWeekStartDate
        /// </summary>
        /// <param name="serviceChannel"></param>
        protected override void LoadAction(IManagerInterface serviceChannel)
        {
            var loadId = InvokeLoadStart(new LoadStartEventArgs(LoadMessage));
            //if we add only 6 days we load all tables until sunday 00:00. Its wrong
            DateTime endDate = _currentWeekStartDate.AddDays(7).AddHours(12);
            TimeTableDTO[] workersTimeTableDtos = serviceChannel.GetTimeTablesByDates(_currentWeekStartDate, endDate);

            foreach (var workersTimeTableDto in workersTimeTableDtos)
            {
                TimeTableManager.GetTimeTable(workersTimeTableDto, Repository.TimeTables, Repository.Users, Repository.Shifts);
            }
            InvokeLoadEnd(loadId);
        }

        private static DateTime GetThisWeekMonday()
        {
            DateTime dateTime = DateTime.Now;

            while (dateTime.DayOfWeek != DayOfWeek.Monday)
            {
                dateTime = dateTime.AddDays(-1);
            }

            return dateTime.Date;
        }
    }
}
