﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using Tasker.Tools.ServiceReference;

namespace Tasker.Manager.Converters
{
    class ImageStatusConverter: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return null;
            ConstantsStatuses status = (ConstantsStatuses)value;
            if (status == null) return null;
            // todo rewrite links to universal
            switch (status)
            {
                case ConstantsStatuses.Nothing:
                    return new BitmapImage(new Uri("../../Images/normal.png", UriKind.Relative));
                case ConstantsStatuses.Enabled:
                    return new BitmapImage(new Uri("../../Images/normal.png", UriKind.Relative));
                case ConstantsStatuses.Disabled:
                    return new BitmapImage(new Uri("../../Images/task_paused.png", UriKind.Relative));
                case ConstantsStatuses.Deleted:
                    return new BitmapImage(new Uri("../../Images/task_deleted.png", UriKind.Relative));
                case ConstantsStatuses.Planing:
                    return new BitmapImage(new Uri("../../Images/task_planing.png", UriKind.Relative));
                case ConstantsStatuses.Closed:
                    return new BitmapImage(new Uri("../../Images/task_done.png", UriKind.Relative));
                case ConstantsStatuses.SendedForReview:
                    return new BitmapImage(new Uri("../../Images/task_sended_for_review.png", UriKind.Relative));
                case ConstantsStatuses.InProcess:
                    return new BitmapImage(new Uri("../../Images/task_in_process.png", UriKind.Relative));
                case ConstantsStatuses.UserEnter:
                    return new BitmapImage(new Uri("../../Images/online16x16.png", UriKind.Relative));
                case ConstantsStatuses.UserLeave:
                    return new BitmapImage(new Uri("../../Images/offline16x16.png", UriKind.Relative));
                case ConstantsStatuses.UserLunch:
                    return new BitmapImage(new Uri("../../Images/awayLunch16x16.png", UriKind.Relative));
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
