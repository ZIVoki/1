﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Manager.Converters
{
    class DrawningColorToMediaColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return null;

            if(value is System.Drawing.Color)
            {
                var oldColor = (System.Drawing.Color) value;
                var newColor = Color.FromRgb(oldColor.R, oldColor.G, oldColor.B);
                return newColor;
            }
            return ConvertBack(value, null, null, null);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Color)
            {
                var oldColor = (Color)value;
                var newColor = System.Drawing.Color.FromArgb(oldColor.R, oldColor.G, oldColor.B);
                return newColor;
            }
            else
            {
                return null;
            }
        }
    }
}
