﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Tasker.Tools.ServiceReference;

namespace Tasker.Manager.Converters
{
    class StatusToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return null;
            ConstantsStatuses status = (ConstantsStatuses)value;
            if (status == null) return null;
            // todo rewrite links to universal
            switch (status)
            {
                case ConstantsStatuses.Nothing:
                case ConstantsStatuses.Enabled:
                case ConstantsStatuses.Planing:
                case ConstantsStatuses.Disabled:
                case ConstantsStatuses.InProcess:
                case ConstantsStatuses.SendedForReview:
                    return Visibility.Visible;
                case ConstantsStatuses.Deleted:
                case ConstantsStatuses.Closed:
                    return Visibility.Hidden;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
