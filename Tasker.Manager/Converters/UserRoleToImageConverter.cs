﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using Tasker.Tools.ServiceReference;

namespace Tasker.Manager.Converters
{
    class UserRoleToImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return null;
            var state = (ConstantsRoles)value;
            // todo rewrite links to universal
            switch (state)
            {
                case ConstantsRoles.Worker:
                    return new BitmapImage(new Uri("../../Images/worker16x16.png", UriKind.Relative));
                case ConstantsRoles.Manager:
                    return new BitmapImage(new Uri("../../Images/manager16x16.png", UriKind.Relative));
                case ConstantsRoles.Administrator:
                    return new BitmapImage(new Uri("../../Images/admin16x16.png", UriKind.Relative));
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
