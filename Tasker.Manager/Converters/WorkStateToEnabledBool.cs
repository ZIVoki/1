﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Tasker.Manager.Converters
{
    class WorkStateToEnabledBool : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return null;
            var state = (Tools.Constants.TaskWorkState)value;

            switch (state)
            {
                case Tools.Constants.TaskWorkState.NotAvailable:
                    return false;
                case Tools.Constants.TaskWorkState.Available:
                case Tools.Constants.TaskWorkState.CheckedOut:
                    return true;
                
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
