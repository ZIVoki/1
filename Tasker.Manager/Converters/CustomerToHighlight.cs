﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;
using Color = System.Drawing.Color;

namespace Tasker.Manager.Converters
{
    class CustomerToHighlight: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Color? color = null;
            if (value == null) return new SolidColorBrush();

            color = (Color?)value;
            var mediaColor = System.Windows.Media.Color.FromRgb(color.Value.R, color.Value.G, color.Value.B);
            return new SolidColorBrush(mediaColor);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class ObjectToUserViewRoleInt: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return 0;
            if (value is UserView)
            {
                UserView user = (UserView) value;
                return (int)user.Role;
            }
            return 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
