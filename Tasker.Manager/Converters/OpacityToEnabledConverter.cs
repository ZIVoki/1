using System;
using System.Globalization;
using System.Windows.Data;

namespace Tasker.Manager.Converters
{
    public class OpacityToEnabledConverter: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool? b = value is Boolean;

            if (!b.HasValue) return 1;

            if (b.Value == true)
                return 1;
            else
                return 0.5d;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}