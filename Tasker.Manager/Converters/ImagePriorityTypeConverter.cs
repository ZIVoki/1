﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using Tasker.Tools.ViewModel;

namespace Tasker.Manager.Converters
{
    class ImagePriorityTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return null;
            PriorityType status = (PriorityType)value;
            if (status == null) return null;

            switch (status)
            {
                case PriorityType.Quick:
                    return new BitmapImage(new Uri("../../Images/hot_24x24.png", UriKind.Relative));
                default:
                    return new BitmapImage(new Uri("", UriKind.Relative));
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}