﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;
using Tasker.Tools.ServiceReference;

namespace Tasker.Manager.Converters
{
    public class IsClosedStatusConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return true;

            if (value is ConstantsStatuses)
            {
                ConstantsStatuses status = (ConstantsStatuses) value;
                if (status == ConstantsStatuses.Closed)
                    return true;
                else
                    return false;
            }
            return true;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
