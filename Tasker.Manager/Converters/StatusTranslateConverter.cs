﻿using System;
using System.Globalization;
using System.Windows.Data;
using Tasker.Tools.ServiceReference;

namespace Tasker.Manager.Converters
{
    class StatusTranslateConverter: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return null;
            ConstantsStatuses status = (ConstantsStatuses)value;
            if (status == null) return null;
            switch (status)
            {
                case ConstantsStatuses.Nothing:
                    return "Нет статуса";
                case ConstantsStatuses.Enabled:
                    return "Доступна";
                case ConstantsStatuses.Disabled:
                    return "Приостановлена";
                case ConstantsStatuses.Deleted:
                    return "Удалена";
                case ConstantsStatuses.Planing:
                    return "Ожидает подтверждения сотрудника";
                case ConstantsStatuses.Closed:
                    return "Закрыта";
                case ConstantsStatuses.SendedForReview:
                    return "Отправлена на проверку";
                case ConstantsStatuses.InProcess:
                    return "Выполняется";
                case ConstantsStatuses.UserEnter:
                    return "Вход";
                case ConstantsStatuses.UserLeave:
                    return "Выход";
                case ConstantsStatuses.UserLunch:
                    return "Обед";
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
