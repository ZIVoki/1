﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace Tasker.Manager.Converters
{
    class TimeSpanToDateTimeConverter: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            TimeSpan timeSpan = new TimeSpan(0, 0, 0);
            if (value != null)
            {
                try
                {
                    timeSpan = (TimeSpan)value;
                }
                catch (InvalidCastException e)
                {
                    return new TimeSpan(0, 0, 0);
                }
            }
            DateTime dateTime = new DateTime(1900, 10, 10, timeSpan.Hours, timeSpan.Minutes, 0);
            return dateTime;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            DateTime dateTime = (DateTime)value;
            TimeSpan timeSpan = dateTime.TimeOfDay;
            return timeSpan;
        }
    }

    class StringToBoolconverter: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string s = value as String;
            if (String.IsNullOrEmpty(s) || String.IsNullOrWhiteSpace(s))
                return true;
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
