﻿using System;
using System.Windows.Media;
using System.Globalization;
using System.Windows.Data;

namespace Tasker.Manager.Converters
{
    class UserOverTimeLoadConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return Color.FromRgb(0, 0, 0);
            if (value is double)
            {
                double hours = (double ) value;

                if (hours > 40)
                    // красный
                    return Color.FromRgb(227,38, 54);
                else
                {
                    Color.FromRgb(0, 0, 0);
                }
            }
            return Color.FromRgb(0, 0, 0);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
