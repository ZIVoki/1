﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace Tasker.Manager.Converters
{
    class UserWorkStateToImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return null;
            var state = (Tools.Constants.UserWorkState)value;

            switch (state)
            {
                case Tools.Constants.UserWorkState.NotAvailable:
                    return new BitmapImage(new Uri("../../Images/lock16x16.gif", UriKind.Relative));
                case Tools.Constants.UserWorkState.Free:
                    return new BitmapImage(new Uri("../../Images/free_for_job16x16.png", UriKind.Relative));
                case Tools.Constants.UserWorkState.Busy:
                    return new BitmapImage(new Uri("../../Images/busy16x16.png", UriKind.Relative));
                case Tools.Constants.UserWorkState.BackgroundTask:
                    return new BitmapImage(new Uri("../../Images/background16x16.png", UriKind.Relative));
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
