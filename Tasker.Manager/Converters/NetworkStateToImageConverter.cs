﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Tasker.Tools.ServiceReference;

namespace Tasker.Manager.Converters
{
    class NetworkStateToImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return null;
            var state = (ConstantsNetworkStates)value;
            // todo rewrite links to universal
            switch (state)
            {
                case ConstantsNetworkStates.Depard:
                    return new BitmapImage(new Uri("../../Images/away16x16.png", UriKind.Relative));
                case ConstantsNetworkStates.Lunch:
                    return new BitmapImage(new Uri("../../Images/awayLunch16x16.png", UriKind.Relative));
                case ConstantsNetworkStates.Online:
                    return new BitmapImage(new Uri("../../Images/online16x16.png", UriKind.Relative));
                case ConstantsNetworkStates.Offline:
                    return new BitmapImage(new Uri("../../Images/offline16x16.png", UriKind.Relative));
                case ConstantsNetworkStates.Nothing:
                    return null;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
