﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace Tasker.Manager.Converters
{
    class TaskWorkStateToImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return null;
            var state = (Tools.Constants.TaskWorkState)value;
            // todo rewrite links to universal
            switch (state)
            {
                case Tools.Constants.TaskWorkState.NotAvailable:
                    return new BitmapImage(new Uri("../../Images/lock16x16.gif", UriKind.Relative));
                case Tools.Constants.TaskWorkState.Available:
                    return null;
                case Tools.Constants.TaskWorkState.CheckedOut:
                    return new BitmapImage(new Uri("../../Images/bullet_key.png", UriKind.Relative));
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}