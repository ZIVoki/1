﻿using System;

namespace Tasker.Manager.Converters
{
    class ShortDayOfWeekConverter
    {
        public static string GetShortDayOfWeek(DateTime date)
        {
            switch (date.DayOfWeek)
            {
                case DayOfWeek.Sunday:
                    return "вс";
                case DayOfWeek.Monday:
                    return "пн";
                case DayOfWeek.Tuesday:
                    return "вт";
                case DayOfWeek.Wednesday:
                    return "ср";
                case DayOfWeek.Thursday:
                    return "чт";
                case DayOfWeek.Friday:
                    return "пт";
                case DayOfWeek.Saturday:
                    return "сб";
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
