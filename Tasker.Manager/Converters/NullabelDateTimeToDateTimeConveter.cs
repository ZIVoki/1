using System;
using System.Globalization;
using System.Windows.Data;

namespace Tasker.Manager.Converters
{
    public class NullabelDateTimeToDateTimeConveter: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is DateTime?)
            {
                DateTime? dt = (DateTime?) value;
                if (dt.HasValue)
                    return dt.Value;
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}