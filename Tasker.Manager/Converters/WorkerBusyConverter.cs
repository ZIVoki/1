﻿using System;
using System.Globalization;
using System.Windows.Data;
using Tasker.Tools.ViewModel;

namespace Tasker.Manager.Converters
{
    class WorkerBusyConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value is WorkerView)
            {
                return ((WorkerView) value).IsBusy;
            }
            else
            {
                return false;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            DateTime dateTime = (DateTime)value;
            TimeSpan timeSpan = dateTime.TimeOfDay;
            return timeSpan;
        }
    }
}
