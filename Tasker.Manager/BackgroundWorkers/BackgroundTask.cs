﻿using System.ComponentModel;

namespace Tasker.Manager.BackgroundWorkers
{
    public class BackgroundTask
    {
        protected BackgroundWorker _backgroundWorker;

        /// <summary>
        /// Background task with circle and normal progressbar 
        /// </summary>
        /// <param name="doWork">working method</param>
        /// <param name="completeWork">method wich invoke after end of working doWork method</param>
        /// <param name="progressChanged"></param>
        public BackgroundTask(DoWorkEventHandler doWork, 
                              RunWorkerCompletedEventHandler completeWork,
                              ProgressChangedEventHandler progressChanged = null)
        {
            _backgroundWorker = new BackgroundWorker();
            
            _backgroundWorker.DoWork += (doWork);
            _backgroundWorker.RunWorkerCompleted += (completeWork);

            if (progressChanged != null)
            {
                
                _backgroundWorker.WorkerReportsProgress = true;
                _backgroundWorker.ProgressChanged += (progressChanged);

            }
        }

        protected BackgroundTask()
        {
            _backgroundWorker = new BackgroundWorker();
        }
        
        public void Run(object argument = null)
        {
            if (argument != null)
            {
                _backgroundWorker.RunWorkerAsync(argument);
            }
            else
            {
                _backgroundWorker.RunWorkerAsync();
            }
        }
    }
}
