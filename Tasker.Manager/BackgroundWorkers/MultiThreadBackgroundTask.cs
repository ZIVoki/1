using System.ComponentModel;
using Tasker.Manager.Interfaces;

namespace Tasker.Manager.BackgroundWorkers
{
    public class MultiThreadBackgroundTask: BackgroundTask
    {
        /// <summary>
        /// Background task with circle and normal progressbar 
        /// </summary>
        /// <param name="sender">IDataHandlerInterface to increment task counter</param>
        /// <param name="doWork">working method</param>
        /// <param name="completeWork">method wich invoke after end of working doWork method</param>
        /// <param name="progressChanged"></param>        
        public MultiThreadBackgroundTask(IDataHandlerInterface sender, 
                                         DoWorkEventHandler doWork, 
                                         RunWorkerCompletedEventHandler completeWork, 
                                         ProgressChangedEventHandler progressChanged = null)
        {
            sender.DoWorkCount++;

            _backgroundWorker.DoWork += (doWork);
            _backgroundWorker.RunWorkerCompleted += (completeWork);

            if (progressChanged != null)
            {
                _backgroundWorker.WorkerReportsProgress = true;
                _backgroundWorker.ProgressChanged += (progressChanged);
                
            }
        }
    }
}