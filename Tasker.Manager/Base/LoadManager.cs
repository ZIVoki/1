﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Tasker.Manager.Managers;
using Tasker.Manager.ServiceReference1;

namespace Tasker.Manager.Base
{
    public delegate void LoadStartEventHandler(object sender, LoadStartEventArgs e);
    public delegate void LoadEndEventHandler(object sender, LoadStartEventArgs e);

    public class LoadStartEventArgs
    {
        public string LoadMessage { get; set; }

        public LoadStartEventArgs(string loadMessage)
        {
            LoadMessage = loadMessage;
        }
    }

    abstract class LoadManager
    {
        private static Dictionary<int, string> workingNow = new Dictionary<int, string>();
        public static event LoadStartEventHandler LoadStart;
        public static event LoadEndEventHandler LoadEnd;
        public static bool IsBusy
        {
            get
            {
                var busy = workingNow.Count != 0;
                return busy;
            }
        }

        private static Random rnd = new Random();

        public void InvokeLoadEnd(int name)
        {
            if (workingNow.ContainsKey(name))
            {
                LoadEndEventHandler handler = LoadEnd;
                var message = workingNow[name];
                workingNow.Remove(name);
                if (handler != null) handler(null, new LoadStartEventArgs(message));
            }
        }

        public int InvokeLoadStart(LoadStartEventArgs e)
        {
            LoadStartEventHandler handler = LoadStart;
            var randName = rnd.Next();
            workingNow.Add(randName,e.LoadMessage);
            if (handler != null) handler(this, e);
            Debug.WriteLine(e.LoadMessage + " " + randName);
            return randName;
        }

        public static int InvokeLoadStartStatic(LoadStartEventArgs e)
        {
            LoadStartEventHandler handler = LoadStart;
            var randName = rnd.Next();
            workingNow.Add(randName,e.LoadMessage);
            if (handler != null) handler(null, e);
            Debug.WriteLine(e.LoadMessage + " " + randName);
            return randName;
        }

        public static void InvokeLoadEndStatic(int name)
        {
            if (workingNow.ContainsKey(name))
            {
                LoadEndEventHandler handler = LoadEnd;
                var message = workingNow[name];
                workingNow.Remove(name);
                Debug.WriteLine("Load end " + name);
                if (handler != null) handler(null, new LoadStartEventArgs(message));
            }
        }

        protected DateTime? lastLoadDateTime = null;

        public DateTime? LastLoadDateTime
        {
            get { return lastLoadDateTime; }
        }

        public void LoadData()
        {
            if (lastLoadDateTime == null)
            {
                if (ServiceManager.Inst.ServiceChannel == null)
                {
                    return;
                }
                LoadAction(ServiceManager.Inst.ServiceChannel);
                lastLoadDateTime = DateTime.Now;
            }
        }

        public void UpdateData()
        {
            if (ServiceManager.Inst.ServiceChannel == null) return;
            UpdateAction(ServiceManager.Inst.ServiceChannel); 
            lastLoadDateTime = DateTime.Now;
        }

        public static bool StopByKey(int hashCode, out string message)
        {
            if (workingNow.ContainsKey(hashCode))
            {
                message = workingNow[hashCode];
                return true;
            }
            else
            {
                message = string.Empty;
                return false;
            }
        }

        protected abstract void UpdateAction(IManagerInterface serviceChannel);
        protected abstract void LoadAction(IManagerInterface serviceChannel);
    }
}
