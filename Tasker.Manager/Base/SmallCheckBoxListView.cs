﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using Tasker.Tools.ViewModel;

namespace Tasker.Manager.Base
{
    public abstract class SmallCheckBoxListView: ListView
    {
        public GridViewColumn CheckBoxColumn;
        public GridViewColumnHeader CheckBoxColumnHeader;

        protected bool _isCheckBoxEnabled = false;

        public bool IsCheckBoxEnabled
        {
            get { return _isCheckBoxEnabled; }
            set
            {
                if (CheckBoxColumn != null)
                {
                    if (value)
                    {
                        CheckBoxColumn.Width = 25;
                        CheckBoxColumnHeader.Visibility = Visibility.Visible;
                        this.SelectionMode = SelectionMode.Multiple;
                    }
                    else
                    {
                        CheckBoxColumn.Width = 0;
                        CheckBoxColumnHeader.Visibility = Visibility.Collapsed;
                        this.SelectionMode = SelectionMode.Single;
                    }
                }
                _isCheckBoxEnabled = value;
            }
        }

        protected void HeaderCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            SelectAll();
        }

        protected void HeaderCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            UnselectAll();
        }

        public IEnumerable<T> GetSelectedItems<T>()
        {
            return SelectedItems.Cast<T>();
        }
    }
}
