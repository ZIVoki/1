namespace Tasker.Manager.Interfaces
{
    public interface IDataHandlerInterface
    {
        int DoWorkCount { get; set; }
    }
}