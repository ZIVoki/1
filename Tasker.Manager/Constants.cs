﻿namespace Tasker.Manager
{
    class Constants
    {
        public enum TabType
        {}

        // to remember last password
        public static string Password { get; set; }

        public static bool IsDomainUser { get; set; }
    }
}
