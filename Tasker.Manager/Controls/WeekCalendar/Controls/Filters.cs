﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tasker.Manager.Controls.WeekCalendar.Model;

namespace Tasker.Manager.Controls.WeekCalendar.Controls
{
    public static class Filters
    {
        public static IEnumerable<Appointment> ByDate(this IEnumerable<Appointment> appointments, DateTime date)
        {
            IEnumerable<Appointment> app = appointments.Where(a => a.StartTime.Date == date.Date || a.EndTime.Date == date.Date);

            #if DEBUG
                int count = app.Count();
            #endif

            return app;
        }
    }
}
