﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace Tasker.Manager.Controls.WeekCalendar.Controls
{
    public class CalendarLedgerItem : Control
    {
        static CalendarLedgerItem()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(CalendarLedgerItem), new FrameworkPropertyMetadata(typeof(CalendarLedgerItem)));
        }

        public CalendarLedgerItem()
        {
            this.Resources.MergedDictionaries.Add(new ResourceDictionary() { Source = new Uri(@"\Controls\WeekCalendar\Controls\weekCalendarStyles.xaml", UriKind.RelativeOrAbsolute) });
            var tt = this.Resources.MergedDictionaries[0]["calendarLedgerItemStyle"];
            Style = (Style)tt;
        }

        #region TimeslotA

        public static readonly DependencyProperty TimeslotAProperty =
            DependencyProperty.Register("TimeslotA", typeof(string), typeof(CalendarLedgerItem),
                new FrameworkPropertyMetadata((string)string.Empty));

        public string TimeslotA
        {
            get { return (string)GetValue(TimeslotAProperty); }
            set { SetValue(TimeslotAProperty, value); }
        }

        #endregion

        #region TimeslotB

        public static readonly DependencyProperty TimeslotBProperty =
            DependencyProperty.Register("TimeslotB", typeof(string), typeof(CalendarLedgerItem),
                new FrameworkPropertyMetadata((string)string.Empty));

        public string TimeslotB
        {
            get { return (string)GetValue(TimeslotBProperty); }
            set { SetValue(TimeslotBProperty, value); }
        }

        #endregion
    }
}
