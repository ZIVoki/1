﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using Tasker.Manager.BackgroundWorkers;
using Tasker.Manager.Controls.WeekCalendar.Model;
using Tasker.Manager.Managers;
using Tasker.Tools.CustomEventArgs;
using Tasker.Tools.Managers;
using Tasker.Tools.ViewModel;
using log4net.Repository.Hierarchy;

namespace Tasker.Manager.Controls.WeekCalendar.Controls
{
    public delegate void ArgumentContainsDelegate(object sender, ContentEventArgs e);
    public class Calendar : Control
    {
        public List<TimeTableView> AppointmentClipboard { get; set; }

        public DateTime? CurrentMonday
        {
            get
            {
                var day1 = this.GetTemplateChild("day1") as CalendarDay;
                if (day1 != null) return day1.CurrentDate;
                switch (DateTime.Now.DayOfWeek)
                {
                    case DayOfWeek.Sunday:
                        return DateTime.Now.Date.AddDays(-6);
                    case DayOfWeek.Monday:
                        return DateTime.Now.Date;
                    case DayOfWeek.Tuesday:
                        return DateTime.Now.Date.AddDays(-1);
                    case DayOfWeek.Wednesday:
                        return DateTime.Now.Date.AddDays(-2);
                    case DayOfWeek.Thursday:
                        return DateTime.Now.Date.AddDays(-3);
                    case DayOfWeek.Friday:
                        return DateTime.Now.Date.AddDays(-4);
                    case DayOfWeek.Saturday:
                        return DateTime.Now.Date.AddDays(-5);
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        static Calendar()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Calendar), new FrameworkPropertyMetadata(typeof(Calendar)));

            CommandManager.RegisterClassCommandBinding(typeof(Calendar), new CommandBinding(NextDay, OnExecutedNextDay, OnCanExecuteNextDay));
            CommandManager.RegisterClassCommandBinding(typeof(Calendar), new CommandBinding(PreviousDay, OnExecutedPreviousDay, OnCanExecutePreviousDay));
            CommandManager.RegisterClassCommandBinding(typeof(Calendar), new CommandBinding(ShiftCollectionViewSourceFilter, ShiftCollectionViewSource_OnFilter, OnCanExecuteShift_OnFilter));
        }

        public bool IsDesignTime
        {
            get
            {
                return (Application.Current == null) ||
                       (Application.Current.GetType() == typeof(Application));
            }
        }

        public Calendar()
        {
            if (IsDesignTime) return;
            Resources.MergedDictionaries.Add(new ResourceDictionary
                                                 {
                                                     Source =
                                                         new Uri(
                                                         @"\Controls\WeekCalendar\Controls\weekCalendarStyles.xaml",
                                                         UriKind.RelativeOrAbsolute)
                                                 });
            Style = (Style)Resources.MergedDictionaries[0]["calendarStyle"];
            CalendarDay.LastDayLoadedEvent += lastDayLoadedHandler;
            TimeTableAppointmentManager.AppointmentEditedEvent += appointmentEditedHandler;
            DrawShifts();
        }

        private static void DrawShifts()
        {
            CalendarRepository.Shifts.Add(new ShiftAppointment
                                              {
                                                  Index = 1,
                                                  ShiftType = Tools.Constants.ShiftType.Weekday,
                                                  Subject = "1-я смена",
                                                  StartTime = new DateTime(2011, 2, 20, 7, 00, 00),
                                                  EndTime = new DateTime(2008, 2, 16, 16, 00, 00)
                                              });

            CalendarRepository.Shifts.Add(new ShiftAppointment
                                              {
                                                  Index = 2,
                                                  ShiftType = Tools.Constants.ShiftType.Weekday,
                                                  Subject = "Средняя смена",
                                                  StartTime = new DateTime(2011, 2, 11, 11, 00, 00),
                                                  EndTime = new DateTime(2008, 2, 12, 20, 00, 00)
                                              });
            CalendarRepository.Shifts.Add(new ShiftAppointment
                                              {
                                                  Index = 4,
                                                  ShiftType = Tools.Constants.ShiftType.Weekday,
                                                  Subject = "2-я смена",
                                                  StartTime = new DateTime(2011, 2, 20, 14, 30, 00),
                                                  EndTime = new DateTime(2008, 2, 16, 23, 00, 00)
                                              });
            CalendarRepository.Shifts.Add(new ShiftAppointment
                                              {
                                                  Index = 5,
                                                  ShiftType = Tools.Constants.ShiftType.Weekday,
                                                  Subject = "Ночная смена",
                                                  StartTime = new DateTime(2011, 2, 20, 23, 00, 00),
                                                  EndTime = new DateTime(2008, 2, 16, 23, 59, 00)
                                              });
            CalendarRepository.Shifts.Add(new ShiftAppointment
                                              {
                                                  Index = 5,
                                                  ShiftType = Tools.Constants.ShiftType.Weekday,
                                                  Subject = "Ночная смена",
                                                  StartTime = new DateTime(2011, 3, 20, 0, 00, 00),
                                                  EndTime = new DateTime(2008, 3, 17, 7, 0, 00)
                                              });

            CalendarRepository.Shifts.Add(new ShiftAppointment
                                              {
                                                  Index = 1,
                                                  ShiftType = Tools.Constants.ShiftType.Sunday,
                                                  Subject = "1-я смена (ВС)",
                                                  StartTime = new DateTime(2011, 3, 8, 6, 30, 00),
                                                  EndTime = new DateTime(2008, 3, 17, 15, 0, 00)
                                              });

            CalendarRepository.Shifts.Add(new ShiftAppointment
                                              {
                                                  Index = 2,
                                                  ShiftType = Tools.Constants.ShiftType.Sunday,
                                                  Subject = "2-я смена (ВС)",
                                                  StartTime = new DateTime(2011, 3, 8, 14, 00, 00),
                                                  EndTime = new DateTime(2008, 3, 17, 23, 0, 00)
                                              });
        }

        private void lastDayLoadedHandler(object sender)
        {
            if (!_daysLoaded)
            {
                var filterDelegate = new FilterDelegate(FilterAppointmentsInRightThread);
                this.Dispatcher.Invoke(DispatcherPriority.Normal, filterDelegate);
            }
        }

        private void appointmentEditedHandler(object sender, EventArgs eventArgs)
        {
            var filterDelegate = new FilterDelegate(FilterAppointmentsInRightThread);
            this.Dispatcher.Invoke(DispatcherPriority.Normal, filterDelegate);
        }

        public new void OnApplyTemplate()
        {
            base.OnApplyTemplate();
        }

        #region Shifts

        private static void ShiftCollectionViewSource_OnFiltera(object sender, ExecutedRoutedEventArgs e)
        {
        }

        private static void OnCanExecuteShift_OnFilter(object sender, CanExecuteRoutedEventArgs e)
        {
        }

        private static void ShiftCollectionViewSource_OnFilter(object sender,
                                                               ExecutedRoutedEventArgs executedRoutedEventArgs)
        {
            Tools.Constants.ShiftType item = (Tools.Constants.ShiftType) (executedRoutedEventArgs.Parameter);
        }

        #endregion

        #region AddAppointment

        public static readonly RoutedEvent AddAppointmentEvent =
            CalendarTimeslotItem.AddAppointmentEvent.AddOwner(typeof(CalendarDay));

        public event RoutedEventHandler AddAppointment
        {
            add
            {
                AddHandler(AddAppointmentEvent, value);
            }
            remove
            {
                RemoveHandler(AddAppointmentEvent, value);
            }
        }

        #endregion

        #region Appointments

        public static readonly DependencyProperty AppointmentsProperty =
            DependencyProperty.Register("Appointments", typeof(IEnumerable<Appointment>), typeof(Calendar),
            new FrameworkPropertyMetadata(null, new PropertyChangedCallback(OnAppointmentsChanged)));

        private delegate IEnumerable<Appointment> GetAppointmentsDelegate();
        public IEnumerable<Appointment> Appointments
        {
            get
            {
                var getAppointmentsDelegate = new GetAppointmentsDelegate(GetAppointments);
                return (IEnumerable<Appointment>)this.Dispatcher.Invoke(DispatcherPriority.Normal, getAppointmentsDelegate);
            }
            set { SetValue(AppointmentsProperty, value); }
        }

        private IEnumerable<Appointment> GetAppointments()
        {
            return (IEnumerable<Appointment>)GetValue(AppointmentsProperty);
        }

        private static void OnAppointmentsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((Calendar)d).OnAppointmentsChanged(e);
        }

        private delegate void FilterDelegate();
        protected virtual void OnAppointmentsChanged(DependencyPropertyChangedEventArgs e)
        {
            this.Dispatcher.Invoke((Action)FilterAppointmentsInRightThread);
        }

        private void FilterAppointmentsInRightThread()
        {
            if (!_loadingTimeTable)
                FilterAppointments();
        }

        #endregion

        #region Clipboard

        private string _textClipboard = string.Empty;
        private List<TimeTableView> GetTimeTablesByDate(DateTime date, List<TimeTableView> timeTableViews)
        {
            foreach (var appointment in Appointments.ByDate(date))
            {
                if (appointment == null) continue;

                var exist = timeTableViews.FirstOrDefault(a => a.Id == appointment.TimeTableId);
                if (exist == null)
                {
                    var timeTableView = TimeTableManager.Clone(Repository.TimeTables.FirstOrDefault(a => a.Id == appointment.TimeTableId));
                    if (timeTableView == null) continue;

                    _textClipboard = string.Format("{0}{1} {2} {3}-{4} \n", 
                        _textClipboard, timeTableView.User.Title,
                                                  timeTableView.StartDate.ToString("dd.MM.yyyy"),
                                                  timeTableView.StartDate.ToString("HH:mm"),
                                                  timeTableView.EndDate.ToString("HH:mm"));

                    timeTableViews.Add(timeTableView);
                }
            }
            return timeTableViews;
        }

        public bool CopyThisWeek()
        {
            if (AppointmentClipboard == null)
                AppointmentClipboard = new List<TimeTableView>();
            else
                AppointmentClipboard.Clear();
            _textClipboard = string.Empty;

            var day = GetTemplateChild("day1") as CalendarDay;
            if (day != null)
            {
                _daysLoaded = true;
                AppointmentClipboard = GetTimeTablesByDate(day.CurrentDate, AppointmentClipboard);
                var day2 = GetTemplateChild("day2") as CalendarDay;
                if (day2 != null) AppointmentClipboard = GetTimeTablesByDate(day2.CurrentDate, AppointmentClipboard);
                var day3 = GetTemplateChild("day3") as CalendarDay;
                if (day3 != null)
                    AppointmentClipboard = GetTimeTablesByDate(day3.CurrentDate, AppointmentClipboard);
                var day4 = GetTemplateChild("day4") as CalendarDay;
                if (day4 != null)
                    AppointmentClipboard = GetTimeTablesByDate(day4.CurrentDate, AppointmentClipboard);
                var day5 = GetTemplateChild("day5") as CalendarDay;
                if (day5 != null)
                    AppointmentClipboard = GetTimeTablesByDate(day5.CurrentDate, AppointmentClipboard);
                var day6 = GetTemplateChild("day6") as CalendarDay;
                if (day6 != null)
                    AppointmentClipboard = GetTimeTablesByDate(day6.CurrentDate, AppointmentClipboard);
                var day7 = GetTemplateChild("day7") as CalendarDay;
                if (day7 != null)
                    AppointmentClipboard = GetTimeTablesByDate(day7.CurrentDate, AppointmentClipboard);
            }
            try
            {
                if (!string.IsNullOrEmpty(_textClipboard))
                {
                    Clipboard.Clear();
                    Clipboard.SetText(_textClipboard);
                }
            }
            catch (Exception e)
            {}
            return AppointmentClipboard.Count > 0;
        }

        public void ClearCurrentWeek()
        {
            if (CurrentMonday.HasValue)
            {
                DateTime curSunday = (CurrentMonday + TimeSpan.FromDays(6)).Value.Date;
                List<TimeTableView> timeTablesToDelete =
                    Repository.TimeTables.Where(
                        a => a.StartDate.Date <= curSunday && a.StartDate.Date >= CurrentMonday.Value.Date).ToList();
                foreach (var timeTableView in timeTablesToDelete)
                {
                    TimeTableAppointmentManager.DeleteTimeTableRedrawAppointment(timeTableView.Id, true);
                }
            }
        }

        #endregion

        public void FilterAppointments()
        {
            //DateTime byDate = CurrentDate;

            CalendarDay day = this.GetTemplateChild("day1") as CalendarDay;
            
            if(day != null)
            {
                Debug.WriteLine("Filtering timetable..");
                _daysLoaded = true;
                if (day.CurrentDate == new DateTime())
                    SetDefaultDays();

                day.ItemsSource = Appointments.ByDate(day.CurrentDate).OrderBy(t => t.StartTime).ThenBy(t => t.Subject);
                
                var day2 = this.GetTemplateChild("day2") as CalendarDay;
                day2.ItemsSource = Appointments.ByDate(day2.CurrentDate).OrderBy(t => t.StartTime).ThenBy(t => t.Subject);

                var day3 = this.GetTemplateChild("day3") as CalendarDay;
                day3.ItemsSource = Appointments.ByDate(day3.CurrentDate).OrderBy(t => t.StartTime).ThenBy(t => t.Subject);

                var day4 = this.GetTemplateChild("day4") as CalendarDay;
                day4.ItemsSource = Appointments.ByDate(day4.CurrentDate).OrderBy(t => t.StartTime).ThenBy(t => t.Subject);

                var day5 = this.GetTemplateChild("day5") as CalendarDay;
                day5.ItemsSource = Appointments.ByDate(day5.CurrentDate).OrderBy(t => t.StartTime).ThenBy(t => t.Subject);

                var day6 = this.GetTemplateChild("day6") as CalendarDay;
                day6.ItemsSource = Appointments.ByDate(day6.CurrentDate).OrderBy(t => t.StartTime).ThenBy(t => t.Subject);

                var day7 = this.GetTemplateChild("day7") as CalendarDay;
                day7.ItemsSource = Appointments.ByDate(day7.CurrentDate).OrderBy(t => t.StartTime).ThenBy(t => t.Subject);
            }
        }

        #region NextDay/PreviousDay

        public static readonly RoutedCommand NextDay = new RoutedCommand("NextDay", typeof(Calendar));
        public static readonly RoutedCommand PreviousDay = new RoutedCommand("PreviousDay", typeof(Calendar));
        public static readonly RoutedCommand ShiftCollectionViewSourceFilter = new RoutedCommand("ShiftCollectionViewSourceFilter", typeof(Calendar));

        private bool _daysLoaded = false;
        private static bool _loadingTimeTable;

        private static void OnCanExecuteNextDay(object sender, CanExecuteRoutedEventArgs e)
        {
            ((Calendar)sender).OnCanExecuteNextDay(e);
        }

        private static void OnExecutedNextDay(object sender, ExecutedRoutedEventArgs e)
        {
            ((Calendar)sender).OnExecutedNextDay(e);
        }

        protected virtual void OnCanExecuteNextDay(CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
            e.Handled = false;
        }

        public event ArgumentContainsDelegate ChangeWeekHandler;
        public static event ArgumentContainsDelegate WeekLoadedHandler;

        protected virtual void OnExecutedNextDay(ExecutedRoutedEventArgs e)
        {
            var day1 = GetTemplateChild("day1") as CalendarDay;
            var day2 = GetTemplateChild("day2") as CalendarDay;
            var day3 = GetTemplateChild("day3") as CalendarDay;
            var day4 = GetTemplateChild("day4") as CalendarDay;
            var day5 = GetTemplateChild("day5") as CalendarDay;
            var day6 = GetTemplateChild("day6") as CalendarDay;
            var day7 = GetTemplateChild("day7") as CalendarDay;

            day1.CurrentDate += TimeSpan.FromDays(7);
            day2.CurrentDate += TimeSpan.FromDays(7);
            day3.CurrentDate += TimeSpan.FromDays(7);
            day4.CurrentDate += TimeSpan.FromDays(7);
            day5.CurrentDate += TimeSpan.FromDays(7);
            day6.CurrentDate += TimeSpan.FromDays(7);
            day7.CurrentDate += TimeSpan.FromDays(7);

            BackgroundTask bg = new BackgroundTask(LoadWeekTimeTables, LoadWeekTimeTablesComplete);
            bg.Run(day1.CurrentDate.Date);
            e.Handled = true;
        }

        private void LoadWeekTimeTablesComplete(object sender, RunWorkerCompletedEventArgs e)
        {
            FilterAppointments();
            InvokeWeekLoaded();
            _loadingTimeTable = false;
        }

        private static void LoadWeekTimeTables(object sender, DoWorkEventArgs e)
        {
            _loadingTimeTable = true;
            var monday = (DateTime)e.Argument;
            Repository.TimeTableLoadManager.LoadTimeTableOnWeek(monday);
        }

        public static void InvokeWeekLoaded()
        {
            var handler = WeekLoadedHandler;
            if (handler != null) handler(null, new ContentEventArgs(null));
        }

        public void InvokeLunchFormChangeState(DateTime e)
        {
            var handler = ChangeWeekHandler;
            if (handler != null) handler(null, new ContentEventArgs(e));
        }

        private static void OnCanExecutePreviousDay(object sender, CanExecuteRoutedEventArgs e)
        {
            ((Calendar)sender).OnCanExecutePreviousDay(e);
        }

        private static void OnExecutedPreviousDay(object sender, ExecutedRoutedEventArgs e)
        {
            ((Calendar)sender).OnExecutedPreviousDay(e);
        }

        protected virtual void OnCanExecutePreviousDay(CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
            e.Handled = false;
        }

        protected virtual void OnExecutedPreviousDay(ExecutedRoutedEventArgs e)
        {
            var day1 = this.GetTemplateChild("day1") as CalendarDay;
            var day2 = this.GetTemplateChild("day2") as CalendarDay;
            var day3 = this.GetTemplateChild("day3") as CalendarDay;
            var day4 = this.GetTemplateChild("day4") as CalendarDay;
            var day5 = this.GetTemplateChild("day5") as CalendarDay;
            var day6 = this.GetTemplateChild("day6") as CalendarDay;
            var day7 = this.GetTemplateChild("day7") as CalendarDay;

            day1.CurrentDate -= TimeSpan.FromDays(7);
            day2.CurrentDate -= TimeSpan.FromDays(7);
            day3.CurrentDate -= TimeSpan.FromDays(7);
            day4.CurrentDate -= TimeSpan.FromDays(7);
            day5.CurrentDate -= TimeSpan.FromDays(7);
            day6.CurrentDate -= TimeSpan.FromDays(7);
            day7.CurrentDate -= TimeSpan.FromDays(7);

            var bg = new BackgroundTask(LoadWeekTimeTables, LoadWeekTimeTablesComplete);
            bg.Run(day1.CurrentDate);
            e.Handled = true;
        }

        #endregion

        private void SetDefaultDays()
        {
            var day1 = GetTemplateChild("day1") as CalendarDay;
            var day2 = GetTemplateChild("day2") as CalendarDay;
            var day3 = GetTemplateChild("day3") as CalendarDay;
            var day4 = GetTemplateChild("day4") as CalendarDay;
            var day5 = GetTemplateChild("day5") as CalendarDay;
            var day6 = GetTemplateChild("day6") as CalendarDay;
            var day7 = GetTemplateChild("day7") as CalendarDay;
            DayOfWeek nowDay = DateTime.Now.DayOfWeek;

            var curDate = DateTime.Now;

            switch (nowDay)
            {
                case (DayOfWeek.Monday):
                    day1.CurrentDate = curDate;
                    day2.CurrentDate = curDate + TimeSpan.FromDays(1);
                    day3.CurrentDate = curDate + TimeSpan.FromDays(2);
                    day4.CurrentDate = curDate + TimeSpan.FromDays(3);
                    day5.CurrentDate = curDate + TimeSpan.FromDays(4);
                    day6.CurrentDate = curDate + TimeSpan.FromDays(5);
                    day7.CurrentDate = curDate + TimeSpan.FromDays(6);
                    break;
                case (DayOfWeek.Tuesday):
                    day1.CurrentDate = curDate - TimeSpan.FromDays(1);
                    day2.CurrentDate = curDate;
                    day3.CurrentDate = curDate + TimeSpan.FromDays(1);
                    day4.CurrentDate = curDate + TimeSpan.FromDays(2);
                    day5.CurrentDate = curDate + TimeSpan.FromDays(3);
                    day6.CurrentDate = curDate + TimeSpan.FromDays(4);
                    day7.CurrentDate = curDate + TimeSpan.FromDays(5);
                    break;
                case (DayOfWeek.Wednesday):
                    day1.CurrentDate = curDate- TimeSpan.FromDays(2);
                    day2.CurrentDate = curDate- TimeSpan.FromDays(1);
                    day3.CurrentDate = curDate;
                    day4.CurrentDate = curDate+ TimeSpan.FromDays(1);
                    day5.CurrentDate = curDate+ TimeSpan.FromDays(2);
                    day6.CurrentDate = curDate+ TimeSpan.FromDays(3);
                    day7.CurrentDate = curDate+ TimeSpan.FromDays(4);
                    break;
                case (DayOfWeek.Thursday):
                    day1.CurrentDate = curDate - TimeSpan.FromDays(3);
                    day2.CurrentDate = curDate - TimeSpan.FromDays(2);
                    day3.CurrentDate = curDate - TimeSpan.FromDays(1);
                    day4.CurrentDate = curDate;
                    day5.CurrentDate = curDate + TimeSpan.FromDays(1);
                    day6.CurrentDate = curDate + TimeSpan.FromDays(2);
                    day7.CurrentDate = curDate + TimeSpan.FromDays(3);
                    break;
                case (DayOfWeek.Friday):
                    day1.CurrentDate = curDate - TimeSpan.FromDays(4);
                    day2.CurrentDate = curDate - TimeSpan.FromDays(3);
                    day3.CurrentDate = curDate - TimeSpan.FromDays(2);
                    day4.CurrentDate = curDate - TimeSpan.FromDays(1);
                    day5.CurrentDate = curDate;
                    day6.CurrentDate = curDate + TimeSpan.FromDays(1);
                    day7.CurrentDate = curDate + TimeSpan.FromDays(2);
                    break;
                case (DayOfWeek.Sunday):
                   day1.CurrentDate =  curDate - TimeSpan.FromDays(6);
                    day2.CurrentDate = curDate - TimeSpan.FromDays(5);
                    day3.CurrentDate = curDate - TimeSpan.FromDays(4);
                    day4.CurrentDate = curDate - TimeSpan.FromDays(3);
                    day5.CurrentDate = curDate - TimeSpan.FromDays(2);
                    day6.CurrentDate = curDate - TimeSpan.FromDays(1);
                    day7.CurrentDate = curDate;
                    break;
                case (DayOfWeek.Saturday):
                    day1.CurrentDate = curDate - TimeSpan.FromDays(5);
                    day2.CurrentDate = curDate - TimeSpan.FromDays(4);
                    day3.CurrentDate = curDate - TimeSpan.FromDays(3);
                    day4.CurrentDate = curDate - TimeSpan.FromDays(2);
                    day5.CurrentDate = curDate - TimeSpan.FromDays(1);
                    day6.CurrentDate = curDate;
                    day7.CurrentDate = curDate + TimeSpan.FromDays(1);
                    break;
            }
        }

        public void Unsign()
        {
            CalendarDay.LastDayLoadedEvent -= lastDayLoadedHandler;
            TimeTableAppointmentManager.AppointmentEditedEvent -= appointmentEditedHandler;
        }
    }
}
