﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace Tasker.Manager.Controls.WeekCalendar.Controls
{
    public class CalendarLedger : Control
    {
        static CalendarLedger()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(CalendarLedger), new FrameworkPropertyMetadata(typeof(CalendarLedger)));
        }

        public CalendarLedger()
        {
            this.Resources.MergedDictionaries.Add(new ResourceDictionary() { Source = new Uri(@"\Controls\WeekCalendar\Controls\weekCalendarStyles.xaml", UriKind.RelativeOrAbsolute) });
            var tt = this.Resources.MergedDictionaries[0]["calendarLedgerStyle"];
            Style = (Style)tt;
            
            //this.Resources.MergedDictionaries.Add(new ResourceDictionary() { Source = new Uri("Generic.xaml", UriKind.RelativeOrAbsolute) });
        }
    }
}
