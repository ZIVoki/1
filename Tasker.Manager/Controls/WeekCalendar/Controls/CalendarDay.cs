﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using Tasker.Manager.Controls.WeekCalendar.Model;

namespace Tasker.Manager.Controls.WeekCalendar.Controls
{
    public delegate void EventHandler(object sender);
    public class CalendarDay : ItemsControl
    {
        public static event EventHandler LastDayLoadedEvent;
        
        static CalendarDay()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(CalendarDay), new FrameworkPropertyMetadata(typeof(CalendarDay)));
        }
        public CalendarDay()
        {
            Loaded += initializedHandled;
        }

        private static void InvokeLastDayLoadedEventHandler()
        {
            EventHandler handler = LastDayLoadedEvent;
            if (handler != null) handler(null);
        }

        private void initializedHandled(object sender, EventArgs e)
        {
            if (Style == null)
            {
                this.Resources.MergedDictionaries.Add(new ResourceDictionary
                                                          {
                                                              Source =
                                                                  new Uri(
                                                                  @"\Controls\WeekCalendar\Controls\weekCalendarStyles.xaml",
                                                                  UriKind.RelativeOrAbsolute)
                                                          });
                var tt = this.Resources.MergedDictionaries[0]["calendarDayStyle"];
                Style = (Style)tt;
            }
            var name = this.Name;
          /*  if(name == "day1")
            {
                CurrentDate = new DateTime(2011, 2, 14);
            }
            else if (name == "day2")
            {
                CurrentDate = new DateTime(2011, 2, 15);
                
            }
            else if (name == "day3")
            {
                CurrentDate = new DateTime(2011, 2, 16);
            }
            else if (name == "day4")
            {
                CurrentDate = new DateTime(2011, 2, 17);
            }
            else if (name == "day5")
            {
                CurrentDate = new DateTime(2011, 2, 18);
            }
            else if (name == "day6")
            {
                CurrentDate = new DateTime(2011, 2, 19);
            }*/
            if (name == "day7")
            {
                //CurrentDate = new DateTime(2011, 2, 20);
                InvokeLastDayLoadedEventHandler();
            }
            
        }

        #region CurrentDate

        /// <summary>
        /// CurrentDate Dependency Property
        /// </summary>
        public static readonly DependencyProperty CurrentDateProperty =
            DependencyProperty.Register("CurrentDate", typeof(DateTime), typeof(CalendarDay),
                new FrameworkPropertyMetadata(new DateTime(),
                    new PropertyChangedCallback(OnCurrentDateChanged)));

        /// <summary>
        /// Gets or sets the CurrentDate property.  This dependency property 
        /// indicates ....
        /// </summary>
        private delegate DateTime CurrentDateDelegate();
        public DateTime CurrentDate
        {
            get
            {
                var getCurrentDateDelegate = new CurrentDateDelegate(getCurrentDate);
                return (DateTime)this.Dispatcher.Invoke(DispatcherPriority.Normal, getCurrentDateDelegate);

                
            }
            set
            {
                SetValue(CurrentDateProperty, value);
            }
        }

        private DateTime getCurrentDate()
        {
            return (DateTime)GetValue(CurrentDateProperty);
        }

        /// <summary>
        /// Handles changes to the CurrentDate property.
        /// </summary>
        private static void OnCurrentDateChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((CalendarDay)d).OnCurrentDateChanged(e);
        }

        /// <summary>
        /// Provides derived classes an opportunity to handle changes to the CurrentDate property.
        /// </summary>
        protected virtual void OnCurrentDateChanged(DependencyPropertyChangedEventArgs e)
        {
            //FilterAppointments();
            //drop event here

        }

        #endregion             

        #region StartTime/EndTime

        public static readonly DependencyProperty HighlightStartTimeProperty =
            TimeslotPanel.StartTimeProperty.AddOwner(typeof(CalendarDay));

        public DateTime? HighlightStartTime
        {
            get { return (DateTime?)GetValue(HighlightStartTimeProperty); }
            set { SetValue(HighlightStartTimeProperty, value); }
        }

        public static readonly DependencyProperty HightlightEndTimeProperty =
            TimeslotPanel.EndTimeProperty.AddOwner(typeof(CalendarDay));

        public DateTime? HighlightEndTime
        {
            get { return (DateTime?)GetValue(HightlightEndTimeProperty); }
            set { SetValue(HightlightEndTimeProperty, value); }
        }

        #endregion


        #region ItemsControl Container Override

        protected override DependencyObject GetContainerForItemOverride()
        {
            return new CalendarAppointmentItem();
        }

        protected override  void PrepareContainerForItemOverride(DependencyObject element, object item)
        {
            CalendarAppointmentItem calAppCont = ((CalendarAppointmentItem) element);
            calAppCont.EndTime = ((Appointment)item).EndTime;
            calAppCont.StartTime = ((Appointment)item).StartTime;
            //calAppCont.IsHighlight = ((Appointment)item).IsHighlight;
            //base.PrepareContainerForItemOverride(element, item);
        }

        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            return (item is CalendarAppointmentItem);
        }

        #endregion
    }
}
