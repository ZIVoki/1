﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Tasker.Manager.Controls.WeekCalendar.Model;
using Tasker.Manager.Managers;

namespace Tasker.Manager.Controls.WeekCalendar.Controls
{
    public class CalendarAppointmentItem : ContentControl
    {
        
        public static readonly RoutedCommand AppointmentEdit = new RoutedCommand("AppointmentEdit", typeof(CalendarAppointmentItem));
        public static readonly RoutedCommand AppointmentDelete = new RoutedCommand("AppointmentDelete", typeof(CalendarAppointmentItem));

        static CalendarAppointmentItem()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(CalendarAppointmentItem), new FrameworkPropertyMetadata(typeof(CalendarAppointmentItem)));
            CommandManager.RegisterClassCommandBinding(typeof(CalendarAppointmentItem), new CommandBinding(AppointmentEdit, ContextMenu_OnEdit, ContextMenu_CanExecute));
            CommandManager.RegisterClassCommandBinding(typeof(CalendarAppointmentItem), new CommandBinding(AppointmentDelete, ContextMenu_OnDelete));
        }

        private static void ContextMenu_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
            e.Handled = false;
        }

        private static void ContextMenu_OnDelete(object sender, ExecutedRoutedEventArgs e)
        {
            var cai = ((CalendarAppointmentItem)sender);
            Appointment appointment = (Appointment)cai.DataContext;
            TimeTableAppointmentManager.DeleteTimeTableRedrawAppointment(appointment.TimeTableId, true);
        }

        private static void ContextMenu_OnEdit(object sender, ExecutedRoutedEventArgs e)
        {
            var cai = ((CalendarAppointmentItem)sender);
            Appointment appointment = (Appointment)cai.DataContext;
            TimeTableAppointmentManager.EditTimeTableRedrawAppointment(appointment);
        }

        public CalendarAppointmentItem()
        {
            
            this.Resources.MergedDictionaries.Add(new ResourceDictionary { Source = new Uri(@"\Controls\WeekCalendar\Controls\weekCalendarStyles.xaml", UriKind.RelativeOrAbsolute) });
            Loaded += loaded;

            //this.Resources.MergedDictionaries.Add(new ResourceDictionary() { Source = new Uri("Generic.xaml", UriKind.RelativeOrAbsolute) });
        }

        private void loaded(object sender, RoutedEventArgs e)
        {
            if (Style == null)
            {
                var tt = this.Resources.MergedDictionaries[0]["CalendarAppointmentItemStyle"];
                Style = (Style)tt;
            }
        }

        #region StartTime/EndTime

        public static readonly DependencyProperty StartTimeProperty =
            TimeslotPanel.StartTimeProperty.AddOwner(typeof(CalendarAppointmentItem));

        public DateTime StartTime
        {
            get { return (DateTime)GetValue(StartTimeProperty); }
            set
            {
                SetValue(StartTimeProperty, value);
            }
        }

        public static readonly DependencyProperty EndTimeProperty =
            TimeslotPanel.EndTimeProperty.AddOwner(typeof(CalendarAppointmentItem));

        public DateTime EndTime
        {
            get { return (DateTime)GetValue(EndTimeProperty); }
            set
            {
                SetValue(EndTimeProperty, value);
            }
        }

        #endregion
        
    }
}
