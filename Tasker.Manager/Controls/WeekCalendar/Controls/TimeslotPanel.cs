﻿using System;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace Tasker.Manager.Controls.WeekCalendar.Controls
{
    public class TimeslotPanel : Panel//, IScrollInfo 
    {

        private static Size InfiniteSize = new Size(double.PositiveInfinity, double.PositiveInfinity);

        private bool _CanHorizontallyScroll;
        private bool _CanVerticallyScroll;
        private ScrollViewer _ScrollOwner;
        private Vector _Offset;
        private Size _Extent;
        private Size _Viewport;


        
        #region StartTime

        /// <summary>
        /// StartTime Attached Dependency Property
        /// </summary>
        public static readonly DependencyProperty StartTimeProperty =
            DependencyProperty.RegisterAttached("StartTime", typeof(DateTime), typeof(TimeslotPanel),
                new FrameworkPropertyMetadata((DateTime)DateTime.Now));

        /// <summary>
        /// Gets the StartTime property.  This dependency property 
        /// indicates ....
        /// </summary>
        public static DateTime GetStartTime(DependencyObject d)
        {
            return (DateTime)d.GetValue(StartTimeProperty);
        }

        /// <summary>
        /// Sets the StartTime property.  This dependency property 
        /// indicates ....
        /// </summary>
        public static void SetStartTime(DependencyObject d, DateTime value)
        {
            d.SetValue(StartTimeProperty, value);
        }

        #endregion

        #region EndTime

        /// <summary>
        /// EndTime Attached Dependency Property
        /// </summary>
        public static readonly DependencyProperty EndTimeProperty =
            DependencyProperty.RegisterAttached("EndTime", typeof(DateTime), typeof(TimeslotPanel),
                new FrameworkPropertyMetadata((DateTime)DateTime.Now));

        /// <summary>
        /// Gets the EndTime property.  This dependency property 
        /// indicates ....
        /// </summary>
        public static DateTime GetEndTime(DependencyObject d)
        {
            return (DateTime)d.GetValue(EndTimeProperty);
        }

        /// <summary>
        /// Sets the EndTime property.  This dependency property 
        /// indicates ....
        /// </summary>
        public static void SetEndTime(DependencyObject d, DateTime value)
        {
            d.SetValue(EndTimeProperty, value);
        }

        #endregion
        

        protected override Size MeasureOverride(Size availableSize)
        {
            // Calculate size based on duration?
            //Size size = new Size(double.PositiveInfinity, double.PositiveInfinity);

            /*foreach (UIElement element in this.Children)
            {
                element.Measure(size);
            }*/
            Size size = new Size(double.PositiveInfinity, double.PositiveInfinity);
            double curX = 0;
            foreach (UIElement child in Children)
            {
                //child.Measure(InfiniteSize);
                child.Measure(size);
                curX += child.DesiredSize.Width;
            }

            VerifyScrollData(availableSize, new Size(curX, 0));

            return _Viewport;
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
            if (this.Children == null || this.Children.Count == 0)
            { return finalSize; }

            double left = 0;
            double curX = 0;
            
            var i = 0;
            foreach (UIElement element in this.Children)
            {
                var trans = element.RenderTransform as TranslateTransform;
                if (trans == null)
                {
                    element.RenderTransformOrigin = new Point(0, 0);
                    trans = new TranslateTransform();
                    element.RenderTransform = trans;
                }

                var startTime = element.GetValue(TimeslotPanel.StartTimeProperty) as Nullable<DateTime>;
                var endTime = element.GetValue(TimeslotPanel.EndTimeProperty) as Nullable<DateTime>;

                var start_minutes = (startTime.Value.Hour * 60) + startTime.Value.Minute;
                var end_minutes = (endTime.Value.Hour * 60) + endTime.Value.Minute;
                var start_offset = (finalSize.Height / (24 * 60)) * start_minutes;
                var end_offset = (finalSize.Height / (24 * 60)) * end_minutes;

                var top = start_offset;

                var width = finalSize.Width;


                var height = (end_offset - start_offset) ;

                

                //element.Arrange(new Rect(left, top, width, height));
                if(height < 0)
                {
                    height = 10;
                }
                if(height < 0)
                {
                    height = 10;
                }
                element.Arrange(new Rect(0, top, width, height));

                if (!Equals(((CalendarAppointmentItem)element).Tag, "shift"))
                {
                    trans.X = curX - HorizontalOffset;
                    curX += element.DesiredSize.Width;
                }
            }
            VerifyScrollData(finalSize, new Size(curX, 0));
            return finalSize;
        }

        #region Movement Methods
        public void LineDown()
        { }

        public void LineUp()
        { }

        public void LineLeft()
        { }

        public void LineRight()
        { }

        public void MouseWheelDown()
        { }

        public void MouseWheelUp()
        { }

        public void MouseWheelLeft()
        { }

        public void MouseWheelRight()
        { }

        public void PageDown()
        { SetVerticalOffset(VerticalOffset + ViewportHeight); }

        public void PageUp()
        { SetVerticalOffset(VerticalOffset - ViewportHeight); }

        public void PageLeft()
        { SetHorizontalOffset(HorizontalOffset - ViewportWidth); }

        public void PageRight()
        { SetHorizontalOffset(HorizontalOffset + ViewportWidth); }
        #endregion

        #region scroll

        public ScrollViewer ScrollOwner
        {
            get { return _ScrollOwner; }
            set { _ScrollOwner = value; }
        }

        public bool CanHorizontallyScroll
        {
            get { return _CanHorizontallyScroll; }
            set { _CanHorizontallyScroll = value; }
        }

        public bool CanVerticallyScroll
        {
            get { return _CanVerticallyScroll; }
            set { _CanVerticallyScroll = value; }
        }

        public double ExtentHeight
        { get { return _Extent.Height; } }

        public double ExtentWidth
        { get { return _Extent.Width; } }

        public double HorizontalOffset
        { get { return _Offset.X; } }

        public double VerticalOffset
        { get { return _Offset.Y; } }

        public double ViewportHeight
        { get { return _Viewport.Height; } }

        public double ViewportWidth
        { get { return _Viewport.Width; } }

        public Rect MakeVisible(Visual visual, Rect rectangle)
        {
            if (rectangle.IsEmpty || visual == null
              || visual == this || !base.IsAncestorOf(visual))
            { return Rect.Empty; }

            rectangle = visual.TransformToAncestor(this).TransformBounds(rectangle);

            Rect viewRect = new Rect(HorizontalOffset,
              VerticalOffset, ViewportWidth, ViewportHeight);
            rectangle.X += viewRect.X;
            //rectangle.Y += viewRect.Y;
            viewRect.X = CalculateNewScrollOffset(viewRect.Left,
              viewRect.Right, rectangle.Left, rectangle.Right);
            //viewRect.Y = CalculateNewScrollOffset(viewRect.Top, 
            // viewRect.Bottom, rectangle.Top, rectangle.Bottom);
            SetHorizontalOffset(viewRect.X);
            SetVerticalOffset(viewRect.Y);
            rectangle.Intersect(viewRect);
            rectangle.X -= viewRect.X;
            //rectangle.Y -= viewRect.Y;

            return rectangle;
        }

        private static double CalculateNewScrollOffset(double topView,
          double bottomView, double topChild, double bottomChild)
        {
            bool offBottom = topChild < topView && bottomChild < bottomView;
            bool offTop = bottomChild > bottomView && topChild > topView;
            bool tooLarge = (bottomChild - topChild) > (bottomView - topView);

            if (!offBottom && !offTop)
            { return topView; } //Don't do anything, already in view

            if ((offBottom && !tooLarge) || (offTop && tooLarge))
            { return topChild; }

            return (bottomChild - (bottomView - topView));
        }

        protected void VerifyScrollData(Size viewport, Size extent)
        {
            if (double.IsInfinity(viewport.Width))
            { viewport.Width = extent.Width; }

            if (double.IsInfinity(viewport.Height))
            { viewport.Height = extent.Height; }

            _Extent = extent;
            _Viewport = viewport;

            _Offset.X = Math.Max(0,
              Math.Min(_Offset.X, ExtentWidth - ViewportWidth));

            if (ScrollOwner != null)
            { ScrollOwner.InvalidateScrollInfo(); }
        }

        public void SetHorizontalOffset(double offset)
        {
            offset = Math.Max(0,
              Math.Min(offset, ExtentWidth - ViewportWidth));
            if (offset != _Offset.Y)
            {
                _Offset.X = offset;
                InvalidateArrange();
            }
        }

        public void SetVerticalOffset(double offset)
        {
            offset = Math.Max(0,
              Math.Min(offset, ExtentHeight - ViewportHeight));
            if (offset != _Offset.Y)
            {
                _Offset.Y = offset;
                InvalidateArrange();
            }
        }

        #endregion










    }
}
