﻿using System;
using System.Windows;
using System.Windows.Controls.Primitives;

namespace Tasker.Manager.Controls.WeekCalendar.Controls
{
    public class CalendarTimeslotItem : ButtonBase
    {
        static CalendarTimeslotItem()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(CalendarTimeslotItem), new FrameworkPropertyMetadata(typeof(CalendarTimeslotItem)));
        }

        public CalendarTimeslotItem()
        {
            this.Resources.MergedDictionaries.Add(new ResourceDictionary
                                                      {
                                                          Source =
                                                              new Uri(
                                                              @"\Controls\WeekCalendar\Controls\weekCalendarStyles.xaml",
                                                              UriKind.RelativeOrAbsolute)
                                                      });
            var tt = this.Resources.MergedDictionaries[0]["calendarTimeslotItemStyle"];
            Style = (Style)tt;
            //this.Resources.MergedDictionaries.Add(new ResourceDictionary() { Source = new Uri("Generic.xaml", UriKind.RelativeOrAbsolute) });
        }

        #region AddAppointment

        private void RaiseAddAppointmentEvent()
        {
            OnAddAppointment(new RoutedEventArgs(AddAppointmentEvent, this));
        }

        public static readonly RoutedEvent AddAppointmentEvent = 
            EventManager.RegisterRoutedEvent("AddAppointment", RoutingStrategy.Bubble, 
            typeof(RoutedEventHandler), typeof(CalendarTimeslotItem));

        public event RoutedEventHandler AddAppointment
        {
            add
            {
                AddHandler(AddAppointmentEvent, value);
            }
            remove
            {
                RemoveHandler(AddAppointmentEvent, value);
            }
        }

        protected virtual void OnAddAppointment(RoutedEventArgs e)
        {
            RaiseEvent(e);
        }

        #endregion

        protected override void OnClick()
        {
            base.OnClick();

            RaiseAddAppointmentEvent();
        }
    }
}
