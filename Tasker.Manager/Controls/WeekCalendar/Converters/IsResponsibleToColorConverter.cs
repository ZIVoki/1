﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace Tasker.Manager.Controls.WeekCalendar.Converters
{
    public class IsResponsibleToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value is bool)
            {
                var isresponsible = (bool)value;
                if (isresponsible)
                {
                    var bc = new BrushConverter();
                    return (Brush)bc.ConvertFrom("#CC0000");
                }
                else
                {
                    var bc = new BrushConverter();
                    return (Brush)bc.ConvertFrom("#5D8CC9");
                }
                
            }
            return 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
