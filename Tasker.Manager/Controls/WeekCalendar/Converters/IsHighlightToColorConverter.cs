﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace Tasker.Manager.Controls.WeekCalendar.Converters
{
    public class IsHighlightToColorConverter : IValueConverter
    {
        #region Implementation of IValueConverter

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value is bool)
            {
                var ishighLight = (bool)value;
                if(ishighLight)
                {
                    var bc = new BrushConverter();
                    return (Brush)bc.ConvertFrom("#FCE899");
                }
                else
                {
                    var bc = new BrushConverter();
                    return (Brush)bc.ConvertFrom("#F1F5E3");
                }
                
            }
            return 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion

        public static T FindAncestor<T>(DependencyObject from) where T : class
        {
            if (from == null)
                return null;

            var candidate = from as T;
            return candidate ?? FindAncestor<T>(VisualTreeHelper.GetParent(from));
        }
    }
}
