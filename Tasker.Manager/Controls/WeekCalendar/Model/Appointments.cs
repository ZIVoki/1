﻿using System.Diagnostics;
using System.Linq;
using System.Collections.ObjectModel;
using Tasker.Tools.Extensions;

namespace Tasker.Manager.Controls.WeekCalendar.Model
{
    public class Appointments : UniqBindableCollection<Appointment>
    {
        public new void Add(Appointment item)
        {
            Debug.WriteLine("Adding new appointment..");
            int dayAppointmentsCount = this.Count(a => a.StartTime.Date == item.StartTime.Date);
            item.Index = dayAppointmentsCount + 1;
            base.Add(item);
        }
    }
}
