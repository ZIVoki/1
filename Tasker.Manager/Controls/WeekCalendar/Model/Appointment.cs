﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Media;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;
using Tasker.Tools.ViewModel.Base;
using Brush = System.Windows.Media.Brush;

namespace Tasker.Manager.Controls.WeekCalendar.Model
{
    public class Appointment : ViewBase
    {
        public Appointment()
        {
            var bc = new BrushConverter();
            HightLightBrush = (Brush)bc.ConvertFrom("#5D8CC9");
        }

        public Appointment(Appointment appointment): this()
        {
            Id = appointment.Id;
            TimeTableId = appointment.Id;
            UserId = appointment.UserId;
            StartTime = appointment.EndTime.Date;
            EndTime = appointment.EndTime;
            Subject = appointment.Subject;
            IsHighlight = appointment.IsHighlight;
            IsResponsable = appointment.IsResponsable;
        }

        public Appointment(TimeTableView timeTableView, bool isHighlight, int adtoid = 0): this()
        {
            Id = timeTableView.Id + adtoid;
            TimeTableId = timeTableView.Id;
            StartTime = timeTableView.StartDate;

            if (timeTableView.EndDate.Date > timeTableView.StartDate.Date)
            {
                if (adtoid == 0)
                    EndTime = timeTableView.StartDate.Date + TimeSpan.FromHours(23.99f);
                else
                {
                    StartTime = timeTableView.EndDate.Date;
                    EndTime = timeTableView.EndDate;
                }
            }
            else
                EndTime = timeTableView.EndDate;

            Subject = timeTableView.User.Title;
            UserId = timeTableView.User.Id;
            IsHighlight = isHighlight;
            IsResponsable = timeTableView.IsResp;

            var bc = new BrushConverter();
            if (timeTableView.User.Role == ConstantsRoles.Manager || timeTableView.User.Role == ConstantsRoles.Administrator)
                HightLightBrush = (Brush)bc.ConvertFrom("#AA3FE6");

            if (timeTableView.IsResp)
                HightLightBrush = (Brush)bc.ConvertFrom("#C95D5D");


            HightLightBrush.Freeze();
        }

        private int _userId;
        public int UserId
        {
            get { return _userId; }
            set
            {
                if (_userId != value)
                {
                    _userId = value;
                    RaisePropertyChanged(() => UserId);
                }
            }
        }

        private bool _isHighlight;
        public bool IsHighlight
        {
            get { return _isHighlight; }
            set
            {
                if (_isHighlight != value)
                {
                    _isHighlight = value;
                    RaisePropertyChanged(() => IsHighlight);
                }
            }
        }

        private bool _isResponsable;
        public bool IsResponsable
        {
            get { return _isResponsable; }
            set
            {
                if (_isResponsable != value)
                {
                    _isResponsable = value;
                    RaisePropertyChanged(() => IsResponsable);
                }
            }
        }

        private Brush _hightLightBrush;

        public Brush HightLightBrush
        {
            get { return _hightLightBrush; }
            set
            {
                RaisePropertyChanged(() => HightLightBrush);
                _hightLightBrush = value;
            }
        }


        private string _subject;
        public string Subject 
        {
            get { return _subject; }
            set
            {
                if (_subject != value)
                {
                    _subject = value;
                    RaisePropertyChanged(() => Subject);
                }
            }
        }

        private int _index;
        public int Index
        {
            get { return _index; }
            set
            {
                if (_index != value)
                {
                    _index = value;
                    RaisePropertyChanged(() => Index);
                }
            }
        }

        private int _timeTableId;
        public int TimeTableId
        {
            get { return _timeTableId; }
            set
            {
                if (_timeTableId != value)
                {
                    _timeTableId = value;
                    RaisePropertyChanged(() => TimeTableId);
                }
            }
        }

       /* private string location;
        public string Location
        {
            get { return location; }
            set
            {
                if (location != value)
                {
                    location = value;
                    RaisePropertyChanged("Location");
                }
            }
        }*/

        private DateTime _startTime;
        public DateTime StartTime
        {
            get { return _startTime; }
            set
            {
                if (_startTime != value)
                {
                    _startTime = value;
                    RaisePropertyChanged(() => StartTime);
                }
            }
        }

        private DateTime _endTime;
        public DateTime EndTime
        {
            get { return _endTime; }
            set
            {
                if (_endTime != value)
                {
                    _endTime = value;
                    RaisePropertyChanged(() => EndTime);
                }
            }
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2}", UserId.ToString(), _startTime.ToString(), _endTime.ToString());
        }
    }
}
