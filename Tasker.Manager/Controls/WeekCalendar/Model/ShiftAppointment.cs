﻿namespace Tasker.Manager.Controls.WeekCalendar.Model
{
    public class ShiftAppointment : Appointment
    {
        private Tools.Constants.ShiftType _shiftType;
        public Tools.Constants.ShiftType ShiftType 
        {
            get { return _shiftType; }
            set
            {
                if (_shiftType != value)
                {
                    _shiftType = value;
                    RaisePropertyChanged(() => ShiftType);
                }
            }
        }
    }
}
