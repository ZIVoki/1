﻿using System.Collections.ObjectModel;
using System.Windows.Data;
using Tasker.Manager.Controls.WeekCalendar.Model;
using Tasker.Manager.Managers;

namespace Tasker.Manager.Controls.WeekCalendar
{
    public static class CalendarRepository
    {
        public static ObservableCollection<ShiftAppointment> Shifts = new ObservableCollection<ShiftAppointment>();
        public static Appointments Appointments = new Appointments();

        public static CollectionViewSource SaturdayShifts;
        public static CollectionViewSource WeekdayShifts;
        public static CollectionViewSource SundayShifts;

        static CalendarRepository()
        {
            SaturdayShifts = new CollectionViewSource { Source = Shifts};
            SaturdayShifts.Filter += SaturdayShiftsFilter_OnFilterEvent;

            WeekdayShifts = new CollectionViewSource { Source = Shifts };
            WeekdayShifts.Filter += WeekdayShiftsFilter_OnFilterEvent;

            SundayShifts = new CollectionViewSource { Source = Shifts };
            SundayShifts.Filter += SundayShiftsFilter_OnFilterEvent;
        }

        private static void SundayShiftsFilter_OnFilterEvent(object sender, FilterEventArgs e)
        {
            var item = ((ShiftAppointment)e.Item).ShiftType;
            if (item != Tools.Constants.ShiftType.Sunday)
            {
                e.Accepted = false;
            }
            else
            {
                e.Accepted = true;
            }
        }

        private static void WeekdayShiftsFilter_OnFilterEvent(object sender, FilterEventArgs e)
        {
            var item = ((ShiftAppointment)e.Item).ShiftType;
            if (item != Tools.Constants.ShiftType.Weekday)
            {
                e.Accepted = false;
            }
            else
            {
                e.Accepted = true;
            }
        }

        private static void SaturdayShiftsFilter_OnFilterEvent(object sender, FilterEventArgs e)
        {
            var item = ((ShiftAppointment)e.Item).ShiftType;
            if(item != Tools.Constants.ShiftType.Saturday)
            {
                e.Accepted = false;
            }
            else
            {
                e.Accepted = true;
            }
        }

        private static int _userIdHighlight;
        public static int UserIdHighlight
        {
            get { return _userIdHighlight; } 
            set
            {
                _userIdHighlight = value;
                TimeTableAppointmentManager.ChangeHighlightAppointments(value);
            }
        }
    }
}
