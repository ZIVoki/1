﻿using System.Windows.Controls;
using System.Windows.Media;

namespace Tasker.Manager.Controls.TaskListHighlight
{
    /// <summary>
    /// Interaction logic for HightlightSettingsControl.xaml
    /// </summary>
    public partial class HightlightSettingsControl : UserControl
    {
        public HightlightSettingsControl()
        {
            InitializeComponent();
        }

        Color[] themeColors = null;

        public Color[] ThemeColors
        {
            get
            {
                if (themeColors == null)
                {
                    themeColors = new Color[10];
                    themeColors[0] = Color.FromRgb(216, 216, 216);
                    themeColors[1] = Color.FromRgb(89, 89, 89);
                    themeColors[2] = Color.FromRgb(196, 189, 151);
                    themeColors[3] = Color.FromRgb(141, 179, 226);
                    themeColors[4] = Color.FromRgb(184, 204, 228);
                    themeColors[5] = Color.FromRgb(229, 185, 183);
                    themeColors[6] = Color.FromRgb(215, 227, 188);
                    themeColors[7] = Color.FromRgb(204, 193, 217);
                    themeColors[8] = Color.FromRgb(183, 221, 232);
                    themeColors[9] = Color.FromRgb(251, 213, 181);
                }

                return themeColors;
            }
        }
    }
}
