﻿using System;
using System.Collections.Generic;
using System.Windows;
using Tasker.Manager.Objects;
using Tasker.Tools.Extensions;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;
using Color = System.Drawing.Color;

namespace Tasker.Manager.Controls.TaskListHighlight
{
    /// <summary>
    /// Interaction logic for HightlightSettingsWindow.xaml
    /// </summary>
    public partial class HightlightSettingsWindow : WindowExt
    {
        public HightlightSettingsWindow()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            CheckAndSaveHighlightColorsToConfig<CustomerView, CustomerDTO>(Repository.Customers, Repository.HighlightSettings.GetCustomerHighlightColor, Repository.HighlightSettings.InsertCustomerColor);
            CheckAndSaveHighlightColorsToConfig<TaskTypeView, TaskTypeDTO>(Repository.TaskTypes, Repository.HighlightSettings.GetTaskTypeHighlightColor, Repository.HighlightSettings.InsertTaskTypeColor);
            this.Close();
        }

        private static void CheckAndSaveHighlightColorsToConfig<T, TD>(IEnumerable<T> collection, Func<int, Color?> getHighlightColorMethod, Action<KeyValuePairSerializable<int, int>> insertColor) where T : HighlightItemView<TD>
        {
            foreach (var item in collection)
            {
                if(item.HighLightBrush.HasValue)
                {
                    if (item.HighLightBrush != Color.White)
                    {
                        var color = getHighlightColorMethod(item.Id);

                        if (color == null || color != item.HighLightBrush)
                        {
                            var itemColor = new KeyValuePairSerializable<int, int>
                                                {
                                                    Key = item.Id, 
                                                    Value = item.HighLightBrush.Value.ToArgb()
                                                };
                            insertColor(itemColor);
                        }
                    }
                }
            }

            foreach (var customer in Repository.Customers)
            {
                customer.EndEdit();
            }
            foreach (var taskType in Repository.TaskTypes)
            {
                taskType.EndEdit();
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            foreach (var customer in Repository.Customers)
            {
                customer.CancelEdit();
                customer.UpdateHighlightBindings();
            }
            foreach (var taskType in Repository.TaskTypes)
            {
                taskType.CancelEdit();
                taskType.UpdateHighlightBindings();
            }
            this.Close();
        }
    }
}
