﻿using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Linq;
using Tasker.Manager.Controls.Task;
using Tasker.Manager.Managers;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Manager.Controls
{
    /// <summary>
    /// Interaction logic for WorkersStateWindow.xaml
    /// </summary>
    public partial class WorkersStateWindow : Window, INotifyPropertyChanged
    {
        readonly SortMachine _currentSortMachine = new SortMachine();
        
        public float Load
        {
            get
            {
                if (!this.IsInitialized) return 0;
                int busyCount = 0;
                int workersCount = 0;
                GetBusyAndWorkersCount(out busyCount, out workersCount);
                if (busyCount == 0) return 0;
                float d = (float) busyCount/workersCount;
                return d*100;
            }
        }

        private void GetBusyAndWorkersCount(out int busyCount, out int workersCount)
        {
            busyCount = 0;
            workersCount = 0;

            if (!this.IsInitialized) return;
            ItemCollection itemCollection = UsersListView.Items;
            
            foreach (var item in itemCollection)
            {
                UserView userView = (UserView)item;
                if (userView.Role == ConstantsRoles.Worker)
                {
                    workersCount++;
                    if (userView.UserWorkState != Tools.Constants.UserWorkState.Free) busyCount++;
                }
            }
        }
        public bool IsDesignTime
        {
            get
            {
                return (Application.Current == null) ||
                       (Application.Current.GetType() == typeof(Application));
            }
        }

        public WorkersStateWindow()
        {
            if (IsDesignTime) return;

            this.Initialized += WorkersStateWindowInitialized;
            InitializeComponent();
            UpdateBusyUsersInfo();

            Repository.Users.CollectionChanged += UsersCollectionChanged;
            ServiceManager.Inst.Callback.WorkerChangedStateEvent += ManagerCallbackWorkerChangedStateEvent;
            ServiceManager.Inst.Callback.TaskReviewCancel += ManagerCallbackTaskReviewCancel;
            ServiceManager.Inst.Callback.TaskUpdatedEvent += ManagerCallbackTaskUpdated;
            ServiceManager.Inst.Callback.TaskFinishedEvent += ManagerCallbackTaskFinished;

            WorkersLoadProgressBar.MouseMove += ShowToolTip;
        }

        private void UpdateBusyUsersInfo()
        {
            Repository.UsersLoadManager.UpdateData();
            foreach (var userView in Repository.Users.Where(u => u.UserWorkState == Tools.Constants.UserWorkState.Busy))
            {
                userView.UserWorkState = Tools.Constants.UserWorkState.Free;
            }
            foreach (var taskView in Repository.Tasks.Where(t => t.Status == ConstantsStatuses.InProcess))
            {
                if (taskView.PriorityType == PriorityType.Quick)
                    taskView.CurrentWorker.UserWorkState = Tools.Constants.UserWorkState.Busy;
                if (taskView.PriorityType == PriorityType.Background)
                    taskView.CurrentWorker.UserWorkState = Tools.Constants.UserWorkState.BackgroundTask;
            }
        }

        void ShowToolTip(object sender, EventArgs e)
        {
            int busyCount = 0;
            int workersCount = 0;
            GetBusyAndWorkersCount(out busyCount, out workersCount);
            var load = 0.0;
            if (busyCount != 0 && workersCount != 0) 
            {
                load = ((float)busyCount / workersCount) * 100;
            }

            var tt = new ToolTip
                         {
                             Content =
                                 string.Format(
                                     "Общее число работников:{0}\n Количество занятых: {1}\n Общий процент занятости: {2:F0}%",
                                     workersCount, busyCount, load)
                         };

            WorkersLoadProgressBar.ToolTip = tt;
        }

        void WorkersStateWindowInitialized(object sender, EventArgs e)
        {
            OnPropertyChanged("Load");
        }
        private delegate void UserStateUpdateDelegate();
        
        void ManagerCallbackTaskUpdated(object sender, TaskUpdatedEventArgs e)
        {
            UserStateUpdateDelegate userStateUpdateDelegate = RefreshView;
            this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, userStateUpdateDelegate);
        }

        void ManagerCallbackTaskFinished(object sender, TaskFinishedEventArgs e)
        {
            UserStateUpdateDelegate userStateUpdateDelegate = RefreshView;
            this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, userStateUpdateDelegate);
        }

        void ManagerCallbackTaskReviewCancel(object sender, TaskCancelReviewEventArgs e)
        {
            UserStateUpdateDelegate userStateUpdateDelegate = RefreshView;
            this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, userStateUpdateDelegate);
        }

        void ManagerCallbackWorkerChangedStateEvent(object sender, WorkerBusyEventArgs e)
        {
            UserStateUpdateDelegate userStateUpdateDelegate = RefreshView;
            this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, userStateUpdateDelegate);
        }

        void UsersCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add || e.Action == NotifyCollectionChangedAction.Remove)
            {
                AutoResizeColumns();
            }
            UserStateUpdateDelegate userStateUpdateDelegate = RefreshView;
            this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, userStateUpdateDelegate);
        }

        void RefreshView()
        {
            CollectionViewSource userCollectionViewSource = (CollectionViewSource)Resources["UsersCollectionViewSource"];
            if (userCollectionViewSource == null) return;
            try
            {
                userCollectionViewSource.View.Refresh();
            }
            catch (InvalidOperationException e)
            {
            }
            AutoResizeColumns();
            OnPropertyChanged("Load");
        }

        private void UsersCollectionViewSource_OnFilter(object sender, FilterEventArgs e)
        {
            UserView userView = (UserView)e.Item;
            e.Accepted = true;
            if (userView == null)
            {
                e.Accepted = false;
            }
            else
            {
                if (userView.Role == ConstantsRoles.Worker)
                {
                    if (userView.CurrentState == ConstantsNetworkStates.Offline ||
                        userView.CurrentState == ConstantsNetworkStates.Nothing)
                    {
                        e.Accepted = false;
                    }

                    if (userView.Status == ConstantsStatuses.Deleted)
                    {
                        e.Accepted = false;
                    }
                }
                else
                {
                    e.Accepted = false;
                }
            }
            OnPropertyChanged("Load");
        }

        private void GridViewColumnHeaderClickedHandler(object sender, RoutedEventArgs e)
        {
            var headerClicked = e.OriginalSource as GridViewColumnHeader;
            // maybe datacontext
            if (sender != null)
                _currentSortMachine.GridViewColumnHeaderClickedHandler(((ListView)sender).ItemsSource, headerClicked, UsersListView);
            AutoResizeColumns();
        }

        private void ListViewItemMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListViewItem viewItem = sender as ListViewItem;
            if (viewItem != null)
            {
                ListViewItem listViewItem = (viewItem);
                UserView userView = (UserView) listViewItem.DataContext;
                if (userView == null) return;
                TaskView taskView =
                    Repository.Tasks.FirstOrDefault(
                        t => t.Status == ConstantsStatuses.InProcess && t.CurrentWorker.Id == userView.Id);
                if (taskView == null) return;
                ViewTaskWindow viewTaskControl = new ViewTaskWindow {DataContext = taskView, Owner = this};
                viewTaskControl.Show();
            }
        }

        public void AutoResizeColumns()
        {
            GridView gv = UsersListView.View as GridView;

            if (gv != null)
            {
                foreach (GridViewColumn gvc in gv.Columns)
                {
                    // Set width to highest possible value
                    gvc.Width = double.MaxValue;

                    // Set to NaN to get the "real" actual width
                    gvc.Width = double.NaN;
                }
            }
        }

        #region INotifyPropertyChanged Members

        public virtual event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string info)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(info));
            }
        }
        #endregion

        private void PickButtonChecked(object sender, RoutedEventArgs e)
        {
            Pin();
            PickButton.ToolTip = "Убрать поверх всех окон";
        }

        private void PickButtonUnchecked(object sender, RoutedEventArgs e)
        {
            Unpin();
            PickButton.ToolTip = "Закрепить поверх всех окон";
        }

        private void Pin()
        {
            this.Topmost = true;
        }

        private void Unpin()
        {
            this.Topmost = false;
        }

        protected override void OnClosed(EventArgs e)
        {
            ServiceManager.Inst.Callback.WorkerChangedStateEvent -= ManagerCallbackWorkerChangedStateEvent;
            ServiceManager.Inst.Callback.TaskReviewCancel -= ManagerCallbackTaskReviewCancel;
            ServiceManager.Inst.Callback.TaskUpdatedEvent -= ManagerCallbackTaskUpdated;
            ServiceManager.Inst.Callback.TaskFinishedEvent -= ManagerCallbackTaskFinished;
            base.OnClosed(e);
        }

        private void CustomersCollectionViewSource_OnFilter(object sender, FilterEventArgs e)
        {
            CustomerView customerView = (CustomerView)e.Item;
            e.Accepted = true;
            if (customerView.Status == ConstantsStatuses.Deleted)
            {
                e.Accepted = false;
            }
        }
    }
}
