﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using Tasker.Manager.Base;

namespace Tasker.Manager.Controls
{
    /// <summary>
    /// Interaction logic for ManagerStatusBar.xaml
    /// </summary>
    public partial class ManagerStatusBar : UserControl
    {
        private delegate void IniStartEventHadler(string message);
        private delegate void IniEndEventHandler(string message);

        public ManagerStatusBar()
        {
            InitializeComponent();

            LoadManager.LoadStart += LoadManager_LoadStart;
            LoadManager.LoadEnd += LoadManager_LoadEnd;
        }

        public void SetSelectionText(string text)
        {
            SelectionStatusTextBlock.Text = text;
        }

        private void LoadManager_LoadEnd(object sender, LoadStartEventArgs loadStartEventArgs)
        {
            EndLoad(loadStartEventArgs.LoadMessage);
        }

        private void LoadManager_LoadStart(object sender, LoadStartEventArgs e)
        {
            StartLoad(e.LoadMessage);
        }

        private void StartLoad(string message)
        {
            IniStartEventHadler iniStartEventHadler = SafelyStartLoad;
            this.Dispatcher.Invoke(DispatcherPriority.Normal, iniStartEventHadler, message);
        }

        private void SafelyStartLoad(string message)
        {
            CircularProgressBarItem.Visibility = Visibility.Visible;
            TspText.TextLabels.Add(message);
            LoadStatusTextBlock.Text = "";
        }

        private void EndLoad(string message)
        {
            IniEndEventHandler iniEndEventHandler = SafelyEndLoad;
            this.Dispatcher.Invoke(DispatcherPriority.Normal, iniEndEventHandler, message);
        }

        private void SafelyEndLoad(string message)
        {
            TspText.TextLabels.Remove(message);

            if (!LoadManager.IsBusy)
            {
                CircularProgressBarItem.Visibility = Visibility.Collapsed;
                LoadStatusTextBlock.Text = "Готово";
            }
        }

        public void Unsign()
        {
            LoadManager.LoadStart -= LoadManager_LoadStart;
            LoadManager.LoadEnd -= LoadManager_LoadEnd;
        }
    }
}
