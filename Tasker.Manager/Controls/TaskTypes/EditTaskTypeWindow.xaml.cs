﻿using System;
using System.Windows;
using System.Windows.Controls;
using Tasker.Manager.Managers;
using Tasker.Manager.Managers.Logger;
using Tasker.Tools.Extensions;
using Tasker.Tools.Managers;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Manager.Controls.TaskTypes
{
    public partial class EditTaskTypeWindow : WindowExt
    {
        public TaskTypeView CurrentTaskTypeView
        {
            get { return (TaskTypeView) DataContext; }
            set { DataContext = value;}
        }

        public EditTaskTypeWindow()
        {
            if (DataContext == null)
                DataContext = CreateDefaultTaskType();

            InitializeComponent();

            titleTextBox.Focus();
            this.Activate();
        }

        private static TaskTypeView CreateDefaultTaskType()
        {
            TaskTypeView taskTypeView = new TaskTypeView
                                            {
                                                Title = "Новый тип задачи",
                                                CompletionTime = new TimeSpan(0, 2, 0, 0),
                                                Description = "Описание",
                                                HighLightBrush = System.Drawing.Color.White
                                            };
            return taskTypeView;
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveTaskType();
        }

        protected override void SaveData()
        {
            SaveTaskType();
            base.SaveData();
        }

        private void SaveTaskType()
        {
            if (CurrentTaskTypeView == null) return;

            TaskTypeDTO taskTypeDto = CurrentTaskTypeView.BuildDTO();
            if (CurrentTaskTypeView.Id == 0)
            {
                int newId = ServiceManager.Inst.ServiceChannel.AddTaskType(taskTypeDto);
                if (newId > 0)
                {
                    CurrentTaskTypeView.Id = newId;
                    Repository.TaskTypes.Add(CurrentTaskTypeView);
                    Log.Write(LogLevel.Debug, "Добавлен новый тип задачи " + CurrentTaskTypeView.Title);
                }
                else
                    Log.Write(LogLevel.Error, "Ошибка при добавлении типа задачи.");
            }
            else
            {
                ServiceManager.Inst.ServiceChannel.UpdateTaskType(taskTypeDto);
                Log.Write(LogLevel.Debug, "Обновлен тип задачи " + CurrentTaskTypeView.Title);
            }

            this.Close();
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            if (CurrentTaskTypeView.Id != 0)
            {
                TaskTypeDTO taskTypeDto = ServiceManager.Inst.ServiceChannel.GetTaskType(CurrentTaskTypeView.Id);
                TaskTypeManager.UpdateTaskType(Repository.TaskTypes, taskTypeDto);
            }

            this.Close();
        }

        private void textBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (sender is TextBox)
            {
                ((TextBox)sender).SelectAll();
            }
        }
    }
}
