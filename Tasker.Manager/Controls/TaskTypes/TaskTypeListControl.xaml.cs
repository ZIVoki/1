﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using Tasker.Manager.Controls.Task;
using Tasker.Manager.Managers;
using Tasker.Manager.Managers.Logger;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Manager.Controls.TaskTypes
{
    /// <summary>
    /// Interaction logic for TaskTypeListControl.xaml
    /// </summary>
    public partial class TaskTypeListControl : UserControl
    {
        private readonly SortMachine _currentSortMachine = new SortMachine();
        public event SelectionChangedDelegate SelectionChangedHandler;

        public TaskTypeListControl()
        {
            InitializeComponent();
            Repository.TaskTypes.CollectionChanged += TaskTypes_CollectionChanged;
        }

        void TaskTypes_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            AutoResizeColumns();
        }

        public void DeleteSelectedItems()
        {
            if (TaskTypesListView.SelectedItem == null) return;

            TaskTypeView taskTypeView = (TaskTypeView)TaskTypesListView.SelectedItem;

            string message = string.Format("Удалить тип задачи {0}", taskTypeView.Title);
            MessageBoxResult messageBoxResult = MessageBox.Show(message,
                                                                "Удаление типа задачи",
                                                                MessageBoxButton.YesNo,
                                                                MessageBoxImage.Question);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                taskTypeView.Status = ConstantsStatuses.Deleted;
                TaskTypeDTO taskTypeDto = taskTypeView.BuildDTO();

                ServiceManager.Inst.ServiceChannel.UpdateTaskType(taskTypeDto);
                Log.Write(LogLevel.Debug, string.Format("Тип задачи {0} удален.", taskTypeView.Title));

                TaskTypeView firstOrDefault = Repository.TaskTypes.FirstOrDefault(c => c.Id == taskTypeView.Id);
                if (firstOrDefault == null) return;
                firstOrDefault.Status = ConstantsStatuses.Deleted;

                CollectionViewSource collectionViewSource = (CollectionViewSource)Resources["TaskTypesCollectionViewSource"];
                collectionViewSource.View.Refresh();
            }
        }


        private void TaskTypesCollectionViewSource_OnFilter(object sender, FilterEventArgs e)
        {
            TaskTypeView typeView = (TaskTypeView)e.Item;
            e.Accepted = true;
            if (typeView.Status == ConstantsStatuses.Deleted)
            {
                e.Accepted = false;
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            var headerClicked =
                  e.OriginalSource as GridViewColumnHeader;
            if (sender != null)
                _currentSortMachine.GridViewColumnHeaderClickedHandler(((ListView)sender).ItemsSource, headerClicked, this);
            AutoResizeColumns();
        }

        private void ListViewItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ShowEditTaskTypeWidow(sender);
        }

        private static void ShowEditTaskTypeWidow(object sender)
        {
            if (sender is ListViewItem)
            {
                ListViewItem listViewItem = ((ListViewItem) sender);

                EditTaskTypeWindow editTaskTypeWindow = new EditTaskTypeWindow {DataContext = listViewItem.DataContext};
                editTaskTypeWindow.ShowDialog();
            }
        }


        public void AutoResizeColumns()
        {
            GridView gv = TaskTypesListView.View as GridView;

            if (gv != null)
            {
                foreach (GridViewColumn gvc in gv.Columns)
                {
                    // Set width to highest possible value
                    gvc.Width = double.MaxValue;

                    // Set to NaN to get the "real" actual width
                    gvc.Width = double.NaN;
                }
            }
        }

        private void GridViewColumnHeaderClickedHandler(object sender, RoutedEventArgs e)
        {
            var headerClicked = e.OriginalSource as GridViewColumnHeader;
            // maybe datacontext
            if (sender != null)
                _currentSortMachine.GridViewColumnHeaderClickedHandler(((ListView)sender).ItemsSource, headerClicked, this);
            AutoResizeColumns();
        }

        private void TaskTypeListView_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int count = TaskTypesListView.SelectedItems.Count;
            OnSelectionChangedHandler(new SelectionChangedArgs(count, TaskTypesListView.Items.Count));
        }

        protected virtual void OnSelectionChangedHandler(SelectionChangedArgs selectionChangedArgs)
        {
            SelectionChangedDelegate handler = SelectionChangedHandler;
            if (handler != null) handler(this, selectionChangedArgs);
        }
    }
}
