﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Tasker.Manager.Managers;
using Tasker.Manager.Managers.Logger;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Manager.Controls.Response
{
    internal delegate void RequestSendedEventHandler(object sender);

    /// <summary>
    /// Interaction logic for LunchResponseWindow.xaml
    /// </summary>
    public partial class LunchResponseWindow : Window
    {
        private bool _hasResult = false;
        private bool _forceClose;
        private readonly List<LunchResponseWindow> _lunchResponseWindows;

        public UserView CurrentUserView
        {
            get { return (UserView) DataContext; }
            set
            {
                if (value == null) return;
                DataContext = value;
            }
        }
        
        internal event RequestSendedEventHandler RequestSended;

        private int UserId { get; set; }

        internal void InvokeRequestSended(bool lunchChangeState)
        {
            UserId = ((UserView)DataContext).Id;
            if (lunchChangeState)
            {
                var userView = (UserView) DataContext;
                
                if(userView != null)
                {
                    var workerTask =
                        Repository.Tasks.Where(task => task.CurrentWorker != null).FirstOrDefault(
                            task => task.CurrentWorker.Id == userView.Id && task.Status == ConstantsStatuses.InProcess);

                    if (workerTask != null)
                    {
                        workerTask.Status = ConstantsStatuses.Disabled;
                    }
                    userView.CurrentState = ConstantsNetworkStates.Lunch;
                }
            }

            ServiceManager.Inst.ServiceChannel.LunchRequestResult(lunchChangeState, UserId);
            WriteLogInfo(lunchChangeState);
            RequestSendedEventHandler handler = RequestSended;
            if (handler != null) handler(this);
            _hasResult = true;
        }

        private void WriteLogInfo(bool lunchChangeState)
        {
            string message;
            string userTitle;

            if (CurrentUserView != null) userTitle = CurrentUserView.Title;
            else userTitle = "None";

            if (lunchChangeState)
                message = string.Format("Пользователь {0} отправлен на обед ({1})",
                                        userTitle, 
                                        Repository.LoginUser.Title);
            else
                message = string.Format("Пользователю {0} отказано в обеде ({1})",
                                            userTitle, 
                                            Repository.LoginUser.Title);

            Log.Write(LogLevel.Info, message);
        }

        public LunchResponseWindow(List<LunchResponseWindow> lunchResponseWindows)
        {
            _lunchResponseWindows = lunchResponseWindows;
            InitializeComponent();
            DataContextChanged += LunchResponseWindow_DataContextChanged;
        }

        void LunchResponseWindow_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Title = string.Format("Запрос на обед {0}", CurrentUserView.Title);
        }
        
        private void allowLunchButton_Click(object sender, RoutedEventArgs e)
        {
            InvokeRequestSended(true);
            this.Close();
        }

        private void denyLunchButton_Click(object sender, RoutedEventArgs e)
        {
            InvokeRequestSended(false);
            this.Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!_hasResult && !_forceClose)
            { 
                InvokeRequestSended(false);
            }
            _lunchResponseWindows.Remove(this);
        }

        public void ForceClose()
        {
            _forceClose = true;
            this.Close();
        }
    }
}
