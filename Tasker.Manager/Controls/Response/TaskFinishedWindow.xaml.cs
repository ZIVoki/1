﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Tasker.Manager.Controls.Task;
using Tasker.Manager.Managers;
using Tasker.Manager.Managers.Logger;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Manager.Controls.Response
{
    /// <summary>
    /// Interaction logic for TaskFinishedWindow.xaml
    /// </summary>
    public partial class TaskFinishedWindow : Window
    {
        private bool _hasResult = false;
        private bool _forceClose;
        private readonly List<TaskFinishedWindow> _taskFinishedWindows;

        internal event EventHandler ResponseSended;

        public TaskFinishedWindow(List<TaskFinishedWindow> taskFinishedWindows)
        {
            _taskFinishedWindows = taskFinishedWindows;
            InitializeComponent();
            DataContextChanged += TaskFinishedWindowDataContextChanged;
        }

        void TaskFinishedWindowDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Title = string.Format("Задача {0} завершена", CurrentTask.Title);
        }

        public TaskView CurrentTask
        {
            get
            {
                if (DataContext == null) return null;
                return (TaskView) DataContext;
            }
            set
            {
                if (value == null) return;
                DataContext = value;
            }
        }

        internal void InvokeRequestSended(bool result)
        {
            if (CurrentTask == null) return;

            ServiceManager.Inst.ServiceChannel.SetTaskReviewResult(CurrentTask.Id, result, RejectCommentTextBox.Text);

            if (result)
            {
                CurrentTask.Status = ConstantsStatuses.Closed;
                CurrentTask.Comment = RejectCommentTextBox.Text;
                if (CurrentTask.CurrentWorker != null)
                {
                    CurrentTask.CurrentWorker.UserWorkState = Tools.Constants.UserWorkState.Free;
                    SetPlaningTask(CurrentTask.CurrentWorker.Id);
                }
            }
            else
            {
                CurrentTask.Status = ConstantsStatuses.InProcess;
            }

            WriteLog(result);
            EventHandler handler = ResponseSended;
            if (handler != null) handler(null,null);
            _hasResult = true;
        }

        private static void SetPlaningTask(int workerId)
        {
            /*TaskView taskView = Repository.Tasks.Where(
                t => t.CurrentWorker != null &&
                t.CurrentWorker.Id == workerId &&
                (t.Status ==ConstantsStatuses.Enabled || t.Status == ConstantsStatuses.InProcess ||
                 t.Status == ConstantsStatuses.Disabled || t.Status == ConstantsStatuses.Planing)).
                OrderByDescending(t => t.Priority).ThenByDescending(t => t.Id).FirstOrDefault();

            if (taskView != null) taskView.Status = ConstantsStatuses.Planing;*/
        }

        private void WriteLog(bool result)
        {
            string message;
            string taskTitle;
            TaskView taskView = ((TaskView)DataContext);

            if (taskView != null)
                taskTitle = taskView.Title;
            else taskTitle = "None";

            if (result)
                message = string.Format("Задача {0} закрыта.", taskTitle);
            else
                message = string.Format("Задача {0} переоткрыта.", taskTitle);

            Log.Write(LogLevel.Info, message);
        }

        private void RejectTaskButtonClick(object sender, RoutedEventArgs e)
        {
            InvokeRequestSended(false);
            this.Close();
        }

        private void AcceptTaskButtonClick(object sender, RoutedEventArgs e)
        {
            InvokeRequestSended(true);
            this.Close();
        }

        private void BtnViewResultClick(object sender, RoutedEventArgs e)
        {
            ViewTaskWindow viewTaskWindow = new ViewTaskWindow() { DataContext = DataContext };
            viewTaskWindow.Show();
        }

        private void WindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!_hasResult && !_forceClose)
            {
                InvokeRequestSended(false);
            }
            _taskFinishedWindows.Remove(this);
        }

        public void ForceClose()
        {
            _forceClose = true;
            this.Close();
        }

    }
}
