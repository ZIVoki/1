﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Tasker.Tools.ViewModel;
using Tasker.Tools.ViewModel.Base;

namespace Tasker.Manager.Controls.TimeTable
{
    /// <summary>
    /// Interaction logic for TimeTableMailDeliveryWindow.xaml
    /// </summary>
    public partial class TimeTableMailDeliveryWindow : Window
    {
        public TimeTableMailDeliveryWindow()
        {
            InitializeComponent();
        }
    }

    public class MailDeliveryDataView: ObservableObject
    {
        ObservableCollection<UserView> _selectedUsers = new ObservableCollection<UserView>();

        public ObservableCollection<UserView> SelectedUsers
        {
            get { return _selectedUsers; }
            set
            {
                _selectedUsers = value;
                RaisePropertyChanged("SelectedUsers");
            }
        }

        public MailDeliveryDataView()
        {
            LoadLastSelectedUsers();
        }

        private void LoadLastSelectedUsers()
        {
            
        }
    }

}
