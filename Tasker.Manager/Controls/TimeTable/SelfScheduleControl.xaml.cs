﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Tasker.Manager.Managers;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Manager.Controls.TimeTable
{
    /// <summary>
    /// Interaction logic for SelfScheduleControl.xaml
    /// </summary>
    public partial class SelfScheduleControl : UserControl
    {
        public bool IsDesignTime
        {
            get
            {
                return (Application.Current == null) ||
                       (Application.Current.GetType() == typeof(Application));
            }
        }

        public SelfScheduleControl()
        {
            InitializeComponent();
            if (IsDesignTime) return;

            Repository.TimeTables.CollectionChanged += TimeTable_CollectionChanged;
            ServiceManager.Inst.Callback.TimeTableChangedEvent += ManagerCallback_TimeTableChangedEvent;
        }

        void ManagerCallback_TimeTableChangedEvent(object sender, TimeTableEventArgs e)
        {
            RefreshView();
        }

        void TimeTable_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            RefreshView();
        }

        void RefreshView()
        {
            var taskCollectionViewSource = (CollectionViewSource)Resources["scheduleCollectionViewSource"];
            if (taskCollectionViewSource == null) return;
            try
            {
                taskCollectionViewSource.View.Refresh();
            }
            catch (InvalidOperationException e)
            {
            }
            AutoResizeColumns();
        }

        private void CollectionViewSourceFilter(object sender, FilterEventArgs e)
        {
            TimeTableView timeTableView = (TimeTableView)e.Item;
            if (timeTableView == null) return;

            if (timeTableView.Status != ConstantsStatuses.Deleted && 
                timeTableView.User.Id == Repository.LoginUser.Id &&
                timeTableView.EndDate >= DateTime.Now) e.Accepted = true;
            else e.Accepted = false;
        }

        public void AutoResizeColumns()
        {
            try
            {
                GridView gv = userShceduleListView.View as GridView;

                if (gv != null)
                {
                    foreach (GridViewColumn gvc in gv.Columns)
                    {
                        // Set width to highest possible value
                        gvc.Width = double.MaxValue;

                        // Set to NaN to get the "real" actual width
                        gvc.Width = double.NaN;
                    }
                }
            }
            catch (InvalidOperationException e)
            {}
        }

        public void Unsign()
        {
            Repository.TimeTables.CollectionChanged -= TimeTable_CollectionChanged;
            ServiceManager.Inst.Callback.TimeTableChangedEvent -= ManagerCallback_TimeTableChangedEvent;
        }
    }
}
