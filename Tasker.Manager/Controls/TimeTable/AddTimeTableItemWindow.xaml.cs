﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Tasker.Manager.Managers;
using Tasker.Tools.Extensions;
using Tasker.Tools.Managers;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;
using Tasker.Manager.Managers.Logger;

namespace Tasker.Manager.Controls.TimeTable
{
    public enum DayType
    {
        WorkDay,
        Saturday,
        Sunday
    }

    /// <summary>
    /// Interaction logic for AddTimeTableItemWindow.xaml
    /// </summary>
    public partial class AddTimeTableItemWindow : WindowExt
    {
        private DayType _currentDayType = DayType.WorkDay;
        private bool _handSelection = false;

        public TimeTableView CurrentTimeTable
        {
            get { return (TimeTableView)DataContext; }
            set { DataContext = value; }
        }

        public DateTime? SelectedDate
        {
            get { return TimeTableStartDate.Value; }
            set
            {
                TimeTableStartDate.Value = value;
                UpdateShiftList(value);
                if (value.HasValue)
                {
                    ShiftAutoSelect(value.Value);
                }
            }
        }

        private void ShiftAutoSelect(DateTime dateTime)
        {
            TimeSpan timeSpan = new TimeSpan(dateTime.Hour, dateTime.Minute, 0);
            IEnumerable<ShiftView> shiftViews =
                Repository.Shifts.Where(s => s.StartTime <= timeSpan).OrderByDescending(f => f.StartTime).ToList();
            if (!shiftViews.Any())
            {
                shiftViews = Repository.Shifts.Where(s => s.Title == "1-я смена").OrderByDescending(f => f.StartTime).ToList();
            }
            if (dateTime.DayOfWeek == DayOfWeek.Sunday)
                {
                    ShiftView first = shiftViews.FirstOrDefault(t => t.Title != "1-я смена" &&
                                                                     t.Title != "2-я смена");
                    CurrentTimeTable.Shift = first;
                    if (ShiftComboBox.SelectedValue == null)
                        ShiftComboBox.SelectedIndex = 0;
                }
                else
                    CurrentTimeTable.Shift =
                        shiftViews.FirstOrDefault(
                            t =>
                            t.Title != "1-я смена (ВС)" &&
                            t.Title != "2-я смена (ВС)" &&
                            t.Title != "Субботняя смена");
                UpdateTimeControls();
        }

        public AddTimeTableItemWindow()
        {
            DataContextChanged += AddTimeTableItemWindowDataContextChanged;
            if (CurrentTimeTable == null)
                CurrentTimeTable = CreateNewTimeTableView();

            InitializeComponent();
            UpdateShiftList(TimeTableStartDate.Value);
            UpdateTimeControls();
            this.Activate();
            TimeTableStartTime.TimeInterval = TimeSpan.FromMinutes(30);
            TimeTableEndTime.TimeInterval = TimeSpan.FromMinutes(30);
        }

        void AddTimeTableItemWindowDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (this.IsInitialized)
            {
                if (CurrentTimeTable == null) return;
                UpdateShiftList(CurrentTimeTable.StartDate.Date);
            }
        }

        private static TimeTableView CreateNewTimeTableView()
        {
            TimeTableView timeTableView = new TimeTableView
            {
                StartDate = DateTime.Now,
                Shift = Repository.Shifts[0],
                User = Repository.Users[0],
                Status = ConstantsStatuses.Enabled
            };
            return timeTableView;
        }

        private void addNewTimeTableButton_Click(object sender, RoutedEventArgs e)
        {
            SaveTimeTable();
        }

        protected override void SaveData()
        {
            SaveTimeTable();
            base.SaveData();
        }

        private void SaveTimeTable()
        {
            if (CurrentTimeTable == null) return;

            if (TimeTableStartDate.Value != null)
            {
                if (CurrentTimeTable.EndDate <= CurrentTimeTable.StartDate)
                    CurrentTimeTable.EndDate = CurrentTimeTable.EndDate.AddDays(1);
            }
            if (CurrentTimeTable.Shift == null)
                CurrentTimeTable.Shift = Repository.Shifts.FirstOrDefault();

            TimeTableDTO timeTableDto = CurrentTimeTable.BuildDTO();

            if (CurrentTimeTable.Id == 0)
            {
                CurrentTimeTable.Id = ServiceManager.Inst.ServiceChannel.AddTimeTable(timeTableDto);
                if (CurrentTimeTable.Id > 0)
                {
                    Repository.TimeTables.Add(CurrentTimeTable);
                    string message = String.Format("Добавлени новая ячейка в расписании пользователя {0} на {1}.",
                                                   timeTableDto.User.LastName, timeTableDto.StartDate.ToString("dd.MM.yyyy"));
                    Log.Write(LogLevel.Debug, message);
                }
                else
                    Log.Write(LogLevel.Error, "Ошибка при добавлении новой ячейки в рассписании.");
            }
            else
            {
                ServiceManager.Inst.ServiceChannel.UpdateTimeTable(timeTableDto);
                Thread.Sleep(TimeSpan.FromMilliseconds(100));
                ServiceManager.Inst.Callback.InvokeTimeTableChangedEvent(new TimeTableEventArgs(CurrentTimeTable.BuildDTO()));
                string message = String.Format("Обновлена ячейка в расписании пользователя {0} на {1}.",
                                               timeTableDto.User.LastName, timeTableDto.StartDate.ToString("dd.MM.yyyy"));
                Log.Write(LogLevel.Debug, message);
            }

            this.Close();
        }

        private void shiftComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_handSelection)
            {
                UpdateTimeControls();
            }
            _handSelection = false;
            if (CurrentTimeTable.Id == 0)
            {
                UpdateTimeControls();
            }
        }

        private void UpdateTimeControls()
        {
            Debug.WriteLine("0");
            if (!this.IsInitialized) return;
            Debug.WriteLine("1");
            if (CurrentTimeTable.Shift == null) return;
            Debug.WriteLine("2");
            if (!TimeTableStartDate.Value.HasValue) return;
            Debug.WriteLine(TimeTableStartDate.Value);

            CurrentTimeTable.StartDate = TimeTableStartDate.Value.Value.Date.Add(CurrentTimeTable.Shift.StartTime);
            CurrentTimeTable.EndDate = TimeTableStartDate.Value.Value.Date.Add(CurrentTimeTable.Shift.EndTime);
            Debug.WriteLine(string.Format("CurrentTimeTable.StartDate {0}", CurrentTimeTable.StartDate));
            Debug.WriteLine(string.Format("CurrentTimeTable.EndDate {0}", CurrentTimeTable.EndDate));
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        protected override void OnClosed(EventArgs e)
        {
            if (CurrentTimeTable.Id != 0)
            {
                TimeTableDTO timeTableDto = ServiceManager.Inst.ServiceChannel.GetTimeTable(CurrentTimeTable.Id);
                TimeTableManager.UpdateTimeTableView(timeTableDto, Repository.TimeTables, Repository.Users, Repository.Shifts);
            }

            base.OnClosed(e);
        }

        private void shiftsCollectionViewSource_Filter(object sender, FilterEventArgs e)
        {
            e.Accepted = false;
            ShiftView shiftView = (ShiftView)e.Item;
            if (shiftView == null) return;

            switch (_currentDayType)
            {
                case DayType.WorkDay:
                    if (shiftView.Title != "1-я смена (ВС)" &&
                        shiftView.Title != "2-я смена (ВС)")
                        e.Accepted = true;
                    break;
                case DayType.Saturday:
                    if (shiftView.Title != "1-я смена (ВС)" &&
                        shiftView.Title != "2-я смена (ВС)")
                        e.Accepted = true;
                    break;
                case DayType.Sunday:
                    if (shiftView.Title != "1-я смена" &&
                        shiftView.Title != "2-я смена")
                        e.Accepted = true;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void timeTableStartDate_LostFocus(object sender, RoutedEventArgs e)
        {            
            UpdateShiftList(TimeTableStartDate.Value);
        }

        private void UpdateShiftList(DateTime? dateTime)
        {
            if (dateTime.HasValue)
            {
                switch (dateTime.Value.DayOfWeek)
                {
                    case DayOfWeek.Sunday:
                        _currentDayType = DayType.Sunday;
                        break;
                    case DayOfWeek.Monday:
                        _currentDayType = DayType.WorkDay;
                        break;
                    case DayOfWeek.Tuesday:
                        _currentDayType = DayType.WorkDay;
                        break;
                    case DayOfWeek.Wednesday:
                        _currentDayType = DayType.WorkDay;
                        break;
                    case DayOfWeek.Thursday:
                        _currentDayType = DayType.WorkDay;
                        break;
                    case DayOfWeek.Friday:
                        _currentDayType = DayType.WorkDay;
                        break;
                    case DayOfWeek.Saturday:
                        _currentDayType = DayType.Saturday;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                RefreshView();
            }
        }

        void RefreshView()
        {
            var taskCollectionViewSource = (CollectionViewSource)Resources["shiftsCollectionViewSource"];
            if (taskCollectionViewSource == null) return;
            try
            {
                taskCollectionViewSource.View.Refresh();
            }
            catch (InvalidOperationException e)
            {
            }
        }

        private void timeTableStartDate_LostMouseCapture(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (TimeTableStartDate.Value.HasValue) 
                ShiftAutoSelect(TimeTableStartDate.Value.Value);
            UpdateTimeControls();
            TimeTableStartDate.IsOpen = false;
            UpdateShiftList(TimeTableStartDate.Value);
        }

        private void timeTableStartDate_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            UpdateShiftList(TimeTableStartDate.Value);
        }

        private void shiftComboBox_GotFocus(object sender, RoutedEventArgs e)
        {
            _handSelection = true;
        }

        private void userComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (CurrentTimeTable.User.Role == ConstantsRoles.Worker)
                CurrentTimeTable.IsResp = false;
        }

        private void CollectionViewSource_Filter(object sender, FilterEventArgs e)
        {
            if (e.Item != null && e.Item is UserView)
            {
                var userView = ((UserView) e.Item);
                if (userView.Login == "admin" || userView.Login == "worker" || userView.Status == ConstantsStatuses.Deleted)
                {
                    e.Accepted = false;
                    return;
                }
            }

            e.Accepted = true;
        }

        private void IsRespCheckBox_OnChecked(object sender, RoutedEventArgs e)
        {
            ShiftComboBox.IsEnabled = false;
            CurrentTimeTable.Shift = Repository.Shifts.FirstOrDefault(s => s.Title == "2-я смена");
            CurrentTimeTable.StartDate = CurrentTimeTable.StartDate.Date + TimeSpan.FromHours(12);
            CurrentTimeTable.EndDate = CurrentTimeTable.StartDate + TimeSpan.FromHours(11);
        }

        private void IsRespCheckBox_OnUnchecked(object sender, RoutedEventArgs e)
        {
            ShiftComboBox.IsEnabled = true;
            if (Repository.Shifts.Any())
            {
                CurrentTimeTable.Shift = Repository.Shifts.FirstOrDefault();
                UpdateTimeControls();
            }
        }
    }
}
