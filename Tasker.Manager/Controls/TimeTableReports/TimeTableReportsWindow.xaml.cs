﻿using System.Windows;
using System.Windows.Data;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Manager.Controls.TimeTableReports
{
    /// <summary>
    /// Interaction logic for TimeTableReportsWindow.xaml
    /// </summary>
    public partial class TimeTableReportsWindow : Window
    {
        public TimeTableReportsDataView Data
        {
            get { return (TimeTableReportsDataView) DataContext; }
        }

        public TimeTableReportsWindow()
        {
            InitializeComponent();
            
        }

        private void CloseButtonClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void UsersCollectionViewSource_OnFilter(object sender, FilterEventArgs e)
        {
            UserView userView = (UserView)e.Item;
            e.Accepted = true;
            if (userView.Status == ConstantsStatuses.Deleted || userView.FirstName == "admin")
            {
                e.Accepted = false;
            }
        }
    }
}
