using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using Tasker.Manager.Managers;
using Tasker.Tools.ViewModel;
using Tasker.Tools.ViewModel.Base;
using Tasker.Tools.Extensions;

namespace Tasker.Manager.Controls.TimeTableReports
{
    public class TimeTableReportsDataView: ObservableObject
    {
        private BackgroundWorker _backgroundWorker;

        private ObservableCollection<UserView> _mailOffers = new ObservableCollection<UserView>();
        private DateTime _startDate;
        private DateTime _endDate;
        private RelayCommand _send;

        private bool _isSendeing;

        public String Title
        {
            get
            {
                return string.Format("�������� ���������� � {0} �� {1}", _startDate.ToString("d.MM"), _endDate.ToString("d.MM"));
            }
        }

        public ObservableCollection<UserView> MailOffers
        {
            get { return _mailOffers; }
            set
            {
                _mailOffers = value;
                RaisePropertyChanged(() => MailOffers);
            }
        }

        public DateTime EndDate
        {
            get { return _endDate; }
            set
            {
                _endDate = value;
                RaisePropertyChanged(() => EndDate);
            }
        }

        public DateTime StartDate
        {
            get { return _startDate; }
            set
            {
                _startDate = value.Date;
                RaisePropertyChanged(() => StartDate);                
                EndDate = _startDate.AddDays(6);
                RaisePropertyChanged(() => Title);
            }
        }

        public bool IsSendeing
        {
            get { return _isSendeing; }
            set
            {
                _isSendeing = value;
                RaisePropertyChanged(() => IsSendeing);
                RaisePropertyChanged(() => SendButtonEnabled);
            }
        }

        public bool SendButtonEnabled
        {
            get { return !IsSendeing; }
        }

        public ICommand SendEmailsCommand
        {
            get
            {
                if (_send == null)
                {
                    _send = new RelayCommand(SendEmails);
                }

                return _send;
            }
        }

        private void SendEmails()
        {
            IsSendeing = true;
            if (_backgroundWorker == null)
            {
                _backgroundWorker = new BackgroundWorker {WorkerSupportsCancellation = true};
                _backgroundWorker.DoWork += _backgroundWorker_DoWork;
                _backgroundWorker.RunWorkerCompleted += _backgroundWorker_RunWorkerCompleted;
            }   

            _backgroundWorker.RunWorkerAsync();
        }

        void _backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            IsSendeing = false;
        }

        void _backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            TimeTabelReportsManager.SendReports(_mailOffers.ToList(), StartDate, EndDate);
        }

        public TimeTableReportsDataView()
        {
            if (Repository.UserConfiguration != null)
            {
                foreach (var id in Repository.UserConfiguration.MailOffersId)
                {
                    UserView userView = Repository.Users.FirstOrDefault(u => u.Id == id);
                    if (userView != null)
                        MailOffers.Add(userView);
                }

                _mailOffers.CollectionChanged += _mailOffers_CollectionChanged;
            }

            _startDate = DateTime.Now.FirstDayOfWeek();
        }

        void _mailOffers_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (!(sender is ObservableCollection<UserView>)) return;
            ObservableCollection<UserView> userViews = sender as ObservableCollection<UserView>;
            IEnumerable<int> sendedUserViews = userViews.Select(u => u.Id).ToArray();
            if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                IEnumerable<int> except = Repository.UserConfiguration.MailOffersId.Except(sendedUserViews).ToList();
                foreach (int i in except)
                {
                    Repository.UserConfiguration.MailOffersId.Remove(i);
                }
            }
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (var i in sendedUserViews)
                {
                    if (!Repository.UserConfiguration.MailOffersId.Contains(i))
                        Repository.UserConfiguration.MailOffersId.Add(i);
                }
            }
        }

    }
}