﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Tasker.Manager.Properties;

namespace Tasker.Manager.Controls
{
    /// <summary>
    /// Interaction logic for RssViewControl.xaml
    /// </summary>
    public partial class RssViewControl : UserControl
    {
        public RssViewControl()
        {
            InitializeComponent();
        }

        private void CollectionViewSourceFilter(object sender, FilterEventArgs e)
        {
            
        }

        private void CreateNewButton_OnClick(object sender, RoutedEventArgs e)
        {
            string newsLink = Settings.Default.CreateNewsLink;
            if (!String.IsNullOrEmpty(newsLink))
                Process.Start(newsLink);
        }
    }
}
