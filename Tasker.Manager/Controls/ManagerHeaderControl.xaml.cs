﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Tasker.Manager.Controls
{

    public delegate void LogOffHandler(object sender);
    /// <summary>
    /// Interaction logic for ManagerHeaderControl.xaml
    /// </summary>
    public partial class ManagerHeaderControl : UserControl
    {
        public event LogOffHandler OnLogoffEventHandler;

        private void InvokeOnLogoffEventHandler()
        {
            LogOffHandler handler = OnLogoffEventHandler;
            if (handler != null) handler(this);
        }

        public ManagerHeaderControl()
        {
            InitializeComponent();
        }

        private void LogoffeventHandler(object sender)
        {
            InvokeOnLogoffEventHandler();
        }
    }
}
