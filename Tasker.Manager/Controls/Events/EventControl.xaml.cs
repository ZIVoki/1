﻿using System;
using System.Windows;
using System.Windows.Controls;
using Tasker.Manager.Managers;

namespace Tasker.Manager.Controls.Events
{
    /// <summary>
    /// Interaction logic for EventControl.xaml
    /// </summary>
    public partial class EventControl : UserControl
    {
        private readonly SortMachine CurrentSortMachine = new SortMachine();

        public EventControl()
        {
            InitializeComponent();
            eventListView.GotFocus += eventListView_GotFocus;
        }

        void eventListView_GotFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            //AutoResizeColumns();
        }

        public void AutoResizeColumns()
        {
            GridView gv = eventListView.View as GridView;

            if (gv != null)
            {
                foreach (GridViewColumn gvc in gv.Columns)
                {
                    // Set width to highest possible value
                    gvc.Width = double.MaxValue;

                    // Set to NaN to get the "real" actual width
                    gvc.Width = double.NaN;
                }
            }
        }

        private void GridViewColumnHeaderClickedHandler(object sender, RoutedEventArgs e)
        {
            var headerClicked = e.OriginalSource as GridViewColumnHeader;
            // maybe datacontext
            if (sender != null)
                CurrentSortMachine.GridViewColumnHeaderClickedHandler(((ListView)sender).ItemsSource, headerClicked, this);
            AutoResizeColumns();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            var headerClicked =
                  e.OriginalSource as GridViewColumnHeader;
            if (sender != null)
                CurrentSortMachine.GridViewColumnHeaderClickedHandler(((ListView)sender).ItemsSource, headerClicked, this);
            AutoResizeColumns();
        }
    }
}
