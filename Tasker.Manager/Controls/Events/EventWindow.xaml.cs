﻿using Tasker.Tools.Extensions;

namespace Tasker.Manager.Controls.Events
{
    /// <summary>
    /// Interaction logic for EventWindow.xaml
    /// </summary>
    public partial class EventWindow : WindowExt
    {
        public EventWindow()
        {
            InitializeComponent();
        }
    }
}
