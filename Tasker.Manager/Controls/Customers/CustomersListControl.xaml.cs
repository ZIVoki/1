﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using Tasker.Manager.Controls.Task;
using Tasker.Manager.Managers;
using Tasker.Manager.Managers.Logger;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Manager.Controls.Customers
{
    /// <summary>
    /// Interaction logic for CustomersListControl.xaml
    /// </summary>
    public partial class CustomersListControl : UserControl
    {
        readonly SortMachine _currentSortMachine = new SortMachine();
        public event SelectionChangedDelegate SelectionChangedHandler;

        protected virtual void OnSelectionChangedHandler(SelectionChangedArgs args)
        {
            SelectionChangedDelegate handler = SelectionChangedHandler;
            if (handler != null) handler(this, args);
        }

        public CustomersListControl()
        {
            InitializeComponent();
            Repository.Customers.CollectionChanged += Customers_CollectionChanged;
        }

        void Customers_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            AutoResizeColumns();
        }

        public void DeleteSelectedItems()
        {
            if (CusomersListView.SelectedItem == null) return;

            CustomerView deleteCustomerView = (CustomerView) CusomersListView.SelectedItem;

            string message = string.Format("Удалить Клиента {0}", deleteCustomerView.Title);
            MessageBoxResult messageBoxResult = MessageBox.Show(message, 
                                                                "Удаления Клиента", 
                                                                MessageBoxButton.YesNo,
                                                                MessageBoxImage.Question);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                deleteCustomerView.Status = ConstantsStatuses.Deleted;
                CustomerDTO customerDto = deleteCustomerView.BuildDTO();

                ServiceManager.Inst.ServiceChannel.UpdateCustomer(customerDto);
                Log.Write(LogLevel.Debug, string.Format("Клиент {0} удален.", deleteCustomerView.Title));

                CustomerView firstOrDefault = Repository.Customers.FirstOrDefault(c => c.Id == deleteCustomerView.Id);
                if (firstOrDefault == null) return;
                firstOrDefault.Status = ConstantsStatuses.Deleted;

                CollectionViewSource collectionViewSource = (CollectionViewSource)Resources["CustomersCollectionViewSource"];
                collectionViewSource.View.Refresh();
            }
        }

        private void CustomerCollectionViewSource_OnFilter(object sender, FilterEventArgs e)
        {
            CustomerView customerView = (CustomerView) e.Item;
            e.Accepted = true;
            if (customerView.Status == ConstantsStatuses.Deleted)
            {
                e.Accepted = false;
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            var headerClicked =
                  e.OriginalSource as GridViewColumnHeader;
            if (sender != null)
                _currentSortMachine.GridViewColumnHeaderClickedHandler(((ListView)sender).ItemsSource, headerClicked, this);
            AutoResizeColumns();
        }
        public delegate void ViewNEwWindowEventHAndler(object sender, object arg);

        public event ViewNEwWindowEventHAndler ViewCustomerWindow;

        public void OnViewCustomerWindow(object arg)
        {
            ViewNEwWindowEventHAndler handler = ViewCustomerWindow;
            if (handler != null) handler(this, arg);
        }

        private void ListViewItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ShowEditCustomerWindow(sender);
        }

        private static void ShowEditCustomerWindow(object sender)
        {
            if (sender is ListViewItem)
            {
                ListViewItem listViewItem = ((ListViewItem) sender);

                EditCustomerWindow editCustomerWindow = new EditCustomerWindow {DataContext = listViewItem.DataContext};
                editCustomerWindow.ShowDialog();
            }
        }

        public void AutoResizeColumns()
        {
            GridView gv = CusomersListView.View as GridView;

            if (gv != null)
            {
                foreach (GridViewColumn gvc in gv.Columns)
                {
                    // Set width to highest possible value
                    gvc.Width = double.MaxValue;

                    // Set to NaN to get the "real" actual width
                    gvc.Width = double.NaN;
                }
            }
        }

        private void GridViewColumnHeaderClickedHandler(object sender, RoutedEventArgs e)
        {
            var headerClicked = e.OriginalSource as GridViewColumnHeader;
            if (sender != null)
                _currentSortMachine.GridViewColumnHeaderClickedHandler(((ListView)sender).ItemsSource, headerClicked, this);
            AutoResizeColumns();
        }

        private void CusomersListView_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int count = CusomersListView.SelectedItems.Count;
            OnSelectionChangedHandler(new SelectionChangedArgs(count, CusomersListView.Items.Count));
        }
    }
}
