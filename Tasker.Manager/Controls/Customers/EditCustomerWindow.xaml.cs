﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Tasker.Manager.Managers;
using Tasker.Manager.Managers.Logger;
using System.Linq;
using Tasker.Manager.ServiceReference1;
using Tasker.Tools.Extensions;
using Tasker.Tools.Managers;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Manager.Controls.Customers
{
    /// <summary>
    /// Interaction logic for editCustomerWindow.xaml
    /// </summary>
    public partial class EditCustomerWindow : WindowExt
    {
        public CustomerView CurrentCustomerView
        {
            get 
            {
                return (CustomerView) DataContext; 
            }
            set { DataContext = value; }
        }

        public EditCustomerWindow()
        {
            if (DataContext == null)
                CurrentCustomerView = CreateDeafaultCustomer();

            InitializeComponent();
            this.Activate();
        }

        protected override void OnInitialized(System.EventArgs e)
        {
            titleTextBox.Focus();
            base.OnInitialized(e);
        }

        private static CustomerView CreateDeafaultCustomer()
        {
            CustomerView customerView = new CustomerView
                                            {
                                                Title = "Новый клиент",
                                                Description = "Описание",
                                                Channel = Repository.Channels[0],
                                                Status = ConstantsStatuses.Enabled
                                            };
            return customerView;
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveCustomerData();
        }

        protected override void SaveData()
        {
            SaveCustomerData();
            base.SaveData();
        }

        private void SaveCustomerData()
        {
            if (CurrentCustomerView == null) return;


            CustomerDTO customerDto = CurrentCustomerView.BuildDTO();

            if (CurrentCustomerView.Id == 0)
            {
                int newId = ServiceManager.Inst.ServiceChannel.AddCustomer(customerDto);
                if (newId > 0)
                {
                    CurrentCustomerView.Id = newId;
                    Repository.Customers.Add(CurrentCustomerView);
                    Log.Write(LogLevel.Debug, "Добавлен клиент " + CurrentCustomerView.Title);
                }
                else
                {
                    Log.Write(LogLevel.Error, "Ошибка при добавлении нового клиента.");
                    return;
                }
            }
            else
            {
                ServiceManager.Inst.ServiceChannel.UpdateCustomer(customerDto);
                Log.Write(LogLevel.Debug, "Обновлен заказчик" + CurrentCustomerView.Title);
            }

            SaveTaskTypeChanges();

            this.Close();
        }

        private void SaveTaskTypeChanges()
        {
            // take taskType Ids !!

            IEnumerable<int> oldCustomerTaskTypes =
                Repository.CustomerTaskTypes.Where(
                    c => c.Customer.Id == CurrentCustomerView.Id && c.Status == ConstantsStatuses.Enabled).Select(
                        c => c.TaskType.Id);
            IEnumerable<int> newCustomerTaskTypes = CurrentCustomerView.TaskTypeViews.Select(tt => tt.Id);

            IEnumerable<int> taskTypesIdsToDel = oldCustomerTaskTypes.Except(newCustomerTaskTypes);
            IEnumerable<int> taskTypeIdsToAdd = newCustomerTaskTypes.Except(oldCustomerTaskTypes);

            DelTaskTypesFromCustomer(taskTypesIdsToDel);

            AddNewTaskTypeToCustomer(taskTypeIdsToAdd);
        }

        private void DelTaskTypesFromCustomer(IEnumerable<int> taskTypesIdsToDel)
        {
            CustomerTaskTypeDTO[] allCustomerTaskTypes;

            lock (Repository.CustomerTaskTypes)
            {
                allCustomerTaskTypes = Repository.CustomerTaskTypes.Where(
                    tt => tt.Customer.Id == CurrentCustomerView.Id && tt.Status == ConstantsStatuses.Enabled).ToArray();
            }

            if (allCustomerTaskTypes.Length == 0) return;

            foreach (var id in taskTypesIdsToDel)
            {
                CustomerTaskTypeDTO customerTaskTypeDto = allCustomerTaskTypes.FirstOrDefault(tt => tt.TaskType.Id == id);
                if (customerTaskTypeDto == null) continue;

                customerTaskTypeDto.Status = ConstantsStatuses.Deleted;
                ServiceManager.Inst.ServiceChannel.UpdateCustomersTaskTypes(customerTaskTypeDto);
            }
        }

        private void AddNewTaskTypeToCustomer(IEnumerable<int> taskTypeIds)
        {
            TaskTypeDTO[] taskTypeDtos;
            lock (CurrentCustomerView.TaskTypeViews)
            {
                taskTypeDtos = CurrentCustomerView.TaskTypeViews.Select(tt => tt.BuildDTO()).ToArray();
            }

            if (taskTypeDtos.Length == 0) return;

            foreach (var id in taskTypeIds)
            {
                TaskTypeDTO taskTypeDto = taskTypeDtos.FirstOrDefault(tt => tt.Id == id);

                CustomerTaskTypeDTO customerTaskTypeDto = new CustomerTaskTypeDTO
                {
                    Customer = CurrentCustomerView.BuildDTO(),
                    Status = ConstantsStatuses.Enabled,
                    TaskType = taskTypeDto
                };

                int newId = ServiceManager.Inst.ServiceChannel.AddCustomersTaskTypes(customerTaskTypeDto);
                if (newId > 0)
                {
                    customerTaskTypeDto.Id = newId;
                    Repository.CustomerTaskTypes.Add(customerTaskTypeDto);
                    Log.Write(LogLevel.Debug, string.Format("Добавлен новый тип задачи {0} клиенту {1}", taskTypeDto.Title, CurrentCustomerView.Title));
                }
                else
                {
                    Log.Write(LogLevel.Error, string.Format("Ошибка при добавлении типа задачи клиенту.{0}", CurrentCustomerView.Title));
                }
            }
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            if (CurrentCustomerView.Id != 0)
            {
                CustomerDTO customerDto = ServiceManager.Inst.ServiceChannel.GetCustomer(CurrentCustomerView.Id);
                CustomerManager.CustomerUpdate(Repository.Customers, Repository.Channels, customerDto);

                RevertTaskTypeValues();
            }

            this.Close();
        }

        private void RevertTaskTypeValues()
        {
            IEnumerable<CustomerTaskTypeDTO> customerTaskTypeDtos =
                Repository.CustomerTaskTypes.Where(ctt => ctt.Customer.Id == CurrentCustomerView.Id);
            lock (CurrentCustomerView)
            {
                CurrentCustomerView.TaskTypeViews.Clear();
            }
            foreach (var customerTaskTypeDto in customerTaskTypeDtos)
            {
                TaskTypeView firstOrDefault;
                lock (Repository.TaskTypes)
                {
                    firstOrDefault = Repository.TaskTypes.FirstOrDefault(tt => tt.Id == customerTaskTypeDto.TaskType.Id);
                }
                if (firstOrDefault != null)
                    lock (CurrentCustomerView)
                    {
                        CurrentCustomerView.TaskTypeViews.Add(firstOrDefault);
                    }
            }
        }

        private void textBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (sender is TextBox)
            {
                ((TextBox)sender).SelectAll();
            }
        }

        private void HeaderCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            taskTypesListView.SelectAll();
        }

        private void HeaderCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            taskTypesListView.UnselectAll();
        }

        private void EditCustomerWindow_OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (Repository.TaskTypes.Count == CurrentCustomerView.TaskTypeViews.Count)
                selectAllCheckBox.IsChecked = true;
            else
                selectAllCheckBox.IsChecked = false;
        }

        private void ChannelsCollectionViewSource_OnFilter(object sender, FilterEventArgs e)
        {
            ChannelView userView = (ChannelView)e.Item;
            if (userView == null) return;

            if (userView.Status == ConstantsStatuses.Enabled) e.Accepted = true;
            else e.Accepted = false;
        }
    }
}
