﻿using System.Windows;
using Tasker.Manager.ServiceReference1;
using Tasker.Tools.Extensions;

namespace Tasker.Manager.Controls.Task
{
    /// <summary>
    /// Interaction logic for NewTaskWindow.xaml
    /// </summary>
    public partial class StatisticWindow : WindowExt
    {
        public StatisticWindow(TaskStatisticRecordDTO[] context)
        {
            InitializeComponent();
            lstStatistic.DataContext = context;
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
