﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Tasker.Tools.Extensions;
using Tasker.Manager.Managers;
using Tasker.Manager.Managers.Logger;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Manager.Controls.Task
{
    /// <summary>
    /// Interaction logic for NewTaskWindow.xaml
    /// </summary>
    public partial class NewTaskWindow : WindowExt
    {
        readonly TaskView _currentTaskView = new TaskView();

        public NewTaskWindow()
        {
            SetTaskViewDefaultParams();
            InitializeComponent();
            ThisNewTaskControl.DataContext = _currentTaskView;
            ThisNewTaskControl.CloseTaskButton.Visibility = Visibility.Hidden;
            ThisNewTaskControl.PausePlayButton.Visibility = Visibility.Hidden;
            ThisNewTaskControl.TaskTitleTextBox.Focus();
            this.Activate();
        }

        private void SetTaskViewDefaultParams()
        {
            _currentTaskView.Status = ConstantsStatuses.Enabled;
            _currentTaskView.Description = "";
            _currentTaskView.Creator = Repository.LoginUser;
            _currentTaskView.Status = ConstantsStatuses.Enabled;
            _currentTaskView.PriorityType = PriorityType.Quick;

            CustomerView firstCustomerView = Repository.Customers.FirstOrDefault(c => c.Status == ConstantsStatuses.Enabled);
            if (firstCustomerView != null)
            {
                _currentTaskView.Customer = firstCustomerView;
                TaskTypeView taskTypeView = firstCustomerView.TaskTypeViews.FirstOrDefault();
                if (taskTypeView != null)
                {
                    _currentTaskView.TaskType = taskTypeView;
                    _currentTaskView.PlannedTime = taskTypeView.CompletionTime;
                }
            }

            _currentTaskView.DeadLine = DateTime.Now;
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void saveNewTaskButton_Click(object sender, RoutedEventArgs e)
        {
            SaveData();
        }

        protected override void SaveData()
        {
            if (!CheckTaskData()) return;

            SaveNewTask();
            base.SaveData();
        }

        private bool CheckTaskData()
        {
            if (_currentTaskView.TaskType == null)
            {
                MessageBox.Show("Необходимо определить тип задачи", "Ошибка сохранения задачи", MessageBoxButton.OK,
                                MessageBoxImage.Asterisk);
                return false;
            }
            if (String.IsNullOrEmpty(_currentTaskView.Title))
            {
                MessageBox.Show("Необходимо ввести название задачи", "Ошибка сохранения задачи", MessageBoxButton.OK,
                                MessageBoxImage.Asterisk);
                return false;
            }
            if (_currentTaskView.Customer == null)
            {
                MessageBox.Show("Необходимо определить заказчика", "Ошибка сохранения задачи", MessageBoxButton.OK,
                                MessageBoxImage.Asterisk);
                return false;
            }

            return true;
        }

        private async void SaveNewTask()
        {
            SaveNewTaskButton.IsEnabled = false;
            AddingNewTaskCircularProgressBar.Visibility = Visibility.Visible;

            if (_currentTaskView.CurrentWorker != null)
            {
                if (_currentTaskView.PriorityType == PriorityType.Quick)
                    _currentTaskView.CurrentWorker.UserWorkState = Tools.Constants.UserWorkState.Busy;
                if (_currentTaskView.PriorityType == PriorityType.Background)
                    _currentTaskView.CurrentWorker.UserWorkState = Tools.Constants.UserWorkState.BackgroundTask;
            }

            if (_currentTaskView.CurrentWorker != null)
                _currentTaskView.Status = ConstantsStatuses.Planing;

            _currentTaskView.CreationDate = DateTime.Now;
            if (_currentTaskView.CreationDate.Add(_currentTaskView.PlannedTime) > _currentTaskView.DeadLine)
                _currentTaskView.DeadLine = _currentTaskView.CreationDate.Add(_currentTaskView.PlannedTime);

            try
            {
                int taskId = await ServiceManager.Inst.ServiceChannel.AddTaskAsync(_currentTaskView.BuildDTO());
                _currentTaskView.Id = taskId;
            }
            catch (Exception exception)
            {
                Log.Write(LogLevel.Error, exception.StackTrace);
                Debug.WriteLine(exception);
            }

            if (_currentTaskView.Id > 0)
            {
                // добавляем файлы к новой задаче
                if (_currentTaskView.Files != null)
                {
                    foreach (var file in _currentTaskView.Files.Where(f => f.Id == 0))
                    {
                        int fileId = await ServiceManager.Inst.ServiceChannel.AddFileToTaskAsync(file.BuildDTO());
                        file.Id = fileId;
                        if (file.Id == 0)
                            Log.Write(LogLevel.Error, string.Format("Ошибка при добавлении нового файла задачи{0}.", _currentTaskView.Id));
                    }
                }
                Repository.Tasks.Add(_currentTaskView);
            }
            else
                Log.Write(LogLevel.Error, "Ошибка на стороне сервера при добавлении новой задачи.");

            AddingNewTaskCircularProgressBar.Visibility = Visibility.Hidden;
            this.Close();
        }

        protected override void OnClosed(EventArgs e)
        {
/*
            if (currentTaskView.Id != 0)
            {
                UserView userView = currentTaskView.CurrentWorker;

                if (userView != null)
                {
                    //reload active task from server
                    IEnumerable<int> tasksIds =
                        Repository.Tasks.Where(
                            t =>
                            (t.Status == ConstantsStatuses.InProcess || t.Status == ConstantsStatuses.Disabled) &&
                            t.CurrentWorker != null &&
                            t.CurrentWorker.Id != null &&
                            t.CurrentWorker.Id == currentTaskView.CurrentWorker.Id).Select(t => t.Id);

                    foreach (int taskViewId in tasksIds)
                    {
                        var tsk = ServiceManager.Inst.ServiceChannel.GetTask(taskViewId);
                        TaskManager.UpdateTask(tsk, Repository.Customers, Repository.TaskTypes,
                                               Repository.Users, Repository.Channels, null,
                                               Repository.Tasks,
                                               Repository.HighlightSettings.GetTaskTypeHighlightColor(tsk.TaskType.Id),
                                               Repository.HighlightSettings.GetCustomerHighlightColor(tsk.Customer.Id));
                    }
                }
            }
*/

            base.OnClosed(e);
        }
    }
}
