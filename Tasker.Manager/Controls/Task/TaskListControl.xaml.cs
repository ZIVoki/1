﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using Tasker.Manager.Managers;
using Tasker.Manager.ServiceReference1;
using Tasker.Tools.CustomEventArgs;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Manager.Controls.Task
{
    public delegate void ShowViewWindowEventHandler(object sender, object dataContext);

    /// <summary>
    /// Interaction logic for TaskListControl.xaml
    /// </summary>
    public partial class TaskListControl : UserControl
    {
        public List<ChatWindow> OpenedChatWindows = new List<ChatWindow>();
        public event MultiArgumentContainsDelegate NewUnreadMessageComeHandler;
        public event MultiArgumentContainsDelegate HasUnreadChangedHandler;
        public event SelectionChangedDelegate SelectionChangedHandler;

        private readonly SortMachine _currentSortMachine = new SortMachine();
        private bool _activeTaskFilter = false;
        private bool _closeTaskFilter = false;
        private bool _mineTaskFilter = false;
        private bool _todayTaskFilter = false;
        private bool _yesterdayTaskFilter = false;
        private bool _customDatesTaskFilter = false;

        private CustomerView _filterCustomer;
        private DateTime _customFilterStartDate;
        private DateTime _customFilterEndDate;
        private UserView _taskCreatorFilter;
        private ChannelView _channelViewFilter;

        #region FilterProperty

        public static readonly DependencyProperty CustomerHightLightProperty =
            DependencyProperty.Register("CustomerHighLight", typeof(bool), typeof(TaskListControl));
        public static readonly DependencyProperty TaskTypeHighLightProperty =
            DependencyProperty.Register("TaskTypeHighLight", typeof(bool), typeof(TaskListControl));

        public bool CustomerHighLight
        {
            get
            {
                return (bool)GetValue(CustomerHightLightProperty);
            }
            set
            {
                SetValue(CustomerHightLightProperty, value);
            }
        }

        public bool TaskTypeHighLight
        {
            get
            {
                return (bool)GetValue(TaskTypeHighLightProperty);
            }
            set
            {
                SetValue(TaskTypeHighLightProperty, value);
            }
        }


        public bool ActiveTaskFilter
        {
            get { return _activeTaskFilter; }
            set
            {
                TaskListView.UnselectAll();
                _activeTaskFilter = value;
                RefreshView();
            }
        }

        public bool CloseTaskFilter
        {
            get { return _closeTaskFilter; }
            set 
            {
                TaskListView.UnselectAll();
                _closeTaskFilter = value;
                RefreshView();
            }
        }

        public bool MineTaskFilter
        {
            get { return _mineTaskFilter; }
            set 
            {
                TaskListView.UnselectAll();
                _mineTaskFilter = value;
                RefreshView();
            }
        }

        public CustomerView FilterCustomer
        {
            get
            {
                return _filterCustomer;
            }
            set
            {
                TaskListView.UnselectAll();
                _filterCustomer = value;
                RefreshView();
            }
        }

        public DateTime CustomFilterEndDate
        {
            get { return _customFilterEndDate; }
            set
            {
                TaskListView.UnselectAll();
                _customFilterEndDate = value;
                RefreshView();
            }
        }

        public DateTime CustomFilterStartDate
        {
            get { return _customFilterStartDate; }
            set
            {
                TaskListView.UnselectAll();
                _customFilterStartDate = value;
                RefreshView();
            }
        }

        public bool CustomDatesTaskFilter
        {
            get { return _customDatesTaskFilter; }
            set
            {
                TaskListView.UnselectAll();
                _customDatesTaskFilter = value;
                if (value)
                {
                    _yesterdayTaskFilter = false;
                    _todayTaskFilter = false;
                }
                RefreshView();
            }
        }

        public bool YesterdayTaskFilter
        {
            get { return _yesterdayTaskFilter; }
            set
            {
                TaskListView.UnselectAll();
                _yesterdayTaskFilter = value;
                if (value)
                {
                    _todayTaskFilter = false;
                    _customDatesTaskFilter = false;
                }
                RefreshView();
            }
        }

        public bool TodayTaskFilter
        {
            get { return _todayTaskFilter; }
            set
            {
                TaskListView.UnselectAll();
                _todayTaskFilter = value;
                if (value)
                {
                    _yesterdayTaskFilter = false;
                    _customDatesTaskFilter = false;
                }
                RefreshView();
            }
        }

        public UserView TaskCreatorFilter
        {
            get { return _taskCreatorFilter;  }
            set
            {
                TaskListView.UnselectAll();
                _taskCreatorFilter = value;
                RefreshView();
            }
        }

        public ChannelView ChannelViewFilter
        {
            get { return _channelViewFilter; }
            set
            {
                TaskListView.UnselectAll();
                _channelViewFilter = value;
                RefreshView();
            }
        }

        #endregion

        public bool IsDesignTime
        {
            get
            {
                return (Application.Current == null) ||
                       (Application.Current.GetType() == typeof(Application));
            }
        }

        public TaskListControl()
        {
            InitializeComponent();
            if (IsDesignTime) return;

            CollectionViewSource taskCollectionViewSource = (CollectionViewSource)Resources["TaskCollectionViewSource"];
            taskCollectionViewSource.SortDescriptions.Add(new SortDescription("Id", ListSortDirection.Descending));

            Repository.Tasks.CollectionChanged += taskCollectionChanged;
            ChatWindowsManager.ChatWindowClosingEventHandler += ChatWindowClosed;
            ServiceManager.Inst.Callback.TaskUpdatedEvent += ManagerCallback_TaskUpdatedEvent;

            foreach (TaskView taskView in Repository.Tasks)
            {
                taskView.NewMessagesAddedHandler += MessageCollectionChanged;
                taskView.HasUnreadMessagesChangedHandler += HasUnreadeadChanged;
                taskView.StatusChangedEventHandler += TaskStatusChangedEventHandler;
            }
        }

        private void UcTaskKictControlLoaded(object sender, RoutedEventArgs e)
        {
            GridView gridView = (GridView)TaskListView.View;
            gridView.Columns.CollectionChanged += Columns_CollectionChanged;
        }


        void Columns_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Move)
            {
                string msg = string.Format("Column moved from position {0} to position {1}", e.OldStartingIndex, e.NewStartingIndex);
                Debug.WriteLine(msg);
            }
        }

        private delegate void TaskUpdateDelegate();
        void ManagerCallback_TaskUpdatedEvent(object sender, TaskUpdatedEventArgs e)
        {
            TaskUpdateDelegate taskUpdateDelegate = RefreshView;
            this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, taskUpdateDelegate);
        }

        private void ChatWindowClosed(object sender, ContentEventArgs e)
        {
            var chatWindow = (ChatWindow)e.Arg;
            OpenedChatWindows.Remove(chatWindow);
        }

        private bool _stopRefresh = false;
        public bool StopRefresh
        {
            get { return _stopRefresh; }
            set
            {
                _stopRefresh = value;
                if (!value)
                    RefreshView();
            }
        }

        private void taskCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            foreach (var newItem in e.NewItems)
            {
                var task = (TaskView) newItem;
                task.NewMessagesAddedHandler += MessageCollectionChanged;
                task.HasUnreadMessagesChangedHandler += HasUnreadeadChanged;
                task.StatusChangedEventHandler += TaskStatusChangedEventHandler;
            }
            RefreshView();
        }

        void TaskStatusChangedEventHandler(object sender, ContentsEventArgs e)
        {
            RefreshView();
        }

        void RefreshView()
        {
            if (StopRefresh) return;
            CollectionViewSource taskCollectionViewSource = (CollectionViewSource)Resources["TaskCollectionViewSource"];

            if (taskCollectionViewSource == null) return;
            try
            {
                taskCollectionViewSource.View.Refresh();
            }
            catch (InvalidOperationException e)
            {
            }
            AutoResizeColumns();
        }

        private void GridViewColumnHeaderClickedHandler(object sender, RoutedEventArgs e)
        {
            var headerClicked = e.OriginalSource as GridViewColumnHeader;
            // maybe datacontext
            if (sender != null)
                _currentSortMachine.GridViewColumnHeaderClickedHandler(TaskListView.ItemsSource, headerClicked, this);
            AutoResizeColumns();
        }

        private void btnChat_Click(object sender, RoutedEventArgs routedEventArgs)
        {
            Button button = (Button) sender;
            TaskView taskView = (TaskView) button.DataContext;
            ShowChatWindow(taskView);
        }

        public void ShowChatWindow(TaskView taskView)
        {
            ChatWindowsManager.ShowChatWindow(taskView, OpenedChatWindows);
        }

        private void TaskCollectionViewSource_OnFilter(object sender, FilterEventArgs e)
        {
            TaskView taskView = (TaskView) e.Item;
            if (taskView == null) return;

            if (_activeTaskFilter)
                if (taskView.Status != ConstantsStatuses.Closed)
                    e.Accepted = true;
                else
                {
                    e.Accepted = false;
                    return;
                }

            if (_closeTaskFilter)
                if (taskView.Status == ConstantsStatuses.Closed)
                    e.Accepted = true;
                else
                {
                    e.Accepted = false;
                    return;
                }

            if (_mineTaskFilter)
                if (taskView.Creator.Id == Repository.LoginUser.Id)
                    e.Accepted = true;
                else
                {
                    e.Accepted = false;
                    return;
                }

            if (_todayTaskFilter)
                if (taskView.CreationDate.Date == DateTime.Now.Date)
                    e.Accepted = true;
                else
                {
                    e.Accepted = false;
                    return;
                }

            if (_customDatesTaskFilter)
            {
                if (taskView.CreationDate.Date >= _customFilterStartDate.Date && 
                    taskView.CreationDate.Date <= _customFilterEndDate.Date)
                {
                    e.Accepted = true;
                }
                else
                {
                    e.Accepted = false;
                    return;
                }
            }

            if (_yesterdayTaskFilter)
                if (taskView.CreationDate.Date == DateTime.Now.AddDays(-1).Date)
                    e.Accepted = true;
                else
                {
                    e.Accepted = false;
                    return;
                }

            if (_taskCreatorFilter != null)
                if (taskView.Creator.Id == _taskCreatorFilter.Id)
                {
                    e.Accepted = true;
                }
                else
                {
                    e.Accepted = false;
                    return;
                }

            if (_filterCustomer != null)
                if (taskView.Customer.Id == _filterCustomer.Id)
                    e.Accepted = true;
                else
                {
                    e.Accepted = false;
                    return;
                }

            if (_channelViewFilter != null)
                if (taskView.Customer.Channel.Id == _channelViewFilter.Id)
                    e.Accepted = true;
                else
                {
                    e.Accepted = false;
                    return;
                }
        }

        private delegate void CheckMessageUserReadHandler(List<MessageView> arg);
        private void MessageCollectionChanged(object sender, ContentEventArgs contentEventArgs)
        {
            var loginSuccessDelegate = new CheckMessageUserReadHandler(CheckMessageReadedByUser);
            this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, loginSuccessDelegate, (List<MessageView>)contentEventArgs.Arg);
        }

        private void CheckMessageReadedByUser(List<MessageView> messages)
        {
            if(messages.Count > 0)
            {
                var taskId = messages[0].Task.Id;
                var task = messages[0].Task;
                foreach (ChatWindow chatWindow in OpenedChatWindows)
                {
                    if (chatWindow.CurrentTask.Id == taskId && chatWindow.WindowState != WindowState.Minimized && chatWindow.IsActive)
                    {
                        //todo check window state
                        ((TaskView)chatWindow.DataContext).Messages.SetAllAsRead();
                    }
                }
                
                foreach (var item in messages)
                {
                    if (!item.IsReaded)
                    {
                        InvokeSystemEventCome(Tools.Constants.BaloonType.Message, item.UserFrom.Title, item.Text, item.Task);
                    }
                }
            }
        }

        public void InvokeSystemEventCome(Tools.Constants.BaloonType eventType, string title, string text, TaskView task)
        {
            var handler = NewUnreadMessageComeHandler;
            if (handler != null) handler(null, new ContentsEventArgs(new object[] { eventType, title, text, task }));
        }

        public void InvokeHasUnreadChanged(int taskId, bool value)
        {
            var handler = HasUnreadChangedHandler;
            if (handler != null) handler(null, new ContentsEventArgs(new object[] { taskId, value }));
        }

        private void HasUnreadeadChanged(object sender, ContentsEventArgs e)
        {
            var task = (int)e.Args[0];
            var has = (bool)e.Args[1];
            InvokeHasUnreadChanged(task, has);
        }

        private void EditTaskClick(object sender, RoutedEventArgs e)
        {
            TaskView taskView = null;
            if (sender is Hyperlink)
                taskView = (TaskView)((Hyperlink)sender).DataContext;
            else if (sender is Button)
                taskView = (TaskView) ((Button) sender).DataContext;

            if (taskView != null)
                ShowEditTaskWindow(taskView);
        }

        private static void ShowEditTaskWindow(TaskView taskView)
        {
            if (taskView == null) return;
            var updateTaskWindow = new UpdateTaskWindow(taskView);
            updateTaskWindow.Show();
        }

        private void ListViewItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ShowNoneEditTaskWindow(sender);
        }        

        /// <summary>
        /// takes only ListViewItem as arg
        /// </summary>
        /// <param name="sender"></param>
        private static void ShowNoneEditTaskWindow(object sender)
        {
            if (sender is ListViewItem)
            {
                ListViewItem listViewItem = ((ListViewItem) sender);
                if (listViewItem.DataContext is TaskView)
                {
                    ViewTaskWindow viewTaskWindow = new ViewTaskWindow { DataContext = listViewItem.DataContext };
                    viewTaskWindow.ShowDialog();
                }
            }
        }

        public void AutoResizeColumns()
        {
            try
            {
                GridView gv = TaskListView.View as GridView;

                if (gv != null)
                {
                    foreach (GridViewColumn gvc in gv.Columns)
                    {
                        // Set width to highest possible value
                        gvc.Width = double.MaxValue;

                        // Set to NaN to get the "real" actual width
                        gvc.Width = double.NaN;
                    }
                }
            }
            catch (InvalidOperationException e)
            {}
        }

        private void BtnStatisticsClick(object sender, RoutedEventArgs e)
        {
            TaskView task = (TaskView)((Button) sender).DataContext;
            TaskStatisticRecordDTO[] statistics = ServiceManager.Inst.ServiceChannel.GetTaskStatistic(task.Id);
            StatisticWindow frm = new StatisticWindow(statistics) {Title = task.Title};
            frm.ShowDialog();
        }

        private void EventSetter_OnHandler(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                if (e.KeyboardDevice.IsKeyDown(Key.LeftCtrl) || e.KeyboardDevice.IsKeyDown(Key.RightCtrl))
                {
                    if (sender is ListViewItem)
                    {
                        ListViewItem listViewItem = ((ListViewItem) sender);
                        if (listViewItem.DataContext is TaskView)
                            ShowEditTaskWindow((TaskView) listViewItem.DataContext);
                    }
                }
                else
                    ShowNoneEditTaskWindow(sender);
        }

        public void Unsign()
        {
            Repository.Tasks.CollectionChanged -= taskCollectionChanged;
            ChatWindowsManager.ChatWindowClosingEventHandler -= ChatWindowClosed;
            ServiceManager.Inst.Callback.TaskUpdatedEvent -= ManagerCallback_TaskUpdatedEvent;
            foreach (var openedChatWindow in OpenedChatWindows)
            {
                openedChatWindow.Close();
            }
        }

        private void TaskListView_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int count = TaskListView.SelectedItems.Count;
            OnSelectionChangedHandler(new SelectionChangedArgs(count, TaskListView.Items.Count));
        }

        protected virtual void OnSelectionChangedHandler(SelectionChangedArgs selectionChangedArgs)
        {
            SelectionChangedDelegate handler = SelectionChangedHandler;
            if (handler != null) handler(this, selectionChangedArgs);
        }
    }
}
