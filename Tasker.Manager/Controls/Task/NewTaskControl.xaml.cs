﻿using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Tasker.Manager.Managers;
using Tasker.Tools.Controls;
using Tasker.Tools.Extensions;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Manager.Controls.Task
{
    /// <summary>
    /// Interaction logic for NewTaskControl.xaml
    /// </summary>
    public partial class NewTaskControl : UserControl
    {
        public delegate void CloseEventDelagate();
        public CloseEventDelagate OnClosing;
        private bool _isPaused;

        public NewTaskControl()
        {
            this.Initialized += NewTaskControl_Initialized;
            this.DataContextChanged += NewTaskControl_DataContextChanged;
            InitializeComponent();

            UpdateBusyUsersInfo();
        }

        private void UpdateBusyUsersInfo()
        {
            Repository.UsersLoadManager.UpdateData();
            foreach (var userView in Repository.Users.Where(u => u.UserWorkState == Tools.Constants.UserWorkState.Busy))
            {
                userView.UserWorkState = Tools.Constants.UserWorkState.Free;
            }
            foreach (var taskView in Repository.Tasks.Where(t => t.Status == ConstantsStatuses.InProcess))
            {
                if (taskView.PriorityType == PriorityType.Quick)
                    taskView.CurrentWorker.UserWorkState = Tools.Constants.UserWorkState.Busy;
                if (taskView.PriorityType == PriorityType.Background)
                    taskView.CurrentWorker.UserWorkState = Tools.Constants.UserWorkState.BackgroundTask;
            }
        }

        void NewTaskControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            CheckResourceAvailable();
            SetPlannedTimeByCurTaskType();
        }

        void NewTaskControl_Initialized(object sender, EventArgs e)
        {
            RefreshTaskTypesView();
        }

        public TaskView CurrentTaskView
        {
            get
            {
                if (DataContext is TaskView)
                    return (TaskView)DataContext;
                return null;
            }
            set { DataContext = value; }
        }

        private void NewFilesCatcher_AddNewFilePathEvent(object sender, AddNewFilePathEventArgs e)
        {
            /*var dialogResult =
                MessageBox.Show("Пометить последние добавленные файлы как 'Файл с конечным результатом'?",
                                "Добаление новых файлов", MessageBoxButton.YesNo, MessageBoxImage.Question);*/

            foreach (var filePath in e.FilePath)
            {
                FileInfo fileInfo = new FileInfo(filePath);
                AttachFileView exist = null;

                //check files collection of task. find exists
                if (CurrentTaskView.Files != null)
                {
                    string path = filePath;
                    exist = CurrentTaskView.Files.FirstOrDefault(a => a.FilePath == path);
                }
                else
                    CurrentTaskView.Files = new UniqBindableCollection<AttachFileView>();

                if (exist == null)
                {
                    AttachFileView attachFile = new AttachFileView(CurrentTaskView)
                    {
                        CreatedAt = DateTime.Now,
                        FilePath = filePath,
                        Title = fileInfo.Name,
                        IsFinalyFile = false,
                        Status = ConstantsStatuses.Enabled
                    };

                    CurrentTaskView.Files.Add(attachFile);
                }
            }
        }

        private void categoryComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetPlannedTimeByCurTaskType();
        }

        private void SetPlannedTimeByCurTaskType()
        {
            if (CurrentTaskView == null) return;

            if (CurrentTaskView.Id == 0)
            {
                TaskTypeView taskTypeView;
                if (TaskTypeComboBox.SelectedItem == null)
                    taskTypeView = (TaskTypeView)TaskTypeComboBox.Items.CurrentItem;
                else
                    taskTypeView = (TaskTypeView)TaskTypeComboBox.SelectedItem;
                if (taskTypeView == null) return;

                CurrentTaskView.PlannedTime = taskTypeView.CompletionTime;

                // + минута форы
                CurrentTaskView.DeadLine = DateTime.Now.Add(CurrentTaskView.PlannedTime).Add(new TimeSpan(0, 0, 0, 1, 0));
            }
        }

        private void closeTaskButton_Click(object sender, RoutedEventArgs e)
        {
            if (CurrentTaskView == null) return;

            bool close = true;
            if (CurrentTaskView.CurrentWorker != null)
            {
                string message = string.Format("Над задачей {0} производятся работы, вы уверены, что хотите её закрыть?",
                                               CurrentTaskView.Title);
                var dialogResult = MessageBox.Show(message, "Закрытие задачи", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (dialogResult == MessageBoxResult.No) close = false;
            }

            if (close)
            {
                CurrentTaskView.Status = ConstantsStatuses.Closed;
                if (CurrentTaskView.CurrentWorker != null)
                {
                    CurrentTaskView.CurrentWorker.UserWorkState = Tools.Constants.UserWorkState.Free;
                    ServiceManager.Inst.ServiceChannel.CheckOutWorker(CurrentTaskView.CurrentWorker.BuildDTO());
                }
                invokeCloseWindowContainer();
            }
        }

        private void UserCollectionViewSource_OnFilter(object sender, FilterEventArgs e)
        {
            UserView userView = (UserView)e.Item;
            if (userView == null) return;

            if (userView.Role == ConstantsRoles.Worker && userView.Status != ConstantsStatuses.Deleted) 
                e.Accepted = true;
            else 
                e.Accepted = false;
        }

        private void btnPausePlay_Click(object sender, RoutedEventArgs e)
        {
            if (!_isPaused)
            {
                var dialogResult = MessageBox.Show("Вы уверены что хотите приостановить выполнение задачи? Занятый на ней сотрудник освободится.",
                                                     "Приостановка задачи", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (dialogResult == MessageBoxResult.Yes)
                {
                    TaskView task = (TaskView)DataContext;
                    task.Status = ConstantsStatuses.Disabled;
                    invokeCloseWindowContainer();
                }
            }
            else
            {
                CurrentTaskView.Status = ConstantsStatuses.Enabled;
                var taskDTO = CurrentTaskView.BuildDTO();
                ServiceManager.Inst.ServiceChannel.UpdateTask(taskDTO);
                _isPaused = false;
                PausePlayButton.Content = "Приостановить";
            }
        }

        private void invokeCloseWindowContainer()
        {
            var handler = OnClosing;
            if (handler != null) handler();
        }

        private void UserControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (CurrentTaskView.Status == ConstantsStatuses.Disabled)
            {
                PausePlayButton.Content = "Запустить";
                _isPaused = true;
            }
        }

        private void taskEndDateDameer_LostFocus(object sender, RoutedEventArgs e)
        {
            if (CurrentTaskView == null) return;
            // + минута форы
            //if task is has creation date then calculate work time by this
            //taskView.DeadLine = DateTime.Now.Add(taskView.PlannedTime).Add(new TimeSpan(0, 0, 0, 1, 0));
            if (CurrentTaskView.CreationDate != default(DateTime))
            {
                CurrentTaskView.DeadLine = CurrentTaskView.CreationDate.Add(CurrentTaskView.PlannedTime).Add(new TimeSpan(0, 0, 0, 1, 0));
            }
            else
            {
                CurrentTaskView.DeadLine = DateTime.Now.Add(CurrentTaskView.PlannedTime).Add(new TimeSpan(0, 0, 0, 1, 0));
            }
            CheckSelectedUserTT();
        }

        private void textBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (sender is TextBox)
            {
                var taskView = (TaskView)DataContext;
                if (taskView == null) return;

                if (taskView.Id == 0)
                    ((TextBox)sender).SelectAll();
            }
        }

        void RefreshTaskTypesView()
        {
            CollectionViewSource taskCollectionViewSource = (CollectionViewSource)Resources["AvailableTaskTypesViewSource"];

            if (taskCollectionViewSource == null) return;
            try
            {
                taskCollectionViewSource.View.Refresh();
            }
            catch (InvalidOperationException e)
            {
            }
        }

        private void CollectionViewSourceFilter(object sender, FilterEventArgs e)
        {
            TaskTypeView currentTaskType = (TaskTypeView)e.Item;
            CustomerView customerView = (CustomerView)CustomerComboBox.SelectedValue;
            if (customerView == null)
            {
                e.Accepted = false;
                return;
            }

            if (customerView.TaskTypeViews.Contains(currentTaskType)) e.Accepted = true;
            else e.Accepted = false;
        }

        private void CustomerComboBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            RefreshTaskTypesView();
            if (IsInitialized)
            {
                if (TaskTypeComboBox.Items.Count == 0)
                    CurrentTaskView.TaskType = null;
                else
                    TaskTypeComboBox.SelectedIndex = 0;
                if (CustomerComboBox.Items.Count == 0)
                    CurrentTaskView.Customer = null;
                CheckResourceAvailable();
            }
        }

        private void CheckResourceAvailable()
        {
            int count =
                Repository.Tasks.Where(
                    t =>
                    t.Customer != null && t.Customer.Id == CurrentTaskView.Customer.Id &&
                    t.Status == ConstantsStatuses.InProcess).
                           GroupBy(t => t.CurrentWorker).Count();
            int limit;
            if (CurrentTaskView.Customer == null)
                limit = 100;
            else
                int.TryParse(CurrentTaskView.Customer.MaxWorkers, out limit);
            if (limit - count > 0)
            {
                LeftWorkersCountTextBlock.Text = string.Format("Осталось {0} человек", limit - count);
                CustomersResourceAlertImage.Visibility = Visibility.Hidden;
            }
            else
            {
                LeftWorkersCountTextBlock.Text = "Людей не осталось";
                CustomersResourceAlertImage.Visibility = Visibility.Visible;
            }
        }

        private void CustomersViewSourceFilter(object sender, FilterEventArgs e)
        {
            CustomerView customer = (CustomerView)e.Item;
            if (customer == null)
            {
                e.Accepted = false;
                return;
            }

            if (customer.Status == ConstantsStatuses.Enabled) e.Accepted = true;
            else e.Accepted = false;
        }

        private void UsersComboBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!this.IsInitialized) return;
            CheckSelectedUserTT();
        }

        private void CheckSelectedUserTT()
        {
            UserInfoMessageTextBlock.Text = "";
            if (CurrentTaskView.CurrentWorker != null)
            {
                if (CurrentTaskView.PriorityType == PriorityType.Background) return;

                DateTime now = DateTime.Now;
                TimeTableView timeTableView = Repository.TimeTables.FirstOrDefault(
                    tt =>
                    tt.Status == ConstantsStatuses.Enabled &&
                    tt.User == CurrentTaskView.CurrentWorker &&
                    tt.StartDate <= now &&
                    tt.EndDate >= now);
                if (timeTableView == null)
                {
                    string message =
                        String.Format("Смена исполнителя {0} подошла к концу.", CurrentTaskView.CurrentWorker);

                    UserInfoMessageTextBlock.Text = message;
                    return;
                }

                TimeSpan timeSpan = timeTableView.EndDate.Subtract(DateTime.Now.Add(CurrentTaskView.PlannedTime));

                if (timeSpan.Ticks < 0)
                {
                    string message =
                        String.Format("Время на выполнение задачи исполнителя выходит за рамки его смены на {0} минут.", Math.Floor(Math.Abs(timeSpan.TotalMinutes)));

                    UserInfoMessageTextBlock.Text = message;
                }
            }
        }

        private void UIElement_OnLostFocus(object sender, RoutedEventArgs e)
        {
            CheckSelectedUserTT();
        }
    }
}
