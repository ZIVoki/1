﻿using System.Windows;
using Tasker.Manager.ServiceReference1;
using Tasker.Tools.Extensions;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Manager.Controls.Task
{
    /// <summary>
    /// Interaction logic for NewTaskWindow.xaml
    /// </summary>
    public partial class ViewTaskWindow : WindowExt
    {
        public TaskView CurrentTaskView
        {
            get { return (TaskView) DataContext; }
            set
            {
                DataContext = value; 
                UpdateView();
            }
        }

        public ViewTaskWindow()
        {
            InitializeComponent();
            this.Activate();
        }

        private void UpdateView()
        {
            /*if (CurrentTaskView.Status != ConstantsStatuses.Closed)
            {
                if (CurrentTaskView.CurrentWorker != null)
                {
                    thisNewTaskControl.currentWorkerTextBox.Text = CurrentTaskView.CurrentWorker.Title;
                }
            }
            else
            {
                //todo history
                var statistics = ServiceManager.Inst.ServiceChannel.GetTaskStatistic(CurrentTaskView.Id);
                var count = statistics.Count();
                var lastRecordWithWorker = findLastWorkerInHistory(statistics, count - 2);

                if (lastRecordWithWorker != null)
                {
                    thisNewTaskControl.currentWorkerTextBox.Text = string.Format("{0} {1}", lastRecordWithWorker.FirstName,
                                                                             lastRecordWithWorker.LastName);
                }
            }*/
        }

        private UserDTO findLastWorkerInHistory(TaskStatisticRecordDTO[] statistics, int id)
        {
            var recordWithWorker = statistics[id];
            if (recordWithWorker.Worker == null)
            {
                if(id != 0)
                {
                    return findLastWorkerInHistory(statistics, id - 1);
                }
                else
                {
                    return null;
                }
                
            }
            else
            {
                return recordWithWorker.Worker;
            }
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
