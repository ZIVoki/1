﻿using System.Windows.Controls;

namespace Tasker.Manager.Controls.Task
{
    /// <summary>
    /// Interaction logic for TaskStatisticControl.xaml
    /// </summary>
    public partial class TaskStatisticControl : UserControl
    {
        public TaskStatisticControl()
        {
            InitializeComponent();
        }
    }
}
