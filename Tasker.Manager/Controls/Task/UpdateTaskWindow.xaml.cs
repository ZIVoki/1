﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using Tasker.Tools.Extensions;
using Tasker.Manager.Managers;
using Tasker.Tools.Managers;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Manager.Controls.Task
{
    /// <summary>
    /// Interaction logic for UpdateTaskWindow.xaml
    /// </summary>
    public partial class UpdateTaskWindow : WindowExt
    {
        private readonly int _previousWorker;
        private bool _cancelEdit = true;

        private TaskView CurrentTaskView
        {
            get { return (TaskView)DataContext; }
        }

        public UpdateTaskWindow(TaskView context)
        {
            InitializeComponent();
            DataContext = context;

            if (context.CurrentWorker != null)
                _previousWorker = context.CurrentWorker.Id;

            ServiceManager.Inst.ServiceChannel.CheckInTask(context.BuildDTO());
            CurrentTaskView.WorkState = Tools.Constants.TaskWorkState.CheckedOut;
            newTaskControl.OnClosing += taskControlEndWork;
            this.Activate();
        }

        private void taskControlEndWork()
        {
            UpdateTask();
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            _cancelEdit = true;
            this.Close();
        }

        private void updateTaskButton_Click(object sender, RoutedEventArgs e)
        {
            SaveData();
        }

        protected override void SaveData()
        {
            if (!CheckTaskData()) return;

            UpdateTask();
            base.SaveData();
        }

        private bool CheckTaskData()
        {
            if (CurrentTaskView.TaskType == null)
            {
                MessageBox.Show("Необходимо определить тип задачи", "Ошибка сохранения задачи", MessageBoxButton.OK,
                                MessageBoxImage.Asterisk);
                return false;
            }
            if (CurrentTaskView.Customer == null)
            {
                MessageBox.Show("Необходимо определить заказчика", "Ошибка сохранения задачи", MessageBoxButton.OK,
                                MessageBoxImage.Asterisk);
                return false;
            }
/*
            if (currentTaskView.CurrentWorker != null)
            {
                DateTime now = DateTime.Now;
                TimeTableView timeTableView = Repository.TimeTables.FirstOrDefault(
                                              tt =>
                                              tt.Status == ConstantsStatuses.Enabled &&
                                              tt.User == currentTaskView.CurrentWorker &&
                                              tt.StartDate <= now &&
                                              tt.EndDate >= now);
                if (timeTableView == null)
                {
                    string message =
                        String.Format("Смена исполнителя {0} подошла к концу, вы уверены, что хотите назначить задачу ему?",
                                      currentTaskView.CurrentWorker);

                    MessageBoxResult messageBoxResult = MessageBox.Show(message, "Исполнитель завершил смену", MessageBoxButton.YesNo,
                                                                        MessageBoxImage.Asterisk, MessageBoxResult.No);

                    if (messageBoxResult == MessageBoxResult.No) return false;
                    return true;
                }

                TimeSpan timeSpan = timeTableView.EndDate.Subtract(DateTime.Now.Add(currentTaskView.PlannedTime));

                if (timeSpan.Ticks < 0)
                {
                    string message =
                        String.Format(
                            "Время на выполнение задачи исполнителя выходит за рамки его смены на {0} минут. Хотели бы вы переназначить исполнителя?",
                            Math.Floor(Math.Abs(timeSpan.TotalMinutes)));
                    MessageBoxResult messageBoxResult = MessageBox.Show(message, "Превышен лимит исполнения в рамках смены", MessageBoxButton.YesNo,
                                                    MessageBoxImage.Asterisk, MessageBoxResult.Yes);

                    if (messageBoxResult == MessageBoxResult.Yes) return false;
                    return true;
                }
            }
*/
            return true;
        }

        private void UpdateTask()
        {
            if (CurrentTaskView.CurrentWorker != null)
            {
                if (_previousWorker != CurrentTaskView.CurrentWorker.Id)
                    CurrentTaskView.Status = ConstantsStatuses.Planing;
            }
            else
            {
                if (CurrentTaskView.Status == ConstantsStatuses.Planing)
                    CurrentTaskView.Status = ConstantsStatuses.Enabled;
            }

            TaskDTO taskDto = CurrentTaskView.BuildDTO();
            ServiceManager.Inst.ServiceChannel.UpdateTask(taskDto);
//            TasksLoadManager.UpdateTaskList(CurrentTaskView.BuildDTO());

            if (CurrentTaskView.Files != null)
            {
                foreach (var file in CurrentTaskView.Files.Where(f => f.Id == 0))
                    file.Id = ServiceManager.Inst.ServiceChannel.AddFileToTask(file.BuildDTO());
            }
            _cancelEdit = false;

            this.Close();
        }

        protected override void OnClosed(EventArgs e)
        {
            ServiceManager.Inst.ServiceChannel.CheckOutTask(CurrentTaskView.BuildDTO());
            CurrentTaskView.WorkState = Tools.Constants.TaskWorkState.Available;

/*            UserView userView = CurrentTaskView.CurrentWorker;

            if (userView != null)
            {
                //reload active task from server
                
                IEnumerable<int> tasksIds =
                    Repository.Tasks.Where(
                        t =>
                        (t.Status == ConstantsStatuses.InProcess || t.Status == ConstantsStatuses.Disabled) &&
                        t.CurrentWorker != null &&
                        t.CurrentWorker.Id != null &&
                        t.CurrentWorker.Id == userView.Id).Select(t => t.Id);

                foreach (int taskViewId in tasksIds)
                {
                    var tsk = ServiceManager.Inst.ServiceChannel.GetTask(taskViewId);
                    TaskManager.UpdateTask(tsk, Repository.Customers, Repository.TaskTypes,
                                           Repository.Users, Repository.Channels, null,
                                           Repository.Tasks,
                                           Repository.HighlightSettings.GetTaskTypeHighlightColor(tsk.TaskType.Id),
                                           Repository.HighlightSettings.GetCustomerHighlightColor(tsk.Customer.Id));
                }
            }*/

            base.OnClosed(e);
        }
    }

}
