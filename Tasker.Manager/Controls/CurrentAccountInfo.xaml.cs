﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Tasker.Tools.Controls;

namespace Tasker.Manager.Controls
{
    /// <summary>
    /// Interaction logic for CurrentAccountInfo.xaml
    /// </summary>
    public partial class CurrentAccountInfo : UserControl
    {
        public CurrentAccountInfo()
        {
            InitializeComponent();
            DataContext = Repository.LoginUser;
        }

        public event LogoffEventHandler LogOffEvent;

        public void InvokeLogOffEvent()
        {
            LogoffEventHandler handler = LogOffEvent;
            if (handler != null) handler(this);
        }

        private void LogoffeventHandler(object sender)
        {
            InvokeLogOffEvent();
        }
    }
}
