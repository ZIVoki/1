﻿using Tasker.Tools.Extensions;

namespace Tasker.Manager.Controls.Users
{
    /// <summary>
    /// Interaction logic for UserTaskTypeListWindow.xaml
    /// </summary>
    public partial class UserTaskTypeListWindow : WindowExt
    {
        public UserTaskTypeListWindow()
        {
            InitializeComponent();
            this.Activate();
        }
    }
}
