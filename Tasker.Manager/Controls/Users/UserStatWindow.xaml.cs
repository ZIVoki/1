﻿using System.Windows;
using System.Windows.Data;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Manager.Controls.Users
{
    /// <summary>
    /// Interaction logic for UserStatWindow.xaml
    /// </summary>
    public partial class UserStatWindow : Window
    {
        public UserStatWindow()
        {
            InitializeComponent();
        }

        private void UserCollectionViewSource_OnFilter(object sender, FilterEventArgs e)
        {
            UserView userView = (UserView)e.Item;
            if (userView == null) return;

            if (userView.Role == ConstantsRoles.Worker && userView.Status != ConstantsStatuses.Deleted)
                e.Accepted = true;
            else
                e.Accepted = false;

        }
    }
}
