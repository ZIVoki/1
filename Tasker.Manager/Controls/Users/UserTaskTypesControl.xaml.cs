﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Tasker.Manager.Converters;
using Tasker.Manager.Managers;
using Tasker.Tools.Managers;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Manager.Controls.Users
{
    /// <summary>
    /// Interaction logic for UserTaskTypesControl.xaml
    /// </summary>
    public partial class UserTaskTypesControl : UserControl
    {
        public UserTaskTypesControl()
        {
            InitializeComponent();
        }

        private void HeaderCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            userTaskTypesList.SelectAll();
        }

        private void HeaderCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            userTaskTypesList.UnselectAll();
        }



        private void TaskTypesCollectionViewSource_OnFilter(object sender, FilterEventArgs e)
        {
            var typeView = (TaskTypeView)e.Item;
            e.Accepted = true;
            if (typeView.Status == ConstantsStatuses.Deleted)
            {
                e.Accepted = false;
            }

            var user = DataContext as UserView;
            if(user != null)
            {
                var userTypeViewExist = user.UserTaskTypes.FirstOrDefault(a => a.TaskType.Id == typeView.Id);
                if (userTypeViewExist != null)
                {
                    e.Accepted = false;
                }
            }
            
        }

        private void addNewUserTaskTypeButton_Click(object sender, RoutedEventArgs e)
        {
            var dataContext = (UserView) DataContext;
            var userView = Repository.Users.FirstOrDefault(u => u.Id == dataContext.Id);
            if (userView == null) return;

            if (taskCopletionTimeDameer.Value == null) return;
            var userTaskTypeView = new UserTaskTypeView(userView)
                                    {
                                        CompletionTime = taskCopletionTimeDameer.Value.Value.TimeOfDay,
                                        TaskType = (TaskTypeView) taskTypesComboBox.SelectedItem
                                    };

            
            var id = ServiceManager.Inst.ServiceChannel.AddUserTaskType(userTaskTypeView.BuildDTO());
            userTaskTypeView.Id = id;
            
                userView.UserTaskTypes.Add(userTaskTypeView);
            

            CollectionViewSource collectionView = Resources["taskTypesCollectionViewSource"] as CollectionViewSource;
            collectionView.View.Refresh();
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            var userTaskType = (UserTaskTypeView)((Button)sender).DataContext;
            var userView = (UserView)DataContext;

            var converter = new TimeSpanToDateTimeConverter();
            var dateTime = converter.Convert(userTaskType.CompletionTime, null, null, null);
            EnterTimeWindow enterTimeWindow = new EnterTimeWindow
                          {
                              Title = string.Format("{0} {1}", userView.Title, userTaskType.Title),
                              taskCopletionTimeDameer = {Value = (DateTime) dateTime}
                          };
            enterTimeWindow.Show();

            if (enterTimeWindow.EnteredTime.HasValue && enterTimeWindow.EnteredTime.Value.TimeOfDay != userTaskType.CompletionTime)
            {
                userTaskType.CompletionTime = enterTimeWindow.EnteredTime.Value.TimeOfDay;
                var dto = userTaskType.BuildDTO();
                ServiceManager.Inst.ServiceChannel.UpdateUserTaskType(dto);
            }
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {

            var userTaskType = (UserTaskTypeView)((Button)sender).DataContext;
            var dataContext = (UserView)DataContext;
            var userView = Repository.Users.FirstOrDefault(u => u.Id == dataContext.Id);
            if (userView == null) return;

            var dialogResult =
                MessageBox.Show(string.Format("Вы уверены что хотите удалить тип задачи '{0}' для пользователя '{1}'?", userTaskType.TaskType.Title, userTaskType.User.Title), "Удаление", MessageBoxButton.YesNo, MessageBoxImage.Question);
            
            if(dialogResult == MessageBoxResult.No)
            {
                return;
            }
            
            var itemForRemove = userView.UserTaskTypes.FirstOrDefault(a => a.Id == userTaskType.Id);
            
            itemForRemove.Status = ConstantsStatuses.Deleted;
            ServiceManager.Inst.ServiceChannel.UpdateUserTaskType(itemForRemove.BuildDTO());
            userView.UserTaskTypes.Remove(itemForRemove);

            var collectionView = Resources["taskTypesCollectionViewSource"] as CollectionViewSource;
            collectionView.View.Refresh();
        }

        private void UserControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var collectionView = Resources["taskTypesCollectionViewSource"] as CollectionViewSource;
            collectionView.View.Refresh();
        }
    }
}
