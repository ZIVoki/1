﻿using System;
using System.Windows;

namespace Tasker.Manager.Controls.Users
{
    /// <summary>
    /// Interaction logic for EnterTimeWindow.xaml
    /// </summary>
    public partial class EnterTimeWindow : Window
    {
        public DateTime? EnteredTime;
        public EnterTimeWindow()
        {
            InitializeComponent();
            taskCopletionTimeDameer.Focus();
            this.Activate();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            EnteredTime = taskCopletionTimeDameer.Value;
            this.Close();
        }
    }
}
