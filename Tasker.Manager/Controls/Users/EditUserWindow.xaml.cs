﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Security.Cryptography;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using Tasker.Manager.Managers;
using Tasker.Tools.Extensions;
using Tasker.Tools.Managers;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;


namespace Tasker.Manager.Controls.Users
{
    /// <summary>
    /// Interaction logic for addUserWindow.xaml
    /// </summary>
    public partial class EditUserWindow : WindowExt
    {
        public UserView CurrentUserView
        {
            get { return (UserView) DataContext; }
            set { DataContext = value; }
        }

        public EditUserWindow()
        {
            if (DataContext == null)
                DataContext = CreateNewUser();
            InitializeComponent();

            FirstNameTextBox.Focus();
            FirstNameTextBox.SelectAll();
            this.Activate();
        }

        private static UserView CreateNewUser()
        {
            UserView userView = new UserView
                                    {
                                        FirstName = "Имя",
                                        LastName = "Фамилия",
                                        Status = ConstantsStatuses.Enabled,
                                        Role = ConstantsRoles.Worker
                                    };
            return userView;
        }

        private void UpdateUserAccessCredentials_OnClick(object sender, RoutedEventArgs e)
        {
            if (CurrentUserView == null) return;

            if (string.IsNullOrEmpty(LoginTextBox.Text) || string.IsNullOrEmpty(UserPasswordTextBox.Text))
            {
                MessageBox.Show("Логи и пароль не могут быть пустыми.", "Ошибка", MessageBoxButton.OK,
                                MessageBoxImage.Error);
                return;
            }

            var hashedPassword = GetMd5HashFromString(UserPasswordTextBox.Text);
            ServiceManager.Inst.ServiceChannel.SetNewUserCredentials(CurrentUserView.BuildDTO(), LoginTextBox.Text,
                                                              hashedPassword);

            MessageBox.Show("Права доступа обновлены.", "Права доступа.", MessageBoxButton.OK,
                            MessageBoxImage.Information);
        }

        // md5 hashig
        private static string GetMd5HashFromString(string forCrypt)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            var encoder = new UTF8Encoding();
            var hashedBytes = md5.ComputeHash(encoder.GetBytes(forCrypt));

            var sb = new StringBuilder();
            for (var i = 0; i < hashedBytes.Length; i++)
            {
                sb.Append(hashedBytes[i].ToString("x2"));
            }
            return sb.ToString();
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveUser();
        }

        protected override void SaveData()
        {
            SaveUser();
            base.SaveData();
        }

        private void SaveUser()
        {
            if (CurrentUserView == null) return;
            // if new user
            if (CurrentUserView.Id == 0)
            {
                UserDTO buildDto = CurrentUserView.BuildDTO();
                int newUserID = ServiceManager.Inst.ServiceChannel.AddUser(buildDto);
                CurrentUserView.Id = newUserID;
                CurrentUserView.CurrentState = ConstantsNetworkStates.Offline;
                Repository.Users.Add(CurrentUserView);
                CredentialsExpander.IsExpanded = true;
            }
                // if exist user
            else
            {
                UserDTO buildDto = CurrentUserView.BuildDTO();
                ServiceManager.Inst.ServiceChannel.UpdateUser(buildDto);
                this.Close();
            }
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        protected override void OnClosed(System.EventArgs e)
        {
            // if not new - update before close
            if (CurrentUserView.Id != 0)
            {
                UserDTO userDto = ServiceManager.Inst.ServiceChannel.GetWorker(CurrentUserView.Id);
                UserManager.UserUpdate(userDto, Repository.Users);
            }

            base.OnClosed(e);
        }

        private void textBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (sender is TextBox)
            {
                ((TextBox)sender).SelectAll();
            }
        }

        private void credentialsExpander_Expanded(object sender, RoutedEventArgs e)
        {
            MinHeight = 403;
            Height = 403;
        }

        private void credentialsExpander_Collapsed(object sender, RoutedEventArgs e)
        {
            MinHeight = 315;
            Height = 315;
        }
    }
}
