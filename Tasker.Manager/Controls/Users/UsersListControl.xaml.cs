﻿using System;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using Tasker.Manager.Controls.Task;
using Tasker.Manager.Managers;
using Tasker.Manager.Managers.Logger;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Manager.Controls.Users
{
    /// <summary>
    /// Interaction logic for UsersListControl.xaml
    /// </summary>
    public partial class UsersListControl : UserControl
    {
        readonly SortMachine _currentSortMachine = new SortMachine();
        private bool _showDeletedUsers = false;

        public event SelectionChangedDelegate SelectionChangedHandler; 

        public UsersListControl()
        {
            InitializeComponent();
            Repository.Users.CollectionChanged += Users_CollectionChanged;
        }

        void Users_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add || e.Action == NotifyCollectionChangedAction.Remove)
            {
                AutoResizeColumns();
            }
        }

        public void DeleteSelectedItems()
        {
            if (UsersListView.SelectedItem == null) return;

            UserView userView = (UserView)UsersListView.SelectedItem;
            if (userView.Status == ConstantsStatuses.Deleted) return;
            if (userView.Role == ConstantsRoles.Administrator)
            {
                MessageBox.Show("Удаление администратора запрещено.", "Удаление пользователя.", MessageBoxButton.OK,
                                MessageBoxImage.Asterisk);
                return;
            }

            string message = string.Format("Удалить пользователя {0}", userView.Title);
            MessageBoxResult messageBoxResult = MessageBox.Show(message,
                                                                "Удаление пользователя",
                                                                MessageBoxButton.YesNo,
                                                                MessageBoxImage.Question);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                userView.Status = ConstantsStatuses.Deleted;
                UserDTO userDto = userView.BuildDTO();

                ServiceManager.Inst.ServiceChannel.UpdateUser(userDto);
                Log.Write(LogLevel.Debug, string.Format("Пользователь {0} удален.", userView.Title));

                UserView firstOrDefault = Repository.Users.FirstOrDefault(u => u.Id == userView.Id);
                if (firstOrDefault == null) return;
                firstOrDefault.Status = ConstantsStatuses.Deleted;

                CollectionViewSource collectionViewSource = (CollectionViewSource)Resources["UsersCollectionViewSource"];
                collectionViewSource.View.Refresh();
            }
        }

        public bool ShowDeletedUsers
        {
            get { return _showDeletedUsers; }
            set
            {
                _showDeletedUsers = value;
                RefreshView();
                UsersListView.UnselectAll();
            }
        }

        void RefreshView()
        {
            var userCollectionViewSource = (CollectionViewSource)Resources["UsersCollectionViewSource"];
            if (userCollectionViewSource == null) return;
            try
            {
                userCollectionViewSource.View.Refresh();
            }
            catch (InvalidOperationException e)
            {
            }
            AutoResizeColumns();
        }

        private void UsersCollectionViewSource_OnFilter(object sender, FilterEventArgs e)
        {
            if (Repository.LoginUser == null)
            {
                e.Accepted = false;
                return;
            }

            UserView userView = (UserView)e.Item;
            e.Accepted = true;
            if ((!ShowDeletedUsers && userView.Status == ConstantsStatuses.Deleted) || userView.FirstName == "admin" || userView.FirstName == "worker")
            {
                e.Accepted = false;
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            var headerClicked =
                  e.OriginalSource as GridViewColumnHeader;
            if (sender != null)
                _currentSortMachine.GridViewColumnHeaderClickedHandler(((ListView)sender).ItemsSource, headerClicked, this);
            AutoResizeColumns();
        }

        private void TaskLisiLink_Click(object sender, RoutedEventArgs e)
        {
            var user = (UserView)((Hyperlink)sender).DataContext;
            UserTaskTypeListWindow userTaskTypeListWindow = new UserTaskTypeListWindow
                                             {
                                                 DataContext = user,
                                                 Title = string.Format("Типы задач пользователя {0}", user.Title)
                                             };
            userTaskTypeListWindow.Show();
        }

        private void ListViewItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListViewItem listViewItem = ((ListViewItem)sender);
            if (listViewItem == null) return;
            if (listViewItem.DataContext == null) return;
            UserView user = (UserView) listViewItem.DataContext;
            if (user == null) return;

            // check if user admin
            if (Repository.LoginUser.Role == ConstantsRoles.Administrator && user.Status == ConstantsStatuses.Enabled) 
                ShowEditUserWindow(sender);
        }

        private static void ShowEditUserWindow(object sender)
        {
            if (sender is ListViewItem)
            {
                ListViewItem listViewItem = ((ListViewItem)sender);
                EditUserWindow editUserWindow = new EditUserWindow { DataContext = listViewItem.DataContext, };
                editUserWindow.ShowDialog();
            }
        }

        public void AutoResizeColumns()
        {
            GridView gv = UsersListView.View as GridView;

            if (gv != null)
            {
                foreach (GridViewColumn gvc in gv.Columns)
                {
                    // Set width to highest possible value
                    gvc.Width = double.MaxValue;

                    // Set to NaN to get the "real" actual width
                    gvc.Width = double.NaN;
                }
            }
        }

        private void GridViewColumnHeaderClickedHandler(object sender, RoutedEventArgs e)
        {
            var headerClicked = e.OriginalSource as GridViewColumnHeader;
            // maybe datacontext
            if (sender != null)
                _currentSortMachine.GridViewColumnHeaderClickedHandler(((ListView)sender).ItemsSource, headerClicked, this);
            AutoResizeColumns();
        }

        private void UsersListView_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int count = UsersListView.SelectedItems.Count;
            OnSelectionChangedHandler(new SelectionChangedArgs(count, UsersListView.Items.Count));
        }

        protected virtual void OnSelectionChangedHandler(SelectionChangedArgs selectionChangedArgs)
        {
            SelectionChangedDelegate handler = SelectionChangedHandler;
            if (handler != null) handler(this, selectionChangedArgs);
        }
    }

}
