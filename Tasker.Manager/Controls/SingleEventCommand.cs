﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Input;

namespace Tasker.Manager.Controls
{
    #region SingleEventCommand Class
    /// 
    /// This class allows a single command to event mappings.  
    /// It is used to wire up View events to a
    /// ViewModel ICommand implementation.  
    /// 
    /// 
    /// <![CDATA[
    /// 
    /// <Grid Background="WhiteSmoke"
    ///    Cinch:SingleEventCommand.RoutedEventName="MouseDown"
    ///      Cinch:SingleEventCommand.TheCommandToRun=
    ///       "{Binding Path=ShowWindowCommand}" />
    /// 
    /// ]]>
    /// 
    public static class SingleEventCommand
    {
        #region TheCommandToRun

        /// 
        /// TheCommandToRun : The actual ICommand to run
        /// 
        public static readonly DependencyProperty TheCommandToRunProperty =
            DependencyProperty.RegisterAttached("TheCommandToRun",
                typeof(ICommand),
                typeof(SingleEventCommand),
                new FrameworkPropertyMetadata((ICommand)null));

        /// 
        /// Gets the TheCommandToRun property.  
        /// 
        public static ICommand GetTheCommandToRun(DependencyObject d)
        {
            return (ICommand)d.GetValue(TheCommandToRunProperty);
        }

        /// 
        /// Sets the TheCommandToRun property.  
        /// 
        public static void SetTheCommandToRun(DependencyObject d, ICommand value)
        {
            d.SetValue(TheCommandToRunProperty, value);
        }
        #endregion

        #region RoutedEventName

        /// 
        /// RoutedEventName : The event that should actually execute the
        /// ICommand
        /// 
        public static readonly DependencyProperty RoutedEventNameProperty =
            DependencyProperty.RegisterAttached("RoutedEventName", typeof(String),
            typeof(SingleEventCommand),
                new FrameworkPropertyMetadata((String)String.Empty,
                    new PropertyChangedCallback(OnRoutedEventNameChanged)));

        /// 
        /// Gets the RoutedEventName property.  
        /// 
        public static String GetRoutedEventName(DependencyObject d)
        {
            return (String)d.GetValue(RoutedEventNameProperty);
        }

        /// 
        /// Sets the RoutedEventName property.  
        /// 
        public static void SetRoutedEventName(DependencyObject d, String value)
        {
            d.SetValue(RoutedEventNameProperty, value);
        }

        /// 
        /// Hooks up a Dynamically created EventHandler (by using the 
        /// EventHooker class) that when
        /// run will run the associated ICommand
        /// 
        private static void OnRoutedEventNameChanged(DependencyObject d,
            DependencyPropertyChangedEventArgs e)
        {
            String routedEvent = (String)e.NewValue;

            if (d == null || String.IsNullOrEmpty(routedEvent))
                return;


            //If the RoutedEvent string is not null, create a new
            //dynamically created EventHandler that when run will execute
            //the actual bound ICommand instance (usually in the ViewModel)
            EventHooker eventHooker = new EventHooker();
            eventHooker.ObjectWithAttachedCommand = d;

            EventInfo eventInfo = d.GetType().GetEvent(routedEvent,
                BindingFlags.Public | BindingFlags.Instance);

            //Hook up Dynamically created event handler
            if (eventInfo != null)
            {
                eventInfo.RemoveEventHandler(d,
                    eventHooker.GetNewEventHandlerToRunCommand(eventInfo));

                eventInfo.AddEventHandler(d,
                    eventHooker.GetNewEventHandlerToRunCommand(eventInfo));
            }

        }
        #endregion
    }
    #endregion

    #region EventHooker Class
    /// 
    /// Contains the event that is hooked into the source RoutedEvent
    /// that was specified to run the ICommand
    /// 
    sealed class EventHooker
    {
        #region Public Methods/Properties
        /// 
        /// The DependencyObject, that holds a binding to the actual
        /// ICommand to execute
        /// 
        public DependencyObject ObjectWithAttachedCommand { get; set; }

        /// 
        /// Creates a Dynamic EventHandler that will be run the ICommand
        /// when the user specified RoutedEvent fires
        /// 
        /// <param name="eventInfo" />The specified RoutedEvent EventInfo
        /// An Delegate that points to a new EventHandler
        /// that will be run the ICommand
        public Delegate GetNewEventHandlerToRunCommand(EventInfo eventInfo)
        {
            Delegate del = null;

            if (eventInfo == null)
                throw new ArgumentNullException("eventInfo");

            if (eventInfo.EventHandlerType == null)
                throw new ArgumentException("EventHandlerType is null");

            if (del == null)
                del = Delegate.CreateDelegate(eventInfo.EventHandlerType, this,
                      GetType().GetMethod("OnEventRaised",
                        BindingFlags.NonPublic |
                        BindingFlags.Instance));

            return del;
        }
        #endregion

        #region Private Methods

        /// 
        /// Runs the ICommand when the requested RoutedEvent fires
        /// 
        private void OnEventRaised(object sender, EventArgs e)
        {
            ICommand command = (ICommand)(sender as DependencyObject).
                GetValue(SingleEventCommand.TheCommandToRunProperty);

            if (command != null)
            {
                command.Execute(null);
            }
        }
        #endregion
    }
    #endregion
}
