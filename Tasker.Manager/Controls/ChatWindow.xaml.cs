﻿using System;
using System.Windows;
using Tasker.Manager.Managers;
using Tasker.Manager.Managers.Logger;
using Tasker.Tools.CustomEventArgs;
using Tasker.Tools.Extensions;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;

namespace Tasker.Manager.Controls
{
    /// <summary>
    /// Interaction logic for ChatWindow.xaml
    /// </summary>
    public partial class ChatWindow : WindowExt
    {
        public TaskView CurrentTask
        {
            get
            {
                return (TaskView) DataContext;
            }
            set
            {
                DataContext = value;
            }
        }

        public ChatWindow()
        {
            this.DataContextChanged += ChatWindow_DataContextChanged;
            InitializeComponent();
            this.Activate();
        }

        void ChatWindow_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (CurrentTask != null)
            {
                messageControl.IsActiveChat = CurrentTask.Status != ConstantsStatuses.Closed;
                messageControl.IsActiveChat = CurrentTask.CurrentWorker != null;
            }
        }

        private void SendNewMessage_MessageControl(object sender, ContentEventArgs contentEventArgs)
        {
            var task = DataContext as TaskView;
            if(task != null && task.Status == ConstantsStatuses.Closed)
            {
                MessageBox.Show("Задача закрыта. Отправка сообщения невозможна.");
                this.Close();
            }

            var messageText = (string) contentEventArgs.Arg;
            if (string.IsNullOrEmpty(messageText.Trim()))
            {
                return;
            }

            if (task == null)
            {
                string message = "Ошибка, отсутвует задача.";
                MessageBox.Show(message, "Отсутствует задача.", MessageBoxButton.OK, MessageBoxImage.Error);
                Log.Write(LogLevel.Error, message);
                return;
            }

            if (task.CurrentWorker == null)
            {
                MessageBox.Show("Ошибка, отсутствует пользователь.", "Отсутствует пользователь.", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            
            MessageView messageView = new MessageView(task)
                                        {
                                            Text = messageText,
                                            UserDestination = task.CurrentWorker,
                                            SendDate = DateTime.Now,
                                            Status = ConstantsStatuses.Enabled
                                        };
            var dto = messageView.BuildDTO();
            
            var id = ServiceManager.Inst.ServiceChannel.SendMessage(dto);
            if (id == 0)
            {
                MessageBox.Show("Не удалось отправить сообщение.");
            }
            else
            {
                messageView.Id = id;
                messageView.IsMy = true;
                messageView.UserFrom = Repository.LoginUser;
                messageView.IsReaded = true;
                task.Messages.Messages.Add(messageView);
            }
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            var task = (TaskView) DataContext;
            task.Messages.SetAllAsRead();
        }
    }
}
