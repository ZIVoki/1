﻿using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;

namespace Tasker.Manager.Controls
{
    /// <summary>
    /// Interaction logic for TextStackPanel.xaml
    /// </summary>
    public partial class TextStackPanel : UserControl
    {
        public TextStackPanel()
        {
            InitializeComponent();
        }

        public static readonly DependencyProperty TextLabelsProperty =
           DependencyProperty.Register("TextLabels", typeof(ObservableCollection<string>), typeof(TextStackPanel), new PropertyMetadata(new ObservableCollection<string>()));

        public ObservableCollection<string> TextLabels
        {
            get
            {
                return (ObservableCollection<string>)GetValue(TextLabelsProperty);
            }
            set
            {
                SetValue(TextLabelsProperty, value);
            }
        }

    }
}
