﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Tasker.Manager.BackgroundWorkers;
using Tasker.Manager.Managers;
using Tasker.Manager.Objects;
using Tasker.Tools.ViewModel;

namespace Tasker.Manager.Controls.AttendanceLog
{
    public partial class AttendanceLogControl : UserControl, INotifyPropertyChanged
    {
        private readonly SortMachine _currentSortMachine = new SortMachine();
        private DateTime _currntDate = DateTime.Now;

        public AttendanceLogControl()
        {
            InitializeComponent();
            CollectionViewSource taskCollectionViewSource = (CollectionViewSource)Resources["AttendanceLogCollectionViewSource"];
            taskCollectionViewSource.SortDescriptions.Add(new SortDescription("EventDateTime", ListSortDirection.Ascending));
        }

        public DateTime CurrentDate
        {
            get
            {
                return _currntDate;
            }
            set
            {
                _currntDate = value;
                OnPropertyChanged("CurrentDate");
                BackgroundTask backgroundTask = new BackgroundTask(LoadAttendanceDayLogTimeTables, LoadDayLogTablesComplete);
                backgroundTask.Run(value);
            }
        }

        public void WhatchForward()
        {
            CurrentDate += TimeSpan.FromDays(1);
        }

        public void WhatchBack()
        {
            CurrentDate -= TimeSpan.FromDays(1);
        }

        private void BtnBackClick(object sender, RoutedEventArgs e)
        {
            WhatchBack();
        }

        private void BtnForwardClick(object sender, RoutedEventArgs e)
        {
            WhatchForward();
        }

        private static void LoadAttendanceDayLogTimeTables(object sender, DoWorkEventArgs e)
        {
            DateTime day = (DateTime)e.Argument;
            Repository.AttendanceLogLoadLoadManager.LoadAttendanceLogOnDay(day);
        }

        private void LoadDayLogTablesComplete(object sender, RunWorkerCompletedEventArgs e)
        {
            ((CollectionViewSource)this.Resources["AttendanceLogCollectionViewSource"]).View.Refresh();
            AutoResizeColumns();
        }

        private void CurrentDateAttendanceLog_OnFilter(object sender, FilterEventArgs e)
        {
            var eventDateTime = ((AttendanceLogItemView)e.Item).EventDateTime;

            if (eventDateTime.Date == CurrentDate.Date)
            {
                e.Accepted = true;
            }
            else
            {
                e.Accepted = false;
            }
        }

        private void GridViewColumnHeaderClickedHandler(object sender, RoutedEventArgs e)
        {
            GridViewColumnHeader headerClicked = e.OriginalSource as GridViewColumnHeader;
            // maybe datacontext
            if (sender != null)
                _currentSortMachine.GridViewColumnHeaderClickedHandler(AttendanceListView.ItemsSource, headerClicked, this);
            AutoResizeColumns();
        }

        private void AutoResizeColumns()
        {
            try
            {
                GridView gv = AttendanceListView.View as GridView;

                if (gv != null)
                {
                    foreach (GridViewColumn gvc in gv.Columns)
                    {
                        // Set width to highest possible value
                        gvc.Width = double.MaxValue;

                        // Set to NaN to get the "real" actual width
                        gvc.Width = double.NaN;
                    }
                }
            }
            catch (InvalidOperationException)
            { }
        }

        #region INotifyPropertyChanged Members

        public virtual event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string info)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(info));
            }
        }
        #endregion

    }
}
