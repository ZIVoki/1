﻿using System;
using System.Windows;
using System.Windows.Input;
using Tasker.Tools.Extensions;

namespace Tasker.Manager.Controls.AttendanceLog
{
    public partial class AttendanceLogWindow : WindowExt
    {
        private static AttendanceLogWindow _attendanceLogWindow = null;
        public static readonly RoutedCommand AttendanceShowDayLogCommand = new RoutedCommand("AttendanceShowDayLogCommand", typeof(object));

        static AttendanceLogWindow()
        {
            CommandManager.RegisterClassCommandBinding(typeof(object), new CommandBinding(AttendanceShowDayLogCommand, DoShowAttendanceDayLog));
        }

        public AttendanceLogWindow(DateTime day)
        {
            KeyUp += AttendanceLogWindowKeyUp;
            InitializeComponent();
            lstLog.CurrentDate = day;
        }

        public static void CloseCurrentWindow()
        {
            if (_attendanceLogWindow != null)
                _attendanceLogWindow.Close();
        }

        private void AttendanceLogWindowKeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyboardDevice.IsKeyDown(Key.LeftCtrl) || e.KeyboardDevice.IsKeyDown(Key.LeftCtrl))
            {
                if (e.Key == Key.Right)
                    lstLog.WhatchForward();
                if (e.Key == Key.Left)
                    lstLog.WhatchBack();
            }
        }

        private static void DoShowAttendanceDayLog(object sender, ExecutedRoutedEventArgs e)
        {
            if (_attendanceLogWindow == null)
            {
                _attendanceLogWindow = new AttendanceLogWindow((DateTime)e.Parameter);
                _attendanceLogWindow.Show();
            }
            else
            {
                _attendanceLogWindow.Activate();
            }
        }

        private void CloseButtonClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            _attendanceLogWindow = null;
        }
    }
}
