﻿using System.Windows;
using System.Windows.Controls;
using Tasker.Manager.Model;
using Tasker.Manager.Objects;

namespace Tasker.Manager.Controls.Board
{
    /// <summary>
    /// Interaction logic for BoardControl.xaml
    /// </summary>
    public partial class BoardControl : UserControl
    {
        public BoardControl()
        {
            InitializeComponent();
        }

        public BoardModel CurrentBoard
        {
            get { return (BoardModel) DataContext; }
            set { DataContext = value; }
        }

        private void FilesTreeViewSelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (CurrentBoard != null)
                CurrentBoard.UpdateSelectedItem();
        }

        public void EditSelectedFile()
        {
            CurrentBoard.OpenSelectedFile();
        }
    }
}
