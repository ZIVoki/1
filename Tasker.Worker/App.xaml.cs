﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows;
using NLog;
using Tasker.Worker.Properties;
using Tasker.Worker.Windows;

namespace Tasker.Worker
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            while (true)
            {
                LoadSettings();
                LoginWindow newClientLoginWindow = new LoginWindow();
                bool? showDialog = newClientLoginWindow.ShowDialog();

                if (showDialog.HasValue && showDialog.Value)
                {
                    SaveSettings();
                    MainWindow mainWindow = new MainWindow();
                    MainWindow = mainWindow;
//                    try
//                    {
                        mainWindow.ShowDialog();
//                    }
//                    catch (Exception exception)
//                    {
//                        Debug.WriteLine(exception);
//                        Logger.Fatal("Apllication fail", exception);
//                        mainWindow.ForceClose();
//                    }
                    MainWindow = null;
                }
                else
                {
                    break;
                }
                SaveSettings();
            }

            this.Shutdown();
        }

        private static void SaveSettings()
        {
//            Repository.UserConfiguration.LastLogin = Settings.Default.LastLogin;
            Configuration.Config.Load().Save();
            Settings.Default.Save();
        }

        public static string CurrentVersion
        {
            get
            {
                System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
                Version version = assembly.GetName().Version;
                return version.ToString();
            }
        }

        private static void LoadSettings()
        {
            Repository.UserConfiguration = Configuration.Config.Load();

            Settings.Default.AutoLogOff = true;
            Settings.Default.LogOffTime = 15;
            Settings.Default.LastLogin = Repository.UserConfiguration.LastLogin;

            Settings.Default.Save();

#if DEBUG
            Settings.Default.NetTcpAddress = "localhost:8732";
            Settings.Default.RssUrl = "http://habrahabr.ru/rss/hubs//";
#endif
        }

        private void ApplicationStartup(object sender, StartupEventArgs e)
        {
            Logger.Info(String.Format("Current version: {0}", CurrentVersion));
        }

        public static string GetSettingsFilesPath()
        {
            string localAppData = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            string userFilePath = Path.Combine(localAppData, "[V.M.K.]_Team");
            if (!Directory.Exists(userFilePath)) Directory.CreateDirectory(userFilePath);
            return userFilePath;
        }
    }
}
