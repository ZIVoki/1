using System;
using System.Diagnostics;
using System.Windows.Input;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;
using Tasker.Tools.ViewModel.Base;
using Tasker.Worker.Managers;

namespace Tasker.Worker.Model
{
    public class WorkerFileView : FileView
    {
        public WorkerFileView(FileVersionDTO fileVersion, FolderView parent, bool lazyLoadChildren)
            : base(fileVersion, parent, lazyLoadChildren)
        { }

        private ICommand _viewFileCommand;

        public ICommand ViewFileCommand
        {
            get
            {
                if (_viewFileCommand == null)
                    _viewFileCommand = new RelayCommand(DoViewFile);
                return _viewFileCommand;
            }
        }

        private void DoViewFile()
        {
            try
            {
                Process.Start(FilePath);
                if (!ReadDate.HasValue)
                {
                    ServiceManager.Inst.ServiceChannel.SetBoardFileAsRead(Id);
                    ReadDate = DateTime.Now;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}