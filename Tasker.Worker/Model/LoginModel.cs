﻿using System.Security.Principal;
using Tasker.Tools.ViewModel;
using Tasker.Worker.Configuration;
using Tasker.Worker.Managers;
using Tasker.Worker.Properties;

namespace Tasker.Worker.Model
{
    internal class LoginModel : LoginViewModel<ServiceManager>
    {
        private readonly Config _userConfig = Config.Load();

        public LoginModel(): this(0)
        { }

        public LoginModel(int iniCounter)
        {
            if (iniCounter <= 1)
            {
                _login = IdentUser();
            }
            else
            {
                _login = Config.Load().LastLogin;
                IsDomainUser = Constants.IsDomainUser;
            }

            _isAutoLogin = _userConfig.IsAutoLogin;
            _password = Constants.Password;
            _url = Settings.Default.NetTcpAddress;
            _versionName = App.CurrentVersion;
            _service = ServiceManager.Inst;
        }

        protected override string IdentUser()
        {
            string login;
            WindowsIdentity currentIdentity = WindowsIdentity.GetCurrent();
            string name = "";
            if (currentIdentity != null)
                name = currentIdentity.Name;

            if (IsDoaminName(name))
            {
                login = name;
                IsDomainUser = true;
            }
            else
            {
                login = Config.Load().LastLogin;
                IsDomainUser = false;
            }

            return login;
        }

        protected override bool IsDoaminName(string name)
        {
            string domainName = Settings.Default.DomainName;
            return name.Length > domainName.Length && name.Substring(0, domainName.Length).Equals(domainName);
        }

        protected override void DoSaveSettings()
        {
            _userConfig.LastLogin = _login;
            _userConfig.IsAutoLogin = _isAutoLogin;
            _userConfig.Save();
            Constants.Password = _password;
            Constants.IsDomainUser = IsDomainUser;
            Settings.Default.NetTcpAddress = _url;
            Settings.Default.Save();
        }

        protected override bool IsUserExist()
        {
            return Repository.LoginUser != null;
        }
    }
}