using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using Microsoft.Win32;
using Tasker.Tools.CustomEventArgs;
using Tasker.Tools.Extensions;
using Tasker.Tools.Helpers;
using Tasker.Tools.Managers;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;
using Tasker.Tools.ViewModel.Base;
using Tasker.Worker.Configuration;
using Tasker.Worker.Managers;
using Tasker.Worker.Properties;

namespace Tasker.Worker.Model
{
    internal class MainWindowModel : ObservableObject, IDisposable
    {
        private readonly TimeSpan _maxLunch = TimeSpan.FromMinutes(30);
        private readonly Dispatcher _dispatcher = Application.Current.Dispatcher;
        private DispatcherTimer _timer = new DispatcherTimer();

        private TimeSpan _maxAutoLogoffTime = TimeSpan.FromMinutes(Settings.Default.LogOffTime);
        private BindableCollection<TaskModel> _tasksList = new BindableCollection<TaskModel>();
        private UserView _currentUser;
        private UniqBindableCollection<TimeTableView> _scheduleList;
        private CollectionViewSource _tasksCollectionView;
        private DateTime _lastTaskUpdate;
        private bool _isActive;
        private bool _isTopMost = false;

        private readonly BoardModel _board;
        private readonly LateControlModel _lateControlModel;
        private readonly SelfStatModel _selfStatModel;
        private readonly NotificationsModel _notificationsModel;
        private readonly LunchModel _lunchModel;

        private Visibility _showNews = Visibility.Hidden;
        private Visibility _showSchedule = Visibility.Hidden;
        private Visibility _showBoard = Visibility.Hidden;
        private Visibility _reconnectVisibility = Visibility.Hidden;

        private RelayCommand _scheduleCommand;
        private RelayCommand _refreshTaskCommand;
        private RelayCommand _showBoardCommand;
        private RelayCommand _showNewsCommand;

        public event EventHandler RequestClose;

        public void OnRequestClose(EventArgs e)
        {
            EventHandler handler = RequestClose;
            if (handler != null) handler(this, e);
        }

        public bool IsDesignTime
        {
            get
            {
                return (Application.Current == null) ||
                       (Application.Current.GetType() == typeof(Application));
            }
        }

        public MainWindowModel()
        {
            if (IsDesignTime) return;

            InitTimer();

            SetAllTasks();

            _lunchModel = new LunchModel(_maxLunch, this);
            _board = new BoardModel();
            _selfStatModel = new SelfStatModel();
            _lateControlModel = new LateControlModel();
            _notificationsModel = new NotificationsModel();
            CurrentUser = Repository.LoginUser;
            ScheduleList = Repository.TimeTables;

            SetLunchTimeTableState();

            // sign to events
            Repository.Tasks.CollectionChanged += TasksCollectionChanged;
            ServiceManager.Inst.Callback.LunchFormChangeState += WorkerCallbackLunchFormChangeState;
            ServiceManager.Inst.Callback.CurrentTaskState += WorkerCallbackCurrentTaskState;
            ServiceManager.Inst.Callback.NewMessageEvent += WorkerCallbackNewMessageEvent;
            ServiceManager.Inst.Callback.BoardFileUpdate += WorkerCallbackBoardFileUpdate;
            ServiceManager.Inst.ConectionReestablished += ServiceManager_ConectionReestablished;
            ServiceManager.Inst.WaitingForConnection += ServiceManager_WaitingForConnection;
            SystemEvents.SessionSwitch += SystemEvents_SessionSwitch;

//            NotifyUserIfHisLate(currentTimeTable);
        }

        #region Init

        private void SetAllTasks()
        {
            foreach (TaskView taskView in Repository.Tasks)
            {
                _tasksList.Add(new TaskModel {Task = taskView});
            }
        }

        private void SetLunchTimeTableState()
        {
            TimeTableView currentTimeTable = Repository.CurrentTimeTable;
            if (currentTimeTable != null)
            {
                if (currentTimeTable.FirstLunchStart != null)
                    LunchModel.LunchCount++;
                if (currentTimeTable.SecondLunchStart != null)
                    LunchModel.LunchCount++;
            }
        }

        private void InitTimer()
        {
            _timer.Tick += TimerOnTick;
            _timer.Interval = TimeSpan.FromSeconds(1);
            _timer.Start();
        }

        private void SystemEvents_SessionSwitch(object sender, SessionSwitchEventArgs e)
        {
            if (e.Reason == SessionSwitchReason.SessionLock || e.Reason == SessionSwitchReason.SessionLogoff)
            {
                OnRequestClose(e);
            }
        }

        private void ServiceManager_WaitingForConnection(object sender)
        {
            ReconnectVisibility = Visibility.Visible;
        }

        private void ServiceManager_ConectionReestablished(object sender)
        {
            ReconnectVisibility = Visibility.Hidden;
        }

        private void NotifyUserIfHisLate(TimeTableView currentTimeTable)
        {
            if (currentTimeTable != null &&
                Repository.UserConfiguration.LateLog.All(t => t.TimeTableId != currentTimeTable.Id))
            {
                // ���������� ���� ����, ����� �� ����������
                Repository.UserConfiguration.LateLog.Add(new EntersLogItem(currentTimeTable.Id, DateTime.Now));
                DateTime dateTime = DateTime.Now.Add(TimeSpan.FromMinutes(10));
                if (dateTime >= currentTimeTable.StartDate)
                {
                    TimeSpan timeSpan = DateTime.Now.Subtract(currentTimeTable.StartDate);
                    string message = string.Format("��� ��������� ��������� �� {0} �����.", (int) timeSpan.TotalMinutes);
                    _notificationsModel.Add(new NotificationView(message));
                }
                Config.Load().Save();
            }
        }

        private void WorkerCallbackBoardFileUpdate(object sender, FileVersioChangedEventArgs e)
        {
            if (e.FileVersion == null) return;

            _dispatcher.Invoke((Action) (() => NotifyAboutNewBoradFile(e.FileVersion)));
        }

        private void NotifyAboutNewBoradFile(FileVersionDTO fileVersion)
        {
            NotificationView notificationView = new NotificationView
            {
                Content =
                    string.Format("�������� {0} ��������� �� ������ {1}.",
                        Path.GetFileNameWithoutExtension(fileVersion.File.Title),
                        fileVersion.Version),
                DateTime = DateTime.Now,
                OnClickAction = () => Process.Start(fileVersion.File.FilePath)
            };
            _notificationsModel.Add(notificationView);
        }

        private void WorkerCallbackNewMessageEvent(object sender, ContentEventArgs e)
        {
            if (e.Arg == null) return;
            MessageView messageView = (MessageView) e.Arg;
            if (messageView == null) return;

            _dispatcher.Invoke((Action) (() => NotifyAboutNewMessage(messageView)));
        }

        private void NotifyAboutNewMessage(MessageView messageView)
        {
            TaskModel messageTask =
                _tasksList.FirstOrDefault(task => task.Task.Id == messageView.Task.Id && task.IsChatWindowIsActive);
            NotificationView notificationView =
                new NotificationView(string.Format("����� ��������� �� {0}", messageView.UserFrom.Title), DateTime.Now,
                    () =>
                    {
                        TaskModel taskModel =
                            _tasksList.FirstOrDefault(task => task.Task.Id == messageView.Task.Id);
                        if (taskModel != null)
                            taskModel.OpenMessagesCommand.Execute(null);
                    });

            if (messageTask == null)
                notificationView.IsRead = IsActive;
            else
            {
                notificationView.IsRead = true;
                messageTask.Task.Messages.SetAllAsRead();
            }

            _notificationsModel.Add(notificationView);
        }

        private void TasksCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (object newItem in e.NewItems)
                {
                    TaskView taskView = (TaskView) newItem;
                    if (taskView != null)
                    {
                        _tasksList.Add(new TaskModel(taskView));
                    }
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (object newItem in e.OldItems)
                {
                    TaskView taskView = (TaskView) newItem;
                    TaskModel taskModel = _tasksList.FirstOrDefault(t => t.Task.Id == taskView.Id);
                    if (taskModel != null)
                    {
                        _tasksList.Remove(taskModel);
                        taskModel.Dispose();
                    }
                }
            }
            UpdateTasksSorting();
        }

        private void WorkerCallbackCurrentTaskState(object sender, ContentsEventArgs e)
        {
            TaskView taskView = (TaskView) e.Args[1];
            if (taskView == null) return;

            switch ((Constants.TaskFormType) e.Args[0])
            {
                case Constants.TaskFormType.NewTask:
                    _notificationsModel.Add(new NotificationView(taskView));
                    break;
                case Constants.TaskFormType.FinishTaskWait:
                    break;
                case Constants.TaskFormType.WaitForTask:
                    break;
                case Constants.TaskFormType.Success:
                    _notificationsModel.Add(new NotificationView
                    {
                        Content = string.Format("������ {0} �������.", taskView.Title),
                        NotificationType = NotificationType.TaskUpdate
                    });
                    SelfStatModel.UpdateCurrentStat();
                    break;
                case Constants.TaskFormType.UnsetTask:
                    _notificationsModel.Add(new NotificationView
                    {
                        Content = string.Format("������ {0} �����.", taskView.Title),
                        NotificationType = NotificationType.TaskUpdate
                    });
                    break;
                case Constants.TaskFormType.BackgroundTaskSet:
                    _notificationsModel.Add(new NotificationView
                    {
                        Content = string.Format("����� ������� ������ {0}.", taskView.Title),
                        NotificationType = NotificationType.NewTask,
                    });
                    break;
                case Constants.TaskFormType.ReopenTask:
                    _notificationsModel.Add(new NotificationView
                    {
                        Content = string.Format("������ {0} �����������.", taskView.Title),
                        NotificationType = NotificationType.TaskUpdate
                    });
                    break;
                case Constants.TaskFormType.UpdateTask:
                    _notificationsModel.Add(new NotificationView
                    {
                        Content = string.Format("������ {0} ���������.", taskView.Title),
                        IsRead = IsActive,
                        NotificationType = NotificationType.TaskUpdate
                    });
                    break;
            }

            UpdateTasksSorting();
        }

        private void UpdateTasksSorting()
        {
            _dispatcher.Invoke((Action) (() => TasksCollectionView.Refresh()));
        }

        private void WorkerCallbackLunchFormChangeState(object sender, ContentEventArgs e)
        {
            if (!LunchModel.WaitingForRequest) return;
            LunchModel.WaitingForRequest = false;

            Constants.DinnerFormState dinnerFormState = (Constants.DinnerFormState) e.Arg;
            if (dinnerFormState == null) return;

            if (dinnerFormState == Constants.DinnerFormState.Started)
            {
                Debug.WriteLine(string.Format("IsActive={0}", IsActive));
                NotificationsModel.Add(new NotificationView("���� �������") {IsRead = false});
                IsTopMost = true;
                LunchModel.LunchCount++;
                LunchModel.LeftForLunchTime = _maxLunch;
            }
            else if (dinnerFormState == Constants.DinnerFormState.Reject)
            {
                Debug.WriteLine(string.Format("IsActive={0}", IsActive));
                NotificationsModel.Add(new NotificationView("����� ��������� ������� ���������") {IsRead = false});
            }
            else if (dinnerFormState == Constants.DinnerFormState.LimitExceed)
            {
                LunchModel.Comeback();
            }
        }

        private void TimerOnTick(object sender, EventArgs e)
        {
            // checking time to check rss

            if (_lastRssTimeUpdate.AddSeconds(Settings.Default.RSSCheckingTimeSec) < DateTime.Now)
            {
                CheckRss();
            }

            // ������� ����� ������ ������� ������
            foreach (var taskModel in TasksList.Where(t => t.Task.Status == ConstantsStatuses.InProcess))
            {
                taskModel.Tick(_timer.Interval);
            }

            if (CurrentUser.CurrentState == ConstantsNetworkStates.Lunch)
            {
                LunchModel.LeftForLunchTime = LunchModel.LeftForLunchTime.Subtract(_timer.Interval);
                //                RaisePropertyChanged(() => LunchModel.LunchButtonMessage);
            }

            // auto logoff
#if !DEBUG
            uint idleTime = Win32.GetIdleTime();
            if (idleTime > _maxAutoLogoffTime.TotalMilliseconds &&
                CurrentUser.CurrentState == ConstantsNetworkStates.Online)
            {
                OnRequestClose(new EventArgs());
            }
#endif

            // task auto updates 
            if (Settings.Default.TaskAutoUpdate)
            {
#if !DEBUG
                if (DateTime.Now.Subtract(_lastTaskUpdate) > Settings.Default.TeasksRefreshTime)
                    DoRefreshTask();
#else
                if (DateTime.Now.Subtract(_lastTaskUpdate).TotalSeconds > 5)
                    DoRefreshTask();
#endif

            }

            if (PeaceOn)
            {
                TimeSpan timeSpan = DateTime.Now.Subtract(_peaceStart);
                if (timeSpan.TotalMinutes >= Settings.Default.PeaceMaxTime)
                    PeaceOn = false;
            }
        }

        private BackgroundWorker _checkRssBackgroundWorker;

        private void CheckRss()
        {
            if (_checkRssBackgroundWorker == null)
            {
                _checkRssBackgroundWorker = new BackgroundWorker();
                _checkRssBackgroundWorker.DoWork += CheckRssBackgroundWorkerDoWork;
            }
            if (!_checkRssBackgroundWorker.IsBusy)
                _checkRssBackgroundWorker.RunWorkerAsync();
        }

        private DateTime _lastRssTimeUpdate = Repository.UserConfiguration.LastRssDate;
        private DateTime _lastRssDate = Repository.UserConfiguration.LastRssDate;
        private bool _peaceOn;
        private DateTime _peaceStart;

        private void CheckRssBackgroundWorkerDoWork(object sender, DoWorkEventArgs e)
        {
#if DEBUG
            Settings.Default.RssUrl = "http://habrahabr.ru/rss/best/";
#endif
            _lastRssDate = Repository.UserConfiguration.LastRssDate;

            IEnumerable<NotificationView> newRssNotify =
                RssManager.GetLast10RssNotify(Settings.Default.RssUrl).OrderByDescending(t => t.DateTime);
            if (newRssNotify == null) return;

            if (NotificationsModel.Notifications.All(n => n.NotificationType != NotificationType.Rss))
            {
                foreach (var rssNotify in newRssNotify)
                {
                    NotificationView notify = rssNotify;
                    _dispatcher.Invoke((Action) (() => NotificationsModel.AddAsRead(notify)));
                }
                var firstOrDefault = newRssNotify.FirstOrDefault();
                if (firstOrDefault != null)
                    Repository.UserConfiguration.LastRssDate = firstOrDefault.DateTime;
            }
            else
            {
                //                NotificationView notificationView = newRssNotify.FirstOrDefault(rss => rss.DateTime > _lastRssDate);
                IEnumerable<NotificationView> oldNotificationViews =
                    NotificationsModel.Notifications.Where(t => t.NotificationType == NotificationType.Rss);
                IEnumerable<NotificationView> notificationViews = newRssNotify.Except(oldNotificationViews);
                if (notificationViews.Any())
                {
                    foreach (var notificationView in notificationViews)
                    {
                        NotificationView view = notificationView;
                        _dispatcher.Invoke((Action) (() => NotificationsModel.Add(view)));
                        Repository.UserConfiguration.LastRssDate = notificationView.DateTime;
                        //                    Log.Write(LogLevel.Info, string.Format("����� ������ � �����: {0}", notificationView.Content));
                    }
                }
            }
            _lastRssTimeUpdate = DateTime.Now;
        }

        #endregion
        
        #region Properies

        public UniqBindableCollection<TimeTableView> ScheduleList
        {
            get { return _scheduleList; }
            set
            {
                _scheduleList = value;
                RaisePropertyChanged(() => ScheduleList);
            }
        }

        public BindableCollection<TaskModel> TasksList
        {
            get { return _tasksList; }
            set
            {
                _tasksList = value;
                RaisePropertyChanged(() => TasksList);
            }
        }

        public SelfStatModel SelfStatModel
        {
            get { return _selfStatModel; }
        }

        public UserView CurrentUser
        {
            get { return _currentUser; }
            set
            {
                _currentUser = value;
                RaisePropertyChanged(() => CurrentUser);
            }
        }

        public TaskModel CurrentTask
        {
            get { return _tasksList.FirstOrDefault(t => t.Task.Status == ConstantsStatuses.InProcess); }
        }

        public NotificationsModel NotificationsModel
        {
            get
            {
                return _notificationsModel;
            }
        }

        public bool IsTopMost
        {
            get { return _isTopMost; }
            set
            {
                _isTopMost = value;
                RaisePropertyChanged(() => IsTopMost);
            }
        }

        public Visibility ShowSchedule
        {
            get { return _showSchedule; }
            set
            {
                _showSchedule = value;
                RaisePropertyChanged(() => ShowSchedule);
            }
        }

        public Visibility ShowBoard
        {
            get { return _showBoard; }
            set
            {
                _showBoard = value;
                RaisePropertyChanged(() => ShowBoard);
            }
        }

        public Visibility ShowNews
        {
            get { return _showNews; }
            set
            {
                _showNews = value;
                RaisePropertyChanged(() => ShowNews);
            }
        }

        public Visibility ReconnectVisibility
        {
            get { return _reconnectVisibility; }
            set
            {
                _reconnectVisibility = value;
                RaisePropertyChanged(() => ReconnectVisibility);
            }
        }

        public LateControlModel LateControlModel
        {
            get { return _lateControlModel; }
        }

        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                RaisePropertyChanged(() => IsActive);
            }
        }

        public ICollectionView TasksCollectionView
        {
            get
            {
                if (_tasksCollectionView == null)
                {
                    _tasksCollectionView = new CollectionViewSource { Source = TasksList };
                    using (_tasksCollectionView.DeferRefresh())
                    {
                        _tasksCollectionView.SortDescriptions.Add(new SortDescription("Task.Priority",
                                                                                      ListSortDirection.Descending));
                        _tasksCollectionView.SortDescriptions.Add(new SortDescription("Task.CreationDate",
                                                                                      ListSortDirection.Ascending));
                        _tasksCollectionView.Filter += tasksCollectionView_Filter;
                    }
                }
                return _tasksCollectionView.View;
            }
        }

        void tasksCollectionView_Filter(object sender, FilterEventArgs e)
        {
            e.Accepted = false;
            if (e.Item is TaskModel)
            {
                TaskModel taskModel = (TaskModel)e.Item;
                if (Settings.Default.ShowCloseTasks)
                {
                    e.Accepted = true;
                }
                else
                {
                    if (taskModel.Task.Status != ConstantsStatuses.Closed)
                        e.Accepted = true;
                }
                if (Repository.LoginUser != null && taskModel.Task.CurrentWorker.Id != Repository.LoginUser.Id)
                    e.Accepted = false;
            }
        }

        #endregion

        #region Commands
        public ICommand ShowScheduleCommand
        {
            get
            {
                if (_scheduleCommand == null)
                    _scheduleCommand = new RelayCommand(DoShowSchedule);
                return _scheduleCommand;
            }
        }

        private void DoShowSchedule()
        {
            if (ShowSchedule == Visibility.Visible)
                ShowSchedule = Visibility.Hidden;
            else
                ShowSchedule = Visibility.Visible;
        }

        public ICommand ShowBoardCommand
        {
            get
            {
                if (_showBoardCommand == null)
                    _showBoardCommand = new RelayCommand(DoShowBoard);
                return _showBoardCommand;
            }
        }

        private void DoShowBoard()
        {
            if (ShowBoard == Visibility.Visible)
                ShowBoard = Visibility.Hidden;
            else
                ShowBoard = Visibility.Visible;
        }

        public ICommand RefreshTaskCommand
        {
            get
            {
                if (_refreshTaskCommand == null)
                    _refreshTaskCommand = new RelayCommand(DoRefreshTask);
                return _refreshTaskCommand;
            }
        }

        public BoardModel Board
        {
            get { return _board; }
        }

        public ICommand ShowNewsCommand
        {
            get
            {
                if (_showNewsCommand == null)
                    _showNewsCommand = new RelayCommand(DoShowRss);
                return _showNewsCommand;
            }
        }

        public LunchModel LunchModel
        {
            get { return _lunchModel; }
        }

        public bool PeaceOn
        {
            get { return _peaceOn; }
            set
            {
                _peaceOn = value;
                RaisePropertyChanged(() => PeaceOn);
                if (_peaceOn)
                    _peaceStart = DateTime.Now;
            }
        }

        private void DoShowRss()
        {
            if (ShowNews == Visibility.Visible)
                ShowNews = Visibility.Hidden;
            else
            {
                ShowNews = Visibility.Visible;
                NotificationsModel.SetAllNewsAsRead();
            }
        }

        public void DoRefreshTask()
        {
            Debug.WriteLine("Refreshing tasks..");
            LoadManager.UpdateCurrentTasks();
            LoadManager.LoadTaskFiles();
            RaisePropertyChanged(() => TasksCollectionView);
            UpdateTasksSorting();
            _lastTaskUpdate = DateTime.Now;
            Debug.WriteLine("Refreshing complete.");
        }

        #endregion

        public void Dispose()
        {
            _timer.IsEnabled = false;
            _timer.Stop();
            _timer = null;

            foreach (var taskModel in TasksList)
            {
                taskModel.Dispose();
            }

            Repository.Tasks.CollectionChanged -= TasksCollectionChanged;
            ServiceManager.Inst.Callback.LunchFormChangeState -= WorkerCallbackLunchFormChangeState;
            ServiceManager.Inst.Callback.CurrentTaskState -= WorkerCallbackCurrentTaskState;
            ServiceManager.Inst.Callback.NewMessageEvent -= WorkerCallbackNewMessageEvent;
            ServiceManager.Inst.ConectionReestablished -= ServiceManager_ConectionReestablished;
            ServiceManager.Inst.WaitingForConnection -= ServiceManager_WaitingForConnection;
        }
    }
}