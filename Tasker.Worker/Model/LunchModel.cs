using System;
using System.ComponentModel;
using System.Threading;
using System.Windows.Input;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel.Base;
using Tasker.Worker.Managers;
using Tasker.Worker.Properties;

namespace Tasker.Worker.Model
{
    internal class LunchModel : ObservableObject
    {
        private readonly MainWindowModel _mainWindowModel;

        private string _lunchButtonMessage = "���� �� ����!";
        private TimeSpan _leftForLunchTime;
        private readonly int _maxLunchCount = Settings.Default.MaxLunchCount;
        private RelayCommand _dinnerCommand;
        private string _lunchButtonTooltip = "��������� ����";
        private int _lunchCount;
        private TaskModel _lastTaskInProcess;
        private bool _waitingForRequest;

        public LunchModel(TimeSpan leftForLunchTime, MainWindowModel mainWindowModel)
        {
            _leftForLunchTime = leftForLunchTime;
            _mainWindowModel = mainWindowModel;
        }

        public TimeSpan LeftForLunchTime
        {
            get
            {
                return _leftForLunchTime;
            }
            set
            {
                _leftForLunchTime = value;
                RaisePropertyChanged(() => LunchButtonMessage);
            }
        }

        public string LunchButtonMessage
        {
            get
            {
                if (_lunchCount > 0)
                {
                    if (_mainWindowModel.CurrentUser.CurrentState == ConstantsNetworkStates.Lunch)
                    {
                        LunchButtonTooltip = "��������� � �����";
                        if (_leftForLunchTime.TotalSeconds >= 0)
                            return string.Format("� �������� ({0:00}:{1:00})", (int)_leftForLunchTime.TotalMinutes,
                                                 _leftForLunchTime.Seconds);
                        else
                        {
                            return string.Format("� ������� �� {0:00}:{1:00}", (int)_leftForLunchTime.Duration().TotalMinutes,
                                                 _leftForLunchTime.Duration().Seconds);
                        }
                    }
                }
                LunchButtonTooltip = "��������� ����";
                return String.Format("���� �� ����! {0}", _maxLunchCount - _lunchCount);
            }
            set
            {
                _lunchButtonMessage = value;
                RaisePropertyChanged(() => LunchButtonMessage);
            }
        }

        public string LunchButtonTooltip
        {
            get { return _lunchButtonTooltip; }
            set
            {
                _lunchButtonTooltip = value;
                RaisePropertyChanged(() => LunchButtonTooltip);
            }
        }

        public bool LunchButtonEnabled
        {
            get
            {
                if (_mainWindowModel.CurrentUser.CurrentState == ConstantsNetworkStates.Online)
                    return _maxLunchCount - _lunchCount > 0;
                return true;
            }
        }

        public int LunchCount
        {
            get { return _lunchCount; }
            set
            {
                _lunchCount = value;
                RaisePropertyChanged(() => LunchCount);
                RaisePropertyChanged(() => LunchButtonMessage);
                RaisePropertyChanged(() => LunchButtonEnabled);
            }
        }

        public bool WaitingForRequest
        {
            set { _waitingForRequest = value; }
            get
            {
                return _waitingForRequest;
            }
        }

        public ICommand SendLunchRequestCommand
        {
            get
            {
                if (_dinnerCommand == null)
                    _dinnerCommand = new RelayCommand(DoSendLunchRequest, CanSendLunchRequest);
                return _dinnerCommand;
            }
        }

        private bool CanSendLunchRequest()
        {
            if (_waitingForRequest) return false;
            return true;
        }

        private void DoSendLunchRequest()
        {
            if (_mainWindowModel.CurrentUser.CurrentState == ConstantsNetworkStates.Lunch)
            {
                ServiceManager.Inst.ServiceChannel.Comeback();
                Comeback();
            }
            else if (!_waitingForRequest)
            {
                _lastTaskInProcess = _mainWindowModel.CurrentTask;
                if (_lastTaskInProcess != null)
                    ServiceManager.Inst.ServiceChannel.GoForLunchRequest(_mainWindowModel.CurrentTask.Task.Creator.Id);
                else
                    ServiceManager.Inst.ServiceChannel.GoForLunchRequest(0);
                _waitingForRequest = true;
            }
        }

        public void Comeback()
        {
            _mainWindowModel.CurrentUser.CurrentState = ConstantsNetworkStates.Online;
            _mainWindowModel.IsTopMost = false;
            RaisePropertyChanged(() => LunchButtonMessage);
            RaisePropertyChanged(() => LunchButtonEnabled);
            if (_lastTaskInProcess != null)
                _lastTaskInProcess.Task.Status = ConstantsStatuses.InProcess;
            BackgroundWorker updaAsync = new BackgroundWorker();
            updaAsync.DoWork += updaAsync_DoWork;
            updaAsync.RunWorkerAsync();
        }

        private void updaAsync_DoWork(object sender, DoWorkEventArgs e)
        {
            Thread.Sleep(TimeSpan.FromSeconds(3));
            _mainWindowModel.DoRefreshTask();
        }
    }
}