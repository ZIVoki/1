using System;
using Tasker.Tools.ViewModel.Base;

namespace Tasker.Worker.Model
{
    class LateItemView: ObservableObject
    {
        private DateTime _lateDateTime;
        private bool _isLate;

        public LateItemView(DateTime lateDateTime, bool isLate)
        {
            _lateDateTime = lateDateTime;
            _isLate = isLate;
        }

        public LateItemView(bool isLate)
        {
            _isLate = isLate;
        }

        public DateTime LateDateTime
        {
            get { return _lateDateTime; }
            set
            {
                _lateDateTime = value;
                RaisePropertyChanged(() => LateDateTime);
            }
        }

        public bool IsLate
        {
            get { return _isLate; }
            set
            {
                _isLate = value;
                RaisePropertyChanged(() => IsLate);
            }
        }

        public override string ToString()
        {
            if (_isLate)
                return _lateDateTime.ToString("��������� dd.MM.yyyy HH:mm");
            return "�� �����!";
        }
    }
}