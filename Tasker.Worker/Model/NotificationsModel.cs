using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Media;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using Tasker.Tools.Extensions;
using Tasker.Tools.ViewModel;
using Tasker.Tools.ViewModel.Base;

namespace Tasker.Worker.Model
{
    internal delegate void NewRssPostComes(NotificationView notification);

    internal class NotificationsModel : ObservableObject
    {
        private BindableCollection<NotificationView> _notifications = new BindableCollection<NotificationView>();
        private readonly SoundPlayer _player;
        private RelayCommand _setAllAsRead;
        public event EventHandler RequestActivate;
        public event NewRssPostComes RequestNewRssPost;

        protected virtual void OnRequestNewRssPost(NotificationView notification)
        {
            NewRssPostComes handler = RequestNewRssPost;
            if (handler != null) handler(notification);
        }

        public void OnRequestActivate(EventArgs e)
        {
            EventHandler handler = RequestActivate;
            if (handler != null) handler(this, e);
        }

        public NotificationsModel()
        {
            Uri uri = new Uri(@"pack://application:,,,/Tasker.Worker;Component/resources/Uke_ring.wav");
            var streamResourceInfo = Application.GetResourceStream(uri);
            if (streamResourceInfo != null)
            {
                _player = new SoundPlayer(streamResourceInfo.Stream);
            }

            _notifications.CollectionChanged += NotificationsCollectionChanged;
        }

        void NotificationsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            NotificationView notificationView;
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (object newItem in e.NewItems)
                    {
                        notificationView = (NotificationView) newItem;
                        notificationView.NotificationReadEvent += NotificationViewNotificationReadEvent;

                        if (notificationView.NotificationType == NotificationType.Rss && !notificationView.IsRead)
                            OnRequestNewRssPost(notificationView);
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (object newItem in e.NewItems)
                    {
                        notificationView = (NotificationView)newItem;
                        notificationView.NotificationReadEvent -= NotificationViewNotificationReadEvent;
                    }
                    break;
            }
            RaisePropertyChanged(() => IsAnyNewNotifications);
            RaisePropertyChanged(() => IsAnyNewRssNotifications);
            RaisePropertyChanged(() => UnreadCount);
            RaisePropertyChanged(() => LastNotification);

            if (UnreadCount > 0)
            {
                if (Repository.UserConfiguration.IsSoundsOn.HasValue && Repository.UserConfiguration.IsSoundsOn.Value)
                {
                    _player.Play();
                }
                OnRequestActivate(new EventArgs());
            }
        }

        void NotificationViewNotificationReadEvent(object sender, EventArgs e)
        {
            RaisePropertyChanged(() => IsAnyNewNotifications);
            RaisePropertyChanged(() => IsAnyNewRssNotifications);
            RaisePropertyChanged(() => UnreadCount);
            RaisePropertyChanged(() => LastNotification);
        }

        public BindableCollection<NotificationView> Notifications
        {
            get
            {
                return _notifications;
            }
            set
            {
                _notifications = value;
                RaisePropertyChanged(() => Notifications);
                RaisePropertyChanged(() => IsAnyNewNotifications);
                RaisePropertyChanged(() => IsAnyNewRssNotifications);
            }
        }

        public bool IsAnyNewNotifications
        {
            get { return Notifications.Any(n => n.IsRead == false); }
        }

        public bool IsAnyNewRssNotifications
        {
            get { return Notifications.Any(n => n.IsRead == false && n.NotificationType == NotificationType.Rss); }
        }

        public int UnreadCount
        {
            get { return Notifications.Count(n => n.IsRead == false); }
        }

        public NotificationView LastNotification
        {
            get { return Notifications.LastOrDefault(); }
        }

        public ICommand SetAllAsRead
        {
            get 
            {
                if (_setAllAsRead == null)
                {
                    _setAllAsRead = new RelayCommand(DoSetAllAsRead);
                }
                return _setAllAsRead;
            }
        }

        private CollectionViewSource _newsCollectionView;
        public ICollectionView RssNotifications
        {
            get
            {
                if (_newsCollectionView == null)
                {
                    _newsCollectionView = new CollectionViewSource { Source = Notifications };
                    using (_newsCollectionView.DeferRefresh())
                    {
                        _newsCollectionView.SortDescriptions.Add(new SortDescription("DateTime",
                                                                                      ListSortDirection.Descending));
                        
                        _newsCollectionView.Filter += newsCollectionView_Filter;
                    }
                }
                return _newsCollectionView.View;
            }
        }

        private void newsCollectionView_Filter(object sender, FilterEventArgs e)
        {
            e.Accepted = false;
            NotificationView notificationView = (NotificationView) e.Item;
            if (notificationView == null) return;
            if (notificationView.NotificationType == NotificationType.Rss)
                e.Accepted = true;
        }

        private void DoSetAllAsRead()
        {
            foreach (NotificationView notificationView in Notifications.Where(n => !n.IsRead))
            {
                notificationView.IsRead = true;
            }
        }

        public void Add(NotificationView notificationView)
        {
            Notifications.Add(notificationView);
        }

        public void AddAsRead(NotificationView notificationView)
        {
            notificationView.IsRead = true;
            Notifications.Add(notificationView);
        }

        public void SetAllNewsAsRead()
        {
            foreach (NotificationView notificationView in Notifications.Where(n => !n.IsRead && n.NotificationType == NotificationType.Rss))
            {
                notificationView.IsRead = true;
            }
        }
    }
}