﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Tasker.Tools.ViewModel;
using Tasker.Tools.ViewModel.Base;

namespace Tasker.Worker.Model
{
    public class BoardModel : ObservableObject
    {
        private bool _isAnyUnreadDocs;

        ObservableCollection<FolderView> _foldersView = new ObservableCollection<FolderView>();

        public ObservableCollection<FolderView> FoldersView
        {
            get { return _foldersView; }
            set
            {
                _foldersView = value;
                RaisePropertyChanged(() => FoldersView);
            }
        }

        public bool IsAnyUnreadDocs
        {
            get { return _isAnyUnreadDocs; }
            set
            {
                _isAnyUnreadDocs = value;
                RaisePropertyChanged(() => IsAnyUnreadDocs);
            }
        }

        public BoardModel()
        {
            List<FolderView> folderViews = Repository.Folders.Where(f => f.Parent == null).ToList();
            if (folderViews != null)
            {
                foreach (var rootFolder in folderViews)
                {
                    _foldersView.Add(rootFolder);
                }
            }
        }
    }
}
