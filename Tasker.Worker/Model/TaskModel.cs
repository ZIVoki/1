﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Input;
using System.Windows.Forms;
using Tasker.Tools.Extensions;
using Tasker.Tools.Managers;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;
using Tasker.Tools.ViewModel.Base;
using Tasker.Worker.Managers;
using Tasker.Worker.Windows;
using Clipboard = System.Windows.Clipboard;
using MessageBox = System.Windows.MessageBox;

namespace Tasker.Worker.Model
{
    public class TaskModel: ObservableObject, IDisposable
    {
        private TaskView _task;
        private double _progressValue;
        private int _minutesLeft;
        private TimeSpan _timeLeft;
        private string _readyButtonText = "Готово!";
        private string _nextMessageString = "";
        private OpenFileDialog _browserDialog;
        private ChatWindow _chatWindow;
        private TimeSpan _inPкocessTime;
        private bool _newFileExist;

        private RelayCommand _taskReadyCommand;
        private RelayCommand _showFilesCommand;
        private RelayCommand _addNewFileCommand;
        private RelayCommand _openDialogCommand;
        private RelayCommand<string> _sendNewMessageCommand;
        private RelayCommand _copyHeaderCommand;

        public TaskModel()
        {
            _progressValue = 0;
            _minutesLeft = 0;
            _timeLeft = TimeSpan.Zero;
        }

        public TaskModel(TaskView task)
        {
            _progressValue = 0;
            _minutesLeft = (int) task.PlannedTime.TotalMinutes;
            _timeLeft = task.PlannedTime;
            _task = task;
            _task.Files.CollectionChanged += FilesCollectionChanged;
            _inPкocessTime = task.InProcessTime;
            _prevFileCount = task.Files.Count;
        }

        #region Properties

        public TaskView Task
        {
            get { return _task; }
            set
            {
                _task = value;
                InPкocessTime = _task.InProcessTime;
                _minutesLeft = (int) _task.PlannedTime.TotalMinutes;
                _timeLeft = _task.PlannedTime;
//                RaisePropertyChanged(() => Task);
                _task.Files.CollectionChanged += FilesCollectionChanged;
            }
        }

        public TimeSpan InPкocessTime
        {
            get { return _inPкocessTime; }
            set
            {
                _inPкocessTime = value;
//                CountUserProgress(TimeSpan.FromMilliseconds(50));
                RaisePropertyChanged(() => InPкocessTime);
            }
        }

        public double ProgressValue
        {
            get
            {
                return _progressValue;
            }
            set
            {
                _progressValue = value;
                RaisePropertyChanged(() => ProgressValue);
            }
        }

        public TimeSpan TimeLeft
        {
            get { return _timeLeft; }
            set
            {
                _timeLeft = value;
                RaisePropertyChanged(() => TimeLeft);
            }
        }

        public int MinutesLeft
        {
            get { return _minutesLeft; }
            set
            {
                _minutesLeft = value;
                RaisePropertyChanged(() => MinutesLeft);
            }
        }

        public string NextMessageString
        {
            get
            {
                return _nextMessageString;
            }
            set
            {
                _nextMessageString = value;
                RaisePropertyChanged(() => NextMessageString);
            }
        }

        private int _prevFileCount = 0;
        private RelayCommand _fileWhatchCommand;

        public bool IsAnyFiles
        {
            get
            {
                bool isAnyFiles = _task.Files.Any();
                if (_prevFileCount < _task.Files.Count)
                {
                    NewFileExist = true;
                    _prevFileCount = _task.Files.Count;
                }
                return isAnyFiles;
            }
        }

        public bool IsChatWindowIsActive
        {
            get
            {
                if (_chatWindow == null) return false;
                return (_chatWindow.IsActive);
            }
        }

        public bool IsChatWindowExist
        {
            get
            {
                if (_chatWindow == null) return false;
                return true;
            }
        }

        public bool NewFileExist
        {
            get
            {
                return _newFileExist;
            }
            set
            {
                _newFileExist = value;
                RaisePropertyChanged(() => NewFileExist);
            }
        }

        #endregion
        
        #region Commands

        public ICommand TaskReadyCommand
        {
            get
            {
                if (_taskReadyCommand == null)
                {
                    _taskReadyCommand = new RelayCommand(SetTaskReady);
                }

                return _taskReadyCommand;
            }
        }

        public ICommand ShowFilesCommand
        {
            get
            {
                if (_showFilesCommand == null)
                {
                    _showFilesCommand = new RelayCommand(DoShowFilesWindow);
                }

                return _showFilesCommand;
            }
        }

        public ICommand AddNewFileCommand
        {
            get 
            {
                if (_addNewFileCommand == null)
                {
                    _addNewFileCommand = new RelayCommand(DoAddNewFileCommand);
                }

                return _addNewFileCommand;
            }
        }

        public ICommand OpenMessagesCommand
        {
            get 
            {
                if (_openDialogCommand == null)
                {
                    _openDialogCommand = new RelayCommand(DoOpenMessages);
                }
                return _openDialogCommand;
            }
        }

        public ICommand SendNewMessageCommand
        {
            get
            {
                if (_sendNewMessageCommand == null)
                {
                    _sendNewMessageCommand = new RelayCommand<string>(DoSendMessageCommand);
                }
                return _sendNewMessageCommand;
            }
        }

        public ICommand CopyHeaderCommand
        {
            get
            {
                if (_copyHeaderCommand == null)
                    _copyHeaderCommand = new RelayCommand(DoCopyHeaderToClipboard);
                return _copyHeaderCommand;
            }
        }

        private void DoSendMessageCommand(string obj)
        {
            NextMessageString = obj;
            DoSendMessageCommand();
        }

        private void DoSendMessageCommand()
        {
            if (String.IsNullOrEmpty(NextMessageString) || String.IsNullOrWhiteSpace(NextMessageString)) return;

            MessageView messageView = new MessageView(Task)
            {
                Text = NextMessageString,
                UserDestination = Task.Creator,
                SendDate = DateTime.Now,
                Status = ConstantsStatuses.Enabled
            };
            MessageDTO dto = messageView.BuildDTO();

            int id = ServiceManager.Inst.ServiceChannel.SendMessage(dto);
            if (id == 0)
            {
                MessageBox.Show("Не удалось отправить пустое сообщение.");
            }
            else
            {
                messageView.Id = id;
                messageView.IsMy = true;
                messageView.UserFrom = Repository.LoginUser;
                messageView.IsReaded = true;
                Task.Messages.Messages.Add(messageView);
                // clear string
                NextMessageString = "";
            }
        }

        private void DoOpenMessages()
        {
            if (_chatWindow == null)
            {
                try
                {
                    MessageDTO[] messageDtos = ServiceManager.Inst.ServiceChannel.GetMessages(Task.Id);
                    foreach (var messageDto in messageDtos)
                    {
                        MessageManager.GetMessage(messageDto, Task, Task.Messages.Messages, 
                                                  Repository.Users, Repository.LoginUser.Id,
                                                  true);
                    }
                }
                catch (Exception e)
                {
                    
                }

                _chatWindow = new ChatWindow {DataContext = this, ShowActivated = true};
                _chatWindow.Closed += (o, args) => _chatWindow = null;
                _chatWindow.Show();
            }
            
            _chatWindow.Activate();
            Task.Messages.SetAllAsRead();
        }

        private void DoAddNewFileCommand()
        {
            DialogResult dialogResult = BrowserDialog.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                foreach (var filePath in BrowserDialog.FileNames)
                {
                    if (string.IsNullOrEmpty(filePath)) continue;
                    if (!File.Exists(filePath)) continue;

                    FileInfo fileInfo = new FileInfo(filePath);
                    AttachFileView exist = null;

                    //check files collection of task. find exists
                    if (Task.Files != null)
                    {
                        exist = Task.Files.FirstOrDefault(a => a.FilePath == filePath);
                    }
                    else
                        Task.Files = new UniqBindableCollection<AttachFileView>();

                    if (exist == null)
                    {
                        AttachFileView newAttachFile = new AttachFileView(Task)
                        {
                            CreatedAt = DateTime.Now,
                            FilePath = filePath,
                            Title = fileInfo.Name,
                            IsFinalyFile = false,
                            Status = ConstantsStatuses.Enabled
                        };

                        int newFileId = ServiceManager.Inst.ServiceChannel.AddFileToTask(newAttachFile.BuildDTO());
                        newAttachFile.Id = newFileId;
                        Task.Files.Add(newAttachFile);
                    }
                }
            }
        }

        private OpenFileDialog BrowserDialog
        {
            get
            {
                if (_browserDialog == null)
                {
                    _browserDialog = new OpenFileDialog
                    {
                        Title = @"Выберите файл..",
                        Multiselect = true
                    };
                }
                return _browserDialog;
            }
        }

        public ICommand FileWhatchCommand
        {
            get
            {
                if (_fileWhatchCommand == null)
                    _fileWhatchCommand = new RelayCommand(DoWhatchFile);
                return _fileWhatchCommand;
            }
        }

        private void DoWhatchFile()
        {
            this.NewFileExist = false;
        }

        private void DoCopyHeaderToClipboard()
        {
            try
            {
                Clipboard.Clear();
                Clipboard.SetText(this.Task.Title);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                App.Logger.Error("Ошибка копирования", e);
            }
        }
        
        private void DoShowFilesWindow()
        {
            if (Task.Files.Any())
            {
                string fileOpenArg = string.Format(@"/select");

                string openS = fileOpenArg;
                string fullPath = string.Empty;
                List<string> allFilesPaths = Task.Files.OrderBy(f => f.FilePath).Select(f => f.FilePath).ToList();
                foreach (string filePath in allFilesPaths)
                {
                    if (fullPath != null && !fullPath.Equals(Path.GetDirectoryName(filePath)) && !openS.Equals(fileOpenArg))
                    {
                        Process.Start("explorer.exe", openS);
                        openS = fileOpenArg;
                    }
                    openS = string.Format("{0},{1}", openS, filePath);
                    fullPath = Path.GetDirectoryName(filePath);
                }
                if (!openS.Equals(fileOpenArg))
                    Process.Start("explorer.exe", openS);
            }
        }

        private void SetTaskReady()
        {
            ServiceManager.Inst.ServiceChannel.SendForReview(Task.BuildDTO(), Task.Creator.Id);
            Task.Status = ConstantsStatuses.SendedForReview;
        }

        #endregion

        private void FilesCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            RaisePropertyChanged(() => IsAnyFiles);
        }

        internal void Tick(TimeSpan interval)
        {
//            CountTaskProgress();
            CountUserProgress(interval);
        }
        
        /// <summary>
        /// Считает сколько пользователь выполнят данную задачу
        /// </summary>
        /// <param name="interval"> </param>
        private void CountUserProgress(TimeSpan interval)
        {
            _inPкocessTime = _inPкocessTime.Add(interval);
            double d = (_inPкocessTime.TotalMilliseconds/Task.PlannedTime.TotalMilliseconds)*100;

            if (d > 0)
                ProgressValue = d;
            else
                ProgressValue = 100;

            TimeSpan timeLeft = Task.PlannedTime.Subtract(_inPкocessTime);
            if (timeLeft.TotalSeconds > 0)
            {
                TimeLeft = timeLeft;
                MinutesLeft = (int) TimeLeft.TotalMinutes;
            }
            else
            {
                MinutesLeft = 0;
                TimeLeft = TimeSpan.Zero;
            }
        }

        /// <summary>
        //// Считает сколько времени прошло от начала до деделайна задачи
        /// </summary>
        private void CountTaskProgress()
        {
            if (Task == null) return;

            DateTime dateTimeNow = DateTime.Now;
//            TimeSpan lostTime = Task.PlannedTime.Subtract(Task.DeadLine.Value.Subtract(dateTimeNow));
            TimeSpan lostTime = dateTimeNow.Subtract(Task.CreationDate);
            TimeSpan totalTime = Task.DeadLine.Value.Subtract(Task.CreationDate);
            double d = (lostTime.TotalSeconds/totalTime.TotalSeconds)*100;

            if (d > 0)
                ProgressValue = d;
            else
                ProgressValue = 100;

            TimeSpan left = Task.DeadLine.Value.Subtract(dateTimeNow);
            if (left.TotalSeconds >= 0)
            {
                TimeLeft = left;
                MinutesLeft = (int) TimeLeft.TotalMinutes;
            }
            else
            {
                MinutesLeft = 0;
                TimeLeft = TimeSpan.Zero;
            }
        }

        public void Dispose()
        {
            if (_chatWindow != null)
                _chatWindow.Close();
        }
    }
}
