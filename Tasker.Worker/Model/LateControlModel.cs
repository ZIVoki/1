using System;
using Tasker.Tools.Extensions;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel.Base;
using Tasker.Worker.Managers;

namespace Tasker.Worker.Model
{
    class LateControlModel: ObservableObject
    {
        private BindableCollection<LateItemView> _latesList = new BindableCollection<LateItemView>();
        private readonly DateTime _startDate = DateTime.Now.Date.FirstDayOfMonth();
        private readonly DateTime _endDate = DateTime.Now.Date.EndOfLastDayOfMonth();
        private const int MaxItemsCount = 3;

        public LateControlModel()
        {
            AttendanceLogItemDTO[] attendanceLogItemDtos = ServiceManager.Inst.ServiceChannel.GetMyLatesByDates(_startDate, _endDate);
            while (MaxItemsCount - attendanceLogItemDtos.Length > _latesList.Count)
                _latesList.Add(new LateItemView(false));
            foreach (var attendanceLogItemDto in attendanceLogItemDtos)
            {
                _latesList.Add(new LateItemView(attendanceLogItemDto.EventDateTime, true));
            }
        }

        public BindableCollection<LateItemView> LatesList
        {
            get { return _latesList; }
            set
            {
                _latesList = value;
                RaisePropertyChanged(() => LatesList);
            }
        }
    }
}