using System;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using System.Windows.Input;
using Tasker.Tools.ViewModel.Base;
using Tasker.Worker.Managers;
using Tasker.Worker.Properties;

namespace Tasker.Worker.Model
{
    internal class SelfStatModel: ObservableObject
    {
        private const string TextMask = "{0}: {1}:{2}";
        private DateTime _currentMonth = DateTime.Now;
        private readonly CultureInfo _ruRu = new CultureInfo("ru-RU");


        private RelayCommand _prevMounthCommand;
        private RelayCommand _nextMounthCommand;
        private RelayCommand _showFullStat;

        public SelfStatModel()
        {
            LoadManager.LoadSelfStatistic(_currentMonth);
        }

        public String StatTitle
        {
            get
            {
                string monthText = _currentMonth.ToString("MMMM", _ruRu);
                string hours = ((int)Math.Floor(Repository.MonthTotalHours.TotalHours)).ToString();
                string minutes = Repository.MonthTotalHours.Minutes.ToString();
                return string.Format(TextMask, monthText, hours, minutes).TrimEnd();
            }
        }

        public void UpdateCurrentStat()
        {
            Thread.Sleep(TimeSpan.FromSeconds(2));
            LoadManager.LoadSelfStatistic(_currentMonth);
            RaisePropertyChanged(() => StatTitle);
        }

        public ICommand PrevMounthCommand
        {
            get
            {
                if (_prevMounthCommand == null)
                {
                    _prevMounthCommand = new RelayCommand(ShowPrevMounth);
                }

                return _prevMounthCommand;
            }
        }

        public ICommand NextMounthCommand
        {
            get
            {
                if (_nextMounthCommand == null)
                {
                    _nextMounthCommand = new RelayCommand(ShowNextMounth, IsNextMonthExist);
                }

                return _nextMounthCommand;
            }
        }

        private bool IsNextMonthExist()
        {
            if (_currentMonth.AddMonths(1).Date > DateTime.Now.Date)
                return false;
            return true;
        }

        public ICommand ShowFullStat
        {
            get 
            {
                if (_showFullStat == null)
                {
                    _showFullStat = new RelayCommand(ShowStatPAge);
                }

                return _showFullStat;
            }
        }

        private void ShowStatPAge()
        {
            string taskerWebSite = Settings.Default.TaskerWebSite;
            Process.Start(taskerWebSite);
        }

        private void ShowPrevMounth()
        {
            _currentMonth = _currentMonth.AddMonths(-1);
            LoadManager.LoadSelfStatistic(_currentMonth);
            RaisePropertyChanged("StatTitle");
        }


        private void ShowNextMounth()
        {
            _currentMonth = _currentMonth.AddMonths(1);
            LoadManager.LoadSelfStatistic(_currentMonth);
            RaisePropertyChanged("StatTitle");
        }
    }
}