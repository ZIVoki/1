﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using NLog;
using Tasker.Tools.ViewModel.Base;
using Tasker.Worker.Properties;

namespace Tasker.Worker.Configuration
{
    [Serializable]
    public class Config : ObservableObject
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private static readonly XmlSerializer Serializer = new XmlSerializer(typeof(Config));
        static Config _config;

        private bool? _isSoundsOn;
        private string _lastLogin;
        private bool? _isAutoLogin = false;
        private List<EntersLogItem> _lateLog;
        private DateTime _lastRssDate;

        public bool? IsSoundsOn
        {
            get { return _isSoundsOn; }
            set
            {
                _isSoundsOn = value;
                RaisePropertyChanged(() => IsSoundsOn);
            }
        }

        public string LastLogin
        {
            get { return _lastLogin; }
            set
            {
                _lastLogin = value;
                RaisePropertyChanged(() => LastLogin);
            }
        }

        public List<EntersLogItem> LateLog
        {
            get { return _lateLog; }
            set
            {
                _lateLog = value;
                RaisePropertyChanged(() => LateLog);
            }
        }

        public DateTime LastRssDate
        {
            get { return _lastRssDate; }
            set
            {
                _lastRssDate = value;
                RaisePropertyChanged(() => LastRssDate);
            }
        }

        public bool? IsAutoLogin
        {
            get { return _isAutoLogin; }
            set
            {
                _isAutoLogin = value;
                RaisePropertyChanged(() => IsAutoLogin);
            }
        }

        protected Config()
        {
            IsSoundsOn = false;
            LateLog = new List<EntersLogItem>();
            LastRssDate = DateTime.Now.AddDays(-1);
        }

        public void Save()
        {
            try
            {
                var settingsPath = GetSettingsPath();
                TextWriter streamWriter = new StreamWriter(settingsPath);
                Serializer.Serialize(streamWriter, _config);
                streamWriter.Close();
            }
            catch (Exception ex)
            {
                Logger.Error("Ошибка сохранения пользовательского конфигурационного файла", ex);
            }
        }

        private static string GetSettingsPath()
        {
            string settingsFilesPath = App.GetSettingsFilesPath();
            string settingsPath = Path.Combine(settingsFilesPath, Settings.Default.UserSettingsFileName);
            return settingsPath;
        }

        public static Config Load()
        {
            if (_config == null)
            {
                string settingsPath = GetSettingsPath();

                if (File.Exists(settingsPath))
                {
                    try
                    {
                        TextReader streamReader = new StreamReader(settingsPath);
                        _config = (Config)Serializer.Deserialize(streamReader);
                        streamReader.Close();
                        streamReader.Dispose();
                    }
                    catch (Exception ex)
                    {
                        _config = new Config();
                        Logger.Error("Ошибка загрузки пользовательского конфигурационного файла", ex);
                    }
                }
                else
                {
                    _config = new Config();
                }
            }

            // удаляем старые входы, чтобы не забивать память
            ClearOldLateLogValues();

            return _config;
        }

        private static void ClearOldLateLogValues()
        {
            DateTime dateTime = DateTime.Now.Subtract(TimeSpan.FromDays(2));
            IEnumerable<EntersLogItem> oldEntersLogItems = _config.LateLog.Where(t => t.EneterDateTime < dateTime).ToList();
            foreach (var logItem in oldEntersLogItems)
            {
                _config.LateLog.Remove(logItem);
            }
        }
    }

    public class EntersLogItem
    {
        public EntersLogItem()
        {
            TimeTableId = 0;
            EneterDateTime = DateTime.Now;
        }

        public EntersLogItem(int timeTableId): this()
        {
            TimeTableId = timeTableId;
        }

        public EntersLogItem(int timeTableId, DateTime eneterDateTime)
        {
            TimeTableId = timeTableId;
            EneterDateTime = eneterDateTime;
        }

        public int TimeTableId { get; set; }
        public DateTime EneterDateTime { get; set; }
    }
}
