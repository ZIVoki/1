using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using Tasker.Tools.ServiceReference;

namespace Tasker.Worker.Converters
{
    public class LunchStateToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            ConstantsNetworkStates constantsNetworkStates = (ConstantsNetworkStates) value;
            if (value != null)
            {
                if (constantsNetworkStates == ConstantsNetworkStates.Lunch)
                    return Visibility.Visible;
            }

            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}