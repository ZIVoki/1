using System;
using System.Globalization;
using System.Windows.Data;
using Tasker.Tools.ServiceReference;

namespace Tasker.Worker.Converters
{
    public class ReadyButtonTooltipConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return null;
            ConstantsStatuses status = (ConstantsStatuses)value;
            if (status == null) return null;

            switch (status)
            {
                case ConstantsStatuses.Nothing:
                    break;
                case ConstantsStatuses.Enabled:
                    break;
                case ConstantsStatuses.Disabled:
                    break;
                case ConstantsStatuses.Deleted:
                    break;
                case ConstantsStatuses.Planing:
                    break;
                case ConstantsStatuses.Closed:
                    return "������ �������";
                case ConstantsStatuses.InProcess:
                    break;
                case ConstantsStatuses.SendedForReview:
                    return "������ ���������� �� ��������";
                case ConstantsStatuses.UserEnter:
                    break;
                case ConstantsStatuses.UserLeave:
                    break;
                case ConstantsStatuses.UserLunch:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return "��������� ������ �� ��������";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}