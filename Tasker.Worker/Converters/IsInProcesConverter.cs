using System;
using System.Globalization;
using System.Windows.Data;
using Tasker.Tools.ServiceReference;

namespace Tasker.Worker.Converters
{
    public class IsInProcesConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return null;
            ConstantsStatuses status = (ConstantsStatuses)value;
            if (status == null) return null;

            return (status == ConstantsStatuses.InProcess || status == ConstantsStatuses.Disabled);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}