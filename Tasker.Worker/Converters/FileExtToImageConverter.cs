using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace Tasker.Worker.Converters
{
    public class FileExtToImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return null;
            if (value is String)
            {
                switch (value.ToString())
                {
                    case ".pdf":
                        return new BitmapImage(new Uri(@"pack://application:,,,/icons/file-pdf.png", UriKind.RelativeOrAbsolute));
                    case ".xls":
                        return new BitmapImage(new Uri(@"pack://application:,,,/icons/file-xls.png", UriKind.RelativeOrAbsolute));
                    case ".xlsx":
                        return new BitmapImage(new Uri(@"pack://application:,,,/icons/file-xls.png", UriKind.RelativeOrAbsolute));
                    case ".doc":
                        return new BitmapImage(new Uri(@"pack://application:,,,/icons/file-doc.png", UriKind.RelativeOrAbsolute));
                    case ".docx":
                        return new BitmapImage(new Uri(@"pack://application:,,,/icons/file-doc.png", UriKind.RelativeOrAbsolute));
                    case ".dotx":
                        return new BitmapImage(new Uri(@"pack://application:,,,/icons/file-doc.png", UriKind.RelativeOrAbsolute));
                    default:
                        return new BitmapImage(new Uri(@"pack://application:,,,/icons/folder.png", UriKind.RelativeOrAbsolute));
                }
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}