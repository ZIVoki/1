﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using Tasker.Tools.ViewModel;

namespace Tasker.Worker.Converters
{
    class ImagePriorityTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return null;
            PriorityType status = (PriorityType)value;
            if (status == null) return null;

            switch (status)
            {
                case PriorityType.Quick:
                    return new BitmapImage(new Uri(@"pack://application:,,,/Tasker.Worker;Component/icons/fire_32x32.png"));
                default:
                    return new BitmapImage(new Uri("", UriKind.Relative));
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}