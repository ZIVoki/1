using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using Tasker.Tools.ServiceReference;

namespace Tasker.Worker.Converters
{
    public class NotClosedToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return null;
            ConstantsStatuses status = (ConstantsStatuses)value;
            if (status == null) return null;

            if (status != ConstantsStatuses.Closed)
                return Visibility.Visible;
            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}