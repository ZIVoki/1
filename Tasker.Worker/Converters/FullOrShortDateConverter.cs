using System;
using System.Globalization;
using System.Windows.Data;

namespace Tasker.Worker.Converters
{
    public class FullOrShortDateConverter: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return string.Empty;
            DateTime dateTime = (DateTime) value;

            if (dateTime != null)
            {
                if (dateTime.Date == DateTime.Now.Date)
                {
                    return dateTime.ToString("HH:mm");
                }
                return dateTime.ToString("d.MM HH:mm");
            }
            return string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}