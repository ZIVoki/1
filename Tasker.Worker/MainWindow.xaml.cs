﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shell;
using Tasker.Tools.Controls.TrayControls;
using Tasker.Worker.Managers;
using Tasker.Worker.Model;
using Tasker.Worker.Windows;

namespace Tasker.Worker
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainWindowModel _mainWindowModel;
        private bool _autoLogOff = false;
        private ExitWindow _exitWindow;

        public MainWindow()
        {
            InitializeComponent();
        }

        protected override void OnInitialized(EventArgs e)
        {
            _mainWindowModel = ((MainWindowModel)DataContext);
            _mainWindowModel.RequestClose += (o, args) => ForceClose();

            _mainWindowModel.NotificationsModel.RequestActivate += OnNotificationsModelOnRequestActivate;
            _mainWindowModel.NotificationsModel.RequestNewRssPost += NotificationsModel_RequestNewRssPost;
            this.Title = string.Format("Tasker Worker (ver. {0})", App.CurrentVersion);
            base.OnInitialized(e);
        }

        private void OnNotificationsModelOnRequestActivate(object o, EventArgs args)
        {
            if (_mainWindowModel.PeaceOn) return;
            if (this.WindowState == WindowState.Minimized)
                this.WindowState = WindowState.Normal;
            this.Activate();
        }

        void NotificationsModel_RequestNewRssPost(Tools.ViewModel.NotificationView notification)
        {
            FancyBalloon balloon = new FancyBalloon(Tools.Constants.BaloonType.RSS)
            {
                BalloonTitle = string.Format("{0}...", notification.Content.Substring(0, 10)),
                OnClickAction = notification.OnClickAction
            };

            if (notification.Content.Length > Tools.Constants.MessagePreviewSignCount)
            {
                balloon.BalloonText = string.Format("{0}...", notification.Content.Substring(0, Tools.Constants.MessagePreviewSignCount));
            }
            else
            {
                balloon.BalloonText = notification.Content;
            }

            TaskBarIcon.ShowCustomBalloon(balloon, PopupAnimation.Slide, null);
        }

        public static void ShowServerError(string message)
        {
            MessageBox.Show(message, "Ошибка на стороне сервера.", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        protected override void OnClosed(EventArgs e)
        {
            _mainWindowModel.Dispose();
            ServiceManager.Inst.CloseAndLogoff();
            base.OnClosed(e);
        }

        public void ForceClose()
        {
            if (_exitWindow != null)
                _exitWindow.Close();
            _autoLogOff = true;
            this.Close(); 
        }

//        private CancelEventArgs cancelEventArgs = new CancelEventArgs(true);
        protected override void OnClosing(CancelEventArgs e)
        {
#if !DEBUG
            if (!_autoLogOff)
            {
                e.Cancel = true;

                if (_exitWindow == null)
                {
                    _exitWindow = new ExitWindow {ShowActivated = true};
                    _exitWindow.Closed += _exitWindow_Closed;
                    _exitWindow.Show();
                }
                else
                {
                    if (_exitWindow.DialogResult.HasValue && _exitWindow.DialogResult.Value)
                        e.Cancel = false;
                    else
                        e.Cancel = true;
                    _exitWindow = null;
                }
            }
#endif
            TaskBarIcon.Dispose();
            base.OnClosing(e);
        }

        void _exitWindow_Closed(object sender, EventArgs e)
        {
            base.Close();
        }

        internal MainWindowModel CurrentDataContext
        {
            get { return (MainWindowModel) DataContext; }
        }

        private void MainWindowDeactivated(object sender, EventArgs e)
        {
            CurrentDataContext.IsActive = false;
            Debug.WriteLine(string.Format("MainWindowDeactivated IsActive={0}", IsActive));
        }

        private void MainWindowActivated(object sender, EventArgs e)
        {
            CurrentDataContext.IsActive = true;
            Debug.WriteLine(string.Format("MainWindowActivated IsActive={0}", IsActive));
        }

        private void TasksListBoxMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
//            tasksListBox.SelectedValue = null;
        }

        private void NotificationsExpanderLostFocus(object sender, RoutedEventArgs e)
        {
            NotificationsExpander.IsExpanded = false;
        }

        private void NotificationsExpanderMouseEnter(object sender, MouseEventArgs e)
        {
            NotificationsExpander.IsExpanded = true;
        }

        private void MainWindow_OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!NotificationsExpander.IsMouseOver)
                NotificationsExpander.IsExpanded = false;
            if (!BoardExpander.IsMouseOver)
                BoardExpander.IsExpanded = false;
        }

        private void MainWindow_OnStateChanged(object sender, EventArgs e)
        {
/*            switch (WindowState)
            {
                case WindowState.Normal:
                    Debug.WriteLine(string.Format("WindowState.Normal IsActive={0}", IsActive));
                    CurrentDataContext.IsActive = true;
                    break;
                case WindowState.Minimized:
                    Debug.WriteLine(string.Format("WindowState.Minimized IsActive={0}", IsActive));
                    CurrentDataContext.IsActive = false;
                    break;
                case WindowState.Maximized:
                    Debug.WriteLine(string.Format("WindowState.Maximized IsActive={0}", IsActive));
                    CurrentDataContext.IsActive = true;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }*/
        }

        private void NewsToggleButton_OnChecked(object sender, RoutedEventArgs e)
        {
            ScheduleTooToggleButton.IsChecked = false;
        }

        private void ScheduleTooToggleButton_OnChecked(object sender, RoutedEventArgs e)
        {
            NewsToggleButton.IsChecked = false;
        }

        private void BtnExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }

    public class TaskbarItemOverlay
    {
        public static readonly DependencyProperty ContentProperty =
            DependencyProperty.RegisterAttached("Content", typeof(object), typeof(TaskbarItemOverlay), new PropertyMetadata(OnPropertyChanged));

        public static readonly DependencyProperty TemplateProperty =
            DependencyProperty.RegisterAttached("Template", typeof(DataTemplate), typeof(TaskbarItemOverlay), new PropertyMetadata(OnPropertyChanged));


        public static object GetContent(DependencyObject dependencyObject)
        {
            return dependencyObject.GetValue(ContentProperty);
        }

        public static void SetContent(DependencyObject dependencyObject, object content)
        {
            dependencyObject.SetValue(ContentProperty, content);
        }

        public static DataTemplate GetTemplate(DependencyObject dependencyObject)
        {
            return (DataTemplate)dependencyObject.GetValue(TemplateProperty);
        }

        public static void SetTemplate(DependencyObject dependencyObject, DataTemplate template)
        {
            dependencyObject.SetValue(TemplateProperty, template);
        }

        private static void OnPropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            var taskbarItemInfo = (TaskbarItemInfo)dependencyObject;
            var content = GetContent(taskbarItemInfo);
            var template = GetTemplate(taskbarItemInfo);

            int i = 0;
            if (content != null)
                i = (int) content;
            if (template == null || content == null || i == 0)
            {
                taskbarItemInfo.Overlay = null;
                return;
            }

            const int ICON_WIDTH = 16;
            const int ICON_HEIGHT = 16;

            var bmp =
                new RenderTargetBitmap(ICON_WIDTH, ICON_HEIGHT, 96, 96, PixelFormats.Default);
            var root = new ContentControl
            {
                ContentTemplate = template,
                Content = content
            };
            root.Arrange(new Rect(0, 0, ICON_WIDTH, ICON_HEIGHT));
            bmp.Render(root);

            taskbarItemInfo.Overlay = bmp;
        }
    }


    /// <summary>
    /// Captures and eats MouseWheel events so that a nested ListBox does not
    /// prevent an outer scrollable control from scrolling.
    /// </summary>
    public sealed class IgnoreMouseWheelBehavior : Behavior<UIElement>
    {

        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.PreviewMouseWheel += AssociatedObject_PreviewMouseWheel;
        }

        protected override void OnDetaching()
        {
            AssociatedObject.PreviewMouseWheel -= AssociatedObject_PreviewMouseWheel;
            base.OnDetaching();
        }

        void AssociatedObject_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {

            e.Handled = true;

            var e2 = new MouseWheelEventArgs(e.MouseDevice, e.Timestamp, e.Delta);
            e2.RoutedEvent = UIElement.MouseWheelEvent;

            AssociatedObject.RaiseEvent(e2);

        }

    }
}
