﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using NLog;
using Tasker.Tools.Managers;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;
using Tasker.Tools.Extensions;
using Tasker.Worker.Model;

namespace Tasker.Worker.Managers
{
    public static class LoadManager
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private static bool _onceTaskLoaded = false;

        public static void LoadData()
        {
            Logger.Info("Loading current tasks");
            LoadCurrentTasks();
            Logger.Info("Loading tasks files");
            LoadTaskFiles();
            Logger.Info("Loading unread messages");
            LoadUnreadMessages();
            Logger.Info("Loading timetables");
            LoadTimeTable();
            Logger.Info("Loading board structure");
            LoadBoard();
        }

        private static void LoadBoard()
        {
            IOrderedEnumerable<FileVersionDTO> fileVersionDtos =
                ServiceManager.Inst.ServiceChannel.GetBoardFiles().OrderBy(t => t.File.Title);
            if (fileVersionDtos.Any())
            {
                FolderManager.LoadBoardStaticStructure(Repository.Folders);
                foreach (var fileVersionDto in fileVersionDtos)
                {
                    FileManager<WorkerFileView>.CreateNewFile(fileVersionDto, Repository.Files, Repository.Folders);
                }
            }
        }

        public static void LoadMyCurrentTask()
        {
            TaskDTO myCurrentTask = ServiceManager.Inst.ServiceChannel.GetMyCurrentTask();
            if (myCurrentTask != null)
            {
                TaskManager.UpdateTask(myCurrentTask, Repository.Customers, Repository.TaskTypes, Repository.Users,
                                       Repository.Channels, null, Repository.Tasks);
            }
        }

        public static void LoadCurrentTasks()
        {
            if (_onceTaskLoaded)
            {
                UpdateCurrentTasks();
                return;
            }
            List<TaskDTO> taskDtos = ServiceManager.Inst.ServiceChannel.GetMyTasks().ToList();
            taskDtos.AddRange(ServiceManager.Inst.ServiceChannel.GetTasksByDate(DateTime.Now));

            if (taskDtos.Any())
            {
                // fill with tasks
                foreach (var taskDto in taskDtos)
                {
                    TaskManager.GetTask(taskDto, Repository.Tasks, Repository.Customers, Repository.TaskTypes,
                                           Repository.Channels, Repository.Users);
                }
            }
            _onceTaskLoaded = true;
        }

        public static void UpdateCurrentTasks()
        {
            List<TaskDTO> taskDtos = ServiceManager.Inst.ServiceChannel.GetMyTasks().ToList();
            taskDtos.AddRange(ServiceManager.Inst.ServiceChannel.GetTasksByDate(DateTime.Now));

            if (taskDtos.Any())
            {
                // fill with tasks
                foreach (var taskDto in taskDtos)
                {
                    TaskManager.UpdateTask(taskDto, Repository.Customers, Repository.TaskTypes, Repository.Users,
                                           Repository.Channels, Repository.LoginUser, Repository.Tasks);
                }
            }
        }

        private static void LoadUnreadMessages()
        {
            //load my Unread messages
            MessageDTO[] messages = ServiceManager.Inst.ServiceChannel.GetUnreadMessages();
            foreach (var dto in messages)
            {
                TaskView taskViews = Repository.Tasks.FirstOrDefault(t => t.Id == dto.Task.Id);
                if (taskViews != null)
                {
                    MessageManager.GetMessage(dto, taskViews, taskViews.Messages.Messages,
                                              Repository.Users, Repository.LoginUser.Id);
                }
            }
        }

        /// <summary>
        /// Loading all files for all tasks
        /// </summary>
        public static void LoadTaskFiles()
        {
            foreach (var taskView in Repository.Tasks.Where(t => t.Status != ConstantsStatuses.Closed))
            {
                AttachFileDTO[] files = ServiceManager.Inst.ServiceChannel.GetFilesData(taskView.Id);
                foreach (var fileDTO in files)
                {
                    TaskFileManager.GetFileData(fileDTO, taskView);
                }
            }
        }

        private static void LoadTimeTable()
        {
            TimeTableDTO[] timeTableDtos = ServiceManager.Inst.ServiceChannel.GetMyTimeTables(DateTime.Now.Date, DateTime.Now.Date.AddDays(366));
            foreach (var timeTableDto in timeTableDtos)
            {
                if (TimeTableManager.GetTimeTable(timeTableDto, Repository.TimeTables, Repository.Users, Repository.Shifts) != null)
                {
                    TimeTableManager.UpdateTimeTableView(timeTableDto, Repository.TimeTables, Repository.Users, Repository.Shifts);
                }
            }
        }

        public static void LoadSelfStatistic(DateTime month)
        {
            DateTime startDate = month.FirstDayOfMonth();
            DateTime endDate = startDate.EndOfLastDayOfMonth();
            TimeSpan taskTotalHoursByDates = ServiceManager.Inst.ServiceChannel.GetTaskTotalHoursByDates(startDate, endDate);
            Repository.MonthTotalHours = taskTotalHoursByDates;
        }
    }
}
