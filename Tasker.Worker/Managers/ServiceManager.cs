﻿using System;
using Tasker.Tools.Extensions;
using Tasker.Tools.Managers;
using Tasker.Worker.ServiceReference;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;
using NLog;

namespace Tasker.Worker.Managers
{
    internal class ServiceManager : ServiceManagerBase<IWorkerInterfaceChannel, WorkerCallback>, IServiceLoginManager
    {
        private readonly Logger _logger;

        protected ServiceManager()
        {
#if DEBUG
PingTimeOut = TimeSpan.FromSeconds(10);
#endif
            NetTcpConnectionMask = "net.tcp://{0}/worker";
            EndpointConfigurationName = "workerEndPoint";
            _logger = LogManager.GetCurrentClassLogger();
        }

        private static ServiceManager _inst;
        public static ServiceManager Inst
        {
            get
            {
                if (_inst == null)
                    _inst = new ServiceManager();
                return _inst;
            }
        }

        public override void CloseAndLogoff()
        {
            LogInfo("Logoff started..");
            Repository.LoginUser = null;
            LogInfo("Clearing tasks..");
            Repository.ClearTasks();
            LogInfo("Clearing timetables..");
            Repository.ClearTimeTables();

            base.CloseAndLogoff();
        }

        public override void LoadData(UserView currentUserView)
        {
            Repository.LoginUser = currentUserView;
            LoadManager.LoadData();
        }

        public override bool DoesUserExist()
        {
            return Repository.LoginUser != null;
        }

        protected override bool ClientTypeCheck(LoginResultDTO loginResult)
        {
            return loginResult.Role != ConstantsRoles.Worker;
        }

        protected override void LogInfo(string message)
        {
            _logger.Info(message);
        }

        protected override void LogError(string message)
        {
            _logger.Error(message);
        }

        protected override void LogErrorEx(string message, Exception e)
        {
            _logger.ErrorException(message, e);
        }

        protected override void LogFatal(string message)
        {
            _logger.Fatal(message);
        }

        protected override void LogFatal(string message, Exception e)
        {
            _logger.FatalException(message, e);
        }

        protected override void DisposeCallback()
        {
            _callback.Do(d => d.Dispose());
        }

        protected override LoginResultDTO Login(string login, string password)
        {
            return _serviceChannel.Login(login, password);
        }

        protected override void Ping()
        {
            _serviceChannel.Ping(DateTime.Now);
        }

        protected override void Logoff()
        {
            _serviceChannel.Logoff();
        }
    }
}
