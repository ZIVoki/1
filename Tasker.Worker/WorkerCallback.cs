﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using NLog;
using Tasker.Tools.CustomEventArgs;
using Tasker.Tools.Managers;
using Tasker.Tools.ServiceReference;
using Tasker.Tools.ViewModel;
using Tasker.Worker.Model;

namespace Tasker.Worker
{
    internal delegate void ArgumentContainsDelegate(object sender, ContentEventArgs e);
    internal delegate void MultiArgumentContainsDelegate(object sender, ContentsEventArgs e);
    internal delegate void TimeTableChangedEventHandler(object sender, TimeTableChangedeventArgs e);
    internal delegate void BoardFileUpdatedEventHandler(object sender, FileVersioChangedEventArgs e);
    
    internal class FileVersioChangedEventArgs
    {
        public FileVersionDTO FileVersion { get; set; }

        public FileVersioChangedEventArgs(FileVersionDTO fileVersion)
        {
            FileVersion = fileVersion;
        }
    }

    internal class TimeTableChangedeventArgs
    {
        public TimeTableDTO TimeTable { get; set; }

        public TimeTableChangedeventArgs(TimeTableDTO timeTable)
        {
            TimeTable = timeTable;
        }
    }

    [CallbackBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, AutomaticSessionShutdown = true)]
    internal class WorkerCallback : ServiceReference.IWorkerInterfaceCallback, IDisposable
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public event ArgumentContainsDelegate LunchFormChangeState;
        public event ArgumentContainsDelegate NewMessageEvent;
        public event TimeTableChangedEventHandler TimeTableChanged;
        public event MultiArgumentContainsDelegate CurrentTaskState;
        public event BoardFileUpdatedEventHandler BoardFileUpdate;

        public static List<MessageDTO> MessagesForNullableTask = new List<MessageDTO>();

        public void ReceiveMessage(MessageDTO msg)
        {
            TaskView taskViews = Repository.Tasks.FirstOrDefault(t => t.Id == msg.Task.Id);
            if (taskViews != null)
            {
                MessageView messageView = MessageManager.GetMessage(msg, taskViews, taskViews.Messages.Messages,
                                                                    Repository.Users, Repository.LoginUser.Id);
                InvokeNewMessageEvent(new ContentEventArgs(messageView));
            }
            else
            {
                MessagesForNullableTask.Add(msg);
            }
        }

        public void UserEnter(int userId)
        {
            //throw new NotImplementedException();
        }

        public void UserLogoff(int userId)
        {
            //throw new NotImplementedException();
        }

        public void FileAdded(int taskId, AttachFileDTO attachFile)
        {
            TaskView taskViews = Repository.Tasks.FirstOrDefault(t => t.Id == taskId);
            if (taskViews != null)
            {
                TaskManager.AddFile(attachFile, taskViews);
            }
        }

        public void TaskTypeAdded(TaskTypeDTO taskType)
        {
            TaskTypeManager.GetTaskType(taskType, Repository.TaskTypes);
        }

        public void TaskTypeUpdated(TaskTypeDTO taskType)
        {
            TaskTypeView taskTypeView = TaskTypeManager.GetTaskType(taskType, Repository.TaskTypes);
            TaskTypeManager.UpdateTaskType(taskTypeView, taskType);
        }

        public void UserTaskTypeAdded(UserTaskTypeDTO userTaskType)
        {
            if (Repository.LoginUser != null && Repository.LoginUser.Id == userTaskType.User.Id)
            {
                UserTaskTypeManager.GetUserTaskType(userTaskType, Repository.LoginUser, Repository.TaskTypes);
            }
        }

        public void UserTaskTypeUpdated(UserTaskTypeDTO userTaskType)
        {
            var utt = UserTaskTypeManager.GetUserTaskType(userTaskType, Repository.LoginUser, Repository.TaskTypes);
            UserTaskTypeManager.UserTaskTypeUpdate(utt, userTaskType, Repository.TaskTypes);
        }

        public void FaultExeptionServerAlarm(FaultDetail createFaultDetail)
        {
            string message = string.Format("Произошла ошибка на стороне сервера {0}", createFaultDetail.Message);
            Logger.Error(message);
            MainWindow.ShowServerError(message);
        }

        public void UserStateChanged(int userId, ConstantsNetworkStates stateId)
        {
            if (Repository.LoginUser != null && Repository.LoginUser.Id == userId)
            {
                switch (stateId)
                {
                    case ConstantsNetworkStates.Online:
                        InvokeLunchFormChangeState(Constants.DinnerFormState.LimitExceed);
                        break;
                    case ConstantsNetworkStates.Lunch:
                        InvokeLunchFormChangeState(Constants.DinnerFormState.Started);
                        break;
                    case ConstantsNetworkStates.Depard:
                        break;
                    case ConstantsNetworkStates.Offline:
                        break;
                }
            }
        }

        public void CustomerUpdated(CustomerDTO customerDTO)
        {
            CustomerManager.CustomerUpdate(Repository.Customers, Repository.Channels, customerDTO);
            Logger.Info(String.Format("New customer item added {0} id:{1}", customerDTO.Title, customerDTO.Id));
        }

        public void TimeTableAdded(TimeTableDTO timeTable)
        {
            TimeTableManager.GetTimeTable(timeTable, Repository.TimeTables, Repository.Users, Repository.Shifts);
            Logger.Info(String.Format("New timetable item added {0} id:{1}", timeTable.StartDate.Date, timeTable.Id));
        }

        public void UserUpdated(UserDTO userDTO)
        {
            if (Repository.LoginUser != null && Repository.LoginUser.Id == userDTO.Id)
            {
                UserManager.UserUpdate(Repository.LoginUser, userDTO);
            }
        }

        public void TaskUpdated(TaskDTO task)
        {
            string message = string.Format("Tasker.Worker updating task {0}", task.Title);
            Debug.WriteLine(message);
            Logger.Info(message);
            if (task.CurrentWorker != null)
            {
                if (task.CurrentWorker.Id == Repository.LoginUser.Id)
                {
                    Debug.WriteLine("Tasker.Worker user the same, just update task {0}", task.Title);
                    TaskView taskViews = Repository.Tasks.FirstOrDefault(t => t.Id == task.Id);
                    if (taskViews != null)
                    {
                        TaskView taskView = TaskManager.UpdateTask(task, Repository.Customers, Repository.TaskTypes,
                                                                   Repository.Users, Repository.Channels, null,
                                                                   Repository.Tasks);
                        InvokeCurrentTaskState(Constants.TaskFormType.UpdateTask, taskView);
                    }
                    else
                    {
                        TaskView taskView = TaskManager.GetTask(task, Repository.Tasks,
                                                       Repository.Customers, Repository.TaskTypes, Repository.Channels, Repository.Users);
                        InvokeCurrentTaskState(Constants.TaskFormType.NewTask, taskView);
                    }
                }
                else
                {
                    Debug.WriteLine("Tasker.Worker user not same, remove task {0}", task.Title);
                    TaskView taskView = Repository.Tasks.FirstOrDefault(t => t.Id == task.Id);
                    Debug.WriteLine("Tasker.Worker user not same, remove task {0}", task.Title);
                    // если она мне назначалась, существует в репозитории
                    if (taskView != null)
                    {
                        taskView = TaskManager.UpdateTask(task, Repository.Customers, Repository.TaskTypes,
                                                                   Repository.Users, Repository.Channels, null,
                                                                   Repository.Tasks);
                        Debug.WriteLine("Tasker.Worker alert about remove task {0}", task.Title);
                        InvokeCurrentTaskState(Constants.TaskFormType.UnsetTask, taskView);
                        Repository.Tasks.Remove(taskView);
                    }
                }
            }
        }

        public void TaskSet(TaskDTO task)
        {
            string message = string.Format("Tasker.Worker set task {0} id:{1}", task.Title, task.Id);
            Debug.WriteLine(message);
            Logger.Info(message);
            TaskView taskView = TaskManager.UpdateTask(task, Repository.Customers,
                                                       Repository.TaskTypes, Repository.Users, Repository.Channels, null,
                                                       Repository.Tasks);
            foreach (var source in Repository.Tasks.Where(t => t.Id != task.Id && t.Status == ConstantsStatuses.InProcess))
            {
                source.Status = ConstantsStatuses.Disabled;
            }
            TaskView firstOrDefault = Repository.Tasks.FirstOrDefault(t => t.Id == task.Id);
            if (firstOrDefault != null) firstOrDefault.Status = ConstantsStatuses.Planing;
        }

        public void NewTask(TaskDTO task)
        {
            string message = string.Format("Tasker.Worker new task {0} id:{1}", task.Title, task.Id);
            Debug.WriteLine(message);
            Logger.Info(message);

            TaskView taskView = TaskManager.GetTask(task, Repository.Tasks,
                                                       Repository.Customers, Repository.TaskTypes, Repository.Channels, Repository.Users);

            Debug.WriteLine("Tasker.Worker add new task and createview {0}", taskView.Title);

            InvokeCurrentTaskState(Constants.TaskFormType.NewTask, taskView);

            Debug.WriteLine("Tasker.Worker invoke event about new task {0}", taskView.Title);
            Debug.WriteLine("Tasker.Worker geting {0} SUCECESS!!!", task.Title);
        }

        public void TaskUnsetSet(int taskId)
        {

            // everithing throw update

/*            TaskView taskView = Repository.Tasks.FirstOrDefault(t => t.Id == taskId);

            if (taskView != null)
            {
                string message = string.Format("Tasker.Worker uset task {0}", taskView.Title);
                Debug.WriteLine(message);
                logger.Info(message);
                if (taskView.Status == ConstantsStatuses.Closed && taskView.CurrentWorker.Id == Repository.LoginUser.Id)
                    return;

                Repository.Tasks.Remove(taskView);
                InvokeCurrentTaskState(Constants.TaskFormType.UnsetTask, taskView);
            }*/
        }

        public void SetTaskReviewResult(bool isSuccess)
        {
            TaskView taskViews = Repository.Tasks.FirstOrDefault(t => t.Status == ConstantsStatuses.SendedForReview);
            if (taskViews == null) return;
            if (isSuccess)
            {
                Logger.Info(string.Format("Task {0} id:{1} accepted.", taskViews.Title, taskViews.Id));
                InvokeCurrentTaskState(Constants.TaskFormType.Success, taskViews);
            }
            else
            {
                Logger.Info(string.Format("Task {0} id:{1} rejected.", taskViews.Title, taskViews.Id));
                InvokeCurrentTaskState(Constants.TaskFormType.ReopenTask, taskViews);
                taskViews.Status = ConstantsStatuses.InProcess;
            }
        }

        public void SetStatusLunchTime(bool isLunch)
        {
            // if isLunch is true then automaticaly invoke UserStateChanged by server
            if (isLunch)
            {
                Logger.Info(String.Format("Lunch is available"));
                Repository.LoginUser.CurrentState = ConstantsNetworkStates.Lunch;
                InvokeLunchFormChangeState(Constants.DinnerFormState.Started);
            }
            else
            {
                Logger.Info(String.Format("Lunch is not available"));
                InvokeLunchFormChangeState(Constants.DinnerFormState.Reject);
            }
        }

        public void TimeTableUpdated(TimeTableDTO timeTable)
        {
            TimeTableManager.UpdateTimeTableView(timeTable, Repository.TimeTables, Repository.Users, Repository.Shifts);
            InvokeTimeTableChanged(new TimeTableChangedeventArgs(timeTable));
            Logger.Info(String.Format("Timetable date {0} id:{1} updated", timeTable.StartDate.Date, timeTable.Id));
        }

        public void ShiftUpdated(ShiftDTO shift)
        {
            ShiftManager.ShiftUpdate(shift, Repository.Shifts);
            Logger.Info(String.Format("Shift {1} id:{0} updated", shift.Id, shift.Title));
        }

        public void BoardFileNewVersionAdded(FileVersionDTO newFileVersionDTO)
        {
            FileManager<WorkerFileView>.UpdateFileView(newFileVersionDTO, Repository.Files, Repository.Folders);
            Logger.Info(String.Format("New file version {0} arrived id: {1}", newFileVersionDTO.File.Title, newFileVersionDTO.Id));
            OnBoardFileUpdates(new FileVersioChangedEventArgs(newFileVersionDTO));
        }

        public void InvokeLunchFormChangeState(Constants.DinnerFormState e)
        {
            ArgumentContainsDelegate handler = LunchFormChangeState;
            if (handler != null) handler(null, new ContentEventArgs(e));
        }

        public void InvokeCurrentTaskState(Constants.TaskFormType e, object dataContext)
        {
            MultiArgumentContainsDelegate handler = CurrentTaskState;
            if (handler != null) handler(null, new ContentsEventArgs(new[] { e, dataContext }));
        }

        public void InvokeNewMessageEvent(ContentEventArgs e)
        {
            ArgumentContainsDelegate handler = NewMessageEvent;
            if (handler != null) handler(null, e);
        }

        public void InvokeTimeTableChanged(TimeTableChangedeventArgs e)
        {
            TimeTableChangedEventHandler handler = TimeTableChanged;
            if (handler != null) handler(null, e);
        }

        public  void OnBoardFileUpdates(FileVersioChangedEventArgs e)
        {
            BoardFileUpdatedEventHandler handler = BoardFileUpdate;
            if (handler != null) handler(null, e);
        }

        public void Dispose()
        {
            
        }
    }
}