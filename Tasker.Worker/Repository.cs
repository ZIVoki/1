﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Tasker.Tools.Extensions;
using Tasker.Tools.ViewModel;
using Tasker.Worker.Configuration;
using Tasker.Worker.Model;

namespace Tasker.Worker
{
    internal class Repository
    {
        private static Config _userConfiguration = null;
        private static UserView _loginUser;

        public static UserView LoginUser
        {
            get{ return _loginUser; }
            set { _loginUser = value; }
        }

        public static Config UserConfiguration
        {
            get { return _userConfiguration; }
            set { _userConfiguration = value; }
        }

        public static DateTime? AcceptCurrentTask { get; set; }
        public static UserView CurrentManager { get; set;}
        public static TimeSpan MonthTotalHours { get; set; }

        private static readonly UniqBindableCollection<CustomerView> _customers = new UniqBindableCollection<CustomerView>();
        private static readonly UniqBindableCollection<UserView> _users = new UniqBindableCollection<UserView>();
        private static readonly UniqBindableCollection<TaskTypeView> _taskTypes = new UniqBindableCollection<TaskTypeView>();

        private static UniqBindableCollection<TaskView> _tasks = new UniqBindableCollection<TaskView>();

        private static UniqBindableCollection<TimeTableView> _timeTables = new UniqBindableCollection<TimeTableView>();
        private static UniqBindableCollection<ShiftView> _shifts = new UniqBindableCollection<ShiftView>();
        private static UniqBindableCollection<ChannelView> _channels = new UniqBindableCollection<ChannelView>();
        private static UniqBindableCollection<MessageView> _messages = new UniqBindableCollection<MessageView>();
        private static UniqBindableCollection<WorkerFileView> _files = new UniqBindableCollection<WorkerFileView>();
        private static ObservableCollection<FolderView> _folders = new ObservableCollection<FolderView>();

        public static UniqBindableCollection<CustomerView> Customers
        {
            get { return _customers; }
        }

        public static UniqBindableCollection<UserView> Users
        {
            get { return _users; }
        }

        public static UniqBindableCollection<TaskTypeView> TaskTypes
        {
            get { return _taskTypes; }
        }

        public static UniqBindableCollection<TaskView> Tasks
        {
            get { return _tasks; }
        }

        public static UniqBindableCollection<TimeTableView> TimeTables
        {
            get { return _timeTables; }
        }

        public static UniqBindableCollection<ShiftView> Shifts
        {
            get { return _shifts; }
        }

        public static UniqBindableCollection<ChannelView> Channels
        {
            get { return _channels; }
        }

        public static TimeTableView CurrentTimeTable
        {
            get
            {
                DateTime now = DateTime.Now;
                return TimeTables.FirstOrDefault(t => t.StartDate <= now && t.EndDate >= now);
            }
        }

        public static UniqBindableCollection<MessageView> Messages
        {
            get { return _messages; }
        }

        public static UniqBindableCollection<WorkerFileView> Files
        {
            get { return _files; }
        }

        public static ObservableCollection<FolderView> Folders
        {
            get { return _folders; }
        }

        public static void ClearTimeTables()
        {
            _timeTables.Clear();
        }

        public static void ClearTasks()
        {
            _tasks.Clear();
        }
    }
}
