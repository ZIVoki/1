﻿using System.Windows.Controls;

namespace Tasker.Worker.Controls
{
	/// <summary>
	/// Interaction logic for TimerControl.xaml
	/// </summary>
	public partial class TimerControl : UserControl
	{
		public TimerControl()
		{
			this.InitializeComponent();
		}
	}
}