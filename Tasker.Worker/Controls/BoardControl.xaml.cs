﻿using System.Windows.Controls;
using Tasker.Worker.Model;

namespace Tasker.Worker.Controls
{
    /// <summary>
    /// Interaction logic for BoardControl.xaml
    /// </summary>
    public partial class BoardControl : UserControl
    {
        public BoardModel CurrentBoardModel
        {
            get { return (BoardModel) DataContext; }
            set { DataContext = value; }
        }

        public BoardControl()
        {
            InitializeComponent();
        }
    }
}
