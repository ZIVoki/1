﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Tasker.Tools.Managers;
using Tasker.Tools.ServiceReference;
using Tasker.Worker.Managers;
using Tasker.Worker.Model;

namespace Tasker.Worker.Controls
{
    /// <summary>
    /// Interaction logic for TaskControl.xaml
    /// </summary>
    public partial class TaskControl : UserControl
    {
        public TaskControl()
        {
            this.InitializeComponent();

            DescriptionRichTextBox.AddHandler(Control.MouseLeftButtonUpEvent, new MouseButtonEventHandler((sender, args) =>
                { FilesToggleButton.IsChecked = false; }), true);
            SendTaskForReviewButton.AddHandler(Control.MouseLeftButtonUpEvent, new MouseButtonEventHandler((sender, args) =>
                { FilesToggleButton.IsChecked = false; }), true);
            SendMessageButton.AddHandler(Control.MouseLeftButtonUpEvent, new MouseButtonEventHandler((sender, args) =>
                { FilesToggleButton.IsChecked = false; }), true);
            AddNewFileButton.AddHandler(Control.MouseLeftButtonUpEvent, new MouseButtonEventHandler((sender, args) =>
                { FilesToggleButton.IsChecked = false; }), true);
        }

        private void TaskUserControlMouseEnter(object sender, MouseEventArgs e)
        {
            ReadTask();
        }

        protected override void OnLostFocus(RoutedEventArgs e)
        {
            base.OnLostFocus(e);
            FilesToggleButton.IsChecked = false;
        }

        protected override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonUp(e);
            if (!FilesItemsControl.IsMouseOver) FilesToggleButton.IsChecked = false;
        }

        private void ReadTask()
        {
            TaskModel taskModel = ((TaskModel)DataContext);
            if (taskModel != null)
                if (taskModel.Task != null)
                {
                    if (taskModel.Task.Status == ConstantsStatuses.Planing)
                    {
                        ServiceManager.Inst.ServiceChannel.AcceptTask(taskModel.Task.Id);
                        TaskDTO myCurrentTask = ServiceManager.Inst.ServiceChannel.GetMyCurrentTask();
                        if (myCurrentTask == null) return;
                        if (myCurrentTask.Id == taskModel.Task.Id)
                        {
                            taskModel.Task.Status = ConstantsStatuses.InProcess;
                        }
                        else
                        {
                            taskModel.Task.Status = ConstantsStatuses.Disabled;
                        }
                        AttachFileDTO[] files = ServiceManager.Inst.ServiceChannel.GetFilesData(taskModel.Task.Id);
                        foreach (var fileDTO in files)
                        {
                            TaskFileManager.GetFileData(fileDTO, taskModel.Task);
                        }
                    }
                }
        }

        private void FilesItemsControl_OnLostFocus(object sender, RoutedEventArgs e)
        {
            FilesToggleButton.IsChecked = false;
        }
    }
}