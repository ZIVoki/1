﻿using System.Windows.Controls;

namespace Tasker.Worker.Controls
{
	/// <summary>
	/// Interaction logic for SelfStatControl.xaml
	/// </summary>
	public partial class SelfStatControl : UserControl
	{
		public SelfStatControl()
		{
			this.InitializeComponent();
		}
	}
}