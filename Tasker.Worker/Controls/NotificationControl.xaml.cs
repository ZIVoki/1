﻿using System.Windows.Controls;
using System.Windows.Input;
using Tasker.Tools.ViewModel;

namespace Tasker.Worker.Controls
{
    /// <summary>
    /// Interaction logic for NotificationControl.xaml
    /// </summary>
    public partial class NotificationControl : UserControl
    {
        public NotificationControl()
        {
            InitializeComponent();
        }

        private void ButtonMouseEnter(object sender, MouseEventArgs e)
        {
            Button button = (Button) sender;
            if (button == null) return;
            NotificationView notificationView = (NotificationView)button.DataContext;
            if (notificationView == null) return;
            notificationView.IsRead = true;
        }
    }
}
