﻿namespace Tasker.Worker
{
    public class Constants
    {
        public enum DinnerFormState
        {
            Request,
            Started,
            Reject,
            Late,
            LimitExceed,
            None
        }

        public enum TaskFormType
        {
            NewTask,
            FinishTaskWait,
            WaitForTask,
            Success,
            UnsetTask,
            BackgroundTaskSet,
            ReopenTask,
            UpdateTask
        }
        
        // to remember last password
        public static string Password { get; set; }

        public static bool IsDomainUser { get; set; }
    }
}
