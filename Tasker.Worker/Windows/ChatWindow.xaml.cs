﻿using System.Windows;
using Tasker.Worker.Model;

namespace Tasker.Worker.Windows
{
    /// <summary>
    /// Interaction logic for ChatWindow.xaml
    /// </summary>
    public partial class ChatWindow : Window
    {
        public TaskModel CurrentTaskModel
        {
            get { return (TaskModel) DataContext; }
            set { DataContext = value; }
        }

        public ChatWindow()
        {
            InitializeComponent();
            MainScrollViewer.ScrollToBottom();
        }

        protected override void OnActivated(System.EventArgs e)
        {
            base.OnActivated(e);
            CurrentTaskModel.Task.Messages.SetAllAsRead();
        }

        private void NewMessageTextBoxGotFocus(object sender, RoutedEventArgs e)
        {
            TxtNewMessageTextBox.Text = "";
        }
    }
}
