﻿using System;
using System.Diagnostics;
using System.Windows;
using Microsoft.Win32;
using Tasker.Worker.Model;

namespace Tasker.Worker.Windows
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    internal partial class LoginWindow : Window
    {
        private static int _iniCounter = 0;

        public LoginModel CurrentLoginModel
        {
            get { return (LoginModel) DataContext; }
            set { DataContext = value; }
        }

        public LoginWindow()
        {
            InitializeComponent();
            _iniCounter++;
            if (_iniCounter <= 1)
                DataContext = new LoginModel();
            else
                DataContext = new LoginModel(_iniCounter);

            CurrentLoginModel.RequestClose += CurrentLoginModel_RequestClose; 
            SystemEvents.SessionSwitch += SystemEvents_SessionSwitch;

            TryAutoLogin();
        }

        private void TryAutoLogin()
        {
            if (CurrentLoginModel.IsAutoLogin.HasValue && CurrentLoginModel.IsAutoLogin.Value && _iniCounter == 1 &&
                CurrentLoginModel.IsDomainUser)
            {
                Debug.WriteLine("Autologin starts");
                CurrentLoginModel.StartLoginCommand.Execute(null);
            }
        }

        void SystemEvents_SessionSwitch(object sender, SessionSwitchEventArgs e)
        {
            Debug.WriteLine("Session switch! {0}", e.Reason);
            if (e.Reason == SessionSwitchReason.SessionUnlock || e.Reason == SessionSwitchReason.SessionLogon)
            {
                if (CurrentLoginModel.IsAutoLogin.HasValue && CurrentLoginModel.IsAutoLogin.Value && CurrentLoginModel.IsDomainUser)
                    CurrentLoginModel.StartLoginCommand.Execute(null);
            }
        }

        void CurrentLoginModel_RequestClose(object sender, EventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }

        protected override void OnClosed(EventArgs e)
        {
            CurrentLoginModel.RequestClose -= CurrentLoginModel_RequestClose;
            SystemEvents.SessionSwitch -= SystemEvents_SessionSwitch;
            base.OnClosed(e);
        }
    }
}
