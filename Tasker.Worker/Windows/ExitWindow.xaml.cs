﻿using System.Windows;

namespace Tasker.Worker.Windows
{
    /// <summary>
    /// Interaction logic for ExitWindow.xaml
    /// </summary>
    public partial class ExitWindow : Window
    {
        public new bool? DialogResult = new bool?();
        public bool IsClosing = false;


        public ExitWindow()
        {
            InitializeComponent();
        }

        private void YesButton_OnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            IsClosing = true;
            this.Close();
        }

        private void NoButton_OnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            IsClosing = true;
            this.Close();
        }
    }
}
