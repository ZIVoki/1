﻿using System;
using System.Diagnostics;
using System.ServiceProcess;
using System.ServiceModel;
using Tasker.Server.Services;

namespace Tasker.HostService
{
    public partial class TaskerHostService : ServiceBase
    {
        ServiceHost _serviceHost;
        ServiceHost _managerServiceHost;
        private const string TaskerSource = "TaskerServerSource";
        private const string LogName = "TaskerSeverLog";

        public TaskerHostService()
        {
            InitializeComponent();

            if (!EventLog.SourceExists(TaskerSource))
            {
                EventLog.CreateEventSource(TaskerSource, LogName);
            }
            eventLog.Source = TaskerSource;
            eventLog.Log = LogName;
        }

        protected override void OnStart(string[] args)
        {
            _serviceHost = new ServiceHost(typeof(WorkerService));
            _managerServiceHost = new ServiceHost(typeof(ManagerService));
            try
            {
                _serviceHost.Open();
                string workerServiceStarted = "Worker service started..";
                foreach (var endpoint in _serviceHost.Description.Endpoints)
                {
                    workerServiceStarted = String.Format("{0}\n{1}", workerServiceStarted, endpoint.Address);
                }
                eventLog.WriteEntry(workerServiceStarted, EventLogEntryType.Information);

                _managerServiceHost.Open();
                string managerServiceStarted = "Manager service started..";
                foreach (var endpoint in _managerServiceHost.Description.Endpoints)
                {
                    managerServiceStarted = String.Format("{0}\n{1}", managerServiceStarted, endpoint.Address);
                }
                eventLog.WriteEntry(managerServiceStarted, EventLogEntryType.Information);
            }
            catch (Exception e)
            {
                eventLog.WriteEntry(e.StackTrace, EventLogEntryType.FailureAudit);
            }
        }

        protected override void OnStop()
        {
            if (_serviceHost.State == CommunicationState.Opened)
            {
                try
                {
                    eventLog.WriteEntry("Worker service closed..", EventLogEntryType.Information);
                    _serviceHost.Close();
                }
                catch (Exception e)
                {
                    eventLog.WriteEntry(e.StackTrace, EventLogEntryType.FailureAudit);
                }
            }
            if (_managerServiceHost.State == CommunicationState.Opened)
            {
                try
                {
                    eventLog.WriteEntry("Manager service closed..", EventLogEntryType.Information);
                    _managerServiceHost.Close();
                }
                catch (Exception e)
                {
                    eventLog.WriteEntry(e.StackTrace, EventLogEntryType.FailureAudit);
                }
            }
            Server.ServiceCloser.Close();
        }

        protected override void OnShutdown()
        {
            Server.ServiceCloser.Close();
            base.OnShutdown();
        }
    }
}
